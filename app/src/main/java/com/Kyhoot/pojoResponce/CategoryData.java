package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 31/1/18.
 */

public class CategoryData
{
    private boolean selected=false;

    private String bannerImageApp;

    private String can_fees;

    private String packupTime;

    private String prepTime;

    private String bannerImageWeb;

    private String sel_img;

    private String service_type;

    private String bookingType;

    private String cat_desc;

    private String unsel_img;

    private String city;

    private String cat_name;

    private String miximum_fees;

    private String city_id;

    private String bussinessGroup;

    private String _id;

    private String bookingTypeRepeate;

    private String billing_model;

    private String bussinessgp;

    private String minimum_fees;

    private int minimum_hour;

    private String surgePrice;

    private float price_per_fees;

    private float visit_fees;

    private BookingTypeAction bookingTypeAction;


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    public float getPrice_per_fees() {
        return price_per_fees;
    }

    public String getBannerImageApp ()
    {
        return bannerImageApp;
    }

    public void setBannerImageApp (String bannerImageApp)
    {
        this.bannerImageApp = bannerImageApp;
    }

    public String getCan_fees ()
    {
        return can_fees;
    }

    public void setCan_fees (String can_fees)
    {
        this.can_fees = can_fees;
    }

    public String getPackupTime ()
    {
        return packupTime;
    }

    public void setPackupTime (String packupTime)
    {
        this.packupTime = packupTime;
    }

    public String getPrepTime ()
    {
        return prepTime;
    }

    public void setPrepTime (String prepTime)
    {
        this.prepTime = prepTime;
    }

    public String getBannerImageWeb ()
    {
        return bannerImageWeb;
    }

    public void setBannerImageWeb (String bannerImageWeb)
    {
        this.bannerImageWeb = bannerImageWeb;
    }

    public String getSel_img ()
    {
        return sel_img;
    }

    public void setSel_img (String sel_img)
    {
        this.sel_img = sel_img;
    }

    public String getService_type ()
    {
        return service_type;
    }

    public void setService_type (String service_type)
    {
        this.service_type = service_type;
    }

    public String getBookingType ()
    {
        return bookingType;
    }

    public void setBookingType (String bookingType)
    {
        this.bookingType = bookingType;
    }

    public String getCat_desc ()
    {
        return cat_desc;
    }

    public void setCat_desc (String cat_desc)
    {
        this.cat_desc = cat_desc;
    }

    public String getUnsel_img ()
    {
        return unsel_img;
    }

    public void setUnsel_img (String unsel_img)
    {
        this.unsel_img = unsel_img;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getCat_name ()
    {
        return cat_name;
    }

    public void setCat_name (String cat_name)
    {
        this.cat_name = cat_name;
    }

    public String getMiximum_fees ()
    {
        return miximum_fees;
    }

    public void setMiximum_fees (String miximum_fees)
    {
        this.miximum_fees = miximum_fees;
    }

    public String getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getBussinessGroup ()
    {
        return bussinessGroup;
    }

    public void setBussinessGroup (String bussinessGroup)
    {
        this.bussinessGroup = bussinessGroup;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getBookingTypeRepeate ()
    {
        return bookingTypeRepeate;
    }

    public void setBookingTypeRepeate (String bookingTypeRepeate)
    {
        this.bookingTypeRepeate = bookingTypeRepeate;
    }

    public String getBilling_model ()
    {
        return billing_model;
    }

    public void setBilling_model (String billing_model)
    {
        this.billing_model = billing_model;
    }

    public String getBussinessgp ()
    {
        return bussinessgp;
    }

    public void setBussinessgp (String bussinessgp)
    {
        this.bussinessgp = bussinessgp;
    }

    public String getMinimum_fees ()
    {
        return minimum_fees;
    }

    public int getMinimum_hour() {
        return minimum_hour;
    }

    public String getSurgePrice() {
        return surgePrice;
    }

    public void setMinimum_fees (String minimum_fees)
    {
        this.minimum_fees = minimum_fees;
    }

    public float getVisit_fees() {
        return visit_fees;
    }

    public BookingTypeAction getBookingTypeAction() {
        return bookingTypeAction;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bannerImageApp = "+bannerImageApp+", can_fees = "+can_fees+", packupTime = "+packupTime+", prepTime = "+prepTime+", bannerImageWeb = "+bannerImageWeb+", sel_img = "+sel_img+", service_type = "+service_type+", bookingType = "+bookingType+", cat_desc = "+cat_desc+", unsel_img = "+unsel_img+", city = "+city+", cat_name = "+cat_name+", miximum_fees = "+miximum_fees+", city_id = "+city_id+", bussinessGroup = "+bussinessGroup+", _id = "+_id+", bookingTypeRepeate = "+bookingTypeRepeate+", billing_model = "+billing_model+", bussinessgp = "+bussinessgp+", minimum_fees = "+minimum_fees+"]";
    }
}