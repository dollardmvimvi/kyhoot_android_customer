package com.Kyhoot.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.main.BookingPendingAccept;
import com.Kyhoot.main.LiveStatus;
import com.Kyhoot.pojoResponce.CancelReasonPojo;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * Created by embed on 16/8/16.
 */
public class CancelAdapter extends BaseAdapter {
    public int selectedpostion = -1;
    private Context mContext;
    private ArrayList<CancelReasonPojo.CancelReasonData.Reason> provider = new ArrayList<>();
    private AppTypeface regular;
    private boolean isPending;

    public CancelAdapter(Context context, ArrayList<CancelReasonPojo.CancelReasonData.Reason> data, boolean isPending) {

        this.mContext = context;
        provider = data;
        this.isPending = isPending;
        regular = AppTypeface.getInstance(mContext);
    }

    @Override
    public int getCount() {
        return provider.size();
    }

    @Override
    public Object getItem(int position) {

        return provider.get(position);
    }

    @Override
    public long getItemId(int position) {

        return provider.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cancel_bookingadapter, parent, false);
            holder.tvforcancel = convertView.findViewById(R.id.tvforcancel);
            holder.imageis = convertView.findViewById(R.id.imageis);

            if (isPending) {
                BookingPendingAccept bookingPendingAccept = (BookingPendingAccept) mContext;
                if (position == bookingPendingAccept.selectedpostion) {
                    holder.imageis.setVisibility(View.VISIBLE);

                } else {
                    holder.imageis.setVisibility(View.GONE);
                }
                holder.tvforcancel.setTypeface(regular.getHind_regular());
            } else {
                LiveStatus liveBookingStatus = (LiveStatus) mContext;
                if (position == liveBookingStatus.selectedpostion) {
                    holder.imageis.setVisibility(View.VISIBLE);
                } else {
                    holder.imageis.setVisibility(View.GONE);
                }
                holder.tvforcancel.setTypeface(regular.getHind_regular());
            }

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvforcancel.setText(provider.get(position).getReason());

        return convertView;
    }

    public class ViewHolder {
        TextView tvforcancel;
        ImageView imageis;

    }
}
