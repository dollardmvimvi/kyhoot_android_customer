package com.Kyhoot.models;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.BuildConfig;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.EmailPhoneValidtn;
import com.Kyhoot.interfaceMgr.RegisterOTPResponce;
import com.Kyhoot.interfaceMgr.ValidateEmail;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.pojoResponce.SignUpData;
import com.Kyhoot.pojoResponce.SignUpInPojo;
import com.Kyhoot.pojoResponce.SignUpPojo;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * <h>SignupModel</h>
 * this contains the business logic
 * Created by Ali on 8/25/2017.
 */

public class SignupModel
{

    private static final String TAG = "SignupModel";

    public String getForMatedDOB(Context mcontext, String fb_birthDay) {
        try {
            Log.d("DATEOFBIRTH", "getForMatedDOB: " + fb_birthDay);
            Calendar newCalendar = Calendar.getInstance();
            String split[] = fb_birthDay.split("/");
            int day = Integer.parseInt(split[0]);
            newCalendar.set(Integer.parseInt(split[2]), Integer.parseInt(split[1]) - (1), day);
            Log.d("FACEBOOKDOB", "getForMatedDOB: " + Integer.parseInt(split[2]) + "-" + (Integer.parseInt(split[1]) - (1)) + "-"
                    + day);
            String month = "";
            if ((Integer.parseInt(split[1]) - (1)) < 10) {
                int mon = Integer.parseInt(split[1]) - (1);
                month = "0" + mon;
            } else
                month = "" + (Integer.parseInt(split[1]) - (1));
            VariableConstant.dateOB = Integer.parseInt(split[2]) + "-" + month + "-" + day;

            return DateUtils.formatDateTime(mcontext,
                    newCalendar.getTimeInMillis(),
                    DateUtils.FORMAT_SHOW_DATE
                            | DateUtils.FORMAT_SHOW_YEAR
                            | DateUtils.FORMAT_ABBREV_MONTH
                            | DateUtils.FORMAT_ABBREV_WEEKDAY);
        } catch (NumberFormatException e) {
            Calendar newCalendar = Calendar.getInstance();
            String spliteddate[] = Utilities.dateintwtfour().split(" ");
            String splitDate[] = spliteddate[0].split("-");
            Log.d("SplitedCatch", "getForMatedDOB: " + splitDate[0] + " :month: " + splitDate[1] + " :days: " + splitDate[2]);
            newCalendar.set(Integer.parseInt(splitDate[0]), Integer.parseInt(splitDate[1]) - (1), Integer.parseInt(splitDate[2]));
            return DateUtils.formatDateTime(mcontext,
                    newCalendar.getTimeInMillis(),
                    DateUtils.FORMAT_SHOW_DATE
                            | DateUtils.FORMAT_SHOW_YEAR
                            | DateUtils.FORMAT_ABBREV_MONTH
                            | DateUtils.FORMAT_ABBREV_WEEKDAY);
        }

    }

    public void emailValidationRequest(String email, final EmailPhoneValidtn emailPhoneValidtn, final boolean isSignup)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
            Log.d(TAG, "emailValidationRequest: "+jsonObject);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "emailValidation", OkHttpConnection.Request_type.POST
                    , jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerResponse, String result)
                        {
                            Log.d("EMAILVALID", "onSuccess: " + result + " headerResponse " + headerResponse);
                            emailPhoneValidtn.onEmailValid(true, "", isSignup);

                        }

                        @Override
                        public void onError(String headerError, String error)
                        {
                           /*Gson gson = new Gson();
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                            emailPhoneValidtn.onEmailValid(false, errorHandel.getMessage(), isSignup);*/

                            if (headerError.equals("502"))
                                emailPhoneValidtn.onEmailValid(false, "Server issue", isSignup);
                            else {
                                try{
                                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                    emailPhoneValidtn.onEmailValid(false, errorHandel.getMessage(), isSignup);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void emailValidationRequestFacebook(String email, final ValidateEmail validateEmail)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "emailValidation", OkHttpConnection.Request_type.POST
                    , jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerResponse, String result)
                        {
                            validateEmail.onSuccessEmailValidate(headerResponse,result);
                        }

                        @Override
                        public void onError(String headerError, String error)
                        {
                            if (headerError.equals("502"))
                                validateEmail.onFailureEmailValidate(headerError, "Server issue");
                            else {
                                try{
                                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                    validateEmail.onFailureEmailValidate(headerError, errorHandel.getMessage());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void mobileNumberVerifcation(String phone, String countryCode, final EmailPhoneValidtn emailPhoneValidtn, final boolean isSignUp)
    {
        String mobileNumberWithoutZero = "";
        if (phone.trim().length() > 0) {
            if (phone.trim().charAt(0) == '0') {
                mobileNumberWithoutZero = phone.substring(1);
            } else {
                mobileNumberWithoutZero = phone.trim();
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("phone",mobileNumberWithoutZero);
                jsonObject.put("countryCode",countryCode);
                Log.d(TAG, "mobileNumberVerifcation: "+jsonObject);
                OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "phoneNumberValidation", OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String result)
                    {
                        Log.d("PHONEVALID ", "onSuccess: " + result + " headerResponse " + headerResponse);
                        emailPhoneValidtn.onPhoneValid(true, "", isSignUp);
                    }

                    @Override
                    public void onError(String headerError, String error) {

                      /*  ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                        emailPhoneValidtn.onPhoneValid(false, errorHandel.getMessage(), isSignUp);*/
                        if (headerError.equals("502"))
                            emailPhoneValidtn.onPhoneValid(false, "Server issue", isSignUp);
                        else {
                            try {
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                emailPhoneValidtn.onPhoneValid(false, errorHandel.getMessage(), isSignUp);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }catch (JSONException e)
            {
                e.printStackTrace();
            }

        }
    }

    public void otapVerification(SignUpInPojo pojo, final RegisterOTPResponce regOtpResp, SharedPrefs spref) {
        JSONObject jsonObject = new JSONObject();
        final SignUpData data = pojo.getData();
        if (data.getPhone().trim().length() > 0) {
            if (data.getPhone().trim().charAt(0) == '0')
                data.setPhone(data.getPhone().substring(1));
            else
                data.setPhone(data.getPhone().trim());
        }
        try {
            jsonObject.put("mobile",data.getPhone());
            jsonObject.put("countryCode",data.getCountryCode());
            jsonObject.put("userType",1);
            Log.d("SIGNUP", "otapVerification: "+jsonObject);

            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "signUpVerificationCode", OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result) {

                    Log.d("SIGNUP", "onSuccess: " + result + " headerResponse " + headerResponse);
                    ValidatorPojo otpVerfication;
                    Gson gson = new Gson();
                    otpVerfication = gson.fromJson(result, ValidatorPojo.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("fName",data.getFirstName());
                    bundle.putString("lName",data.getLastName());
                    bundle.putString("email",data.getEmail());
                    bundle.putString("password",data.getPassword());
                    bundle.putString("countryCode",data.getCountryCode());
                    bundle.putString("mobile",data.getPhone());
                    bundle.putString("dateOfBirth",data.getDatOB());
                    bundle.putString("profilePic",data.getProfilePic());
                    bundle.putString("facebookId",data.getFbId());
                    bundle.putString("referralCode", data.getReferralcode());
                    bundle.putString("SignupOrForgot","SIGNUP");
                    bundle.putInt("loginType",data.getLoginType());

                    regOtpResp.otpResponce(otpVerfication.getErrMsg(),bundle);

                }

                @Override
                public void onError(String headerError, String error) {
                   /* ErrorHandel vPojo = new Gson().fromJson(error, ErrorHandel.class);
                    regOtpResp.onOTPError(vPojo.getMessage());*/

                    if (headerError.equals("502"))
                        regOtpResp.onOTPError("Server issue");
                    else {
                        ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                        regOtpResp.onOTPError(errorHandel.getMessage());
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void signupService(JSONObject jsonObject, final RegisterOTPResponce verifyOtpIntrface, final SharedPrefs spref, Activity mcontext) {
        String countryCode = "",countrySymbol="", mobileNumber = "";
        try {
            countryCode = jsonObject.get("countryCode").toString();
            mobileNumber = jsonObject.get("phone").toString();
            countrySymbol = jsonObject.get("countrySymbol").toString();
            Log.d("SIGNUPS", "signupService: " + countryCode + " Number " + mobileNumber+" CountrySymbol "+countrySymbol);
            jsonObject.put("latitude", spref.getSplashLatitude());
            jsonObject.put("longitude", spref.getSplashLongitude());
            jsonObject.put("termsAndCond", 1);
            jsonObject.put("devType", 2);
            jsonObject.put("deviceId", spref.getDeviceId());
            jsonObject.put("countrySymbol",countrySymbol);
            jsonObject.put("pushToken", spref.getRegistrationId());
            jsonObject.put("appVersion", BuildConfig.VERSION_NAME);
            jsonObject.put("deviceOsVersion", Build.VERSION.RELEASE);
            jsonObject.put("devMake", Build.BRAND);
            jsonObject.put("devModel", Build.MODEL);
            jsonObject.put("deviceTime", Utilities.dateintwtfour());
            jsonObject.put("ipAddress", VariableConstant.setIPAddress);
            Log.d("SIGNUPSERVICE", "signupService: " + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String finalCountryCode = countryCode;
        final String finalMobileNumber = mobileNumber;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "registerUser", OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result) {
                Log.d("SIGNUPS", "onSuccess: " + result + " headerResponse " + headerResponse);
                SignUpPojo vpojo = new Gson().fromJson(result, SignUpPojo.class);
                if (vpojo != null) {

                    String sid = vpojo.getData().getSid();
                    Bundle bundle = new Bundle();
                    bundle.putString("countryCode", "" + finalCountryCode);
                    bundle.putString("mobile", "" + finalMobileNumber);
                    bundle.putString("SID", "" + sid);
                    bundle.putString("SignupOrForgot", "SIGNUP");
                    verifyOtpIntrface.otpResponce(vpojo.getErrMsg() + "", bundle);
                } else {
                    verifyOtpIntrface.onOTPError("Something went wrong");
                }
            }

            @Override
            public void onError(String headerError, String error) {
               /* ErrorHandel vpojo = new Gson().fromJson(error, ErrorHandel.class);
                verifyOtpIntrface.onOTPError(vpojo.getMessage());*/

                if (headerError.equals("502"))
                    verifyOtpIntrface.onOTPError("Server issue");
                else {
                    try{
                        ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                        verifyOtpIntrface.onOTPError(errorHandel.getMessage());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    public void refferalCodeValidation(String referralCode,final EmailPhoneValidtn emailPhoneValidtn)
    {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("referralCode",referralCode);
            Log.d(TAG, "refferalCodeValidation: "+jsonObject);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "referralCodeValidation", OkHttpConnection.Request_type.POST
                    , jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerResponse, String result)
                        {
                            Log.d("REFERRALCODEVALIDATION", "onSuccess: " + result + " headerResponse " + headerResponse);
                            emailPhoneValidtn.onReferralCodeValid(true, "");

                        }

                        @Override
                        public void onError(String headerError, String error)
                        {
                           /* Gson gson = new Gson();
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                            emailPhoneValidtn.onEmailValid(false, errorHandel.getMessage(), isSignup);*/

                            if (headerError.equals("502"))
                                emailPhoneValidtn.onReferralCodeValid(false, "Server issue");
                            else {
                                try{
                                    ErrorHandleMsg errorHandel = new Gson().fromJson(error, ErrorHandleMsg.class);
                                    emailPhoneValidtn.onReferralCodeValid(false, errorHandel.getMessage());
                                }catch(Exception e){
                                    e.printStackTrace();
                                }

                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkNetwork(AlertProgress progress, CheckNetworkAvailablity networkavailble, String fb_emailId, String fbId, int logintype, String dateOfBirth, String fb_firstName, String fb_lastName) {
        LoginPojo loginPojo = new LoginPojo();
        if (progress.isNetworkAvailable()) {
            SignUpData loginData = new SignUpData();
            loginPojo.setData(loginData);
            loginPojo.getData().setFbId(fbId);
            loginPojo.getData().setPassword(fbId);
            loginPojo.getData().setEmail(fb_emailId);
            loginPojo.getData().setLoginType(logintype);
            loginPojo.getData().setDatOB(dateOfBirth);
            loginPojo.getData().setFirstName(fb_firstName);
            loginPojo.getData().setLastName(fb_lastName);
            networkavailble.networkAvalble(loginPojo);
        } else
            networkavailble.notNetworkAvalble();
    }
    /**
     * Represents an signinService login/registration task used to authenticate
     * the user.
     */
    public void signinService(final LoginPojo loginPojo, final ValidateEmail callback, final SharedPrefs sharedPrefs)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailOrPhone",loginPojo.getData().getEmail());
            jsonObject.put("password",loginPojo.getData().getPassword());
            jsonObject.put("deviceId", sharedPrefs.getDeviceId());
            jsonObject.put("pushToken", sharedPrefs.getRegistrationId());
            jsonObject.put("appVersion", BuildConfig.VERSION_NAME);
            jsonObject.put("devMake", Build.BRAND);//device os
            jsonObject.put("devModel",Build.MODEL);
            jsonObject.put("devType",2);
            jsonObject.put("deviceTime", Utilities.dateintwtfour());
            jsonObject.put("loginType",loginPojo.getData().getLoginType());
            jsonObject.put("facebookId",loginPojo.getData().getFbId());
            jsonObject.put("deviceOsVersion",Build.VERSION.RELEASE);
            jsonObject.put("latitude", sharedPrefs.getSplashLatitude());
            jsonObject.put("longitude", sharedPrefs.getSplashLongitude());
            Log.d(TAG, "signinService: "+jsonObject);
            sharedPrefs.setOldpassword(loginPojo.getData().getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "signIn", OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result)
            {
                Log.d(TAG, "LOginonSuccess: " + result + " HeaderResponce " + headerResponse);
                try {
                    if (headerResponse.equals("200")) {
                        LoginPojo lPojo;
                        Gson gson = new Gson();

                        lPojo = gson.fromJson(result, LoginPojo.class);
                        JSONObject props = new JSONObject();
                        props.put("Name", lPojo.getData().getFirstName() + " " + lPojo.getData().getLastName());
                        props.put("Email", lPojo.getData().getEmail());
                        props.put("UserId", lPojo.getData().getSid());
                        props.put("date", Utilities.dateintwtfour());
                        props.put("SignUpMethod", "Facebook");
                        props.put("Phone", lPojo.getData().getCountryCode()+""+lPojo.getData().getPhone());
                        props.put("DOB",lPojo.getData().getDatOB());
                        if(lPojo.getData().getCardDetail()!=null && lPojo.getData().getCardDetail().getLast4()!=null)
                            props.put("CreditCard","Complete");
                        else
                            props.put("CreditCard","Not complete");
                        // callGetProvider(sharedPrefs,lPojo.getData().getToken());
                        sharedPrefs.setCustomerSid(lPojo.getData().getSid());
                        sharedPrefs.setSession(lPojo.getData().getToken());
                        sharedPrefs.setRequesterId(lPojo.getData().getRequester_id());
                        sharedPrefs.setIsLogin(true);
                       /* if(!ApplicationController.getInstance().isMqttConnected())
                            ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());*/
                        sharedPrefs.setImageUrl(lPojo.getData().getProfilePic());
                        sharedPrefs.setEMail(lPojo.getData().getEmail());
                        sharedPrefs.setFirstname(lPojo.getData().getFirstName());
                        sharedPrefs.setLastname(lPojo.getData().getLastName());
                        sharedPrefs.setPassword(lPojo.getData().getPassword());
                        sharedPrefs.setMobileNo(lPojo.getData().getPhone());
                        sharedPrefs.setCountryCode(lPojo.getData().getCountryCode());
                        sharedPrefs.setPaymentId(lPojo.getData().getPublishableKey());
                        Log.e(TAG, "onSuccess: Referral "+lPojo.getData().getReferralCode());
                        sharedPrefs.setReferral(lPojo.getData().getReferralcode());
                        sharedPrefs.setFcmTopic(lPojo.getData().getFcmTopic()); //FCM TOPIC which is used to get push topic
                        Log.d(TAG, "onSuccess: "+lPojo.getData().getCardDetail() +" detailsPojo "+lPojo.getData().getCardDetail().getLast4());
                        if(lPojo.getData().getCardDetail()!=null && lPojo.getData().getCardDetail().getLast4()!=null)
                        {
                            String card = "**** **** **** " + " " + lPojo.getData().getCardDetail().getLast4();
                            sharedPrefs.setDefaultCardNum(lPojo.getData().getCardDetail().getLast4());
                            sharedPrefs.setDefaultCardBrand(lPojo.getData().getCardDetail().getBrand());
                            sharedPrefs.setDefaultCardId(lPojo.getData().getCardDetail().getId());
                        }

                        callback.onLoginSuccess();
                    } else {
                        callback.onLoginFailure(headerResponse);
                    }

                } catch (Exception e) {
                    callback.onLoginFailure(e.toString());
                }


            }

            @Override
            public void onError(String headerError, String error) {
                Log.d(TAG, "onError: " + error + " header " + headerError);

                if(headerError.equals("502")||headerError.equals("549") ) {
                    callback.onLoginFailure("server down");
                }else {
                    Gson gson = new Gson();
                    ErrorHandel lPojo=null;
                    try{
                        lPojo = gson.fromJson(error, ErrorHandel.class);
                        callback.onLoginFailure(lPojo.getMessage());
                    }catch (Exception e){
                        e.printStackTrace();
                        callback.onLoginFailure(e.getMessage());
                    }
                }
            }
        });
    }

}
