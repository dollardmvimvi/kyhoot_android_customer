package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.SignUpInPojo;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

//import ApplicationController;

/**
 * <h>ConfirmNumberModel</h>
 * Created by Ali on 9/13/2017.
 */

public class ConfirmNumberModel {

    private static final String TAG = "ConfirmNumberModel";

    public void verifyOtp(JSONObject jsonObject, final VerifyOtpIntrface verifyOtpIntrface, final String signupOrForgot, final SharedPrefs sprefs)
    {
        Log.d(TAG, "verifyOtp: "+VariableConstant.VERIFYPHONE);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.VERIFYPHONE, OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result) {
                //  ValidatorPojo otpVerfication;
                Log.d("TAGIS", "onSuccessSiguUp: " + result + " headerResponse " + headerResponse);
                SignUpInPojo sPojo;
                Gson gson = new Gson();
                //  otpVerfication = gson.fromJson(result, ValidatorPojo.class);
                sPojo = gson.fromJson(result, SignUpInPojo.class);
                if (signupOrForgot.equals("SIGNUP")) {
                   // callGetProvider(sprefs,sPojo.getData().getToken());
                    sprefs.setEMail(sPojo.getData().getEmail());
                    sprefs.setImageUrl(sPojo.getData().getProfilePic());
                    sprefs.setIsLogin(true);
                    sprefs.setCustomerSid(sPojo.getData().getSid());
                    sprefs.setSession(sPojo.getData().getToken());
                    sprefs.setRequesterId(sPojo.getData().getRequester_id());
                    sprefs.setCurrencySymbl(sPojo.getData().getCurrencyCode());
                    VariableConstant.CurrencySymbol=sPojo.getData().getCurrencyCode();
                    sprefs.setFirstname(sPojo.getData().getFirstName());
                    sprefs.setLastname(sPojo.getData().getLastName());
                    sprefs.setPassword(sPojo.getData().getPassword());
                    sprefs.setMobileNo(sPojo.getData().getPhone());
                    sprefs.setCountryCode(sPojo.getData().getCountryCode());
                    sprefs.setPaymentId(sPojo.getData().getPublishableKey());
                    sprefs.setReferral(sPojo.getData().getReferralcode());
                    sprefs.setFcmTopic(sPojo.getData().getFcmTopic());

                   String signUpType;
                    if(sPojo.getData().getLoginType()==1)
                        signUpType = "Email";
                    else
                        signUpType = "Facebook";

                    JSONObject props = new JSONObject();
                    try {
                        props.put("Name", sPojo.getData().getFirstName() + " " + sPojo.getData().getLastName());
                        props.put("Email", sPojo.getData().getEmail());
                        props.put("UserId", sPojo.getData().getSid());
                        props.put("date", Utilities.dateintwtfour());
                        props.put("SignUpMethod", signUpType);
                        props.put("Phone", sPojo.getData().getCountryCode()+""+sPojo.getData().getPhone());
                        props.put("DOB",sPojo.getData().getDatOB());
                        if(sPojo.getData().getCardDetail()!=null && sPojo.getData().getCardDetail().getLast4()!=null)
                            props.put("CreditCard","Complete");
                        else
                            props.put("CreditCard","Not complete");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    verifyOtpIntrface.onOtpVerify(sPojo.getErrMsg());
                    //ApplicationController.getInstance().createMQttConnection(sprefs.getMobileNo());
                }
                    else
                    verifyOtpIntrface.onSignUpSuccess(sPojo.getErrMsg());

            }

            @Override
            public void onError(String headerResponce, String error)
            {
                Gson gon = new Gson();
                try{
                    ErrorHandel errorHandel = gon.fromJson(error, ErrorHandel.class);
                    verifyOtpIntrface.onOtpError(errorHandel.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

    public void resendOtp(final VerifyOtpIntrface verifyoTp, String sid, String signupOrForgot)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", sid);
            jsonObject.put("userType", 1);
            if (signupOrForgot.equals("PASSWORD"))
            {

                jsonObject.put("trigger", 2);
            } else {
                jsonObject.put("trigger", 1);
                //   resendOTPUrl = VariableConstant.SERVICE_URL+"slave/resendOtp";
            }

            Log.d("resendOtp", "resendOtp: " + jsonObject);


            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "resendOtp", OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result)
                {
                    Log.d("resendOtp", "onSuccess: " + result + " headerRespinse " + headerResponse);
                    ValidatorPojo vpojo = new Gson().fromJson(result, ValidatorPojo.class);
                    verifyoTp.onSignUpSuccess(vpojo.getErrMsg());
                }

                @Override
                public void onError(String headerError, String error) {

                    Gson gon = new Gson();
                    ErrorHandel errorHandel = gon.fromJson(error, ErrorHandel.class);
                    verifyoTp.onOtpError(errorHandel.getMessage());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void verifyiOtpForPass(JSONObject jsonObject, final VerifyOtpIntrface verifyOtpIntrface) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "verifyVerificationCode", OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result) {
                Log.d("FORGOTRESENDOTP", "onSuccess: " + result + " headerReponse " + headerResponse);
                ValidatorPojo vpojo = new Gson().fromJson(result, ValidatorPojo.class);
                verifyOtpIntrface.onOtpVerify(vpojo.getErrMsg());
            }

            @Override
            public void onError(String headerError, String error) {
                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                verifyOtpIntrface.onOtpError(errorHandel.getMessage());
            }
        });
    }
}
