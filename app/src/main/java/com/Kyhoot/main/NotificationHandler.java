package com.Kyhoot.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Kyhoot.R;
import com.Kyhoot.utilities.NotificationUtils;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;


/**
 * Created by embed on 22/8/16.
 *
 */
public class NotificationHandler extends AppCompatActivity
{
    String message,latlong,dialogtitle;
    String spltlatlonf[];
    String TAG = "NotificationHandler";
    Intent intent;
    ProgressDialog pDialog;
    SharedPrefs manager;
    private int action;
    private long bid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         manager = new SharedPrefs(this);
         pDialog = new ProgressDialog(this);
        if(getIntent().getExtras()!=null)
        {
            message = getIntent().getStringExtra("message");
            action = getIntent().getIntExtra("statcode",0);
            if(action==23)
            {

                dialogtitle = "Customer ID: "+manager.getCustomerSid();
            }
            else {
                bid = getIntent().getLongExtra("bid",0);
                dialogtitle = "Booking ID: "+bid;
            }

            if(action == 11 || action == 12 ||action == 10 || action == 5 || action == 4 || action == 23)
            {
                NotificationUtils.clearNotifications(NotificationHandler.this);
            }
            Log.i(TAG,"Handlermess "+message+" bid "+bid+" action "+action);

        }
        if(action==23)
        {
            new AlertDialog.Builder(this)
                    .setTitle(dialogtitle)
                    .setMessage(""+message)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Utilities.setMAnagerWithBID(NotificationHandler.this,manager,"");
                            dialog.dismiss();
                        }
                    })

                    .setIcon(R.drawable.ic_launcher)
                    .show();
        }
        else
        {
         new AlertDialog.Builder(this)
                .setTitle(dialogtitle)
                .setMessage(""+message)
                 .setCancelable(false)
                 .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(action == 11 || action == 12 ||action == 10 || action == 5 || action == 4) {
                         //   intent = new Intent(NotificationHandler.this,MenuActivity.class);
                          //  Utility.deleteEventFromCalender(bid,manager,NotificationHandler.this);

                            finish();
                            dialog.dismiss();
                        }
                        else {
                            NotificationUtils.clearNotifications(NotificationHandler.this);
                            intent = new Intent(NotificationHandler.this,LiveStatus.class);
                            intent.putExtra("BID", bid);
                            intent.putExtra("STATUS", action);
                            intent.putExtra("ImageUrl","");
                            //new task | clear task
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            finish();
                            dialog.dismiss();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_launcher)
            .show();
        }

    }

}
