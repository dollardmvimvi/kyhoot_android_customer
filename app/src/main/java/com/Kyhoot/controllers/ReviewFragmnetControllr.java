package com.Kyhoot.controllers;

import android.util.Log;

import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.models.ReviewModel;
import com.Kyhoot.utilities.AlertProgress;

/**
 * <h>ReviewFragmnetControllr</h>
 * Created by Ali on 9/19/2017.
 */

public class ReviewFragmnetControllr implements NetworkCheck {


    private UtilInterFace uInterFace;
    private ReviewModel rModel;
    private String session;
    private int page;

    public ReviewFragmnetControllr(UtilInterFace uInterFace) {
        this.uInterFace = uInterFace;
        rModel = new ReviewModel();
    }

    public void callReviewApi(int page, String session, AlertProgress alertProgress) {
        this.session = session;
        this.page = page;
        rModel.checkNetwork(alertProgress, this);
    }

    @Override
    public void onNetworkAvailble() {
        uInterFace.showProgress();
        Log.d("TAGPAGE", "onNetworkAvailble: "+page);
        rModel.getReviewService(page, uInterFace, session);
    }

    @Override
    public void onNetworkNotAvailble() {

    }
}
