package com.Kyhoot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.PredefinedData;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * <h>LangExpertiseAdapter</h>
 * Created by Ali on 2/6/2018.
 */

public class LangExpertiseAdapter extends RecyclerView.Adapter {
    private final boolean isIconAvailable;
    private final ArrayList<PredefinedData> preDefined;
    private Context mContext;
    private String[] langExpertise;
    private String TAG = LangExpertiseAdapter.class.getSimpleName();

    public LangExpertiseAdapter(Context mContext, String[] langExpertise, boolean isIconAvailable, ArrayList<PredefinedData> preDefined) {
        this.mContext = mContext;
        this.langExpertise = langExpertise;
        this.isIconAvailable = isIconAvailable;
        this.preDefined = preDefined;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(mContext).inflate(R.layout.single_cell_lang_expertise,parent,false);
        return new ViewHolders(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolders holders = (ViewHolders) holder;
        if(isIconAvailable)
        {
            holders.tvBulletIcon.setVisibility(View.GONE);
            holders.imageIconMeta.setVisibility(View.VISIBLE);
            if(!"".equals(preDefined.get(position).getIcon()))
            {
                Glide.with(mContext)
                        .load(preDefined.get(position).getIcon())
                        .into(holders.imageIconMeta);


            }
            holders.tvLangExpertise.setText(preDefined.get(position).getName());
        }else
        {
            holders.imageIconMeta.setVisibility(View.GONE);
            holders.tvBulletIcon.setVisibility(View.VISIBLE);
            holders.tvLangExpertise.setText(langExpertise[position]);
        }

    }

    @Override
    public int getItemCount()
    {
        if(isIconAvailable)
            return preDefined.size();
        else
            return langExpertise.length;
    }

    class ViewHolders extends RecyclerView.ViewHolder
    {

        TextView tvLangExpertise;
        TextView tvBulletIcon;
        ImageView imageIconMeta;
        private AppTypeface appTypeface;

        public ViewHolders(View itemView) {
            super(itemView);
            tvLangExpertise=itemView.findViewById(R.id.tvLangExpertise);
            tvBulletIcon=itemView.findViewById(R.id.tvBulletIcon);
            imageIconMeta=itemView.findViewById(R.id.imageIconMeta);
            appTypeface = AppTypeface.getInstance(mContext);
            tvLangExpertise.setTypeface(appTypeface.getHind_regular());


        }
    }
}
