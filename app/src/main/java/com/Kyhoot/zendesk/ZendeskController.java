package com.Kyhoot.zendesk;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>ZendeskController</h>
 * Created by Ali on 12/29/2017.
 */

public class ZendeskController
{
    private Context mContext;
    private SharedPrefs prefs;
    private ZendeskInterFace zendexInterFace;
    private int zenId;

    public ZendeskController(Context mContext, SharedPrefs prefs, ZendeskInterFace zendexInterFace) {
        this.mContext = mContext;
        this.prefs = prefs;
        this.zendexInterFace = zendexInterFace;
    }


    public void callApiToCreateTicket(String trim,String subject,String priority)
    {
       final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subject",subject);
            jsonObject.put("body",trim);
            jsonObject.put("status","open");
            jsonObject.put("priority",priority);
            jsonObject.put("type","problem");
            jsonObject.put("requester_id",prefs.getRequesterId());
            Log.d("TAG", "callApiToCreateTicket: "+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "zendesk/ticket",
                OkHttpConnection.Request_type.POST, jsonObject, prefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        Log.d("TAG", "RESULTonSuccess: "+result);
                        zendexInterFace.onZendexTicketAdded(result);
                    }

                    @Override
                    public void onError(String headerError, String error)
                    {
                        if (headerError.equals("502"))
                        {
                            zendexInterFace.onError("");
                        }
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        zendexInterFace.onSessionExpired(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData(), jsonObject,"create");
                                        break;
                                    default:
                                        zendexInterFace.onError(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    public void callApiToCommentOnTicket(String trim, int zenId)
    {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",zenId+"");
            jsonObject.put("body",trim);
            jsonObject.put("author_id",prefs.getRequesterId());

            Log.d("TAG", "callApiToCommentOnTicket: "+jsonObject);
            //http://api2.0.iserve.ind.in/zendesk/ticket/comments
            OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "zendesk/ticket/comments",
                    OkHttpConnection.Request_type.PUT, jsonObject, prefs.getSession(), VariableConstant.SelLang,
                    new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerData, String result) {
                            Log.d("TAG", "callApiToCommentOnSuccess: "+result);

                        }

                        @Override
                        public void onError(String headerError, String error)
                        {
                            if (headerError.equals("502"))
                            {
                                zendexInterFace.onError("");
                            }
                            else {
                                try{
                                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                    switch (headerError) {
                                        case "498":
                                            zendexInterFace.onSessionExpired(errorHandel.getMessage());
                                            break;
                                        case "440":
                                            getAccessToken(errorHandel.getData(),jsonObject,"comment");
                                            break;
                                        default:
                                            zendexInterFace.onError(errorHandel.getMessage());
                                            break;
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        prefs.getRequesterId();
    }

    public void callApiToGetTicketInfo(int zenId)
    {
        this.zenId = zenId;

        final JSONObject jsonObject = new JSONObject();
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "zendesk/ticket/history/"+zenId,
                OkHttpConnection.Request_type.GET, jsonObject, prefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        zendexInterFace.onSuccess(result);
                    }

                    @Override
                    public void onError(String headerError, String error)
                    {

                        if (headerError.equals("502"))
                        {
                            zendexInterFace.onError("");
                        }
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        zendexInterFace.onSessionExpired(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData(), jsonObject,"ticket");
                                        break;
                                    default:
                                        zendexInterFace.onError(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    private void getAccessToken(String data, final JSONObject jsonObject, final String type)
    {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            prefs.setSession(jsonobj.getString("data"));
                            // inteface.excessToken
                            switch (type)
                            {
                                case "create":
                                    callApiToCreateTicket(jsonObject.getString("body"),jsonObject.getString("subject"),
                                            jsonObject.getString("priority"));
                                    break;
                                case "comment":
                                    callApiToCommentOnTicket(jsonObject.getString("body"),Integer.parseInt(jsonObject.getString("id")));
                                    break;
                                    default:
                                        callApiToGetTicketInfo(zenId);
                                        break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            zendexInterFace.onError("");
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        zendexInterFace.onSessionExpired(errorHandel.getMessage());
                                        break;
                                    default:
                                        zendexInterFace.onError(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }
}
