package com.Kyhoot.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.CategoriesAndServicesInterface;
import com.Kyhoot.pojoResponce.ServicesData;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 24/1/18.
 */

public class CategoryAndServiceAdapter extends RecyclerView.Adapter<CategoryAndServiceAdapter.CategoryHolder> {
    private final Context mContext;
    private ArrayList<ServicesData> categoryLists=new ArrayList<>();
    private CategoriesAndServicesInterface callback;

    public CategoryAndServiceAdapter(Context mContext, ArrayList<ServicesData> categoryLists, CategoriesAndServicesInterface callback) {
        this.categoryLists = categoryLists;
        this.mContext=mContext;
        this.callback=callback;
    }
    public void setCategoryLists(ArrayList<ServicesData> categoryLists){
        this.categoryLists=categoryLists;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.category_header,parent,false);
        return new CategoryHolder(view) ;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
        if(categoryLists.get(position).getSub_cat_id().isEmpty()){
            holder.rl_heading_subCat.setVisibility(View.GONE);
        }
        holder.rvServices.setLayoutManager(new LinearLayoutManager(mContext));
        holder.categoryHeader.setText(categoryLists.get(position).getSub_cat_name());
        holder.categoryDesc.setText(categoryLists.get(position).getSub_cat_desc());
        holder.rvServices.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        holder.rvServices.setAdapter(new ServicesAdapter(mContext, categoryLists.get(position).getService(),callback));
    }

    @Override
    public int getItemCount() {
        if(categoryLists==null){
            return 0;
        }else{
            return categoryLists.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class CategoryHolder extends RecyclerView.ViewHolder{
        RecyclerView rvServices;
        TextView categoryHeader,categoryDesc;
        RelativeLayout rl_heading_subCat;
        CategoryHolder(View itemView) {
            super(itemView);
            rvServices=itemView.findViewById(R.id.rv_services);
            categoryHeader=itemView.findViewById(R.id.categoryHeader);
            categoryDesc=itemView.findViewById(R.id.categoryDesc);
            rl_heading_subCat=itemView.findViewById(R.id.rl_heading_subCat);
            categoryHeader.setTypeface(AppTypeface.getInstance(mContext).getHind_semiBold());
            categoryDesc.setTypeface(AppTypeface.getInstance(mContext).getHind_regular());
        }
    }
}
