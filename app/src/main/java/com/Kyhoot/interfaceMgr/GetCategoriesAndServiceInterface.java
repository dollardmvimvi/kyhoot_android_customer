package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 3/2/18.
 */

public interface GetCategoriesAndServiceInterface {
    public void onSuccessCategoriesAvailable(String responseCode,String result);
    public void onFailureCategoriesAvailable(String responseCode,String error);
}
