package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.ScheduleMonthPojo;

public interface scheduleInterFace
{
    void showProgress();

    void hideProgress();

    void onSuccessGetSchedule(String result);

    void onFailure(String failureMsg);

    void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo);

    void onError(String error);

    void onSessionError(String error);
}
