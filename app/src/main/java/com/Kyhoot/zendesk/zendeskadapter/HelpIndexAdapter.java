package com.Kyhoot.zendesk.zendeskadapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.zendesk.zendesk.HelpIndexTicketDetails;
import com.Kyhoot.zendesk.zendeskpojo.OpenClose;

import java.util.ArrayList;
import java.util.Date;

/**
 * <h>HelpIndexAdapter</h>
 * Created by Ali on 12/30/2017.
 */

public class HelpIndexAdapter extends RecyclerView.Adapter
{
    private Context mContext;
    private ArrayList<OpenClose>openCloses;

    public HelpIndexAdapter(Context mContext, ArrayList<OpenClose> openCloses) {
        this.mContext = mContext;
        this.openCloses = openCloses;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.help_index_adapter,parent,false);
        return new ViewHolders(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolders hldr  = (ViewHolders) holder;

        if(openCloses.get(position).isFirst())
        {
            hldr.tvOpenCloseStatus.setVisibility(View.VISIBLE);
            String status = mContext.getString(R.string.status)+" : "+openCloses.get(position).getStatus();
            hldr.tvOpenCloseStatus.setText(status);
        }
        else {
            hldr.tvOpenCloseStatus.setVisibility(View.GONE);
        }
        hldr.tvHelpSubject.setText(openCloses.get(position).getSubject());
        Date date = new Date(openCloses.get(position).getTimeStamp() * 1000L);

        String formattedDate[] = Utilities.getFormattedDate(date).split(",");
        hldr.tvHelpDate.setText(formattedDate[0]);
        hldr.tvHelpTime.setText(formattedDate[1]);
        char c = openCloses.get(position).getSubject().charAt(0);
        hldr.tvHelpText.setText(c+"");

    }

    @Override
    public int getItemCount() {
        return openCloses.size();
    }

    private class ViewHolders extends RecyclerView.ViewHolder
    {
        private TextView tvOpenCloseStatus,tvHelpSubject,tvHelpTime,tvHelpDate,tvHelpText;
        private AppTypeface appTypeface;
        ViewHolders(View itemView) {
            super(itemView);
            appTypeface = AppTypeface.getInstance(mContext);
            tvOpenCloseStatus = itemView.findViewById(R.id.tvOpenCloseStatus);
            tvHelpSubject = itemView.findViewById(R.id.tvHelpSubject);
            tvHelpTime = itemView.findViewById(R.id.tvHelpTime);
            tvHelpDate = itemView.findViewById(R.id.tvHelpDate);
            tvHelpText = itemView.findViewById(R.id.tvHelpText);
            tvOpenCloseStatus.setTypeface(appTypeface.getHind_semiBold());
            tvHelpSubject.setTypeface(appTypeface.getHind_bold());
            tvHelpTime.setTypeface(appTypeface.getHind_regular());
            tvHelpDate.setTypeface(appTypeface.getHind_regular());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mContext,HelpIndexTicketDetails.class);
                    intent.putExtra("ISTOAddTICKET",false);
                    intent.putExtra("ZendeskId",openCloses.get(getAdapterPosition()).getId());
                    mContext.startActivity(intent);
                    Activity activity= (Activity) mContext;
                    activity.overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
                }
            });

        }
    }

}
