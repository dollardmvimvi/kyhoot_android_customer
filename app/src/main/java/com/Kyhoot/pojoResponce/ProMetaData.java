package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 14/3/18.
 */

public class ProMetaData {
    private String Description;

    private String _id;

    private int fieldType;

    private String data;

    private String isManadatory;

    private String fieldName;

    public ArrayList<PredefinedData> getPreDefined() {
        return preDefined;
    }

    private ArrayList<PredefinedData> preDefined;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public int getFieldType ()
    {
        return fieldType;
    }

    public void setFieldType (int fieldType)
    {
        this.fieldType = fieldType;
    }

    public String getData ()
    {
        return data;
    }

    public void setData (String data)
    {
        this.data = data;
    }

    public String getIsManadatory ()
    {
        return isManadatory;
    }

    public void setIsManadatory (String isManadatory)
    {
        this.isManadatory = isManadatory;
    }

    public String getFieldName ()
    {
        return fieldName;
    }

    public void setFieldName (String fieldName)
    {
        this.fieldName = fieldName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", _id = "+_id+", fieldType = "+fieldType+", data = "+data+", isManadatory = "+isManadatory+", fieldName = "+fieldName+"]";
    }
}
