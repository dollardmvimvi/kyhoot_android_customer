package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * <h>Shop</h>
 * Created by Ali on 10/3/2017.
 */

public class Shop {
    private static final String STORAGE = "shop";

    public static Shop get() {
        return new Shop();
    }

    public ArrayList<SongListBean> getData() {
        ArrayList<SongListBean> aList = new ArrayList<>();
        aList.add(new SongListBean("JGwWNGJdvx8", "Ed Sheeran-Shape of you"));
        aList.add(new SongListBean("-j0dlcfekqw", "Otilia"));
        aList.add(new SongListBean("kJQP7kiw5Fk", "Despacito"));
        aList.add(new SongListBean("nYh-n7EOtMA", "Sia-Cheap Thrill"));
        aList.add(new SongListBean("papuvlVeZg8", "Clean Bandit - Rockabye "));
        aList.add(new SongListBean("PT2_F-1esPk", "The Chainsmokers - Closer"));
        aList.add(new SongListBean("lY2yjAdbvdQ", "Shawn Mendes - Treat You Better"));
        aList.add(new SongListBean("fRh_vgS2dFE", "Justin Bieber - Sorry"));
        aList.add(new SongListBean("e-ORhEE9VVg", "Taylor Swift - Blank Space"));
        return aList;
    }

}
