package com.Kyhoot.interfaceMgr;

/**
 * <h>EmailPhoneValidtn</h>
 * Created by Ali on 9/13/2017.
 */

public interface EmailPhoneValidtn
{
    void onEmailValid(boolean isValid, String msg, boolean isSignUp);
    void onPhoneValid(boolean isValid, String msg, boolean isSignUp);

    void onReferralCodeValid(boolean b, String s);
}
