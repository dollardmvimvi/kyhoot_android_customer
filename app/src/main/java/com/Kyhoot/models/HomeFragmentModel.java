package com.Kyhoot.models;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.ETAResultInterface;
import com.Kyhoot.interfaceMgr.HomeModelIntrface;
import com.Kyhoot.main.GoogleRoute;
import com.Kyhoot.main.HomeFragment;
import com.Kyhoot.pojoResponce.CategoriesPojo;
import com.Kyhoot.pojoResponce.ConfigPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.MarkerPojo;
import com.Kyhoot.pojoResponce.MqttProviderData;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.pojoResponce.MqttProvidrResp;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * <h>HomeFragmentModel</h>
 * Created by Ali on 9/29/2017.
 */

@TargetApi(Build.VERSION_CODES.KITKAT)
public class HomeFragmentModel {
    public int i = 1;
    private HomeModelIntrface inteface;
    private String modelResponce = "";
    private ArrayList<MqttProviderDetails> mqttrespArry = new ArrayList<>();
    private int providerInitialSize = 0;
    //private int infltdCnt = 0;
    private ArrayList<String> proIdsList = new ArrayList<>();
    private ArrayList<Double> proDistance = new ArrayList<>();
    private SharedPrefs sprefs;
    private int inflatedMusic = 0;
    private ArrayMap<String, MarkerPojo> markerHshMap = new ArrayMap<>();
    private String TAG = "HomeFragmentModel";
    private boolean isAvailableMusic;
    private Context mcontext;
    private AlertDialog dialog;
    private Gson gson;
    public static ArrayList<String> categoryIds=new ArrayList<>();
    public static LinkedHashMap<String,LatLng> idLatLngSet=new LinkedHashMap<>();
    private String destinationLatLog="";
    int selectedCategoryPosition = -1;
    private String previousDestLatLng="";
    String distanceMatrixurls="";
    private double previousLat,previousLong;
    private LinkedHashMap<String, LatLng> previousIdLatLngSet;

    public void putDriverMarkerRow(String driverId, MarkerPojo markerPojo) {
        markerHshMap.put(driverId, markerPojo);
        Log.d(TAG, "putDriverMarkerRow:  size " + markerHshMap.size());
    }
    //This called when we get message from MQTT on Providers update
    public void responceHomeModel(String jsonObject) {
        //Log.d(TAG, "responceHomeModel: "+jsonObject);
        if(VariableConstant.CALLMQTTDATA==1) {
            responceGeneralMethod(jsonObject);
        }else {
            providerInitialSize = 0;
        }
    }
    //Called from home fragment while service call
    public void responceHomeFragModel(String jsonObject, HomeModelIntrface inteface) {
        this.inteface = inteface;
        providerInitialSize = 0;
        responceGeneralMethod(jsonObject);
        MqttProvidrResp mqttresp = new Gson().fromJson(jsonObject, MqttProvidrResp.class);
        infalteData(mqttresp, jsonObject);
    }


    private void responceGeneralMethod(String jsonObject) {
        try
        {
            //Log.d("MAINACTIVITY", "responceHomeModel: " + jsonObject);
            VariableConstant.MQTTRESPONSEPROVIDER=jsonObject;
            inteface.onHideProgressBar();
            MqttProvidrResp mqttresp = new Gson().fromJson(jsonObject, MqttProvidrResp.class);
            if(mqttresp.getErrFlag()==0 && mqttresp.getErrNum()==200)
            {

                inteface.onUpdateProviders(jsonObject);
                if (!modelResponce.equalsIgnoreCase(jsonObject.trim())) {
                    Log.d("HOMEFRAGMO", "CHAnged: ");
                    isAvailableMusic = true;
                    infalteData(mqttresp, jsonObject);
                } else {
                    if (VariableConstant.infltdCnt!=2)
                    {
                        infalteData(mqttresp, jsonObject);
                        VariableConstant.infltdCnt++;
                        providerInitialSize = 0;

                    }
                    if(VariableConstant.isCurrentLocation)
                    {
                        VariableConstant.isCurrentLocation = false;
                        inteface.onHideProgressBar();
                    }
                    Log.d("HOMEFRAGMO", "CHAngedNot: ");
                }
            }
            else if(mqttresp.getErrFlag()==1 && mqttresp.getErrNum()==404)
            {
                inteface.onError(mqttresp.getErrMsg());
                inteface.onClearList();
                inteface.onClearMapLocation();
                providerInitialSize = 0;
                modelResponce = "";
            }
        }catch (Exception e)
        {
            inteface.onError("Unable to resolve host");
            e.printStackTrace();
        }


    }

    private void infalteData(MqttProvidrResp mqttresp, String jsonObject) {
        mqttrespArry.clear();
        modelResponce = jsonObject;
        Log.d(TAG, "infalteData: "+jsonObject);
        if (mqttresp!=null && mqttresp.getData()!=null && mqttresp.getData().size() > 0) {
            //idLatLngSet is updated in this method
            int selectedCategoryPosition=getCategoryPosition(mqttresp);
            // Call for eta updates

            if(idLatLngSet.size()>0){
                inteface.getEtaUpdate();
            }else{
                inteface.setNoProviders();
            }
            //Surge Price check
            if (selectedCategoryPosition!=-1&&mqttresp.getData().get(selectedCategoryPosition).getCategoryData()!=null) {
                VariableConstant.SURGE_PRICE=mqttresp.getData().get(selectedCategoryPosition).getCategoryData().getSurgePrice();
                Log.e(TAG, "infalteData:  SURGE PRICE " +selectedCategoryPosition+" "+VariableConstant.SURGE_PRICE);
            }
            if(selectedCategoryPosition!=-1 && mqttresp.getData().get(selectedCategoryPosition).getProviderList()!=null && mqttresp.getData().get(selectedCategoryPosition).getProviderList().size()>0)
                inteface.onAvailableMusic(mqttresp.getData().get(selectedCategoryPosition).getProviderList());
            if (providerInitialSize == 0) {
                inteface.onClearMapLocation();
                plotProMarker(mqttresp);
            } else if (selectedCategoryPosition!=-1 && providerInitialSize != mqttresp.getData().get(selectedCategoryPosition).getProviderList().size()) {
                inteface.onClearMapLocation();
                isAvailableMusic = true;
                plotProMarker(mqttresp);
            } else {
                ArrayList<String> previousListOfDrivers = new ArrayList<>(markerHshMap.keySet());
                for (int proIdsListPos = 0; proIdsListPos < providerInitialSize; proIdsListPos++) {
                    MqttProviderDetails providerDetails=mqttresp.getData().get(selectedCategoryPosition).getProviderList().get(proIdsListPos);
                    if (previousListOfDrivers.contains(providerDetails.getId())) {
                        //already plotted driver
                        LatLng latLng = new LatLng(providerDetails.getLocation().getLatitude(),
                                providerDetails.getLocation().getLongitude());
                        //Updating Marker position
                        inteface.onUpdateMakerPosition(latLng, markerHshMap.get(providerDetails.getId()).getMarkerId());
                        if(proDistance.size()>0)
                        {
                            if (providerDetails.getDistance()==(proDistance.get(proIdsListPos)))
                            {
                                if (VariableConstant.isAdapterNotified) {
                                    inteface.onAvailableMusic(mqttresp.getData().get(selectedCategoryPosition).getProviderList());
                                    VariableConstant.isAdapterNotified = false;
                                }
                            } else {
                                inteface.onAvailableMusic(mqttresp.getData().get(selectedCategoryPosition).getProviderList());
                            }
                        }
                        if (markerHshMap.get(providerDetails.getId()).getStatus() != providerDetails.getStatus()
                                || !markerHshMap.get(providerDetails.getId()).getImage().equalsIgnoreCase(providerDetails.getImage())) {
                            ArrayList<String> proInfo = new ArrayList<>();
                            proInfo.add(providerDetails.getId());
                            proInfo.add(providerDetails.getYoutubeUrlLink());
                            proInfo.add(providerDetails.getLocation().getLatitude() + "");
                            proInfo.add(providerDetails.getLocation().getLongitude() + "");
                            proInfo.add(providerDetails.getImage());
                            if (providerDetails.getStatus() == 1) {
                                inteface.onUpdateMakerStatusOnline(proInfo, providerDetails.getImage(), latLng,
                                        markerHshMap.get(mqttresp.getData().get(selectedCategoryPosition).getProviderList()
                                                .get(proIdsListPos).getId()).getMarkerId());
                            } else if (providerDetails.getStatus() == 0) {
                                inteface.onUpdateMakerStatusOffline(proInfo, providerDetails.getImage(), latLng,
                                        markerHshMap.get(mqttresp.getData().get(selectedCategoryPosition).getProviderList()
                                                .get(proIdsListPos).getId()).getMarkerId());
                            }
                            inteface.onAvailableMusic(mqttresp.getData().get(selectedCategoryPosition).getProviderList());
                        }
                    }
                }
            }
            //================Setting wallet==============================
            if(mqttresp.getWallet()!=null ){
                VariableConstant.walletCurrency=mqttresp.getWallet().getCurrencySymbol();
                VariableConstant.walletBalance=mqttresp.getWallet().getWalletAmount();
                VariableConstant.ENABLEWALLET=mqttresp.getWallet().getEnableWallet();
                if(inteface!=null){
                    inteface.updateWallet(mqttresp.getWallet());
                }
            }
            //============================================================

        } else {
            inteface.onError("No provider available for this date,time and location, please change date,time or location");
            inteface.onClearList();
            inteface.onClearMapLocation();
            providerInitialSize = 0;
        }

        if(VariableConstant.isCurrentLocation)
        {
            VariableConstant.isCurrentLocation = false;
            inteface.onHideProgressBar();
        }
    }

    public void setDestinationLatLong(double srcLat, double srcLng, ETAResultInterface etaResultInterface) {

        String tempDestLatLng="";

        for(int i=0;i<categoryIds.size();i++){
            if(idLatLngSet.containsKey(categoryIds.get(i))){
                LatLng latLng=idLatLngSet.get(categoryIds.get(i));
                tempDestLatLng=tempDestLatLng+ Utilities.doubleformateForLatLng(latLng.latitude+"")+","+Utilities.doubleformateForLatLng(latLng.longitude+"")+"|";
            }
        }

        destinationLatLog=tempDestLatLng.substring(0,tempDestLatLng.length());
        if(((!destinationLatLog.equals("") && !previousDestLatLng.equals(destinationLatLog)) || srcLat!=previousLat||srcLng!=previousLong) || HomeFragment.etaCleared ){
            if(srcLat!=0 && srcLng!=0){
                previousIdLatLngSet=idLatLngSet;
                previousLat=srcLat;
                previousLong=srcLng;
                previousDestLatLng=destinationLatLog;
                distanceMatrixurls = GoogleRoute.distanceMatrixMultipleDestURL(srcLat, srcLng, destinationLatLog);
                HomeFragment.etaCleared=false;
                new GoogleRoute.GetDistanceMatrixEta(distanceMatrixurls,etaResultInterface).execute();
            }
        }
    }

    private int getCategoryPosition(MqttProvidrResp mqttresp) {
        categoryIds.clear();
        idLatLngSet.clear();
        if(mqttresp!=null){
            MqttProviderData tempMqttProviderData;
            for(int i=0;i<mqttresp.getData().size();i++){
                tempMqttProviderData = mqttresp.getData().get(i);
                //Getting array of categories
                categoryIds.add(tempMqttProviderData.getCategoryId());
                // destination latlong set
                if(tempMqttProviderData.getProviderList().size()>0){
                    MqttProviderDetails tempProvider = tempMqttProviderData.getProviderList().get(0);
                    idLatLngSet.put(tempMqttProviderData.getCategoryId(),new LatLng(tempProvider.getLocation().getLatitude(),tempProvider.getLocation().getLongitude()));
                }
                if(VariableConstant.selectedCategoryId.equals(tempMqttProviderData.getCategoryId())){
                    selectedCategoryPosition= i;
                }
            }
        }
        Log.d(TAG, "getCategoryPosition: CategoryIds:"+categoryIds);
        Log.d(TAG, "getCategoryPosition: idLatLngs:"+idLatLngSet);
        return selectedCategoryPosition;
    }

    private void plotProMarker(MqttProvidrResp mqttresp) {
        Log.d(TAG, "plotProMarkerCALLED: ");
        try {
            if (providerInitialSize == 0) {
                providerInitialSize = mqttresp.getData().get(selectedCategoryPosition).getProviderList().size();
                if(isAvailableMusic)
                    inteface.onAvailableMusic(mqttresp.getData().get(selectedCategoryPosition).getProviderList());
                isAvailableMusic = false;

            }
            if (providerInitialSize != mqttresp.getData().get(selectedCategoryPosition).getProviderList().size()) {
                providerInitialSize = mqttresp.getData().get(selectedCategoryPosition).getProviderList().size();
                inteface.onAvailableMusic(mqttresp.getData().get(selectedCategoryPosition).getProviderList());
            }
            proIdsList.clear();
            proDistance.clear();

            for (int proSize = 0; proSize < providerInitialSize; proSize++) {
                MqttProviderDetails mDtls = mqttresp.getData().get(selectedCategoryPosition).getProviderList().get(proSize);
                proIdsList.add(mDtls.getId());
                proDistance.add(mDtls.getDistance());
                ArrayList<String> proInfo = new ArrayList<>();
                proInfo.add(mDtls.getId());
                proInfo.add(mDtls.getYoutubeUrlLink());
                proInfo.add(mDtls.getLocation().getLatitude() + "");
                proInfo.add(mDtls.getLocation().getLongitude() + "");
                proInfo.add(mDtls.getImage());
                if (mqttresp.getData().get(selectedCategoryPosition).getProviderList().get(proSize).getStatus() == 0) {
                    inteface.onAvailableOffLineProvider(proInfo, mDtls.getImage(), new LatLng(mDtls.getLocation().getLatitude(),
                            mDtls.getLocation().getLongitude()));
                } else {
                    inteface.onAvailableOnlineProvider(proInfo, mDtls.getImage(), new LatLng(mDtls.getLocation().getLatitude(),
                            mDtls.getLocation().getLongitude()));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void callService(double currentLati, double currentLongi, final SharedPrefs sprefs, final HomeModelIntrface inteface) {
        this.inteface = inteface;
        this.sprefs = sprefs;
        JSONObject json = new JSONObject();
        try {
            json.put("lat", currentLati);
            json.put("long", currentLongi);
            //Passing Category id to null so we can get all the providers (GOTASKER requirement)
            json.put("categoryId","");//sprefs.getCategoryId()
            json.put("ipAddress",VariableConstant.setIPAddress );
            json.put("bookingType", VariableConstant.BOOKINGTYPE);
            json.put("scheduleDate", VariableConstant.SCHEDULEDDATE);
            json.put("scheduleTime", VariableConstant.SCHEDULEDTIME);
            json.put("deviceTime", Utilities.dateintwtfour());
            if(sprefs.getCategoryId().trim().isEmpty()){
                Log.e(TAG, "callServiceHOME: "+json);
            }else{
                Log.e(TAG, "callServiceHOME: "+json);
            }

            String sesnTkn = sprefs.getSession();
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "location", OkHttpConnection.Request_type.POST, json, sesnTkn, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result) {
                    if(VariableConstant.BOOKINGTYPE==2) {
                        modelResponce = "";
                        providerInitialSize = 0;
                    }
                    Log.d(TAG, "onSuccessHOMEFRAGLOC: "+result);
                    Log.d(TAG, "onSuccessHOMEFRAGISCONNECTED: "+ApplicationController.getInstance().isMqttConnected());
                    if(ApplicationController.getInstance().isMqttConnected()) {
                        try {
                            if(!sprefs.getCustomerSid().equals(""))
                                ApplicationController.getInstance().subscribeToTopic(sprefs.getCustomerSid(),1);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            if(!sprefs.getCustomerSid().equals(""))
                                ApplicationController.getInstance().createMQttConnection(sprefs.getCustomerSid());
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onError(String headerError, String error) {
                    ErrorHandel errorHandel;
                    if (headerError.equals("502"))
                        inteface.onError("");
                    else {
                        try {
                            errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    inteface.onSessionError(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccessToken(errorHandel.getData());
                                    break;
                                default:
                                    inteface.onError(errorHandel.getMessage());
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void callServicesService( final HomeModelIntrface inteface) {
        this.inteface = inteface;
        JSONObject json = new JSONObject();
        try {
            json.put("catId",sprefs.getCategoryId() );
            Log.d(TAG, "callServicesService: "+VariableConstant.SERVICE_URL + "services"+"/"+sprefs.getCategoryId()+"/0"+"  Json: "+json);
            String sesnTkn = sprefs.getSession();
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "services"+"/"+sprefs.getCategoryId()+"/0",
                    OkHttpConnection.Request_type.GET, json, sesnTkn, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerResponse, String result) {
                            inteface.onServicesUpdated(result);
                            Log.d(TAG, "onSuccess servicesService: "+result);
                            Log.d(TAG, "onSuccessHOMEFRAGISCONNECTED: "+ApplicationController.getInstance().isMqttConnected());
                        }

                        @Override
                        public void onError(String headerError, String error) {
                            try{
                                if (headerError.equals("502"))
                                    inteface.onError("");
                                else {
                                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                    switch (headerError) {
                                        case "498":
                                            inteface.onSessionError(errorHandel.getMessage());
                                            break;
                                        case "440":
                                            getAccessToken(errorHandel.getData());
                                            break;
                                        default:
                                            inteface.onError(errorHandel.getMessage());
                                            break;
                                    }
                                }

                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCategories(Context context,final double latiLongi[], final SharedPrefs sprefss) {
        this.sprefs=sprefss;
        this.mcontext=context;
        Log.d(TAG, "callGetCategoriesSess: " + VariableConstant.SERVICE_URL + "categories/" + latiLongi[0] + "/" + latiLongi[1]+"/"+VariableConstant.setIPAddress);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "categories/" + latiLongi[0] + "/" + latiLongi[1]+"/"+VariableConstant.setIPAddress,
                OkHttpConnection.Request_type.GET, new JSONObject(), sprefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: getCategories "+result);
                        sprefs.setCategoriesList(result);
                        if(result!=null && !result.equals("")){
                            gson=new Gson();
                            CategoriesPojo categoriesPojo=null;
                            try {
                                categoriesPojo = gson.fromJson(result, CategoriesPojo.class);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            long l = Utilities.setServerTimeAndDifference(categoriesPojo.getData().getServerGmtTime());
                            sprefs.setServerTimeDifference(l);
                            VariableConstant.SERVERTIMEDIFFERNCE=l;
                            Log.e(TAG, "onSuccess: Payment Methods" +
                                    " CASH  "+categoriesPojo.getData().getCityData().getPaymentMode().getCash()
                            +" CARD  "+categoriesPojo.getData().getCityData().getPaymentMode().getCard()
                            +" WALLET  "+categoriesPojo.getData().getCityData().getPaymentMode().getWallet());
                            VariableConstant.CARDBOOKING=categoriesPojo.getData().getCityData().getPaymentMode().getCard();
                            VariableConstant.CASHBOOKING=categoriesPojo.getData().getCityData().getPaymentMode().getCash();
                            VariableConstant.WALLETBOOKING=categoriesPojo.getData().getCityData().getPaymentMode().getWallet();
                            try{
                                sprefs.setCurrencySymbl(categoriesPojo.getData().getCityData().getCurrencySymbol());
                                sprefs.setCurrencyPosition(categoriesPojo.getData().getCityData().getCurrencyAbbrText());
                                VariableConstant.CurrencyPosition=categoriesPojo.getData().getCityData().getCurrencyAbbrText();
                                VariableConstant.CurrencySymbol=categoriesPojo.getData().getCityData().getCurrencySymbol();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            if(categoriesPojo.getData().getCatArr().size()>0){
                                for(int i=0;i<categoriesPojo.getData().getCatArr().size();i++){
                                    if(categoriesPojo.getData().getCatArr().size()>0){
                                        String categoryId=categoriesPojo.getData().getCatArr().get(i).get_id();
                                        inteface.onSuccessGetCategories(categoriesPojo);
                                        VariableConstant.selectedCategoryId=categoryId;
                                        VariableConstant.DISTANCEMATRIXUNIT=categoriesPojo.getData().getCityData().getDistanceMatrix();
                                        VariableConstant.BILLINGMODEL=categoriesPojo.getData().getCatArr().get(0).getBilling_model();
                                        VariableConstant.SERVICEHOURLYPRICE=categoriesPojo.getData().getCatArr().get(0).getPrice_per_fees();
                                        VariableConstant.SELECTEDCATEGORYNAME=categoriesPojo.getData().getCatArr().get(0).getCat_name();
                                        VariableConstant.SERVICE_TYPE=categoriesPojo.getData().getCatArr().get(0).getService_type();
                                        VariableConstant.SELECTEDCATEGORYVISITFEE=categoriesPojo.getData().getCatArr().get(0).getVisit_fees();
                                        VariableConstant.BOOKINGTYPEAVAILABILITY=categoriesPojo.getData().getCatArr().get(0).getBookingTypeAction();
                                        VariableConstant.MINIMUM_HOUR=categoriesPojo.getData().getCatArr().get(0).getMinimum_hour();
                                        //VariableConstant.SURGE_PRICE=categoriesPojo.getData().getCatArr().get(0).getSurgePrice();
                                        sprefs.setCategoryId(VariableConstant.selectedCategoryId);
                                        sprefs.setBillingModel(VariableConstant.BILLINGMODEL);
                                        sprefs.setCategoryHourlyPrice(VariableConstant.SERVICEHOURLYPRICE);
                                        sprefs.setSelectedCategoryName(VariableConstant.SELECTEDCATEGORYNAME);
                                        sprefs.setSelectedServiceType(VariableConstant.SERVICE_TYPE);
                                        return;
                                    }
                                }
                                //Toast.makeText(mcontext,"notary category not found!",Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(mcontext,"No services found",Toast.LENGTH_LONG).show();
                            }

                            VariableConstant.PROFREQUENCYINTERVAL = categoriesPojo.getData().getCityData().getCustomerFrequency().getCustomerHomePageInterval();
                            //sprefs.setPaymentId(categoriesPojo.getData().getCityData().getStripeTestKeys());
                            sprefs.setFcmTopicCity(categoriesPojo.getData().getCityData().getPushTopics().getCity());
                            sprefs.setFcmTopicAllCustomer(categoriesPojo.getData().getCityData().getPushTopics().getAllCustomers());
                            sprefs.setFcmTopicAllCityCustomer(categoriesPojo.getData().getCityData().getPushTopics().getAllCitiesCustomers());
                            sprefs.setFcmTopicAllOutZoneCustomers(categoriesPojo.getData().getCityData().getPushTopics().getOutZoneCustomers());
                            try{
                                FirebaseMessaging.getInstance().subscribeToTopic(sprefs.getFcmTopicCity());
                                FirebaseMessaging.getInstance().subscribeToTopic(sprefs.getFcmTopicAllCustomer());
                                FirebaseMessaging.getInstance().subscribeToTopic(sprefs.getFcmTopicAllCityCustomer());
                                FirebaseMessaging.getInstance().subscribeToTopic(sprefs.getFcmTopicAllOutZoneCustomers());
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            callService(latiLongi[0],latiLongi[1],sprefs,inteface);
                        }else{
                            Toast.makeText(mcontext,"No services found",Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d(TAG, "onError: getCategories "+error+ " headererror "+headerError);
                        switch(headerError){
                            case "549":
                                if(dialog==null ){
                                    AlertDialog.Builder builder=new AlertDialog.Builder(mcontext);
                                    builder.setMessage("COULD NOT GET CATEGORY");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            getCategories(mcontext,latiLongi,sprefss);
                                        }
                                    });
                                    dialog = builder.create();
                                    dialog.show();
                                }else if(dialog!=null && !dialog.isShowing()){
                                    dialog.show();
                                }
                                break;
                            case "404":{
                                inteface.onErrorCategories(headerError,error);
                                break;
                            }

                        }
                        if(error.contains("Failed to connect")){
                            if(dialog==null ){
                                AlertDialog.Builder builder=new AlertDialog.Builder(mcontext);
                                builder.setMessage("Unable to connect to Server ");
                                builder.setCancelable(false);
                                builder.setPositiveButton("RECONNECT", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        getCategories(mcontext,latiLongi,sprefss);
                                    }
                                });
                                dialog = builder.create();
                                dialog.show();
                            }else if(dialog!=null && !dialog.isShowing()){
                                dialog.show();
                            }
                        }
                    }
                });
    }

    private void getAccessToken(String data) {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {


                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            sprefs.setSession(jsonobj.getString("data"));
                            // inteface.excessToken

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            inteface.onError("");
                        else {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    inteface.onSessionError(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccessToken(errorHandel.getData());
                                    break;
                                default:
                                    inteface.onError(errorHandel.getMessage());
                                    break;
                            }
                        }
                    }
                });
    }

    public void callConfigApi(final SharedPrefs sharedPrefs, Context mcontext, final HomeModelIntrface inteface) {

        this.mcontext = mcontext;
        if (!sharedPrefs.getSession().equals("")) {
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "config",
                    OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang
                    , new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerData, String result) {
                            Log.w(TAG, "onSuccess: config"+result );
                            try {
                                ConfigPojo configPojo = new Gson().fromJson(result, ConfigPojo.class);
                                VariableConstant.PROFREQUENCYINTERVAL = configPojo.getData().getCustomerFrequency().getCustomerHomePageInterval();
                                sharedPrefs.setPaymentId(configPojo.getData().getStripeTestKeys());
                                sharedPrefs.setFcmTopicCity(configPojo.getData().getPushTopics().getCity());
                                sharedPrefs.setFcmTopicAllCustomer(configPojo.getData().getPushTopics().getAllCustomers());
                                sharedPrefs.setFcmTopicAllCityCustomer(configPojo.getData().getPushTopics().getAllCitiesCustomers());
                                sharedPrefs.setFcmTopicAllOutZoneCustomers(configPojo.getData().getPushTopics().getOutZoneCustomers());
                                FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicCity());
                                FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicAllCustomer());
                                FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicAllCityCustomer());
                                FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicAllOutZoneCustomers());
                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(String headerError, String error) {
                            if (headerError.equals("502")) {
                                inteface.onError("");
                            }else if(headerError.equals("549")){
                                inteface.onError(error);
                            }
                            else {
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        inteface.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        inteface.onError(errorHandel.getMessage());
                                        break;
                                }
                            }

                        }
                    });
        }

    }

    public void callPendingBooking(SharedPrefs sprefs, final HomeModelIntrface inteface)
    {
        this.sprefs = sprefs;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "reviewAndRatingPending", OkHttpConnection.Request_type.GET,
                new JSONObject(), sprefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result)
                    {
                        Log.i(TAG, "PendinBooking " + result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray arry = jsonObject.getJSONArray("data");
                            if(arry!=null && arry.length()>0)
                                inteface.onPendingBooking(arry.getLong(0));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(String headerError, String error) {
                        ErrorHandel errorHandel;
                        if (headerError.equals("502"))
                            HomeFragmentModel.this.inteface.onError("");
                        else {
                            try {
                                errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        HomeFragmentModel.this.inteface.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        HomeFragmentModel.this.inteface.onError(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    }
                });
    }

    public void saveImage(String result,Context mContext)
    {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray =  jsonObject.getJSONArray("data");
            for(int i= 0;i<jsonObject.getJSONArray("data").length();i++)
            {
                String imageName = jsonArray.getJSONObject(i).getString("image");
                int status = jsonArray.getJSONObject(i).getInt("status");
                ImageView ivOnline;
                if(status == 1)
                {
                    final View marker_view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custome_online_marker, null);
                    ivOnline = marker_view.findViewById(R.id.ivOffLine);
                }
                else
                {
                    final View marker_view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custome_offline_marker, null);
                    ivOnline = marker_view.findViewById(R.id.ivOffLine);
                }
                Log.i(TAG, "saveImage: "+imageName);
                if(!imageName.equals("")) {

                    /*Picasso.with(mContext)
                            .load(imageName)
                            .into(ivOnline);*/
                  /*  Glide.with(mContext)
                            .load(imageName)
                            .into(ivOnline);*/
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateCategoryProviders(SharedPrefs sharedPrefs, HomeFragment homeFragment) {
        if(!sharedPrefs.getCategoryId().equalsIgnoreCase("")){
            if(!VariableConstant.MQTTRESPONSEPROVIDER.equals(""))
                ApplicationController.getInstance().gethModel().responceHomeFragModel(VariableConstant.MQTTRESPONSEPROVIDER, homeFragment);
        }else{
            Log.e(TAG, "callGetProvider: Category id is blank");
        }
    }
}
