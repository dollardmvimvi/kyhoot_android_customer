package com.Kyhoot.pojoResponce;


import java.util.ArrayList;

/**
 * @since 19/09/17.
 */

public class WalletTransDataPojo
{
   /* "data":{
    "debitArr":[],
    "creditArr":[],
    "creditDebitArr":[]}*/

   private ArrayList<CreditDebitTransctions> debitArr;
   private ArrayList<CreditDebitTransctions> creditArr;
   private ArrayList<CreditDebitTransctions> creditDebitArr;


    public ArrayList<CreditDebitTransctions> getDebitArr() {
        return debitArr;
    }

    public void setDebitArr(ArrayList<CreditDebitTransctions> debitArr) {
        this.debitArr = debitArr;
    }

    public ArrayList<CreditDebitTransctions> getCreditArr() {
        return creditArr;
    }

    public void setCreditArr(ArrayList<CreditDebitTransctions> creditArr) {
        this.creditArr = creditArr;
    }

    public ArrayList<CreditDebitTransctions> getCreditDebitArr() {
        return creditDebitArr;
    }

    public void setCreditDebitArr(ArrayList<CreditDebitTransctions> creditDebitArr) {
        this.creditDebitArr = creditDebitArr;
    }

    @Override
    public String toString() {
        return "WalletTransDataPojo{" +
                "debitArr=" + debitArr +
                ", creditArr=" + creditArr +
                ", creditDebitArr=" + creditDebitArr +
                '}';
    }
}
