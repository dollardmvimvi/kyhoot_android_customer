package com.Kyhoot.controllers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.main.ConfirmBookingActivity;
import com.Kyhoot.models.ConfirmBookModel;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * <h>ConfirmBookController</h>
 * Created by Ali on 10/30/2017.
 */

public class ConfirmBookController implements NetworkCheck {


    private static final String TAG = "ConfirmBookController";
    private ConfirmBookModel confirmBookModel;
    private UtilInterFace.ConfirmBook confirmBook;
    private SharedPrefs sharedPrefs;

    public ConfirmBookController(UtilInterFace.ConfirmBook confirmBook) {
        confirmBookModel = new ConfirmBookModel();
        this.confirmBook = confirmBook;
    }


    public void checkNetwork(SharedPrefs sharedPrefs, AlertProgress alertProgress) {
        this.sharedPrefs = sharedPrefs;
        confirmBookModel.checkNetwork(alertProgress, this);
    }

    public void liveBooking(SharedPrefs sharedPrefs, int bookingModel, int paymentType, int paidByWallet, String proId, String jobDesc, String promoCode, String paymentCardId, int bookingTime, int noofHours,JSONArray jobPhotosArray) {
        this.sharedPrefs=sharedPrefs;
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bookingModel", bookingModel);
            jsonObject.put("bookingType", VariableConstant.BOOKINGTYPE);
            jsonObject.put("paymentMethod", paymentType);
            jsonObject.put("addLine1", sharedPrefs.getBookingAddress());
            jsonObject.put("latitude", Double.parseDouble(sharedPrefs.getBookLatitude()));
            jsonObject.put("longitude", Double.parseDouble(sharedPrefs.getBookLongitude()));
            jsonObject.put("paidByWallet", paidByWallet);
            jsonObject.put("promoCode", promoCode);
            jsonObject.put("categoryId", sharedPrefs.getCategoryId());
            jsonObject.put("cartId",VariableConstant.CARTID );
            jsonObject.put("paymentCardId", paymentCardId);
            jsonObject.put("jobDescription", jobDesc);
            jsonObject.put("deviceTime", Utilities.dateintwtfour());
            jsonObject.put("jobImages",jobPhotosArray);

            if (VariableConstant.BOOKINGTYPE==3)
            {
                if (VariableConstant.repeatDays.size()>0)
                    jsonObject.put("days", new JSONArray(VariableConstant.repeatDays));
                jsonObject.put("bookingDate",""+VariableConstant.SCHEDULE_BOOK_REPEAT_START_DATE);
                //jsonObject.put("scheduleTime",VariableConstant.SCHEDULEDTIME);
                if(noofHours>1)
                {
                    int minutes = noofHours*60;
                    jsonObject.put("scheduleTime",minutes);
                }else{
                    int minutes = (int) VariableConstant.SCHEDULEDTIME * 60;
                    jsonObject.put("scheduleTime",minutes);
                }
                jsonObject.put("endTimeStamp",VariableConstant.SCHEDULE_BOOK_REPEAT_END_DATE);
            }

            if(VariableConstant.BOOKINGMODEL==1){
                jsonObject.put("providerId", "");
            }else if(VariableConstant.BOOKINGMODEL==2){
                jsonObject.put("providerId", VariableConstant.providerId);
            }
            if(VariableConstant.BOOKINGTYPE==2 /*|| VariableConstant.BOOKINGTYPE==3*/) {
                jsonObject.put("bookingDate",VariableConstant.SCHEDULEDDATE);
                if(noofHours>1)
                {
                    int minutes = noofHours*60;
                    jsonObject.put("scheduleTime",minutes);
                }else{
                    int minutes = (int) VariableConstant.SCHEDULEDTIME * 60;
                    jsonObject.put("scheduleTime",minutes);
                }

            }
            Log.d("CONFIRMBOOKING", "liveBooking: " + jsonObject);
            confirmBookModel.doLiveBooking(jsonObject, sharedPrefs, confirmBook);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkSchedule(SharedPrefs prefs, int data) {
        this.sharedPrefs=prefs;
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("latitude", Double.parseDouble(sharedPrefs.getBookLatitude()));
            jsonObject.put("longitude", Double.parseDouble(sharedPrefs.getBookLongitude()));
            jsonObject.put("categoryId", VariableConstant.selectedCategoryId);
            jsonObject.put("deviceTime", Utilities.dateintwtfour());
            /*if(VariableConstant.BOOKINGMODEL==2){
                jsonObject.put("providerId", VariableConstant.providerId);
            }*/
            jsonObject.put("cartId",VariableConstant.CARTID);

            if(VariableConstant.BOOKINGMODEL==1){
                jsonObject.put("providerId", "");
            }else if(VariableConstant.BOOKINGMODEL==2){
                jsonObject.put("providerId", VariableConstant.providerId);
            }
            if(VariableConstant.BOOKINGTYPE==2) {
                jsonObject.put("bookingDate",VariableConstant.SCHEDULEDDATE);
                jsonObject.put("scheduleTime",60);
            }
            Log.d("CHECKSCHEDULE", ""+jsonObject);
            confirmBookModel.checkSchedule(jsonObject, sharedPrefs, confirmBook);



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getLastDue(String token) {
        confirmBookModel.getLastDue(token, confirmBook);
    }

    public void getSurgePrice(SharedPrefs sharedPrefs,double latitude,double longitude){
        this.sharedPrefs=sharedPrefs;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingType", VariableConstant.BOOKINGTYPE);
            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
            jsonObject.put("categoryId", VariableConstant.selectedCategoryId);

            Log.d("getSurgePrice", "SURGE PRICE: " + jsonObject);

            confirmBookModel.getSurgePrice(jsonObject,sharedPrefs,confirmBook);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void validatePromocode(SharedPrefs sharedPrefs,int paymentType,int paidByWallet, String promoCode){
        this.sharedPrefs=sharedPrefs;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("paymentMethod", paymentType);
            jsonObject.put("latitude", Double.parseDouble(sharedPrefs.getBookLatitude()));
            jsonObject.put("longitude", Double.parseDouble(sharedPrefs.getBookLongitude()));
            jsonObject.put("cartId", VariableConstant.CARTID);
            jsonObject.put("couponCode", promoCode);
            jsonObject.put("paidByWallet", paidByWallet);

            Log.d("VALIDATE PROMOCODE", "validatePromocode: " + jsonObject);
            confirmBookModel.validatePromocode(jsonObject,sharedPrefs,confirmBook);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCartServiceCall(SharedPrefs prefs,String categoryId, ConfirmBookingActivity confirmBookingActivity) {
        this.sharedPrefs=prefs;
        String url="";
        if(VariableConstant.BOOKINGMODEL==1)
        {
            url=VariableConstant.SERVICE_URL+ "cart/"+categoryId+"/0"; //Passing 0 instead of provider id if
            //provider id is not available
        }else{
            url=VariableConstant.SERVICE_URL+ "cart/"+categoryId+"/"+VariableConstant.providerId+"";
        }
        Log.d(TAG, "getCartServiceCall: "+url);
        OkHttpConnection.requestOkHttpConnection(url,
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        Log.d(TAG, "onSuccess: "+headerData+" result: "+result);
                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            confirmBook.onSuccessGetCart(headerData,result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //convert java object to JSON format
                    }

                    @Override
                    public void onError(String headerError, String error) {

                        confirmBook.onFailureGetCart(headerError,error);
                    }
                });
    }


    @Override
    public void onNetworkAvailble() {
        confirmBook.showProgress();

    }

    @Override
    public void onNetworkNotAvailble() {
        confirmBook.hideProgress();
    }

    public void distanceCal(double distance, TextView tvProDistance, Context mContext) {

        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        // String faremount=nf_out.format(Double.parseDouble(pubNubMas)/1000);
        @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", distance);
        String kilometr = kilometer + " " + VariableConstant.distanceUnit;
//        tvProDistance.setText(kilometr);

    }
    public void addCartData(String serviceId, int action, boolean isHourlyService,boolean minimumhr) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("categoryId", sharedPrefs.getCategoryId());
            jsonObject.put("serviceId", serviceId);
            if(!minimumhr){
                jsonObject.put("quntity", VariableConstant.MINIMUM_HOUR);
            }else{
                jsonObject.put("quntity", 1);
            }

            jsonObject.put("action", action);
            if(VariableConstant.BOOKINGMODEL==1){
                jsonObject.put("providerId", "");
            }else{
                jsonObject.put("providerId", VariableConstant.providerId);
            }
            if(isHourlyService){
                jsonObject.put("serviceType", 2);
            }else{
                jsonObject.put("serviceType", 1);
            }
            Log.d(TAG, "addCartData " + jsonObject);
            addCartDataServiceCall(jsonObject, confirmBook);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void addCartDataServiceCall(JSONObject jsonObject, final UtilInterFace.ConfirmBook confirmBook) {
        Log.d(TAG, "addCartDataServiceCall: "+VariableConstant.SERVICE_URL + "cart");
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "cart",
                OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            confirmBook.onSuccessCartDataAdd(headerData,result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                        confirmBook.onFailureCartDataAdd(headerError,error);
                    }
                });
    }

    public void getdefaultCard(final UtilInterFace.ConfirmBook confirmBook, SharedPrefs sharedPrefs, final Context mcontext) {
            final Gson gson = new Gson();
            //http://api2.0.iserve.ind.in/card
            OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "card",
                    OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang
                    , new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerData, String result) {
                            Log.d("TAG", "onSuccessPAYMENT: "+result);
                            confirmBook.onCardSuccess(headerData,result);
                        }

                        @Override
                        public void onError(String headerError, String error) {
                            try {
                                ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                                confirmBook.onCardFailure(headerError,errorHandel.getMessage());
                            } catch (Exception e) {
                                confirmBook.onCardFailure(headerError,e.getMessage());
                            }
                        }
                    });
    }

    public void listOfDates(List<Date> datesBetweenUsingJava7) {

        Calendar cal = Calendar.getInstance();
        ArrayList<Integer> dayInArray = new ArrayList<>();
        for(int i = 0; i<datesBetweenUsingJava7.size();i++)
        {
            cal.setTime(datesBetweenUsingJava7.get(i));
            int dayIn = cal.get(Calendar.DAY_OF_WEEK);
            dayInArray.add(dayIn);
        }

        if(dayInArray.get(dayInArray.size()-1) == 7)
            dayInArray.add(1);
        else
            dayInArray.add(dayInArray.get(dayInArray.size()-1)+1);

        confirmBook.setDateOnMap(dayInArray);
    }

    /**
     * <h2>Upload Image</h2>
     * <p>This method is used to call UploadFileToServer class to upload image in the server</p>
     * @param path this parameter contains image path to be uploaded in the server
     */
    public void uploadImageOnServer(String path){
        new UploadFileToServer().execute(path);
    }

    /**
     * Uploading the file to server
     */
    @SuppressLint("StaticFieldLeak")
    public class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(VariableConstant.CHAT_URL + "uploadImageOnServer")
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d("Shijen", "doInBackground: " + responseCode);
                Log.d("Shijen", "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");
                    confirmBook.onSuccessImageUpload(image);
                }
                else
                    confirmBook.onFailureImageUpload();
            } catch (JSONException e) {
                confirmBook.onFailureImageUpload();
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }

    }
}
