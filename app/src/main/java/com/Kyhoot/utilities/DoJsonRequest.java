package com.Kyhoot.utilities;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

/**
 * <h>DoJsonRequest</h>
 * Created by Ali on 12/22/2017.
 */

public class DoJsonRequest
{

    private static SimpleDateFormat GMTTimeFormatter = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss'Z'");

    public void doJsonRequest(String request_Url, JSONObject requestParameters, String token,int lang, JsonRequestCallback callbacks)
    {
        DoJsonRequest.JsonHttpRequestData data = new DoJsonRequest.JsonHttpRequestData();
        data.request_Url = request_Url;
        data.requestParameter = requestParameters;
        data.token = token;
        data.lang = lang;
        data.callbacks = callbacks;

        new JsonHttpRequest().execute(data);
    }
    public interface JsonRequestCallback
    {
        /**
         * Called When Success result of JSON request
         *
         * @param result if get successful response
         */
        void onSuccess(String result);


        /**
         * Called When Error result of JSON request
         *
         * @param error if get error response
         */
        void onError(String error);

    }

    private static  class JsonHttpRequestData
    {
        String request_Url,token;
        int lang;
        JSONObject requestParameter;
        JsonRequestCallback callbacks;
    }

    /********************************************************************************************************************/

    private  static class JsonHttpRequest extends AsyncTask<JsonHttpRequestData, Void, String>
    {
        JsonRequestCallback callbacks;
        boolean error =false;

        @Override
        protected void onPreExecute()
        {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(JsonHttpRequestData... params)
        {
            // TODO Auto-generated method stub
            callbacks = params[0].callbacks;
            String result="";

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(params[0].request_Url);

            HttpConnectionParams.setConnectionTimeout(client.getParams(), 60000); //Timeout Limit
            HttpResponse http_response;

            StringEntity request;
            try
            {
                request = new StringEntity( params[0].requestParameter.toString(), HTTP.UTF_8);

                // To Check sending parameters open following comment
                Log.d("TAG","Request Data: " + params[0].requestParameter.toString());

                request.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //request.setContentEncoding(new BasicHeader("authentication: authorization",params[0].token));
               // request.setContentEncoding(new BasicHeader("Authorization: authorization",params[0].token));
                request.setContentEncoding(new BasicHeader("authorization",params[0].token));

                request.setContentEncoding(new BasicHeader("lan",params[0].lang+""));
                post.setEntity(request);
                http_response = client.execute(post);
                result = EntityUtils.toString(http_response.getEntity());
            }
            catch (UnsupportedEncodingException e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            catch (ClientProtocolException e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            catch (IOException e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            catch (Exception e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            if(!error)
            {
                callbacks.onSuccess(result);
            }
            else
            {
                callbacks.onError(result);
            }
        }
    }


}
