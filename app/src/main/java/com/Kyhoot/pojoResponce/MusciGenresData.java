package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>MusciGenresData</h>
 * Created by Ali on 9/26/2017.
 */

public class MusciGenresData implements Serializable {
    private String id, catName;

    public String getId() {
        return id;
    }

    public String getCatName() {
        return catName;
    }
}
