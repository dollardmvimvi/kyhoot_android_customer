package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>GeneralDataMsg</h>
 * GeneralDataMsg for Data Msg in Data
 * Created by 3Embed on 10/17/2017.
 */

public class GeneralDataMsg implements Serializable {
    private String message;
    private GeneralProviderData data;

    public String getMessage() {
        return message;
    }

    public GeneralProviderData getData() {
        return data;
    }
}
