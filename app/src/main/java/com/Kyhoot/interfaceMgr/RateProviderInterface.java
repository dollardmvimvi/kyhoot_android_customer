package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.InvoiceDetails;

/**
 * <h>RateProviderInterface</h>
 * Created by Ali on 11/25/2017.
 */

public interface RateProviderInterface {
    void onRateProviderSuccess();

    void onRateProviderError(String headerError, String errorMsg);

    void onSessionExpired(String sessionExp);

    void onShowProgress();

    void onHideProgress();

    void onGetInvoiceDetails(InvoiceDetails.InvoiceData invoiceData);
}
