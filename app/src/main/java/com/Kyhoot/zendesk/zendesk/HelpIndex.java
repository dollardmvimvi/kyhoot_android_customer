package com.Kyhoot.zendesk.zendesk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.Kyhoot.zendesk.zendeskadapter.HelpIndexAdapter;
import com.Kyhoot.zendesk.zendeskpojo.AllTicket;
import com.Kyhoot.zendesk.zendeskpojo.OpenClose;
import com.Kyhoot.zendesk.zendeskpojo.TicketClose;
import com.Kyhoot.zendesk.zendeskpojo.TicketOpen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HelpIndex extends AppCompatActivity {

    Toolbar toolHelpIndex;
    private AppTypeface appTypeface;
    private RelativeLayout rlHelpIndex;
    private RecyclerView recyclerHelpIndex;
    private SharedPrefs sprefs;
    private HelpIndexAdapter helpIndexAdapter;
    private ArrayList<OpenClose> openCloses;
    private ProgressBar progressbarHelpIndex;
    private AlertProgress alertProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_index);
        appTypeface = AppTypeface.getInstance(this);
        openCloses = new ArrayList<>();
        initializeToolBar();
        initializeView();
        getZendexTicket();
    }

    private void initializeView() {
        sprefs = new SharedPrefs(this);
        alertProgress = new AlertProgress(this);
        rlHelpIndex = findViewById(R.id.rlHelpIndex);
        recyclerHelpIndex = findViewById(R.id.recyclerHelpIndex);
        progressbarHelpIndex = findViewById(R.id.progressbarHelpIndex);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerHelpIndex.setLayoutManager(layoutManager);
        helpIndexAdapter = new HelpIndexAdapter(this,openCloses);
        recyclerHelpIndex.setAdapter(helpIndexAdapter);
    }

    /*
    initialize toolBar
     */
    private void initializeToolBar()
    {
      //  toolHelpIndex = findViewById(R.id.tool_helpIndex);
        setSupportActionBar(toolHelpIndex);
        TextView tv_center = findViewById(R.id.tv_center);
        TextView tv_skip = findViewById(R.id.tv_skip);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.helpcenter);
        tv_skip.setText("+");
        tv_skip.setTextSize(20);
        tv_skip.setTextColor(Utilities.getColor(this,R.color.red_login));
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        tv_skip.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolHelpIndex.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolHelpIndex.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HelpIndex.this,HelpIndexTicketDetails.class);
                intent.putExtra("ISTOAddTICKET",true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
            }
        });
    }

    private void getZendexTicket()
    {
        progressbarHelpIndex.setVisibility(View.VISIBLE);
        //http://138.197.78.50:9871/zendesk/user/ticket/dhaval
        OkHttpConnection.requestOkHttpConnection("http://api2.0.iserve.ind.in/zendesk/user/ticket/" + sprefs.getEMail(), OkHttpConnection.Request_type.GET
                , new JSONObject(), sprefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result)
                    {
                        Log.d("TAG", "SpinneronSuccess: "+result);
                        AllTicket allTicket = new Gson().fromJson(result,AllTicket.class);

                        if(allTicket.getData().getClose().size()>0 || allTicket.getData().getOpen().size()>0)
                        {
                            progressbarHelpIndex.setVisibility(View.GONE);
                            rlHelpIndex.setVisibility(View.GONE);
                            recyclerHelpIndex.setVisibility(View.VISIBLE);
                            if(allTicket.getData().getClose().size()>0)
                            {
                                for(int i = 0;i<allTicket.getData().getClose().size();i++)
                                {
                                    TicketClose ticketClose = allTicket.getData().getClose().get(i);
                                    OpenClose openClose = new OpenClose(ticketClose.getId(),ticketClose.getTimeStamp()
                                    ,ticketClose.getStatus(),ticketClose.getSubject(),ticketClose.getType(),ticketClose.getPriority()
                                    ,ticketClose.getDescription());
                                    if(i==0)
                                    {
                                        openClose.setFirst(true);
                                    }
                                    openCloses.add(openClose);
                                }
                            }
                            if(allTicket.getData().getOpen().size()>0)
                            {
                                for(int i = 0;i<allTicket.getData().getOpen().size();i++)
                                {
                                    TicketOpen ticketOpen = allTicket.getData().getOpen().get(i);
                                    OpenClose openClose = new OpenClose(ticketOpen.getId(),ticketOpen.getTimeStamp()
                                            ,ticketOpen.getStatus(),ticketOpen.getSubject(),ticketOpen.getType(),
                                            ticketOpen.getPriority(),ticketOpen.getDescription());

                                    if(i==0)
                                    {
                                        openClose.setFirst(true);
                                    }
                                    openCloses.add(openClose);
                                }
                            }
                            helpIndexAdapter.notifyDataSetChanged();
                        }
                        else
                        {
                            rlHelpIndex.setVisibility(View.VISIBLE);
                            recyclerHelpIndex.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onError(String headerError, String error)
                    {
                        progressbarHelpIndex.setVisibility(View.GONE);
                        if (headerError.equals("502"))
                        {
                            alertProgress.alertinfo(getString(R.string.serverProblm));
                        }
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        sessionExpired(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        alertProgress.alertinfo(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    private void getAccessToken(String data)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            sprefs.setSession(jsonobj.getString("data"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            alertProgress.alertinfo(getString(R.string.serverProblm));
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        sessionExpired(errorHandel.getMessage());

                                        break;
                                    default:
                                        alertProgress.alertinfo(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    private void sessionExpired(final String errorMsg)
    {
        alertProgress.alertPostivionclick(errorMsg,
                new DialogInterfaceListner() {
                    @Override
                    public void dialogClick(boolean isClicked) {
                        Utilities.setMAnagerWithBID(HelpIndex.this,sprefs,errorMsg);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VariableConstant.isHelpIndexOpen = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        VariableConstant.isHelpIndexOpen = true;
    }
}
