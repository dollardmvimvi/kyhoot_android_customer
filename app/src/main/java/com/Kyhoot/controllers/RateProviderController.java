package com.Kyhoot.controllers;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

//import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.RateProviderInterface;
import com.Kyhoot.utilities.SharedPrefs;

/**
 * Created by Ali on 11/25/2017.
 */

public class RateProviderController {

    private Dialog indialog;
    private SharedPrefs sharedPrefs;
    private RateProviderInterface rateProviderInterface;
    public static final String TAG="RATEPROVIDERCONTROLLER";
    public TextView tvInvoiceTotalAmt;
    private Context mContext;

    /*public RateProviderController(SharedPrefs sharedPrefs, RateProviderInterface rateProviderInterface) {
        this.sharedPrefs = sharedPrefs;
        this.rateProviderInterface = rateProviderInterface;
    }


    public void iNvoiceDetails(double bid) {
        Log.d(TAG, "iNvoiceDetails: bid"+bid+" url: "+VariableConstant.SERVICE_URL + "booking/invoice/" + bid);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "booking/invoice/" + bid,
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang,
                new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: "+result);
                        InvoiceDetails invoiceDetails = new Gson().fromJson(result, InvoiceDetails.class);
                        rateProviderInterface.onGetInvoiceDetails(invoiceDetails.getData());
                    }

                    @Override
                    public void onError(String headerError, String error) {


                    }
                });
    }

    public void updateReview(double bid, float rating, String etReview) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bid);
            jsonObject.put("rating", rating);
            jsonObject.put("review", etReview);
            Log.d("TAG", "updateReview: " + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "reviewAndRating",
                OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(), VariableConstant.SelLang,
                new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: UpdateReview "+result);
                        rateProviderInterface.onHideProgress();
                        rateProviderInterface.onRateProviderSuccess();

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    rateProviderInterface.onSessionExpired(errorHandel.getMessage());
                                    break;
                                case "440":
                                    rateProviderInterface.onSessionExpired(errorHandel.getMessage());
                                default:
                                    rateProviderInterface.onRateProviderError(headerError,errorHandel.getMessage());
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void openDialog(Context mcontext, AppTypeface apptypeFace, InvoiceDetails.InvoiceData service, String signURL, double discount) {

        TextView tvInvoiceDialog, tvInvoiceDialogDismiss, tvInvGigTimeFee, tvInvGigTimeFeeAmt, tvInvDiscount, tvInvDiscountAmount,
                tvInvoiceTotal,tvLocation,tvsignature,tvReceiptDetailsHead,
        tvTravelAmount,tvTravelLabel,tvVisitAmount,tvVisitLabel,tvCancelAmount,tvCancelLabel,tvInvLastDuesFee,tvInvLastDuesFeeAmt;
        RelativeLayout rlCancelLayout,rlLastDuesFee,rltravelLayout,rlVisitFee,rldiscount;
        ImageView ivInvoiceSignature;
        LinearLayout llLiveFee;
        this.mContext=mcontext;
        indialog = new Dialog(mcontext);
        indialog.setCanceledOnTouchOutside(true);
        indialog.setCancelable(true);
        indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        indialog.setContentView(R.layout.popup_invoice_fee_breakdown);
        indialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tvInvoiceDialog = indialog.findViewById(R.id.tvInvoiceDialog);
        llLiveFee = indialog.findViewById(R.id.llLiveFee);
        tvInvoiceDialogDismiss = indialog.findViewById(R.id.tvInvoiceDialogDismiss);
        tvInvGigTimeFee = indialog.findViewById(R.id.tvInvGigTimeFee);
        tvInvGigTimeFeeAmt = indialog.findViewById(R.id.tvInvGigTimeFeeAmt);
        tvLocation = indialog.findViewById(R.id.tvLocation);
        tvInvDiscount = indialog.findViewById(R.id.tvInvDiscount);
        tvInvDiscountAmount = indialog.findViewById(R.id.tvInvDiscountAmount);
        tvInvoiceTotal = indialog.findViewById(R.id.tvInvoiceTotal);
        tvInvoiceTotalAmt = indialog.findViewById(R.id.tvInvoiceTotalAmt);
        ivInvoiceSignature = indialog.findViewById(R.id.ivInvoiceSignature);
        tvReceiptDetailsHead = indialog.findViewById(R.id.tvReceiptDetailsHead);
        tvTravelAmount = indialog.findViewById(R.id.tvTravelAmount);
        tvTravelLabel = indialog.findViewById(R.id.tvTravelLabel);
        tvVisitAmount = indialog.findViewById(R.id.tvVisitAmount);
        tvVisitLabel = indialog.findViewById(R.id.tvVisitLabel);
        tvCancelAmount = indialog.findViewById(R.id.tvCancelAmount);
        tvCancelLabel = indialog.findViewById(R.id.tvCancelLabel);
        tvInvLastDuesFee = indialog.findViewById(R.id.tvInvLastDuesFee);
        tvsignature = indialog.findViewById(R.id.tvsignature);
        tvInvLastDuesFeeAmt = indialog.findViewById(R.id.tvInvLastDuesFeeAmt);
        rlCancelLayout = indialog.findViewById(R.id.rlCancelLayout);
        rlLastDuesFee = indialog.findViewById(R.id.rlLastDuesFee);
        rldiscount = indialog.findViewById(R.id.rldiscount);
        rltravelLayout = indialog.findViewById(R.id.rltravelLayout);
        rlVisitFee = indialog.findViewById(R.id.rlVisitFee);
        tvInvoiceDialog.setTypeface(apptypeFace.getHind_bold());
        tvsignature.setTypeface(apptypeFace.getHind_bold());
        tvReceiptDetailsHead.setTypeface(apptypeFace.getHind_bold());
        tvInvoiceDialogDismiss.setTypeface(apptypeFace.getHind_bold());
        tvInvoiceTotal.setTypeface(apptypeFace.getHind_bold());
        tvInvoiceTotalAmt.setTypeface(apptypeFace.getHind_bold());
        tvInvDiscountAmount.setTypeface(apptypeFace.getHind_regular());
        tvTravelAmount.setTypeface(apptypeFace.getHind_regular());
        tvTravelLabel.setTypeface(apptypeFace.getHind_regular());
        tvVisitAmount.setTypeface(apptypeFace.getHind_regular());
        tvVisitLabel.setTypeface(apptypeFace.getHind_regular());
        tvCancelAmount.setTypeface(apptypeFace.getHind_regular());
        tvCancelLabel.setTypeface(apptypeFace.getHind_regular());
        tvInvDiscount.setTypeface(apptypeFace.getHind_regular());
        tvInvLastDuesFee.setTypeface(apptypeFace.getHind_regular());
        tvLocation.setTypeface(apptypeFace.getHind_regular());
        tvInvLastDuesFeeAmt.setTypeface(apptypeFace.getHind_regular());
        tvInvGigTimeFee.setTypeface(apptypeFace.getHind_regular());
        tvInvGigTimeFeeAmt.setTypeface(apptypeFace.getHind_regular());
        if (!signURL.equals("")) {
            Log.d(TAG, "openDialog: "+signURL);
            Picasso.with(mcontext)
                    .load(signURL)
                    .into(ivInvoiceSignature);
        }


        tvInvoiceDialogDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indialog.dismiss();
            }
        });
        if(service.getStatus()==12){
            rlCancelLayout.setVisibility(View.VISIBLE);
        }
        tvLocation.setText(service.getAddLine1());
        String currencySymbol = sharedPrefs.getCurrencySymbol();
        setPaymentBreakdown(service,llLiveFee,apptypeFace);
        Utilities.setAmtOnRecept(service.getCartdata().getTotalAmount(),tvInvGigTimeFeeAmt,currencySymbol);
        Utilities.setAmtOnRecept(service.getAccounting().getCancellationFee(),tvCancelAmount,currencySymbol);
        if(service.getAccounting().getVisitFee()>0){
            rlVisitFee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getVisitFee(),tvVisitAmount,currencySymbol);
        }
        if(service.getAccounting().getTravelFee()>0){
            rltravelLayout.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getTravelFee(),tvTravelAmount,currencySymbol);
        }

        if(service.getAccounting().getLastDues()>0){
            rlLastDuesFee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getLastDues(),tvInvLastDuesFeeAmt,currencySymbol);
        }

        String discountFee = currencySymbol + " " + discount;
        Utilities.setAmtOnRecept(discount,tvInvDiscountAmount,service.getCartdata().getCurrencySymbol());
        double totalamtFee = service.getAccounting().getTotal();
        Utilities.setAmtOnRecept(totalamtFee,tvInvoiceTotalAmt,service.getCartdata().getCurrencySymbol());
        indialog.show();
    }
    private void setPaymentBreakdown(InvoiceDetails.InvoiceData invoiceData, LinearLayout llLiveFee, AppTypeface apptypeFace) {
        int size=invoiceData.getCartdata().getCheckOutItem().size();
        for(int i=0;i<size;i++){
            Item item=invoiceData.getCartdata().getCheckOutItem().get(i);
            View infltedView = LayoutInflater.from(mContext).inflate(R.layout.single_service_price, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price

            String namePerUnit;
            if(invoiceData.getAccounting().getServiceType()==2){
                int accJobhr= (int) (invoiceData.getAccounting().getTotalActualJobTimeMinutes()/60);
                int accJobMin= (int) (invoiceData.getAccounting().getTotalActualJobTimeMinutes()%60);
                String actual_hrs;
                if(accJobMin>0) {
                    actual_hrs = accJobhr + " hr " + accJobMin + " min";
                }else{
                    actual_hrs = accJobhr + " hr " ;
                }
                namePerUnit=item.getServiceName() + " (x" + actual_hrs+")";
                Utilities.setAmtOnRecept(invoiceData.getAccounting().getTotalActualHourFee(),tvServicePrice,invoiceData.getCartdata().getCurrencySymbol());
            }else{
                if(item.getServiceName().trim().isEmpty()){
                    namePerUnit="Hourly"+" (x" + item.getQuntity()+")";
                }else{
                    namePerUnit = item.getServiceName() + " (x" + item.getQuntity()+")";
                }
                Utilities.setAmtOnRecept(item.getAmount(),tvServicePrice,invoiceData.getCartdata().getCurrencySymbol());
            }
            tvServiceName.setText(namePerUnit);
            tvServiceName.setTypeface(apptypeFace.getHind_regular());
            tvServicePrice.setTypeface(apptypeFace.getHind_regular());
        }
        size=invoiceData.getAdditionalService().size();
        for(int i=0;i<size;i++){
            AdditionalService item=invoiceData.getAdditionalService().get(i);
            View infltedView = LayoutInflater.from(mContext).inflate(R.layout.single_service_price, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit = item.getServiceName();
            tvServiceName.setText(namePerUnit);
            tvServicePrice.setText(invoiceData.getCartdata().getCurrencySymbol()+" "+item.getPrice());
            tvServiceName.setTypeface(apptypeFace.getHind_regular());
            tvServicePrice.setTypeface(apptypeFace.getHind_regular());
        }
        //Utilities.setAmtOnRecept(invoiceData.getCartdata().getTotalAmount(),tvInvoiceTotalAmt,invoiceData.getCartdata().getCurrencySymbol());
    }*/
}
