package com.Kyhoot.utilities;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;


/**
 * <h>HttpConnections</h>
 * Created by 3Embed on 10/11/2017.
 */

public class HttpConnections {
    public static String callhttpRequest(String url) {
        System.out.println("com.com.utility url..." + url);
        url = url.replaceAll(" ", "%20");
        String resp = null;
        HttpGet httpRequest;
        try {
            httpRequest = new HttpGet(url);
            HttpParams httpParameters = new BasicHttpParams();

            int timeoutConnection = 60000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 60000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpRequest);
            HttpEntity entity = response.getEntity();
            BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
            final long contentLength = bufHttpEntity.getContentLength();
            if ((contentLength >= 0)) {
                InputStream is = bufHttpEntity.getContent();
                int tobeRead = is.available();
                System.out.println("Utility callhttpRequest tobeRead.." + tobeRead);
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                int ch;

                while ((ch = is.read()) != -1) {
                    bytestream.write(ch);
                }

                resp = new String(bytestream.toByteArray());
                System.out.println("Utility callhttpRequest resp.." + resp);
            }
        } catch (MalformedURLException e) {
            System.out.println("Utility callhttpRequest.." + e);
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Utility callhttpRequest.." + e);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Utility callhttpRequest.." + e);
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Utility Exception.." + e);
        }
        return resp;
    }

}
