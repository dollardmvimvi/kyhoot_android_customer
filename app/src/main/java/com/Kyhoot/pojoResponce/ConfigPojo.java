package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by Ali on 11/23/2017.
 */

public class ConfigPojo implements Serializable {
    private String message;
    private ConfigData data;

    public String getMessage() {
        return message;
    }

    public ConfigData getData() {
        return data;
    }

    public class ConfigData implements Serializable{
        /*"currencySymbol":"£",
"currency":"GBP",
"mileage_metric":"1",
"appVersions":{},
"stripeTestKeys":{},
"stripeLiveKeys":{},
"custGoogleMapKeys":[],
"paymentMethods":{},
"customerFrequency":{},
"pushTopics":{},
"latLongDisplacement":10*/
        private PushTopics pushTopics;
        private String currencySymbol, currency, mileage_metric;
        private String stripeTestKeys;
        private StripeLiveKeys stripeLiveKeys;
        private GoogleMapKeys custGoogleMapKeys;
        private PaymentMethod paymentMethods;
        private CustomerFrequency customerFrequency;

        public PushTopics getPushTopics() {
            return pushTopics;
        }

        public CustomerFrequency getCustomerFrequency() {
            return customerFrequency;
        }

        public String getCurrencySymbol() {
            return currencySymbol;
        }

        public String getCurrency() {
            return currency;
        }

        public String getMileage_metric() {
            return mileage_metric;
        }

        public String getStripeTestKeys() {
            return stripeTestKeys;
        }

        public StripeLiveKeys getStripeLiveKeys() {
            return stripeLiveKeys;
        }

        public GoogleMapKeys getCustGoogleMapKeys() {
            return custGoogleMapKeys;
        }

        public PaymentMethod getPaymentMethods() {
            return paymentMethods;
        }


        public class StripeTestKeys {
            private String PublishableKey, SecreteKey;

            public String getPublishableKey() {
                return PublishableKey;
            }

            public String getSecreteKey() {
                return SecreteKey;
            }
        }

        public class StripeLiveKeys {
            /*"SecreteKey":"",
    "PublishableKey":""*/
            private String SecreteKey, PublishableKey;

            public String getSecreteKey() {
                return SecreteKey;
            }

            public String getPublishableKey() {
                return PublishableKey;
            }
        }

        private class GoogleMapKeys {
        }

        public class PaymentMethod {
            /*"card":true,
    "cash":false,
    "wallet":false*/
            private boolean card, cash, wallet;

            public boolean isCard() {
                return card;
            }

            public boolean isCash() {
                return cash;
            }

            public boolean isWallet() {
                return wallet;
            }
        }

        public class CustomerFrequency {
            private int customerHomePageInterval;

            public int getCustomerHomePageInterval() {
                return customerHomePageInterval;
            }
        }

        public class PushTopics implements Serializable
        {
/*"allCustomers":"allCustomers_1513853971044",
"allCitiesCustomers":"allCitiesCustomers_1513853971044",
"outZoneCustomers":"OutZoneCustomers_1513853971044"*/
private String allCustomers,allCitiesCustomers,outZoneCustomers,city;


            public String getCity() {
                return city;
            }

            public String getAllCustomers() {
                return allCustomers;
            }

            public String getAllCitiesCustomers() {
                return allCitiesCustomers;
            }

            public String getOutZoneCustomers() {
                return outZoneCustomers;
            }
        }

    }


}
