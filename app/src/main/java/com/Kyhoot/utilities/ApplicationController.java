package com.Kyhoot.utilities;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.Kyhoot.models.HomeFragmentModel;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;


/**
 * <h>ApplicationController</h>
 * Created by Ali on 28/09/17.
 */

public class ApplicationController extends Application implements Application.ActivityLifecycleCallbacks
{
    public static final String TAG = ApplicationController.class.getSimpleName();
    private static ApplicationController mInstance;
    /**
     * Arrays for the secret chats dTag message received
     */

    HomeFragmentModel hModel;
    String clientId;
    private MqttManegr mqttManegr;
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions;
    private IMqttActionListener listener;
    // private SessionManager sessionManager;

    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;

        mInstance.sethModel(new HomeFragmentModel());
        mqttManegr = new MqttManegr(mInstance.gethModel());
        SharedPrefs  sharedPrefs=new SharedPrefs(this);
        TimezoneMapper timezoneMapper=new TimezoneMapper();
        if(!sharedPrefs.getSplashLatitude().equals("")){
            VariableConstant.SPLASHLAT=Double.parseDouble(sharedPrefs.getSplashLatitude());
            VariableConstant.SPLASHLONG=Double.parseDouble(sharedPrefs.getSplashLongitude());
            Utilities.getTimeZoneFromLocation();
        }
        callMqttListnr();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void callMqttListnr() {
        listener = new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {

                updatePresence(1, false);
                Log.d("log31",clientId);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        subscribe(clientId);
                    }
                },1000);
                Log.d(TAG, "onSuccess: myqtt client " + asyncActionToken.isComplete());
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                if(exception!=null ){
                    exception.printStackTrace();
                    Log.d(TAG, "onFailure: myqtt client " + asyncActionToken.isComplete() + " Exception " + exception.getMessage());
                }

            }
        };
    }

    private void subscribe(String clientId)
    {
        ApplicationController.getInstance().subscribeToTopic(MqttEvents.Provider.value + "/" + clientId, 1);
        //  ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + clientId, 1);
        ApplicationController.getInstance().subscribeToTopic(MqttEvents.Message.value + "/" + clientId, 2);
    }

    @SuppressWarnings("unchecked")
    public void createMQttConnection(String clientId)
    {
        String serverUri = VariableConstant.HOST;
        this.clientId = clientId;
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        MemoryPersistence persistence = new MemoryPersistence();
        mqttAndroidClient = new MqttAndroidClient(mInstance, serverUri, clientId+android_id,persistence);//
        Log.d(TAG, "createMQttConnection: clientid:"+clientId+" serverUri:"+serverUri);
        mqttAndroidClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                Log.d(TAG, "connectionLost: ");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                /*
                 * To parse the result of the message received on a MQtt topic
                 */
                JSONObject obj = convertMessageToJsonObject(message);
                // obj.put("topic",topic);
                Log.d(TAG, "messageArrived: " + topic);
                mqttManegr.managerMqtt(obj, topic);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setKeepAliveInterval(10);

        /*
         * Has been removed from here to avoid the reace condition for the mqtt connection with the mqtt broker
         */
        connectMqttClient();

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void publish(String topicName, JSONObject obj, int qos, boolean retained) {

        try {
            mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);

        } catch (MqttException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private JSONObject convertMessageToJsonObject(MqttMessage message) {

        JSONObject obj = new JSONObject();
        try {

            obj = new JSONObject(new String(message.getPayload()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }


    private void connectMqttClient() {
        try {
            mqttAndroidClient.connect(mqttConnectOptions, mInstance, listener);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void subscribeToTopic(String topic, int qos) {

        try {
            if (mqttAndroidClient != null) {
                Log.d(TAG, "subscribeToTopic: " + topic);
                mqttAndroidClient.subscribe(topic, qos);
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    /**
     * @param topic Topic name from which to  unsubscribe
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    public void unsubscribeToTopic(String topic) {

        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient.unsubscribe(topic);
            }
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }

    }


    public void updatePresence(int status, boolean applicationKilled) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("id", 1);
            obj.put("status", "");
            publish(MqttEvents.PresenceTopic.value + "/" + "", obj, 0, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * <h1>isMqttConnected</h1>
     *
     * @return boolean value depending on whther currently can publish or not
     */

    public boolean isMqttConnected() {

        Log.d(TAG, "isMqttConnected: "+mqttAndroidClient);
        if(mqttAndroidClient!=null && mqttAndroidClient.isConnected())
            return true;
        else
            return false;
        // return mqttAndroidClient != null && mqttAndroidClient.isConnected();


    }

    /**
     * To disconnect the MQtt client on app being killed
     */

    public void disconnect(String clientId) {

        try {
            if (mqttAndroidClient != null) {
                unsubscribeToTopic(MqttEvents.Provider.value + "/" + clientId);
                unsubscribeToTopic(MqttEvents.JobStatus.value + "/" + clientId);
                if (mqttAndroidClient != null)
                {
                    mqttAndroidClient.disconnect();
                    mqttAndroidClient = null;
                }


            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
    }


    public HomeFragmentModel gethModel() {
        return hModel;
    }

    public void sethModel(HomeFragmentModel hModel) {
        this.hModel = hModel;
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.d(TAG, "onActivityCreated: ");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        Log.d(TAG, "onActivityDestroyed: ");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.d(TAG, "onActivityPaused: ");

    }


    @Override
    public void onActivityResumed(Activity activity) {
        Log.d(TAG, "onActivityResumed: ");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity,
                                            Bundle outState) {
        Log.d(TAG, "onActivitySaveInstanceState: ");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.d(TAG, "onActivityStarted: ");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.d(TAG, "onActivityStopped: ");
    }

//    /**
//     * <h>updateReconnected</h>
//     * Callback when reconnection is made
//     */
//    public void updateReconnected() {
//        updatePresence(1, false);
//    }

}

