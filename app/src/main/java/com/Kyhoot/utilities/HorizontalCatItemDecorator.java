package com.Kyhoot.utilities;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ${3Embed} on 26/3/18.
 */

public class HorizontalCatItemDecorator extends RecyclerView.ItemDecoration{

        private final int horizontalSpace;

        public HorizontalCatItemDecorator(int horizontalSpace) {
            this.horizontalSpace = horizontalSpace;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            /*outRect.right = horizontalSpace;
            outRect.bottom = horizontalSpace;*/
            outRect.left = horizontalSpace;
            outRect.top = horizontalSpace;
        }
}
