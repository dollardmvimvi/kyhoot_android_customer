package com.Kyhoot.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.Support_Subcat_pojo;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

public class SupportChild extends AppCompatActivity {

    String title;
    AppTypeface typeFace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_child);
        initialize();
        initializeToolBar();
    }


    /*
    initialize toolBar
     */
    private void initializeToolBar() {
        Toolbar login_tool = findViewById(R.id.toolBarChild);
        setSupportActionBar(login_tool);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(getString(R.string.supportFragment));
        tv_center.setTypeface(typeFace.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay_still, R.anim.side_slide_in);
    }

    private void initialize() {
        typeFace = AppTypeface.getInstance(this);
        ArrayList<Support_Subcat_pojo> subCat = (ArrayList<Support_Subcat_pojo>) getIntent().getSerializableExtra("SUB_CAT");
        title = getIntent().getStringExtra("Title");
        TextView tvSupportTittleChild = findViewById(R.id.tvSupportTittleChild);
        tvSupportTittleChild.setTypeface(typeFace.getHind_semiBold());
        tvSupportTittleChild.setText(title);
        RecyclerView mListVHelp = findViewById(R.id.mListVHelp);
        LinearLayoutManager llManger = new LinearLayoutManager(this);
        mListVHelp.setLayoutManager(llManger);
        SupportChildAdapter supportChildAdapter = new SupportChildAdapter(SupportChild.this, subCat);
        mListVHelp.setAdapter(supportChildAdapter);

    }

    private class SupportChildAdapter extends RecyclerView.Adapter {
        Activity mcontext;
        ArrayList<Support_Subcat_pojo> subCat;

        public SupportChildAdapter(Activity supportChild, ArrayList<Support_Subcat_pojo> subCat) {
            mcontext = supportChild;
            this.subCat = subCat;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View viewhld = LayoutInflater.from(mcontext).inflate(R.layout.support_list_row, parent, false);
            return new ViewHoldr(viewhld);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHoldr viewHldr = (ViewHoldr) holder;
            viewHldr.list_row_text.setText(subCat.get(position).getName());
            viewHldr.rlSupport.setOnClickListener(onClicked(position));
        }

        private View.OnClickListener onClicked(final int position) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, WebViewActivity.class);
                    intent.putExtra("Link", subCat.get(position).getLink());
                    intent.putExtra("Title", subCat.get(position).getName());
                    intent.putExtra("COMINFROM", mcontext.getString(R.string.supportFragment));
                    mcontext.startActivity(intent);
                    overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                }
            };
        }

        @Override
        public int getItemCount() {
            return subCat.size();
        }
    }

    class ViewHoldr extends RecyclerView.ViewHolder {
        TextView list_row_text;
        RelativeLayout rlSupport;

        public ViewHoldr(View itemView) {
            super(itemView);
            list_row_text = itemView.findViewById(R.id.list_row_text);
            rlSupport = itemView.findViewById(R.id.rlSupport);
        }
    }

}
