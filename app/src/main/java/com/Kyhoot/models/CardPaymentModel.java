package com.Kyhoot.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.pojoResponce.CardDetailsResp;
import com.Kyhoot.pojoResponce.CardInfoPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.GetCard_pojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * <h>CardPaymentModel</h>
 * this model is to check the validity of the card and call api
 * Created by ${Ali} on 8/19/2017.
 */

public class CardPaymentModel {

    private UtilInterFace resListnr;

    public void addCardService(JSONObject jsonObject, SharedPrefs manager, final UtilInterFace resListnr) {
        Log.d("TAG", "addCardService: " + jsonObject);
        this.resListnr = resListnr;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "card", OkHttpConnection.Request_type.POST, jsonObject, manager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result) {
                Log.d("TAGCARD", "onSuccess: " + result + " headerResponse " + headerResponse);
                calladdcardresponce(result);
            }

            @Override
            public void onError(String headerError, String error) {
                Gson gson = new Gson();
                ErrorHandel errorHandel;
                try{
                    errorHandel = gson.fromJson(error, ErrorHandel.class);
                    resListnr.onError(errorHandel.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                    resListnr.onError(e.getMessage());
                }


            }
        });

    }

    private void calladdcardresponce(String result) {
        try {
            Gson gson = new Gson();
            GetCard_pojo response = gson.fromJson(result, GetCard_pojo.class);

            resListnr.onSuccess(response.getErrMsg());

        } catch (Exception e) {

            resListnr.onError(e + "");
        }
    }

    public void getPaymentCard(final VerifyOtpIntrface.PaymentInterFace cardInterface, SharedPrefs sharedPrefs, final Context mcontext) {
        final Gson gson = new Gson();
        //http://api2.0.iserve.ind.in/card
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "card",
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        Log.d("TAG", "onSuccessPAYMENT: "+result);
                        callGetCardServiceResponse(result, gson, mcontext, cardInterface);
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try {
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                            cardInterface.onError(errorHandel.getMessage());
                        } catch (Exception e) {
                            cardInterface.onError(e.getMessage());
                        }

                    }
                });
    }

    /**
     * This method is used to perform the work, that we will do after getting the response from server.
     */
    private void callGetCardServiceResponse(String Response, Gson gson, Context mcontext, VerifyOtpIntrface.PaymentInterFace cardInterface) {

        try {
            CardDetailsResp response = gson.fromJson(Response, CardDetailsResp.class);

            if (response.getData().size() > 0) {
                cardInterface.onClearList();
                for (int i = 0; i < response.getData().size(); i++) {
                    CardDetailsResp.CardDetail cardDetls = response.getData().get(i);
                    Bitmap bitmap = Utilities.setCreditCardLogo(cardDetls.getBrand(), mcontext);
                    CardInfoPojo item = new CardInfoPojo(bitmap, cardDetls.getName(), cardDetls.getLast4(),
                            cardDetls.getId(), cardDetls.getBrand(), cardDetls.getFunding(),
                            cardDetls.getExpYear(),cardDetls.getExpMonth(),cardDetls.isDefault());
                    cardInterface.onResponceItem(item);
                }
            } else {
                cardInterface.onClearList();
                cardInterface.onError(response.getMessage());
            }

        } catch (Exception e) {
            cardInterface.onError(e.toString());
        }
    }

    public Bundle createBundle(CardInfoPojo row_details) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        row_details.getCardImage().compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        String expiry;
        if(row_details.getExpiryMonth()<10)
            expiry = "0"+row_details.getExpiryMonth()+"/"+row_details.getExpiryYear();
        else
            expiry = row_details.getExpiryMonth()+"/"+row_details.getExpiryYear();
        Bundle bundle = new Bundle();
        bundle.putString("NUM", row_details.getLast4());
        bundle.putString("EXP", expiry);
        bundle.putString("ID", row_details.getId());
        bundle.putString("NAM", row_details.getName());
        bundle.putBoolean("DFLT", row_details.isDefault());
        bundle.putByteArray("IMG", byteArray);
        return bundle;
    }

}
