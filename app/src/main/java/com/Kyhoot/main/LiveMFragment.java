package com.Kyhoot.main;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.BuildConfig;
import com.Kyhoot.R;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

/**
 * <h>LiveMFragment</h>
 * Created by ${Ali} on 8/16/2017.
 */

public class LiveMFragment extends Fragment implements View.OnClickListener
{
    AlertProgress aProgres;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_livem,container,false);
        initializeView(view);
        return view;

    }
    /*
    initializing the view
     */

    private void initializeView(View view) {
        AppTypeface apptypeFace = AppTypeface.getInstance(getActivity());
        aProgres = new AlertProgress(getActivity());
        RelativeLayout rlLiveMLegal = view.findViewById(R.id.rlLiveMLegal);
        RelativeLayout rlLiveMRate = view.findViewById(R.id.rlLiveMRate);
        RelativeLayout rlLiveMFaceBookLike = view.findViewById(R.id.rlLiveMFaceBookLike);

        rlLiveMLegal.setOnClickListener(this);
        rlLiveMRate.setOnClickListener(this);
        rlLiveMFaceBookLike.setOnClickListener(this);
        TextView tvLiveMRate = view.findViewById(R.id.tvLiveMRate);
        TextView tvLiveMLike = view.findViewById(R.id.tvLiveMLike);
        TextView tvLiveMLegal = view.findViewById(R.id.tvLiveMLegal);
        TextView tvLiveMMusic = view.findViewById(R.id.tvLiveMMusic);
        TextView tvLiveMWebLink = view.findViewById(R.id.tvLiveMWebLink);
        TextView tvLiveMAppVersion = view.findViewById(R.id.tvLiveMAppVersion);
        tvLiveMRate.setTypeface(apptypeFace.getHind_regular());
        tvLiveMLike.setTypeface(apptypeFace.getHind_regular());
        tvLiveMLegal.setTypeface(apptypeFace.getHind_regular());
        tvLiveMMusic.setTypeface(apptypeFace.getHind_regular());
        tvLiveMWebLink.setTypeface(apptypeFace.getHind_regular());
        tvLiveMAppVersion.setTypeface(apptypeFace.getHind_regular());
        tvLiveMAppVersion.setText(BuildConfig.VERSION_NAME);
        tvLiveMWebLink.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlLiveMRate:
                rateUsOnGoogle();
                break;
            case R.id.rlLiveMLegal:
                legalTermsConditn();
                break;
            case R.id.tvLiveMWebLink:
                if (Utilities.isNetworkAvailable(getActivity()))
                {
                    try {
                        String url = VariableConstant.WEBSITE_LINK;
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    aProgres.showNetworkAlert();
                }
                break;
            default:
                likeusOnFb();
                break;

        }
    }

    private void likeusOnFb() {
        if (aProgres.isNetworkAvailable()) {
            String url = getString(R.string.facebook_link);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else {
            aProgres.showNetworkAlert();
        }
    }

    /**
     * <h1>legalTermsConditn</h1>
     * calling the terms and condition activity
     */
    private void legalTermsConditn() {
        Intent intent = new Intent(getActivity(), TermsActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.side_slide_in, R.anim.side_slide_out);
    }

    /*
    Rating the app on the google play store
     */
    private void rateUsOnGoogle() {
        if (aProgres.isNetworkAvailable()) {
            Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
            }
        } else {
            aProgres.showNetworkAlert();
        }
    }
}
