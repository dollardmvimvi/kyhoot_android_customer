package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 23/4/18.
 */
public class WalletData {
    private String currencySymbol;

    private String walletAmount;

    private String currencyAbbr;

    private double softLimit;

    private boolean reachedSoftLimit;

    private boolean enableWallet;

    private boolean reachedHardLimit;

    private String currency;

    private double hardLimit;

    public String getCurrencySymbol ()
    {
        return currencySymbol;
    }

    public String getWalletAmount ()
    {
        return walletAmount;
    }

    public String getCurrencyAbbr ()
    {
        return currencyAbbr;
    }

    public double getSoftLimit ()
    {
        return softLimit;
    }

    public boolean getReachedSoftLimit ()
    {
        return reachedSoftLimit;
    }

    public boolean getEnableWallet ()
    {
        return enableWallet;
    }

    public boolean getReachedHardLimit ()
    {
        return reachedHardLimit;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public double getHardLimit ()
    {
        return hardLimit;
    }
}
