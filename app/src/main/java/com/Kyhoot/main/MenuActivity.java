package com.Kyhoot.main;



import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.controllers.ShareControllr;
import com.Kyhoot.main.wallet.WalletActivity;
import com.Kyhoot.pojoResponce.MyEventStatus;
import com.Kyhoot.pojoResponce.MyEventStatusObservable;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.CustomTypefaceSpan;
import com.Kyhoot.utilities.LocationUtil;
import com.Kyhoot.utilities.MqttEvents;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.livechatinc.inappchat.ChatWindowActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.URL;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MenuActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener
{
    private String TAG = "MainAct";
    private Toolbar toolBarMainAct;
    private SharedPrefs sessionMgr;
    private long backPressed;
    private DrawerLayout drawer;
    private TextView tvAbarFragTitle;
    private AlertProgress aProgress;
    private TextView tvUserName;
    private ImageView ivProfilePic;
    private ShareControllr shareContrlr;
    private AppTypeface appTypeface;

    private HomeFragment homeFrag;
    private MyEventsFrag myEnventFrag;
    private PaymentFragment paymentFrag;
    private ReviewFragment reviewFrag;
    private AddressFragment addressFrag;
    private FaqFragment faqFrag;
    private ShareFragment shareFrag;
    private HelpFragment helpFrag;
    private LiveMFragment liveMFrag;
    private FavProviderFragment favProviderFragment;
    private ProfileFragment profileFrag;

    private FragmentTransaction fTransaction;
    NavigationView navView;
    FragmentManager fragmentManager;
    boolean isHomeOnTop=false;
    private String currentTag="";
    private Observer<MyEventStatus> observer;
    private CompositeDisposable disposable=new CompositeDisposable();
    private long bId;
    private String proProfilePic = "";

    private int bookStatus = 0;
    private int bookingModel = 0;
    //**************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate ");
        Log.i(TAG, "onCreateActivi ");
        appTypeface = AppTypeface.getInstance(this);
        Utilities.verifyAndSetRTL(this, "1");
        setContentView(R.layout.activity_main);
        initToolBar();
        initializeObj();
        initializeListener();
        initNavigationView();
        connectToJobStatusMqtt();
    }

    private void initializeObj() {
        sessionMgr = new SharedPrefs(this);
        aProgress = new AlertProgress(this);
        shareContrlr = new ShareControllr(this, aProgress);

        homeFrag = new HomeFragment();
        paymentFrag = new PaymentFragment();
        reviewFrag = new ReviewFragment();
        addressFrag = new AddressFragment();
        faqFrag = new FaqFragment();
        favProviderFragment=new FavProviderFragment();
        shareFrag = new ShareFragment();
        helpFrag = new HelpFragment();
        liveMFrag = new LiveMFragment();
        myEnventFrag = new MyEventsFrag();
        profileFrag = new ProfileFragment();

        if(!ApplicationController.getInstance().isMqttConnected())
        {
            ApplicationController.getInstance().createMQttConnection(sessionMgr.getCustomerSid());
        }
        if (getIntent().getExtras() != null)
        {
            Bundle extras = getIntent().getExtras();
            bId = extras.getLong("BID", 0);
            bookStatus = extras.getInt("STATUS", 0);
            proProfilePic = extras.getString("ImageUrl");
            Log.e("TAG", "initializeLIVEBOOKING: "+bId +" pic "+proProfilePic);
            if(bId!=0 && bookStatus==11 && VariableConstant.BOOKINGTYPEMODEL==1){
                alertNoProvider(bId);
            }
        }


        /*if (bookStatus==11) {
            toolBarMainAct.setVisibility(View.VISIBLE);
            loadFragment(myEnventFrag, getString(R.string.myeventsFrag), "MYEVENT");
        }*/
        if (VariableConstant.isConfirmBook) {
            toolBarMainAct.setVisibility(View.VISIBLE);
            loadFragment(myEnventFrag, getString(R.string.myeventsFrag), "MYEVENT");
        }else  if (VariableConstant.toHelpFrag)
        {
            toolBarMainAct.setVisibility(View.GONE);
            loadFragment(helpFrag, "", "HELP");
        }
        else {
            toolBarMainAct.setVisibility(View.GONE);
            loadFragment(homeFrag, "", "HOME");
        }


    }

    private void initializeListener() {

        //Intialising observer to get status of the booking done from the dispatcher on booking unassigned from dispatcher..
        //If booking status is one the the booking is done from the dispatcher and updated on the eventsfragment screen
        observer=new Observer<MyEventStatus>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable.add(d);
            }

            @Override
            public void onNext(MyEventStatus myEventStatus) {

                Log.d(TAG, "onNextLive: " + myEventStatus.getData().getStatus());
                final MyEventStatus myEvent = myEventStatus;
                if(bookStatus==myEventStatus.getData().getStatus() && myEventStatus.getData().getStatus()!=8){
                    //Checking 8 as the status is used many times
                    //when job is paused and started
                    return;
                }
                bookStatus = myEventStatus.getData().getStatus();
                Log.i("message","status-->"+myEventStatus.getData().getStatus());
                bookingModel = myEventStatus.getData().getBookingModel();
                Log.i("message","status-->"+bookingModel);

                switch (myEventStatus.getData().getStatus())
                {

                    case 1://Status 1 New booking from dispatcher
                    case 15://Status 15 Unassign booking from dispatcher
                        if(myEnventFrag!=null && myEnventFrag.isAdded())
                            myEnventFrag.callService();
                        else
                            loadFragment(myEnventFrag,getResources().getString(R.string.myeventsFrag),"MYEVENT");
                        break;

                }

                //  setProgressLine(bookStatus);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
        MyEventStatusObservable.getInstance().subscribe(observer);
        MyEventStatusObservable.getInstance().subscribeOn(Schedulers.io());
    }

    public void alertNoProvider(long bookingId)
    {

        AlertDialog.Builder builder=new AlertDialog.Builder(MenuActivity.this);
        builder.setMessage("Booking cancelled by the provider ");
        builder.setPositiveButton(" OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                VariableConstant.SCHEDULEAVAILABLE = true;
                dialog.dismiss();
                if (VariableConstant.SCHEDULEAVAILABLE)
                {
                    AlertDialog.Builder builder2=new AlertDialog.Builder(MenuActivity.this);
                    builder2.setMessage(getString(R.string.book_another_provider));
                    builder2.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(aProgress.isNetworkAvailable()) {

                                triggerNewScheduleBooking(bookingId);
                                VariableConstant.SCHEDULEAVAILABLE = false;
                            } else {
                                aProgress.showNetworkAlert();
                            }
                            dialog.dismiss();
                        }
                    }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder2.create().show();

                }
            }
        });
        builder.create().show();
    }



    private void setProgressLine(int bookStatus)
    {
    }

    private void connectToJobStatusMqtt() {
        if(ApplicationController.getInstance().isMqttConnected()){
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + sessionMgr.getCustomerSid(), 1);
            Log.d("SHIJENTEST", "onResume: subscribing "+MqttEvents.JobStatus.value + "/" + sessionMgr.getCustomerSid());
        } else {
            ApplicationController.getInstance().createMQttConnection(sessionMgr.getCustomerSid());
            toSubscribe();
        }
    }

    private void toSubscribe()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("SHIJENTEST", "run: "+MqttEvents.JobStatus.value + "/" + sessionMgr.getCustomerSid());
                ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + sessionMgr.getCustomerSid(), 1);
            }
        },1000);
    }





    //********************************************************/

    private void initToolBar()
    {
        if(getActionBar() != null)
        {
            getActionBar().hide();
        }

        toolBarMainAct = findViewById(R.id.rlAbarMainAct);
        setSupportActionBar(toolBarMainAct);
        final ActionBar aBar = getSupportActionBar();
        if(aBar != null)
        {
            aBar.setHomeAsUpIndicator(R.drawable.ic_launcher);
            aBar.setDisplayShowTitleEnabled(false);
            aBar.setDisplayHomeAsUpEnabled(true);
        }

        tvAbarFragTitle = findViewById(R.id.tvAbarTitle);
        tvAbarFragTitle.setTypeface(appTypeface.getHind_semiBold());
    }
    //**********************************************************/

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
    //**********************************************************/

    private void initNavigationView()
    {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolBarMainAct, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_login_back_icon_off, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    //drawer.openDrawer(GravityCompat.START);
                    popBackstack();
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navView = findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);
        View headerView = navView.getHeaderView(0);
        Menu m = navView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            SpannableString mNewTitle = new SpannableString(mi.getTitle());
            mNewTitle.setSpan(new CustomTypefaceSpan("", appTypeface.getHind_regular()), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mi.setTitle(mNewTitle);
        }
        ivProfilePic = headerView.findViewById(R.id.ivProfilePic);
        setProfilePic();

        tvUserName = headerView.findViewById(R.id.tvUserName);
        TextView tvViewProf = headerView.findViewById(R.id.tvViewProf);
        String name = sessionMgr.getFirstname() + " " + sessionMgr.getLastname();
        tvUserName.setText(name);
        tvUserName.setTypeface(appTypeface.getHind_semiBold());
        tvViewProf.setTypeface(appTypeface.getHind_regular());

        headerView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                toolBarMainAct.setVisibility(View.GONE);
                loadFragment(profileFrag, "", "PROFILE");


                drawer.closeDrawer(GravityCompat.START);
            }
        });
    }

    public void setProfilePic() {
        String profilePicUrl = "";
        if (sessionMgr.getImageUrl().contains("https")) {
            profilePicUrl = sessionMgr.getImageUrl();
        }
        if (!profilePicUrl.isEmpty()) {
            URL myUrl;
            try {
                myUrl = new URL(profilePicUrl.trim());
                double size[] = Scaler.getScalingFactor(this);
                double height = (120) * size[1];
                double width = (120) * size[0];
                Picasso.with(this) //

                        .load(myUrl.toExternalForm().replace(" ", "%20"))
                        .centerCrop()
                        .transform(new CircleTransform())
                        .resize((int) width, (int) height)
                        .into(ivProfilePic);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    //**********************************************************/

    /**
     * This method is used to set the data on Header View.
     */
    public void setHeaderView() {
        if (sessionMgr.getImageUrl() != null && !sessionMgr.getImageUrl().equals("")) {
            try {
                String url = sessionMgr.getImageUrl().replace(" ", "%20");
                if (!url.equals("")) {
                    double size[] = Scaler.getScalingFactor(this);
                    double height = (120) * size[1];
                    double width = (120) * size[0];
                    Picasso.with(MenuActivity.this).load(url)
                            .resize((int) width, (int) height)
                            .transform(new CircleTransform())
                            //  .placeholder(R.drawable.default_userpic)
                            .into(ivProfilePic);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is used to set the name on Header View.
     */
    public void setHeaderName() {
        String name = sessionMgr.getFirstname() + " " + sessionMgr.getLastname();
        tvUserName.setText(name);
    }

    //**********************************************************/

    public void toggleDrawer() {
        // Drawer State checking
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            drawer.openDrawer(GravityCompat.START);
            Utilities.hideKeyboard(this);
        }


    }
    /********************************************************/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.navHome:
                toolBarMainAct.setVisibility(View.GONE);
                loadFragment(homeFrag, "", "HOME");
                break;

            case R.id.navMyEvent:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(myEnventFrag, getString(R.string.myeventsFrag), "MYEVENT");
                homeFrag.onPause();
                break;

            case R.id.navPayment:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(paymentFrag, getString(R.string.paymentMethodFrag), "PAYMENT");
                homeFrag.onPause();
                break;

            case R.id.navWallet:
                Intent intent=new Intent(this, WalletActivity.class);
                startActivity(intent);
                break;

            case R.id.navFavProvider:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(favProviderFragment,getString(R.string.favProviderFrag), "FAVPROVIDER");
                homeFrag.onPause();
                break;

            case R.id.navAddress:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(addressFrag, getString(R.string.youraddressFrag), "ADDRESS");
                homeFrag.onPause();
                break;
            case R.id.navFAQ:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(faqFrag, getString(R.string.supportFragment), "FAQ");
                homeFrag.onPause();
                break;
            case R.id.navShare:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(shareFrag, getString(R.string.shareFrag), "SHARE");
                homeFrag.onPause();
                break;
            case R.id.navLivechat:
                callLiveSupport(sessionMgr.getFirstname(),sessionMgr.getEMail());
                homeFrag.onPause();
                break;
            /*case R.id.navHelp:
                toolBarMainAct.setVisibility(View.GONE);
                loadFragment(helpFrag, "", "HELP");
                homeFrag.onPause();
                break;*/

            case R.id.navLiveM:
                toolBarMainAct.setVisibility(View.VISIBLE);
                loadFragment(liveMFrag, getString(R.string.LiveMFrag), "LIVEM");
                homeFrag.onPause();
                break;
            default:
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //**********************************************************/

    private void loadFragment(Fragment fragment1, String title, String TAG)
    {
        currentTag=TAG;
        Log.d(TAG, "loadFragment: "+TAG);
        fragmentManager = getSupportFragmentManager();
        fTransaction = fragmentManager.beginTransaction();
        // fTransaction.addToBackStack(TAG);
        tvAbarFragTitle.setText(title);
        callHomeFrag(TAG, fTransaction);
    }

    public void callLiveSupport(String uname,String email) {
        //ChatWindowSupportFragment.newInstance(LICENSE_KEY, getString(R.string.app_name)), "chat_fragment")
        final String LICENSE_KEY = "11890674";//4711811 8926529
        final String GROUP_ID = MenuActivity.this.getString(R.string.app_name);
        Intent intent = new Intent(MenuActivity.this, com.livechatinc.inappchat.ChatWindowActivity.class);
        intent.putExtra(com.livechatinc.inappchat.ChatWindowActivity.KEY_GROUP_ID, GROUP_ID);
        intent.putExtra(com.livechatinc.inappchat.ChatWindowActivity.KEY_LICENCE_NUMBER, LICENSE_KEY);
        intent.putExtra(ChatWindowActivity.KEY_VISITOR_NAME, uname);
        intent.putExtra(ChatWindowActivity.KEY_VISITOR_EMAIL, email);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    //********************************************************/

    @Override
    public void onBackPressed()
    {
        if(isHomeOnTop){
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START))
            {
                drawer.closeDrawer(GravityCompat.START);
            }
            else
            {
                if(homeFrag!=null)
                {
                    if(homeFrag.listScreen){
                        if(homeFrag.categoryPage){
                            homeFrag.showMapView();
                        }else{
                            if(homeFrag.listFromBookNow){
                                homeFrag.showMapView();
                                homeFrag.listFromBookNow=false;
                            }else{
                                homeFrag.showCategoryPage();
                            }
                        }
                    }else{
                        if(homeFrag.singleBuilderReturn!=null)
                        {
                            if(homeFrag.singleBuilderReturn.isDisplay())
                                homeFrag.singleBuilderReturn.close();
                            else
                            {
                                if(backPressed+2000>System.currentTimeMillis())
                                {
                                    super.onBackPressed();
                                }
                                else
                                {
                                    Toast.makeText(getBaseContext(), getResources().getString(R.string.double_press_exit), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        else
                        {
                            if(backPressed+2000>System.currentTimeMillis())
                            {
                                super.onBackPressed();
                            }
                            else
                            {
                                Toast.makeText(getBaseContext(), getResources().getString(R.string.double_press_exit), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                else
                {
                    if(backPressed+2000>System.currentTimeMillis())
                    {
                        super.onBackPressed();
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.double_press_exit), Toast.LENGTH_SHORT).show();
                    }
                }


            }
            backPressed = System.currentTimeMillis();

        }else{
            popBackstack();
        }

    }
    //********************************************************/

    @Override
    public void onStop()
    {
        super.onStop();
    }
    //*****************************************************/

    @Override
    protected void onDestroy()
    {
        MyEventStatusObservable.getInstance().removeObserver(observer);
        disposable.clear();
        super.onDestroy();
    }
    //*****************************************************/

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState)
    {
        super.onSaveInstanceState(outState, outPersistentState);
    }
    //*****************************************************/


    @Override
    protected void onRestart()
    {
        Log.i(TAG, "onRestart ");
        super.onRestart();
    }
    //********************************************************/

    @Override
    protected void onStart()
    {
        Log.i(TAG, "onStart ");
        super.onStart();
    }
    //**********************************************************/


    /**
     * <h>onShareClicked</h>
     * onclick of the icon to share code through
     *
     * @param view this is the view of the sharefragment's share Icon to be clicked to share your code
     */

    public void onShareClicked(View view) {
        RelativeLayout rlonclikd = (RelativeLayout) view;
        String rlOnClicked = rlonclikd.getTag().toString();
        Log.e(TAG, "onShareClicked: "+rlOnClicked);
        shareContrlr.performOnClikdAction(Integer.parseInt(rlOnClicked), sessionMgr.getReferral());

    }
    //********************************************************/

    private void callHomeFrag(String TAG, FragmentTransaction fTransaction)
    {
        Intent intent;
        switch (TAG)
        {
            case "HOME":
                toolBarMainAct.setVisibility(View.GONE);
                isHomeOnTop=true;
                if (paymentFrag.isAdded()) {
                    fTransaction.hide(paymentFrag);
                }
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (homeFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    homeFrag.makeVariableValue();
                    homeFrag.onResume();
                    fTransaction.show(homeFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, homeFrag, TAG);
                    //fTransaction.addToBackStack(TAG);
                }
                break;
            case "MYEVENT":
                isHomeOnTop=false;
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (myEnventFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(myEnventFrag);
                    fTransaction.addToBackStack("HOME");

                    myEnventFrag.onResume();
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, myEnventFrag, TAG);
                    fTransaction.addToBackStack("HOME");
                }
                break;
            case "PAYMENT":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (paymentFrag.isAdded()) {
                    CheckAddedFrag("", "isPayment");
                    fTransaction.show(paymentFrag);
                } else {
                    CheckAddedFrag("", "isPayment");
                    fTransaction.add(R.id.frameLayoutMainAct, paymentFrag, TAG);
                }
                break;
            case "REVIEW":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (reviewFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(reviewFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, reviewFrag, TAG);
                }
                break;
            case "ADDRESS":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (addressFrag.isAdded()) {
                    CheckAddedFrag("IsAddress", "");
                    fTransaction.show(addressFrag);
                } else {
                    CheckAddedFrag("IsAddress", "");
                    fTransaction.add(R.id.frameLayoutMainAct, addressFrag, TAG);
                }
                break;
            case "FAQ":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (faqFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(faqFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, faqFrag, TAG);
                }
                break;
            case "SHARE":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (shareFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(shareFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, shareFrag, TAG);
                }
                break;
            case "HELP":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (helpFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(helpFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, helpFrag, TAG);
                }
                break;
            case "FAVPROVIDER":
                isHomeOnTop=false;
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(favProviderFragment);
                    favProviderFragment.onResume();
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, favProviderFragment, TAG);
                }
                break;
            case "LIVEM":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (liveMFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(liveMFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, liveMFrag, TAG);
                }
                break;
            case "PROFILE":
                isHomeOnTop=false;
                if (myEnventFrag.isAdded())
                    fTransaction.hide(myEnventFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (profileFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(profileFrag);
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, profileFrag, TAG);
                }
                break;
        }
        fTransaction.commit();
    }

    private void CheckAddedFrag(String isAddress, String isPayment) {

        if (!isAddress.equals("")) {
            VariableConstant.isAddressCalled = true;
            VariableConstant.isReviewCalled = false;
            VariableConstant.isPaymentCalled = false;
        } else if (!isPayment.equals("")) {
            VariableConstant.isAddressCalled = false;
            VariableConstant.isPaymentCalled = true;
            VariableConstant.isReviewCalled = false;

        } else {
            VariableConstant.isAddressCalled = false;
            VariableConstant.isReviewCalled = false;
            VariableConstant.isPaymentCalled = false;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utilities.checkAndShowNetworkError(this);
    }

    public void popBackstack() {
        //  fragmentManager.popBackStack();
        loadFragment(homeFrag, "", "HOME");
    }

    public void updateProfilePic() {
        homeFrag.updateProfilePic();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        Log.e(TAG, "onRequestPermissionsResult: Permission test");
        boolean isDenine = false;
        switch (requestCode)
        {
            case 1:
                int i = 0;
                for (; i < grantResults.length; i++)
                {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    {
                        isDenine = true;
                        break;
                    }

                }
                if (isDenine)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(getResources().getString(R.string.ohuhavedenied)+" "+homeFrag.myPermissionLocationArrayList.get(i)+" "+getResources().getString(R.string.pleaseexitapptogivepermission))
                            .setTitle(getResources().getString(R.string.permissionrejected));
                    builder.setPositiveButton(getResources().getString(R.string.exitapp), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //finish();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    if(!isFinishing())
                        dialog.show();
                }
                else
                {
                    homeFrag.networkUtilObj = new LocationUtil(this, homeFrag);
                    if (!homeFrag.networkUtilObj.isGoogleApiConnected())
                    {
                        homeFrag.networkUtilObj.checkLocationSettings();
                        homeFrag.isLocationUpdated=true;
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void triggerNewScheduleBooking(long bookingId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "bookingByBookingId",
                OkHttpConnection.Request_type.POST, jsonObject, sessionMgr.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                            // Toast.makeText(MenuActivity.this, "Menu Activity", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "onSuccess: "+result);
                            VariableConstant.SHOWPOPUP = false;

                            toolBarMainAct.setVisibility(View.VISIBLE);
                            loadFragment(myEnventFrag, getString(R.string.myeventsFrag), "MYEVENT");


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }


    @Override
    protected void onResume()
    {

        super.onResume();
        connectToJobStatusMqtt();
    }

    public void loadMyEvent()
    {
        toolBarMainAct.setVisibility(View.VISIBLE);
        loadFragment(myEnventFrag, getString(R.string.myeventsFrag), "MYEVENT");

    }
}

