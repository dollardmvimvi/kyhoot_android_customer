package com.Kyhoot.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Validator;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassword extends AppCompatActivity {

    private AlertProgress alertProgress;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        validator = new Validator();
        intialize();
    }

    private void intialize() {
        AppTypeface appTypeface = AppTypeface.getInstance(this);
        alertProgress = new AlertProgress(this);
        final SharedPrefs sharedPrefs = new SharedPrefs(this);

        TextView tvHeader = findViewById(R.id.tvHeader);
        final EditText etConfirmPass = findViewById(R.id.etConfirmPass);
        final EditText etPassword = findViewById(R.id.etPassword);
        final EditText etoldpass = findViewById(R.id.etoldpass);
        TextView submit = findViewById(R.id.submit);

        Toolbar toolbar = findViewById(R.id.toolbarChange);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_login_back_icon_off);
            getSupportActionBar().setTitle("");
            TextView tv_center = findViewById(R.id.tv_center);
            tv_center.setText(R.string.changePasswrd);
            tv_center.setTypeface(appTypeface.getHind_semiBold());
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        /*if(comingFrom.equals("Profile"))
        {

            tvHeader.setText(getResources().getString(R.string.changeuapassword));
            etPassword.setVisibility(View.VISIBLE);
            etoldpass.setVisibility(View.VISIBLE);
        }
        else
        {
            actionbartext.setText(getResources().getString(R.string.forgotpassword));
            tvHeader.setText(getResources().getString(R.string.thankyouenternewpass));
            etPassword.setVisibility(View.VISIBLE);
            etoldpass.setVisibility(View.GONE);
        }*/


      /*  if(comingFrom.equals("Profile"))
        {*/
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etoldpass.getText().toString().isEmpty()) {
                    if (etoldpass.getText().toString().equals(sharedPrefs.getOldpassword())) {
                        if (!etPassword.getText().toString().isEmpty()) {
                            if(validator.passStatus(etPassword.getText().toString().trim()))
                            {
                                if (!etConfirmPass.getText().toString().trim().isEmpty()) {

                                    if(validator.passStatus(etConfirmPass.getText().toString().trim()))
                                    {
                                        if (etPassword.getText().toString().equals(etConfirmPass.getText().toString())) {
                                            submitPass(sharedPrefs, etoldpass, etConfirmPass);
                                        } else {
                                            alertProgress.alertinfo(getResources().getString(R.string.pasNotMatch));
                                        }
                                    }
                                    else
                                    {
                                        alertProgress.alertinfo(getResources().getString(R.string.passwordcontain));
                                    }


                                } else {
                                    alertProgress.alertinfo(getResources().getString(R.string.reEnteredpass));
                                }
                            }
                            else {
                                alertProgress.alertinfo(getResources().getString(R.string.passwordcontain));
                            }

                        }
                        else {
                            alertProgress.alertinfo(getResources().getString(R.string.passwordShouldNotBeEmpty));
                        }
                    } else {
                        alertProgress.alertinfo(getResources().getString(R.string.pasNotMatchOld));
                    }

                } else {
                    alertProgress.alertinfo(getResources().getString(R.string.passwordShouldNotBeEmpty));

                }

            }
        });
        // }


        tvHeader.setTypeface(appTypeface.getHind_light());
        etPassword.setTypeface(appTypeface.getHind_regular());
        etConfirmPass.setTypeface(appTypeface.getHind_regular());
        etoldpass.setTypeface(appTypeface.getHind_regular());
        submit.setTypeface(appTypeface.getHind_semiBold());
    }

    private void submitPass(SharedPrefs sharedPrefs, EditText etoldpass, EditText etConfirmPass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("oldPassword", etoldpass.getText().toString());
            jsonObject.put("newPassword", etConfirmPass.getText().toString());
            Log.i("TAG", "params to login " + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "password/me",
                OkHttpConnection.Request_type.PATCH, jsonObject, sharedPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        alertProgress.alertPostivionclick(getString(R.string.passwordreset), new DialogInterfaceListner() {
                            @Override
                            public void dialogClick(boolean isClicked) {
                                onBackPressed();
                            }
                        });
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        //Utility.hideInputMethod(this,getCurrentFocus());
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
    }
}
