package com.Kyhoot.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.AdditionalService;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

public class CompletedInvoiceInfo extends AppCompatActivity {

    private AppTypeface appTypeface;
    private long bid;
    private String imageurl;
    private ArrayList<AllEventsDataPojo> pendingEventPojo;
    private double discountFee;
    private String currencySymbol;
    private TextView tvTotalAmt, tvVisitFeeAmt, tvDiscountAmount,tvCategory,tvCategoryName
            ,tvYourNotary,tv_travelFeeAmt,tv_lastdues,tv_lastdues_Amt,tvWallet,tvWalletAmt,tvRemainingAmt;
    private LinearLayout llLiveFee;
    private RelativeLayout rlconfm_visitfee,rlLastdues,rlWalletLayout,rltravel_fee,rldiscount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_invoice_info);
        appTypeface = AppTypeface.getInstance(this);
        getIntentValue();
        initializeToolBar();
        initializeView();
    }

    private void getIntentValue() {
        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            bid = extras.getLong("BID", 0);
            imageurl = extras.getString("ImageUrl");
            pendingEventPojo = (ArrayList<AllEventsDataPojo>) extras.getSerializable("ALLPOJO");
            Log.d("TAG", "getIntentView: " + pendingEventPojo.get(0).getStatus()
                    + " BID " + bid + " BID1 " + pendingEventPojo.get(0).getBookingId());
            Log.d("TAG", "getIntentValue: "+pendingEventPojo.get(0).getCurrencySymbol());
            currencySymbol = pendingEventPojo.get(0).getCurrencySymbol();
        }
    }

    private void initializeView() {
        discountFee = 0;
        double size[] = Scaler.getScalingFactor(this);
        double width = 65 * size[0];
        double height = 65 * size[1];
        TextView tvCompletePrice = findViewById(R.id.tvCompletePrice);
        TextView tvCompletePricePaid = findViewById(R.id.tvCompletePricePaid);
        ImageView ivCompltProProfile = findViewById(R.id.ivCompltProProfile);
        ImageView ivCompleteCardLogo = findViewById(R.id.ivCompleteCardLogo);
        tvCategory=findViewById(R.id.tvCategory);
        tvCategoryName=findViewById(R.id.tvCategoryName);
        TextView tvCompleteName = findViewById(R.id.tvCompleteName);
        rlWalletLayout = findViewById(R.id.rlWalletLayout);
        TextView tvCompleteYourrate = findViewById(R.id.tvCompleteYourrate);
        TextView tvCompleterate = findViewById(R.id.tvCompleterate);
        RatingBar ratingCompleteStar = findViewById(R.id.ratingCompleteStar);
        tv_travelFeeAmt = findViewById(R.id.tv_travelFeeAmt);
        TextView tv_travelFee = findViewById(R.id.tv_travelFee);
        TextView tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        TextView tvDistance=findViewById(R.id.tvDistance);
        rltravel_fee=findViewById(R.id.rltravel_fee);
        tvYourNotary=findViewById(R.id.tvYourNotary);
        tvWallet=findViewById(R.id.tvWallet);
        tvRemainingAmt=findViewById(R.id.tvRemainingAmt);
        tvWalletAmt=findViewById(R.id.tvWalletAmt);
        tvYourNotary.setTypeface(appTypeface.getHind_semiBold());
        tvWalletAmt.setTypeface(appTypeface.getHind_regular());
        tvRemainingAmt.setTypeface(appTypeface.getHind_regular());
        tvDistance.setTypeface(appTypeface.getHind_regular());
        tvDistance.setText(pendingEventPojo.get(0).getAccounting().getTotalDistanceTravelText());
        //TextView tvCompleteEventName = findViewById(R.id.tvCompleteEventName);
        //TextView tvCompleteEventType = findViewById(R.id.tvCompleteEventType);
        TextView tvCompleteEventLocInfo = findViewById(R.id.tvCompleteEventLocInfo);
        TextView tvCompleteEventLoc = findViewById(R.id.tvCompleteEventLoc);
        TextView tvCompleteCard = findViewById(R.id.tvCompleteCard);
        rlLastdues = findViewById(R.id.rlLastdues);
        rldiscount=findViewById(R.id.rldiscount);
        tv_lastdues = findViewById(R.id.tv_lastdues);
        tv_lastdues_Amt = findViewById(R.id.tv_lastdues_Amt);
        tvCompletePrice.setTypeface(appTypeface.getHind_bold());
        Utilities.setAmtOnRecept(pendingEventPojo.get(0).getTotalAmount(),tvCompletePrice,currencySymbol);
        tvCompletePricePaid.setTypeface(appTypeface.getHind_regular());
        tv_travelFee.setTypeface(appTypeface.getHind_regular());
        tvCategoryName.setTypeface(appTypeface.getHind_regular());
        tvCategory.setTypeface(appTypeface.getHind_regular());
        tv_travelFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvCompleteName.setTypeface(appTypeface.getHind_semiBold());
        String name = pendingEventPojo.get(0).getFirstName() + " " + pendingEventPojo.get(0).getLastName();
        tvCompleteName.setText(name);
        tvCategoryName.setText(pendingEventPojo.get(0).getCart().getCategoryName());
        //tvCompleteEventName.setText(pendingEventPojo.get(0).getTypeofEvent().getName());
        tvCompleteEventLoc.setText(pendingEventPojo.get(0).getAddLine1());
        if(pendingEventPojo.get(0).getPaymentMethod().equalsIgnoreCase("cash")) {
            ivCompleteCardLogo.setImageBitmap(Utilities.getBitmapFromVectorDrawable(this, R.drawable.ic_002_notes));
            if(pendingEventPojo.get(0).getAccounting().getPaidByWallet()==0){
                tvCompleteCard.setText(pendingEventPojo.get(0).getPaymentMethod());
            }else{
                rlWalletLayout.setVisibility(View.VISIBLE);
                tvRemainingAmt.setVisibility(View.VISIBLE);
                tvCompleteCard.setText(pendingEventPojo.get(0).getPaymentMethod());
                tvRemainingAmt.setText( Utilities.getAmtForRecept(pendingEventPojo.get(0).getAccounting().getRemainingAmount(),pendingEventPojo.get(0).getCurrencySymbol()));
                tvWalletAmt.setText(Utilities.getAmtForRecept(pendingEventPojo.get(0).getAccounting().getCaptureAmount(),pendingEventPojo.get(0).getCurrencySymbol()));
            }

        }else {
            ivCompleteCardLogo.setImageBitmap(Utilities.setCreditCardLogo(pendingEventPojo.get(0).getAccounting().getBrand(),this));
            String cardEndWith = pendingEventPojo.get(0).getPaymentMethod() + " "+getString(R.string.endsWith)+" "+pendingEventPojo.get(0).getAccounting().getLast4();
            if(pendingEventPojo.get(0).getAccounting().getPaidByWallet()==0){
                tvCompleteCard.setText(cardEndWith);
            }else{
                rlWalletLayout.setVisibility(View.VISIBLE);
                tvRemainingAmt.setVisibility(View.VISIBLE);
                tvCompleteCard.setText(pendingEventPojo.get(0).getPaymentMethod());
                tvRemainingAmt.setText( Utilities.getAmtForRecept(pendingEventPojo.get(0).getAccounting().getRemainingAmount(),pendingEventPojo.get(0).getCurrencySymbol()));
                tvWalletAmt.setText(Utilities.getAmtForRecept(pendingEventPojo.get(0).getAccounting().getCaptureAmount(),pendingEventPojo.get(0).getCurrencySymbol()));
            }
        }
        tvCompleteYourrate.setTypeface(appTypeface.getHind_regular());
        ratingCompleteStar.setIsIndicator(true);
        tvCompleterate.setTypeface(appTypeface.getHind_regular());
        float rating = 0;
        if(pendingEventPojo.get(0).getReviewByProvider()!=null)
             rating = pendingEventPojo.get(0).getReviewByProvider().getRating();
        ratingCompleteStar.setRating(rating);
        tvCompleterate.setText(rating+"");
        //tvCompleteEventName.setTypeface(appTypeface.getHind_semiBold());
        //tvCompleteEventType.setTypeface(appTypeface.getHind_regular());
        tvCompleteEventLocInfo.setTypeface(appTypeface.getHind_semiBold());
        tvCompleteEventLoc.setTypeface(appTypeface.getHind_regular());
        tvCompleteCard.setTypeface(appTypeface.getHind_semiBold());
        TextView tvPaymentBreak = findViewById(R.id.tvPaymentBreak);
        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        tvVisitFeeAmt = findViewById(R.id.tvVisitFeeAmt);
        llLiveFee = findViewById(R.id.llLiveFee);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        tvDiscountAmount = findViewById(R.id.tvDiscountAmount);
        TextView tvTotal = findViewById(R.id.tvTotal);
        tvTotalAmt = findViewById(R.id.tvTotalAmt);
        rlconfm_visitfee = findViewById(R.id.rlconfm_visitfee);
        tvPaymentBreak.setText(R.string.detailsCaps);
        tvPaymentMethod.setTypeface(appTypeface.getHind_semiBold());
        tvPaymentBreak.setTypeface(appTypeface.getHind_semiBold());
        tvVisitFee.setTypeface(appTypeface.getHind_regular());
        tvVisitFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvDiscount.setTypeface(appTypeface.getHind_regular());
        tvDiscountAmount.setTypeface(appTypeface.getHind_regular());
        tvTotal.setTypeface(appTypeface.getHind_bold());
        tvTotalAmt.setTypeface(appTypeface.getHind_bold());
        if (!imageurl.equals("") && imageurl != null) {
            Picasso.with(this)
                    .load(imageurl)
                    .resize((int) width, (int) height)
                    .transform(new CircleTransform())
                    .into(ivCompltProProfile);
        }
        //todo need to check
        //inflateLayout(llLiveFee);
        setPaymentBreakDown();

    }

    private void setPaymentBreakDown() {
        //Adding servicess
        int size=pendingEventPojo.get(0).getCart().getCheckOutItem().size();
        for(int i=0;i<size;i++){
            Item item=pendingEventPojo.get(0).getCart().getCheckOutItem().get(i);
            View infltedView = LayoutInflater.from(CompletedInvoiceInfo.this).inflate(R.layout.single_service_price, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit;
            if(pendingEventPojo.get(0).getAccounting().getServiceType()==2){
                int accJobhr= (int) (pendingEventPojo.get(0).getAccounting().getTotalActualJobTimeMinutes()/60);
                int accJobMin= (int) (pendingEventPojo.get(0).getAccounting().getTotalActualJobTimeMinutes()%60);
                String actual_hrs;
                if(accJobMin>0) {
                    actual_hrs = accJobhr + " hr " + accJobMin + " min";
                }else{
                    actual_hrs = accJobhr + " hr " ;
                }
                namePerUnit=item.getServiceName() + " (x" + actual_hrs+")";
                Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getTotalActualHourFee(),tvServicePrice,pendingEventPojo.get(0).getCart().getCurrencySymbol());
            }else{
                if(item.getServiceName().trim().isEmpty()){
                    namePerUnit="Hourly"+" (x" + item.getQuntity()+")";
                }else{
                    namePerUnit = item.getServiceName() + " (x" + item.getQuntity()+")";
                }
                Utilities.setAmtOnRecept(item.getAmount(),tvServicePrice,pendingEventPojo.get(0).getCart().getCurrencySymbol());
            }
            tvServiceName.setText(namePerUnit);
            discountFee=pendingEventPojo.get(0).getAccounting().getDiscount();
            tvServiceName.setTypeface(appTypeface.getHind_regular());
            tvServicePrice.setTypeface(appTypeface.getHind_regular());
        }
        //Adding Additional service if any
        size=pendingEventPojo.get(0).getAdditionalService().size();
        for(int i=0;i<size;i++){
            AdditionalService item=pendingEventPojo.get(0).getAdditionalService().get(i);
            View infltedView = LayoutInflater.from(CompletedInvoiceInfo.this).inflate(R.layout.single_service_price, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit = item.getServiceName();
            tvServiceName.setText(namePerUnit);
            tvServicePrice.setText(pendingEventPojo.get(0).getCurrencySymbol()+" "+item.getPrice());
            tvServiceName.setTypeface(appTypeface.getHind_regular());
            tvServicePrice.setTypeface(appTypeface.getHind_regular());
        }

        setTotalAmount();

    }

    private void setTotalAmount() {

        if(pendingEventPojo.get(0).getAccounting().getVisitFee()>0){
            rlconfm_visitfee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getVisitFee(),tvVisitFeeAmt,currencySymbol);
        }
        if(pendingEventPojo.get(0).getAccounting().getDiscount()>0){
            rldiscount.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getDiscount(),tvDiscountAmount,currencySymbol);
        }else{
            rldiscount.setVisibility(View.GONE);
        }

        if(pendingEventPojo.get(0).getAccounting().getTravelFee()>0){
            rltravel_fee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getTravelFee(),tv_travelFeeAmt,currencySymbol);
        }else{
            rltravel_fee.setVisibility(View.GONE);
        }

        Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getTotal(),tvTotalAmt,currencySymbol);
        if(pendingEventPojo.get(0).getAccounting().getLastDues()>0){
            rlLastdues.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getLastDues(),tv_lastdues_Amt,currencySymbol);
        }
    }

    private void initializeToolBar() {
        Toolbar login_tool = findViewById(R.id.toolBarComplete);
        setSupportActionBar(login_tool);
        TextView tvEventCompleteId = findViewById(R.id.tvEventCompleteId);
        TextView tvCompleteDate = findViewById(R.id.tvCompleteDate);
        getSupportActionBar().setTitle("");
        tvEventCompleteId.setTypeface(appTypeface.getHind_regular());
        tvCompleteDate.setTypeface(appTypeface.getHind_medium());
        String eventId = "bid: " + bid;
        tvEventCompleteId.setText(eventId);
        timeMethod(tvCompleteDate);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void timeMethod(TextView tvDate) {

        try {


            Log.d("TAGTIME", " expireTime " + pendingEventPojo.get(0).getBookingRequestedFor());
            Date date = new Date(pendingEventPojo.get(0).getBookingRequestedFor() * 1000L);

            String formattedDate = Utilities.getFormattedDate(date);

            // String splitDate[] = formattedDate.split(" ");

            String dat[] = formattedDate.split(",");

            tvDate.setText(dat[0]);
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        //overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
        overridePendingTransition(R.anim.stay_still, R.anim.side_slide_in);;
    }
}
