package com.Kyhoot.pojoResponce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Pramod
 * @since 07-May-2019.
 */

public class SurgePriceData implements Serializable{

    @SerializedName("surgePrice")
    @Expose
    private double surgePrice;

    public double getSurgePrice() {
        return surgePrice;
    }

    public void setSurgePrice(double surgePrice) {
        this.surgePrice = surgePrice;
    }

    @Override
    public String toString() {
        return "SurgePriceData{" +
                "surgePrice=" + surgePrice +
                '}';
    }
}
