package com.Kyhoot.main.wallet.wallet_transaction_activity;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.pojoResponce.OldWalletTransPojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

public class WalletTransactionActivityPresenter implements WalletTransactionContract.WalletTransactionPresenter
{

    private final String TAG = "WalletTransProvider";

    SharedPrefs preferenceHelperDataSource;
    Context mContext;
    WalletTransactionContract.WalletTrasactionView trasactionView;

    Gson gson;


    WalletTransactionActivityPresenter(WalletTransactionContract.WalletTrasactionView view,Context context)
    {
        this.mContext=context;
        trasactionView=view;
        gson=new Gson();
        preferenceHelperDataSource=new SharedPrefs(context);
    }

    /**
     * <h2>initLoadTransactions</h2>
     * <p> method to init the getTransactionsHistory() api call if network connectivity is there </p>
     * @param isToLoadMore: true if is from load more option
     * @param isFromOnRefresh: true if it is to refresh
     */
    public void initLoadTransactions(boolean isToLoadMore, boolean isFromOnRefresh)
    {
//        if( networkStateHolder.isConnected())
//        {
        if(!isFromOnRefresh)
        {
//                trasactionView.showProgressDialog(mContext.getString(R.string.pleaseWait));
            getTransactionHistory();
        }

//        }
//        else
//        {
//            trasactionView.noInternetAlert();
//        }
    }


    @Override
    public void showToastNotifier(String msg, int duration)
    {
        trasactionView.showToast(msg, duration);
    }

    private void getTransactionHistory()
    {
        if (Utilities.isNetworkAvailable(mContext)) {
            Log.d(TAG, "getWalletTransaction: " + VariableConstant.SERVICE_URL + "wallet/transction/0 :: Session: " + preferenceHelperDataSource.getSession() + " : " + VariableConstant.SelLang);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "wallet/transction/0", OkHttpConnection.Request_type.GET, new JSONObject(), preferenceHelperDataSource.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String value) {
                    Log.d(TAG, " getWalletLimits onNext: " + headerResponse);
                    switch (headerResponse) {
                        case "200":
                            String response = value;
                            Log.d(TAG, " getWalletTrans onNext: " + response);
                            handleResponse(response);
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onError(String headerError, String error) {
                    //pDialog.dismiss();
                    /*if (headerError.equals("502"))
                        Toast.makeText(mContext, mContext.getString(R.string.serverProblm), Toast.LENGTH_LONG).show();
                    else {
                        try{
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    prodataResponce.sessionExpired(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccesToken(errorHandel.getData());
                                    break;
                                default:
                                    Toast.makeText(mcontext, errorHandel.getMessage(), Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    Log.d("TAG", "HomeFrag JSON DATA Error" + error + " onAMp " + headerError);*/
                    trasactionView.hideProgressDialog();
                    Log.d(TAG, "getWalletLimits error: " + headerError + ":" + error);
                }

            });
        }
    }



    /**
     * <h>Response Handler</h>
     * <p>this method is using to  handle the Server Response</p>
     * @param response server response
     */
    private void handleResponse(String response)
    {
        OldWalletTransPojo oldWalletTransPojo=null;
        try{
            oldWalletTransPojo = gson.fromJson(response, OldWalletTransPojo.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        trasactionView.setAllTransactionsAL(oldWalletTransPojo.getData().getCreditDebitArr());
        trasactionView.setCreditTransactionsAL(oldWalletTransPojo.getData().getCreditArr());
        trasactionView.setDebitTransactionsAL(oldWalletTransPojo.getData().getDebitArr());
        trasactionView.walletTransactionsApiSuccessViewNotifier();
    }
}