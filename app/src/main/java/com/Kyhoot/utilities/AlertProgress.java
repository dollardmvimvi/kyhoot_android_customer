package com.Kyhoot.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.IPServiceListener;
import com.Kyhoot.pojoResponce.IPServicePojo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * <h>AlertProgress</h>
 * Created by Ali on 8/5/2017.
 */

public class AlertProgress
{
    private static final String TAG = "AlertProgress";
    private Context mcontect;
    private IPServiceListener listeners;
    AlertDialog dialog;
    AlertDialog.Builder alertDialog;
    public static AlertProgress getInstance(Context context){
        return new AlertProgress(context);
    }
    public AlertProgress(Context mcontect)
    {
        this.mcontect  =mcontect;
    }
    public ProgressDialog getProgressDialog(String message)
    {
        final ProgressDialog progressDialog = new ProgressDialog(mcontect,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        return progressDialog;
    }

    /************************************************    for checking internet connection*******/
    public boolean isNetworkAvailable() {

        ConnectivityManager connectivity;
        boolean isNetworkAvail = false;
        try {
            connectivity = (ConnectivityManager)mcontect.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();

                if (info != null) { // connected to the internet
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        // connected to wifi
                        isNetworkAvail = true;
                        return isNetworkAvail;
                    } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                        // connected to the mobile provider's data plan
                        isNetworkAvail = true;
                        return isNetworkAvail;
                    }
                } else {
                    // not connected to the internet
                    isNetworkAvail = false;
                    return isNetworkAvail;
                }

            }
            else
            {
                isNetworkAvail = false;
                return isNetworkAvail;
            }


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return isNetworkAvail;
    }

    /************************************get Ip Address****************************************/

    public AsyncTask<Void, Void, IPServicePojo> IPAddress(IPServiceListener listener) {
        this.listeners=listener;
        return  new IPAddres().execute();
    }

    public void  showNetworkAlert()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontect);

        // Setting Dialog Title
        alertDialog.setTitle(mcontect.getResources().getString(R.string.network_alert_title));
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(mcontect.getResources().getString(R.string.network_alert_message));

        // On pressing Settings button
        alertDialog.setPositiveButton(mcontect.getResources().getString(R.string.action_settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                dialog.dismiss();
                mcontect.startActivity(intent);

            }
        });

        // Showing Alert Message
        if(!((Activity)mcontect).isFinishing())
            alertDialog.show();
    }

    public void alertinfo(String message)
    {
        try
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontect);
            AlertDialog dialog = alertDialog.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            // Setting Dialog Title
            alertDialog.setTitle(mcontect.getResources().getString(R.string.system_error));
            alertDialog.setCancelable(false);
            // Setting Dialog Message
            alertDialog.setMessage(message);
            // On pressing Settings button
            alertDialog.setPositiveButton(mcontect.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            // Showing Alert Message
            alertDialog.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public void alertinfoSingle(String message)
    {
        try
        {
            if(alertDialog==null){
                alertDialog = new AlertDialog.Builder(mcontect);
                // Setting Dialog Title
                alertDialog.setTitle(mcontect.getResources().getString(R.string.system_error));
                alertDialog.setCancelable(false);
                // Setting Dialog Message
                alertDialog.setMessage(message);
                // On pressing Settings button
                alertDialog.setPositiveButton(mcontect.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog=alertDialog.create();
            }
            // Showing Alert Message
            if(dialog!=null && !dialog.isShowing()){
                dialog.show();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void alertPostivionclick(String message,
                                    final DialogInterfaceListner isclick)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mcontect);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        builder.setTitle(mcontect.getResources().getString(R.string.system_error));
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(mcontect.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                dialog.cancel();
                isclick.dialogClick(true);
            }
        });
        builder.show();
    }

    public void alertonclick(String title,String message,String positivTitle,
                             String negativTitle,final DialogInterfaceListner isclick)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mcontect);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positivTitle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                dialog.cancel();
                isclick.dialogClick(true);
            }
        });

        builder.setNegativeButton(negativTitle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
                isclick.dialogClick(false);
            }
        });
        builder.show();
    }

    @SuppressLint("StaticFieldLeak")
    private class IPAddres  extends AsyncTask<Void,Void,IPServicePojo> {
        @Override
        protected IPServicePojo doInBackground(Void... voids) {
            String stringUrl = VariableConstant.IPINFO;
            try {
                URL url = new URL(stringUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                StringBuffer response = new StringBuffer();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                Log.d(TAG, "ipAddress2: "+response.toString());
                Gson gson=new Gson();
                IPServicePojo ipServicePojo = gson.fromJson(response.toString(), IPServicePojo.class);
                return  ipServicePojo;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(IPServicePojo s) {
            super.onPostExecute(s);
            if(s!=null){
                Log.d(TAG, "IPADDRESS onPostExecute: "+s);
                listeners.onIPServiceSuccess(s);
            }else{
                listeners.onIPServiceFailed();
            }

        }
    }
}

