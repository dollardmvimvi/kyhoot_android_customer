package com.Kyhoot.controllers;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateTimeDialogReturn;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.ETAResultInterface;
import com.Kyhoot.interfaceMgr.HomeModelIntrface;
import com.Kyhoot.main.GoogleRoute;
import com.Kyhoot.main.HomeFragment;
import com.Kyhoot.models.HomeFragmentModel;
import com.Kyhoot.pojoResponce.MarkerPojo;
import com.Kyhoot.pojoResponce.MqttProviderData;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.pojoResponce.MqttProvidrResp;
import com.Kyhoot.pojoResponce.Polygons;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.LocationUtil;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

/**
 * <h>HomeFragmentController</h>
 * Created by Ali on 9/29/2017.
 */

public class HomeFragmentController {
    // String TAG = "HomeFragmentController";
    private HomeFragmentModel hModel;
    private HomeModelIntrface inteface;
    private boolean isFirstTime = true;
    private Marker driverMarker;
    private Context mcontext;
    private Geocoder geocoder;
    private GoogleMap googleMap;
    private LatLng latLng;
    private int iTemPosition;
    //private YoutubePlayerView vdVwItm = null;
    private ImageView vdVwItm = null;
    private SharedPrefs sprefs;
    private LatLng previousLatlng = new LatLng(0,0);
    private String markerId;
    private static final String TAG = "HomeFragmentController";
    private double previousDistance=0;
    private Calendar calendar;

    public HomeFragmentController(HomeModelIntrface inteface, SharedPrefs sPrefs,Context context) {
        hModel = ApplicationController.getInstance().gethModel();
        this.inteface = inteface;
        this.sprefs = sPrefs;
        this.mcontext=context;
    }
    public void providerService(double currentLatI, double currentLongI) {
        hModel.callService(currentLatI, currentLongI, sprefs, inteface);
    }



    public void locatnAddress(String[] params, Context mcontext) {
        this.mcontext = mcontext;
        new BackgroundGetAddress().execute(params);
    }

    public String markProviderMarkr(String driverId, GoogleMap mMap, final ImageView ivOffline, String image, LatLng latLngr,
                                    final View marker_view, double width,
                                    double height, int status) {
        googleMap = mMap;
        latLng = latLngr;
        driverMarker = googleMap.addMarker(new MarkerOptions().position(latLng));


        if (!image.equals("")) {
            Bitmap customerMarker = Utilities.createDrawableFromView(mcontext, marker_view);
            // Try Catch the exception for GlDrawEventsException and UnmanagedDescriptor for Bitmap
            // load for the marker image load on home screen
            try {
                driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(customerMarker));

                //  callPicasso(mcontext,width,height,ivOffline,driverMarker,marker_view,image);
                //  callPicasso(mcontext,width,height,ivOffline,driverMarker,marker_view,image);

                Picasso.with(mcontext)
                        .load(image)
                        .resize((int) width, (int) height)
                        .centerCrop()
                        .transform(new CircleTransform())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                Log.e(TAG, "onBitmapLoaded: ");
                                ivOffline.setImageBitmap(bitmap);
                                driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(Utilities.createDrawableFromView(mcontext, marker_view)));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                Log.e(TAG, "onBitmapFailed: ");
                                driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(Utilities.createDrawableFromView(mcontext, marker_view)));

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                Log.e(TAG, "onBitmapPrepareLoad: ");
                                driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(Utilities.createDrawableFromView(mcontext, marker_view)));
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "onBitmap markProviderMarkr: ");
            // Try Catch the exception for GlDrawEventsException and UnmanagedDescriptor for Bitmap load
            // for the marker image load on home screen
            try {
                driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(Utilities.createDrawableFromView(mcontext, marker_view)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        hModel.putDriverMarkerRow(driverId, new MarkerPojo(status, driverMarker, image));

        return driverMarker.getId();
    }

    public void getCategories(double latLong[]){
        hModel.getCategories(mcontext,latLong,sprefs);
    }

    /**
     * @param latC  current location latitude
     * @param longC current location longitude
     * @param mMap  google map object
     */
    public void onCurrentLocation(double latC, double longC, GoogleMap mMap) {
        LatLng latLng;
        if (latC == 0 && longC == 0)
            latLng = new LatLng(Double.parseDouble(sprefs.getSplashLatitude()), Double.parseDouble(sprefs.getSplashLongitude()));
        else
            latLng = new LatLng(latC, longC);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

    }

    public void upDateMapPostion(GoogleMap mMap, ArrayList<MqttProviderDetails> providerDtls, HashMap<String, ArrayList<String>> hashMarker, int position) {
        List<String> markrList = new ArrayList<>(hashMarker.keySet());

        for (int i = 0; i < markrList.size(); i++) {
            if (providerDtls.get(position).getId().equals(hashMarker.get(markrList.get(i)).get(0))) {
                double lat = Double.parseDouble(hashMarker.get(markrList.get(i)).get(2));
                double lag = Double.parseDouble(hashMarker.get(markrList.get(i)).get(3));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lag), 24.0f));
            }
        }
    }

    /**
     * <h1>getFavoriteProviders</h1>
     * <P>
     *     This method is used to get the favorite provider list who are online;
     * </P>
     * @return list of favorite providers WHO ARE ONLINE
     */
    public ArrayList<MqttProviderDetails> getFavoriteProviders() {
        ArrayList<MqttProviderDetails> favoriteProviderList=new ArrayList<>();
        if(!VariableConstant.selectedCategoryId.isEmpty() && !VariableConstant.MQTTRESPONSEPROVIDER.isEmpty()){
            MqttProvidrResp mqttresp;
            try{
                mqttresp = new Gson().fromJson(VariableConstant.MQTTRESPONSEPROVIDER, MqttProvidrResp.class);
                for(int i=0;i<mqttresp.getData().size();i++) {
                    MqttProviderData tempCategoryData = mqttresp.getData().get(i);
                    if (tempCategoryData.getCategoryId().equals(VariableConstant.selectedCategoryId)) {
                        for (int j = 0; j < tempCategoryData.getProviderList().size();j++){
                            MqttProviderDetails tempProviderDetails = tempCategoryData.getProviderList().get(j);
                            if(tempProviderDetails.isFavouriteProvider() && tempProviderDetails.getStatus()==1){
                                favoriteProviderList.add(tempProviderDetails);
                            }
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                return  favoriteProviderList;
            }
        }
        return favoriteProviderList;
    }

    public void onPause(Timer myTimer, LocationUtil networkUtilObj, Timer myTimer_publish) {
        if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
            networkUtilObj.stoppingLocationUpdate();

        if (myTimer_publish != null) {
            myTimer_publish.cancel();
            myTimer_publish.purge();
        }
        if (myTimer != null) {
            myTimer.purge();
            myTimer.cancel();
        }
    }

    public void onDestroy(Timer myTimer, LocationUtil networkUtilObj, Timer myTimer_publish) {
        if (myTimer_publish != null) {
            myTimer_publish.cancel();
            myTimer_publish.purge();
        }
        if (myTimer != null) {
            myTimer.purge();
            myTimer.cancel();
        }
    }

    public void configApi(SharedPrefs sharedPrefs, Context mcontext, AlertProgress aProgres) {

        /* /customer/config*/
        if (aProgres.isNetworkAvailable())
            hModel.callConfigApi(sharedPrefs, mcontext, inteface);

    }
    public void getEta(double srcLat, double srcLng,ETAResultInterface etaResultInterface){
        hModel.setDestinationLatLong(srcLat,srcLng,etaResultInterface);
    }

    public void amimateMarker(LatLng latLng, final Marker proMarker, GoogleMap mMap,boolean isFirstTime)
    {
        if(mMap!=null)
        {
            if(isFirstTime)
            {
                markerId = proMarker.getId();
                previousLatlng = latLng;
            }
            if(!proMarker.getId().equalsIgnoreCase(markerId))
            {
                previousLatlng = latLng;
            }
            final Location mCurrentLoc=new Location("current_location");
            final Location mPreviousLoc=new Location("previous_location");
            mCurrentLoc .setLatitude(latLng.latitude);
            mCurrentLoc .setLongitude( latLng.longitude);
            mPreviousLoc .setLatitude(previousLatlng.latitude);
            mPreviousLoc .setLongitude(previousLatlng.longitude);

            //  final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
            final Projection proj = mMap.getProjection();
            Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(),mPreviousLoc.getLongitude()));
            final LatLng startLatLng = proj.fromScreenLocation(startPoint);
            final long duration = 1000;
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final Interpolator interpolator = new LinearInterpolator();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("LiveBooking","TRACK marker inside run "+proMarker);
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / duration);
                    double lng = t * mCurrentLoc.getLongitude() + (1 - t)
                            * startLatLng.longitude;
                    double lat = t * mCurrentLoc.getLatitude() + (1 - t)
                            * startLatLng.latitude;
                    proMarker.setPosition(new LatLng(lat, lng));
                    proMarker.setAnchor(0.5f, 0.5f);
                    //  proMarker.setRotation(bearing);
                    proMarker.setFlat(true);
                    Log.d("LiveBooking","TRACK marker inside setFlat "+proMarker);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    }
                }
            });
            previousLatlng = latLng;
        }

    }

    /*public void getProviders(double currentLatitude, double currentLongitude, SharedPrefs sharedPrefs, HomeModelIntrface homeFragment) {
        hModel.callGetProvider(currentLatitude,currentLongitude,sharedPrefs,homeFragment);
    }*/
    public void pendingBooking()
    {
        hModel.callPendingBooking(sprefs,inteface);
    }

    public void singlewindowBootomSheet(Context mcontext, SingleDateTimeDialogReturn.Builder singleBuilderReturn, final SimpleDateFormat simpleDateFormat)
    {
        Date now = new Date();
        Calendar calendarMin = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        Calendar calendarMax = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        long systemCurrenttime=System.currentTimeMillis();
        long diff=sprefs.getServerTimeDifference();
        calendarMin.setTime(new Date(systemCurrenttime-diff + TimeUnit.HOURS.toMillis(1))); // Set min now
        calendarMax.setTime(new Date(systemCurrenttime-diff + TimeUnit.DAYS.toMillis(150)));
        Log.d(TAG, "singlewindowBootomSheet: "+calendarMin.getTimeInMillis());
        Date minDate = calendarMin.getTime();
        Date maxDate = calendarMax.getTime();
        Log.d(TAG, "singlewindowBootomSheet: "+minDate.toString()+" Date:"+minDate.getTime() +" TimeZone " +VariableConstant.BOOKINGTIMEZONE);
        Log.d(TAG, "singlewindowBootomSheet: "+maxDate.toString()+" Date:"+maxDate.getTime() +" TimeZone " +VariableConstant.BOOKINGTIMEZONE);
        simpleDateFormat.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
        singleBuilderReturn
                .bottomSheet()
                .curved()
                .backgroundColor(Color.WHITE)
                .mainColor(mcontext.getResources().getColor(R.color.red_login))
                .setTimeZone(VariableConstant.BOOKINGTIMEZONE)
                .minutesStep(15)
                .mustBeOnFuture()
                .minDateRange(minDate)
                .maxDateRange(maxDate)
                .tab1Date(new Date(now.getTime()-diff + TimeUnit.HOURS.toMillis(1)))
                .title("Choose date and time")
                .timeText0("30 min")
                .timeText1("60 min")
                .timeText2("90 min")
                .timeText3("120 min")
                .buttonOkText("Ok")

                .listener(new SingleDateTimeDialogReturn.Listener() {
                    @Override
                    public void onDateSelected(List<Date> dates, String selectedTime) {
                        Log.d("TAG", "onDateSelected: "+selectedTime);

                        String splittime[] = selectedTime.split(" ");
                        VariableConstant.SCHEDULEDTIME = 60;//Double.parseDouble(splittime[0]);

                        final StringBuilder stringBuilder = new StringBuilder();
                        final StringBuilder stringBuilderday = new StringBuilder();
                        Log.d(TAG, "onDateSelected: "+dates.size());
                        for (Date date : dates)
                        {
                            Log.d(TAG, "onDateSelected: "+date);
                            stringBuilderday.append(Utilities.getFormattedDate(date)).append("\n");

                            stringBuilder.append(simpleDateFormat.format(date)).append(":00");
                            // stringBuilderday.append(simpleDateFormatday.format(date)).append("\n");
                        }
                        VariableConstant.BOOKINGTYPE = 2;
                        VariableConstant.SCHEDULEDDATE = stringBuilder.toString();
                        inteface.onScheduledDateTime(stringBuilderday.toString());
                        Log.d(TAG, "onDateSelectedSelected: "+stringBuilder.toString()
                                +" SCHEDULEDATE "+VariableConstant.SCHEDULEDDATE);

                    }
                });
        singleBuilderReturn.display();

        //openDatePicker(minDate,maxDate);

    }

    public void openDatePicker() {
        calendar = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE, Locale.US);
        calendar.setTimeZone(VariableConstant.BOOKINGTIMEZONE);

        Calendar calendarMin = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        Calendar calendarMax = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        long systemCurrenttime=System.currentTimeMillis();
        long diff=sprefs.getServerTimeDifference();
        calendarMin.setTime(new Date(systemCurrenttime-diff + TimeUnit.HOURS.toMillis(1))); // Set min now
        calendarMax.setTime(new Date(systemCurrenttime-diff + TimeUnit.DAYS.toMillis(150)));
        calendar.setTimeInMillis(calendarMin.getTimeInMillis()-10000);
        Log.d(TAG, "singlewindowBootomSheet: "+calendarMin.getTimeInMillis());
        Date minDate = calendarMin.getTime();
        Date maxDate = calendarMax.getTime();
        Log.d(TAG, "singlewindowBootomSheet: "+minDate.toString()+" Date:"+minDate.getTime() +" TimeZone " +VariableConstant.BOOKINGTIMEZONE);
        Log.d(TAG, "singlewindowBootomSheet: "+maxDate.toString()+" Date:"+maxDate.getTime() +" TimeZone " +VariableConstant.BOOKINGTIMEZONE);
        //simpleDateFormat.setTimeZone(VariableConstant.BOOKINGTIMEZONE);

        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mcontext, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year,monthOfYear,dayOfMonth);
                calltimepicker();
            }

        },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        Log.d(TAG, "openDatePicker: maxDate:"+maxDate.getTime()+" MinDate:"+minDate.getTime());
        fromDatePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());
        fromDatePickerDialog.getDatePicker().updateDate(minDate.getYear(),minDate.getMonth(),minDate.getDay());
        fromDatePickerDialog.getDatePicker().setMinDate(minDate.getTime()-1000);
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d(TAG, "onCancel: "+ " Cancelled");
            }
        });
        fromDatePickerDialog.show();
    }
    //End of selecting the date

    private void calltimepicker()
    {
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(mcontext,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar temp=Calendar.getInstance();
                        temp.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
                        temp.set(temp.get(Calendar.YEAR),temp.get(Calendar.MONTH)
                                ,temp.get(Calendar.DAY_OF_MONTH),temp.get(Calendar.HOUR_OF_DAY)+1,temp.get(Calendar.MINUTE));
                        Calendar selectedtime=Calendar.getInstance();
                        selectedtime.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
                        selectedtime.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)
                                ,calendar.get(Calendar.DAY_OF_MONTH),hourOfDay,minute);
                        long currentTimeplus1=temp.getTimeInMillis();
                        //Log.d(TAG, "onTimeSet: currenttimePlus1:"+currentTimeplus1+" selectedtime:"+selectedtime.getTimeInMillis());
                        if(currentTimeplus1>selectedtime.getTimeInMillis()){
                            Toast.makeText(mcontext,"Please select a time at least 1 hr from now",Toast.LENGTH_SHORT).show();
                            calltimepicker();
                            return;
                        }
                        //year month day hour minute
                        calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)
                                ,calendar.get(Calendar.DAY_OF_MONTH),hourOfDay,minute);
                        VariableConstant.BOOKINGTYPE = 2;
                        VariableConstant.SCHEDULEDTIME = 60;
                        VariableConstant.SCHEDULEDDATE=(calendar.getTimeInMillis()/1000)+"";
                        Log.d(TAG, "onTimeSet: ScheduledDate: "+VariableConstant.SCHEDULEDDATE);
                        inteface.onScheduledDateTime(Utilities.getFormattedDateBookingLocation(calendar.getTime()));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d(TAG, "onCancel: time "+"cancelled");
            }
        });
        timePickerDialog.show();

    }

    public double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public void callServicesService() {
        hModel.callServicesService(inteface);
    }

    //Checking whether a lat long is inside a Zone
    public void checkZone(final Polygons polygons, final LatLng mlatLng) {
        ZoneAsyncTask zoneAsyncTask=new ZoneAsyncTask(polygons,mlatLng);
        zoneAsyncTask.execute();
    }

    public void sortProviderByDistance(ArrayList<MqttProviderDetails> providerDtl) {
        Collections.sort(providerDtl, new Comparator<MqttProviderDetails>() {
            @Override
            public int compare(MqttProviderDetails mqttProviderDetails, MqttProviderDetails t1) {
                if(mqttProviderDetails.getDistance()==t1.getDistance())
                    return 0;
                else if(mqttProviderDetails.getDistance()>t1.getDistance())
                    return 1;
                else
                    return -1;
            }
        });
    }

    public void checkDistanceAndUpdateETA(ArrayList<MqttProviderDetails> mqttProviderDetails, double currentLatitude, double currentLongitude, ETAResultInterface etaResultInterface) {
        if(mqttProviderDetails.size()>0 ) {
            double distance = distance(mqttProviderDetails.get(0).getLocation().getLatitude(), mqttProviderDetails.get(0).getLocation().getLatitude(),
                    currentLatitude, currentLongitude);
            if(previousDistance!=distance){
                previousDistance=distance;
                String url = GoogleRoute.makeURL(mqttProviderDetails.get(0).getLocation().getLatitude(), mqttProviderDetails.get(0).getLocation().getLongitude(),
                        currentLatitude, currentLongitude);
                new GoogleRoute.GetEta(url,etaResultInterface).execute();
            }
        }else{
            Log.e(TAG, "checkDistanceAndUpdateETA: No PROVIDERS" );
        }
    }

    public void updateCategoryProviders(SharedPrefs sharedPrefs, HomeFragment homeFragment) {
        hModel.updateCategoryProviders(sharedPrefs,homeFragment);
    }

    //to get the address on the homescreen
    private class BackgroundGetAddress extends AsyncTask<String, Void, String> {
        List<Address> address;
        String lat, lng;

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.d(TAG, "doInBackground: lat:"+lat+" lng:"+lng);
                lat = params[0];
                lng = params[1];

                if (lat != null && lng != null) {
                    if (mcontext != null) {
                        geocoder = new Geocoder(mcontext);
                    }
                    if (geocoder != null) {
                        address = geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if (address != null && address.size() > 0) {
                String fuladdress = address.get(0).getAddressLine(0);
                inteface.onLocationSavedAddress(fuladdress);

            }

        }
    }

    private class ZoneAsyncTask extends AsyncTask<Void, Boolean, Boolean> {
        private final Polygons polygons;
        private final LatLng mlatLng;

        ZoneAsyncTask(Polygons polygons, LatLng mlatLng) {
            this.polygons=polygons;
            this.mlatLng=mlatLng;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean result;
            ArrayList<ArrayList<ArrayList<Double>>> coordinates = polygons.getCoordinates();
            ArrayList<LatLng> latLngList=new ArrayList<>();
            if (coordinates.size() > 0) {
                latLngList.clear();
                for (ArrayList<Double> doubleArrayList : coordinates.get(0)) {
                    latLngList.add(new LatLng(doubleArrayList.get(1), doubleArrayList.get(0)));
                }
            }
            if (coordinates.get(0) != null && coordinates.get(0).size() > 0) {
                result = Utilities.containsLocation(mlatLng.latitude, mlatLng.longitude, latLngList, false);
            } else
                result = false;

            return result;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.e(TAG, "  zoneCheck: "+aBoolean );
            if(!aBoolean){
                double[] loc={mlatLng.latitude, mlatLng.longitude};
                getCategories(loc);
            }
        }
    };
}
