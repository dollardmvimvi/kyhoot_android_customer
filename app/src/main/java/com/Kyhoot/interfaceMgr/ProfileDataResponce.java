package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.GeneralProviderData;

/**
 * <h>ProfileDataResponce</h>
 * Created by Ali on 8/25/2017.
 */

public interface ProfileDataResponce
{
    void successUpdateUi();
    void sessionExpired(String msg);

    interface ProProfileDataResponce {
        void onProSuccessDtls(GeneralProviderData providerDataResp);
        void onSessionExpired(String msg);
        void onError(String errorMsg);
        void onSuccessServicesUpdate(String headerCode,String result);
        void onFailureServicesUpdate(String headerCode,String result);
    }

    interface proProfileSelectedMusic
    {
        void onSelectedMusic(String id);
    }
}
