package com.Kyhoot.main;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.controllers.ProfileController;
import com.Kyhoot.countrypic.Country;
import com.Kyhoot.countrypic.CountryPicker;
import com.Kyhoot.interfaceMgr.ImageUploadedAmazon;
import com.Kyhoot.interfaceMgr.ProfileDataResponce;
import com.Kyhoot.interfaceMgr.ProfilePageCallback;
import com.Kyhoot.models.ProfileModel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.HandlePictureEvents;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.Validator;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * <h>Profile Screen</h>
 * This class is used to provide the Profile screen, where we can show the user pictures and their information details.
 * Created by ${Ali} on 8/16/2017.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener
        , ProfileDataResponce, ProfileDataResponce.proProfileSelectedMusic, ProfilePageCallback {
    private final int REQUEST_CODE_PERMISSION_CAMERAFILE = 1;
    private final int MUSIC_REQUESTCODE = 02;
    View view;
    TextView name;
    Validator validator;
    FragmentTransaction fragmentTransaction;
    DrawerLayout mDrawerLayout;
    boolean imageflag = false;
    String profilePicUrl = "";
    TextInputLayout tilProflNam, tilProflLNam;
    TextView tvProflEml, tvProflMob;
    View viewNm, viewLNm, viewEml, viewMob, viewDob;//
    TextView tvChangePaswd, tvLogout, tv_skip, countryCode, tvProflDob;
    AppTypeface appTypeface;
    private SharedPrefs session;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionCameraFileArrayList;
    private HandlePictureEvents handlePicEvent;
    private ProfileModel profilModel;
    private Context mcontext;
    private String TAG = "ProfileFragment";
    private AlertProgress aprogress;
    private ArrayList<String> musicGeners;
    private boolean isEditable;
    private String aboutm = "", Fname = "";
    private ArrayList<String> musicGenersId;
    /*******************/

    private ImageView ivProflPic, ivAddProflPic, ivProflCalendr, country_code_divider, ivProFLag, ivProEditForwrdMob, ivProEditForwrdEml;
    private EditText etProflNme, etProflLNme;
    private ProfileController profileController;
    private CountryPicker mCountryPicker;
    private ProgressDialog progressDialog;

    /**
     * This is the onCreate method that is called firstly, when user came to login screen.
     *
     * @param savedInstanceState contains an instance of Bundle.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appTypeface = AppTypeface.getInstance(getActivity());
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));
        Country country = mCountryPicker.getUserCountryInfo(getActivity());
        Log.e(TAG, "onCreate: " + country.getFlag());
        initializeObj();
    }

    private void initializeObj() {
        progressDialog = new ProgressDialog(getActivity());
        musicGenersId = new ArrayList<>();
        mcontext = getActivity();
        musicGeners = new ArrayList<>();
        profileController = new ProfileController(mcontext);
        handlePicEvent = new HandlePictureEvents(getActivity(), ProfileFragment.this);
        profilModel = new ProfileModel(getActivity());
        aprogress = new AlertProgress(getActivity());
    }

    /**
     * <p>inflating the view</p>
     *
     * @param inflater           inflate the view
     * @param container          contains the view
     * @param savedInstanceState instance
     * @return the instance of View.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.profile_fragment, container, false);
            initializeToolBar();
            initialize();
            setFocusListnr();
            setListeners();

        } catch (InflateException e) {
            Log.d(TAG, "InflateException: " + e);
        }
        return view;
    }

    private void initializeToolBar() {
        final MenuActivity mainActivity = (MenuActivity) getActivity();

        TextView tv_center = view.findViewById(R.id.tvProfilecenter);
        tv_skip = view.findViewById(R.id.tvProfileSkip);
        tv_skip.setText(R.string.edit);
        tv_skip.setTextColor(getResources().getColor(R.color.red_login));
        ImageView ivProMenu = view.findViewById(R.id.ivProMenu);
        tv_center.setText("");
        ivProMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainActivity != null) {
                    mainActivity.onBackPressed();
                }
            }
        });
        tv_skip.setOnClickListener(this);
    }

    /**
     * <h3>setListeners</h3>
     * This method is used to set listeners on the view
     */
    private void setListeners() {
        ivProflPic.setOnClickListener(this);
        ivAddProflPic.setOnClickListener(this);
        //First time no click for profile pic change
        ivAddProflPic.setClickable(false);
        ivProflPic.setClickable(false);
        //
        ivProflCalendr.setOnClickListener(this);
        viewEml.setOnClickListener(this);
        viewMob.setOnClickListener(this);
        tvProflEml.setOnClickListener(this);
        tvProflMob.setOnClickListener(this);
        tvProflDob.setOnClickListener(this);
        tvChangePaswd.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
    }

    private void setFocusListnr() {
        tvProflDob.setOnFocusChangeListener(this);
    }

    /**
     * <h1>initialize</h1>
     * <p>Intiliazing view elements</p>
     */
    private void initialize() {
        //  tvInvisibleText = view.findViewById(R.id.tvInvisibleText);
        session = new SharedPrefs(mcontext);
        validator = new Validator();
        countryCode = view.findViewById(R.id.countryCode);
        country_code_divider = view.findViewById(R.id.country_code_divider);
        ivProFLag = view.findViewById(R.id.ivProFLag);
        ivProflPic = view.findViewById(R.id.ivProflPic);
        ivProEditForwrdMob = view.findViewById(R.id.ivProEditForwrdMob);
        ivProEditForwrdEml = view.findViewById(R.id.ivProEditForwrdEml);
        ivAddProflPic = view.findViewById(R.id.ivAddProflPic);
        ivProflCalendr = view.findViewById(R.id.ivProflCalendr);
        etProflNme = view.findViewById(R.id.etProflNme);
        etProflLNme = view.findViewById(R.id.etProflLNme);
        tvProflEml = view.findViewById(R.id.tvProflEml);
        tvProflMob = view.findViewById(R.id.tvProflMob);
        tvProflDob = view.findViewById(R.id.tvProflDob);
        tilProflNam = view.findViewById(R.id.tilProflNam);
        tilProflNam.setTypeface(AppTypeface.getInstance(getActivity()).getHind_regular());
        tilProflLNam = view.findViewById(R.id.tilProflLNam);
        tilProflLNam.setTypeface(AppTypeface.getInstance(getActivity()).getHind_regular());
        viewNm = view.findViewById(R.id.viewNm);
        viewLNm = view.findViewById(R.id.viewLNm);
        viewEml = view.findViewById(R.id.viewEml);
        viewMob = view.findViewById(R.id.viewMob);
        viewDob = view.findViewById(R.id.viewDob);
        tvChangePaswd = view.findViewById(R.id.tvChangePaswd);
        tvLogout = view.findViewById(R.id.tvLogout);
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        saveMethod();
        setfontType();

    }

    private void setfontType() {
        TextView tvProEmail = view.findViewById(R.id.tvProEmail);
        TextView tvProNumr = view.findViewById(R.id.tvProNumr);
        TextView tvProfDob = view.findViewById(R.id.tvProfDob);
        etProflNme.setTypeface(appTypeface.getHind_regular());
        etProflLNme.setTypeface(appTypeface.getHind_regular());
        tvProEmail.setTypeface(appTypeface.getHind_regular());
        tvProflEml.setTypeface(appTypeface.getHind_regular());
        tvProNumr.setTypeface(appTypeface.getHind_regular());
        tvProflMob.setTypeface(appTypeface.getHind_regular());
        tvProfDob.setTypeface(appTypeface.getHind_regular());
        tvProflDob.setTypeface(appTypeface.getHind_regular());
        tvChangePaswd.setTypeface(appTypeface.getHind_semiBold());
        tvLogout.setTypeface(appTypeface.getHind_regular());
        tv_skip.setTypeface(appTypeface.getHind_regular());
    }

    /**
     * <h3>onClick()</h3>
     * This is the overridden onClick() method which is used to handle the click events on the view
     *
     * @param v Parameter on which the click event happened
     */
    //TODO: Edit the onclick methods
    @Override
    public void onClick(View v) {
        Intent intnt;
        switch (v.getId()) {
            case R.id.viewEml:
            case R.id.tvProflEml:
                if (isEditable)
                    editProfile(true);
                break;
            case R.id.viewMob:
            case R.id.tvProflMob:
                if (isEditable)
                    editProfile(false);
                break;
            case R.id.ivAddProflPic:
            case R.id.ivProflPic:
                if (ivAddProflPic.isClickable() && ivProflPic.isClickable())
                    checkpermission();
                break;
            case R.id.tvLogout:

                final ProfileDataResponce profileDataResponce = this;
                aprogress.alertonclick(getString(R.string.logout), getResources().getString(R.string.doyouwishtologout), getString(R.string.yes),
                        getString(R.string.no), new DialogInterfaceListner() {
                            @Override
                            public void dialogClick(boolean isClicked) {
                                if (isClicked)
                                    profilModel.logoutService(profileDataResponce);
                            }
                        });
                break;

            case R.id.tvProfileSkip:
                if (tv_skip.getText().toString().equals(mcontext.getResources().getString(R.string.edit)))
                    editMethod();
                else
                    saveMethod();
                break;
            case R.id.tvChangePaswd:
                Intent intent = new Intent(mcontext, ChangePassword.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                break;
        }
    }

    private void editProfile(boolean isEmail) {
        Intent intent = new Intent(mcontext, EditEmailPhone.class);
        intent.putExtra("ISEMAIL", isEmail);
        startActivity(intent);
    }

    private void editMethod() {
        aboutm = "";
        Fname = "";
        tv_skip.setText(mcontext.getString(R.string.save));
        viewNm.setVisibility(View.VISIBLE);
        viewLNm.setVisibility(View.VISIBLE);
        viewEml.setVisibility(View.VISIBLE);
        viewMob.setVisibility(View.VISIBLE);
        viewDob.setVisibility(View.VISIBLE);
        tvChangePaswd.setVisibility(View.GONE);
        tvLogout.setVisibility(View.GONE);
        country_code_divider.setVisibility(View.VISIBLE);
        countryCode.setVisibility(View.VISIBLE);
        ivProFLag.setVisibility(View.VISIBLE);
        ivAddProflPic.setVisibility(View.VISIBLE);
        ivProEditForwrdMob.setVisibility(View.VISIBLE);
        ivProEditForwrdEml.setVisibility(View.VISIBLE);
        ivProflCalendr.setVisibility(View.VISIBLE);

        countryCode.setText(session.getCountryCode());
        Country country = mCountryPicker.getCountryFlagFromSymbol(session.getCountrySymbol());
        Log.e(TAG, "updateUI:  TAG_CHANGE_PH  " + country.getDialCode() + "   ===  " + session.getCountrySymbol() + "  FLAG  " + country.getFlag());
        ivProFLag.setImageResource(country.getFlag());

        etProflNme.setFocusable(true);
        etProflNme.setFocusableInTouchMode(true);
        etProflNme.setClickable(true);
        etProflLNme.setFocusable(true);
        etProflLNme.setFocusableInTouchMode(true);
        etProflLNme.setClickable(true);
        ivAddProflPic.setClickable(true);
        ivProflPic.setClickable(true);
        tvProflMob.setText(session.getMobileNo());
        isEditable = true;
    }

    private void saveMethod() {
        if (mcontext != null && getActivity() != null) {
            Utilities.hideKeyboard(getActivity());
        }

        aboutm = "";
        Fname = "";
        ivAddProflPic.setClickable(false);
        ivProflPic.setClickable(false);
        tv_skip.setText(mcontext.getString(R.string.edit));
        viewNm.setVisibility(View.INVISIBLE);
        viewLNm.setVisibility(View.INVISIBLE);
        viewEml.setVisibility(View.INVISIBLE);
        viewMob.setVisibility(View.INVISIBLE);
        viewDob.setVisibility(View.INVISIBLE);
        tvChangePaswd.setVisibility(View.VISIBLE);
        tvLogout.setVisibility(View.VISIBLE);
        country_code_divider.setVisibility(View.GONE);
        countryCode.setVisibility(View.GONE);
        ivProFLag.setVisibility(View.GONE);
        ivAddProflPic.setVisibility(View.INVISIBLE);
        ivProEditForwrdMob.setVisibility(View.INVISIBLE);
        ivProEditForwrdEml.setVisibility(View.INVISIBLE);
        ivProflCalendr.setVisibility(View.INVISIBLE);
        etProflNme.setFocusable(false);
        etProflNme.setFocusableInTouchMode(false);
        etProflNme.setClickable(false);
        etProflLNme.setFocusable(false);
        etProflLNme.setFocusableInTouchMode(false);
        etProflLNme.setClickable(false);
        etProflNme.setBackgroundColor(Color.TRANSPARENT);
        etProflLNme.setBackgroundColor(Color.TRANSPARENT);
        String fname = etProflNme.getText().toString().trim();
        String lname = etProflLNme.getText().toString().trim();
        if (!fname.equals("") && !fname.equals(session.getFirstname())) {
            profilModel.updateName(true, fname, lname, this);//, etProflAbt,etProflMusicGnr
        } else {
            profilModel.updateName(false, fname, lname, this);//, etProflAbt,etProflMusicGnr
        }
        if (!lname.equals("") && !lname.equals(session.getLastname())) {
            profilModel.updateName(true, fname, lname, this);//, etProflAbt,etProflMusicGnr
        } else {
            profilModel.updateName(false, fname, lname, this);//, etProflAbt,etProflMusicGnr
        }
        String phoneNumer = session.getCountryCode() + "-" + session.getMobileNo();
        tvProflMob.setText(phoneNumer);
        Utilities.hideKeyboard(getActivity());
        isEditable = false;
    }

    private void checkpermission() {

        if (Build.VERSION.SDK_INT >= 23) {
            myPermissionCameraFileArrayList = new ArrayList<>();
            //myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_PHONE_STATE);
            myPermissionCameraFileArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionCameraFileArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

            if (AppPermissionsRunTime.checkPermission(getActivity(), myPermissionCameraFileArrayList, REQUEST_CODE_PERMISSION_CAMERAFILE)) {
                selectImage();
            }
        } else {
            selectImage();
        }
    }

    /**
     * This method is used to open a dialog for asking the current password and then will start operating based on our requirements like, changing name, password, phone or email.
     */
    private void checkCurrentPassword() {
        profileController.openPasswordDialog();
    }

    /**
     * This method is used to open a dialog for asking the current password and then will start operating based on our requirements like, changing name, password, phone or email.
     *
     * @param flag, contains the flag, for checking. 1->Name, 2->Password, 3-> Email, 4-> Phone.
     */
    private void checkPassword(final int flag) {
        Intent intent;
        switch (flag) {
            case 1:
                intent = new Intent(getActivity(), Log.class);//EditNameActivity
                Bundle bundle = new Bundle();
                bundle.putString("ent_name", etProflNme.getText().toString() + " " + etProflLNme.getText().toString());
                // bundle.putString("ent_password", et.getText().toString());
                bundle.putString("ent_email", tvProflEml.getText().toString());
                bundle.putString("ent_mobile", tvProflMob.getText().toString());
                bundle.putString("ent_profile", session.getImageUrl());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 2:
                checkCurrentPassword();
                break;
            case 3:
                intent = new Intent(getActivity(), Log.class);//EditEmailActivity
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(getActivity(), Log.class);//EditPhoneNumberActivity
                startActivity(intent);
                break;
        }
    }

    /**
     * This is onResume() method, which is getting call each time.
     */
    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= 23) {

            if (AppPermissionsRunTime.checkPermission(getActivity(), myPermissionCameraFileArrayList, REQUEST_CODE_PERMISSION_CAMERAFILE))
                callResume();
        } else
            callResume();
    }

    /**
     * This method got called, once we give any permission to our required permission.
     *
     * @param requestCode  contains request code.
     * @param permissions  contains Permission list.
     * @param grantResults contains the grant permission result.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION_CAMERAFILE) {
            boolean isAllGranted = true;
            for (String permission : permissions) {
                if (permission.equals(PackageManager.PERMISSION_GRANTED)) {
                    isAllGranted = false;
                }
            }
            if (!isAllGranted) {
                //  permissionsRunTime.getPermission(permissionArrayList, getActivity(), true);
                AppPermissionsRunTime.checkPermission(getActivity(), myPermissionCameraFileArrayList, REQUEST_CODE_PERMISSION_CAMERAFILE);
            } else {
                callResume();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * This method is used to perform all the task, which we wants to do on our onResume() method.
     */
    private void callResume() {
        if (session.IsProfile()) {
            session.setIsProfile(false);
            profilModel.profileService(this, this);
        } else
            updateUI();
    }

    /**
     * This method is used for updating the UI.
     */
    private void updateUI() {
        String img_url = session.getImageUrl();
        if (!Fname.equals(""))
            etProflNme.setText(Fname);
        else
            etProflNme.setText(session.getFirstname());

        etProflLNme.setText(session.getLastname());
        tvProflEml.setText(session.getEMail());
        if (tv_skip.getText().toString().equals(mcontext.getResources().getString(R.string.edit))) {
            String phoneNumber = session.getCountryCode() + "-" + session.getMobileNo();
            tvProflMob.setText(phoneNumber);
        } else {
            countryCode.setText(session.getCountryCode());
            Country country = mCountryPicker.getCountryFlagFromSymbol(session.getCountrySymbol());
            Log.e(TAG, "updateUI:  TAG_CHANGE_PH  " + country.getDialCode() + "   ===  " + session.getCountrySymbol() + "  FLAG  " + country.getFlag());
            ivProFLag.setImageResource(country.getFlag());
            tvProflMob.setText(session.getMobileNo());
        }

        tvProflDob.setText(session.getDateOBirth());

        double size[] = Scaler.getScalingFactor(getActivity());
        double height = (130) * size[1];
        double width = (130) * size[0];
        if (img_url != null && !img_url.isEmpty() && !img_url.equals(" ")) {
            Picasso.with(getActivity()).load(img_url)
                    .resize((int) width, (int) height)
                    .centerCrop().transform(new CircleTransform())
                    .into(ivProflPic);
        }
        ((MenuActivity) getActivity()).setHeaderName();
    }

    /**
     * <h1>selectImage</h1>
     *
     * @see HandlePictureEvents
     * This mehtod is used to show the popup where we can select our images.
     */
    private void selectImage() {
        handlePicEvent.openDialog();
    }

    /**
     * This is an overrided method, got a call, when an activity opens by StartActivityForResult(), and return something back to its calling activity.
     *
     * @param requestCode returning the request code.
     * @param resultCode  returning the result code.
     * @param data        contains the actual data.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            //result code to check is the result is ok or not
            Fname = etProflNme.getText().toString().trim();
            return;
        }

        switch (requestCode) {
            case MUSIC_REQUESTCODE:
                if (data != null) {
                    musicGeners = data.getStringArrayListExtra("SELECTEDMUSIC");
                    musicGenersId = data.getStringArrayListExtra("SELECTEDMUSICID");
                    String selectedMusic = "";
                    String selectedMusicId = "";
                    for (int i = 0; i < musicGeners.size(); i++) {
                        if (selectedMusic.equals(""))
                            selectedMusic = musicGeners.get(i);
                        else
                            selectedMusic = selectedMusic + ", " + musicGeners.get(i);
                    }
                    for (int id = 0; id < musicGenersId.size(); id++) {
                        if (selectedMusicId.equals(""))
                            selectedMusicId = musicGenersId.get(id);
                        else
                            selectedMusicId = selectedMusicId + "," + musicGenersId.get(id);
                    }
                    session.setMusicGen(selectedMusic);
                    JSONObject json = new JSONObject();
                    try {
                        json.put("preferredGenres", selectedMusicId);
                        if (!etProflNme.getText().toString().trim().equals("")) {
                            session.setFirstname(etProflNme.getText().toString().trim());
                            json.put("firstName", etProflNme.getText().toString().trim());
                        }

                        profilModel.updateProfile(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case VariableConstant.CAMERA_PIC:
                File newFile = handlePicEvent.startCropImage(handlePicEvent.newFile);
                break;
            case VariableConstant.GALLERY_PIC:
                if (data != null)
                    newFile = handlePicEvent.gallery(data.getData());
                break;
            case VariableConstant.CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path != null) {
                    try {
                        File fileExist = new File(path);


                        imageflag = true;
                        /*handlePicEvent.uploadToAmazon(VariableConstant.Amazonbucket+"/"+VariableConstant.AmazonProfileFolderName,fileExist, new ImageUploadedAmazon() {
                            @Override
                            public void onSuccessAdded(String image)
                            {
                                Log.d(TAG, "onSuccessAdded: "+image);
                                profilePicUrl = image;
                                session.setImageUrl(profilePicUrl);
                                ((MenuActivity)getActivity()).setHeaderView();
                                JSONObject json = new JSONObject();
                                try {
                                    json.put("profilePic", profilePicUrl);
                                    profilModel.updateProfile(json);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                ((MenuActivity) getActivity()).updateProfilePic();
                                Picasso.with(getActivity()).load(profilePicUrl)
                                        .resize(ivProflPic.getWidth(), ivProflPic.getHeight())
                                        .centerCrop().transform(new CircleTransform())
                                        .into(ivProflPic);

                            }
                            @Override
                            public void onerror(String errormsg)
                            {
                                Log.d(TAG, "onerror: "+errormsg);
                            }

                        });*/
                        progressDialog.setMessage(getString(R.string.uploading));
                        progressDialog.show();
                        UploadFileToServer fileToServer = new UploadFileToServer();
                        fileToServer.execute(path);

                        Picasso.with(getActivity()).load(fileExist)
                                .resize(ivProflPic.getWidth(), ivProflPic.getHeight())
                                .centerCrop().transform(new CircleTransform())
                                .into(ivProflPic);
                        ivProflPic.setImageURI(Uri.parse(path));
                        ivProflPic.setImageURI(Uri.parse(fileExist.toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                }
        }
    }

    /**
     * This method is used to open/close the drawer.
     */
    public void OpenOrCloseDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void successUpdateUi() {
        updateUI();
    }

    @Override
    public void sessionExpired(String message) {
        Utilities.setMAnagerWithBID(mcontext, session, message);

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public void onSelectedMusic(String id) {
        musicGenersId.add(id);
    }

    @Override
    public void onProfileUpdate() {
        ((MenuActivity) getActivity()).setHeaderName();
    }

    @Override
    public void onProfileUpdateFailed() {

    }

    /**
     * Uploading the file to server
     * Image upload issue in profile fixed
     */
    @SuppressLint("StaticFieldLeak")
    public class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(VariableConstant.CHAT_URL + "uploadImageOnServer")
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d("Shijen", "doInBackground: " + responseCode);
                Log.d("Shijen", "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            progressDialog.dismiss();
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");

                    Log.d(TAG, "onSuccessAdded: "+image);
                    profilePicUrl = image;
                    session.setImageUrl(profilePicUrl);
                    ((MenuActivity)getActivity()).setHeaderView();

                    ((MenuActivity) getActivity()).updateProfilePic();
                    Picasso.with(getActivity()).load(profilePicUrl)
                            .resize(ivProflPic.getWidth(), ivProflPic.getHeight())
                            .centerCrop().transform(new CircleTransform())
                            .into(ivProflPic);


                    JSONObject json = new JSONObject();
                    try {
                        json.put("profilePic", image);
                        profilModel.updateProfile(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }

    }
}
