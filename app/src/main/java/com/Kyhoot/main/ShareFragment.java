package com.Kyhoot.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.ReferralCodeResponsePojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

/**
 * <h>ShareFragment</h>
 * Created by ${Ali} on 8/16/2017.
 */

public class ShareFragment extends Fragment
{
    TextView tvShareCodeShare;
    private SharedPrefs prefs;
    private static final String TAG = "ShareFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_share,container,false);
        initializeView(view);
        return view;
    }

    private void initializeView(View view) {
        AppTypeface appTypeface = AppTypeface.getInstance(getActivity());
        tvShareCodeShare = view.findViewById(R.id.tvShareCodeShare);
        SharedPrefs sharedPrefs = new SharedPrefs(getActivity());
        tvShareCodeShare.setText(sharedPrefs.getReferral());
        tvShareCodeShare.setTypeface(appTypeface.getHind_regular());

        TextView tvShare = view.findViewById(R.id.tvShare);
        tvShare.setTypeface(appTypeface.getHind_regular());
        TextView tvShareCode = view.findViewById(R.id.tvShareCode);
        tvShareCode.setTypeface(appTypeface.getHind_regular());
        TextView tvShareToFrndz = view.findViewById(R.id.tvShareToFrndz);
        tvShareToFrndz.setTypeface(appTypeface.getHind_regular());
        TextView tvShareFb = view.findViewById(R.id.tvShareFb);
        tvShareFb.setTypeface(appTypeface.getHind_regular());

        TextView tvShareEmail = view.findViewById(R.id.tvShareEmail);
        tvShareEmail.setTypeface(appTypeface.getHind_regular());
        TextView tvShareMsg = view.findViewById(R.id.tvShareMsg);
        tvShareMsg.setTypeface(appTypeface.getHind_regular());
        TextView tvShareWhatsApp = view.findViewById(R.id.tvShareWhatsApp);
        tvShareWhatsApp.setTypeface(appTypeface.getHind_regular());
        TextView tvShareTwitter = view.findViewById(R.id.tvShareTwitter);
        tvShareTwitter.setTypeface(appTypeface.getHind_regular());
        prefs = new SharedPrefs(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        getRefferalCode(prefs.getSession());
    }

    private void getRefferalCode(String token) {
        //http://45.77.190.140:8899/customer/referralCode
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "referralCode",
                OkHttpConnection.Request_type.GET, new JSONObject(), token, VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Gson gson=new Gson();
                        if(headerData.equals("200")){
                            try{
                                ReferralCodeResponsePojo referralCodeResponsePojo = gson.fromJson(result, ReferralCodeResponsePojo.class);
                                tvShareCodeShare.setText(referralCodeResponsePojo.getData().getReferralCode());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        Log.d(TAG, "onSuccess: "+result);

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d(TAG, "onError: "+error);
                    }
                });

    }
}
