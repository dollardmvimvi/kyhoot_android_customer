package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.GeocodingResponse;
import com.Kyhoot.utilities.HttpConnections;
import com.Kyhoot.utilities.LocationUtil;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * <h>CurrentLocation</h>
 * search the location for the selected map Pin icon
 * Created by 3Embed on 10/11/2017.
 */

public class CurrentLocation extends AppCompatActivity implements LocationUtil.LocationNotifier,
        GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener, View.OnClickListener {
    private static final int searchPlaceRequestCode = 100;
    private static final int minDistanceToUpdate = 10;
    private final int REQUEST_CODE_PERMISSION_LOCATION = 1;
    double[] latLng = new double[2];
    TextView addresstxt;
    Timer myTimer;
    Context mcontext;
    Location old = new Location("OLD");
    SharedPrefs manager;
    Typeface regularfont, lightfont;
    AppTypeface typeFace;
    String TAG = "CurrentLocation";
    Gson gson;
    String fullAddress = "";
    // CardView cardview;
    Animation hide, visible;
    ProgressDialog progressDialog;
    private EditText searchAddres;
    private GoogleMap mMap;
    private LocationUtil networkUtilObj;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionLocationArrayList;
    private ImageView midpointmarker,ivAddressSearch;
    private Geocoder geocoder;
    private boolean IsreturnFromSearch = false;
    private boolean isFirstTime = false;
    private String addressTag;
    private RadioButton radio0, radio1, radio2;
    private TextView tvCurntLocHome, tvCurntLocWork, tvCurntLocOther;
    private boolean isClicked = true;
    /**
     * *****************************************************************************************************************
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_location);
        // overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        addressTag = getResources().getString(R.string.homeaddress);
        initializeObj();
        isFirstTime = true;
        initializeView();
        initializeToolBar();
        setTypeFace();
        mcontext = this.getApplication();
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.ABOVE);

        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                if (ActivityCompat.checkSelfPermission(CurrentLocation.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CurrentLocation.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                double[] latiLongi = new double[2];
                latiLongi[0] = Double.parseDouble(manager.getSplashLatitude());
                latiLongi[1] = Double.parseDouble(manager.getSplashLongitude());
                if (latiLongi[0] != 0 && latiLongi[1] != 0) {
                    initMapandAddress(latiLongi[0], latiLongi[1]);
                    LatLng latLng = new LatLng(latiLongi[0], latiLongi[1]);
                    CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(15.0f);
                    googleMap.moveCamera(center);
                    googleMap.animateCamera(zoom);
                }

                googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                    @Override
                    public void onCameraChange(CameraPosition arg0) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                IsreturnFromSearch = false;
                            }
                        }, 5000);
                    }
                });

            }
        });
    }

    private void setTypeFace() {
        addresstxt.setTypeface(typeFace.getHind_regular());
    }

    private void initializeToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbarCurrent);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView actionBarText = findViewById(R.id.tv_center);
        TextView tv_skip = findViewById(R.id.tv_skip);
        tv_skip.setText(getString(R.string.save));
        tv_skip.setTextColor(Utilities.getColor(this, R.color.red_login));
        toolbar.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        actionBarText.setText(getResources().getString(R.string.addresses));
        actionBarText.setAllCaps(false);
        actionBarText.setTypeface(typeFace.getHind_semiBold());
        tv_skip.setOnClickListener(this);
    }

    private void initializeView() {
        searchAddres = findViewById(R.id.searchAddres);
        midpointmarker = findViewById(R.id.midpointmarker);
        addresstxt = findViewById(R.id.addresstxt);

        RelativeLayout rlCurLoc1 = findViewById(R.id.rlCurLoc1);
        RelativeLayout rlCurLoc2 = findViewById(R.id.rlCurLoc2);
        RelativeLayout rlCurLoc3 = findViewById(R.id.rlCurLoc3);
        radio0 = findViewById(R.id.radio0);
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        radio0.setClickable(false);
        radio1.setClickable(false);
        radio2.setClickable(false);
        tvCurntLocHome = findViewById(R.id.tvCurntLocHome);
        tvCurntLocWork = findViewById(R.id.tvCurntLocWork);
        tvCurntLocOther = findViewById(R.id.tvCurntLocOther);
        ivAddressSearch=findViewById(R.id.ivAddressSearch);
        ivAddressSearch.setOnClickListener(this);
        tvCurntLocHome.setSelected(true);
        radio0.setChecked(true);
        tvCurntLocHome.setTypeface(typeFace.getHind_regular());
        tvCurntLocWork.setTypeface(typeFace.getHind_regular());
        tvCurntLocOther.setTypeface(typeFace.getHind_regular());


        addresstxt.setOnClickListener(this);
        rlCurLoc1.setOnClickListener(this);
        rlCurLoc2.setOnClickListener(this);
        rlCurLoc3.setOnClickListener(this);
    }

    private void initializeObj() {
        typeFace = AppTypeface.getInstance(this);
        gson = new Gson();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.wait));
        progressDialog.setCancelable(false);
        manager = new SharedPrefs(this);
        hide = AnimationUtils.loadAnimation(this, R.anim.up_moving_view_component);
        visible = AnimationUtils.loadAnimation(this, R.anim.down_moving_view_component);

    }

    @Override
    public void onCameraIdle() {
        String[] params = new String[]{"" + old.getLatitude(), "" + old.getLongitude()};
        new BackgroundGetAddress().execute(params);
    }

    @Override
    public void onCameraMoveStarted(int i) {
        IsreturnFromSearch = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
            networkUtilObj.stoppingLocationUpdate();
        if (myTimer != null) {
            myTimer.cancel();
            myTimer.purge();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermissionLocation();
        if (myTimer != null) {
            myTimer.cancel();
            myTimer.purge();
            myTimer = new Timer();
        } else {
            myTimer = new Timer();
        }

        statMyTimer();
    }

    private void checkPermissionLocation() {
        if (Build.VERSION.SDK_INT >= 23) {
            myPermissionLocationArrayList = new ArrayList<>();
            myPermissionLocationArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_ACCESS_FINE_LOCATION);
            myPermissionLocationArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_ACCESS_COARSE_LOCATION);

            if (AppPermissionsRunTime.checkPermission(this, myPermissionLocationArrayList, REQUEST_CODE_PERMISSION_LOCATION)) {

                networkUtilObj = new LocationUtil(this, this);
                if (!networkUtilObj.isGoogleApiConnected()) {
                    networkUtilObj.checkLocationSettings();
                }
            }
        } else {
            networkUtilObj = new LocationUtil(this, this);
            if (!networkUtilObj.isGoogleApiConnected()) {
                networkUtilObj.checkLocationSettings();
            }
        }
    }

    private void statMyTimer() {
        TimerTask myTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateMapLocationAddress();

                    }
                });
            }

        };
        myTimer.schedule(myTimerTask, 0, 3000);
    }

    private void initMapandAddress(double newLat, double newLong) {
        if (isFirstTime) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(newLat, newLong), 17f));
            String[] params = new String[]{"" + newLat, "" + newLong};
            new BackgroundGetAddress().execute(params);
            isFirstTime = false;
        }

    }

    private void updateMapLocationAddress() {
        VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();

        Point x1 = mMap.getProjection().toScreenLocation(visibleRegion.farRight);
        Point y = mMap.getProjection().toScreenLocation(visibleRegion.nearLeft);
        Point centerPoint = new Point(x1.x / 2, y.y / 2);
        LatLng centerFromPoint = mMap.getProjection().fromScreenLocation(centerPoint);
        double lat = centerFromPoint.latitude;
        double lon = centerFromPoint.longitude;
        Location newLoc = new Location("NEW");
        newLoc.setLatitude(lat);
        newLoc.setLongitude(lon);
        if (old.getLongitude() != 0 && old.getLatitude() != 0) {
            double distance = newLoc.distanceTo(old);
            if ((lat == old.getLatitude() && lon == old.getLongitude())) {
                Log.d("", "Update Address: FALSE");
            } else {
                old.setLatitude(lat);
                old.setLongitude(lon);
                if (midpointmarker.isShown()) {
                    if (distance > minDistanceToUpdate) {
                        String[] params = new String[]{"" + lat, "" + lon};
                        if (!IsreturnFromSearch) {
                            new BackgroundGetAddress().execute(params);
                        }
                    }

                }
            }
        } else {
            old.setLatitude(lat);
            old.setLongitude(lon);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == searchPlaceRequestCode) {

            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = PlaceAutocomplete.getPlace(this, data);
                String fullAdd = place.getName() +" \n "+place.getAddress();
                addresstxt.setText(fullAdd);
                place.getName();
                fullAddress = place.getName() + " " + place.getAddress();
                //  searchAddres.setText(place.getName());
                old.setLatitude(place.getLatLng().latitude);
                old.setLongitude(place.getLatLng().longitude);
                IsreturnFromSearch = true;
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude), 17f));

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                assert data != null;
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                IsreturnFromSearch = false;
            }
        }
    }

    @Override
    public void updateLocation(Location location) {
        latLng[0] = location.getLatitude();
        latLng[1] = location.getLongitude();
        Log.d("GOOGLE MAP LOC lat", "=" + latLng[0]);
        Log.d("GOOGLE MAP LOC long", "=" + latLng[1]);
    }

    @Override
    public void locationMsg(String error) {
        Log.d(TAG, "locationMsg: " + error);
    }

    /*************************************************************************************************************************/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
        finish();
        //overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_skip:
                //save address
               /* if(!addressTag.equals(""))
                {*/
                if (isClicked) {
                    isClicked = false;
                    if (!searchAddres.getText().toString().equals("")) {
                        addressTag = searchAddres.getText().toString();
                        addCustomerAddres(addresstxt.getText().toString().trim(), addressTag,
                                old.getLatitude(), old.getLongitude());
                    } else
                        addCustomerAddres(addresstxt.getText().toString().trim(), addressTag,
                                old.getLatitude(), old.getLongitude());
                }


                //  }
               /* else
                {
                    Toast.makeText(this,getString(R.string.pleaseSelectTag),Toast.LENGTH_SHORT).show();
                }*/

                break;
            case R.id.rlCurLoc1:
                radio0.setChecked(true);
                radio1.setChecked(false);
                radio2.setChecked(false);
                tvCurntLocHome.setSelected(true);
                tvCurntLocWork.setSelected(false);
                tvCurntLocOther.setSelected(false);
                searchAddres.setText("");
                searchAddres.setVisibility(View.GONE);
                addressTag = getResources().getString(R.string.homeaddress);


                break;
            case R.id.rlCurLoc2:
                radio0.setChecked(false);
                radio1.setChecked(true);
                radio2.setChecked(false);
                tvCurntLocHome.setSelected(false);
                tvCurntLocWork.setSelected(true);
                tvCurntLocOther.setSelected(false);
                searchAddres.setText("");
                searchAddres.setVisibility(View.GONE);
                addressTag = getResources().getString(R.string.officeaddress);
                //2
                break;
            case R.id.rlCurLoc3:
                radio0.setChecked(false);
                radio1.setChecked(false);
                radio2.setChecked(true);
                tvCurntLocHome.setSelected(false);
                tvCurntLocWork.setSelected(false);
                tvCurntLocOther.setSelected(true);
                searchAddres.setVisibility(View.VISIBLE);
                addressTag = getResources().getString(R.string.beenHereAddress);
                //3
                break;

            case R.id.addresstxt:
            case R.id.ivAddressSearch:

                try {

                    LatLngBounds bound = Utilities.setBound(old.getLatitude(), old.getLongitude());
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .setBoundsBias(bound)
                                    .build(CurrentLocation.this);
                    startActivityForResult(intent, searchPlaceRequestCode);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    e.printStackTrace();
                }
                break;
        }
    }

    private void addCustomerAddres(String road, String tagAddress, double lat, double log) {
        progressDialog.show();
        JSONObject json = new JSONObject();
        try {
            json.put("addLine1", road);
            json.put("latitude", lat);
            json.put("longitude", log);
            json.put("taggedAs", tagAddress);
            json.put("userType", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "address",
                OkHttpConnection.Request_type.POST, json, manager.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        progressDialog.dismiss();
                        try {
                            // ValidatorPojo pojo = gson.fromJson(result, ValidatorPojo.class);
                            CurrentLocation.this.overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        progressDialog.dismiss();
                        isClicked = true;
                        if (headerError.equals("502"))
                            new AlertProgress(CurrentLocation.this).alertinfo("Server issue");
                        else {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    Utilities.setMAnagerWithBID(CurrentLocation.this, manager,errorHandel.getMessage());
                                    break;
                                default:
                                    new AlertProgress(CurrentLocation.this).alertinfo(errorHandel.getMessage());
                                    break;
                            }
                        }
                    }
                });
    }

    /*********************************************************************************************************************/

    class BackgroundGeocodingTask extends AsyncTask<String, Void, String> {
        String param[];
        GeocodingResponse response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            param = params;

            String url = "http://maps.google.com/maps/api/geocode/json?latlng=" + params[0] + "," + params[1] + "&sensor=false";
            String stringResponse = HttpConnections.callhttpRequest(url);


            if (stringResponse != null) {
                response = gson.fromJson(stringResponse, GeocodingResponse.class);
            }

            return null;

        }

       /* *******************************************************************************************************************/

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if (response != null) {

                if (response.getStatus().equals("OK") && response.getResults() != null && response.getResults().size() > 0) {

                    if (response.getResults().size() > 0 && !response.getResults().get(0).getFormatted_address().isEmpty()) {

                        fullAddress = response.getResults().get(0).getFormatted_address();
                        addresstxt.setText(fullAddress);
                    }
                }
            }

        }
    }

    /*********************************************************************************************************************/

    class BackgroundGetAddress extends AsyncTask<String, Void, String> {
        List<Address> address = null;
        String lat, lng;
        String[] param;

        @Override
        protected String doInBackground(String... params) {
            param = params;

            try {
                Log.i("", "LatLong test lat " + params[0]);
                Log.i("", "LatLong test lon " + params[1]);
                lat = params[0];
                lng = params[1];

                if (lat != null && lng != null) {
                    if (CurrentLocation.this != null) {

                        geocoder = new Geocoder(CurrentLocation.this);
                    }

                    if (geocoder != null) {
                        address = geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);

                    }
                }
            } catch (IOException e) {

                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "HIII " + address);

            super.onPostExecute(result);
            if (address != null && address.size() > 0) {

                fullAddress = address.get(0).getAddressLine(0);//+","+address.get(0).getAddressLine(1);//+","+address.get(0).getAddressLine(2);
                addresstxt.setText(fullAddress);

            }
           /* else
            {
                new BackgroundGeocodingTask().execute(param);
            }*/
        }
    }
}
