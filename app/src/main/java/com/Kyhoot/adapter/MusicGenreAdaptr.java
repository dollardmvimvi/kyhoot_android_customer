package com.Kyhoot.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.MusciGenresData;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * <h>MusicGenreAdaptr</h>
 * Created by Ali on 9/26/2017.
 */

public class MusicGenreAdaptr extends RecyclerView.Adapter implements View.OnClickListener {
    private Activity mcontext;
    private ArrayList<MusciGenresData> musciGenresDatas;
    private ArrayList<String> selectedMusicGenrs;
    private ArrayList<String> selectedMusicId;

    public MusicGenreAdaptr(Activity mcontext, ArrayList<MusciGenresData> mgData, ArrayList<String>selectedMusicId) {
        this.mcontext = mcontext;
        musciGenresDatas = mgData;
        selectedMusicGenrs = new ArrayList<>();
        this.selectedMusicId = selectedMusicId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.music_genres, parent, false);

        return new ViewHoldr(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHoldr hldr = (ViewHoldr) holder;
        hldr.tvMusicGenres.setText(musciGenresDatas.get(position).getCatName());
        if(selectedMusicId.size()>0)
        {
            for(int i = 0;i<selectedMusicId.size();i++)
            {
                if(musciGenresDatas.get(position).getId().contains(selectedMusicId.get(i)))
                {
                    hldr.checkBoxMusic.setChecked(true);
                    selectedMusicGenrs.add(musciGenresDatas.get(hldr.getAdapterPosition()).getCatName());
                }
            }

        }
        hldr.checkBoxMusic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    selectedMusicGenrs.remove(musciGenresDatas.get(hldr.getAdapterPosition()).getCatName());
                    selectedMusicId.remove(musciGenresDatas.get(hldr.getAdapterPosition()).getId());
                } else {
                    selectedMusicGenrs.add(musciGenresDatas.get(hldr.getAdapterPosition()).getCatName());
                    selectedMusicId.add(musciGenresDatas.get(hldr.getAdapterPosition()).getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return musciGenresDatas.size();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("SELECTEDMUSIC", selectedMusicGenrs);
        intent.putExtra("SELECTEDMUSICID", selectedMusicId);
        mcontext.setResult(Activity.RESULT_OK, intent);
        mcontext.finish();
    }

    private class ViewHoldr extends RecyclerView.ViewHolder {
        CheckBox checkBoxMusic;
        TextView tvMusicGenres;
        private AppTypeface appTypeface;

        ViewHoldr(View itemView) {
            super(itemView);
            appTypeface = AppTypeface.getInstance(mcontext);
            tvMusicGenres = itemView.findViewById(R.id.tvMusicGenres);
            checkBoxMusic = itemView.findViewById(R.id.checkBoxMusic);
            tvMusicGenres.setTypeface(appTypeface.getHind_regular());

        }
    }
}
