package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 10/5/2017.
 */

public class Support_pojo implements Serializable {
    private String errNum;

    private String errMsg;

    private ArrayList<Support_Data_pojo> data;

    private String errFlag;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public ArrayList<Support_Data_pojo> getData() {
        return data;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    @Override
    public String toString() {
        return "ClassPojo [errNum = " + errNum + ", errMsg = " + errMsg + ", data = " + data + ", errFlag = " + errFlag + "]";
    }
}
