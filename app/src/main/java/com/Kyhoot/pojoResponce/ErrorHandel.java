package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>ErrorHandel</h>
 * Created by 3Embed on 10/12/2017.
 */

public class ErrorHandel implements Serializable {
    private String message, data;

    public String getMessage() {
        return message;
    }

    public String getData() {
        return data;
    }
}
