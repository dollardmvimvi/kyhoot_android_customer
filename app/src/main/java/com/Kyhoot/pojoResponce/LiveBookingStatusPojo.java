package com.Kyhoot.pojoResponce;


import java.io.Serializable;

/**
 * <h>LiveBookingStatusPojo</h>
 * this bean object is to get the live booking status data from the server
 * Created by Ali on 11/8/2017.
 */

public class LiveBookingStatusPojo {
    /*"message":"Get All Booking Details",
"data":{}*/
    private String message;
    private LiveBookingData data;

    public String getMessage() {
        return message;
    }

    public LiveBookingData getData() {
        return data;
    }

    public class LiveBookingData {
        private String statusMsg, paymentMethod, signURL,currencySymbol;
        private double latitude,longitude;
        //private GigBookinTim gigTime;
        private ProviderBookingDetls providerDetail;
        private JobBookingStatus jobStatusLogs;
        private BookingTimer bookingTimer;
        private int status;
        private CartData cart;

        private int cancellationAfterXMinute;

        public CartData getCart() {
            return cart;
        }

        public void setCart(CartData cart) {
            this.cart = cart;
        }

        public AllEventsDataPojo.Accounting getAccounting() {
            return accounting;
        }

        private AllEventsDataPojo.Accounting accounting;


        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public String getCurrencySymbol() {
            return currencySymbol;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public String getSignURL() {
            return signURL;
        }

        //public GigBookinTim getGigTime() {
          //  return gigTime;
        //}

        public ProviderBookingDetls getProviderDetail() {
            return providerDetail;
        }

        public JobBookingStatus getJobStatusLogs() {
            return jobStatusLogs;
        }

        public BookingTimer getBookingTimer() {
            return bookingTimer;
        }

        public int getCancellationAfterXMinute() {
            return cancellationAfterXMinute;
        }

        public class GigBookinTim implements Serializable{
            /* "id":"59d6291cb0554924860bb333","name":"15","unit":"MIN","price":36,"second":900*/
            private String id, name, unit;
            private double price;
            private long second;

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getUnit() {
                return unit;
            }

            public double getPrice() {
                return price;
            }

            public long getSecond() {
                return second;
            }
        }

        public class ProviderBookingDetls implements Serializable {
            /*"firstName":"Murashid",
"lastName":"Test",
"phone":"+96691752386505",
"providerId":"59dc6e42b4ed3e4e0951fe42",
"proStatus":0,
"distance":0.001792,
"averageRating":0,
"reviewCount":0*/
            //proLocation

            private String firstName, lastName, phone, providerId, profilePic;
            private int proStatus, reviewCount;
            private double distance;
            private float averageRating;
            private ProviderLocation proLocation;

            public ProviderLocation getProLocation() {
                return proLocation;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public String getPhone() {
                return phone;
            }

            public String getProviderId() {
                return providerId;
            }

            public int getProStatus() {
                return proStatus;
            }

            public int getReviewCount() {
                return reviewCount;
            }

            public double getDistance() {
                return distance;
            }

            public float getAverageRating() {
                return averageRating;
            }
        }

        public class JobBookingStatus implements Serializable {
            /*"receivedTime":1510127118,"acceptedTime":1510127159*/
            private long requestedTime, receivedTime, acceptedTime, onthewayTime, arrivedTime, startedTime, completedTime;

            public long getReceivedTime() {
                return receivedTime;
            }

            public long getAcceptedTime() {
                return acceptedTime;
            }

            public long getOnthewayTime() {
                return onthewayTime;
            }

            public long getArrivedTime() {
                return arrivedTime;
            }

            public long getStartedTime() {
                return startedTime;
            }

            public long getRequestedTime() {
                return requestedTime;
            }

            public long getCompletedTime() {
                return completedTime;
            }
        }

        public class BookingTimer implements Serializable {
            /*"status":0,
"second":0,
"startTimeStamp":0*/
            private int status;
            private long second;
            private long startTimeStamp;

            public int getStatus() {
                return status;
            }

            public long getSecond() {
                return second;
            }

            public long getStartTimeStamp() {
                return startTimeStamp;
            }
        }
    }
}
