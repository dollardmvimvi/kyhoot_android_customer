package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 11/1/2017.
 */

public class MyEventDataPojo implements Serializable {

    private ArrayList<AllEventsDataPojo> pending;
    private ArrayList<AllEventsDataPojo> past;
    private ArrayList<AllEventsDataPojo> upcoming;

    public ArrayList<AllEventsDataPojo> getPending() {
        return pending;
    }

    public ArrayList<AllEventsDataPojo> getPast() {
        return past;
    }

    public ArrayList<AllEventsDataPojo> getUpcoming() {
        return upcoming;
    }
}
