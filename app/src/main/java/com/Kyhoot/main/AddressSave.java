package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by embed on 12/9/16.
 */
public class AddressSave extends AppCompatActivity {
    TextView tvaddredtls, tvtagaddress, tvSaveAddress, tvhomeadd, tvotheradd, tvofficeadd;
    TextInputEditText tieStreetlocaty, tieBuildinflat, tieLandmark, tieAddressTag;
    TextInputLayout tilStreetlocaty, tilBuildinflat, tilLandmark, tilAddressTag;
    ImageView ivhomeselectr, ivoffceselectr, ivothrselectr;
    String fulladdress;
    double latitude, longitude;
    ProgressDialog progressDialog;
    SharedPrefs manager;
    Gson gson;
    AppTypeface apTypeFace;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_address);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        apTypeFace = AppTypeface.getInstance(this);
        if (getIntent().getExtras() != null) {
            fulladdress = getIntent().getStringExtra("fulladdress");
            latitude = getIntent().getDoubleExtra("latitude", 0.0);
            longitude = getIntent().getDoubleExtra("longitude", 0.0);
        }
        initializeToolBar();
        initializeObj();
        initialize();
    }

    private void initializeObj() {
        progressDialog = new ProgressDialog(this);
        manager = new SharedPrefs(this);
        progressDialog.setMessage(getResources().getString(R.string.wait));
        gson = new Gson();
    }

    private void initializeToolBar() {
        Toolbar toolbarAddress = findViewById(R.id.toolbarAddress);
        setSupportActionBar(toolbarAddress);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.saveAddress);
        tv_center.setTypeface(apTypeFace.getHind_bold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarAddress.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolbarAddress.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initialize() {
        tvofficeadd = findViewById(R.id.tvofficeadd);
        tvhomeadd = findViewById(R.id.tvhomeadd);
        tvotheradd = findViewById(R.id.tvotheradd);
        tvtagaddress = findViewById(R.id.tvtagaddress);
        tvaddredtls = findViewById(R.id.tvaddredtls);
        tvSaveAddress = findViewById(R.id.SaveAddress);
        ivhomeselectr = findViewById(R.id.ivhomeselectr);
        ivoffceselectr = findViewById(R.id.ivoffceselectr);
        ivothrselectr = findViewById(R.id.ivothrselectr);
        tieStreetlocaty = findViewById(R.id.etstreetlocaty);
        tieBuildinflat = findViewById(R.id.etbuildinflat);
        tieLandmark = findViewById(R.id.etlandmark);
        tieAddressTag = findViewById(R.id.ettagtheaddress);
        tilStreetlocaty = findViewById(R.id.etloutstreetlocaty);
        tilBuildinflat = findViewById(R.id.etloutbuildinflat);
        tilLandmark = findViewById(R.id.etloutlandmark);
        tilAddressTag = findViewById(R.id.etlltagtheaddress);

        tilBuildinflat.setFocusable(true);
        tieBuildinflat.setFocusable(true);
        tieStreetlocaty.setText(fulladdress);
        typeface();
        selectype();
        tvSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tieAddressTag.getText().toString().trim().equals("")) {
                    addCustomerAddres(tieBuildinflat.getText().toString(), tieStreetlocaty.getText().toString(), tieLandmark.getText().toString()
                            , tieAddressTag.getText().toString()
                            , latitude, longitude);
                } else {
                    Toast.makeText(AddressSave.this, getResources().getString(R.string.pleaseentertag), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void selectype() {
        ivhomeselectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivhomeselectr.setSelected(true);
                ivoffceselectr.setSelected(false);
                ivothrselectr.setSelected(false);
                tvhomeadd.setTextColor(Color.parseColor("#2598ED"));
                tvotheradd.setTextColor(Color.GRAY);
                tvofficeadd.setTextColor(Color.GRAY);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                tilAddressTag.setVisibility(View.GONE);
                tieAddressTag.setText(getResources().getString(R.string.homeaddress));
            }
        });
        ivoffceselectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivhomeselectr.setSelected(false);
                ivoffceselectr.setSelected(true);
                ivothrselectr.setSelected(false);
                tvhomeadd.setTextColor(Color.GRAY);
                tvotheradd.setTextColor(Color.GRAY);
                tvofficeadd.setTextColor(Color.parseColor("#2598ED"));
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                tilAddressTag.setVisibility(View.GONE);
                tieAddressTag.setText(getResources().getString(R.string.officeaddress));
            }
        });
        ivothrselectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivhomeselectr.setSelected(false);
                ivoffceselectr.setSelected(false);
                ivothrselectr.setSelected(true);
                tvotheradd.setTextColor(Color.parseColor("#2598ED"));
                tvhomeadd.setTextColor(Color.GRAY);
                tvofficeadd.setTextColor(Color.GRAY);
                tieAddressTag.setText("");
                tieAddressTag.requestFocus();
                tilAddressTag.setVisibility(View.VISIBLE);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });
    }

    private void typeface() {
        tvSaveAddress.setTypeface(apTypeFace.getHind_regular());
        tvaddredtls.setTypeface(apTypeFace.getHind_light());
        tieStreetlocaty.setTypeface(apTypeFace.getHind_light());
        tieBuildinflat.setTypeface(apTypeFace.getHind_light());
        tieLandmark.setTypeface(apTypeFace.getHind_light());
        tvtagaddress.setTypeface(apTypeFace.getHind_light());
        tieAddressTag.setTypeface(apTypeFace.getHind_light());
        tvofficeadd.setTypeface(apTypeFace.getHind_light());
        tvhomeadd.setTypeface(apTypeFace.getHind_light());
        tvotheradd.setTypeface(apTypeFace.getHind_light());
    }

    /**
     * <h1>addCustomerAddres</h1>
     * this method is saving the address using the api
     *
     * @param road       street
     * @param city       address of the city
     * @param land_mark  Landmark of the address
     * @param tagAddress tag of the address
     * @param lat        latitude of the address
     * @param log        longitude of the address
     */
    private void addCustomerAddres(String road, String city, String land_mark, String tagAddress, double lat, double log) {
        progressDialog.show();
        JSONObject json = new JSONObject();
        try {
            json.put("addLine1", city);
            json.put("addLine2", land_mark);
            json.put("latitude", lat);
            json.put("longitude", log);
            json.put("taggedAs", tagAddress);
            json.put("userType", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "address",
                OkHttpConnection.Request_type.POST, json, manager.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        progressDialog.dismiss();
                        try {
                            ValidatorPojo pojo = gson.fromJson(result, ValidatorPojo.class);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        progressDialog.dismiss();
                    }
                });
    }
}
