package com.Kyhoot.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.models.CardPaymentModel;
import com.Kyhoot.pojoResponce.CardDetailsResp;
import com.Kyhoot.pojoResponce.CardInfoPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <h>ChangeCardActivity</h>
 * Created by embed on 9/12/15.
 */
public class ChangeCardActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener,VerifyOtpIntrface.PaymentInterFace {


    List<CardInfoPojo> rowItems;
    SharedPrefs sessionManager;
    ProgressDialog pDialog;
    Resources resources;
    CardDetailsResp response;
    private ListView card_list;
    private CustomListViewAdapter adapter;
    private AppTypeface appTypeface;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_card);
        gson = new Gson();
        appTypeface = AppTypeface.getInstance(this);
        initialize();
        initializeToolBar();
    }

    private void initializeToolBar() {

        Toolbar actionbar = findViewById(R.id.actionbar);
        setSupportActionBar(actionbar);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.paymentMethodFrag);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        actionbar.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        actionbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * intilize the typeface for font
     */
    private void initialize() {
        resources = ChangeCardActivity.this.getResources();

        card_list = findViewById(R.id.cards_list_view);
        LayoutInflater footerinflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footerView = footerinflater.inflate(R.layout.footer_add_card, card_list, false);
        TextView tvAddCard = footerView.findViewById(R.id.tvAddCard);
        RelativeLayout rlPaymntFragAddCard = footerView.findViewById(R.id.rlPaymntFragAddCard);
        card_list.addFooterView(footerView);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        sessionManager = new SharedPrefs(ChangeCardActivity.this);
        rowItems = new ArrayList<>();
        adapter = new CustomListViewAdapter(ChangeCardActivity.this, R.layout.card_list_row, rowItems);
        card_list.setAdapter(adapter);

        tvAddCard.setOnClickListener(this);
        card_list.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {

        /*
         *start payment activity for adding card
         */
        if (v.getId() == R.id.tvAddCard) {
            Intent intent = new Intent(ChangeCardActivity.this, SignupPayment.class);
            intent.putExtra("coming_From", "payment_Fragment");
            startActivity(intent);
            //  ChangeCardActivity.this.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
    }

    /**
     * calling all card api
     */
    @Override
    protected void onResume() {
        super.onResume();
        cllGetAllCards();
    }

    /***
     * on clicking list item get all all values from selected item and finishing this activity
     * @param parent adapter parent
     * @param view adapter view
     * @param position adapter view
     * @param id adapter id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        CardInfoPojo row_details = (CardInfoPojo) card_list.getItemAtPosition(position);
        if (row_details != null) {


            Log.d("TAG", " postion of card no " + row_details.getLast4() + "id" + row_details.getId());

            Intent returnIntent = new Intent();
            returnIntent.putExtra("cardNo", row_details.getLast4());
            returnIntent.putExtra("cardID", row_details.getId());
            returnIntent.putExtra("cardImage", row_details.getCardImage());
            returnIntent.putExtra("cardBrand", row_details.getBrand());
            ChangeCardActivity.this.overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            ChangeCardActivity.this.setResult(RESULT_OK, returnIntent);
            ChangeCardActivity.this.finish();
        } else {
            Toast.makeText(ChangeCardActivity.this, getString(R.string.tryAgain), Toast.LENGTH_SHORT).show();
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    /**
     * api call for get all card
     */
    public void cllGetAllCards() {
        //creating request with parameter

        if (pDialog != null)
            pDialog.show();
        else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

        }
        new CardPaymentModel().getPaymentCard(this, sessionManager, ChangeCardActivity.this);

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
    }

    @Override
    public void onClearList() {
        rowItems.clear();
        adapter.notifyDataSetChanged();
        if(pDialog!=null)
            pDialog.dismiss();
    }

    @Override
    public void onError(String errorMsg)
    {
        if(pDialog!=null)
            pDialog.dismiss();
    }

    @Override
    public void onResponceItem(CardInfoPojo cardInfoPojo) {
        rowItems.add(cardInfoPojo);
        if(pDialog!=null)
            pDialog.dismiss();
        adapter.notifyDataSetChanged();
        if(cardInfoPojo.isDefault()){
            sessionManager.setDefaultCardBrand(cardInfoPojo.getBrand());
            sessionManager.setDefaultCardId(cardInfoPojo.getId());
            sessionManager.setDefaultCardNum(cardInfoPojo.getLast4());
        }
    }

    /**
     * adapter class for list view
     */

    class CustomListViewAdapter extends ArrayAdapter<CardInfoPojo> {

        Context context;

        private CustomListViewAdapter(Context context, int resourceId, List<CardInfoPojo> items) {
            super(context, resourceId, items);
            this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            CustomListViewAdapter.ViewHolder holder;
            final CardInfoPojo rowItem = getItem(position);
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.card_list_row, parent, false);
                holder = new CustomListViewAdapter.ViewHolder();
                holder.card_numb = convertView.findViewById(R.id.card_numb_row_change);
                holder.card_image = convertView.findViewById(R.id.card_img_row_change);
                holder.iv_tick = convertView.findViewById(R.id.iv_tick);
                holder.change_card_relative = convertView.findViewById(R.id.change_card_relative);
                convertView.setTag(holder);
            } else
                holder = (CustomListViewAdapter.ViewHolder) convertView.getTag();
            assert rowItem != null;
            holder.card_image.setImageBitmap(rowItem.getCardImage());
            holder.card_numb.setText(rowItem.getLast4());
            if (rowItem.isDefault()) {
                holder.iv_tick.setVisibility(View.VISIBLE);
            } else {
                holder.iv_tick.setVisibility(View.GONE);
            }
            return convertView;
        }

        private class ViewHolder {
            ImageView card_image, iv_tick;
            TextView card_numb;
            RelativeLayout change_card_relative;
        }
    }
}
