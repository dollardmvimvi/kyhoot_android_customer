package com.Kyhoot.controllers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.ProfileDataResponce;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.GeneralDataMsg;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * <h>ProviderDetailsControl</h>
 * Created by 3Embed on 10/20/2017.
 */

public class ProviderDetailsControl{
    private Activity mActivity;
    private SharedPrefs prefs;
    private ProfileDataResponce.ProProfileDataResponce profileDataResponce;
    public static final String TAG="PROVIDERDETAILSCONTROL";

    public ProviderDetailsControl(Activity mActivity, SharedPrefs prefs) {
        this.mActivity = mActivity;
        this.prefs = prefs;
    }

    public void ProDetailsService(final ProfileDataResponce.ProProfileDataResponce profileDataResponce, final LatLng latlng, final String proId) {
        JSONObject jsonObject = new JSONObject();

        this.profileDataResponce = profileDataResponce;
        final Gson gson = new Gson();
        Log.d(TAG, "service providerDetails: "+VariableConstant.SERVICE_URL + "providerDetails/" + proId+"/"+VariableConstant.selectedCategoryId + "/" + latlng.latitude + "/" + latlng.longitude);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "providerDetails/" + proId + "/" +
                        VariableConstant.selectedCategoryId +"/"+latlng.latitude + "/" + latlng.longitude, OkHttpConnection.Request_type.GET, jsonObject, prefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {

                    @Override
                    public void onSuccess(String headerData, String result)
                    {
                        Log.d(TAG, "onSuccess: "+result);
                        try
                        {
                            GeneralDataMsg generalDataMsg = gson.fromJson(result, GeneralDataMsg.class);
                            Log.d(TAG, "onSuccess: amount "+generalDataMsg.getData().getAmount());
                            profileDataResponce.onProSuccessDtls(generalDataMsg.getData());
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                            profileDataResponce.onError(e.getMessage());
                        }

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.e(TAG, "onError: "+error+" HeaderError:"+headerError );
                        try{
                            if(headerError.equals("502"))
                            {
                                profileDataResponce.onError("Server issue");
                            }else if(headerError.equals("549")){
                                profileDataResponce.onError("Socket timed out");
                            }
                            else
                            {
                                ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "440":
                                        getAccessToken(errorHandel.getData(), profileDataResponce, latlng, proId);
                                        break;
                                    case "498":
                                        profileDataResponce.onSessionExpired(errorHandel.getMessage());
                                        break;
                                    default:
                                        profileDataResponce.onError(errorHandel.getMessage());
                                        break;
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                });


    }

    private void getAccessToken(String data, final ProfileDataResponce.ProProfileDataResponce profileDataResponce, final LatLng latlng, final String proId) {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {


                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            prefs.setSession(jsonobj.getString("data"));
                            ProDetailsService(profileDataResponce, latlng, proId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                        if(headerError.equals("502"))
                        {
                            profileDataResponce.onError("Server issue");
                        }
                        else
                        {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            if (headerError.equals("440"))
                                getAccessToken(errorHandel.getData(), profileDataResponce, latlng, proId);
                            else if (headerError.equals("498")) {
                                profileDataResponce.onSessionExpired(errorHandel.getMessage());
                            } else
                                profileDataResponce.onError(errorHandel.getMessage());
                        }

                    }
                });
    }

    public void moreReadable(TextView tvProAbout) {
        String readMore = "read more";
        String readLess = "read less";


        new ReadMoreSpannable(readMore, readLess);
        if (tvProAbout.getText().toString().length() > 100)
            ReadMoreSpannable.makeTextViewResizable(tvProAbout, 3, readMore, true);
    }

    public void distanceCal(double distance, TextView tvProDistance, Context mContext) {

        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        // String faremount=nf_out.format(Double.parseDouble(pubNubMas)/1000);
        @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", distance);
        String kilometr = kilometer + " " + VariableConstant.distanceUnit;
        tvProDistance.setText(kilometr);

    }


    public String providerYoutubeLink(String videoUrl)
    {
        if (videoUrl.contains("youtu.be/"))
            videoUrl = videoUrl.substring(videoUrl.lastIndexOf("/"));
        else
            videoUrl =  videoUrl.substring(videoUrl.indexOf('=') + 1);
        return videoUrl;
    }

    public void callReviewApi(final int pageCount, final String proId, final UtilInterFace utilInterface)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "providerReview/" + proId + "/" + pageCount,
                OkHttpConnection.Request_type.GET, new JSONObject(), prefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        ValidatorPojo vPojo = new Gson().fromJson(result, ValidatorPojo.class);
                        utilInterface.onSuccessReview(vPojo);
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if(headerError.equals("502"))
                        {
                            utilInterface.onError("Server issue");
                        }
                        else
                        {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            if (headerError.equals("440"))
                                getAccessToken(errorHandel.getData(),pageCount,proId,utilInterface);
                            else if (headerError.equals("498")) {
                                profileDataResponce.onSessionExpired(errorHandel.getMessage());
                            } else
                                utilInterface.onError(errorHandel.getMessage());
                        }

                    }
                });
    }


    private void getAccessToken(String data, final int pageCount, final String proId, final UtilInterFace utilInterface)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            prefs.setSession(jsonobj.getString("data"));
                            callReviewApi(pageCount,proId,utilInterface);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                        if(headerError.equals("502"))
                        {
                            profileDataResponce.onError("Server issue");
                        }
                        else
                        {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            if (headerError.equals("498")) {
                                profileDataResponce.onSessionExpired(errorHandel.getMessage());
                            } else
                                profileDataResponce.onError(errorHandel.getMessage());
                        }

                    }
                });
    }
    public void callServicesService(final ProfileDataResponce.ProProfileDataResponce inteface, String proId) {
        JSONObject json = new JSONObject();
        try {
            json.put("catId",prefs.getCategoryId() );
            Log.d(TAG, "callServicesService: "+json);
            String sesnTkn = prefs.getSession();
            Log.d(TAG, "callServicesService: "+VariableConstant.SERVICE_URL + "services"+"/"+prefs.getCategoryId()+"/"+proId);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "services"+"/"+prefs.getCategoryId()+"/"+proId,
                    OkHttpConnection.Request_type.GET, json, sesnTkn, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerResponse, String result)
                        {
                            inteface.onSuccessServicesUpdate(headerResponse,result);
                            Log.d(TAG, "onSuccess servicesService: "+result);

                        }

                        @Override
                        public void onError(String headerError, String error) {

                            if (headerError.equals("502"))
                                inteface.onFailureServicesUpdate(headerError,"");
                            else {
                                /* *
                                 * crash # 11: ProviderDetailsControl.java line 264
                                  * #11com.Kyhoot.controllers.ProviderDetailsControl$5.onError
                                 * CardId #93
                                 * Cause : When it doesn't
                                 * Fix : Added Try catch for the exception
                                 * hello baby how are you
                                 * hope are you doing well
                                 */
                                try {
                                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                    switch (headerError) {
                                    /*case "498":
                                        inteface.onSessionError(errorHandel.getMessage());
                                        break;*/
                                    /*case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;*/
                                        default:
                                            inteface.onFailureServicesUpdate(headerError, errorHandel.getMessage());
                                            break;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
