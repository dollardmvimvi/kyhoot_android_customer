package com.Kyhoot.main;

import android.Manifest;
import android.animation.Animator;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.BuildConfig;
import com.Kyhoot.R;
import com.Kyhoot.adapter.ConfirmBookingServicesAdapter;
import com.Kyhoot.adapter.ServiceFlowJobPhotosAdapter;
import com.Kyhoot.controllers.ConfirmBookController;
import com.Kyhoot.interfaceMgr.AccessTokenCallback;
import com.Kyhoot.interfaceMgr.JobPhotosCallback;
import com.Kyhoot.interfaceMgr.Terms_privacy_interface;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.main.bookingType.DialogTimeFragment;
import com.Kyhoot.main.bookingType.RepeatDays;
import com.Kyhoot.main.bookingType.SelectTimeAndDuration;
import com.Kyhoot.pojoResponce.CardDetailsResp;
import com.Kyhoot.pojoResponce.CartModifiedPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.pojoResponce.LastDuesPojo;
import com.Kyhoot.pojoResponce.PromocodePojo;
import com.Kyhoot.pojoResponce.ProviderServiceDtls;
import com.Kyhoot.pojoResponce.SurgePriceResponse;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CreateOrClearDirectory;
import com.Kyhoot.utilities.HandlePictureEvents;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.UploadAmazonS3;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONArray;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;

import static android.os.Build.VERSION_CODES.N;

public class ConfirmBookingActivity extends AppCompatActivity implements JobPhotosCallback , View.OnClickListener, UtilInterFace.ConfirmBook,AccessTokenCallback,Terms_privacy_interface,DialogTimeFragment.OnFragmentInteractionListener
{

    private static final int MY_PERMISSIONS_REQUEST_CALENDAR = 534;
    private static final int SELECT_PAYMENT_CODE = 234;
    double serviceFee = 0, travelFee = 0, discountFee = 0;
    double dummyserviceFee = 0, dummytravelFee = 0, dummydiscountFee = 0;

    int noofHours=1 ;

    private SharedPrefs sPrefs;
    private TextView tvConfirmAddCard, tvConfirmCardNumber, tvConfirmAppAddress, tvConfirmPromoApply,toolBarTitle,tvConfirmAddWalletMoney,tvWalletBalance,tvRepeat;
    private TextView tvVisitFeeAmt, tvDiscountAmount, tvTotalAmt,tvNow,tvSchedule,tvConfirmBook,tv_lastdues_Amt,tv_lastdues,tv_service_price,tvJobDesc;
    private TextView tvWhentoBook,tvTodaysDate,tvTodaysTime,tvNowDesc,tvLaterDesc,tvSelTime,tvSelDate,tv_travelFee,tv_travelFeeAmt,tvYahBookingDone,tvWillLetUKnw,tvRepeatDesc;
    private EditText etConfirmPromoCodes,etJobDescription;
    private String imageurl = "",  reviewCount;
    private float rating;
    private int paymentType = -1;
    double totalAmount=0.0;
    private int bookingTime = 30;
    private String proId, promoCode, paymentCardId;//eventId,gigTimeId,
    private AlertProgress aProgress;
    private ConfirmBookController confirmBookCont;
    private ProgressBar progressBarConfirm,progress_bar;
    private int ADDRESS_CODE = 140;
    private LinearLayout llLiveFee;
    private AppTypeface appTypeface;
    private String currencySymbol;
    private double distance;
    private AppBarLayout appBarLayout;
    private GridLayout glNowGrid,glLaterGrid,glLaterPartGrid,glLaterTimePart,glRepeatGrid;
    private ImageView ivCalendar,ivNowCheck,ivLaterCheck,ivRepeatCheck;
    private String TAG="CONFIRMBOOKINGACTIVITY";
    private String cardBrand;
    private RelativeLayout rlCardLayout,rlconfm_visitfee,rltravel_fee,rlMAin,rlWalletLayout,rlSelectPaymentMethod,rlCashPaymentMethod,rlSelectedCategory;
    private ImageView ivCardBrand;
    private CartModifiedPojo cartPojo;
    private ImageView ivRightArrow;
    private Calendar testCalender=null;
    private boolean promoCodeActive;
    private RecyclerView rv_selected_services;
    private Gson gson;
    private ImageView ivTickCheck;
    ConfirmBookingServicesAdapter confirmBookingServicesAdapter;
    private int paidByWallet=0;
    private RelativeLayout rldiscount,rlLastdues;
    private boolean isHourlyService=false;
    private double lastDuesAmount=0;
    private int cartQuantity;
    private NestedScrollView nestedScrollView;
    private int previousPaymentType=-1;
    private int previousPaidByWallet=-1;
    private double total=0.0;
    private boolean minimumHr=false;
    private String dataShowOnAdapter;

    //multiple shifts

    private GridLayout glRepeatPartGrid,glRepeatPartTimeGrid,glRepeatPartDurationGrid,glRepeatPartDaysGrid;

    private LinearLayout multipleShiftBooking;

    private TextView tvSelRepeatStartDate,tvTodaysRepeatStartDate;

    private SimpleDateFormat simpleDateFormat;
    Date startDates,  end;

    private boolean isCustomSelected = false;
    private boolean durationActivityCalled=false;
    private ArrayList<String> selectedDays = new ArrayList<>();
    private String currentDay;

    private ArrayList<String>allDayOfTheWeek = new ArrayList<>();
    HashMap<Integer,String> dayMap = new HashMap<Integer,String>();
    ArrayList<String> maxNameOfDays = new ArrayList<>();

    private boolean isNowSelected,isLaterSelected,isRepeatSelected;
    private boolean isAnyTypeSelected=false;

    private long onSelectedScheduledDateTime;

    private TextView tvTodaysRepeatDays,tvSelRepeatDuration,tvSelRepeatDurationTime;
    private TextView tvRepeatStartTime;

    private int durationHour,durationMin;
    private int onSelectedScheduledDuration = 30;
    private TextView tvEditShiftDetails;

    private TextView tvConfirmStartTime,tvConfirmEndTime,tvConfirmStartDate,tvConfirmEndDate;

    long finaldateZero,startDatezero,startDateCorrectValue,finaldateCorrectValue;

    private LinearLayout llConfirmScheduledDays;

    int numOfShifts = 0;

    private TextView tvConfirmNumberOfShift;

    private boolean durationSet=false;


    private File imageStorageDir,imageStorageDir2;

    private JSONArray jobPhotosArray;
    private File newFile;
    private String takenNewImage;
    private HandlePictureEvents handlePictureEvents;

    public final int PERMISSIONREQUESTCODE=434;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    private ArrayList<String> jobPhotosList;
    ServiceFlowJobPhotosAdapter jobPhotosAdapter;
    private RecyclerView rv_photos;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_book);
        initObjects();
        takeImage();
        initializeView();
        typeFace();
        getLastDue();
        initToolBar();
    }

    private void getLastDue()
    {

        confirmBookCont.getLastDue(sPrefs.getSession());
    }

    private void initToolBar() {

        Toolbar toolProvider = findViewById(R.id.toolbar);
        setSupportActionBar(toolProvider);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolProvider.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void takeImage() {
        sPrefs = new SharedPrefs(this);
        currencySymbol=sPrefs.getCurrencySymbol();
        aProgress = new AlertProgress(this);
        confirmBookCont = new ConfirmBookController(this);
        progressBarConfirm = findViewById(R.id.progressBarConfirm);
        getDefaultCard();
        gson=new Gson();


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            imageurl = bundle.getString("IMAGEURL");
            //proName = bundle.getString("NAME");
            proId = bundle.getString("PROID");
            rating = bundle.getFloat("RATING");
            reviewCount = bundle.getString("REVIEWNUMBER");
            distance = bundle.getDouble("DISTANCE");
            cartPojo=(CartModifiedPojo) bundle.getSerializable("CARTDATA");
        }
        getCart();
    }

    private void getDefaultCard() {
        confirmBookCont.getdefaultCard(this,sPrefs,this);
    }

    private void initObjects() {
        appTypeface = AppTypeface.getInstance(this);
        simpleDateFormat = new SimpleDateFormat("MMMM d, yyyy',' h:mm a", Locale.getDefault());
    }

    private void initializeView() {
        rlLastdues=findViewById(R.id.rlLastdues);
        tv_lastdues=findViewById(R.id.tv_lastdues);
        nestedScrollView=findViewById(R.id.nestedScrollView);

        //ConfirmBook Terms n Conditions
        TextView tvConfirmTerms = findViewById(R.id.tvConfirmTerms);
        Utilities.setSpannableString(this,tvConfirmTerms,getString(R.string.termsAndConditionConfirm),"terms & conditions","privacy policy",this);

        tv_lastdues_Amt=findViewById(R.id.tv_lastdues_Amt);
        testCalender=Calendar.getInstance(Locale.US);
        testCalender.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
        try {
            if (VariableConstant.SCHEDULEDDATE.equals("")) {
                testCalender.setTime(new Date(System.currentTimeMillis() - sPrefs.getServerTimeDifference()));
            } else {
                testCalender.setTimeInMillis(Long.parseLong(VariableConstant.SCHEDULEDDATE) * 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "initializeView: testcalendartime"+testCalender.getTimeInMillis());
        rldiscount=findViewById(R.id.rldiscount);
        toolBarTitle=findViewById(R.id.toolBarTitle);
        tvWhentoBook=findViewById(R.id.tvWhentoBook);
        tvTodaysDate=findViewById(R.id.tvTodaysDate);
        tvTodaysTime=findViewById(R.id.tvTodaysTime);
        tv_travelFee=findViewById(R.id.tv_travelFee);
        tv_service_price=findViewById(R.id.tv_service_price);
        rlCashPaymentMethod=findViewById(R.id.rlCashPaymentMethod);
        rlSelectPaymentMethod=findViewById(R.id.rlSelectPaymentMethod);
        tv_travelFeeAmt=findViewById(R.id.tv_travelFeeAmt);
        rv_selected_services=findViewById(R.id.rv_selected_services);
        rlMAin=findViewById(R.id.rlMAin);
        tvNowDesc=findViewById(R.id.tvNowDesc);
        tvLaterDesc=findViewById(R.id.tvLaterDesc);
        tvRepeatDesc = findViewById(R.id.tvRepeatDesc);
        tvSelDate=findViewById(R.id.tvSelDate);
        tvSelTime=findViewById(R.id.tvSelTime);
        tvRepeat=findViewById(R.id.tvRepeat);
        tvConfirmPromoApply = findViewById(R.id.tvConfirmPromoApply);
        glNowGrid=findViewById(R.id.glNowGrid);
        glLaterGrid=findViewById(R.id.glLaterGrid);
        glRepeatGrid=findViewById(R.id.glRepeatGrid);
        glLaterPartGrid=findViewById(R.id.glLaterPartGrid);
        glLaterTimePart=findViewById(R.id.glLaterTimePart);
        glLaterTimePart.setOnClickListener(this);
        ivCalendar=findViewById(R.id.ivCalendar);
        ivLaterCheck=findViewById(R.id.ivLaterCheck);
        ivNowCheck=findViewById(R.id.ivNowCheck);
        ivRepeatCheck=findViewById(R.id.ivRepeatCheck);
        rlSelectedCategory=findViewById(R.id.rlSelectedCategory);
        tvJobDesc=findViewById(R.id.tvJobDesc);
        etJobDescription=findViewById(R.id.etJobDescription);
        rlSelectedCategory.setVisibility(View.GONE);
        tvNow=findViewById(R.id.tvNow);
        tvSchedule=findViewById(R.id.tvSchedule);
        tvTodaysDate=findViewById(R.id.tvTodaysDate);
        ivCalendar.setOnClickListener(this);
        glNowGrid.setOnClickListener(this);
        glLaterGrid.setOnClickListener(this);
        glRepeatGrid.setOnClickListener(this);
        glLaterPartGrid.setOnClickListener(this);
        rlSelectPaymentMethod.setOnClickListener(this);
        etConfirmPromoCodes = findViewById(R.id.etConfirmPromoCodes);
        tvVisitFeeAmt = findViewById(R.id.tvVisitFeeAmt);
        tvDiscountAmount = findViewById(R.id.tvDiscountAmount);
        tvTotalAmt = findViewById(R.id.tvTotalAmt);
        llLiveFee = findViewById(R.id.llLiveFee);
        appBarLayout = findViewById(R.id.app_bar);
        tvWalletBalance=findViewById(R.id.tvWalletBalance);
        toolBarTitle.setText(VariableConstant.SELECTEDCATEGORYNAME);
        checkforCalendarPermission();
        checkNowBooking();
        checkLaterBooking();
        checkRepeatBooking();
        tvConfirmAddWalletMoney=findViewById(R.id.tvConfirmAddWalletMoney);
        toolBarTitle.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvWhentoBook.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvNow.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvSchedule.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvTodaysDate.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvTodaysTime.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvNowDesc.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvLaterDesc.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_lastdues.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        etJobDescription.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvJobDesc.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tv_lastdues_Amt.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_service_price.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvSelTime.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvSelDate.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvWalletBalance.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvConfirmAddWalletMoney.setTypeface(AppTypeface.getInstance(this).getHind_regular());

        tvWalletBalance.setText(String.format("My Wallet (%s %s)", VariableConstant.walletCurrency, VariableConstant.walletBalance));

        ivCardBrand=findViewById(R.id.ivCardBrand);
        ivRightArrow=findViewById(R.id.ivRightArrow);
        ivRightArrow.setOnClickListener(this);
        rlconfm_visitfee=findViewById(R.id.rlconfm_visitfee);
        rlconfm_visitfee.setVisibility(View.GONE);
        rlCashPaymentMethod.setOnClickListener(this);
        rltravel_fee=findViewById(R.id.rltravel_fee);
        rltravel_fee.setVisibility(View.GONE);
        tvWillLetUKnw=findViewById(R.id.tvWillLetUKnw);
        tvYahBookingDone=findViewById(R.id.tvYahBookingDone);
        ivTickCheck=findViewById(R.id.ivTickCheck);
        progress_bar=findViewById(R.id.progress_bar);
        rlWalletLayout=findViewById(R.id.rlWalletLayout);
        rlWalletLayout.setOnClickListener(this);
        appTypeface = AppTypeface.getInstance(this);


        //multiple shifts

        glRepeatPartGrid=findViewById(R.id.glRepeatPartGrid);
        glRepeatPartGrid.setVisibility(View.GONE);
        glRepeatPartTimeGrid=findViewById(R.id.glRepeatPartTimeGrid);
        glRepeatPartDurationGrid = findViewById(R.id.glRepeatPartDurationGrid);
        glRepeatPartDaysGrid = findViewById(R.id.glRepeatPartDaysGrid);


        multipleShiftBooking=findViewById(R.id.multipleShiftBooking);
        tvSelRepeatStartDate=findViewById(R.id.tvSelRepeatStartDate);

        glRepeatPartGrid.setOnClickListener(this);
        glRepeatPartTimeGrid.setOnClickListener(this);
        glRepeatPartDurationGrid.setOnClickListener(this);
        glRepeatPartDaysGrid.setOnClickListener(this);

        tvRepeatStartTime=findViewById(R.id.tvRepeatStartTime);

        tvTodaysRepeatDays=findViewById(R.id.tvTodaysRepeatDays);
        tvSelRepeatDuration=findViewById(R.id.tvSelRepeatDuration);
        tvSelRepeatDurationTime = findViewById(R.id.tvSelRepeatDurationTime);
        tvTodaysRepeatStartDate = findViewById(R.id.tvTodaysRepeatStartDate);

        tvEditShiftDetails=findViewById(R.id.tvEditShiftDetails);
        tvEditShiftDetails.setVisibility(View.VISIBLE);
        tvEditShiftDetails.setOnClickListener(this);

        tvConfirmStartTime=findViewById(R.id.tvConfirmStartTime);
        tvConfirmEndTime=findViewById(R.id.tvConfirmEndTime);

        tvConfirmStartDate=findViewById(R.id.tvConfirmStartDate);
        tvConfirmEndDate=findViewById(R.id.tvConfirmEndDate);

        llConfirmScheduledDays=findViewById(R.id.llConfirmScheduledDays);
        tvConfirmNumberOfShift=findViewById(R.id.tvConfirmNumberOfShift);


        myPermissionConstantsArrayList = new ArrayList<>();
        myPermissionConstantsArrayList.clear();
        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);

        jobPhotosList=new ArrayList<>();
        rv_photos = findViewById(R.id.rv_photos);

        jobPhotosAdapter = new ServiceFlowJobPhotosAdapter(jobPhotosList,this,true);
        rv_photos.setLayoutManager(new GridLayoutManager(this,4,GridLayoutManager.VERTICAL,false));
        rv_photos.setAdapter(jobPhotosAdapter);

        setMultipleShiftTypeFace();

        if(VariableConstant.BOOKINGTYPE==1)
        {
            glLaterPartGrid.setVisibility(View.GONE);
            ivNowCheck.setImageResource(R.drawable.check_on);
            ivLaterCheck.setImageResource(R.drawable.check_off);
            tvNow.setTextColor(getResources().getColor(R.color.notaryAccent));
            tvNowDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
            tvSchedule.setTextColor(getResources().getColor(R.color.toolbarColor));
            tvLaterDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
            VariableConstant.SCHEDULEAVAILABLE=true;
            glLaterPartGrid.setVisibility(View.GONE);
            glLaterTimePart.setVisibility(View.GONE);
        } else {
            glLaterPartGrid.setVisibility(View.VISIBLE);
            glLaterTimePart.setVisibility(View.VISIBLE);
            ivNowCheck.setImageResource(R.drawable.check_off);
            ivLaterCheck.setImageResource(R.drawable.check_on);
            tvNow.setTextColor(getResources().getColor(R.color.toolbarColor));
            tvNowDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
            tvSchedule.setTextColor(getResources().getColor(R.color.notaryAccent));
            tvLaterDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
            glLaterPartGrid.setVisibility(View.VISIBLE);
            glLaterTimePart.setVisibility(View.VISIBLE);
            tvTodaysDate.setTextColor(getResources().getColor(R.color.notaryAccent));

            setScheduledDate();
            setScheduledTime();
        }
        setPaymentBreakdown();
    }

    private void setMultipleShiftTypeFace()
    {
        tvConfirmStartTime.setTypeface(appTypeface.getHind_light());
        tvConfirmStartDate.setTypeface(appTypeface.getHind_light());
        tvConfirmEndTime.setTypeface(appTypeface.getHind_light());
        tvConfirmEndDate.setTypeface(appTypeface.getHind_light());
        tvConfirmNumberOfShift.setTypeface(appTypeface.getHind_semiBold());
    }

    private void checkNowBooking(){
        if(VariableConstant.BOOKINGTYPEAVAILABILITY.getNow()){
            glNowGrid.setVisibility(View.VISIBLE);

            /*if(VariableConstant.BOOKINGTYPEAVAILABILITY.getNow() && VariableConstant.BOOKINGTYPEAVAILABILITY.getSchedule()==false && VariableConstant.BOOKINGTYPEAVAILABILITY.getRepeat()==false){
                glNowGrid.setVisibility(View.GONE);
                SCHEDULEAVAILABLE=true;
                checkScheduleAndQuantity();
                VariableConstant.BOOKINGTYPE=1;
            }*/
        }else{
            glNowGrid.setVisibility(View.GONE);
        }
    }

    private void checkLaterBooking() {
        if(VariableConstant.BOOKINGTYPEAVAILABILITY.getSchedule()){
            glLaterGrid.setVisibility(View.VISIBLE);
        }else{
            glLaterGrid.setVisibility(View.GONE);
        }
    }

    private void checkRepeatBooking() {
        if(VariableConstant.BOOKINGTYPEAVAILABILITY.getRepeat()){
            glRepeatGrid.setVisibility(View.VISIBLE);
        }else{
            glRepeatGrid.setVisibility(View.GONE);
        }
    }

    private float dpToPx(ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener, int valueInDp) {
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
    public static int convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) (px / (metrics.densityDpi / 160f));
        return dp;
    }
    private void setScheduledTime() {
        Calendar calendar=Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        if(VariableConstant.SCHEDULEDDATE.equals("")){
            calendar.setTime(new Date(System.currentTimeMillis()-sPrefs.getServerTimeDifference()));
        }else{
            calendar.setTimeInMillis(Long.parseLong(VariableConstant.SCHEDULEDDATE)*1000);
        }
        tvTodaysTime.setText(getFormattedTime(calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE)));
    }

    private void setScheduledDate() {
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
        if(VariableConstant.SCHEDULEDDATE.equals("")){
            calendar.setTime(new Date(System.currentTimeMillis()-sPrefs.getServerTimeDifference()));
        }else{
            calendar.setTimeInMillis(Long.parseLong(VariableConstant.SCHEDULEDDATE)*1000);
        }
        tvTodaysDate.setText(getFormattedDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)));
    }


    private void typeFace() {
        if(getIntent().hasExtra("SERVICEFEE")){
            double totalAmt=getIntent().getDoubleExtra("SERVICEFEE",0.0);
            Log.d(TAG, "typeFace: "+totalAmt);
            tvTotalAmt.setText(sPrefs.getCurrencySymbol()+" "+ String.format("%.2f",totalAmt));
            serviceFee=totalAmt;
            dummyserviceFee = totalAmt;
        }
        travelFee=VariableConstant.SELECTEDCATEGORYVISITFEE;
        checkCartPojo();
        confirmBookingServicesAdapter=new ConfirmBookingServicesAdapter(ConfirmBookingActivity.this,cartPojo,sPrefs,this,dataShowOnAdapter);
        rv_selected_services.setLayoutManager(new LinearLayoutManager(this));
        rv_selected_services.setAdapter(confirmBookingServicesAdapter);
        setTotalAmount(serviceFee, travelFee, discountFee);
        tvConfirmBook = findViewById(R.id.tvConfirmBook);
        TextView tvConfirmAppAdd = findViewById(R.id.tvConfirmAppAdd);
        TextView tvConfirmAppcngadd = findViewById(R.id.tvConfirmAppcngadd);

        TextView tvConfirmPayment = findViewById(R.id.tvConfirmpayment);
        TextView tvConfirmDistance = findViewById(R.id.tvConfirmDistance);

        TextView tvConfirmPromoCode = findViewById(R.id.tvConfirmPromoCode);

        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvPaymentBreak = findViewById(R.id.tvPaymentBreak);
        tvPaymentBreak.setVisibility(View.GONE);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotal = findViewById(R.id.tvTotal);

        tvVisitFee.setTypeface(appTypeface.getHind_regular());
        tvPaymentBreak.setTypeface(appTypeface.getHind_semiBold());
        tvDiscount.setTypeface(appTypeface.getHind_regular());
        tvTotal.setTypeface(appTypeface.getHind_bold());
        tvVisitFeeAmt.setTypeface(appTypeface.getHind_regular());
        tv_travelFee.setTypeface(appTypeface.getHind_regular());
        tv_travelFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvDiscountAmount.setTypeface(appTypeface.getHind_regular());
        tvTotalAmt.setTypeface(appTypeface.getHind_bold());
        tvWillLetUKnw.setTypeface(appTypeface.getHind_regular());
        tvYahBookingDone.setTypeface(appTypeface.getHind_regular());
        tvConfirmAddCard = findViewById(R.id.tvConfirmAddCard);
        tvConfirmCardNumber = findViewById(R.id.tvConfirmCardNumber);
        rlCardLayout=findViewById(R.id.rlCardLayout);
        rlCardLayout.setOnClickListener(this);

        confirmBookCont.distanceCal(distance, tvConfirmDistance, this);

        tvConfirmAppAddress = findViewById(R.id.tvConfirmAppAddress);
        tvConfirmAppcngadd.setOnClickListener(this);
        tvConfirmBook.setOnClickListener(this);
        tvConfirmPromoApply.setOnClickListener(this);
        tvConfirmAppAddress.setText(sPrefs.getBookingAddress());
        tvConfirmAppAddress.setTypeface(appTypeface.getHind_regular());


        Log.d(TAG, "typeFace: "+sPrefs.getDefaultCardNum()+" Done");

        String selectedMTD = sPrefs.getPaymentToShow();
        Log.i("selectedMTD","selectedMTD "+selectedMTD);

        /*if(selectedMTD.equalsIgnoreCase("cash"))
        {
            rlCashPaymentMethod.setVisibility(View.VISIBLE);
            rlCardLayout.setVisibility(View.GONE);
            rlSelectPaymentMethod.setVisibility(View.GONE);
            rlWalletLayout.setVisibility(View.GONE);
            tvConfirmBook.setClickable(false);
            tvConfirmBook.setEnabled(false);

        }else  if(selectedMTD.equalsIgnoreCase("card"))
        {
            tvConfirmAddCard.setText(R.string.change);
            tvConfirmAddCard.setVisibility(View.VISIBLE);
            rlCardLayout.setVisibility(View.VISIBLE);
            rlCashPaymentMethod.setVisibility(View.GONE);
            rlSelectPaymentMethod.setVisibility(View.GONE);
            rlWalletLayout.setVisibility(View.GONE);
            String card = sPrefs.getDefaultCardNum();
            Log.d("CARD", "typeFace: "+sPrefs.getDefaultCardNum());
            tvConfirmCardNumber.setText(getString(R.string.card_stars)+card);
            paymentType = 2;
            paymentCardId = sPrefs.getDefaultCardId();
            ivCardBrand.setImageBitmap(Utilities.setCreditCardLogo(sPrefs.getDefaultCardBrand(),this));
        } else if(selectedMTD.equalsIgnoreCase(""))
        {
            rlCardLayout.setVisibility(View.GONE);
            rlCashPaymentMethod.setVisibility(View.GONE);
            rlSelectPaymentMethod.setVisibility(View.VISIBLE);
            rlWalletLayout.setVisibility(View.GONE);
            tvConfirmBook.setClickable(false);
            tvConfirmBook.setEnabled(false);
        }*/


        if(!sPrefs.getDefaultCardNum().trim().equals("") &&VariableConstant.CARDBOOKING ) {
            tvConfirmAddCard.setText(R.string.change);
            tvConfirmAddCard.setVisibility(View.VISIBLE);
            rlCardLayout.setVisibility(View.VISIBLE);
            rlCashPaymentMethod.setVisibility(View.GONE);
            rlSelectPaymentMethod.setVisibility(View.GONE);
            rlWalletLayout.setVisibility(View.GONE);
            String card = sPrefs.getDefaultCardNum();
            Log.d("CARD", "typeFace: "+sPrefs.getDefaultCardNum());
            tvConfirmCardNumber.setText(getString(R.string.card_stars)+card);
            paymentType = 2;
            paymentCardId = sPrefs.getDefaultCardId();
            ivCardBrand.setImageBitmap(Utilities.setCreditCardLogo(sPrefs.getDefaultCardBrand(),this));
        }else{
            rlCardLayout.setVisibility(View.GONE);
            rlCashPaymentMethod.setVisibility(View.GONE);
            rlSelectPaymentMethod.setVisibility(View.VISIBLE);
            rlWalletLayout.setVisibility(View.GONE);
            //tvConfirmBook.setClickable(false);
            //tvConfirmBook.setEnabled(false);
            paymentType=-1;
        }

        tvConfirmCardNumber.setTypeface(appTypeface.getHind_regular());
        tvConfirmAppAdd.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmAppcngadd.setTypeface(appTypeface.getHind_regular());
        tvConfirmAddCard.setTypeface(appTypeface.getHind_regular());
        tvConfirmPayment.setTypeface(appTypeface.getHind_semiBold());
        etConfirmPromoCodes.setTypeface(appTypeface.getHind_regular());
        tvConfirmPromoApply.setTypeface(appTypeface.getHind_regular());
        tvConfirmBook.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmPromoCode.setTypeface(appTypeface.getHind_semiBold());
        disablePromoCodeBtn();


        etConfirmPromoCodes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etConfirmPromoCodes.getText().toString().length()>2) {
                    enablePromoCode();
                } else {
                    disablePromoCodeBtn();
                }
            }
        });
    }

    private void enablePromoCode() {
        tvConfirmPromoApply.setTextColor(Utilities.getColor(ConfirmBookingActivity.this, R.color.red_login));
        tvConfirmPromoApply.setClickable(true);
    }

    private void disablePromoCodeBtn() {
        tvConfirmPromoApply.setClickable(false);
        tvConfirmPromoApply.setTextColor(Utilities.getColor(ConfirmBookingActivity.this, R.color.colorAccent));
    }


    private void checkDefaultCard() {
        String cardNo = sPrefs.getDefaultCardNum();
        paymentCardId = sPrefs.getDefaultCardId();
        cardBrand=sPrefs.getDefaultCardBrand();
        if(!cardNo.trim().equals("")){
            tvConfirmCardNumber.setText(getString(R.string.card_stars)+" "+cardNo);
            ivCardBrand.setImageBitmap(Utilities.setCreditCardLogo(sPrefs.getDefaultCardBrand(),this));
        }
    }

    private void getCart() {
        if(Utilities.isNetworkAvailable(this)){
            showProgress();
            confirmBookCont.getCartServiceCall(sPrefs,sPrefs.getCategoryId(),this);
        }else{
            Toast.makeText(this,"Please check Internet connection.",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        checkDefaultCard();
        checkWalletBalanceUpdate();
        checkScheduleAndQuantity();
    }

    public void setPaymentBreakdown(){
        //Inflating layout and adding
        llLiveFee.removeAllViews();
        setTotalAmount(serviceFee, travelFee, discountFee);
    }

    private void setTotalAmount(double serviceFee, double travelFee, double discountFee) {
        Utilities.setAmtOnRecept(travelFee,tvVisitFeeAmt,currencySymbol);
        if(discountFee>0){
            rldiscount.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(discountFee,tvDiscountAmount,currencySymbol);
        }else{
            rldiscount.setVisibility(View.GONE);
        }
        if(travelFee>0){
            rlconfm_visitfee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(travelFee,tv_travelFeeAmt,currencySymbol);
        }else{
            rlconfm_visitfee.setVisibility(View.GONE);
        }

        // double totalservicefee = serviceFee;
        //  double remainingfee = travelFee -discountFee +lastDuesAmount;
        //  total = serviceFee - discountFee +travelFee+lastDuesAmount;
        /*if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE))
        {
            total = serviceFee;
        }else{
            total = serviceFee - discountFee +travelFee+lastDuesAmount;
        }*/

        total = serviceFee - discountFee +travelFee+lastDuesAmount;


        if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE))
        {
            double service_price_surge=0.0;
            double final_value = 0.0;
            try {
                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*serviceFee));
                final_value = service_price_surge -discountFee +travelFee + lastDuesAmount;
                Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                Utilities.setAmtOnRecept(final_value,tvTotalAmt,currencySymbol);
            } catch (Exception e) {
                Toast.makeText(ConfirmBookingActivity.this, TAG+ " Surge not applied", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }finally {
            }
        } else {
            Utilities.setAmtOnRecept(total,tvTotalAmt,currencySymbol);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvConfirmPromoApply:
                promoCode=etConfirmPromoCodes.getText().toString();
                if(Utilities.isNetworkAvailable(ConfirmBookingActivity.this)){
                    showProgress();
                    confirmBookCont.validatePromocode(sPrefs,paymentType,paidByWallet,promoCode);
                }else{
                    Toast.makeText(ConfirmBookingActivity.this,"Please check network connectivity",Toast.LENGTH_SHORT).show();
                }
                progressBarConfirm.setVisibility(View.VISIBLE);
                break;
            case R.id.tvConfirmBook:
                if(aProgress.isNetworkAvailable())
                    if(paymentType==2 && sPrefs.getDefaultCardId().equalsIgnoreCase("")){
                        onError("Card not selected or added!");
                    }

                    /**
                     * <h2>showCardSelectionAlert</h2>
                     * <p>show an alert dialog to take user to choose the card</p>
                     */

                else if(paymentType == -1){
                        new android.app.AlertDialog.Builder(this)
                                .setTitle(getString(R.string.alert_cap))
                                .setMessage(getString(R.string.select_payment_method))
                                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                    }
                else{
                        Log.e(TAG, "onClick: BookLat BookLong  "+sPrefs.getBookLatitude()+"  "+sPrefs.getBookLongitude());
                        if (!TextUtils.isEmpty(sPrefs.getBookLatitude())&&!TextUtils.isEmpty(sPrefs.getBookLongitude())) {
                            jobPhotosAdapter.setPhotos();
                            uploadImagesToServer();
                            if(VariableConstant.STUFFIMAGES.size()==0)
                            liveBooking();
                            else
                                uploadImagesToServer();
                            rlMAin.setVisibility(View.VISIBLE);
                            appBarLayout.setVisibility(View.GONE);
                        } else {
                            Log.e(TAG, "backaddress:  Please select Job Location");
                        }
                    }
                else
                    aProgress.showNetworkAlert();
                break;
            case R.id.tvConfirmAppcngadd:
                Intent intent = new Intent(this, AddressConfirm.class);
                startActivityForResult(intent, ADDRESS_CODE);
                break;
            case R.id.glNowGrid:

                glLaterPartGrid.setVisibility(View.GONE);
                ivNowCheck.setImageResource(R.drawable.check_on);
                ivRepeatCheck.setImageResource(R.drawable.check_off);
                ivLaterCheck.setImageResource(R.drawable.check_off);
                tvNow.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvNowDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvSchedule.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvLaterDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvRepeat.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvRepeatDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                glLaterPartGrid.setVisibility(View.GONE);
                glLaterTimePart.setVisibility(View.GONE);
                glRepeatPartGrid.setVisibility(View.GONE);
                multipleShiftBooking.setVisibility(View.GONE);

                /*glLaterPartGrid.setVisibility(View.GONE);
                ivNowCheck.setImageResource(R.drawable.check_on);
                ivLaterCheck.setImageResource(R.drawable.check_off);
                tvNow.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvNowDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvSchedule.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvLaterDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                glLaterPartGrid.setVisibility(View.GONE);
                glLaterTimePart.setVisibility(View.GONE);*/
                VariableConstant.SCHEDULEAVAILABLE=true;
                checkScheduleAndQuantity();
                VariableConstant.BOOKINGTYPE=1;

                if (Utilities.isNetworkAvailable(this))
                {

                    String latitude = String.valueOf(VariableConstant.NOWBOOKINGLAT);
                    String longitude = String.valueOf(VariableConstant.NOWBOOKINGLONG);
                    showProgress();
                    confirmBookCont.getSurgePrice(sPrefs,Double.parseDouble(latitude),Double.parseDouble(longitude));
                } else {
                    Toast.makeText(ConfirmBookingActivity.this,"Please check internet connectivity",Toast.LENGTH_SHORT).show();
                }
                break;

            //Showing selectPayment activity if any of this is changed
            case R.id.rlSelectPaymentMethod:
            case R.id.rlCashPaymentMethod:
            case R.id.rlCardLayout:
            case R.id.rlWalletLayout:
                if(promoCodeActive){
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage(R.string.this_promcode_invalid);
                    alertDialogBuilder.setTitle("Alert");
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Intent for selectPayment activity
                            Intent intent2=new Intent(ConfirmBookingActivity.this,SelectPayment.class);
                            startActivityForResult(intent2,SELECT_PAYMENT_CODE);
                        }
                    });
                    alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    if(!isFinishing())
                        alertDialogBuilder.create().show();
                }else{
                    Intent intent2=new Intent(this,SelectPayment.class);
                    startActivityForResult(intent2,SELECT_PAYMENT_CODE);
                }

                break;
            case R.id.glLaterGrid:

                glLaterPartGrid.setVisibility(View.VISIBLE);
                ivNowCheck.setImageResource(R.drawable.check_off);
                ivRepeatCheck.setImageResource(R.drawable.check_off);
                ivLaterCheck.setImageResource(R.drawable.check_on);
                tvNow.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvNowDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvSchedule.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvLaterDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvRepeat.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvRepeatDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                glLaterPartGrid.setVisibility(View.VISIBLE);
                glLaterTimePart.setVisibility(View.VISIBLE);
                glRepeatPartGrid.setVisibility(View.GONE);
                glRepeatPartTimeGrid.setVisibility(View.GONE);
                multipleShiftBooking.setVisibility(View.GONE);
                tvTodaysDate.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvConfirmBook.setClickable(false);
                tvConfirmBook.setEnabled(false);
                setScheduledDate();
                setScheduledTime();
                VariableConstant.BOOKINGTYPE=2;
                break;


                /*glLaterPartGrid.setVisibility(View.VISIBLE);
                ivNowCheck.setImageResource(R.drawable.check_off);
                ivLaterCheck.setImageResource(R.drawable.check_on);
                tvNow.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvNowDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvSchedule.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvLaterDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
                glLaterPartGrid.setVisibility(View.VISIBLE);
                glLaterTimePart.setVisibility(View.VISIBLE);
                tvTodaysDate.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvConfirmBook.setClickable(false);
                tvConfirmBook.setEnabled(false);
                setScheduledDate();
                VariableConstant.BOOKINGTYPE=2;
                break;*/

            case R.id.glRepeatGrid:

                VariableConstant.BOOKINGTYPE=3;
                VariableConstant.SCHEDULEAVAILABLE=true;
                glRepeatGrid.setVisibility(View.VISIBLE);
                ivNowCheck.setImageResource(R.drawable.check_off);
                ivLaterCheck.setImageResource(R.drawable.check_off);
                ivRepeatCheck.setImageResource(R.drawable.check_on);
                tvNow.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvNowDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvSchedule.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvLaterDesc.setTextColor(getResources().getColor(R.color.toolbarColor));
                tvRepeat.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvRepeatDesc.setTextColor(getResources().getColor(R.color.notaryAccent));
                glLaterPartGrid.setVisibility(View.GONE);
                glLaterTimePart.setVisibility(View.GONE);
                glRepeatPartGrid.setVisibility(View.VISIBLE);
                tvSelRepeatStartDate.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvConfirmBook.setClickable(false);
                tvConfirmBook.setEnabled(false);
                break;

                /*ivNowCheck.setImageResource(R.drawable.check_off);
                ivLaterCheck.setImageResource(R.drawable.check_off);
                ivRepeatCheck.setImageResource(R.drawable.check_on);
                tvNow.setTextColor(getResources().getColor(R.color.default_text_grey));
                tvSchedule.setTextColor(getResources().getColor(R.color.default_text_grey));
                tvRepeat.setTextColor(getResources().getColor(R.color.notaryAccent));
                glLaterPartGrid.setVisibility(View.GONE);
                glLaterTimePart.setVisibility(View.GONE);
                VariableConstant.BOOKINGTYPE=3;
                break;*/

            case R.id.glRepeatPartGrid:
                callRepeatDateTimeIntent(false);
                break;

            case R.id.glRepeatPartTimeGrid:
                onSelectTime(false,startDates);
                break;
            case R.id.glRepeatPartDurationGrid:
                //   onSelectedDuration(true);
                //  callIntent(false,"");
                break;
            case R.id.glRepeatPartDaysGrid:
                selectDays();
                break;

            case R.id.tvEditShiftDetails:
                callRepeatDateTimeIntent(true);
                break;


            case R.id.ivCalendar:
            case R.id.glLaterPartGrid:
                ivNowCheck.setImageResource(R.drawable.check_off);
                ivLaterCheck.setImageResource(R.drawable.check_on);
                tvNow.setTextColor(getResources().getColor(R.color.default_text_grey));
                tvSchedule.setTextColor(getResources().getColor(R.color.notaryAccent));
                glLaterTimePart.setVisibility(View.VISIBLE);
                tvTodaysDate.setTextColor(getResources().getColor(R.color.notaryAccent));
                tvConfirmBook.setClickable(false);
                tvConfirmBook.setEnabled(false);
                setScheduledDate();
                openDate_Picker(this);
                break;
            case R.id.glLaterTimePart:
            case R.id.ivRightArrow:
                tvConfirmBook.setClickable(false);
                tvConfirmBook.setEnabled(false);
                calltimepicker();
                break;

        }
    }

    private void callRepeatDateTimeIntent(boolean isEdit) {

        Intent intent = new Intent(this, RepeatDateTime.class);
        intent.putExtra("isEditTrue",isEdit);
        startActivityForResult(intent,VariableConstant.REPEAT_RESULT_CODE);
        overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
    }

    public void onSelectTime(boolean isScheduled, Date calendar) {
        // i am doing
        String time = simpleDateFormat.format(calendar.getTime());
        Log.d(TAG, "onSelectTime: "+time);
        if(isScheduled)
        {
            DialogTimeFragment dialogTimeFragment = DialogTimeFragment.newInstance(true,time,
                    isCustomSelected,selectedDays,currentDay);
            dialogTimeFragment.show(getSupportFragmentManager(),"Timer");

        }else
            callIntent(time);
    }

    private void selectDays()
    {
        allDayOfTheWeek.clear();
        if(dayMap.size()>0)
        {
            ArrayList<Integer> keyset = new ArrayList<>(dayMap.keySet());

            for(int i = 0; i<keyset.size();i++)
            {
                allDayOfTheWeek.add(dayMap.get(keyset.get(i)));
            }
            Intent intent = new Intent(this, RepeatDays.class);
            intent.putExtra("SelectedDays",selectedDays);
            intent.putExtra("allDaysOfTheWeek",allDayOfTheWeek);
            startActivityForResult(intent,4);
        }
    }

    private void callIntent(String time) {

        //isCustomSelected,selectedDays,currentDay
        Intent intent = new Intent(this, SelectTimeAndDuration.class);

        Bundle args = new Bundle();
        durationActivityCalled=true;
        args.putString(SelectTimeAndDuration.ARG_PARAM1,time);
        args.putBoolean(SelectTimeAndDuration.ARG_PARAM2, false);
        args.putBoolean(SelectTimeAndDuration.ARG_PARAM_NOW, false);
        args.putBoolean(SelectTimeAndDuration.ARG_CUSTOME, isCustomSelected);
        args.putStringArrayList(SelectTimeAndDuration.ARG_SELECTED, selectedDays);
        args.putString(SelectTimeAndDuration.ARG_CurrentDay, currentDay);
        intent.putExtras(args);
        startActivityForResult(intent,VariableConstant.TIME_RESULT_CODE);
        overridePendingTransition(R.anim.side_slide_out,R.anim.stay_still);

    }

    private void checkWalletBalanceUpdate() {
        Utilities.setAmtOnRecept(Double.parseDouble(VariableConstant.walletBalance),tvWalletBalance,VariableConstant.walletCurrency);
    }

    @Override
    public void onSuccessPromocode(PromocodePojo pojo) {
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("Message");
        builder.setMessage("Promocode is added successfully !");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                etConfirmPromoCodes.post(new Runnable() {
                    @Override
                    public void run() {
                        Utilities.hideKeyBoard(ConfirmBookingActivity.this,etConfirmPromoCodes);
                    }
                });
            }
        });
        if(!isFinishing()){
            builder.create().show();
        }

        promoCodeActive=true;
        previousPaymentType=paymentType;
        previousPaidByWallet=paidByWallet;
        discountFee=pojo.getData().getDiscountAmount();
        setTotalAmount(serviceFee,travelFee,discountFee);
        hideProgress();
    }

    @Override
    public void onSuccessSurgePrice(SurgePriceResponse response) {
        hideProgress();
        if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE)) {
            confirmBookingServicesAdapter.notifyDataSetChanged();
            double service_price_surge=0.0;
            double final_value = 0.0;
            try {
                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*serviceFee));
                final_value = service_price_surge -discountFee +travelFee + lastDuesAmount;
                Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                Utilities.setAmtOnRecept(final_value,tvTotalAmt,currencySymbol);
            } catch (Exception e) {
                Toast.makeText(ConfirmBookingActivity.this, TAG+ " Surge not applied", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }finally {
            }
        } else {
            Utilities.setAmtOnRecept(total,tvTotalAmt,currencySymbol);
        }


    }



    @Override
    public void onErrorPromocode(String message) {
        clearPromocode();
        onError(message);
        hideProgress();
    }

    @Override
    public void onFailureGetCart(String headerError, String error) {
        hideProgress();
        try{
            ErrorHandel errorHandel=gson.fromJson(error,ErrorHandel.class);
            switch (headerError){
                case "416":
                    //Cart not created in ConfirmBooking page only happens in hourly booking services ie: billing model 3 and 4
                    tvConfirmBook.setClickable(false);
                    tvConfirmBook.setEnabled(false);
                    if(Utilities.isNetworkAvailable(this)) {
                        if(sPrefs.getBillingModel().equals("3") || sPrefs.getBillingModel().equals("4"))
                            onCartModified("1",1);
                    }else{
                        Toast.makeText(this, "Please check internet connectivity!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "440":
                    Utilities.getAccessToken(errorHandel.getData(),this);
                    break;
            }
            Log.d(TAG, "onFailureGetCart: code:"+headerError+" ErrorMsg"+error);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessGetCart(String headerData, String result) {
        hideProgress();
        switch (headerData){
            case "200":
                cartPojo=gson.fromJson(result,CartModifiedPojo.class);
                VariableConstant.CARTID=cartPojo.getData().get_id();
                serviceFee=cartPojo.getData().getTotalAmount();
                dummyserviceFee = cartPojo.getData().getTotalAmount();
                confirmBookingServicesAdapter.setCartModifiedPojo(cartPojo);
                confirmBookingServicesAdapter.notifyDataSetChanged();
                Log.d(TAG, "onSuccessGetCart: "+cartPojo.getData().getTotalQuntity());
                if(cartPojo.getData().getTotalQuntity()>0 && paymentType!=-1){
                    tvConfirmBook.setClickable(true);
                    tvConfirmBook.setEnabled(true);
                }else{
                   // tvConfirmBook.setClickable(false);
                   // tvConfirmBook.setEnabled(false);
                }
                if(cartPojo.getData().getServiceType()==2)
                {
                    if (cartQuantity >= VariableConstant.MINIMUM_HOUR) {
                        minimumHr = true;
                    } else {
                        minimumHr = false;
                    }
                    isHourlyService = true;
                }else
                    isHourlyService=false;
                cartQuantity=cartPojo.getData().getTotalQuntity();
                Log.i("SAITESTING","cartQuantity--> "+cartQuantity);
                VariableConstant.SCHEDULEDTIME = cartQuantity;
                setToolBarQuatity();
                checkScheduleAndQuantity();
                break;
        }
        Log.d(TAG, "onSuccessGetCart: "+result);
        setPaymentBreakdown();
    }




    private void setToolBarQuatity()
    {
        if(!TextUtils.isEmpty(VariableConstant.SURGE_PRICE)){
            double service_price_surge=0.0;
            try {
                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*cartPojo.getData().getTotalAmount()));
                Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                totalAmount = Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*cartPojo.getData().getTotalAmount()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            totalAmount = cartPojo.getData().getTotalAmount();
        }

        if(isHourlyService)
        {

            Double unitPrice;
            if(!TextUtils.isEmpty(VariableConstant.SURGE_PRICE)){
                unitPrice = Double.parseDouble(cartPojo.getData().getItem().get(0).getUnitPrice())*Double.parseDouble(VariableConstant.SURGE_PRICE);
            }else{
                unitPrice = Double.parseDouble(cartPojo.getData().getItem().get(0).getUnitPrice());
            }

            String finalServicePrice = "Hourly @ "+cartPojo.getData().getCurrencySymbol()+""+String.format("%.2f",unitPrice)+"/hr";
            Log.i("finalServicePrice","finalServicePrice --> "+finalServicePrice);

            tv_service_price.setText(finalServicePrice);

            //  tv_service_price.setText(String.format("%s for %s hr", cartPojo.getData().getItem().get(0).getServiceName(), totalAmount));
            //   tv_service_price.setText(String.format("%d service for %s %.2f",finalServicePrice));
            dataShowOnAdapter = finalServicePrice;

        }else{

            tv_service_price.setText(String.format("%d service for %s %.2f", cartPojo.getData().getItem().size(), cartPojo.getData().getCurrencySymbol(), totalAmount));
            dataShowOnAdapter = tv_service_price.getText().toString();
        }

    }

    @Override
    public void onCardSuccess(String headerError, String result) {
        try{
            CardDetailsResp response = gson.fromJson(result, CardDetailsResp.class);
            CardDetailsResp.CardDetail cardDetail;
            if(response.getData().size()>0){
                for(int i=0;i<response.getData().size();i++){
                    cardDetail = response.getData().get(i);
                    if(cardDetail.isDefault()){
                        sPrefs.setDefaultCardNum(cardDetail.getLast4());
                        sPrefs.setDefaultCardId(cardDetail.getId());
                        sPrefs.setDefaultCardBrand(cardDetail.getBrand());
                    }
                }
            }else{
                sPrefs.setDefaultCardNum("");
                sPrefs.setDefaultCardId("");
                sPrefs.setDefaultCardBrand("");
            }
            //checkDefaultCard();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCardFailure(String headerError, String result) {
        checkDefaultCard();
    }

    private void checkScheduleAndQuantity() {


        if(VariableConstant.SCHEDULEAVAILABLE)
        {
            if(cartQuantity>0 && paymentType!=-1){
                tvConfirmBook.setClickable(true);
                tvConfirmBook.setEnabled(true);
            }else{
               // tvConfirmBook.setClickable(false);
               // tvConfirmBook.setEnabled(false);
            }
        }else{
            tvConfirmBook.setClickable(false);
            tvConfirmBook.setEnabled(false);
        }
    }


    public void clearPromocode(){
        promoCode="";
        etConfirmPromoCodes.setText("");
        discountFee=0;
        promoCodeActive=false;
        setTotalAmount(serviceFee,travelFee,discountFee);
    }

    private void liveBooking() {
        if(Utilities.isNetworkAvailable(ConfirmBookingActivity.this)){
            showProgress();
            confirmBookCont.liveBooking(sPrefs,VariableConstant.BOOKINGMODEL, paymentType,paidByWallet, proId,etJobDescription.getText().toString(), promoCode, paymentCardId,bookingTime,noofHours,jobPhotosArray);
        }else{
            Toast.makeText(ConfirmBookingActivity.this,"Please check internet connectivity",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Reset to the NOW BOOKING values by default on Back Press
        if (glNowGrid!=null)
            onClick(glNowGrid);
        overridePendingTransition(R.anim.stay_still,R.anim.side_slide_in);
    }

    private void selectStartEventTime(int i) {
        switch (i) {
            case 30:
                //veConfirmevent1.setVisibility(View.VISIBLE);
                //tvConfirmEventTime1.setSelected(true);
                bookingTime = 30;
                break;
            case 60:
                //veConfirmevent2.setVisibility(View.VISIBLE);
                //tvConfirmEventTime2.setSelected(true);
                bookingTime = 60;
                break;
            case 120:
                //veConfirmevent3.setVisibility(View.VISIBLE);
                //tvConfirmEventTime3.setSelected(true);
                bookingTime = 120;
                break;
        }
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBarConfirm.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarConfirm.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onGigTime(ProviderServiceDtls providerServiceDtls) {
        llLiveFee.removeAllViews();
        View infltedView = LayoutInflater.from(ConfirmBookingActivity.this).inflate(R.layout.single_service_price, llLiveFee, false);
        llLiveFee.addView(infltedView);
        TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
        TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);

        String namePerUnit = providerServiceDtls.getName() + " "
                + providerServiceDtls.getUnit();
        tvServiceName.setText(namePerUnit);
        Utilities.setAmtOnRecept(providerServiceDtls.getPrice(),tvServicePrice,currencySymbol);
        tvServiceName.setTypeface(appTypeface.getHind_regular());
        tvServicePrice.setTypeface(appTypeface.getHind_regular());
        setTotalAmount(serviceFee, travelFee, discountFee);
    }

    @Override
    public void onEventSelected(String eventId) {
    }

    @Override
    public void onError(String error) {
        rlMAin.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
        if (error.equals("")){

        }//showProgress();
        else {
            aProgress.alertinfo(error);
        }
        hideProgress();
    }

    @Override
    public void onSessionError(String error) {
        rlMAin.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
        Utilities.setMAnagerWithBID(ConfirmBookingActivity.this, sPrefs,error);
    }

    @Override
    public void onSuccess() {
        hideProgress();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            switch(requestCode)
            {
                case VariableConstant.CAMERA_PIC:
                    handlePictureEvents.startCropImage(newFile);
                    break;

                case VariableConstant.GALLERY_PIC:
                    if(data!=null)
                    {
                        handlePictureEvents.gallery(data.getData());
                    }
                    break;

                case VariableConstant.CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path != null) {
                        try {
                            File fileExist = new File(path);
                            jobPhotosList.add(0,fileExist.getAbsolutePath());
                            jobPhotosAdapter.notifyItemInserted(0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }

            if(requestCode == 4) //after selection of days
            {
                StringBuilder days = new StringBuilder();
                selectedDays = data.getStringArrayListExtra("SELECTEDDAys");

                if(selectedDays.size()>0)
                {
                    //isAnyTypeSelected = true;
                    isNowSelected = false;
                    isLaterSelected = false;
                    isRepeatSelected = true;

                    for(int i = 0;i<selectedDays.size();i++)
                    {
                        if(selectedDays.size()==1)
                            days.append(selectedDays.get(i));
                        else
                        {
                            if(i == selectedDays.size()-1)
                                days.append(selectedDays.get(i));
                            else
                                days.append(selectedDays.get(i)).append(", ");
                        }
                    }
                    tvTodaysRepeatDays.setText(days);
                    glRepeatPartTimeGrid.setVisibility(View.VISIBLE);
                    onSelectTime(false,startDates);
                    // i am doing
                    numOfShifts = 0;
                    for(int nod = 0; nod< maxNameOfDays.size(); nod++)
                    {
                        for(int nsl = 0; nsl<selectedDays.size();nsl++)
                        {
                            if(selectedDays.get(nsl).equals(maxNameOfDays.get(nod)))
                                numOfShifts++;
                        }
                    }

                }
            }
            if(requestCode == VariableConstant.REPEAT_RESULT_CODE) //after selection of shift date
            {
                dayMap.clear();
                String startDate =  data.getStringExtra("StartDate");
                String endDate = data.getStringExtra("EndDate");
                isCustomSelected = data.getBooleanExtra("isCustomSelected",false);

                glRepeatPartDaysGrid.setVisibility(View.VISIBLE);

                try {
                    startDates = simpleDateFormat.parse(startDate);
                    end = simpleDateFormat.parse(endDate);

                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy_hh-mm-ss");
                    int day = end.getDate();
                    int month = end.getMonth()+1;
                    int year = 2019;
                    int am = end.getTimezoneOffset();
                    String formate = month+"-"+day+"-"+year+"_00-00-00";

                    Date gmtt = sdf.parse(formate);
                    long checkdate = gmtt.getTime();
                    finaldateZero = checkdate;
                    Log.i("formate","formate --> "+checkdate);

                    Log.e(TAG, "onActivityResult: END_TIME_STAMP "+end.getTime()/1000);
                    confirmBookCont.listOfDates(Utilities.getDatesBetweenUsingJava7(startDates,end));
                    String startDatesAre[] = startDate.split(",");
                    String endDatesAre[] =  endDate.split(",");
                    String dateAre = startDatesAre[0]+", "+startDatesAre[1]+" - "+endDatesAre[0]+", "+endDatesAre[1];
                    tvTodaysRepeatStartDate.setText(dateAre);

                    // VariableConstant.SCHEDULE_BOOK_REPEAT_END_DATE =end.getTime()/1000;
                    onSelectedScheduledDateTime = startDates.getTime()/1000;
                    Log.e(TAG, "onActivityResult: SEL_SCH_DATE  "+onSelectedScheduledDateTime );
                    // VariableConstant.SCHEDULE_BOOK_REPEAT_START_DATE =onSelectedScheduledDateTime;

                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(requestCode == VariableConstant.TIME_RESULT_CODE)
            { // after selection of time
                boolean mParamIsSchedule;
                durationSet=true;
                boolean isNowBookingSele = data.getBooleanExtra("isNow",false);
                mParamIsSchedule =  data.getBooleanExtra("isSchedule",false);
                durationHour = data.getIntExtra("durationHour",0);
                durationMin = data.getIntExtra("durationMin",0);
                String dateTIme = data.getStringExtra("SelectedTime");

                SimpleDateFormat formatter = new SimpleDateFormat("MMMM d, yyyy',' h:mm a",Locale.getDefault());
                try {
                    Date gmt = formatter.parse(dateTIme);
                    long startDate = gmt.getTime();
                    startDateCorrectValue = startDate;
                    Log.i("SAITESTING","FINALVALUES-->S"+startDateCorrectValue);
                    long selectedstartDateTime = startDate/1000;
                    Log.e(TAG, "onActivityResult: SEL_SCH_DATE  "+selectedstartDateTime );
                    VariableConstant.SCHEDULE_BOOK_REPEAT_START_DATE =selectedstartDateTime;

                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy_hh-mm-ss");
                    int day = gmt.getDate();
                    int month = gmt.getMonth()+1;
                    int year = 2019;
                    int am = gmt.getTimezoneOffset();
                    String formate = month+"-"+day+"-"+year+"_00-00-00";

                    Date gmtt = sdf.parse(formate);
                    long checkdate1 = gmtt.getTime();
                    Log.i("formate","formate --> "+checkdate1);
                    startDatezero = checkdate1;

                    long diff = startDateCorrectValue - startDatezero;
                    long dummyfdCorrectValue = finaldateZero + diff;
                    Log.i("formate","formate --> "+dummyfdCorrectValue);

                    long endTime = (cartQuantity*60 +durationMin)*60;
                    finaldateCorrectValue = ((dummyfdCorrectValue)+(endTime*1000));
                    Log.i("SAITESTING","FINALVALUES-->"+finaldateCorrectValue);
                    VariableConstant.SCHEDULE_BOOK_REPEAT_END_DATE =finaldateCorrectValue/1000;

                    if(Utilities.isNetworkAvailable(ConfirmBookingActivity.this)){


                        if(cartPojo.getData().getServiceType()==1)
                        {
                            showSelectHoursDialog();
                        }else if(cartPojo!=null && !cartPojo.getData().getItem().get(0).getServiceId().equalsIgnoreCase("1") && cartPojo.getData().getServiceType()==2){
                            showSelectHoursDialog();
                        }
                    }else{
                        Toast.makeText(ConfirmBookingActivity.this,"Please check internet connectivity",Toast.LENGTH_SHORT).show();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "onActivityResult: "+dateTIme +"\nisNow "+isNowBookingSele+"\nisSchedule "
                        +mParamIsSchedule+"\nduration "+durationHour+"\ndurationMin "+durationMin);

                SimpleDateFormat simpleDateFormats = new SimpleDateFormat("MMMM d, yyyy',' h:mm a",Locale.getDefault());

                String hours = durationHour +" hr : "+durationMin +" mn";
                onSelectedScheduledDuration = (durationHour*60)+durationMin;
                VariableConstant.SCHEDULEDTIME=onSelectedScheduledDuration;
                if(isNowBookingSele)
                {
                    isNowSelected = true;
                    isAnyTypeSelected = true;

                }else
                {
                    try {
                        Date dateTImeFromat = simpleDateFormats.parse(dateTIme);
                        onFragmentInteraction(dateTImeFromat,mParamIsSchedule);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    tvSelRepeatDurationTime.setText(hours);
                    if(!mParamIsSchedule)
                    {
                        isAnyTypeSelected = true;
                        isRepeatSelected = true;
                        setShiftVisibility(false);
                        setValueOnTheShifts();
                    }
                    else
                    {
                        isAnyTypeSelected = true;
                        isNowSelected = false;
                        isLaterSelected = true;
                        isRepeatSelected = false;
                    }
                }
            }

            if (requestCode == ADDRESS_CODE)
            {
                if (data != null) {
                    String area = data.getStringExtra("AREA");
                    String latitude = data.getStringExtra("LATITUDE");
                    String longitude = data.getStringExtra("LONGITUDE");

                    VariableConstant.NOWBOOKINGLAT= Double.parseDouble(latitude);
                    VariableConstant.NOWBOOKINGLONG= Double.parseDouble(longitude);

                    sPrefs.setBookLatitude(latitude);
                    sPrefs.setBookLongitude(longitude);
                    sPrefs.setBookingAddress(area);
                    if (Utilities.isNetworkAvailable(this))
                    {
                        showProgress();

                        if(VariableConstant.BOOKINGTYPE==1){
                            confirmBookCont.getSurgePrice(sPrefs,Double.parseDouble(latitude),Double.parseDouble(longitude));
                        }else if(VariableConstant.BOOKINGTYPE==2){
                            confirmBookCont.checkSchedule(sPrefs,noofHours);

                        }

                    } else {
                        Toast.makeText(ConfirmBookingActivity.this,"Please check internet connectivity",Toast.LENGTH_SHORT).show();
                    }
                    tvConfirmAppAddress.setText(area);
                }
            }
            if(requestCode==SELECT_PAYMENT_CODE){
                if(data!=null)
                {
                    Log.d(TAG, "onActivityResult: paymenttype "+paymentType);
                    paymentType = data.getIntExtra("PAYMENTTYPE",-1);
                    paidByWallet=data.getIntExtra("PAIDBYWALLET",0);
                    if(paidByWallet==0 && paymentType == 1 )
                    {
                        rlCashPaymentMethod.setVisibility(View.VISIBLE);
                        rlCardLayout.setVisibility(View.GONE);
                        rlSelectPaymentMethod.setVisibility(View.GONE);
                        rlWalletLayout.setVisibility(View.GONE);
                    }
                    else if(paidByWallet==0 && paymentType ==2 )
                    {
                        rlCashPaymentMethod.setVisibility(View.GONE);
                        rlCardLayout.setVisibility(View.VISIBLE);
                        rlSelectPaymentMethod.setVisibility(View.GONE);
                        rlWalletLayout.setVisibility(View.GONE);
                        paymentCardId = data.getStringExtra("CARDID");
                        cardBrand = data.getStringExtra("CARDTYPE");
                        String last4 = data.getStringExtra("LAST4");

                        Log.d(TAG, "onActivityResultCARD: "+paymentCardId+" CardType "+cardBrand
                                +" last4 "+last4);
                        setCardInfo(cardBrand,last4);
                        sPrefs.setDefaultCardId(paymentCardId);
                        sPrefs.setDefaultCardNum(last4);
                        sPrefs.setDefaultCardBrand(cardBrand);

                    }else if(paidByWallet==1){
                        rlWalletLayout.setVisibility(View.VISIBLE);
                        rlSelectPaymentMethod.setVisibility(View.GONE);
                        rlCardLayout.setVisibility(View.GONE);
                        rlCashPaymentMethod.setVisibility(View.GONE);
                        tvWalletBalance.setText("My Wallet ("+VariableConstant.walletCurrency+" "+VariableConstant.walletBalance+")");
                    }
                    //Checking if payment method is changed from previous
                    if(promoCodeActive){
                        if(paymentType!=previousPaymentType || paidByWallet!=previousPaidByWallet){
                            clearPromocode();
                        }
                    }
                    checkScheduleAndQuantity();
                }else{
                    tvConfirmBook.setClickable(false);
                    tvConfirmBook.setEnabled(false);
                }
            }
        }
        if (requestCode == 1) {

            if (data!=null&&data.getExtras() != null) {

                String cardNo = data.getStringExtra("cardNo");
                paymentCardId = data.getStringExtra("cardID");
                cardBrand=data.getStringExtra("cardBrand");
                sPrefs.setDefaultCardId(paymentCardId);
                Bitmap cardbitmap = data.getParcelableExtra("cardImage");
                String card = "**** **** **** " + " " + cardNo;
                paymentType = 2;
                sPrefs.setDefaultCardNum(cardNo);
                sPrefs.setDefaultCardBrand(cardBrand);
                tvConfirmCardNumber.setText(card);
                ivCardBrand.setImageBitmap(cardbitmap);

                boolean changeDefault = false;

                while (!changeDefault) {
                    if (aProgress.isNetworkAvailable()) {

                        changeDefault = true;
                    } else {
                        aProgress.alertinfo("Check the Internet Connect");
                    }
                }
            }
        }
    }

    private void showSelectHoursDialog(){


        final Dialog login = new Dialog(this);
        login.setContentView(R.layout.duration_picker);
        login.setTitle("Select number of hours");
        ImageView img_minus = login.findViewById(R.id.iv_remove);
        TextView tv_hours = login.findViewById(R.id.tv_quantity);
        noofHours = Integer.parseInt(tv_hours.getText().toString());
        tv_hours.setText("1");

        ImageView img_plus = login.findViewById(R.id.iv_add);
        Button done = login.findViewById(R.id.done);


        img_minus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                noofHours--;
                tv_hours.setText(""+noofHours);
            }
        });

        img_plus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                noofHours++;
                tv_hours.setText(""+noofHours);
            }
        });

        done.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                login.dismiss();
                // confirmBookCont.checkSchedule(sPrefs,noofHours);
                if(VariableConstant.BOOKINGTYPE!=3)
                {
                    hideProgress();
                    confirmBookCont.checkSchedule(sPrefs,noofHours);
                }else{
                    if(noofHours>=1)
                    {
                        hideProgress();
                        int minutes = noofHours*60;
                        // VariableConstant.SCHEDULEDTIME = minutes;
                    }
                }
            }
        });

        // Make dialog box visible.
        login.show();
    }

    /*private void showDurationDialog()
    {

            final Dialog dialog = new Dialog(this);
            View inflate = LayoutInflater.from(this).inflate(R.layout.duration_picker,multipleShiftBooking,false);
            final NumberPicker month=  inflate.findViewById(R.id.numberPickerMonth);
            final NumberPicker year=  inflate.findViewById(R.id.numberPickerYear);
            final TextView tvMonthDialog=  inflate.findViewById(R.id.dialogMonth);
            final TextView tvYearDialog=  inflate.findViewById(R.id.dialogYear);
            final TextView tvSelectDuration=  inflate.findViewById(R.id.tvSelectDuration);
            Button done=  inflate.findViewById(R.id.done);
            tvSelectDuration.setTypeface(appTypeface.getHind_semiBold());
            if(isRepeat)
            {
                tvSelRepeatDuration.setText(R.string.shift_Duration);
                tvSelectDuration.setText(R.string.shift_Duration);
            }else
                tvSelRepeatDuration.setText(R.string.job_duration);
            done.setTypeface(appTypeface.getHind_semiBold());
            String[] strings = {"00","15","30","45"};
            month.setOnValueChangedListener((picker, oldVal, newVal) -> {
                durationHour = newVal;
                tvMonthDialog.setText(""+newVal);
            });
            year.setOnValueChangedListener((picker, oldVal, newVal) -> {
                durationMin = Integer.parseInt(strings[newVal]);
                tvYearDialog.setText(""+durationMin);
            });

            year.setMinValue(0);
            year.setMaxValue(strings.length-1);
            year.setDisplayedValues(strings);
            month.setMaxValue(23);
            month.setMinValue(0);
            done.setOnClickListener(v -> {

                if(durationHour!=0 || durationMin!=0)
                {
                    if(isNowBooking)
                    {
                        //String hours = durationHour +" hr : "+durationMin +" mn";
                        //tvNowTodaysTime.setText(hours);
                        onSelectedScheduledDuration = (durationHour*60)+durationMin;
                        VariableConstant.SCHEDULEDTIME=onSelectedScheduledDuration;
                        isNowSelected = true;
                        isAnyTypeSelected = true;
                    }else
                    {
                        String hours = durationHour +" hr : "+durationMin +" mn";
                        tvSelRepeatDurationTime.setText(hours);
                        if(isRepeat)
                        {
                            isAnyTypeSelected = true;
                            isRepeatSelected = true;
                            setShiftVisibility(false);
                            setValueOnTheShifts();
                        }
                        else
                        {
                            isAnyTypeSelected = true;
                            isNowSelected = false;
                            isLaterSelected = true;
                            isRepeatSelected = false;
                        }
                    }
                    onSelectedScheduledDuration = (durationHour*60)+durationMin;
                    VariableConstant.SCHEDULEDTIME=onSelectedScheduledDuration;
                    dialog.dismiss();
                }
                else
                    Toast.makeText(ConfirmBookingActivity.this,getString(R.string.minimumBookingDuration),Toast.LENGTH_SHORT).show();

            });
            dialog.setContentView(inflate);
            dialog.show();

    }*/

    private void setCardInfo(String cardBrand, String last4) {
        ivCardBrand.setImageBitmap(Utilities.setCreditCardLogo(cardBrand,this));
        tvConfirmCardNumber.setText(getResources().getString(R.string.card_stars)+" "+last4);
        tvConfirmAddCard.setText("Change");
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utilities.checkAndShowNetworkError(this);
    }
    private void openDate_Picker(final Context context) {
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                testCalender.set(year,monthOfYear,dayOfMonth);
                VariableConstant.SCHEDULEDDATE=(testCalender.getTimeInMillis()/1000)+"";
                Log.d(TAG, "onDateSet: VC"+VariableConstant.SCHEDULEDDATE);
                String title=getFormattedDate(testCalender.get(Calendar.YEAR),testCalender.get(Calendar.MONTH),testCalender.get(Calendar.DAY_OF_MONTH));
                tvTodaysDate.setText(title);
                setScheduledTime();
                calltimepicker();
            }

        },testCalender.get(Calendar.YEAR), testCalender.get(Calendar.MONTH), testCalender.get(Calendar.DAY_OF_MONTH));
        Log.d(TAG, "openDate_Picker: "+testCalender.getTime());
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-sPrefs.getServerTimeDifference() - 1000);// new Date().getTime()
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d(TAG, "onCancel: "+ " Cancelled");
            }
        });
        fromDatePickerDialog.show();
    }
    public String getFormattedDate(int year, int monthOfYear, int dayOfMonth){
        Log.d(TAG, "getFormattedDate: "+year+ " "+monthOfYear+" "+dayOfMonth);
        testCalender.set(year, monthOfYear, dayOfMonth);
        String title = DateUtils.formatDateTime(this,
                testCalender.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE
                        | DateUtils.FORMAT_SHOW_WEEKDAY
                        | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_ABBREV_MONTH
                        | DateUtils.FORMAT_ABBREV_WEEKDAY);
        Log.d(TAG, "getFormattedDate: "+title);
        return title;
    }
    public String getFormattedTime(int hourOfDay, int minute){
        Calendar calendar=testCalender;
        testCalender.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),hourOfDay,minute);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("hh:mm a");
        simpleDateFormat.setTimeZone(Utilities.getTimeZoneFromBookingLocation());
        String selectedtime=simpleDateFormat.format(testCalender.getTime());
        return selectedtime;
    }
    //End of selecting the date

    private void calltimepicker()
    {
        int mHour = testCalender.get(Calendar.HOUR_OF_DAY);
        int mMinute = testCalender.get(Calendar.MINUTE);
        final int mYear = testCalender.get(Calendar.YEAR);
        final int mMonth = testCalender.get(Calendar.MONTH);
        final int mDay = testCalender.get(Calendar.DAY_OF_MONTH);
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,TimePickerDialog.THEME_HOLO_LIGHT,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar temp=Calendar.getInstance();
                        temp.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
                        temp.set(temp.get(Calendar.YEAR),temp.get(Calendar.MONTH)
                                ,temp.get(Calendar.DAY_OF_MONTH),temp.get(Calendar.HOUR_OF_DAY)+1,temp.get(Calendar.MINUTE));
                        Calendar selectedtime=Calendar.getInstance();
                        selectedtime.setTimeZone(VariableConstant.BOOKINGTIMEZONE);
                        selectedtime.set(testCalender.get(Calendar.YEAR),testCalender.get(Calendar.MONTH)
                                ,testCalender.get(Calendar.DAY_OF_MONTH),hourOfDay,minute);
                        long currentTimeplus1=temp.getTimeInMillis();
                        Log.d(TAG, "onTimeSet: currenttimePlus1:"+currentTimeplus1+" selectedtime:"+selectedtime.getTimeInMillis());
                        if(currentTimeplus1>selectedtime.getTimeInMillis()){
                            Toast.makeText(ConfirmBookingActivity.this,"Please select a time at least 1 hr from now",Toast.LENGTH_SHORT).show();
                            calltimepicker();
                            return;
                        }
                        testCalender.set(mYear,mMonth,mDay,hourOfDay,minute);
                        tvTodaysTime.setText(getFormattedTime(testCalender.get(Calendar.HOUR_OF_DAY),testCalender.get(Calendar.MINUTE)));
                        Log.d(TAG, "onTimeSet: "+hourOfDay+":"+minute);
                        VariableConstant.SCHEDULEDDATE=(testCalender.getTimeInMillis()/1000)+"";
                        Log.d(TAG, "onTimeSet: VC"+VariableConstant.SCHEDULEDDATE);
                        checkScheduleService();
                        if(Utilities.isNetworkAvailable(ConfirmBookingActivity.this))
                        {
                            showProgress();

                            if(cartPojo.getData().getServiceType()==1)
                            {
                                showSelectHoursDialog();
                            }else if(cartPojo!=null && !cartPojo.getData().getItem().get(0).getServiceId().equalsIgnoreCase("1") && cartPojo.getData().getServiceType()==2){
                                showSelectHoursDialog();
                            } else {
                                confirmBookCont.checkSchedule(sPrefs,noofHours);
                            }


                        }else{
                            Toast.makeText(ConfirmBookingActivity.this,"Please check internet connectivity",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, mHour, mMinute, false);
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d(TAG, "onCancel: time "+"cancelled");
                checkScheduleService();
            }
        });
        timePickerDialog.show();
    }

    void checkScheduleService(){
        if(Utilities.isNetworkAvailable(ConfirmBookingActivity.this)){
            showProgress();
            if(VariableConstant.BOOKINGTYPE!=3){
                confirmBookCont.checkSchedule(sPrefs,noofHours);
            }

        }else{
            Toast.makeText(ConfirmBookingActivity.this,"Please check internet connectivity",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onCartModified( final String serviceId, final int action) {
        Log.d(TAG, "onCartModified: "+serviceId+" action:"+action);
        if(promoCodeActive){
            AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(R.string.promocode_invalid_quatity_changed);
            alertDialogBuilder.setTitle("Alert");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showProgress();
                    clearPromocode();
                    if(sPrefs.getBillingModel().equals("3")||sPrefs.getBillingModel().equals("4")){
                        isHourlyService=true;
                    }else{
                        if(cartPojo.getData().getServiceType()==2){
                            isHourlyService=true;
                            if(cartQuantity>=VariableConstant.MINIMUM_HOUR){
                                minimumHr=true;
                            }else{
                                minimumHr=false;
                            }
                        }else{
                            isHourlyService=false;
                        }
                    }
                    confirmBookCont.addCartData(serviceId,action,isHourlyService,minimumHr);
                }
            });
            alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if(!isFinishing())
                alertDialogBuilder.create().show();
        }else{
            showProgress();
            if(sPrefs.getBillingModel().equals("3")||sPrefs.getBillingModel().equals("4")){
                isHourlyService=true;
                if(cartQuantity>=VariableConstant.MINIMUM_HOUR){
                    minimumHr=true;
                }else{
                    minimumHr=false;
                }
            }else{
                if(cartPojo.getData().getServiceType()==2){
                    if(cartQuantity>=VariableConstant.MINIMUM_HOUR){
                        minimumHr=true;
                    }else{
                        minimumHr=false;
                    }
                    isHourlyService=true;
                }else{
                    isHourlyService=false;
                }
            }
            confirmBookCont.addCartData(serviceId,action,isHourlyService,minimumHr);
        }
    }

    public void checkCartPojo(){
        if(cartPojo!=null && cartPojo.getData()!=null ){
            if(!sPrefs.getBillingModel().equals("3") && !sPrefs.getBillingModel().equals("4")){
                Iterator<Item> iterator=cartPojo.getData().getItem().iterator();
                while (iterator.hasNext()){
                    Item item=iterator.next();
                    if(item.getQuntity()==0){
                        iterator.remove();
                    }
                }
                Log.d(TAG, "checkCartPojo: "+cartPojo.getData().getItem());
            }
        }
    }
    @Override
    public void onSuccessCartDataAdd(String headerData, String result) {
        hideProgress();
        Log.d(TAG, "onSuccessCartDataAdd: "+result);
        if(headerData.equals("200")){
            try{
                cartPojo=gson.fromJson(result,CartModifiedPojo.class);
                if(VariableConstant.BOOKINGTYPE==2){
                    checkScheduleService();
                }
                checkCartPojo();
                confirmBookingServicesAdapter.setCartModifiedPojo(cartPojo);
                VariableConstant.CARTID=cartPojo.getData().get_id();
                confirmBookingServicesAdapter.notifyDataSetChanged();
                serviceFee=cartPojo.getData().getTotalAmount();
                cartQuantity=cartPojo.getData().getTotalQuntity();
                Log.i("SAITESTING","cartQuantity--> "+cartQuantity);
                VariableConstant.SCHEDULEDTIME = cartQuantity;

                if( serviceFee>0){
                    setPaymentBreakdown();
                }
                if(cartPojo.getMessage().equals("sucess")){
                    cartQuantity=cartPojo.getData().getTotalQuntity();
                    Log.i("SAITESTING","cartQuantity--> "+cartQuantity);
                    VariableConstant.SCHEDULEDTIME = cartQuantity;

                    long diff = startDateCorrectValue - startDatezero;
                    long dummyfdCorrectValue = finaldateZero + diff;
                    Log.i("formate","formate --> "+dummyfdCorrectValue);

                    long endTime = (cartQuantity*60 +durationMin)*60;
                    finaldateCorrectValue = ((dummyfdCorrectValue)+(endTime*1000));
                    Log.i("SAITESTING","FINALVALUES-->"+finaldateCorrectValue);
                    VariableConstant.SCHEDULE_BOOK_REPEAT_END_DATE =finaldateCorrectValue/1000;


                    String splitDate[] = tvTodaysRepeatStartDate.getText().toString().split("-");
                    String formattedDateStart[] = simpleDateFormat.format(startDates).split(",");
                    Date date = new Date(finaldateCorrectValue);

                    String formattedDate[] = simpleDateFormat.format(date).split(",");

                    if(!(formattedDateStart[0]+" "+formattedDateStart[1]).equals(formattedDate[0]+" "+formattedDate[1]))
                        tvConfirmEndDate.setText(splitDate[1]);

                    tvConfirmEndTime.setText(formattedDate[2]);

                    checkScheduleAndQuantity();
                }
                // setPaymentBreakdown();
                setToolBarQuatity();
                Log.d(TAG, "onSuccessCartDataAdd: "+result);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailureCartDataAdd(String headerError, String error) {
        hideProgress();
        try{
            ErrorHandel errorHandel=gson.fromJson(error,ErrorHandel.class);
            if(headerError.equals("440")){
                Utilities.getAccessToken(errorHandel.getData(),this);
            }
            Log.d(TAG, "onFailureCartDataAdd: "+error);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onLastDues(String headerError, String result)
    {
        final LastDuesPojo lastDuesPojo;
        try{
            lastDuesPojo = gson.fromJson(result, LastDuesPojo.class);
            if(headerError.equals("200")){
                AlertDialog.Builder aBuilder=new AlertDialog.Builder(this)
                        .setTitle("Last Dues")
                        .setMessage(lastDuesPojo.getMessage())
                        /*.setMessage("You have "+sPrefs.getCurrencySymbol()+" "+lastDuesPojo.getData().getLastDues()+
                        " from last booking which will be added in this booking")*/
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                rlLastdues.setVisibility(View.VISIBLE);
                                lastDuesAmount=Double.parseDouble(lastDuesPojo.getData().getLastDues());
                                Utilities.setAmtOnRecept(Double.parseDouble(lastDuesPojo.getData().getLastDues()),tv_lastdues_Amt,VariableConstant.walletCurrency);
                                setTotalAmount(serviceFee,travelFee,discountFee);
                            }
                        });
                if(!isFinishing())
                    aBuilder.setCancelable(false);
                aBuilder.create().show();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccessLiveBooking(String header, String result) {
        hideProgress();
        VariableConstant.isConfirmBook = true;
        VariableConstant.SCHEDULEDDATE = "";
        VariableConstant.BOOKINGTYPE = 1;
        VariableConstant.SCHEDULEDTIME = 0;
        VariableConstant.CALLMQTTDATA = 0;
        progress_bar.setVisibility(View.GONE);
        ivTickCheck.animate().rotationY(360).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                Log.e(TAG, "onAnimationStart: " );
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.e(TAG, "onAnimationEnd: " );
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                Log.e(TAG, "onAnimationCancel: " );
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
                Log.e(TAG, "onAnimationRepeat: " );
            }
        });
        ivTickCheck.setVisibility(View.VISIBLE);
        tvYahBookingDone.setVisibility(View.VISIBLE);
        tvWillLetUKnw.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ConfirmBookingActivity.this, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                VariableConstant.isSplashCalled = true;
                startActivity(intent);
                finish();
            }
        },2000);
    }

    @Override
    public void onFailureLiveBooking(String header, String result) {
        aProgress.alertinfo(result);
        hideProgress();
        rlMAin.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessScheduleCheck(String header, String result) {


        hideProgress();
        VariableConstant.SCHEDULEAVAILABLE=true;
        checkScheduleAndQuantity();

        if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE))
        {
            confirmBookingServicesAdapter.notifyDataSetChanged();
            double service_price_surge=0.0;
            double final_value = 0.0;
            try {
                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*serviceFee));
                final_value = service_price_surge -discountFee +travelFee + lastDuesAmount;
                Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                Utilities.setAmtOnRecept(final_value,tvTotalAmt,currencySymbol);
            } catch (Exception e) {
                Toast.makeText(ConfirmBookingActivity.this, TAG+ " Surge not applied", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }finally {
            }
        } else {
            Utilities.setAmtOnRecept(total,tvTotalAmt,currencySymbol);
        }

        /*double service_price_surge=0.0;
        double final_value = 0.0;
        try {
            service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*serviceFee));
            final_value = service_price_surge -discountFee +travelFee + lastDuesAmount;
            Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
            Utilities.setAmtOnRecept(final_value,tvTotalAmt,currencySymbol);
        } catch (Exception e) {
            Toast.makeText(ConfirmBookingActivity.this, TAG+ " Surge not applied", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }finally {
        }*/

        // confirmBookingServicesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFailureScheduleCheck(String header, String result) {
        hideProgress();
        aProgress.alertinfo(result);
        VariableConstant.SCHEDULEAVAILABLE=false;
        checkScheduleAndQuantity();
    }

    @Override
    public void onSessionExpired() {
        rlMAin.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
        Utilities.setMAnagerWithBID(ConfirmBookingActivity.this, sPrefs,"Session Expired");
    }

    @Override
    public void onSuccessAccessToken(String data) {
        sPrefs.setSession(data);
    }

    @Override
    public void onFailureAccessToken(String headerError, String server_issue) {
        Toast.makeText(this, server_issue, Toast.LENGTH_SHORT).show();
    }
    private void checkforCalendarPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(ConfirmBookingActivity.this,Manifest.permission.READ_CONTACTS)){
                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle("CALENDER PERMISSION");
                builder.setMessage("Need calendar permission to set remainder on later booking !");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(ConfirmBookingActivity.this,new String[]{Manifest.permission.WRITE_CALENDAR},MY_PERMISSIONS_REQUEST_CALENDAR);
                    }
                });
                builder.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_CALENDAR},MY_PERMISSIONS_REQUEST_CALENDAR);
            }
        }
    }


    @Override
    public void toTermsLink() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }

    @Override
    public void toPrivacyPage() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }


    @Override
    public void onFragmentInteraction(Date calendar, boolean isSchedule) {

        if(isSchedule)
            onDateSelected(calendar);
        else
            onRepeatDateSelected(calendar);

    }

    @Override
    public void onDateSelected(Date time) {

        String dateSel = simpleDateFormat.format(time.getTime());
        String splitDate[] = dateSel.split(",");
        String selDateTime = splitDate[0]+", "+splitDate[1];
        tvTodaysDate.setText(selDateTime);
        tvTodaysTime.setText(splitDate[2]);
        onSelectedScheduledDateTime = time.getTime()/1000;
        glLaterPartGrid.setVisibility(View.VISIBLE);
        glLaterTimePart.setVisibility(View.VISIBLE);
        onSelectedDuration(false);
    }

    Date dateForEndTime;
    @Override
    public void onRepeatDateSelected(Date time) {
        dateForEndTime = time;
        String dateSel = simpleDateFormat.format(time.getTime());
        String splitDate[] = dateSel.split(",");
        tvRepeatStartTime.setText(splitDate[2]);
        glRepeatPartDurationGrid.setVisibility(View.VISIBLE);
        glRepeatPartTimeGrid.setVisibility(View.VISIBLE);
        //  onSelectedDuration(true);
        //   callIntent(false,false,"");
    }

    @Override
    public void setDateOnMap(ArrayList<Integer> dayInArray) {

        maxNameOfDays.clear();
        for(int i = 0; i<dayInArray.size();i++)
        {
            Log.d(TAG, "setDateOnMap: "+dayInArray.get(i));
            dayMap.put(dayInArray.get(i),Utilities.dayOfTheWeek(dayInArray.get(i)));
            maxNameOfDays.add(Utilities.dayOfTheWeek(dayInArray.get(i)));
        }
        if(!isCustomSelected)
        {
            currentDay = Utilities.dayOfTheWeek(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
        }

        selectDays();
    }

    private void onSelectedDuration(boolean isRepeat)
    {
        //showDialogFragment(isRepeat);
        showDurationDialog(isRepeat,false);

    }

    private void showDurationDialog(boolean isRepeat,boolean isNowBooking) {
        final Dialog dialog =new Dialog(this);
        View inflate = LayoutInflater.from(this).inflate(R.layout.duration_picker,multipleShiftBooking,false);
        final NumberPicker month=  inflate.findViewById(R.id.numberPickerMonth);
        final NumberPicker year=  inflate.findViewById(R.id.numberPickerYear);
        final TextView tvMonthDialog=  inflate.findViewById(R.id.dialogMonth);
        final TextView tvYearDialog=  inflate.findViewById(R.id.dialogYear);
        final TextView tvSelectDuration=  inflate.findViewById(R.id.tvSelectDuration);
        Button done=  inflate.findViewById(R.id.done);
        tvSelectDuration.setTypeface(appTypeface.getHind_semiBold());
        if(isRepeat)
        {
            tvSelRepeatDuration.setText(R.string.shift_Duration);
            tvSelectDuration.setText(R.string.shift_Duration);
        }else
            tvSelRepeatDuration.setText(R.string.job_duration);
        done.setTypeface(appTypeface.getHind_semiBold());
        String[] strings = {"00","15","30","45"};
        month.setOnValueChangedListener((picker, oldVal, newVal) -> {
            durationHour = newVal;
            tvMonthDialog.setText(""+newVal);
        });
        year.setOnValueChangedListener((picker, oldVal, newVal) -> {
            durationMin = Integer.parseInt(strings[newVal]);
            tvYearDialog.setText(""+durationMin);
        });

        year.setMinValue(0);
        year.setMaxValue(strings.length-1);
        year.setDisplayedValues(strings);
        month.setMaxValue(23);
        month.setMinValue(0);
        done.setOnClickListener(v -> {

            if(durationHour!=0 || durationMin!=0)
            {
                if(isNowBooking)
                {
                    //String hours = durationHour +" hr : "+durationMin +" mn";
                    //tvNowTodaysTime.setText(hours);
                    onSelectedScheduledDuration = (durationHour*60)+durationMin;
                    VariableConstant.SCHEDULEDTIME=onSelectedScheduledDuration;
                    isNowSelected = true;
                    isAnyTypeSelected = true;
                }else
                {
                    String hours = durationHour +" hr : "+durationMin +" mn";
                    tvSelRepeatDurationTime.setText(hours);
                    if(isRepeat)
                    {
                        isAnyTypeSelected = true;
                        isRepeatSelected = true;
                        setShiftVisibility(false);
                        setValueOnTheShifts();
                    }
                    else
                    {
                        isAnyTypeSelected = true;
                        isNowSelected = false;
                        isLaterSelected = true;
                        isRepeatSelected = false;
                    }
                }
                onSelectedScheduledDuration = (durationHour*60)+durationMin;
                VariableConstant.SCHEDULEDTIME=onSelectedScheduledDuration;
                dialog.dismiss();
            }
            else
                Toast.makeText(ConfirmBookingActivity.this,getString(R.string.minimumBookingDuration),Toast.LENGTH_SHORT).show();

        });
        dialog.setContentView(inflate);
        dialog.show();
    }

    private void setShiftVisibility(boolean b) {
        if(b)
        {
            glRepeatPartGrid.setVisibility(View.VISIBLE);
            multipleShiftBooking.setVisibility(View.GONE);
            callRepeatDateTimeIntent(true);
        }else
        {
            multipleShiftBooking.setVisibility(View.VISIBLE);
            glRepeatPartGrid.setVisibility(View.GONE);
            glRepeatPartDaysGrid.setVisibility(View.GONE);
            glRepeatPartTimeGrid.setVisibility(View.GONE);
            glRepeatPartDurationGrid.setVisibility(View.GONE);
        }
    }

    private void setValueOnTheShifts()
    {
        //tvConfirmAmountScheduleCharge.setVisibility(View.VISIBLE);
        tvEditShiftDetails.setText("Edit");

        tvConfirmStartTime.setTypeface(appTypeface.getHind_light());
        tvConfirmStartDate.setTypeface(appTypeface.getHind_light());
        tvConfirmEndTime.setTypeface(appTypeface.getHind_light());
        tvConfirmEndDate.setTypeface(appTypeface.getHind_light());
        String splitDate[] = tvTodaysRepeatStartDate.getText().toString().split("-");
        tvConfirmStartDate.setText(splitDate[0]);
        tvConfirmEndDate.setText(splitDate[1]);
        tvConfirmStartTime.setText(tvRepeatStartTime.getText().toString());

        //duration hour is number of hours
        long endTime = (cartQuantity*60 +durationMin)*60;
        //   long endTime = (durationHour*60 +durationMin)*60;
        long endTimes = ((dateForEndTime.getTime())+(endTime*1000));
        String formattedDateStart[] = simpleDateFormat.format(startDates).split(",");
        Date date = new Date(endTimes);

        String formattedDate[] = simpleDateFormat.format(date).split(",");

        if(!(formattedDateStart[0]+" "+formattedDateStart[1]).equals(formattedDate[0]+" "+formattedDate[1]))
            tvConfirmEndDate.setText(splitDate[1]+"(+1)");

        tvConfirmEndTime.setText(formattedDate[2]);
        llConfirmScheduledDays.removeAllViews();
        for(int i = 0; i<selectedDays.size();i++)
        {
            TextView tvWeekDays;
            View view  = LayoutInflater.from(this).inflate(R.layout.booking_dates,multipleShiftBooking,false);
            llConfirmScheduledDays.addView(view);

            tvWeekDays = view.findViewById(R.id.tvWeekDays);

            tvWeekDays.setTypeface(appTypeface.getHind_light());
            String valueSelected = selectedDays.get(i).charAt(0)+"";
            tvWeekDays.setText(valueSelected);
        }
        String numberOfShift = numOfShifts+"";
        tvConfirmNumberOfShift.setText(numberOfShift);

        VariableConstant.repeatDays.clear();
        VariableConstant.repeatStartTime = tvConfirmStartTime.getText().toString();
        VariableConstant.repeatStartDate = tvConfirmStartDate.getText().toString();
        VariableConstant.repeatEndTime = tvConfirmEndTime.getText().toString();
        VariableConstant.repeatEndDate = tvConfirmEndDate.getText().toString();
        VariableConstant.repeatNumOfShift = numOfShifts;
        VariableConstant.repeatDays.addAll(selectedDays);

    }


    @Override
    public void onAddPhotos() {
        if(AppPermissionsRunTime.checkPermission(this,myPermissionConstantsArrayList,PERMISSIONREQUESTCODE));
        {
            openDialog();
        }
    }

    public void openDialog()
    {
        //StuffImage_1545404798315
        takenNewImage = "CameraImage_"+String.valueOf(System.nanoTime())+".png";
        handlePictureEvents = new HandlePictureEvents(this);
        CreateOrClearDirectory directory = CreateOrClearDirectory.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG, "openDialog: write external storage: "+checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
        }
        imageStorageDir = directory.getAlbumStorageDir(this,VariableConstant.PARENT_FOLDER+ File.separator+"StuffImages",false);
        imageStorageDir2 = directory.getAlbumStorageDir(this,VariableConstant.PARENT_FOLDER+File.separator+"Profile_Pictures",false);
        final Resources resources = this.getResources();
        final CharSequence[] options = {resources.getString(R.string.TakePhoto), resources.getString(R.string.ChoosefromGallery), resources.getString(R.string.action_cancel)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(resources.getString(R.string.TakePhoto))) {
                    takePicFromCamera();
                }
                else if (options[item].equals(resources.getString(R.string.ChoosefromGallery))) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, VariableConstant.GALLERY_PIC);
                    overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
                }
                else if (options[item].equals(resources.getString(R.string.action_cancel))){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void takePicFromCamera() {
        takenNewImage = "CameraImage_"+String.valueOf(System.nanoTime())+".png";
        try {
            Uri newProfileImageUri;
            Log.d(TAG, "takePicFromCamera: "+imageStorageDir.getAbsolutePath());
            newFile = new File(imageStorageDir,takenNewImage);
            if (Build.VERSION.SDK_INT >= N)
                newProfileImageUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);
            Log.d(TAG, "takePicFromCamera: newfile: path : "+newFile.getAbsolutePath());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent,VariableConstant.CAMERA_PIC);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <h2>upload images to Server</h2>
     * <p>this method is used to call method in controller class for uploading image</p>
     */
    public void uploadImagesToServer()
    {
        jobPhotosArray = new JSONArray();
        for(int i=0;i<VariableConstant.STUFFIMAGES.size();i++) {
            confirmBookCont.uploadImageOnServer(VariableConstant.STUFFIMAGES.get(i));
        }

    }

    /**
     * <h2>Store image in array</h2>
     * <p>this method is used to store imageUrl in the JSONArray</p>
     * @param imageUrl contains the image url which will be stored in JSONArray
     */
    @Override
    public void onSuccessImageUpload(String imageUrl){
        jobPhotosArray.put(imageUrl);
        if(jobPhotosArray.length() == VariableConstant.STUFFIMAGES.size())
            liveBooking();
    }

    @Override
    public void onFailureImageUpload() {
        jobPhotosArray.put("");
        if(jobPhotosArray.length() == VariableConstant.STUFFIMAGES.size())
            liveBooking();
        Toast.makeText(this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
    }
}


