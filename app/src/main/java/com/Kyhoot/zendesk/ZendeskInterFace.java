package com.Kyhoot.zendesk;

/**
 * <h>ZendeskInterFace</h>
 * Created by Ali on 12/29/2017.
 */

public interface ZendeskInterFace
{
    void onShowProgress();
    void onHideProgress();
    void onSuccess(String result);
    void onZendexTicketAdded(String msg);
    void onError(String errMsg);
    void onSessionExpired(String sessionMsg);

}
