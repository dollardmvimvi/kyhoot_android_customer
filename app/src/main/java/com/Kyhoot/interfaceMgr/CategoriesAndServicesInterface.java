package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 1/2/18.
 */

public interface CategoriesAndServicesInterface {
    public void onCartModified(String serviceId, int action, boolean b);
    public void onSuccessCartDataAdd(String headerData, String result);
    public void onFailureCartDataAdd(String headerError, String error);
    public void onViewMore(double price, String serviceName, String description);
    void onSuccessGetCart(String headerData, String result);

    void onFailureGetCart(String headerError, String error);

    void onSessionError(String message);

    void onError(String message);

    void onSuccessAccessToken(String result);
}
