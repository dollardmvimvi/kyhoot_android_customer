package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.pojoResponce.PromocodePojo;
import com.Kyhoot.pojoResponce.ProviderServiceDtls;
import com.Kyhoot.pojoResponce.SurgePriceResponse;
import com.Kyhoot.pojoResponce.ValidatorPojo;

import java.util.ArrayList;
import java.util.Date;

/**
 * <h>UtilInterFace</h>
 * Created by Ali on 9/19/2017.
 */

public interface UtilInterFace {
    void showProgress();

    void hideProgress();

    void onSuccess(String onSuccess);

    void onError(String errorMsg);

    void onSuccess(ArrayList<AddressPojo.AddressData> addressList);

    void onSuccessReview(ValidatorPojo reviewlist);

    interface ConfirmBook {
        void showProgress();

        void hideProgress();

        void onGigTime(ProviderServiceDtls providerServiceDtls);

        void onEventSelected(String eventId);

        void onError(String error);

        void onSessionError(String error);

        void onSuccess();

        void onSuccessPromocode(PromocodePojo pojo);
        void onSuccessSurgePrice(SurgePriceResponse response);

        void onErrorPromocode(String internal_server_error);
        void onFailureGetCart(String headerError, String error);

        void onSuccessGetCart(String headerData, String result);
        void onCardSuccess(String headerError,String result);
        void onCardFailure(String headerError,String result);

        public void onCartModified(String serviceId, int action);
        public void onSuccessCartDataAdd(String headerData, String result);
        public void onFailureCartDataAdd(String headerError, String error);
        void onLastDues(String headerError,String result);
        void onSuccessLiveBooking(String header, String result);
        void onFailureLiveBooking(String header, String result);
        void onSuccessScheduleCheck(String header,String result);
        void onFailureScheduleCheck(String header,String result);

        void setDateOnMap(ArrayList<Integer> dayInArray);

        void onDateSelected(Date time);

        void onRepeatDateSelected(Date time);

        /**
         * this method is used to pass the image to the confirmBookActivity
         */
        void onSuccessImageUpload(String imageUrl);
        void onFailureImageUpload();

    }
}
