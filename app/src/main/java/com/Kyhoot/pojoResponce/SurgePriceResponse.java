package com.Kyhoot.pojoResponce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Pramod
 * @since 07-May-2019.
 */

public class SurgePriceResponse implements Serializable{

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private SurgePriceData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SurgePriceData getData() {
        return data;
    }

    public void setData(SurgePriceData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SurgePriceResponse{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
