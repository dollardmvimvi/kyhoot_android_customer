package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.controllers.PasswordHelpController;
import com.Kyhoot.countrypic.Country;
import com.Kyhoot.countrypic.CountryPicker;
import com.Kyhoot.countrypic.CountryPickerListener;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.Utilities;


/**
 * <h>PasswordHelp</h>
 * <p>
 * this help the user to reset his password i.e in case of forgot password
 * implemented the interface
 *
 * @see CheckNetworkAvailablity and also
 * @see VerifyOtpIntrface
 * </p>
 */
public class PasswordHelp extends AppCompatActivity implements View.OnClickListener,CheckNetworkAvailablity,VerifyOtpIntrface
{
    AppTypeface appTypeface;
    private RelativeLayout rlForgotPassMob;
    private ImageView ivCountryFlg;
    private TextView tvForgotCountryCode, tvForgotPhone, tvForgotEmail, tvPasswordNext;
    private EditText etForgotPassMob,etForgotPassEmail;
    private View viewForgotMob,viewForgotemail;
    private CountryPicker mCountryPicker;
    private PasswordHelpController passwordHelpController;
    private boolean isEmailFlag = false;
    private AlertProgress alertProgress;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        setContentView(R.layout.activity_password_help);
        appTypeface = AppTypeface.getInstance(this);
        initializeToolBar();
        initializeView();
    }

    /*
    initializing view of the class
     */
    private void initializeView() {
        alertProgress = new AlertProgress(this);
        pDialog = alertProgress.getProgressDialog(getString(R.string.wait));
        passwordHelpController = new PasswordHelpController(this,this,this);
        RelativeLayout rlForgotPassPhone = findViewById(R.id.rlForgotPassPhone);
        RelativeLayout rlForgotPassemail = findViewById(R.id.rlForgotPassemail);
        rlForgotPassMob = findViewById(R.id.rlForgotPassMob);
        tvPasswordNext = findViewById(R.id.tvPasswordNext);
        tvForgotCountryCode = findViewById(R.id.tvForgotCountryCode);
        tvForgotPhone = findViewById(R.id.tvForgotPhone);
        tvForgotEmail = findViewById(R.id.tvForgotEmail);
        TextView tvPasswrdHlp = findViewById(R.id.tvPasswrdHlp);
        etForgotPassMob = findViewById(R.id.etForgotPassMob);
        etForgotPassEmail = findViewById(R.id.etForgotPassEmail);
        viewForgotMob = findViewById(R.id.viewForgotMob);
        viewForgotemail = findViewById(R.id.viewForgotemail);
        ivCountryFlg = findViewById(R.id.ivCountryFlg);
        rlForgotPassPhone.setOnClickListener(this);
        rlForgotPassemail.setOnClickListener(this);
        tvPasswordNext.setOnClickListener(this);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));
        setListener();
        tvPasswrdHlp.setTypeface(appTypeface.getHind_regular());
        tvForgotPhone.setTypeface(appTypeface.getHind_regular());
        tvForgotEmail.setTypeface(appTypeface.getHind_regular());
        etForgotPassMob.setTypeface(appTypeface.getHind_regular());
        tvPasswordNext.setTypeface(appTypeface.getHind_semiBold());
        etForgotPassMob.setBackgroundColor(Color.TRANSPARENT);
    }

    /*
    initialize toolBar
     */
    private void initializeToolBar() {
        Toolbar toolbalPasword = findViewById(R.id.toolbalPasword);
        setSupportActionBar(toolbalPasword);
        getSupportActionBar().setTitle("");
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.helpWithPassword);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbalPasword.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolbalPasword.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.rlForgotPassPhone:
                changeUiToPhone(true);
                break;
            case R.id.rlForgotPassemail:
                changeUiToPhone(false);
                break;
            case R.id.tvPasswordNext:
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                String email = etForgotPassEmail.getText().toString();
                String phone = etForgotPassMob.getText().toString();
                String countryCode = tvForgotCountryCode.getText().toString();
                passwordHelpController.checkEmailPhoneValid(isEmailFlag, email,countryCode, phone, etForgotPassEmail, etForgotPassMob);
                break;
        }
    }

    /**
     * <h1>changeUiToPhone</h1>
     * <p>
     * this is helping to change the ui according to user interface
     *
     * @param b this represent which one selected i.e Email or Phone
     *          </p>
     */
    private void changeUiToPhone(boolean b)
    {
        if (b)
            phoneUi();
        else
            emailUi();
    }

    /*
    Email UI changing to email ui
     */
    private void emailUi() {
        isEmailFlag = true;
        etForgotPassEmail.setVisibility(View.VISIBLE);
        tvPasswordNext.setText(R.string.confirm);
        rlForgotPassMob.setVisibility(View.GONE);
        tvForgotPhone.setTextColor(Utilities.getColor(this, R.color.colorAccent));
        viewForgotMob.setVisibility(View.GONE);
        tvForgotEmail.setTextColor(ContextCompat.getColor(this, R.color.red_login));
        viewForgotemail.setVisibility(View.VISIBLE);
    }

    /*
    Email UI changing to Phone UI
     */
    private void phoneUi() {
        isEmailFlag = false;
        etForgotPassEmail.setVisibility(View.GONE);
        tvPasswordNext.setText(R.string.next);
        rlForgotPassMob.setVisibility(View.VISIBLE);
        tvForgotPhone.setTextColor(Utilities.getColor(this, R.color.red_login));
        viewForgotMob.setVisibility(View.VISIBLE);
        tvForgotEmail.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        viewForgotemail.setVisibility(View.GONE);
    }

    /*
    set the country falg and country code for the phone number
     */
    private void setListener()
    {

        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,
                                                  int flagDrawableResID,int maxDigit) {

                tvForgotCountryCode.setText(dialCode);
                ivCountryFlg.setImageResource(flagDrawableResID);
                etForgotPassMob.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxDigit)});
                mCountryPicker.dismiss();
            }
        });
        tvForgotCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker));
            }
        });
        Country country = mCountryPicker.getUserCountryInfo(this);
        if(country!=null)
        {
            ivCountryFlg.setImageResource(country.getFlag());
            tvForgotCountryCode.setText(country.getDialCode());
            etForgotPassMob.setFilters(new InputFilter[]{new InputFilter.LengthFilter(country.getMax_digits())});

        }

    }
    @Override
    public void networkAvalble(LoginPojo loginPojo) {

    }

    @Override
    public void networkAvalble(String emailOrPhone, boolean isEmailOrPhone)
    {
        pDialog.show();
        passwordHelpController.emailPhoneService(emailOrPhone,isEmailOrPhone,tvForgotCountryCode.getText().toString());
    }

    @Override
    public void notNetworkAvalble() {
        alertProgress.showNetworkAlert();
    }

    @Override
    public void onOtpVerify(String success) {
        pDialog.dismiss();
        Intent intent = new Intent(PasswordHelp.this,ConfirmOTP.class);
        Bundle bundle = new Bundle();
        bundle.putString("mobile",etForgotPassMob.getText().toString());
        bundle.putString("countryCode",tvForgotCountryCode.getText().toString());
        bundle.putString("SignupOrForgot","PASSWORD");
        bundle.putString("SID", success);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onOtpError(String error) {
        pDialog.dismiss();
        alertProgress.alertinfo(error);
    }

    @Override
    public void onSignUpSuccess(String msg) {
        pDialog.dismiss();
        alertProgress.alertPostivionclick(msg, new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onPhoneNumError() {

    }
}
