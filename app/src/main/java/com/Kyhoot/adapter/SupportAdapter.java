package com.Kyhoot.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.main.SupportChild;
import com.Kyhoot.main.WebViewActivity;
import com.Kyhoot.pojoResponce.Support_Data_pojo;

import java.util.ArrayList;

/**
 * <h>SupportAdapter</h>
 * Created by Ali on 10/5/2017.
 */

public class SupportAdapter extends RecyclerView.Adapter {

    FragmentActivity activity;
    Activity mcontext;
    private ArrayList<Support_Data_pojo> supportList;

    public SupportAdapter(Activity activity, ArrayList<Support_Data_pojo> supportList) {
        this.mcontext = activity;
        this.supportList = supportList;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View viewhld = LayoutInflater.from(mcontext).inflate(R.layout.support_list_row, parent, false);
        return new ViewHldr(viewhld);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHldr viewHldr = (ViewHldr) holder;
        viewHldr.list_row_text.setText(supportList.get(position).getName());
        viewHldr.rlSupport.setOnClickListener(onClicked(position));
    }

    private View.OnClickListener onClicked(final int position) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (supportList.get(position).getSubcat().size() > 0) {
                    Intent intent = new Intent(mcontext, SupportChild.class);
                    intent.putExtra("SUB_CAT", supportList.get(position).getSubcat());
                    intent.putExtra("Title", supportList.get(position).getName());
                    mcontext.startActivity(intent);
                } else if (!supportList.get(position).getLink().equals("") && supportList.get(position).getLink() != null) {
                    Intent intent = new Intent(mcontext, WebViewActivity.class);
                    intent.putExtra("Link", supportList.get(position).getLink());
                    intent.putExtra("Title", supportList.get(position).getName());
                    intent.putExtra("COMINFROM", mcontext.getString(R.string.supportFragment));
                    mcontext.startActivity(intent);
                }
                mcontext.overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
            }
        };
    }

    @Override
    public int getItemCount() {
        return supportList.size();
    }

    private class ViewHldr extends RecyclerView.ViewHolder {
        TextView list_row_text;
        RelativeLayout rlSupport;

        public ViewHldr(View itemView) {
            super(itemView);
            list_row_text = itemView.findViewById(R.id.list_row_text);
            rlSupport = itemView.findViewById(R.id.rlSupport);
        }
    }
}
