package com.Kyhoot.interfaceMgr;

import android.os.Bundle;

/**
 * <h>RegisterOTPResponce</h>
 * Created by Ali on 8/28/2017.
 */
public interface RegisterOTPResponce
{
    void onOTPSuccess(String onOTPSuccss);
    void otpResponce(String msg, Bundle data);
    void onOTPError(String errorMsg);
    void onValidationError(int errAt);
    void onEmailValidate(String mailError);

    void onEmailLocalValidity(String mailError);
    void onPhoneValidate(String phonError);

    void onPhoneLocalValidity(String phonError);

    void onNameValidity(String nameError);

    void onEmailLocalSuccess(String email);

    void onMobLocalSuccess(String email);

    void onPasswordLocalValidity(String email);

    void onDateOBirthLocalValidity(String email);

    void onReferralError(String msg);

    void onReferralValidate(String msg);

    void onEmailValidFromAPi();
}
