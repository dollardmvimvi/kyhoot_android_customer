package com.Kyhoot.main.wallet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.main.ChangeCardActivity;
import com.Kyhoot.main.wallet.wallet_transaction_activity.WalletTransActivity;
import com.Kyhoot.utilities.Alerts;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import static com.stripe.android.model.Card.BRAND_RESOURCE_MAP;

public class WalletActivity extends AppCompatActivity implements WalletActivityContract.WalletView, View.OnClickListener {
    TextView tv_wallet_balance;
    TextView tv_wallet_softLimitValue;
    TextView tv_wallet_hardLimitValue;
    TextView tv_wallet_cardNo;
    TextView tv_wallet_currencySymbol;
    EditText et_wallet_payAmountValue;
    RelativeLayout rl_custom_toolBarBackBtn;
    TextView tv_custom_toolBarTitle;
    TextView tv_wallet_curCredit_label;
    TextView tv_wallet_softLimitLabel;
    TextView tv_wallet_hardLimitLabel;
    Button btn_wallet_recentTransactions;
    TextView tv_wallet_payUsing_cardLabel;
    TextView tv_wallet_payAmountLabel;
    TextView tv_wallet_softLimitMsgLabel;
    TextView tv_wallet_softLimitMsg;
    TextView tv_wallet_hardLimitMsgLabel;
    TextView tv_wallet_hardLimitMsg;
    TextView tv_wallet_credit_desc;
    Button btn_wallet_ConfirmAndPay;
    String cardId;
    SharedPrefs sprefs;
    Drawable ic_payment_card_icon;
    Drawable home_next_arrow_icon_off;
    private boolean isCardSelected;

    WalletActivityContract.WalletPresenter walletPresenter;
    Alerts alerts;

    private ProgressDialog pDialog;
    private static final String TAG = "WalletActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_wallet);
        initializeComponents();
        initializeComponents();
        initToolBar();
        initViews();
        initPayViews();
        initSoftHardLimitDescriptionsView();
    }

    private void initializeComponents() {
        sprefs=new SharedPrefs(this);
        walletPresenter=new WalletActivityPresenter(this,this);
        alerts=new Alerts(this);
        tv_wallet_balance=findViewById(R.id.tv_wallet_balance);

    }


    /**<h2>initToolBar</h2>
     * <p> method to initialize customer toolbar </p>
     */
    private void initToolBar()
    {

        rl_custom_toolBarBackBtn=findViewById(R.id.rl_custom_toolBarBackBtn);
        tv_custom_toolBarTitle=findViewById(R.id.tv_custom_toolBarTitle);
        tv_custom_toolBarTitle.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tv_custom_toolBarTitle.setText(getResources().getString(R.string.wallet));
    }

    /**
     *<h2>initViews</h2>
     * <P> custom method to load tooth_top views of the screen </P>
     */
    private void initViews(){
        tv_wallet_curCredit_label=findViewById(R.id.tv_wallet_curCredit_label);
        tv_wallet_softLimitLabel=findViewById(R.id.tv_wallet_softLimitLabel);
        tv_wallet_hardLimitLabel=findViewById(R.id.tv_wallet_hardLimitLabel);
        tv_wallet_payUsing_cardLabel=findViewById(R.id.tv_wallet_payUsing_cardLabel);
        btn_wallet_recentTransactions=findViewById(R.id.btn_wallet_recentTransactions);
        tv_wallet_payAmountLabel=findViewById(R.id.tv_wallet_payAmountLabel);
        tv_wallet_softLimitMsgLabel=findViewById(R.id.tv_wallet_softLimitMsgLabel);
        tv_wallet_softLimitMsg=findViewById(R.id.tv_wallet_softLimitMsg);
        tv_wallet_hardLimitMsgLabel=findViewById(R.id.tv_wallet_hardLimitMsgLabel);
        tv_wallet_hardLimitMsg=findViewById(R.id.tv_wallet_hardLimitMsg);
        tv_wallet_credit_desc=findViewById(R.id.tv_wallet_credit_desc);
        btn_wallet_ConfirmAndPay=findViewById(R.id.btn_wallet_ConfirmAndPay);
        btn_wallet_recentTransactions.setOnClickListener(this);
        home_next_arrow_icon_off=getResources().getDrawable(R.drawable.ic_chevron_right_24dp);
        ic_payment_card_icon=getResources().getDrawable(R.drawable.ic_payment_card_icon);
        tv_wallet_softLimitValue=findViewById(R.id.tv_wallet_softLimitValue);
        tv_wallet_hardLimitValue=findViewById(R.id.tv_wallet_hardLimitValue);
        tv_wallet_cardNo=findViewById(R.id.tv_wallet_cardNo);
        tv_wallet_currencySymbol=findViewById(R.id.tv_wallet_currencySymbol);
        tv_wallet_currencySymbol.setText(VariableConstant.walletCurrency);
        et_wallet_payAmountValue=findViewById(R.id.et_wallet_payAmountValue);
        walletPresenter.getLastCardNo();
        tv_wallet_credit_desc.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_curCredit_label.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_balance.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_softLimitLabel.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tv_wallet_softLimitValue.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_hardLimitLabel.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tv_wallet_hardLimitValue.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        btn_wallet_recentTransactions.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        btn_wallet_ConfirmAndPay.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        et_wallet_payAmountValue.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_cardNo.setOnClickListener(this);
        btn_wallet_ConfirmAndPay.setOnClickListener(this);
        rl_custom_toolBarBackBtn.setOnClickListener(this);
        tv_wallet_payUsing_cardLabel.setOnClickListener(this);
    }

    @Override
    public void setBalanceValues(String currencySymbol,String balance,double hardLimit,double softLimit)
    {
        Utilities.setAmtOnRecept(softLimit,tv_wallet_softLimitValue,currencySymbol);
        Utilities.setAmtOnRecept(hardLimit,tv_wallet_hardLimitValue,currencySymbol);
        Utilities.setAmtOnRecept(Double.parseDouble(balance),tv_wallet_balance,currencySymbol);
        et_wallet_payAmountValue.setText("");
        Utilities.hideKeyBoard(this,et_wallet_payAmountValue);
    }


    /**
     *<h2>initPayViews</h2>
     * <P> custom method to load payment views of the screen </P>
     */
    private void initPayViews()
    {
        tv_wallet_payUsing_cardLabel.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_cardNo.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_payAmountLabel.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_currencySymbol.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_hardLimitLabel.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_hardLimitValue.setTypeface(AppTypeface.getInstance(this).getHind_regular());
    }


    /**
     *<h2>initSoftHardLimitDescriptionsView</h2>
     * <P> custom method to init soft and hard limit description views of the screen </P>
     */
    private void initSoftHardLimitDescriptionsView()
    {
        tv_wallet_softLimitMsgLabel.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tv_wallet_softLimitMsg.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_softLimitMsg.setText(R.string.softLimitMsg);
        tv_wallet_hardLimitMsgLabel.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tv_wallet_hardLimitMsg.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_wallet_hardLimitMsg.setText(R.string.hardLimitMsg);
        btn_wallet_ConfirmAndPay.setTypeface(AppTypeface.getInstance(this).getHind_regular());
    }

    @Override
    public void onResume()
    {
        super.onResume();
        showProgressDialog();

        walletPresenter.getWalletLimits();
        if(sprefs!=null && !sprefs.getDefaultCardId().equals("")){
            setCard(sprefs.getDefaultCardNum(),sprefs.getDefaultCardBrand(),sprefs.getDefaultCardId());
        }
//        Constants.isWalletFragActive = true;
    }


    @Override
    public void hideProgressDialog()
    {
        if(pDialog != null && pDialog.isShowing())
        {
            pDialog.dismiss();
        }
    }



    @Override
    public void showProgressDialog()
    {
        if(pDialog == null)
        {
            pDialog = alerts.getProcessDialog(this);
        }

        if(!pDialog.isShowing())
        {
            pDialog = alerts.getProcessDialog(this);
            pDialog.show();
        }
    }


    @Override
    public void showToast(String msg, int duration)
    {
        hideProgressDialog();
        Toast.makeText(this, msg, duration).show();
    }


    @Override
    public void showAlert(String title, String msg)
    {
        hideProgressDialog();
        Toast.makeText(this, "SHOW ALERT", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void noInternetAlert()
    {
        alerts.showNetworkAlert(this);
    }

    /**
     *<h2>walletDetailsApiErrorViewNotifier</h2>
     * <p> method to notify api error </p>
     */
    @Override
    public void walletDetailsApiErrorViewNotifier(String error)
    {
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Alert")
                .setMessage(error)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        dialogBuilder.create().show();
    }



    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            Bundle bundle;
            bundle=data.getExtras();
            assert bundle != null;
            String cardNum = bundle.getString("cardNo");
            String cardType = bundle.getString("cardBrand");
            cardId = bundle.getString("cardID");
            setCard(cardNum,cardType, cardId);
            Log.i("TEST", "onActivityResult: "+cardNum+" : "+cardType+" : "+cardId);
        }
        else
            setNoCard();


    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void setNoCard()
    {
        isCardSelected = false;
        // btn_wallet_ConfirmAndPay.setEnabled(false);
        //btn_wallet_ConfirmAndPay.setBackgroundColor(getResources().getColor(R.color.grey_view));
        Drawable cardBrand = ic_payment_card_icon;
        tv_wallet_cardNo.setText(getResources().getText(R.string.choose_card));
        tv_wallet_cardNo.setCompoundDrawablesWithIntrinsicBounds(cardBrand,null,home_next_arrow_icon_off,null);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void setCard(String cardNum, String cardType, String defaultCardId) {
        //btn_wallet_ConfirmAndPay.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.selector_layout));
        isCardSelected = true;
        Drawable cardBrand = ic_payment_card_icon;
        if(defaultCardId!=null && !defaultCardId.isEmpty()){
            try{
                cardBrand = getResources().getDrawable(BRAND_RESOURCE_MAP.get(cardType));
            }catch (Exception e){
                e.printStackTrace();
            }

            tv_wallet_cardNo.setText(getString(R.string.card)+" "+cardNum);
            btn_wallet_ConfirmAndPay.setEnabled(true);
            cardId=defaultCardId;
        }
        tv_wallet_cardNo.setCompoundDrawablesWithIntrinsicBounds(cardBrand,null,home_next_arrow_icon_off,null);
    }


    @Override
    public void onPause()
    {
        super.onPause();
//        Constants.isWalletFragActive = false;
    }

    /**
     * <h2>showRechargeConfirmationAlert</h2>
     * <p> method to show an alert dialog to take user confirmation to proceed to recharge </p>
     */
    @SuppressLint("SetTextI18n")
    public void showRechargeConfirmationAlert(final String amount)
    {
        if(cardId!=null && !cardId.isEmpty()){
            AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(getString(R.string.wallet_recharge)+ VariableConstant.walletCurrency+" "+amount);
            alertDialogBuilder.setTitle(R.string.confirm_payment);
            alertDialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showProgressDialog();
                    walletPresenter.rechargeWallet(amount,cardId);
                }
            });

            alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialogBuilder.create().show();

        }else{
            Toast.makeText(this,"Please select a card!",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_wallet_recentTransactions:
                Intent intent = new Intent(this, WalletTransActivity.class);
                startActivity(intent);
                this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_acvtivity);
                break;

            case R.id.tv_wallet_cardNo:
                Intent cardsIntent = new Intent(this, ChangeCardActivity.class);
                startActivityForResult(cardsIntent, 1);
                this.overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                break;

            case R.id.btn_wallet_ConfirmAndPay:

                /**
                 * <h2>showCardSelectionAlert</h2>
                 * <p>show an alert dialog to take user to choose the card</p>
                 */
                if(!isCardSelected){
                    new AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert_cap))
                            .setMessage(getString(R.string.select_card))
                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                }
               else if(!et_wallet_payAmountValue.getText().toString().isEmpty() && !et_wallet_payAmountValue.getText().toString().equals("0") && isCardSelected)
                showRechargeConfirmationAlert(et_wallet_payAmountValue.getText().toString().trim());
                break;

            case R.id.rl_custom_toolBarBackBtn:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if(VariableConstant.WALLETUPDATEINTENT){
            VariableConstant.WALLETUPDATEINTENT=false;
            Intent intent =new Intent();
            setResult(RESULT_OK,intent);
            finish();
        }else{
            super.onBackPressed();
        }
    }
}

