package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;


public class DeleteCardActivity extends AppCompatActivity implements OnClickListener {
    private TextView card_numb_text, exp_text;
    private ImageView card_img;
    private Button delete;
    private ProgressDialog pDialog;
    private String id;
    private Resources resources;
    private SharedPrefs session;
    private boolean isdflt;
    private CheckBox check_selectr;
    private AppTypeface apptypeFace;
    private AlertProgress aProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.delete_card);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        resources = this.getResources();
        aProgress = new AlertProgress(this);
        intializeVariables();
        initializeView();
        initializeToolBar();

    }

    private void initializeToolBar() {
        Toolbar login_tool = findViewById(R.id.DeleteToolBar);
        setSupportActionBar(login_tool);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.Card_Info);
        tv_center.setTypeface(apptypeFace.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initializeView() {
        TextView tvCardName = findViewById(R.id.tvCardName);
        CardView cardMakeDflt = findViewById(R.id.cardMakeDflt);
        TextView tvDefaultCard = findViewById(R.id.tvDefaultCard);
        Bundle bundle = getIntent().getExtras();
        card_numb_text.setText(bundle.getString("NUM"));
        exp_text.setText(bundle.getString("EXP"));
        tvCardName.setText(bundle.getString("NAM"));
        isdflt = bundle.getBoolean("DFLT");
        byte[] byteArray = bundle.getByteArray("IMG");
        id = bundle.getString("ID");
        if (byteArray != null)
            card_img.setImageBitmap(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length));
        if (!isdflt)
        {
            delete.setVisibility(View.VISIBLE);
            cardMakeDflt.setVisibility(View.VISIBLE);
        }

        else
            tvDefaultCard.setVisibility(View.VISIBLE);

        tvDefaultCard.setTypeface(apptypeFace.getHind_regular());
    }

    /**
     * <p>Intiliazing view elements</p>
     *
     * @author 3embed
     */
    private void intializeVariables() {
        apptypeFace = AppTypeface.getInstance(this);
        // TODO Auto-generated method stub
        card_numb_text = findViewById(R.id.card_numb_delete);
        exp_text = findViewById(R.id.exp_date_delete);
        delete = findViewById(R.id.delete_card_btn);
        card_img = findViewById(R.id.card_img_delete);
        check_selectr = findViewById(R.id.check_selectr);

        session = new SharedPrefs(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(resources.getString(R.string.wait));
        delete.setOnClickListener(this);

        check_selectr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    make_Card_Dflt();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * handling onclick event for different action
     *
     * @param v the view which is clicked
     */


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        /**
         * on click delet button,checking network available and calling delete card api
         */
        if (v.getId() == R.id.delete_card_btn) {
            if(!isdflt)
            {
                if (aProgress.isNetworkAvailable()) {
                    deleteCards();
                } else {
                    aProgress.showNetworkAlert();
                }
            }
            else
                aProgress.alertinfo(getString(R.string.defaultCantBeDeleted));

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);

    }

    /**
     * service call for get all card
     */
    public void deleteCards() {

        //creating request with parameter
        pDialog.show();

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("cardId", id);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "card",
                OkHttpConnection.Request_type.DELETE, jsonObj, session.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        dismissDialog();
                        Toast.makeText(DeleteCardActivity.this, resources.getString(R.string.card_removed), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        dismissDialog();
                    }
                });

    }

	/*public void calldeleteServiceResponse(String Response)
    {

		try {
			Gson gson = new Gson();
			AddCardResponse response = gson.fromJson(Response, AddCardResponse.class);

			Utility.printLog("error no delete card" + response.getErrNum());

			if (response.getErrNum().equals("200") && response.getErrFlag().equals("0")) {
				Toast.makeText(DeleteCardActivity.this,resources.getString(R.string.card_removed), Toast.LENGTH_SHORT).show();
				Intent intent=new Intent();
				setResult(RESULT_OK,intent);
                finish();

			}
			else if(response.getErrFlag().equals("1")&&response.getErrNum().equals("96")){
				Toast.makeText(DeleteCardActivity.this, response.getErrMsg(), Toast.LENGTH_LONG).show();
				Utility.sessionExpire(DeleteCardActivity.this);
			}
			else if(response.getErrFlag().equals("1")&&response.getErrNum().equals("94")){
				Toast.makeText(DeleteCardActivity.this, response.getErrMsg(), Toast.LENGTH_LONG).show();
				Utility.sessionExpire(DeleteCardActivity.this);
			}else {
				Toast.makeText(this, response.getErrMsg(), Toast.LENGTH_LONG).show();
			}
			dismissDialog();
		}
		catch (Exception e) {
			dismissDialog();
			e.printStackTrace();

		}

	}*/

    private void make_Card_Dflt() throws JSONException {
        pDialog.show();


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("cardId", id);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "card",
                OkHttpConnection.Request_type.PATCH, jsonObject, session.getSession(), VariableConstant.SelLang,
                new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        pDialog.dismiss();
                        Log.i("TAG", "CARDRESPONCE " + result);
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        pDialog.dismiss();
                    }
                });


    }

    private void dismissDialog() {
        if (pDialog != null) {
            pDialog.cancel();
            pDialog.dismiss();
        }
    }
}
