package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>MqttProvidrResp</h>
 * Created by Ali on 10/4/2017.
 */

public class MqttProvidrResp implements Serializable {
    /*"errNum":200,
"errFlag":0,
"errMsg":"Got Details.",
"data":[]*/

      private int errNum,errFlag;
      private String errMsg;
      private WalletData wallet;
    private ArrayList<MqttProviderData> data;

    public int getErrNum() {
        return errNum;
    }

    public int getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public ArrayList<MqttProviderData> getData() {
        return data;
    }

    public WalletData getWallet() {
        return wallet;
    }
    /* public void setErrNum(int errNum) {
        this.errNum = errNum;
    }

    public void setErrFlag(int errFlag) {
        this.errFlag = errFlag;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }*/
}
