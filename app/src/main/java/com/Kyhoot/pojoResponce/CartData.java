package com.Kyhoot.pojoResponce;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${3Embed} on 2/2/18.
 */

public class CartData implements Serializable {

    private String status;

    private int totalQuntity;

    private String _id;

    private ArrayList<Item> item;

    private ArrayList<Item> checkOutItem;

    private String userId,categoryId,categoryName;

    private double totalAmount;

    private int serviceType;

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    private String currencySymbol;

    public ArrayList<Item> getCheckOutItem() {
        return checkOutItem;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getTotalQuntity ()
    {
        return totalQuntity;
    }

    public String get_id ()
    {
        return _id;
    }

    public ArrayList<Item> getItem ()
    {
        return item;
    }

    public double getTotalAmount ()
    {
        return totalAmount;
    }

    public int getServiceType() {
        return serviceType;
    }

    @NonNull
    @Override
    public String toString()
    {
        return "ClassPojo [ status = "+status+", totalQuntity = "+totalQuntity+", _id = "+_id+", item = "+item+", totalAmount = "+totalAmount+"]";
    }

}

