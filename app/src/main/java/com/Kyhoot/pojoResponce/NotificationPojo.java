package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by Ali on 12/6/2017.
 */

public class NotificationPojo implements Serializable
{
    /*{"statusMsg":"Accepted","bookingId":1512546681308,"statusUpdateTime":1512546716,"proProfilePic":"https:\/\/s3.amazonaws.com\/livemapplication\/Provider\/ProfilePics\/Profile1511933654625.png","status":3*/
    private long bookingId,statusUpdateTime,timestamp,bid;
    private int type;
    private String targetId,_id,fromID,content,name,profilePic;


    public String getName() {
        return name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getBid() {
        return bid;
    }

    public int getType() {
        return type;
    }

    public String getTargetId() {
        return targetId;
    }

    public String get_id() {
        return _id;
    }

    public String getFromID() {
        return fromID;
    }

    public String getContent() {
        return content;
    }

    public long getBookingId() {
        return bookingId;
    }

    public long getStatusUpdateTime() {
        return statusUpdateTime;
    }
}
