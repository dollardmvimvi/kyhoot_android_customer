package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.adapter.MusicGenreAdaptr;
import com.Kyhoot.interfaceMgr.MusicGeners;
import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.models.ReviewModel;
import com.Kyhoot.pojoResponce.MusciGenresData;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

public class MusicGenres extends AppCompatActivity implements NetworkCheck, MusicGeners {

    String TAG = MusicGenres.class.getName();
    private ReviewModel rmodel;
    private AlertProgress alertProgress;
    private ProgressDialog pDialg;
    private ArrayList<MusciGenresData> mgData;
    private MusicGenreAdaptr musicGenreAdaptr;
    private ArrayList<String> musicGenersId = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_genres);
        musicGenersId = (ArrayList<String>) getIntent().getSerializableExtra("SelectedMusic");
        Log.d(TAG, "onCreate: "+musicGenersId);
        initializeToolBar();
    }

    private void initializeToolBar() {
        AppTypeface appTypeface = AppTypeface.getInstance(this);
        alertProgress = new AlertProgress(this);
        mgData = new ArrayList<>();
        RecyclerView recyclrMusicGenrs = findViewById(R.id.recyclrMusicGenrs);
        LinearLayoutManager llManager = new LinearLayoutManager(this);
        recyclrMusicGenrs.setLayoutManager(llManager);
        musicGenreAdaptr = new MusicGenreAdaptr(this, mgData,musicGenersId);
        recyclrMusicGenrs.setAdapter(musicGenreAdaptr);
        Toolbar toolMusciGenres = findViewById(R.id.toolMusciGenres);
        setSupportActionBar(toolMusciGenres);
        TextView tvCenter = findViewById(R.id.tv_center);
        TextView tvDone = findViewById(R.id.tv_skip);
        tvDone.setText(R.string.done);
        tvDone.setTypeface(appTypeface.getHind_regular());
        getSupportActionBar().setTitle("");
        tvCenter.setText(R.string.musciGenres);
        tvCenter.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolMusciGenres.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolMusciGenres.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvDone.setOnClickListener(musicGenreAdaptr);
        rmodel = new ReviewModel();
        rmodel.checkNetwork(alertProgress, this);
    }

    @Override
    public void onNetworkAvailble() {
        pDialg = alertProgress.getProgressDialog(getString(R.string.wait));
        pDialg.show();
        rmodel.getMusicGenres(this);
    }

    @Override
    public void onNetworkNotAvailble() {
        alertProgress.alertinfo(getString(R.string.network_alert_message));
    }

    @Override
    public void onSuccess(ArrayList<MusciGenresData> musicGenres) {
        pDialg.dismiss();
        mgData.addAll(musicGenres);
        musicGenreAdaptr.notifyDataSetChanged();
    }

    @Override
    public void onError(String errorMsg) {
        pDialg.dismiss();
        alertProgress.alertinfo(errorMsg);
    }
}
