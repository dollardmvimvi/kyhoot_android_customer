package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.CardInfoPojo;

/**
 * <h>VerifyOtpIntrface</h>
 * Created by Ali on 9/13/2017.
 */

public interface VerifyOtpIntrface
{
    void onOtpVerify(String success);
    void onOtpError(String error);
    void onSignUpSuccess(String msg);
    void onPhoneNumError();

    interface PaymentInterFace {
        void onClearList();

        void onError(String errorMsg);

        void onResponceItem(CardInfoPojo cardInfoPojo);
    }
}
