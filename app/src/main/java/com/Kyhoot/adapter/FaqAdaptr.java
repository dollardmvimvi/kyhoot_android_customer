package com.Kyhoot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;

/**
 * <h>FaqAdaptr</h>
 * Created by Ali on 9/21/2017.
 */

public class FaqAdaptr extends Adapter {
    private Context mcontext;
    private ViewHldr hldr;

    public FaqAdaptr(Context mcontext) {
        this.mcontext = mcontext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mcontext).inflate(R.layout.single_faq, parent, false);
        return new ViewHldr(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        hldr = (ViewHldr) holder;
        hldr.ivFaqShowMor.setOnClickListener(onClicked());
        hldr.ivFaqShowless.setOnClickListener(onClicked());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    private View.OnClickListener onClicked() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.ivFaqShowMor) {
                    hldr.ivFaqShowless.setVisibility(View.VISIBLE);
                    hldr.ivFaqShowMor.setVisibility(View.GONE);
                    hldr.tvfaqShwAbtInfo.setVisibility(View.VISIBLE);
                } else {
                    hldr.ivFaqShowless.setVisibility(View.GONE);
                    hldr.ivFaqShowMor.setVisibility(View.VISIBLE);
                    hldr.tvfaqShwAbtInfo.setVisibility(View.GONE);
                }
            }
        };
    }

    private class ViewHldr extends RecyclerView.ViewHolder {

        TextView tvfaqShwAbt, tvfaqShwAbtInfo;
        ImageView ivFaqShowMor, ivFaqShowless;

        private ViewHldr(View itemView) {
            super(itemView);
            ivFaqShowless = itemView.findViewById(R.id.ivFaqShowless);
            ivFaqShowMor = itemView.findViewById(R.id.ivFaqShowMor);
            tvfaqShwAbt = itemView.findViewById(R.id.tvfaqShwAbt);
            tvfaqShwAbtInfo = itemView.findViewById(R.id.tvfaqShwAbtInfo);
        }
    }
}
