package com.Kyhoot.utilities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatDrawableManager;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.Kyhoot.BuildConfig;
import com.Kyhoot.DistanceMatrix;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.AccessTokenCallback;
import com.Kyhoot.interfaceMgr.NonMandatoryUpdateCallback;
import com.Kyhoot.interfaceMgr.Terms_privacy_interface;
import com.Kyhoot.main.SplashActivity;
import com.Kyhoot.pojoResponce.ErrorHandel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

/**
 * <h>Utilities</h>
 * Created by Ali on 8/5/2017.
 */

public class Utilities {
    private static AlertDialog alertDialog;
    /********************************************************************************************************************/
    private static final String TAG = "Utilities";
    //GETTING CURRENT DATE
    public static String dateintwtfour() {
        Calendar calendar = Calendar.getInstance(VariableConstant.TIMEZONE);
        calendar.setTime(new Date(System.currentTimeMillis()-VariableConstant.SERVERTIMEDIFFERNCE));
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        formater.setTimeZone(VariableConstant.TIMEZONE);
        Utilities.getTimeZoneFromLocation();
        return formater.format(date);
    }


    /************************************************    for checking internet connection*******/
    public static boolean isNetworkAvailable(Context mcontext) {
        ConnectivityManager connectivity;
        boolean isNetworkAvail = false;
        try {
            connectivity = (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if (info != null) { // connected to the internet
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        // connected to wifi
                        isNetworkAvail = true;
                        return isNetworkAvail;
                    } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                        // connected to the mobile provider's data plan
                        isNetworkAvail = true;
                        return isNetworkAvail;
                    }
                } else {
                    // not connected to the internet
                    isNetworkAvail = false;
                    return isNetworkAvail;
                }

            } else {
                isNetworkAvail = false;
                return isNetworkAvail;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isNetworkAvail;
    }


    /**
     * <h>utcTimeStamp</h>
     *
     * @return Utc timeStamp
     */
    public static long utcTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();
        return (now / 1000);
    }

    public static boolean isRTL() {
        return isRTL(Locale.getDefault());
    }

    private static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }
    //************************************************************************/

    public static boolean verifyAndSetRTL(Activity activity, String localeStr) {
        if (!localeStr.isEmpty() && "ar".equals(localeStr)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
            return true;
        } else {
            return false;
        }
    }

    public static String getCountryCode(Context context) {
        try {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager != null) {
                String countryCode = manager.getNetworkCountryIso().toUpperCase();
                if (TextUtils.isEmpty(countryCode))
                    return context.getResources().getConfiguration().locale.getCountry();
                else
                    return manager.getNetworkCountryIso().toUpperCase();
            } else {
                return context.getResources().getConfiguration().locale.getCountry();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return context.getResources().getConfiguration().locale.getCountry();
        }
    }

    static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[2084];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static void hideKeyboard(Activity mcontext) {
        try {
            InputMethodManager inputManager = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(mcontext.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is used when our current Session got expired, so we instructs user to again do Login.
     *
     * @param mcontext Context of the Activity or the Fragment
     * @param manager  sharedPreference
     */
    public static void setMAnagerWithBID(Context mcontext, final SharedPrefs manager, String logoutMessage) {
        Log.d("TAG", "TAGISOUT " + manager.getCustomerSid());
        if (!logoutMessage.trim().equals("")) {
            Toast.makeText(mcontext, logoutMessage, Toast.LENGTH_SHORT).show();
        }
        Intent intent = new Intent(mcontext, SplashActivity.class);
        Log.d("TAG", "setMAnagerWithBID: " + manager.getFcmTopic());
        if (!manager.getFcmTopic().equals("") && manager.getFcmTopic() != null) {
            try {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopic());
                FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicCity());
                FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicAllCustomer());
                FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicAllCityCustomer());
                FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicAllOutZoneCustomers());
            } catch (Exception e) {
                Log.e("UTILITIES", "setMAnagerWithBID: Unsubsribe from topic error !" + e.getMessage().toString());
                ;
            }
        }
        if (!manager.getCustomerSid().equals("")) {
            Log.d("UTILITIES ", "SHIJENTEST setMAnagerWithBID: unsubscribing");
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.JobStatus.value + "/" + manager.getCustomerSid());
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Provider.value + "/" + manager.getCustomerSid());
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Message.value + "/" + manager.getCustomerSid());
            String android_id = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.ANDROID_ID);
            ApplicationController.getInstance().disconnect(manager.getCustomerSid()+android_id);
        }


        manager.setIsLogin(false);
        manager.clearSession();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mcontext.startActivity(intent);
        VariableConstant.CALLMQTTDATA = 0;
        ((Activity) mcontext).finish();

    }

    public static String MonthName(int month) {
        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sep";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            default:
                return "Dec";
        }
    }

    public static String dayOfTheWeek(int day)
    {
        String days;
        switch (day)
        {
            case 1:
                days =  "Sunday";
                break;
            case 2:
                days =  "Monday";
                break;
            case 3:
                days =  "Tuesday";
                break;
            case 4:
                days =  "Wednesday";
                break;
            case 5:
                days =  "Thursday";
                break;
            case 6:
                days =  "Friday";
                break;
            default:
                days ="Saturday";
                break;

        }
        return  days;
    }

    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "<sup>th</sup>";
        }
        switch (day % 10) {
            case 1:
                return "<sup>st</sup>";
            case 2:
                return "<sup>nd</sup>";
            case 3:
                return "<sup>rd</sup>";
            default:
                return "<sup>th</sup>";
        }
    }


    public static String doubleformate(double amountvalue) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String datavalueIs;
        String datavalue = String.valueOf(amountvalue);
        if (datavalue.contains(",")) {
            String value = datavalue.replace(",", ".");
            datavalueIs = formatter.format(Double.parseDouble(value));
        } else {
            datavalueIs = formatter.format(amountvalue);
        }

        return datavalueIs;
    }

    public static void setAmtOnRecept(double amt, TextView textView, String currencySybmol) {
        String timefee;
        if(VariableConstant.CurrencyPosition.equalsIgnoreCase("Prefix")){
            if (amt <= 0) {
                timefee= currencySybmol + " "+doubleformate(amt);
            } else {
                timefee = currencySybmol + " " + doubleformate(amt);

            }
        }else{
            if (amt <= 0) {
                timefee=  doubleformate(amt)+" "+currencySybmol;
            } else {
                timefee = doubleformate(amt)+" "+currencySybmol;
            }
        }
        textView.setText(timefee);
    }
    public static String getAmtForRecept(double amt, String currencySybmol) {
        String timefee;
        if(VariableConstant.CurrencyPosition.equalsIgnoreCase("Prefix")){
            if (amt <= 0) {
                timefee= currencySybmol + " "+doubleformate(amt);
            } else {
                timefee = currencySybmol + " " + doubleformate(amt);

            }
        }else{
            if (amt <= 0) {
                timefee=  doubleformate(amt)+" "+currencySybmol;
            } else {
                timefee = doubleformate(amt)+" "+currencySybmol;
            }
        }
        return timefee;
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (context != null) {
            if (context.getApplicationContext() != null) {
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            }
        }
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    @SuppressLint("PrivateResource")
    public static Bitmap setCreditCardLogo(String cardMethod, Context context) {
        Bitmap anImage;
        switch (cardMethod) {
            case "Visa":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_visa);
                break;
            case "MasterCard":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_mastercard);
                break;
            case "American Express":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_amex);
                break;
            case "Discover":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_discover);
                break;
            case "Diners Club":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_diners);
                break;

            case "JCB":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_jcb);
                break;

            default:
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_menu_card_icon_off);
                break;
        }
        return anImage;
    }

    @SuppressLint("RestrictedApi")
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static LatLngBounds setBound(double lat, double lng) {
        LatLng pos1, pos2;
        pos1 = latLngAtDistance(lat, lng, 50000, 225.0);
        pos2 = latLngAtDistance(lat, lng, 50000, 45.0);
        return new LatLngBounds(pos1, pos2);
    }

    private static LatLng latLngAtDistance(double latitude, double longitude, double distanceInMetres, double angle) {
        double brngRad = toRadians(angle);
        double latRad = toRadians(latitude);
        double lonRad = toRadians(longitude);
        int earthRadiusInMetres = 6371000;
        double distFrac = distanceInMetres / earthRadiusInMetres;

        double latitudeResult = asin(sin(latRad) * cos(distFrac) + cos(latRad) * sin(distFrac) * cos(brngRad));
        double a = atan2(sin(brngRad) * sin(distFrac) * cos(latRad), cos(distFrac) - sin(latRad) * sin(latitudeResult));
        double longitudeResult = (lonRad + a + 3 * PI) % (2 * PI) - PI;

        System.out.println("latitude: " + toDegrees(latitudeResult) + ", longitude: " + toDegrees(longitudeResult));

        return new LatLng(toDegrees(latitudeResult), toDegrees(longitudeResult));
    }


    /*get the color*/

    public static int getColor(Context mContext, int id) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return mContext.getColor(id);
        } else {
            return ContextCompat.getColor(mContext, id);
        }
    }

    public static long timeStamp(long expireTime, long servertime) {
        return expireTime-servertime;
    }

    public static long timeStampS(long expireTime, long servertime) {


        long currentTime = (Calendar.getInstance().getTimeInMillis()-VariableConstant.SERVERTIMEDIFFERNCE) / 1000;
        long total_time = expireTime - servertime;
        long remain = total_time - (currentTime - servertime);
        if (remain > 0) {
            return remain*1000;//*1000
        } else {
            return 0;
        }
    }

    public static int[] calculateTimeDifference(long reviewRate) {
        String currentDate = Utilities.dateintwtfour();
        int[] duration = {0, 0, 0};
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date curDate;

        try {
            //  rAppDate = simpleDateFormat.parse(reviewRate*1000L);
            curDate = simpleDateFormat.parse(currentDate);
            long timeDiffInMilli = curDate.getTime() - (reviewRate * 1000L);
            int seconds = (int) (timeDiffInMilli / 1000);
            duration[0] = Utilities.getDurationString(seconds)[0];
            duration[1] = Utilities.getDurationString(seconds)[1];
            duration[2] = Utilities.getDurationString(seconds)[2];
            Log.d("TAG", "InvoiceAct calculateTimeDifference duration " + duration[0] + " min "
                    + duration[1] + " sec " + duration[2]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return duration;
    }

    /**
     * Custom method to convert consumed seconds into hh:mm:sec
     *
     * @param seconds: is the total amount of time consumed to reach to pickup adrs
     */
    private static int[] getDurationString(int seconds) {
        int duration[] = {0, 0, 0};
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        //return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
        /*if (hours > 9) {
            duration += String.valueOf(hours);
        } else {
            duration += "0" + String.valueOf(hours);
        }*/
        duration[0] = hours;

        /*if (minutes > 9) {
            duration += " : " + String.valueOf(minutes);
        } else {
            duration += " : 0" + String.valueOf(minutes);
        }*/
        duration[1] = minutes;

       /* if (seconds > 9) {
            duration += " : " + String.valueOf(seconds);
        } else {
            duration += " : 0" + String.valueOf(seconds);
        }*/
        duration[2] = seconds;

        return duration;
    }

    public static String[] jobStatusTime(long jobtime) {
        Log.d("TAGTIME", " expireTime " + jobtime + " server " + jobtime);
        Date date = new Date(jobtime * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a", Locale.US);
        sdf.setTimeZone(VariableConstant.TIMEZONE);
        String formattedDate = sdf.format(date);
        Log.d("TIME", "timeStamp: " + formattedDate + " TIMER " + (date.getTime()));
        //07:19:08 PM
        if (date.getTime() > 0) {
            String splitTimeNMeridian[] = formattedDate.split(" ");
            String splitTime[] = splitTimeNMeridian[0].split(":");
            return new String[]{splitTime[0] + ":" + splitTime[1], splitTimeNMeridian[1]};
        } else {
            return new String[]{"05:30", "AM"};
        }

    }

    /**
     * TODO 根据包名判断是否在最前面显示
     *
     * @param context     {@link Context}
     * @param packageName
     * @return boolean
     * @author Melvin
     * @date 2013-4-23
     */
    private static boolean isTopActivity(Context context, String packageName) {
        if (context == null) {
            return false;
        }//|| isNull(packageName)
        int id = context.checkCallingOrSelfPermission(android.Manifest.permission.GET_TASKS);
        if (PackageManager.PERMISSION_GRANTED != id) {
            return false;
        }

        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
        if (tasksInfo.size() > 0) {
            if (packageName.equals(tasksInfo.get(0).topActivity.getPackageName())) {
                return true;
            }
        }
        return false;
    }


    public static String getFormattedDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        SimpleDateFormat sdf=new SimpleDateFormat("d MMM yyyy',' hh:mm a", Locale.getDefault());
        sdf.setTimeZone(Utilities.getTimeZoneFromLocation());
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);

        if (!((day > 10) && (day < 19)))
            switch (day % 10) {
                case 1:
                    return sdf.format(date);
                case 2:
                    return sdf.format(date);
                case 3:
                    return sdf.format(date);
                default:
                    return sdf.format(date);
            }
        return sdf.format(date);
    }

    public static String getFormattedDateBookingLocation(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        SimpleDateFormat sdf=new SimpleDateFormat("d MMM yyyy',' hh:mm a", Locale.getDefault());
        sdf.setTimeZone(Utilities.getTimeZoneFromBookingLocation());
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);

        if (!((day > 10) && (day < 19)))
            switch (day % 10) {
                case 1:
                    return sdf.format(date);
                case 2:
                    return sdf.format(date);
                case 3:
                    return sdf.format(date);
                default:
                    return sdf.format(date);
            }
        return sdf.format(date);
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {

            if (isTopActivity(context, context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    public static void checkAndShowNetworkError(Context context) {
        if (!isNetworkAvailable(context) && !VariableConstant.IS_NETWORK_ERROR_SHOWED) {
            Intent intent = new Intent(context, NetworkErrorActivity.class);
            context.startActivity(intent);
        }
    }

    public static boolean containsLocation(double latitude, double longitude, List<LatLng> polygon, boolean geodesic) {
        int size = polygon.size();
        if(size == 0) {
            return false;
        } else {
            double lat3 = Math.toRadians(latitude);
            double lng3 = Math.toRadians(longitude);
            LatLng prev = (LatLng)polygon.get(size - 1);
            double lat1 = Math.toRadians(prev.latitude);
            double lng1 = Math.toRadians(prev.longitude);
            int nIntersect = 0;

            double lng2;
            for(Iterator var17 = polygon.iterator(); var17.hasNext(); lng1 = lng2) {
                LatLng point2 = (LatLng)var17.next();
                double dLng3 = MathUtil.wrap(lng3 - lng1, -3.141592653589793D, 3.141592653589793D);
                if(lat3 == lat1 && dLng3 == 0.0D) {
                    return true;
                }

                double lat2 = Math.toRadians(point2.latitude);
                lng2 = Math.toRadians(point2.longitude);
                if(intersects(lat1, lat2, MathUtil.wrap(lng2 - lng1, -3.141592653589793D, 3.141592653589793D), lat3, dLng3, geodesic)) {
                    ++nIntersect;
                }

                lat1 = lat2;
            }

            return (nIntersect & 1) != 0;
        }
    }

    private static boolean intersects(double lat1, double lat2, double lng2, double lat3, double lng3, boolean geodesic) {
        if((lng3 < 0.0D || lng3 < lng2) && (lng3 >= 0.0D || lng3 >= lng2)) {
            if(lat3 <= -1.5707963267948966D) {
                return false;
            } else if(lat1 > -1.5707963267948966D && lat2 > -1.5707963267948966D && lat1 < 1.5707963267948966D && lat2 < 1.5707963267948966D) {
                if(lng2 <= -3.141592653589793D) {
                    return false;
                } else {
                    double linearLat = (lat1 * (lng2 - lng3) + lat2 * lng3) / lng2;
                    return !(lat1 >= 0.0D && lat2 >= 0.0D && lat3 < linearLat) && (lat1 <= 0.0D && lat2 <= 0.0D && lat3 >= linearLat || (lat3 >= 1.5707963267948966D || (geodesic ? Math.tan(lat3) >= tanLatGC(lat1, lat2, lng2, lng3) : MathUtil.mercator(lat3) >= mercatorLatRhumb(lat1, lat2, lng2, lng3))));
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static double tanLatGC(double lat1, double lat2, double lng2, double lng3) {
        return (Math.tan(lat1) * Math.sin(lng2 - lng3) + Math.tan(lat2) * Math.sin(lng3)) / Math.sin(lng2);
    }

    private static double mercatorLatRhumb(double lat1, double lat2, double lng2, double lng3) {
        return (MathUtil.mercator(lat1) * (lng2 - lng3) + MathUtil.mercator(lat2) * lng3) / lng2;
    }

    public static void showKeyBoard(final Context context, final EditText editText) {
        try {
            editText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    InputMethodManager keyboard = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.showSoftInput(editText, 0);
                }
            },1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void hideKeyBoard(final Context context, final EditText editText) {
        try {
            editText.post(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    InputMethodManager keyboard = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(editText.getWindowToken(),0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long setServerTimeAndDifference(long serverGmtTime) {
        Log.d(TAG, "setServerTimeAndDifference: serverGmtTime:"+serverGmtTime+" currenttimeMillis:"+System.currentTimeMillis());
        long difference=System.currentTimeMillis()-(serverGmtTime*1000);
        Log.d(TAG, "setServerTimeAndDifference: "+difference);
        return difference;

    }
    public static TimeZone getTimeZoneFromBookingLocation(){
        String s = TimezoneMapper.latLngToTimezoneString(VariableConstant.BOOKINGLAT,VariableConstant.BOOKINGLONG);
        TimeZone timeZone = TimeZone.getTimeZone(s);
        VariableConstant.BOOKINGTIMEZONE=timeZone;
        Log.d(TAG, "MyEventsPageAdapter: "+s+" "+timeZone.getRawOffset());
        Log.d(TAG, "MyEventsPageAdapter: CurrenttimeMillis: "+System.currentTimeMillis());
        return timeZone;
    }
    public static TimeZone getTimeZoneFromLocation(){
        String s = TimezoneMapper.latLngToTimezoneString(VariableConstant.SPLASHLAT,VariableConstant.SPLASHLONG);
        TimeZone timeZone = TimeZone.getTimeZone(s);
        VariableConstant.TIMEZONE=timeZone;
        Log.d(TAG, "MyEventsPageAdapter: "+s+" "+timeZone.getRawOffset());
        Log.d(TAG, "MyEventsPageAdapter: CurrenttimeMillis: "+System.currentTimeMillis());
        return timeZone;
    }

    public static String doubleformateForLatLng(String datavalue)
    {
        NumberFormat  formatter = new DecimalFormat("#0.000000");
        String datavalueIs;
        if(datavalue.contains(","))
        {
            String value = datavalue.replace(",",".");
            datavalueIs = formatter.format(Double.parseDouble(value));

        }
        else
        {
            datavalueIs = formatter.format(Double.parseDouble(datavalue));
        }

        return datavalueIs;
    }
    public static void getAccessToken(String data, final AccessTokenCallback callback)
    {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            callback.onSuccessAccessToken(jsonobj.getString("data"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            callback.onFailureAccessToken(headerError,"Server Issue");
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        callback.onSessionExpired();
                                        break;
                                    default:
                                        callback.onFailureAccessToken(headerError,error);
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    }
                });
    }

    public static void setSpannableString(Context context, TextView txtSpan, String totalString, String termsString, String privacyPolicy, final Terms_privacy_interface callback) {
        ForegroundColorSpan colorPrimaryTerms = new ForegroundColorSpan(context.getResources().getColor(R.color.red_login));
        ForegroundColorSpan colorPrimaryPrivacy = new ForegroundColorSpan(context.getResources().getColor(R.color.red_login));
        Spannable spanText = new SpannableString(totalString);
        spanText.setSpan(colorPrimaryTerms, totalString.indexOf(termsString),totalString.indexOf(termsString)+termsString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(colorPrimaryPrivacy, totalString.indexOf(privacyPolicy), totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ClickableSpan csterms = new ClickableSpan() {
            @Override
            public void onClick(View v) {
                callback.toTermsLink();
            } };
        ClickableSpan csprivacy = new ClickableSpan() {
            @Override
            public void onClick(View v) {
                callback.toPrivacyPage();
            } };
        spanText.setSpan(csterms, totalString.indexOf(termsString),totalString.indexOf(termsString)+termsString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(csprivacy, totalString.indexOf(privacyPolicy), totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpan.setMovementMethod(LinkMovementMethod.getInstance());
        txtSpan.setText(spanText);
    }
    public static boolean isLatestVersion(String latestVersion)
    {
        String s1 = normalisedVersion(BuildConfig.VERSION_NAME);
        String s2 = normalisedVersion(latestVersion);
        Log.d(TAG, "isLatestVersion: PhoneVersion:"+s1+" appVersion:"+s2);
        return 0 > s1.compareTo(s2);
    }
    private static String normalisedVersion(String version) {
        String[] split = Pattern.compile(".", Pattern.LITERAL).split(version);
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            sb.append(String.format("%" + 10 + 's', s));
        }
        return sb.toString();
    }

    /**
     * method for showing custome alertDialog and closing the current activity
     * @param mActivity context
     */

    public static void appUpdateMandatory(final Activity mActivity)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_app_update_mandatory,null);
        alertDialogBuilder.setView(view);
        TextView tvUpdate= view.findViewById(R.id.tvUpdate);
        if(alertDialog==null){
            alertDialog = alertDialogBuilder.create();
            //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            if(!mActivity.isFinishing())
            alertDialog.show();

            tvUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    alertDialog.dismiss();
                    mActivity.finish();
                    mActivity.overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
                    openPlayStore(mActivity);
                }
            });

            alertDialog.setCancelable(false);
        }else if(!alertDialog.isShowing()){
            alertDialog.show();
        }

    }

    public static void appUpdateNonMandatory(final Activity mActivity, final NonMandatoryUpdateCallback callback)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_app_update_non_mandatory,null);
        alertDialogBuilder.setView(view);
        TextView tvUpdate= view.findViewById(R.id.tvUpdate);
        TextView tvLater= view.findViewById(R.id.tvLater);
        ImageView ivClose  = view.findViewById(R.id.ivClose);
        if(alertDialog==null){
            alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            tvUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    callback.onUpdateClicked();
                    openPlayStore(mActivity);
                }
            });
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    callback.onLaterClicked();
                }
            });
            tvLater.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    callback.onLaterClicked();
                }
            });
        }else if(!alertDialog.isShowing()){
            alertDialog.show();
        }
    }
    public static void checkAndAddEvent(Context context, long bookingId,long bookingTime, SharedPrefs sPrefs) {
        CalendarEventHelper calendarEventHelper=new CalendarEventHelper(context);
        Log.d(TAG, "checkAndAddEvent: bookingId: "+bookingId);
        if(sPrefs.getBookingStatus(bookingId)<=3){
            calendarEventHelper.addEvent(bookingTime,bookingId);
        }
    }

    private static void openPlayStore(Activity mActivity)
    {
        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
        i.setData(Uri.parse(VariableConstant.PLAY_STORE_LINK));
        mActivity.startActivity(i);
    }
    public static  Calendar getCalendarServerTime(){
        Calendar calendar=Calendar.getInstance(VariableConstant.TIMEZONE);
        calendar.setTime(new Date(System.currentTimeMillis()-VariableConstant.SERVERTIMEDIFFERNCE));
        return calendar;
    }
    /**
     * <h1>getEtaInUnits</h1>
     * <p>This method is used to get unit distance in meters to Locations unit</p>
     * @param distanceInM distance in meters
     * @return converted distance in km or miles or yard
     */
    public static String getEtaInUnits(int distanceInM) {
        if(VariableConstant.DISTANCEMATRIXUNIT== DistanceMatrix.UNIT_KM.ordinal()){
            return String.format("%.2f km away",(float)distanceInM/1000);
        }else if(VariableConstant.DISTANCEMATRIXUNIT==DistanceMatrix.UNIT_MILES.ordinal()){
            return String.format("%.2f miles away",(float)distanceInM/1609.03);
        }else
            return String.format("%.2f yards away",(float)distanceInM/0.914);
    }

    public static List<Date> getDatesBetweenUsingJava7(
            Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);

        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();
            datesInRange.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return datesInRange;
    }

    /**
     * method for conveting utctime to custom time format
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToServerFormat(String time) {
        long timestamp = Long.parseLong(time) * 1000 ;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a",Locale.getDefault());
        //sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }

    /**
     * method for converting utc time to timeStamp
     * @param time utc time
     * @return timestamp
     */
    public static long convertUTCToTimeStamp(String time) {

        try {
            long timestamp = Long.parseLong(time) * 1000 ;
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timestamp);
            cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            // cal.setTimeZone(AppController.getInstance().getTimeZone());

            return cal.getTimeInMillis() ;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

}

