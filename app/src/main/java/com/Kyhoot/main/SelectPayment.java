package com.Kyhoot.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.adapter.CardListAdapter;
import com.Kyhoot.controllers.SelectedPaymentTypeImpl;
import com.Kyhoot.interfaceMgr.SelectedCardInfoInterface;
import com.Kyhoot.main.wallet.WalletActivity;
import com.Kyhoot.pojoResponce.CardGetData;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;


import java.util.ArrayList;

public class SelectPayment extends AppCompatActivity implements SelectedCardInfoInterface.SelectedView,View.OnClickListener{

    private static final int WALLETREQUEST = 45;

    private int paymentType;
    private AppTypeface appTypeface;
    SelectedCardInfoInterface.SelectedPresenter presenter;
    AlertProgress alertProgress;
    SharedPrefs sharedPrefs;
    private CardListAdapter cardListAdapter;
    private ArrayList<CardGetData> cardItem = new ArrayList<>();
    TextView tvConfirmAddWalletBalance;
    TextView tvWalletBalance;
    TextView tvWalletBalanceAmt,tvConfirmPaymentCard,tvConfirmPaymentWallet,tvSelectedWalletPay;;
    private BottomSheetDialog bottomSheetDialog;
    TextView tvSelectedCashPay;
    TextView tvConfirmPaymentCash;
    TextView tv_center;
    TextView tvAddCard;
    Toolbar toolBarSelect;
    RecyclerView recyclerViewCard;
    ProgressBar progressPayment;
    LinearLayout rlConfirmCard;
    LinearLayout llSelectCard,llSelectWallet;
    View viewSelectCard;
    View viewSelectWallet;
    private int paidByWallet=0;
    String selectedMTD = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment);
        sharedPrefs =new SharedPrefs(this);
        presenter=new SelectedPaymentTypeImpl(this);
        alertProgress=new AlertProgress(this);
        initializeView();
        typeFaceSetValue();
        toolBarSetValue();
        setVisiblity();

    }

    private void initializeView() {
        appTypeface=AppTypeface.getInstance(this);
        tvWalletBalance = findViewById(R.id.tvWalletBalance);
        tvWalletBalanceAmt = findViewById(R.id.tvWalletBalanceAmt);
        tvConfirmAddWalletBalance = findViewById(R.id.tvConfirmAddWalletBalance);
        rlConfirmCard = findViewById(R.id.rlConfirmCard);
        tvConfirmPaymentCard = findViewById(R.id.tvConfirmPaymentCard);
        tvConfirmPaymentWallet = findViewById(R.id.tvConfirmPaymentWallet);
        tvSelectedWalletPay = findViewById(R.id.tvSelectedWalletPay);
        tvConfirmPaymentCash = findViewById(R.id.tvConfirmPaymentCash);
        tvSelectedCashPay = findViewById(R.id.tvSelectedCashPay);
        tv_center = findViewById(R.id.tv_center);
        toolBarSelect = findViewById(R.id.toolBarSelect);
        recyclerViewCard = findViewById(R.id.recyclerViewCard);
        progressPayment = findViewById(R.id.progressPayment);
        tvAddCard = findViewById(R.id.tvAddCard);
        tvConfirmPaymentCard.setOnClickListener(this);
        tvConfirmPaymentCash.setOnClickListener(this);
        tvConfirmPaymentWallet.setOnClickListener(this);
        tvAddCard.setOnClickListener(this);
        tvSelectedWalletPay.setOnClickListener(this);
        tvSelectedCashPay.setOnClickListener(this);
        tvConfirmAddWalletBalance.setOnClickListener(this);

        llSelectCard = findViewById(R.id.llSelectCard);
        viewSelectCard = findViewById(R.id.viewSelectCard);

        llSelectWallet = findViewById(R.id.llSelectWallet);
        viewSelectWallet=findViewById(R.id.viewSelectWallet);

        selectedMTD = sharedPrefs.getPaymentToShow();
        Log.i("selectedMTD","selectedMTD "+selectedMTD);
    }

    private void setVisiblity() {
        if(VariableConstant.CARDBOOKING)
        {
            llSelectCard.setVisibility(View.VISIBLE);
            viewSelectCard.setVisibility(View.VISIBLE);
        }
        if(VariableConstant.WALLETBOOKING)
        {
            llSelectWallet.setVisibility(View.VISIBLE);
            viewSelectWallet.setVisibility(View.VISIBLE);
        }
        if(VariableConstant.CASHBOOKING)
            tvConfirmPaymentCash.setVisibility(View.VISIBLE);

        /*if(selectedMTD.equalsIgnoreCase("card")){
            llSelectCard.setVisibility(View.VISIBLE);
            viewSelectCard.setVisibility(View.VISIBLE);
        }
        if(selectedMTD.equalsIgnoreCase("wallet")){
            llSelectWallet.setVisibility(View.VISIBLE);
            viewSelectWallet.setVisibility(View.VISIBLE);
        }
        if(selectedMTD.equalsIgnoreCase("cash")){
            tvConfirmPaymentCash.setVisibility(View.VISIBLE);
        }*/
    }

    private void toolBarSetValue()
    {
        setSupportActionBar(toolBarSelect);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_center.setText(getString(R.string.payment_method));
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        toolBarSelect.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void typeFaceSetValue()
    {


        tvConfirmPaymentCard.setTypeface(appTypeface.getHind_bold());
        tvConfirmPaymentCash.setTypeface(appTypeface.getHind_bold());
        tvConfirmPaymentWallet.setTypeface(appTypeface.getHind_bold());
        tvWalletBalanceAmt.setTypeface(appTypeface.getHind_regular());
        tvWalletBalanceAmt.setText(VariableConstant.walletCurrency+" "+VariableConstant.walletBalance);
        //tvConfirmAddMoreCard.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmAddWalletBalance.setTypeface(appTypeface.getHind_semiBold());
        tvSelectedCashPay.setTypeface(appTypeface.getHind_semiBold());

        tvSelectedWalletPay.setTypeface(appTypeface.getHind_semiBold());
        tvSelectedCashPay.setTypeface(appTypeface.getHind_semiBold());

        LinearLayoutManager llManager = new LinearLayoutManager(this);
        recyclerViewCard.setLayoutManager(llManager);
        cardListAdapter = new CardListAdapter(this,cardItem,this,selectedMTD);
        recyclerViewCard.setAdapter(cardListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Utilities.isNetworkAvailable(this))
        {
            onShowProgress();
            presenter.onGetCards(sharedPrefs);
        }else
            alertProgress.showNetworkAlert();

    }


    public void onPaymentSelection(View v)
    {

    }
    private void setPaymentSelectionType(int i)
    {
        tvConfirmPaymentCard.setSelected(false);
        tvConfirmPaymentCash.setSelected(false);
        tvConfirmPaymentWallet.setSelected(false);
        tvSelectedWalletPay.setVisibility(View.GONE);
        tvSelectedCashPay.setVisibility(View.GONE);
        switch (i)
        {
            case 1:
                paymentType=2;
                paidByWallet=0;
                tvConfirmPaymentCard.setSelected(true);
                rlConfirmCard.setVisibility(View.VISIBLE);
                break;
            case 2:
                paymentType=1;
                paidByWallet=0;
                tvConfirmPaymentCash.setSelected(true);
                tvSelectedCashPay.setVisibility(View.VISIBLE);
                break;
            case 3:
                tvConfirmPaymentWallet.setSelected(true);
                if(Double.parseDouble(VariableConstant.walletBalance)>0){
                    tvSelectedWalletPay.setVisibility(View.VISIBLE);
                    showBottomSheetDialog();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message) {

    }

    @Override
    public void onError(String error) {
        alertProgress.alertinfo(error);
    }

    @Override
    public void onShowProgress() {
        progressPayment.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgress() {
        progressPayment.setVisibility(View.GONE);
    }

    @Override
    public void onToCallIntent() {

        Intent intent = new Intent(this, SignupPayment.class);
        intent.putExtra("coming_From","LiveBooking");
        startActivity(intent);
        overridePendingTransition(R.anim.side_slide_out,R.anim.stay_still);
    }

    @Override
    public void onToBackIntent(int adapterPosition)
    {
        paymentType = 2;
        CardGetData data = cardItem.get(adapterPosition);
        Intent intent = new Intent();
        intent.putExtra("PAYMENTTYPE",paymentType);
        intent.putExtra("CARDID",data.getId());
        intent.putExtra("CARDTYPE",data.getBrand());
        intent.putExtra("LAST4",data.getLast4());
        setResult(RESULT_OK,intent);
        finish();
        sharedPrefs.setPaymentToShow("card");
        overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
    }

    @Override
    public void addItems(ArrayList<CardGetData> cardsList)
    {
        cardItem.clear();
        cardItem.addAll(cardsList);
        cardListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onVisibilitySet() {

        tvSelectedWalletPay.setVisibility(View.GONE);
        tvSelectedCashPay.setVisibility(View.GONE);
        tvConfirmPaymentWallet.setSelected(false);
        tvConfirmPaymentCash.setSelected(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            if(requestCode==WALLETREQUEST) {
                tvWalletBalanceAmt.setText(VariableConstant.walletCurrency+" "+VariableConstant.walletBalance);
            }
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId())
        {
            case R.id.tvSelectedCashPay:
                paymentType = 1;
                intent = new Intent();
                intent.putExtra("PAYMENTTYPE",paymentType);
                intent.putExtra("PAIDBYWALLET",paidByWallet);
                setResult(RESULT_OK,intent);
                finish();
                sharedPrefs.setPaymentToShow("cash");
                overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);

                break;

            case R.id.tvSelectedWalletPay:
                intent = new Intent();
                intent.putExtra("PAYMENTTYPE",paymentType);
                intent.putExtra("PAIDBYWALLET",paidByWallet);
                setResult(RESULT_OK,intent);
                finish();
                sharedPrefs.setPaymentToShow("wallet");
                overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
                break;

            case R.id.tvAddCard:
                onToCallIntent();


                break;
            case R.id.tvConfirmAddWalletBalance:
                Intent intentq = new Intent(this, WalletActivity.class);
                VariableConstant.WALLETUPDATEINTENT=true;
                startActivityForResult(intentq, WALLETREQUEST);
                break;
            case R.id.tvConfirmPaymentCard:
                setPaymentSelectionType(1);
                break;
            case R.id.tvConfirmPaymentCash:
                setPaymentSelectionType(2);
                cardListAdapter.onAdapterChanged(-1);
                break;
            case R.id.tvConfirmPaymentWallet:
                setPaymentSelectionType(3);
                cardListAdapter.onAdapterChanged(-1);
                break;
            case R.id.btn_cash:
                paymentType = 1;
                paidByWallet=1;
                bottomSheetDialog.dismiss();
                break;

            case R.id.btn_card:
                paymentType = 2;
                paidByWallet=1;
                bottomSheetDialog.dismiss();
                break;
        }
    }
    //Excess fare payment dialog if wallet is selected
    public void showBottomSheetDialog() {
        View view = getLayoutInflater().inflate(R.layout.walletbottomsheet, null);
        TextView tvExcessAmntDesc=view.findViewById(R.id.tvExcessAmntDesc);
        Button btn_card=view.findViewById(R.id.btn_card);
        Button btn_cash=view.findViewById(R.id.btn_cash);
        if(sharedPrefs.getDefaultCardNum().isEmpty()){
            btn_card.setVisibility(View.GONE);
        }
        tvExcessAmntDesc.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        btn_card.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        btn_cash.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        btn_card.setOnClickListener(this);
        btn_cash.setOnClickListener(this);
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if(VariableConstant.CASHBOOKING){
                    setPaymentSelectionType(1);
                }
                setPaymentSelectionType(2);
            }
        });
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCancelable(true);
        bottomSheetDialog.show();
    }
}
