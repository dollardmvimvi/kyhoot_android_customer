package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 5/6/18.
 */
public class BookingTypeAction
{
    private boolean schedule;

    private boolean now;

    private boolean repeat;

    public boolean getSchedule ()
    {
        return schedule;
    }


    public boolean getNow ()
    {
        return now;
    }

    public boolean getRepeat ()
    {
        return repeat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [schedule = "+schedule+", now = "+now+", repeat = "+repeat+"]";
    }
}