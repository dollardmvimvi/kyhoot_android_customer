package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>SignUpInPojo</h>
 * Created by Ali on 8/28/2017.
 */

public class SignUpInPojo implements Serializable
{
    private int errNum;
    private int errFlag;
 private String errMsg;
 private SignUpData data;

    public int getErrNum()
 {
  return errNum;
 }

    public int getErrFlag() {
        return errFlag;
 }

    public void setErrFlag(int errFlag) {
        this.errFlag = errFlag;
 }

 public String getErrMsg() {
  return errMsg;
 }

 public SignUpData getData() {
  return data;
 }

 public void setData(SignUpData data) {
  this.data = data;
 }
}
