package com.Kyhoot.pojoResponce;

/**
 * <h>MyProfile_pojo</h>
 * Created by admin-3embed on 13/6/15.
 */
public class MyProfile_pojo {
    private int errNum;

    private String errMsg;

    private MyProfile_Data_Pojo data;

    private int errFlag;

    private String statusCode;

    public String getStatusCode() {
        return statusCode;
    }


    public int getErrNum()
    {
        return errNum;
    }


    public String getErrMsg ()
    {
        return errMsg;
    }


    public MyProfile_Data_Pojo getData ()
    {
        return data;
    }


    public int getErrFlag()
    {
        return errFlag;
    }


    @Override
    public String toString()
    {
        return "ClassPojo [errNum = "+errNum+", errMsg = "+errMsg+", data = "+data+", errFlag = "+errFlag+"]";
    }
}
