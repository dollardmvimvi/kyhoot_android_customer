package com.Kyhoot.main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.CardValidation.CardUtils;
import com.Kyhoot.R;
import com.Kyhoot.controllers.CardPaymentController;
import com.Kyhoot.interfaceMgr.ImageUploadedAmazon;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.models.CardPaymentModel;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * <h>SignupPayment</h>
 * Created by embed on 11/10/16.
 */
public class SignupPayment extends AppCompatActivity implements View.OnClickListener, UtilInterFace, ImageUploadedAmazon
        , View.OnFocusChangeListener, TextWatcher {
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 127;
    public String PUBLISHABLE_KEY;
    private TextView tvdonepayment, tv_skip, tvscancard, poweredby,tvOr;
    private EditText etcard, etcardname, etExpiryDate, etcardcvv;
    private TextInputLayout cardlayout,cardnamelayout,expiryDateLayout,cardcvv;
    private RelativeLayout rldone, rlscan;
    private RelativeLayout stripe_paypal;
    private String result;
    private int expiryMonth = 0, expiryYear = 0;
    private ProgressBar progressbar;
    private SharedPrefs manager;
    private CreditCard scanResult;
    private Typeface semibold, regular, bold;
    private String coming_From, cardType = "";
    private boolean scanflag = false;
    private int MY_SCAN_REQUEST_CODE = 100;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    private CardPaymentController cardPaymentController;
    private CardPaymentModel cardPaymentModel;
    private ImageView ivCardLogo;
    private AlertProgress aDialog;
    InputMethodManager imm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        setContentView(R.layout.signup_payment);
        setFonts();
        Intent intent = getIntent();
        if (intent != null) {
            coming_From = getIntent().getStringExtra("coming_From");
            Log.d("TAG", "coming_From signup payment :: " + coming_From);
        }
        manager = new SharedPrefs(this);
        initialize();
    }

    private void initialize() {
        cardPaymentController = new CardPaymentController(this);
        cardPaymentModel = new CardPaymentModel();
        aDialog = new AlertProgress(this);
        PUBLISHABLE_KEY = manager.getPaymentId();
        progressbar = findViewById(R.id.progressbar);
        stripe_paypal = findViewById(R.id.stripe_paypal);
        Toolbar toolbar = findViewById(R.id.toobarPayment);
        tvdonepayment = findViewById(R.id.tvdonepayment);
        tvscancard = findViewById(R.id.tvscancard);
        tv_skip = findViewById(R.id.tv_skip);

        poweredby = findViewById(R.id.poweredby);
        cardlayout = findViewById(R.id.cardlayout);
        expiryDateLayout = findViewById(R.id.expiryDateLayout);
        cardcvv = findViewById(R.id.cardcvv);
        cardnamelayout = findViewById(R.id.cardnamelayout);
        etcard = findViewById(R.id.etcard);
        etcardname = findViewById(R.id.etcardname);
        etExpiryDate = findViewById(R.id.etExpiryDate);
        etcardcvv = findViewById(R.id.etcardcvv);
        rlscan = findViewById(R.id.rlscan);
        rldone = findViewById(R.id.rldone);
        ivCardLogo = findViewById(R.id.ivCardLogo);
        tvOr=findViewById(R.id.tvOr);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        TextView tv_center = findViewById(R.id.tv_center);
        if (coming_From.equals("payment_Fragment") || coming_From.equals("LiveBooking")) {
            toolbar.setNavigationIcon(R.drawable.ic_login_back_icon_off);
            getSupportActionBar().setTitle("");
            tv_center.setText(R.string.addCard);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            tv_skip.setVisibility(View.GONE);
        }
        /*else
        {
			tv_skip.setVisibility(View.VISIBLE);
			tv_skip.setText(getResources().getString(R.string.skip));
		}*/

        tvdonepayment.setOnClickListener(this);
        tv_skip.setOnClickListener(this);
        tvscancard.setOnClickListener(this);
        etcard.setOnFocusChangeListener(this);
        etExpiryDate.setOnFocusChangeListener(this);
        etcardcvv.setOnFocusChangeListener(this);
        etcard.addTextChangedListener(this);
        etExpiryDate.setFocusable(false);
        etExpiryDate.setOnClickListener(this);
        tvdonepayment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_DONE) {
                    calldonebutton();
                    return true;
                }
                return false;
            }
        });
        etcardname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT ) {
                    cardPaymentController.openExpiryDatePickr(SignupPayment.this);
                    return true;
                }
                return false;
            }
        });
        setfont();
        tv_center.setTypeface(bold);
    }

    public void setFonts() {
        semibold = AppTypeface.getInstance(this).getHind_semiBold();
        regular = AppTypeface.getInstance(this).getHind_regular();
        bold = AppTypeface.getInstance(this).getHind_semiBold();
    }

    private void setfont() {
        etcard.setTypeface(regular);
        etcardname.setTypeface(regular);
        etExpiryDate.setTypeface(regular);
        etcardcvv.setTypeface(regular);
        tvscancard.setTypeface(semibold);
        tvdonepayment.setTypeface(semibold);
        poweredby.setTypeface(regular);
        cardnamelayout.setTypeface(regular);
        cardlayout.setTypeface(regular);
        cardcvv.setTypeface(regular);
        expiryDateLayout.setTypeface(regular);
        tvOr.setTypeface(regular);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        imm.hideSoftInputFromWindow(etcard.getWindowToken(), 0);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        imm.hideSoftInputFromWindow(etcard.getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_skip:
                Intent intent = new Intent(SignupPayment.this, MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case R.id.tvdonepayment:
                calldonebutton();
                break;
            case R.id.tvscancard:
                if (Build.VERSION.SDK_INT >= 23) {
                    myPermissionConstantsArrayList = new ArrayList<>();
                    myPermissionConstantsArrayList.clear();
                    myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
                    if (AppPermissionsRunTime.checkPermission(SignupPayment.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                        //TODO: if all permissions are already granted
                        onScanPress();
                    }
                } else {
                    onScanPress();
                }
                break;
            case R.id.etExpiryDate:
            case R.id.lloutname:
                cardPaymentController.openExpiryDatePickr(this);
                break;
        }
    }

    private void onScanPress() {
        Intent scanIntent = new Intent(this, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: true
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, true);
        int MY_SCAN_REQUEST_CODE = 100;
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    public void calldonebutton() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tvdonepayment.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        String cardNum=getCardNumber();
        final String cvv = etcardcvv.getText().toString().trim();
        if (cardPaymentController.checktheValidity(cardNum, expiryMonth, expiryYear, cvv))//,expiryMonth,expiryYear
        {

            showProgress();
            stripe_paypal.setVisibility(View.GONE);
            updateUi(false, Utilities.getColor(SignupPayment.this, R.color.red_loginLight), Utilities.getColor(SignupPayment.this, R.color.grayLightTextcolor));

            Card card = new Card(
                    cardNum,
                    expiryMonth,
                    expiryYear,
                    etcardcvv.getText().toString()
            );
            //	final ImageUploadedAmazon respon = this;
            final UtilInterFace respon = this;
            new Stripe(this).createToken(card, PUBLISHABLE_KEY, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    hideProgress();

                    aDialog.alertinfo(error.getMessage());
                    updateUi(true, Utilities.getColor(SignupPayment.this, R.color.red_login), Utilities.getColor(SignupPayment.this, R.color.white));
                }

                @Override
                public void onSuccess(com.stripe.android.model.Token token) {

                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("cardToken", token.getId());
                        jsonObject.put("email", manager.getEMail());
                        Log.i("TAG", "cardrequest " + jsonObject);

                        cardPaymentModel.addCardService(jsonObject, manager, respon);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (scanflag) {
            scanCardResult();
        }else if(etcard.getText().toString().equals("")){
            etcard.requestFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }

    }

    private void scanCardResult() {
        etcard.setText(scanResult.cardNumber);
        String expiryyear = String.valueOf(scanResult.expiryYear);
        String scancardmonth = String.valueOf(scanResult.expiryMonth);
        expiryMonth = scanResult.expiryMonth;
        expiryYear = scanResult.expiryYear;
        if (scanResult.expiryMonth < 10) {
            scancardmonth = "0" + scancardmonth;
            etExpiryDate.setText(scancardmonth + "/" + expiryyear);
        } else {
            etExpiryDate.setText(scancardmonth + "/" + expiryyear);
        }
        etcardcvv.setText(scanResult.cvv);
        etcardname.requestFocus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
            scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
            scanflag = true;
        }
    }

    /**
     * predefined method to check run time permissions list call back
     *
     * @param permissions:  contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean isDenine = false;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    Log.i("Permission", "Denied ");
                } else {
                    onScanPress();
                    //TODO: if permissions granted by user, move forward
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void updateUi(boolean b, int color, int color1) {
        tvdonepayment.setClickable(b);
        tvscancard.setClickable(b);
        tvdonepayment.setEnabled(b);
        tvscancard.setEnabled(b);
        rldone.setBackgroundColor(color);
        rlscan.setBackgroundColor(color);
        tvdonepayment.setTextColor(color1);
        tvscancard.setTextColor(color1);
    }


    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressbar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onSuccess(String onSuccess) {
        switch (coming_From) {
            case "confirm_no":
                Intent intent = new Intent(SignupPayment.this, MenuActivity.class);
                startActivity(intent);
                finish();

                break;
            case "payment_Fragment":
                finish();
                break;
            case "LiveBooking":
                finish();
                break;
        }
    }

    @Override
    public void onError(String errormsg) {

        hideProgress();
        stripe_paypal.setVisibility(View.VISIBLE);
        updateUi(true, Utilities.getColor(SignupPayment.this, R.color.red_login), Utilities.getColor(SignupPayment.this, R.color.white));
        aDialog.alertinfo(errormsg);
    }

    @Override
    public void onSuccess(ArrayList<AddressPojo.AddressData> reviewList) {

    }

    @Override
    public void onSuccessReview(ValidatorPojo reviewlist) {

    }

    @Override
    public void onSuccessAdded(String msg) {
        etExpiryDate.setText(msg);
        String[] split = msg.split("/");
        expiryMonth = Integer.parseInt(split[0]);
        expiryYear = Integer.parseInt(split[1]);

      //  etcardcvv.setFocusable(true);
        etcardcvv.requestFocus();
    }

    @Override
    public void onerror(String errormsg) {

    }
    //This method is used to get the card number without spaces
    private String getCardNumber(){
        String formattedCardnum="";
        String temp[]=etcard.getText().toString().split(" ");
        for(int i=0;i<temp.length;i++){
            formattedCardnum=formattedCardnum+temp[i].trim();
        }
        Log.d("SIGNUPPAYMENT", "getCardNumber: "+formattedCardnum);
        return formattedCardnum;
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        switch (v.getId()) {
            case R.id.etcard:
                if (!hasFocus) {

                    Log.d("SIGNUPPAYMENT", "onFocusChange: " + CardUtils.isValidCardNumber(getCardNumber()));
                    if (!CardUtils.isValidCardNumber(getCardNumber())) {
                        cardlayout.setErrorEnabled(true);
                        cardlayout.setError("Invalid card");
                    } else {
                        cardlayout.setErrorEnabled(false);
                        cardlayout.setError("");
                    }

                }
                break;
            case R.id.etExpiryDate:
                if (hasFocus) {
                    cardPaymentController.openExpiryDatePickr(this);
                }
            case R.id.etcardcvv:
                if(hasFocus){

                }
                break;

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d("SIGNUPPAYMENT", "onTextChanged: s="+s+" start="+start+" before="+before+" count "+count);
        int len=s.length();
        if(count==1){
            if(len==5||len==10||len==15){
                etcard.setText(s.subSequence(0,len-1)+" "+s.charAt(len-1));
                etcard.setSelection(etcard.getText().length());
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (etcard.getText().toString().length() > 3) {
            cardType = CardUtils.getPossibleCardType(etcard.getText().toString(), true);
            Log.d("TAG", "afterTextChanged: " + cardType);
            Bitmap bitmap = Utilities.setCreditCardLogo(cardType, this);
            ivCardLogo.setImageBitmap(bitmap);
            switch (cardType) {
                case "Visa":
                case "MasterCard":
                case "Discover":
                case "Diners Club":
                    // etcardcvv.setEms(3);
                    etcardcvv.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                    break;
                case "American Express":
                    etcardcvv.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                    break;
                default:
                    etcardcvv.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                    break;

            }
        } else {
            Bitmap bitmap = Utilities.setCreditCardLogo("AmexNone", this);
            ivCardLogo.setImageBitmap(bitmap);
        }
    }
}
