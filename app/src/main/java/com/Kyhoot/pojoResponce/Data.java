package com.Kyhoot.pojoResponce;

public class Data
{
    private String surgePrice;

    public String getSurgePrice ()
    {
        return surgePrice;
    }

    public void setSurgePrice (String surgePrice)
    {
        this.surgePrice = surgePrice;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [surgePrice = "+surgePrice+"]";
    }
}
