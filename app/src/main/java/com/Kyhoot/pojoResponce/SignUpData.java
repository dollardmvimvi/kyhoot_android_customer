package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h></h>
 * Created by Ali on 8/28/2017.
 */

public class SignUpData implements Serializable
{
    /*"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OWE0MjEyMGY5Y2UxNjUyYzVhN2QyNmIiLCJhY2Nlc3NDb2RlIjo3NDI0LCJpYXQiOjE1MDM5Mjg2MDgsImV4cCI6MTUwNjUyMDYwOCwic3ViIjoic2xhdmUifQ.1kJDDzrHI6mEyYan-_Rbm2pqqWo-pvaYESVT7ar6GgU",
"email":"piyushd@mobifyi.com",
"phone":"9624745861",
"countryCode":"+91",
"firstName":"piyush",
"lastName":"dhameliya",
"profilePic":"http://www.teamkrishna.in/images/developer/default_user-2.jpg",
"referralCode":"XCNZAF",
"currencyCode":"$"*/
    private String token, email = "", phone = "", countryCode, firstName = "", lastName, profilePic, referralCode, currencyCode,
            fcmTopic,
    password = "",datOB = "",fbId = "";
    private int loginType = 1;
    private String sid,paymentId,PublishableKey,requester_id;
    private CardDetails cardDetail;


    public SignUpData() {
    }

    public String getReferralCode() {
        return referralCode;
    }

    public String getRequester_id() {
        return requester_id;
    }

    public CardDetails getCardDetail() {
        return cardDetail;
    }
    public String getFcmTopic() {
        return fcmTopic;
    }

    public String getSid() {
        return sid;
    }
    public String getPaymentId() {
        return paymentId;
    }
    public String getPublishableKey() {
        return PublishableKey;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    public String getDatOB() {
        return datOB;
    }

    public void setDatOB(String datOB) {
        this.datOB = datOB;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getReferralcode() {
        return referralCode;
    }

    public void setReferralcode(String referralcode) {
        this.referralCode = referralcode;
    }
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public class CardDetails
    {
        private String name;
        private String last4;
        private String id;
        private String brand;
        private String funding;
        private boolean isDefault;

        public String getName() {
            return name;
        }

        public String getLast4() {
            return last4;
        }

        public String getId() {
            return id;
        }

        public String getBrand() {
            return brand;
        }

        public String getFunding() {
            return funding;
        }

        public boolean isDefault() {
            return isDefault;
        }
    }
}
