package com.Kyhoot.controllers;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.models.ProfileModel;

/**
 * <h>ProfileController</h>
 * Profile fragment controller
 * Created by Ali on 8/25/2017.
 */

public class ProfileController
{
    private Context mcontext;
    private ProfileModel profileModel;
    public ProfileController(Context mcontext)
    {
        this.mcontext = mcontext;
        profileModel = new ProfileModel(mcontext);
    }

    public void openPasswordDialog()
    {
        final TextInputEditText et_oldPasswd;
        final Dialog dialog = new Dialog(mcontext);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_dialog);
        et_oldPasswd = dialog.findViewById(R.id.et_oldPasswd);
        et_oldPasswd.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        //Setting on click listener on the ok button on dialog
        dialog.findViewById(R.id.tvPaswrdDialg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_oldPasswd.getText().toString().equals("")) {
                    Toast.makeText(mcontext,mcontext.getString(R.string.password_not_empty),Toast.LENGTH_SHORT).show();
                }
                else {
                    dialog.dismiss();
                    profileModel.checkCurrentPassAPI(et_oldPasswd.getText().toString());
                }
            }
        });
        dialog.show();
    }

}
