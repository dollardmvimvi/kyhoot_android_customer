package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.SupportModelInterFace;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.Support_pojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

/**
 * <h>SupportModel</h>
 * call the service for the support
 * Created by ${Ali} on 8/21/2017.
 */

public class SupportModel {

    /**
     * calling volley request for support add setting the adapter
     *
     * @param sessionManager {@see SessionManager }
     * @param resLstnr       {@see ResponceListner}
     */
    public void callSupportService(SharedPrefs sessionManager, final SupportModelInterFace resLstnr) {
        try {
            JSONObject jsonObject = new JSONObject();
            //   jsonObject.put("lang", 0);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "support/" + 1, OkHttpConnection.Request_type.GET, jsonObject, sessionManager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result) {

                    Log.d("TAG", "onSuccess: " + result + " headerResponse " + headerResponse);
                    Gson gson = new Gson();
                    Support_pojo support_pojo = gson.fromJson(result, Support_pojo.class);
                    // if (support_pojo.getErrFlag().equals("0") && support_pojo.getErrNum().equals("200")) {
                    resLstnr.onSupportSuccess(support_pojo);
                   /* } else {

                        resLstnr.onError(support_pojo.getErrMsg());
                    }*/
                }

                @Override
                public void onError(String headerError, String error) {
                    try{
                        ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                        resLstnr.onError(errorHandel.getMessage());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
