package com.Kyhoot.controllers;

import android.app.Activity;
import android.text.TextUtils;

import com.facebook.CallbackManager;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.LoginResponceIntr;
import com.Kyhoot.models.LoginModel;
import com.Kyhoot.models.SignupModel;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.pojoResponce.SignUpData;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.FacebookLogin;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Validator;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>LoginController</h>
 * this controller checks the validity and update the user interface
 * Created by Ali on 8/22/2017.
 */

public class LoginController
{
    LoginModel lModel;
    private Activity mcontext;
    private Validator validator;
    private LoginResponceIntr loginResponceIntr;
    public LoginController(Activity mcontext, LoginResponceIntr loginResponceIntr)
    {
        this.mcontext = mcontext;
        validator = new Validator();
        this.loginResponceIntr = loginResponceIntr;
        lModel = new LoginModel();

    }

    /**
     * <h1>isEmailValid</h1>
     * this mathod is validiting the user input email or phone number provided by him
     * @param email this is users phone number or email id
     * @param checkValidty  check valid interface
     * @return return if the validation is true or not
     */

    public boolean isEmailValid(String email, LoginResponceIntr.CheckValidty checkValidty) {
        return lModel.isEmailValid(email, checkValidty, mcontext);
    }


    public void facebook(CallbackManager callbackManager, final CheckNetworkAvailablity networkavailble, final AlertProgress progress)
    {
        FacebookLogin fbLogin = new FacebookLogin(mcontext);
        fbLogin.refreshToken();
        fbLogin.facebook_login(callbackManager, fbLogin.createFacebook_requestData(), new FacebookLogin.Facebook_callback() {
            @Override
            public void sucess(JSONObject json)
            {
                try {
                    String fbId = json.getString("id");
                    String fb_emailId = json.getString("email");
                    String fb_birthDay = json.optString("birthday");
                    String Fb_firstName = json.optString("first_name");
                    String fb_lastName = json.optString("last_name");
                    String DateOfBirth = new SignupModel().getForMatedDOB(mcontext, fb_birthDay);
                    checkNetwork(progress, networkavailble, fb_emailId, fbId, 2, DateOfBirth, Fb_firstName, fb_lastName);
                } catch (JSONException e) {
                    try
                    {
                        String fbId =  json.getString("id");
                        String fb_birthDay = json.optString("birthday");
                        String  Fb_firstName = json.optString("first_name");
                        String fb_lastName = json.optString("last_name");
                        String DateOfBirth = new SignupModel().getForMatedDOB(mcontext, fb_birthDay);
                        checkNetwork(progress, networkavailble, "", fbId, 2, DateOfBirth, Fb_firstName, fb_lastName);
                    }catch (JSONException e1)
                    {
                      //  fbResp.onError(e1.getMessage());
                        loginResponceIntr.onError(e.getMessage());
                        e1.printStackTrace();
                    }
                    loginResponceIntr.onError(e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void error(String error)
            {
                loginResponceIntr.onError(error);
            }

            @Override
            public void cancel(String cancel)
            {
                loginResponceIntr.onError(cancel);
            }
        });
    }

    private void checkNetwork(AlertProgress progress, CheckNetworkAvailablity networkavailble, String fb_emailId, String fbId, int logintype, String dateOfBirth, String fb_firstName, String fb_lastName) {
        LoginPojo loginPojo = new LoginPojo();
        if (progress.isNetworkAvailable()) {
            SignUpData loginData = new SignUpData();
            loginPojo.setData(loginData);
            loginPojo.getData().setFbId(fbId);
            loginPojo.getData().setPassword(fbId);
            loginPojo.getData().setEmail(fb_emailId);
            loginPojo.getData().setLoginType(logintype);
            loginPojo.getData().setDatOB(dateOfBirth);
            loginPojo.getData().setFirstName(fb_firstName);
            loginPojo.getData().setLastName(fb_lastName);
            networkavailble.networkAvalble(loginPojo);
        } else
            networkavailble.notNetworkAvalble();
    }

    public void CheckNetworkAvailablity(AlertProgress progress, CheckNetworkAvailablity networkavailble, String email, String password, int logintype, String proflpic)
    {
        LoginPojo loginPojo = new LoginPojo();
        if(progress.isNetworkAvailable())
        {
            SignUpData loginData = new SignUpData();
            loginPojo.setData(loginData);
            loginPojo.getData().setFbId(password);
            loginPojo.getData().setPassword(password);
            loginPojo.getData().setEmail(email);
            loginPojo.getData().setLoginType(logintype);
            loginPojo.getData().setProfilePic(proflpic);
            networkavailble.networkAvalble(loginPojo);
        }
        else
            networkavailble.notNetworkAvalble();

    }

    public void serviceMethod(LoginPojo loginPojo, LoginResponceIntr lgIntr)
    {
        lModel.signinService(loginPojo, lgIntr, new SharedPrefs(mcontext));
    }

    public void checkPassword(String password, LoginResponceIntr.CheckValidty checkValidty) {
        if (TextUtils.isEmpty(password)) {
            checkValidty.passwordValid();
        }
    }

    public void checkEmailValidity(String email, LoginResponceIntr.CheckValidty checkValidty) {
       /* if (TextUtils.isEmpty(email)) {
            checkValidty.emailEmpty();
        } else if (!isEmailValid(email, checkValidty)) {
            checkValidty.emailValid();
        }*/
       if (TextUtils.isEmpty(email)) {
            checkValidty.emailEmpty();
        } else {
           isEmailValid(email, checkValidty);
           // checkValidty.emailValid();
        }
    }
}
