package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 30/1/18.
 */

public class ServiceSelectedPojo {
    String serviceId;
    double price;
    int quantity;
    private ServiceSelectedPojo(){

    }
    public ServiceSelectedPojo(String serviceId,double price,int quantity){
        this.serviceId=serviceId;
        this.price=price;
        this.quantity=quantity;
    }
    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
