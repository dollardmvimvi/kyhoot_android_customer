package com.Kyhoot.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.Kyhoot.R;
import com.Kyhoot.controllers.LiveBookingStatusController;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.pojoResponce.MyEventStatus;
import com.Kyhoot.pojoResponce.MyEventStatusObservable;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.MyNetworkChangeListner;
import com.Kyhoot.utilities.NetworkChangeReceiver;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BookingPendingAccept extends AppCompatActivity {

    public int selectedpostion = -1;
    private long bid;
    //private String Imageurl;
    private int status;
    private ArrayList<AllEventsDataPojo> pendingEventPojo;
    private CountDownTimer timer;
    private TextView tvRemainingTime;
    private AppTypeface appTypeface;
    private AlertProgress alertProgress;
    private SharedPrefs sprefs;
    //private ImageView ivCustomer;
    private SupportMapFragment mGoogleMap;
    private AppBarLayout app_barPending;
    private  Observer<MyEventStatus> observer;
    private CompositeDisposable disposable;
    private LinearLayout payment_layout,llLiveFee;
    private RelativeLayout rlconfm_visitfee,rlconfm_travelfee,rldiscount,rlLastdues;
    private TextView tvPaymentMethod,tvTotalBillAmount,tvJobLocation,tvPaymentBreak;
    private TextView tvConfirmCardNumber,tvVisitFee,tvVisitFeeAmt,tv_travelFeeFee,tvTravelFeeAmt,
            tvDiscount,tvDiscountAmount,tvTotal,tvTotalAmt,tvCategory,tvCategoryName;
    private ImageView ivCard;
    private TextView tvLastDues,tvLastDuesAmt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_pending_accept2);
        sprefs = new SharedPrefs(this);
        disposable = new CompositeDisposable();
        VariableConstant.isBookingPending = true;
      //  getIntentView();

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            bid = extras.getLong("BID", 0);
            //Imageurl = getIntent().getStringExtra("ImageUrl");
            status = extras.getInt("STATUS", 0);

             pendingEventPojo = (ArrayList<AllEventsDataPojo>) extras.getSerializable("ALLPOJO");
            Log.d("TAG", "getIntentView: " + status + " status " + pendingEventPojo.get(0).getStatus()
                    + " BID " + bid + " BID1 " + pendingEventPojo.get(0).getBookingId());
        }
        initializeView();
        initializeToolBar();
        initializeRxJava();
        mGoogleMap = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapPloting();
        checkNetworkListener();

    }

    private void checkNetworkListener() {

        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Log.d("BOOKINGPENDING"," NetworkStatus "+nwStatus);
                if(nwStatus)
                {

                    Log.d("TAGBOOKING", "onNetworkStateChanges: "+ ApplicationController.getInstance().isMqttConnected());

                   /* if(!ApplicationController.getInstance().isMqttConnected())
                        ApplicationController.getInstance().createMQttConnection(sprefs.getCustomerSid());*/
                }
            }
        });
    }

    private void mapPloting() {
        if (mGoogleMap != null) {
            mGoogleMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    loadMap(googleMap);
                }
            });
        }
    }

    private void loadMap(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        double cusLat = Double.parseDouble(sprefs.getSplashLatitude());
        double cusLng = Double.parseDouble(sprefs.getSplashLongitude());
        LatLng latlng = new LatLng(cusLat,cusLng);
        final  LatLng latlong = latlng;
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 12);
        googleMap.animateCamera(cameraUpdate);
        googleMap.moveCamera(cameraUpdate);
        final GoogleMap mMap = googleMap;
        ImageView bookPendingCurrentLoc = findViewById(R.id.bookPendingCurrentLoc);
        bookPendingCurrentLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 12));
            }
        });
    }

    private void initializeToolBar() {

        alertProgress = new AlertProgress(this);
        Toolbar login_tool = findViewById(R.id.toolbar);
        setSupportActionBar(login_tool);
        TextView cancelAction = findViewById(R.id.cancelAction);
        getSupportActionBar().setTitle("");
        cancelAction.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        cancelAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "onClick: ");
                if (alertProgress.isNetworkAvailable())
                    new LiveBookingStatusController().cancelBooking(BookingPendingAccept.this, sprefs,
                            alertProgress, true, bid,false);
                else
                    alertProgress.showNetworkAlert();
            }
        });
    }

    private void initializeRxJava() {

        observer = new Observer<MyEventStatus>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable.add(d);
            }

            @Override
            public void onNext(MyEventStatus myEventStatus) {
                Log.d("BookingPen", "onNextPendin: " + myEventStatus.getData().getStatus());
                if(myEventStatus.getData().getStatus()!=5 && myEventStatus.getData().getStatus()!=4 && myEventStatus.getData().getStatus()!=1 && myEventStatus.getData().getStatus()!=15)
                {
                    if (Build.VERSION.SDK_INT >= 23) {

                        Intent intnet1 = new Intent(BookingPendingAccept.this, LiveStatus.class);
                        intnet1.putExtra("BID", myEventStatus.getData().getBookingId());
                        intnet1.putExtra("ImageUrl", myEventStatus.getData().getProProfilePic());
                        intnet1.putExtra("STATUS", myEventStatus.getData().getStatus());
                        /*ActivityOptionsCompat options1 = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(BookingPendingAccept.this,
                                        ivCustomer,
                                        ViewCompat.getTransitionName(ivCustomer));*/
                        startActivity(intnet1);//, options1.toBundle()
                        finish();
                    } else {
                        Intent intnet1 = new Intent(BookingPendingAccept.this, LiveStatus.class);
                        intnet1.putExtra("BID", myEventStatus.getData().getBookingId());
                        intnet1.putExtra("ImageUrl", myEventStatus.getData().getProProfilePic());
                        intnet1.putExtra("STATUS", myEventStatus.getData().getStatus());
                        startActivity(intnet1);
                        finish();
                    }
                }
                if(myEventStatus.getData().getStatus()==5)
                {
                    Toast.makeText(BookingPendingAccept.this,getResources().getString(R.string.bookingExpired),Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else if(myEventStatus.getData().getStatus()==4)
                {
                    Toast.makeText(BookingPendingAccept.this,getResources().getString(R.string.bookingRejected),Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else if(myEventStatus.getData().getStatus()==15)
                {
                    Toast.makeText(BookingPendingAccept.this,getResources().getString(R.string.booking_unassigned),Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }

            @Override
            public void onError(Throwable e)
            {

            }

            @Override
            public void onComplete() {

            }
        };
        MyEventStatusObservable.getInstance().subscribe(observer);
    }

    private void getIntentView() {

        if (getIntent().getExtras() != null) {
            bid = getIntent().getLongExtra("BID", 0);
            //Imageurl = getIntent().getStringExtra("ImageUrl");
            status = getIntent().getIntExtra("STATUS", 0);
            pendingEventPojo = (ArrayList<AllEventsDataPojo>) getIntent().getSerializableExtra("ALLPOJO");
            Log.d("TAG", "getIntentView: " + status + " status " + pendingEventPojo.get(0).getStatus()
                    + " BID " + bid + " BID1 " + pendingEventPojo.get(0).getBookingId());
        }
    }

    private void initializeView() {
        double size[] = Scaler.getScalingFactor(this);
        double width = 80 * size[0];
        double height = 80 * size[1];
        appTypeface = AppTypeface.getInstance(this);
        TextView tvAddress = findViewById(R.id.tvAddress);
        TextView tvDistance = findViewById(R.id.tvDistance);
        ivCard=findViewById(R.id.ivCard);
        //ivCustomer = findViewById(R.id.ivCustomer);
        TextView tvtbBookingId=findViewById(R.id.tvtbBookingId);
        tvtbBookingId.setTypeface(appTypeface.getHind_semiBold());
        tvtbBookingId.setText("bid: "+bid);
        tvVisitFee=findViewById(R.id.tvVisitFee);
        tvVisitFeeAmt=findViewById(R.id.tvVisitFeeAmt);
        rlconfm_travelfee=findViewById(R.id.rlconfm_travelfee);
        rldiscount=findViewById(R.id.rldiscount);
        rlLastdues=findViewById(R.id.rlLastdues);
        tv_travelFeeFee=findViewById(R.id.tv_travelFeeFee);
        tvTravelFeeAmt=findViewById(R.id.tvTravelFeeAmt);
        tvDiscount=findViewById(R.id.tvDiscount);
        tvDiscountAmount=findViewById(R.id.tvDiscountAmount);
        tvLastDues=findViewById(R.id.tvLastDues);
        tvLastDuesAmt=findViewById(R.id.tvLastDuesAmt);
        tvTotal=findViewById(R.id.tvTotal);
        tvTotalAmt=findViewById(R.id.tvTotalAmt);
        tvTotalBillAmount=findViewById(R.id.tvTotalBillAmount);
        tvJobLocation=findViewById(R.id.tvJobLocation);
        tvPaymentBreak=findViewById(R.id.tvPaymentBreak);
        tvCategory=findViewById(R.id.tvCategory);
        tvCategoryName=findViewById(R.id.tvCategoryName);
        tvTotalBillAmount.setTypeface(appTypeface.getHind_semiBold());
        tvJobLocation.setTypeface(appTypeface.getHind_semiBold());
        tvPaymentBreak.setTypeface(appTypeface.getHind_semiBold());
        tvVisitFee.setTypeface(appTypeface.getHind_regular());
        tvVisitFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvLastDues.setTypeface(appTypeface.getHind_regular());
        tvLastDuesAmt.setTypeface(appTypeface.getHind_regular());
        tvCategory.setTypeface(appTypeface.getHind_regular());
        tvCategoryName.setTypeface(appTypeface.getHind_regular());
        tv_travelFeeFee.setTypeface(appTypeface.getHind_regular());
        tvTravelFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvDiscount.setTypeface(appTypeface.getHind_regular());
        tvDiscountAmount.setTypeface(appTypeface.getHind_regular());
        tvTotal.setTypeface(appTypeface.getHind_semiBold());
        tvTotalAmt.setTypeface(appTypeface.getHind_semiBold());
        //TextView tvCustomerName = findViewById(R.id.tvCustomerName);
        //RatingBar ratingStar = findViewById(R.id.ratingStar);
        ProgressBar pBRemainingTime = findViewById(R.id.pBRemainingTime);
        tvRemainingTime = findViewById(R.id.tvRemainingTime);
        TextView tvDateLabel = findViewById(R.id.tvDateLabel);
        TextView tvDate = findViewById(R.id.tvDate);
        payment_layout=findViewById(R.id.payment_layout);
        rlconfm_visitfee=findViewById(R.id.rlconfm_visitfee);
        rlconfm_visitfee.setVisibility(View.GONE);
        //TextView tvTypeOfEventLable = findViewById(R.id.tvTypeOfEventLable);
        //TextView tvTypeOfEvent = findViewById(R.id.tvTypeOfEvent);
        //TextView tvTimeLabel = findViewById(R.id.tvTimeLabel);
        //TextView tvTime = findViewById(R.id.tvTime);
        //TextView tvGigTimeLable = findViewById(R.id.tvGigTimeLable);
        //TextView tvGigTime = findViewById(R.id.tvGigTime);
        //TextView tvPriceLabel = findViewById(R.id.tvPriceLabel);
        TextView tvPrice = findViewById(R.id.tvPrice);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);

        tvAddress.setTypeface(appTypeface.getHind_medium());
        tvDistance.setTypeface(appTypeface.getHind_regular());
        tvDate.setTypeface(appTypeface.getHind_regular());
        tvDistance.setText(pendingEventPojo.get(0).getDistance()+" "+VariableConstant.distanceUnit);
        tvCategoryName.setText(pendingEventPojo.get(0).getCart().getCategoryName());
        tvConfirmCardNumber=findViewById(R.id.tvConfirmCardNumber);
        tvPrice.setTypeface(appTypeface.getHind_regular());
        tvPaymentMethodLabel.setTypeface(appTypeface.getHind_regular());
        tvPaymentMethod.setTypeface(appTypeface.getHind_semiBold());
        app_barPending = findViewById(R.id.app_barPending);
        llLiveFee = findViewById(R.id.llLiveFee);
        tvVisitFee = findViewById(R.id.tvVisitFee);

        tvAddress.setText(pendingEventPojo.get(0).getAddLine1());

        String name = pendingEventPojo.get(0).getFirstName() + " " + pendingEventPojo.get(0).getLastName();

        progressDialog(pBRemainingTime);

        setPaymentMethod();
        Utilities.setAmtOnRecept(pendingEventPojo.get(0).getTotalAmount(),tvPrice,pendingEventPojo.get(0).getCurrencySymbol());
        if(pendingEventPojo.get(0).getAccounting().getVisitFee()>0){
            rlconfm_visitfee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getVisitFee(),tvVisitFeeAmt,pendingEventPojo.get(0).getCurrencySymbol());
        }
        Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getTotal(),tvTotalAmt,pendingEventPojo.get(0).getCurrencySymbol());
        if(pendingEventPojo.get(0).getAccounting().getTravelFee()>0){
            rlconfm_travelfee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getTravelFee(),tvTravelFeeAmt,pendingEventPojo.get(0).getCurrencySymbol());
        }
        if(pendingEventPojo.get(0).getAccounting().getDiscount()>0){
            rldiscount.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getDiscount(),tvDiscountAmount,pendingEventPojo.get(0).getCurrencySymbol());
        }
        if(pendingEventPojo.get(0).getAccounting().getLastDues()>0){
            rlLastdues.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(pendingEventPojo.get(0).getAccounting().getLastDues(),tvLastDuesAmt,pendingEventPojo.get(0).getCurrencySymbol());
        }

        dateMethod(tvDate);
        setPaymentBreakdown();
    }

    private void setPaymentBreakdown() {
        int size=pendingEventPojo.get(0).getCart().getCheckOutItem().size();
        for(int i=0;i<size;i++){
            Item item=pendingEventPojo.get(0).getCart().getCheckOutItem().get(i);
            View infltedView = LayoutInflater.from(BookingPendingAccept.this).inflate(R.layout.singleserviceprice_pending, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit;
            if(item.getServiceName().trim().isEmpty()){
                namePerUnit = "Hourly (x" + item.getQuntity()+")";
            }else{
                namePerUnit = item.getServiceName()+ " (x" + item.getQuntity()+")";
            }
            tvServiceName.setText(namePerUnit);
            Utilities.setAmtOnRecept(item.getAmount(),tvServicePrice,pendingEventPojo.get(0).getCurrencySymbol());
            tvServiceName.setTypeface(appTypeface.getHind_regular());
            tvServicePrice.setTypeface(appTypeface.getHind_regular());
        }
        Utilities.setAmtOnRecept(pendingEventPojo.get(0).getTotalAmount(),tvTotalAmt,pendingEventPojo.get(0).getCurrencySymbol());
    }

    public void setPaymentMethod(){
        tvPaymentMethod.setText(pendingEventPojo.get(0).getPaymentMethod());
        if(pendingEventPojo.get(0).getPaymentMethod().equalsIgnoreCase("cash")) {
            ivCard.setImageBitmap(Utilities.getBitmapFromVectorDrawable(this,R.drawable.ic_002_notes));
            if(pendingEventPojo.get(0).getAccounting().getPaidByWallet()==0){
                tvConfirmCardNumber.setText(pendingEventPojo.get(0).getPaymentMethod());
            }else{
                tvConfirmCardNumber.setText(pendingEventPojo.get(0).getPaymentMethod()+" + "+"wallet");
            }

        }else {
            ivCard.setImageBitmap(Utilities.setCreditCardLogo(pendingEventPojo.get(0).getAccounting().getBrand(),this));
            String cardEndWith = pendingEventPojo.get(0).getPaymentMethod() + " "+getString(R.string.endsWith)+" "+pendingEventPojo.get(0).getAccounting().getLast4();
            if(pendingEventPojo.get(0).getAccounting().getPaidByWallet()==0){
                tvConfirmCardNumber.setText(cardEndWith);
            }else{
                tvConfirmCardNumber.setText(cardEndWith+" + "+"wallet");
            }
        }
    }

    private void appBarChangeListnr(final String name) {
        app_barPending.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            boolean isShow = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0)
                {
                    Log.d("TAG", "onOffsetChanged: "+name);


                   // toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);
                    isShow = true;
                } else if (isShow) {
                    // toolBarTitle.setText("");
                   // ivProDtlsPic.setVisibility(View.VISIBLE);
                  //  toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_white_off);

                    isShow = false;
                }
            }
        });
    }

    private void timeMethod(TextView tvDate, TextView tvTime) {
        try {
            Log.d("TAGTIME", " expireTime " + pendingEventPojo.get(0).getBookingRequestedFor());
            Date date = new Date(pendingEventPojo.get(0).getBookingRequestedFor() * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            // sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDate = sdf.format(date);

            String splitDate[] = formattedDate.split(" ");
            //Removing time as there is no time element in ui
            Log.d("TIME", "timeStamp: " + formattedDate + " TIMER " + (date.getTime()) + " time " + splitDate[1]);
            String timeIS = splitDate[1] + " " + splitDate[2];
            tvTime.setText(timeIS);
            String dat[] = splitDate[0].split("/");
            String month = Utilities.MonthName(Integer.parseInt(dat[0]));
            String day = Utilities.getDayNumberSuffix(Integer.parseInt(dat[1]));
            String year = dat[2];
            String bookinDate = dat[1] + day + " " + month + " " + year;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                tvDate.setText(Html.fromHtml(bookinDate, Html.FROM_HTML_MODE_LEGACY));
            else
                tvDate.setText(Html.fromHtml(bookinDate));
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }
    private void dateMethod(TextView tvDate) {
        try {
            Log.d("TAGTIME", " expireTime " + pendingEventPojo.get(0).getBookingRequestedFor());
            Date date = new Date(pendingEventPojo.get(0).getBookingRequestedFor() * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            // sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDate = sdf.format(date);
            String splitDate[] = formattedDate.split(" ");
            //Removing time as there is no time element in ui
            //Log.d("TIME", "timeStamp: " + formattedDate + " TIMER " + (date.getTime()) + " time " + splitDate[1]);
            //String timeIS = splitDate[1] + " " + splitDate[2];
            //tvTime.setText(timeIS);
            String dat[] = splitDate[0].split("/");
            String month = Utilities.MonthName(Integer.parseInt(dat[0]));
            String day = Utilities.getDayNumberSuffix(Integer.parseInt(dat[1]));
            String year = dat[2];
            String bookinDate = dat[1] + day + " " + month + " " + year;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                tvDate.setText(Html.fromHtml(bookinDate, Html.FROM_HTML_MODE_LEGACY));
            else
                tvDate.setText(Html.fromHtml(bookinDate));
        } catch (Exception e) {
        }
    }

    private void progressDialog(final ProgressBar pBRemainingTime) {
        long remainginTime = Utilities.timeStampS(pendingEventPojo.get(0).getBookingExpireTime()
                , pendingEventPojo.get(0).getBookingRequestedAt());

        long maxtime = Utilities.timeStamp(pendingEventPojo.get(0).getBookingExpireTime(), pendingEventPojo.get(0)
                .getBookingRequestedAt());
        Log.d("Shijen", "progressDialog: "+maxtime);
        pBRemainingTime.setMax((int) maxtime);

        if (remainginTime > 0) {

            timer = new CountDownTimer(remainginTime, 1000) {
                @Override
                public void onTick(long l) {
                    Log.d("TAG", "onTick: " + l);

                    pBRemainingTime.setProgress((int) (l/1000));
                    timeRemain(l);

                }

                @Override
                public void onFinish() {
                    pBRemainingTime.setProgress(0);
                    tvRemainingTime.setText("");
                }
            }.start();
        } else {
            pBRemainingTime.setProgress(0);
            tvRemainingTime.setText("");
        }
    }

    private void timeRemain(long l) {
        tvRemainingTime.setText(formatSeconds(l));
    }

    /*private void gigTime(AllEventsDataPojo.GigTime gigTime, TextView tvGigTime, TextView tvPrice) {
        String price = pendingEventPojo.get(0).getCurrencySymbol() + " " + gigTime.getPrice();
        tvPrice.setText(price);
        long mint = gigTime.getSecond() / 60;
        String gittime;
        if (mint > 59) {
            long hr = mint / 60;
            long min = mint % 60;
            gittime = hr + ":" + min + " Min";
            tvGigTime.setText(gittime);
        } else {
            gittime = mint + " Min";
            tvGigTime.setText(gittime);
        }
    }*/

    private void distanceInMiles(double distance, TextView tvDistance) {

        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        // double kilometerDistance = distance;//*1.6
        @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", distance);
        String kilometr = kilometer + " " + VariableConstant.distanceUnit;
        tvDistance.setText(kilometr);
    }

    @SuppressLint("DefaultLocale")
    private String formatSeconds(long timeInSeconds) {

        long second = (timeInSeconds / 1000) % 60;
        long minute = (timeInSeconds / (1000 * 60)) % 60;

        return twoDigitString(minute) + " : " + twoDigitString(second);
    }

    private String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }
        if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }

    @Override
    protected void onResume() {
        super.onResume();
        VariableConstant.isLiveBokinOpen = true;
        Log.d("BOOKINGPEN SHIJENTEST", "onResume: "+VariableConstant.isLiveBokinOpen);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null)
            timer.cancel();
        Log.d("BOOKINGPEND SHIJENTEST", "onPause: "+VariableConstant.isLiveBokinOpen);
        VariableConstant.isLiveBokinOpen = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.stay_still, R.anim.side_slide_in);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VariableConstant.isLiveBokinOpen = false;
        Log.d("BOOKINGPEN SHIJENTEST", "onDestroy: "+VariableConstant.isLiveBokinOpen);
        MyEventStatusObservable.getInstance().removeObserver(observer);
        disposable.clear();
    }
}
