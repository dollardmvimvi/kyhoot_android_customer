package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>Saving_Address</h>
 * Created by Ali on 27/7/15.
 */
public class Saving_Address implements Serializable
{

    private String aid,address1,address2,zipcode,tag_address,lat,suite_num,lng;
    private String houseno;

    public String getHouseno() {
        return houseno;
    }

    public String getAddressid()
    {
        return aid;
    }

    public String getCity()
    {
        return address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public String getZipcode()
    {
        return zipcode;
    }

    public String getAddresstype()
    {
        return tag_address;
    }

    public String getLati()
    {
        return lat;
    }

    public String getArea()
    {
        return suite_num;
    }

    public String getLongi()
    {
        return lng;
    }

}
