package com.Kyhoot.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.CartModifiedPojo;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${3Embed} on 10/4/18.
 */
public class ConfirmBookingServicesAdapter extends RecyclerView.Adapter<ConfirmBookingServicesAdapter.ConfirmPageServices> {

    private final UtilInterFace.ConfirmBook callback;
    private static final String TAG = "ConfirmBookingServicesA";
    CartModifiedPojo cartModifiedPojo;
    SharedPrefs prefs;
    Item pojo;
    //Filtering the status 0 items using an ArrayList
    List<Item> itemsList;
    private AlertProgress alertProgress;
    private Context context;
    private String dataShowOnAdapter;
    Boolean isClicked = false;

    public ConfirmBookingServicesAdapter(Context mContext, CartModifiedPojo cartServiceList, SharedPrefs prefs, UtilInterFace.ConfirmBook callback, String dataShowOnAdapter) {
        this.cartModifiedPojo = cartServiceList;
        this.itemsList=new ArrayList<>();
        this.prefs=prefs;
        this.callback=callback;
        this.dataShowOnAdapter = dataShowOnAdapter;
        Gson gson=new Gson();
        this.context=mContext;
        alertProgress=new AlertProgress(context);
        gson.toJson(cartModifiedPojo,CartModifiedPojo.class);
    }

    @NonNull
    @Override
    public ConfirmPageServices onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_booking_service_item_layout,parent,false);

        for (int i =0;i<cartModifiedPojo.getData().getItem().size();i++) {
            if ("1".equals(cartModifiedPojo.getData().getItem().get(i).getStatus())) {
                itemsList.add(cartModifiedPojo.getData().getItem().get(i));
            }
        }
        Log.e(TAG, "onCreateViewHolder:  CART ITEMS"+itemsList.size());

        return new ConfirmPageServices(view) ;
    }

    public void setCartModifiedPojo(CartModifiedPojo cartModifiedPojo) {
        this.cartModifiedPojo = cartModifiedPojo;
    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmPageServices holder, int position) {
        if(cartModifiedPojo!=null && cartModifiedPojo.getData()!=null){
            //if(position!=cartModifiedPojo.getData().getItem().size()){
            pojo= cartModifiedPojo.getData().getItem().get(position);
            if ("0".equals(pojo.getStatus())) {
                Log.e(TAG, "onBindViewHolder: CART REMOVED ITEM "
                        + position + "  Name  " + pojo.getServiceName() + " status " + pojo.getStatus());
                holder.ll_parent_confirmBook.setVisibility(View.GONE);
            } else {
                Log.e(TAG, "onBindViewHolder: CART VISIBLE ITEM  "
                        + position + "  Name  " + pojo.getServiceName() + " status " + pojo.getStatus());
                holder.ll_parent_confirmBook.setVisibility(View.VISIBLE);
            }


            if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE) )
            {
                double totalPrice = 0.0;
                double service_price_surge=0.0;
                try
                {
                    service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*Double.parseDouble(pojo.getUnitPrice())));
                    Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                String finalServicePrice = "Hourly @ "+prefs.getCurrencySymbol()+""+String.format("%.2f", service_price_surge)+"/hr";
                 totalPrice = service_price_surge*pojo.getQuntity();
             //   double totalPrice = service_price_surge*VariableConstant.MINIMUM_HOUR;


                /*if(cartModifiedPojo.getData().getServiceType()==2)
                {

                    if(isClicked==true)
                    {
                        totalPrice = service_price_surge*pojo.getQuntity();
                    }else{
                        totalPrice = service_price_surge*VariableConstant.MINIMUM_HOUR;
                    }

                  //   totalPrice = service_price_surge*VariableConstant.MINIMUM_HOUR;
                }else{
                     totalPrice = service_price_surge*pojo.getQuntity();
                }*/

                holder.tvServicePrice.setText(String.format("%s %s", prefs.getCurrencySymbol(),String.format("%.2f", totalPrice)));

                if(cartModifiedPojo.getData().getServiceType()==1)
                {
                    holder.tvServiceName.setText(pojo.getServiceName());
                }else{
                    holder.tvServiceName.setText(finalServicePrice);
                }

            } else {
                holder.tvServicePrice.setText(String.format("%s %s", prefs.getCurrencySymbol(), String.format("%.2f", pojo.getAmount())));


                if(cartModifiedPojo.getData().getServiceType()==1)
                {
                    holder.tvServiceName.setText(pojo.getServiceName());
                }else{
                    holder.tvServiceName.setText("Hourly"+" @ "+prefs.getCurrencySymbol()+String.format("%.2f", prefs.getCategoryHourlyPrice()));
                }
            }


            /*if(cartModifiedPojo.getData().getServiceType()==2)
            {
                if(isClicked==true)
                {
                    holder.tv_quantity.setText(String.format("%d hr", pojo.getQuntity()));
                }else{
                    holder.tv_quantity.setText(String.format("%d hr", VariableConstant.MINIMUM_HOUR));
                }


               // holder.tv_quantity.setText(String.format("%d hr", pojo.getQuntity()));
            }else{
                holder.tv_quantity.setText(pojo.getQuntity()+"");
            }*/

            holder.tv_quantity.setText(pojo.getQuntity()+"");


            if(pojo.getQuantityAction()==null || !pojo.getQuantityAction().equals("0")){ //QuantityAction is null in hourly
                if(pojo.getQuntity()==0){
                    holder.ll_add_service.setVisibility(View.GONE);
                    holder.addbtn.setVisibility(View.VISIBLE);
                    holder.tv_removeService.setVisibility(View.GONE);
                }else {
                    holder.ll_add_service.setVisibility(View.VISIBLE);
                    holder.addbtn.setVisibility(View.GONE);
                    holder.tv_removeService.setVisibility(View.GONE);
                }
            }else{
                if(pojo.getQuntity()==0){
                    holder.ll_add_service.setVisibility(View.GONE);
                    holder.addbtn.setVisibility(View.VISIBLE);
                    holder.tv_removeService.setVisibility(View.GONE);
                }else if(pojo.getQuntity()==1){
                    holder.ll_add_service.setVisibility(View.GONE);
                    holder.addbtn.setVisibility(View.GONE);
                    holder.tv_removeService.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        if((cartModifiedPojo==null || cartModifiedPojo.getData()==null) || cartModifiedPojo.getData().getItem().size()==0){
            return 0;
        }
        return cartModifiedPojo.getData().getItem().size();
    }


    class ConfirmPageServices extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvServiceName,addbtn,tv_removeService,tv_quantity,tvServicePrice;
        LinearLayout ll_add_service,ll_parent_confirmBook;
        ImageView iv_add,iv_remove;
        FrameLayout fl_service_update;
        ConfirmPageServices(View itemView) {
            super(itemView);
            tvServiceName=itemView.findViewById(R.id.tvServiceName);
            addbtn=itemView.findViewById(R.id.addbtn);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            tv_removeService=itemView.findViewById(R.id.tv_removeService);
            ll_add_service=itemView.findViewById(R.id.ll_add_service);
            tvServicePrice=itemView.findViewById(R.id.tvServicePrice);
            ll_parent_confirmBook=itemView.findViewById(R.id.ll_parent_confirmBook);
            fl_service_update=itemView.findViewById(R.id.fl_service_update);
            iv_add=itemView.findViewById(R.id.iv_add);
            iv_remove=itemView.findViewById(R.id.iv_remove);
            tv_removeService.setOnClickListener(this);
            addbtn.setOnClickListener(this);
            iv_remove.setOnClickListener(this);
            iv_add.setOnClickListener(this);

            tvServiceName.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_semiBold());
            tvServicePrice.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            pojo = cartModifiedPojo.getData().getItem().get(position);
            int quantity=pojo.getQuntity();
            if(Utilities.isNetworkAvailable(addbtn.getContext())) {
                switch (view.getId())
                {
                    case R.id.iv_add:
                        if(cartModifiedPojo.getData().getServiceType()==2)
                        {
                            isClicked = true;
                            callback.onCartModified(pojo.getServiceId(), 1);
                        }else{
                            if (quantity < pojo.getMaxquantity()) {
                                callback.onCartModified(pojo.getServiceId(), 1);
                            } else {
                                Toast.makeText(view.getContext(), R.string.max_limit_service, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                    case R.id.iv_remove:
                        if(quantity>0)
                        {
                            int hourSelected=0;
                            if (tv_quantity.getText()!=null) {
                                try {
                                    Log.e(TAG, "onClick: hourSelected " + tv_quantity.getText().toString().split(" ")[0]);
                                    hourSelected = Integer.parseInt(tv_quantity.getText().toString().split(" ")[0]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if ("1".equals(pojo.getServiceId())) {
                                if (VariableConstant.MINIMUM_HOUR > 0 && hourSelected != 0 && hourSelected <= VariableConstant.MINIMUM_HOUR) {
                                    alertProgress.alertinfo("Cannot select lesser than the minimum hour " + VariableConstant.MINIMUM_HOUR);
                                    tv_quantity.setText(VariableConstant.MINIMUM_HOUR + " hr");
                                } else {
                                    callback.onCartModified("1", 2);//2 for removing
                                }
                            } else {
                                callback.onCartModified(pojo.getServiceId(), 2);
                            }
                        }
                        break;
                    case R.id.addbtn:
                        int hourSelectedNow=0;
                        if (tv_quantity.getText()!=null) {
                            try {
                                Log.e(TAG, "onClick: hourSelected " + tv_quantity.getText().toString().split(" ")[0]);
                                hourSelectedNow = Integer.parseInt(tv_quantity.getText().toString().split(" ")[0]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (hourSelectedNow<= VariableConstant.MINIMUM_HOUR)
                            tv_quantity.setText(VariableConstant.MINIMUM_HOUR + " hr");
                        else {
                            callback.onCartModified(pojo.getServiceId(),1);
                        }
                        break;
                    case R.id.tv_removeService:
                        if(pojo.getQuntity()>0){
                            callback.onCartModified( pojo.getServiceId(),2);
                        }
                        break;
                }
                notifyItemChanged(position);
            }else{
                Toast.makeText(view.getContext(), "Please check internet connectivity!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

