package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 16/3/18.
 */

public class GetCategoryData {

    ZoneDetailsPojo cityData;
    ArrayList<CategoryData> catArr;
    long serverGmtTime;

    public ZoneDetailsPojo getCityData() {
        return cityData;
    }

    public ArrayList<CategoryData> getCatArr() {
        return catArr;
    }

    public long getServerGmtTime() {
        return serverGmtTime;
    }
}
