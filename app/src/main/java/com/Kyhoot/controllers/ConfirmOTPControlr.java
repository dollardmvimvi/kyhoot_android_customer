package com.Kyhoot.controllers;

import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.widget.TextView;

import com.Kyhoot.BuildConfig;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.ProfileDataResponce;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.main.ConfirmOTP;
import com.Kyhoot.models.ConfirmNumberModel;
import com.Kyhoot.pojoResponce.SignUpData;
import com.Kyhoot.pojoResponce.SignUpInPojo;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>ConfirmOTPControlr</h>
 * Created by Ali on 9/13/2017.
 */

public class ConfirmOTPControlr
{
    private Context mcontext;
    private ConfirmNumberModel confirmNumberModel;
    private ConfirmOTP confirmOTP;
    public ConfirmOTPControlr(Context mcontext)
    {
        this.mcontext = mcontext;
        confirmNumberModel = new ConfirmNumberModel();
    }

    public void verifyOtp(VerifyOtpIntrface verifyOtpIntrface, String otp, String sid, String signupOrForgot)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("code",otp);
            jsonObject.put("userId", sid);
            if (signupOrForgot.equals("SIGNUP"))
                confirmNumberModel.verifyOtp(jsonObject, verifyOtpIntrface, signupOrForgot, new SharedPrefs(mcontext));
            else if (signupOrForgot.equals("PROFILE")) {
                jsonObject.put("code", otp);
                jsonObject.put("userId", sid);
                jsonObject.put("trigger", 3);
                jsonObject.put("userType", 1);
                confirmNumberModel.verifyiOtpForPass(jsonObject, verifyOtpIntrface);
            } else {
                if (signupOrForgot.equals("CHANGENUMBER"))
                    jsonObject.put("trigger", 1);
                else
                    jsonObject.put("trigger", 2);

                jsonObject.put("userType", 1);
                confirmNumberModel.verifyiOtpForPass(jsonObject, verifyOtpIntrface);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void signupservice(SignUpInPojo signupPojo, SharedPrefs spref, VerifyOtpIntrface verifyOtpIntrface)
    {
        JSONObject jsonObject = new JSONObject();
        SignUpData data = signupPojo.getData();
        try {
            jsonObject.put("fName",data.getFirstName());
            jsonObject.put("lName",data.getLastName());
            jsonObject.put("email",data.getEmail());
            jsonObject.put("password",data.getPassword());
            jsonObject.put("countryCode",data.getCountryCode());
            jsonObject.put("mobile",data.getPhone());
            jsonObject.put("dateOfBirth",data.getDatOB());
            jsonObject.put("profilePic",data.getProfilePic());
            jsonObject.put("loginType",data.getLoginType());
            jsonObject.put("facebookId",data.getFbId());
            jsonObject.put("latitude",spref.getSplashLatitude());
            jsonObject.put("longitude",spref.getSplashLongitude());
            jsonObject.put("termsAndCond",1);
            jsonObject.put("devType",2);
            jsonObject.put("deviceId",spref.getDeviceId());
            jsonObject.put("pushToken",spref.getRegistrationId());
            jsonObject.put("appVersion", BuildConfig.VERSION_NAME);
            jsonObject.put("deviceOsVersion", Build.VERSION.RELEASE);
            jsonObject.put("devMake", Build.BRAND);
            jsonObject.put("devModel", Build.MODEL);
            jsonObject.put("deviceTime", Utilities.dateintwtfour());
            // confirmNumberModel.signupService(jsonObject,verifyOtpIntrface,spref);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void resendOtp(VerifyOtpIntrface verifyoTp, String sid, String signupOrForgot) {
        confirmNumberModel.resendOtp(verifyoTp, sid, signupOrForgot);
    }

    public void setTimerOntheTime(final TextView tvTimerOtp, final ProfileDataResponce ontimerResponce) {

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvTimerOtp.setText("00:" + millisUntilFinished / 1000 + "");
            }

            public void onFinish() {
                tvTimerOtp.setText(mcontext.getString(R.string.resendcode));

                ontimerResponce.sessionExpired("");

            }
        }.start();
    }
}
