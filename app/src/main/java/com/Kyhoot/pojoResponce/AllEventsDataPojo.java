package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>AllEventsDataPojo</h>
 * this bean object is to get the booking details of the all on going, pending and the past job
 * Created by 3Embed on 11/1/2017.
 */

public class AllEventsDataPojo implements Serializable {
    /*"bookingId":1509466652440,
"bookingRequestedFor":1509466652,
"bookingExpireTime":1509467252,
"distance":0.004717,
"status":5,
"statusMsg":"Expired",
"firstName":"Raghavendra",
"lastName":"V",
"profilePic":"https://s3.amazonaws.com/livemapplication/Provider/ProfilePics/1509351606290_0_01.png",
"proLocation":{
"longitude":77.58952331542969,
"latitude":13.02863883972168
},
"addLine1":"Creative Villa Apartment, 44 RBI Colony, Vishveshvaraiah Nagar, Ganga Nagar, Bengaluru, Karnataka 560024, India, null",
"addLine2":"",
"city":"",
"state":"",
"country":"",
"currencySymbol": "$"
"placeId":"",
"pincode":"",
"latitude":13.028641492265745,
"longitude":77.58947990834713,
"averageRating":0,
"typeofEvent":{
"_id":"59cc98e4b05549255330ae12",
"name":"Birthday Party",
"selectImage":"https://s3.amazonaws.com/iserve/8988260408472.png",
"unselectImage":"https://s3.amazonaws.com/iserve/4937064970285.png",
"status":1
},
"gigTime":{
"id":"59d6291cb0554924860bb333",
"name":"15",
"unit":"MIN",
"price":15,
"second":900
},
"paymentMethod":"cash"*/


    private String lastName;
    private float averageRating;

    private String addLine1, statusMsg,currencySymbol,cancellationReason;

    private String profilePic;

    private double distance;

    private long bookingExpireTime, serverTime, bookingId, bookingRequestedFor,bookingRequestedAt;
    private int status;

    public int getBookingModel() {
        return bookingModel;
    }

    private int bookingModel;

    private String firstName;
    private String paymentMethod;
    private double totalAmount;
    private CartData cart;
    private ArrayList<AdditionalService> additionalService;
    public CartData getCart() {
        return cart;
    }


    //private TypeofEvent typeofEvent;
    private ReviewByCustomer reviewByCustomer ;
    private Accounting accounting;


    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }
    public String getCancellationReason() {
        return cancellationReason;
    }

    public Accounting getAccounting() {
        return accounting;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public ReviewByCustomer getReviewByProvider() {
        return reviewByCustomer;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /*public TypeofEvent getTypeofEvent() {
        return typeofEvent;
    }*/

    /*public GigTime getGigTime() {
        return gigTime;
    }*/

    public ArrayList<AdditionalService> getAdditionalService() {
        return additionalService;
    }

    public long getBookingRequestedAt() {
        return bookingRequestedAt;
    }

    public long getBookingRequestedFor() {
        return bookingRequestedFor;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getBookingId() {
        return bookingId;
    }


    public float getAverageRating() {
        return averageRating;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAddLine1() {
        return addLine1;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }


    public long getBookingExpireTime() {
        return bookingExpireTime;
    }

    public long getServerTime() {
        return serverTime;
    }

    public double getDistance() {
        return distance;
    }


    public String getProfilePic() {
        return profilePic;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    /*public class GigTime implements Serializable {
     *//*"id":"59d6291cb0554924860bb333",
"name":"15",
"unit":"MIN",
"price":15,
"second":900*//*
        private String id, name, unit;
        private double price;
        private long second;


        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getUnit() {
            return unit;
        }

        public double getPrice() {
            return price;
        }

        public long getSecond() {
            return second;
        }
    }*/

    public class ReviewByCustomer implements Serializable
    {
        /*   "rating" : 4,
                "review" : "xfhhvyff",
                "userId" : ObjectId("5a50e8b3b121331d381cd93d"),
       "reviewAt" : 1515409348*/

        private float rating;
        public float getRating() {
            return rating;
        }
    }

    public class Accounting implements Serializable
    {
        /*"amount":110,
"cancellationFee":70,
"discount":0,
"total":110,
"appEarning":0,
"providerEarning":0,
"pgCommissionApp":0,
"pgCommissionProvider":0,
"totalPgCommission":0,
"appEarningPgComm":0,
"paymentMethod":1,
"chargeId":0,
"last4":"",
"appCommission":20*/

        private double amount,cancellationFee,discount,total,travelFee,visitFee,lastDues,captureAmount,remainingAmount
                ,totalExpectJobTimeMinutes,totalActualJobTimeMinutes,totalActualHourFee;
        private String last4,brand,totalDistanceTravelText;
        private int paidByWallet,paymentMethod,serviceType;
        public String getBrand() {
            return brand;
        }

        public double getAmount() {
            return amount;
        }

        public double getCancellationFee() {
            return cancellationFee;
        }

        public double getVisitFee() {
            return visitFee;
        }

        public double getLastDues() {
            return lastDues;
        }

        public double getDiscount() {
            return discount;
        }

        public double getTotal() {
            return total;
        }

        public double getTravelFee() {
            return travelFee;
        }

        public String getLast4() {
            return last4;
        }

        public double getCaptureAmount() {
            return captureAmount;
        }

        public double getRemainingAmount() {
            return remainingAmount;
        }

        public int getPaidByWallet() {
            return paidByWallet;
        }

        public int getPaymentMethod() {
            return paymentMethod;
        }

        public String getTotalDistanceTravelText() {
            return totalDistanceTravelText;
        }

        public int getServiceType() {
            return serviceType;
        }

        public double getTotalExpectJobTimeMinutes() {
            return totalExpectJobTimeMinutes;
        }

        public double getTotalActualJobTimeMinutes() {
            return totalActualJobTimeMinutes;
        }

        public double getTotalActualHourFee() {
            return totalActualHourFee;
        }
    }
}
