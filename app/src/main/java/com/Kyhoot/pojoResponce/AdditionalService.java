package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by ${3Embed} on 16/3/18.
 */

public class AdditionalService implements Serializable
{
    private String price;

    private String serviceName;

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getServiceName ()
    {
        return serviceName;
    }

    public void setServiceName (String serviceName)
    {
        this.serviceName = serviceName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [price = "+price+", serviceName = "+serviceName+"]";
    }
}

