package com.Kyhoot.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.Kyhoot.R;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 21/2/18.
 */

class HomeListImageAdapter extends RecyclerView.Adapter {
    private final View.OnClickListener onClickCallback;
    ArrayList<String> images;
    Context mContext;
    public HomeListImageAdapter(ArrayList<String> images,Context context,View.OnClickListener onClickCallback) {
        this.images=images;
        this.mContext=context;
        this.onClickCallback=onClickCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_image_layout,null,false);
        return new ImageViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ImageView view=((ImageViewHolder)holder).ivJobImages;
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        Glide.with(mContext).load(images.get(position)).apply(options).into(view);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }
    class ImageViewHolder extends RecyclerView.ViewHolder{
        public ImageView ivJobImages;
        public ImageViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(onClickCallback);
            ivJobImages=itemView.findViewById(R.id.ivJobImages);
        }
    }

}

