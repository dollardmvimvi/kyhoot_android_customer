package com.Kyhoot.interfaceMgr;

/**
 * <h>NetworkCheck</h>
 * Created by Ali on 9/19/2017.
 */

public interface NetworkCheck {
    void onNetworkAvailble();

    void onNetworkNotAvailble();
}
