package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 6/2/16.
 */
public class ValidatorPojo implements Serializable {
    private String message;  // = "";
    private String addressid;   // = "";
    private SignUpDataSid data; // for user Id  SignUp
    private ArrayList<Saving_Address> addlist;

    public SignUpDataSid getData() {
        return data;
    }

    public void setData(SignUpDataSid data) {
        this.data = data;
    }

    public String getErrMsg() {
        return message;
    }

    public void setErrMsg(String errMsg) {
        this.message = errMsg;
    }

    public String getAddressid() {
        return addressid;
    }

    public void setAddressid(String addressid) {
        this.addressid = addressid;
    }

    public ArrayList<Saving_Address> getAddlist() {
        return addlist;
    }

    public void setAddlist(ArrayList<Saving_Address> addlist) {
        this.addlist = addlist;
    }

    @Override
    public String toString() {
        return "ValidatorPojo{" +
                ", errMsg='" + message + '\'' +
                ", addressid='" + addressid + '\'' +
                ", data=" + data +
                ", addlist=" + addlist +
                '}';
    }

    public class SignUpDataSid {
        private String sid;// for signup Sid Unique Id
        private float averageRating; // for Review Fragment
        private int reviewCount; // for Review Fragment

        private ArrayList<RateReviews> reviews; //ArrayList For Reviews

        public String getSid() {
            return sid;
        }

        public ArrayList<RateReviews> getReviews() {
            return reviews;
        }

        public float getAverageRating() {
            return averageRating;
        }

        public int getReviewCount() {
            return reviewCount;
        }
    }
}
