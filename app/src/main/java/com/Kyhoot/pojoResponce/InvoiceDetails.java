package com.Kyhoot.pojoResponce;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>InvoiceDetails</h>
 * Created by Ali on 11/25/2017.
 */

public class InvoiceDetails implements Serializable {
    /*"message":"sucess",
"data":{}*/
    private String message;
    private InvoiceData data;

    public String getMessage() {
        return message;
    }

    public InvoiceData getData() {
        return data;
    }





    public class InvoiceData {
        /*"bookingId":1511588491962,
"bookingRequestedFor":1511588492,
"status":10,
"statusMsg":"Raise invoice",
"addLine1":"",
"providerData":{},
"accounting":{},
"service":{},
"signURL":""*/

        private long bookingId;
        private long bookingRequestedFor;
        private String signURL,addLine1;
        private ProviderInvoiceDetails providerData;
        private AllEventsDataPojo.Accounting accounting;
        private CartData cartData;
        private int status;
        private boolean favouriteProvider;
        private float addFavoriteProviderRating,removeFavoriteProviderRating;
        private ArrayList<CustomerRating>customerRating;
        private String categoryId;

        public boolean isFavouriteProvider() {
            return favouriteProvider;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public float getAddFavoriteProviderRating() {
            return addFavoriteProviderRating;
        }

        public float getRemoveFavoriteProviderRating() {
            return removeFavoriteProviderRating;
        }

        public ArrayList<CustomerRating> getCustomerRating() {
            return customerRating;
        }

        public String getAddLine1() {
            return addLine1;
        }

        public long getBookingId() {
            return bookingId;
        }

        public long getBookingRequestedFor() {
            return bookingRequestedFor;
        }

        public String getSignURL() {
            return signURL;
        }

        public ProviderInvoiceDetails getProviderData() {
            return providerData;
        }

        public AllEventsDataPojo.Accounting getAccounting() {
            return accounting;
        }

        public CartData getCart() {
            return cartData;
        }
        public ArrayList<AdditionalService> additionalService;

        public ArrayList<AdditionalService> getAdditionalService() {
            return additionalService;
        }

        public int getStatus() {
            return status;
        }

        public class ProviderInvoiceDetails {
            /*"firstName":"Piyush",
"lastName":"kumar",
"profilePic":"https://s3.amazonaws.com/livemapplication/Provider/ProfilePics/Profile1511587166634.png",
"phone":"+918505997523"*/

            private String firstName, lastName, profilePic, phone,providerId;

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public String getPhone() {
                return phone;
            }

            public String getProviderId() {
                return providerId;
            }


        }


    }

    public class CustomerRating implements Serializable
    {
        /*"_id":"5b0952b561596a47cc4f3e86",
"name":""*/
        String _id,name;
        private float rating;

        public float getRatings() {
            return rating;
        }

        public void setRatings(float ratings) {
            this.rating = ratings;
        }

        public String get_id() {
            return _id;
        }

        public String getName() {
            return name;
        }
    }
}