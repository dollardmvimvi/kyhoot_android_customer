package com.Kyhoot.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.CardInfoPojo;
import com.Kyhoot.utilities.AppTypeface;

import java.util.List;

/**
 * This is an inner class of adapter type, to integrate the list on screen.
 * * Created by ${Ali} on 8/19/2017.
 */

public class CustomListViewAdapter extends ArrayAdapter<CardInfoPojo> {
    Context context;
    public CustomListViewAdapter(Context context, int resourceId, List<CardInfoPojo> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder = null;
        final CardInfoPojo rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.card_list_row, null);
            holder = new ViewHolder();
            holder.card_numb = convertView.findViewById(R.id.card_numb_row_change);
            holder.card_image = convertView.findViewById(R.id.card_img_row_change);
            holder.tvCardUsedIn = convertView.findViewById(R.id.tvCardUsedIn);
            holder.iv_tick = convertView.findViewById(R.id.iv_tick);
            holder.change_card_relative = convertView.findViewById(R.id.change_card_relative);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        assert rowItem != null;
        holder.card_image.setImageBitmap(rowItem.getCardImage());
        holder.card_numb.setText(rowItem.getLast4());
        if (rowItem.isDefault()) {
            holder.iv_tick.setVisibility(View.VISIBLE);
            holder.tvCardUsedIn.setText(context.getString(R.string.cardInUse));
        } else {
            holder.iv_tick.setVisibility(View.GONE);
            holder.tvCardUsedIn.setText("");
        }
        holder.card_numb.setTypeface(holder.appTypeface.getHind_regular());
        holder.tvCardUsedIn.setTypeface(holder.appTypeface.getHind_regular());
        return convertView;
    }

    private class ViewHolder {
        ImageView card_image, iv_tick;
        TextView card_numb,tvCardUsedIn;
        RelativeLayout change_card_relative;
        AppTypeface appTypeface = AppTypeface.getInstance(context);
    }
}
