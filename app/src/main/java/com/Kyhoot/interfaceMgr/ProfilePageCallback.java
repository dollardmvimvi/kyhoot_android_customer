package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 14/2/18.
 */

public interface ProfilePageCallback {
    void onProfileUpdate();
    void onProfileUpdateFailed();
}
