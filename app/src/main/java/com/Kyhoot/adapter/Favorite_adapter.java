package com.Kyhoot.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.Kyhoot.Dialog.BottomsheetFragmentFavProvider;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.OnFavoriteSelected;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 15/6/18.
 */
public class Favorite_adapter extends RecyclerView.Adapter<Favorite_adapter.Favorite_VH> {
    private static final String TAG = "Favorite_adapter";
    private final OnFavoriteSelected callBackFavoriteSelected;
    ArrayList<MqttProviderDetails> favProviderList;
    private View tempView;

    public Favorite_adapter(ArrayList<MqttProviderDetails> favProviderList, OnFavoriteSelected callBackFavoriteSelected) {
        this.favProviderList = favProviderList;
        this.callBackFavoriteSelected=callBackFavoriteSelected;
    }

    @NonNull

    @Override
    public Favorite_VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_provider_item,null,false);
        return new Favorite_VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Favorite_VH viewHolder, int position) {
        if(position==favProviderList.size()){
            viewHolder.tv_favProvName.setText(R.string.any_other_taskers);
        }else{
            if(!favProviderList.get(position).getImage().isEmpty()){
                RequestOptions options = new RequestOptions();
                options.circleCrop();
                Glide.with(viewHolder.iv_favoriteProvider.getContext()).load(favProviderList.get(position).getImage()).apply(options).into(viewHolder.iv_favoriteProvider);
            }
            viewHolder.tv_favProvName.setText(String.format("%s %s", favProviderList.get(position).getFirstName(), favProviderList.get(position).getLastName()));
        }

    }

    @Override
    public int getItemCount() {
        return favProviderList.size()+1;
    }
    class Favorite_VH extends ViewHolder implements View.OnClickListener {
        TextView tv_favProvName;
        ImageView iv_favoriteProvider;
        public Favorite_VH(View itemView) {
            super(itemView);
            tv_favProvName = itemView.findViewById(R.id.tv_favProvName);
            iv_favoriteProvider = itemView.findViewById(R.id.iv_favoriteProvider);
            tv_favProvName.setTypeface(AppTypeface.getInstance(tv_favProvName.getContext()).getHind_regular());
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(getAdapterPosition()!=-1){
                if(tempView != null && tempView.isSelected()) {
                    tempView.setSelected(false);
                }
                view.setSelected(true);
                tempView = view;
                Bundle bundle=new Bundle();
                if(getAdapterPosition()==favProviderList.size()){
                    callBackFavoriteSelected.onOthersSelected();
                }else {
                    MqttProviderDetails tempMqttProvider=favProviderList.get(getAdapterPosition());
                    bundle.putString(BottomsheetFragmentFavProvider.PROVIDERID,tempMqttProvider.getId());
                    bundle.putString(BottomsheetFragmentFavProvider.IMAGEURL,tempMqttProvider.getImage());
                    callBackFavoriteSelected.onFavoriteSelected(bundle);
                }
            }
        }
    }
}
