package com.Kyhoot.main.wallet;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

public class WalletActivityPresenter implements WalletActivityContract.WalletPresenter
{
    private final String TAG = "WalletDetailsProvider";

    WalletActivityContract.WalletView walletView;
    SharedPrefs preferenceHelperDataSource;
    Context mContext;

    WalletActivityPresenter(WalletActivityContract.WalletView view,Context context)
    {
        walletView=view;
        this.mContext=context;
        preferenceHelperDataSource=new SharedPrefs(context);

    }

    @Override
    public String getCurrencySymbol()
    {
        return preferenceHelperDataSource.getCurrencySymbol();
    }


    public void getWalletLimits() {

        if (Utilities.isNetworkAvailable(mContext))
        {
            Log.d(TAG, "wallet: "+VariableConstant.SERVICE_URL+"wallet"+preferenceHelperDataSource.getSession()+" "+ VariableConstant.SelLang );
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "wallet", OkHttpConnection.Request_type.GET, new JSONObject(), preferenceHelperDataSource.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String value) {
                    Log.d(TAG , " getWalletLimits onNext: " + headerResponse);
                    walletView.hideProgressDialog();
                    switch (headerResponse) {
                        case "200":
                            try {
                                String responseString = value;
                                Log.d(TAG , " getWalletLimits onNext: "+responseString);
                                JSONObject profileObject = new JSONObject(responseString);
                                JSONObject dataObject = profileObject.getJSONObject("data");
                                String balance = dataObject.getString("walletAmount");
                                double softLimit = dataObject.getDouble("softLimit");
                                double hardLimit = dataObject.getDouble("hardLimit");
                                String currencySymbol=preferenceHelperDataSource.getCurrencySymbol();
                                boolean reachedHardLimit=dataObject.getBoolean("reachedHardLimit");
                                boolean reachedSoftLimit=dataObject.getBoolean("reachedSoftLimit");
                                walletView.setBalanceValues(currencySymbol,balance,hardLimit,softLimit);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        case "410":
                            walletView.hideProgressDialog();
                            break;

                        default:
                            //walletView.walletDetailsApiErrorViewNotifier(DataParser.fetchErrorMessage(value));
                            walletView.hideProgressDialog();
                            break;
                    }
                }

                @Override
                public void onError(String headerError, String error) {
                    //pDialog.dismiss();
                    /*if (headerError.equals("502"))
                        Toast.makeText(mContext, mContext.getString(R.string.serverProblm), Toast.LENGTH_LONG).show();
                    else {
                        try{
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    prodataResponce.sessionExpired(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccesToken(errorHandel.getData());
                                    break;
                                default:
                                    Toast.makeText(mcontext, errorHandel.getMessage(), Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    Log.d("TAG", "HomeFrag JSON DATA Error" + error + " onAMp " + headerError);*/
                    walletView.hideProgressDialog();
                    Log.d(TAG, "getWalletLimits error: "+headerError+":" + error);
                }

            });

        }
        else{
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void rechargeWallet(String amount, String cardId) {
        if(Utilities.isNetworkAvailable(mContext)){
            JSONObject jsonObject=new JSONObject();
            try{
                jsonObject.put("amount",amount);
                jsonObject.put("cardId",cardId);
            }catch (Exception e){
                e.printStackTrace();
            }

            Log.d(TAG, "rechargeWallet: "+VariableConstant.SERVICE_URL+"wallet/recharge"+preferenceHelperDataSource.getSession()+" "+ VariableConstant.SelLang +" jsonObj:"+jsonObject);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "wallet/recharge", OkHttpConnection.Request_type.POST, jsonObject, preferenceHelperDataSource.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String value) {
                    Log.d(TAG , " RechargeWallet onNext: " + headerResponse);
                    walletView.hideProgressDialog();
//                            Utility.printLog(TAG + " RechargeWallet onNext: " + DataParser.fetchErrorMessage(value));
                    switch (headerResponse) {
                        case "200":
                            try {
                                String responseString = value;
                                Log.d(TAG , " RechargeWallet onNext: " + responseString);
                                JSONObject profileObject = new JSONObject(responseString);
                                JSONObject dataObject = profileObject.getJSONObject("data");
                                String balance=dataObject.getString("walletAmount");
                                double softLimit=dataObject.getDouble("softLimit");
                                double hardLimit=dataObject.getDouble("hardLimit");
                                String currencySymbol=dataObject.getString("currencySymbol");
                                VariableConstant.walletBalance=balance;
                                walletView.setBalanceValues(currencySymbol,balance, hardLimit, softLimit);

                                AlertDialog.Builder alertDialogb=new AlertDialog.Builder(mContext);
                                alertDialogb.setMessage(profileObject.getString("message"));
                                alertDialogb.setTitle("Wallet Recharged!");
                                alertDialogb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        default:
                            //walletView.walletDetailsApiErrorViewNotifier(DataParser.fetchErrorMessage(value));
                            break;
                    }

                }

                @Override
                public void onError(String headerError, String error) {
                    walletView.hideProgressDialog();
                }
            });
        }
    }

    @Override
    public void getLastCardNo()
    {
        if(preferenceHelperDataSource!=null && !preferenceHelperDataSource.getDefaultCardNum().isEmpty()) {
            Log.d(TAG,"MyCardNumber"+preferenceHelperDataSource.getDefaultCardId()
                    +" brand:"+preferenceHelperDataSource.getDefaultCardBrand()
                    +" cardNum: "+preferenceHelperDataSource.getDefaultCardNum());
            walletView.setCard(preferenceHelperDataSource.getDefaultCardNum(),
                    preferenceHelperDataSource.getDefaultCardBrand(),preferenceHelperDataSource.getDefaultCardId());
        }
        else
            walletView.setNoCard();
    }
}
