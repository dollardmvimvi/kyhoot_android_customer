package com.Kyhoot.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.Kyhoot.R;
import com.Kyhoot.controllers.LoginController;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.LoginResponceIntr;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.VariableConstant;

/**
 * <h>ActivityLogin</h>
 * <p>
 * A login screen that offers login via email/password.
 * </p>
 */

public class ActivityLogin extends AppCompatActivity implements OnClickListener, LoginResponceIntr, CheckNetworkAvailablity, LoginResponceIntr.CheckValidty {

    boolean cancel = false;
    View focusView = null;
    private AutoCompleteTextView mEmailView;
    private EditText etPasswordView;
    private TextInputLayout tilEmail, tilPassword;
    private ScrollView loginscrollview;
    private AlertProgress progress;
    private ProgressBar progressDialog;
    private CallbackManager callbackManager;
    private LoginController loginController;
    private AppTypeface appTypeface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_login);
        initializeObj();
        initialize();
        initializeToolBar();
        // To login on keyboard done action
        etPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.tv_login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

    }

    private void initializeObj() {
        appTypeface = AppTypeface.getInstance(this);
    }

    /*
    initialize toolBar
     */
    private void initializeToolBar()
    {
        Toolbar login_tool = findViewById(R.id.login_tool);
        setSupportActionBar(login_tool);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.loginTitle);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void initialize() {
        loginController = new LoginController(this,this);
        progress = new AlertProgress(this);
        progressDialog = findViewById(R.id.progressDialog);
        mEmailView = findViewById(R.id.at_login_email);
        etPasswordView = findViewById(R.id.et_login_password);
        tilEmail = findViewById(R.id.emtxtlayout);
        tilPassword = findViewById(R.id.passtxtlayout);
        loginscrollview = findViewById(R.id.loginscrollview);
        tilEmail.setTypeface(appTypeface.getHind_regular());
        tilPassword.setTypeface(appTypeface.getHind_regular());
        TextView signinortextv = findViewById(R.id.signinortextv);
        signinortextv.setTypeface(appTypeface.getHind_regular());
        TextView tv_login = findViewById(R.id.tv_login);
        tv_login.setTypeface(appTypeface.getHind_semiBold());
        tv_login.setOnClickListener(this);

        TextView tvSignUp = findViewById(R.id.tv_signup);
        tvSignUp.setOnClickListener(this);
        tvSignUp.setTypeface(appTypeface.getHind_medium());

        TextView tvForgotPass = findViewById(R.id.tv_forgotpass);
        RelativeLayout rlLoginWithFbButton = findViewById(R.id.loginwithfacbookbuttonrlout);


        tvForgotPass.setOnClickListener(this);
        rlLoginWithFbButton.setOnClickListener(this);

        TextView tvDntHaveAct = findViewById(R.id.tv_donthaveacount);
        TextView tvLoginWithFb = findViewById(R.id.tv_login_withfb);
        tvDntHaveAct.setTypeface(appTypeface.getHind_regular());

        tvForgotPass.setTypeface(appTypeface.getHind_regular());
        etPasswordView.setTypeface(appTypeface.getHind_regular());
        mEmailView.setTypeface(appTypeface.getHind_regular());
        tvLoginWithFb.setTypeface(appTypeface.getHind_regular());
    }

    private float dpToPx(ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener, int valueInDp) {
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        cancel = false;
        tilEmail.setError(null);
        tilPassword.setError(null);
        String email = mEmailView.getText().toString();
        String password = etPasswordView.getText().toString();

        // Check for a valid password, if the user entered one.
        loginController.checkPassword(password, this);

        // Check for a valid email address.
        loginController.checkEmailValidity(email, this);
        if (cancel) {
            focusView.requestFocus();
        } else
            loginController.CheckNetworkAvailablity(progress,this,email,password,1,"");
    }
    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.tv_login:
                attemptLogin();
                break;

            case R.id.loginwithfacbookbuttonrlout:
                callbackManager = CallbackManager.Factory.create();
                showProgress();
                loginController.facebook(callbackManager,this,progress);
                break;

            case R.id.tv_signup:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                finish();
                break;

            case R.id.tv_forgotpass:
                intent = new Intent(this,PasswordHelp.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onSuccess()
    {
        Log.d("ONActivity", "isMqttConnectedLogin: "+ApplicationController.getInstance().isMqttConnected());
        hideProgress();
        VariableConstant.isChattingCalled = true;
        Intent intnet = new Intent(this, SplashActivity.class);
        intnet.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intnet);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        finish();

    }

    @Override
    public void onError(String errormsg)
    {
        hideProgress();
        progress.alertinfo(errormsg);
    }

    @Override
    public void onError(String errorMsg, final Bundle loginBundle) {
        hideProgress();
        progress.alertPostivionclick(errorMsg, new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                if (isClicked) {
                    Intent intent = new Intent(ActivityLogin.this, SignUpActivity.class);
                    intent.putExtras(loginBundle);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public void networkAvalble(LoginPojo loginPojo)
    {
        showProgress();
        loginController.serviceMethod(loginPojo,this);
    }

    @Override
    public void networkAvalble(String emailOrPhone, boolean isEmailOrPhone) {

    }

    @Override
    public void notNetworkAvalble() {
        progress.showNetworkAlert();
    }

    @Override
    public void passwordValid() {
        tilPassword.setError(getString(R.string.error_invalid_password));
        focusView = etPasswordView;
        cancel = true;
    }

    @Override
    public void emailValid() {
        tilEmail.setError(getString(R.string.error_invalid_email));
        focusView = mEmailView;
        cancel = true;
        focusView();
    }

    @Override
    public void inValidEmail() {

        tilEmail.setError(getResources().getString(R.string.email_invalid));
        tilEmail.setErrorEnabled(true);
        focusView();
    }

    private void focusView()
    {
        focusView = mEmailView;
        cancel = true;
    }

    @Override
    public void onEnterEmailPhone() {


        tilEmail.setError(getResources().getString(R.string.enteremailorphone));
        tilEmail.setErrorEnabled(true);
        focusView();
    }

    @Override
    public void onEmailValidSuccess() {

        tilEmail.setError(null);
        tilEmail.setErrorEnabled(false);
    }

    @Override
    public void onPhoneError()
    {
        tilEmail.setError(getResources().getString(R.string.mobile_invalid));
        tilEmail.setErrorEnabled(true);
        focusView();
    }

    @Override
    public void emailEmpty() {
        tilEmail.setError(getString(R.string.error_field_required));
       /* focusView = mEmailView;
        cancel = true;*/
        focusView();
    }

    private void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressDialog.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressDialog.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

