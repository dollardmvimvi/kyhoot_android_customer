package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 12/5/18.
 */
public class DistanceMatrixPojo
{
    private String status;

    private String[] destination_addresses;

    private String[] origin_addresses;

    private ArrayList<Rows> rows;

    public String getStatus ()
    {
        return status;
    }

    public String[] getDestination_addresses ()
    {
        return destination_addresses;
    }

    public String[] getOrigin_addresses ()
    {
        return origin_addresses;
    }

    public ArrayList<Rows> getRows ()
    {
        return rows;
    }

}
