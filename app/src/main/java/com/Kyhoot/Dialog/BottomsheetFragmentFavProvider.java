package com.Kyhoot.Dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.adapter.Favorite_adapter;
import com.Kyhoot.interfaceMgr.OnFavoriteSelected;
import com.Kyhoot.main.HomeFragment;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 15/6/18.
 */
public class BottomsheetFragmentFavProvider extends BottomSheetDialogFragment implements View.OnClickListener,OnFavoriteSelected{
    public static final String PROVIDERID="PROVIDERID";
    public static final String IMAGEURL="IMAGEURL";
    RecyclerView rv_fav_list;
    Favorite_adapter favoriteAdapter;
    TextView tv_next;
    OnFavoriteSelected onFavoriteSelected;
    ArrayList<MqttProviderDetails> favProviders;
    boolean favoriteSelected=false;
    private Bundle favBundle;

    public BottomsheetFragmentFavProvider() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        favProviders = (ArrayList<MqttProviderDetails>) arguments.get(HomeFragment.FAVPROVIDER);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.favorite_provider_bottomsheet, container, false);
        init(view);
        return view;

    }

    private void init(View view) {
        TextView tv_fav_list_label=view.findViewById(R.id.tv_fav_list_label);
        rv_fav_list=view.findViewById(R.id.rv_fav_list);
        favoriteAdapter =new Favorite_adapter(favProviders,this);
        tv_next=view.findViewById(R.id.tv_next);
        rv_fav_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
        rv_fav_list.setAdapter(favoriteAdapter);
        tv_fav_list_label.setTypeface(AppTypeface.getInstance(getContext()).getHind_regular());
        tv_next.setTypeface(AppTypeface.getInstance(getContext()).getHind_semiBold());
        tv_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.tv_next:
                if(favoriteSelected){
                    onFavoriteSelected.onFavoriteSelected(favBundle);
                }else{
                    onFavoriteSelected.onOthersSelected();
                }
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onFavoriteSelected(Bundle bundle) {
        favoriteSelected=true;
        this.favBundle=bundle;
    }

    @Override
    public void onOthersSelected() {
        favoriteSelected=false;
    }

    public void setCallBack(OnFavoriteSelected onFavoriteSelected) {
        this.onFavoriteSelected=onFavoriteSelected;
    }
}
