package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>SongListBean</h>
 * Created by 3Embed on 10/10/2017.
 */

public class SongListBean implements Serializable {
    private String songlint, songName;

    public SongListBean(String songlint, String songName) {
        this.songlint = songlint;
        this.songName = songName;
    }

    public String getSonglint() {
        return songlint;
    }

    public String getSongName() {
        return songName;
    }
}
