package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.CategoriesAndServicesInterface;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ${3Embed} on 1/2/18.
 */

public class CategoriesAndServicesModel {
    public static String TAG="CATEGORYSERVICEMODEL";
    SharedPrefs sharedPrefs;
    CategoriesAndServicesInterface callback;
    Gson gson;
    public CategoriesAndServicesModel(SharedPrefs sharedPrefs, CategoriesAndServicesInterface callback) {
        this.sharedPrefs = sharedPrefs;
        this.callback = callback;
        gson=new Gson();
    }

    public void addCartData(String serviceId, int action, boolean isHourlyService,boolean isMinimumHour) {
        JSONObject jsonObject = new JSONObject();
        Log.e(TAG, "addCartData: isMinimumHour  "+isMinimumHour);

        try {
            jsonObject.put("categoryId", sharedPrefs.getCategoryId());
            jsonObject.put("serviceId", serviceId);
            if (isMinimumHour && isHourlyService) {
                //isMinimumHour=false;
                jsonObject.put("quntity", VariableConstant.MINIMUM_HOUR);
            } else
                jsonObject.put("quntity", 1);
            jsonObject.put("action", action);
            if(VariableConstant.BOOKINGMODEL==1){
                jsonObject.put("providerId", "");
            }else{
                jsonObject.put("providerId", VariableConstant.providerId);
            }
            if(isHourlyService){
                jsonObject.put("serviceType", 2);
            }else{
                jsonObject.put("serviceType", 1);
            }
            Log.d(TAG, "addCartData " + jsonObject);
            addCartDataServiceCall(jsonObject, callback);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addCartDataServiceCall(JSONObject jsonObject, final CategoriesAndServicesInterface confirmBook) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "cart",
                OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            confirmBook.onSuccessCartDataAdd(headerData,result);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //convert java object to JSON format

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        confirmBook.onFailureCartDataAdd(headerError,error);
                    }
                });
    }
    public void getCartServiceCall(String categoryId, final CategoriesAndServicesInterface confirmBook) {

        String url="";
        if(VariableConstant.BOOKINGMODEL==1){
            url=VariableConstant.SERVICE_URL + "cart/"+categoryId+"/0"; //Passing 0 instead of provider id if
                                                                        //provider id is not available
        }else{
            url=VariableConstant.SERVICE_URL + "cart/"+categoryId+"/"+VariableConstant.providerId+"";
        }
        Log.d(TAG, "getCartServiceCall: "+url+"  sharedPrefs.getSession():"+sharedPrefs.getSession());
        OkHttpConnection.requestOkHttpConnection(url,
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: "+headerData+""+result);
                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            confirmBook.onSuccessGetCart(headerData,result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //convert java object to JSON format
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        confirmBook.onFailureGetCart(headerError,error);
                    }
                });
    }

    public void getAccessToken(String data, final CategoriesAndServicesInterface inteface) {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                            inteface.onSuccessAccessToken(result);
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            inteface.onError("");
                        else {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    inteface.onSessionError(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccessToken(errorHandel.getData(), inteface);
                                    break;
                                default:
                                    inteface.onError(errorHandel.getMessage());
                                    break;
                            }
                        }
                    }
                });
    }

}
