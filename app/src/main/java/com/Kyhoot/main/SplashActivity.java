package com.Kyhoot.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.Splash_ViewPagerAdapter;
import com.Kyhoot.interfaceMgr.IPServiceListener;
import com.Kyhoot.interfaceMgr.NonMandatoryUpdateCallback;
import com.Kyhoot.pojoResponce.CategoriesPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.IPServicePojo;
import com.Kyhoot.utilities.ABaseTransformer;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.CirclePageIndicator;
import com.Kyhoot.utilities.LocationUtil;
import com.Kyhoot.utilities.MyFirebaseInstanceIDService;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.Kyhoot.utilities.WaveDrawable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.UUID;

import io.fabric.sdk.android.Fabric;

/**
 * <h>SplashActivity</h>
 * <p>Launching Activity or the landing page where login signUp button are there</p>
 */
public class SplashActivity extends AppCompatActivity implements View.OnClickListener, LocationUtil.LocationNotifier,IPServiceListener,NonMandatoryUpdateCallback {

    private static boolean isGetProviderCalled = false;
    private final String TAG = "SplashActivity";
    private ViewPager viewpager_custom;
    private CirclePageIndicator indicator;
    private SharedPrefs sharedPrefs;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionLocationArrayList;
    private LocationUtil networkUtilObj;
    private double latiLongi[] = new double[2];
    private RelativeLayout llSplashLoinSinupBt,rlSignText;
    private int visibleCountr = 1;
    private TextView tv_splsh_login, tv_splsh_signup,tvFindingArtist,tv_waveDrawable;;
    private ImageView splashLiveMLoading;
    private RelativeLayout  rlWebView,rlSplashLiveMVedio;
    private int count = 1;
    private VideoView videoView;
    private AlertProgress alertprogress;
    private FrameLayout parentView;
    private boolean called =false;
    private String categoryId="";
    private Gson gson;
    WaveDrawable waveDrawable;
    private int count2=1;
    AlertDialog dialog;
    ImageView logo;
    CategoriesPojo mCategoriesPojo=null;
    private boolean locationUpdated=false;
    private FrameLayout flLoginLayout;
    private ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        //startService(new Intent(getBaseContext(), OnMyService.class));
        gson=new Gson();
        printHashKey();
        initializeView();
        changeStatusBarColor();
        getPushToken();
        checkPlayServices();
    }

    /**
     * Creates a Debug HashKey
     */

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.d("Facebook", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Facebook", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("Facebook", "printHashKey()", e);
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        isGetProviderCalled = false;
        if (sharedPrefs.isLogin()) {
            tvFindingArtist.setVisibility(View.VISIBLE);
            if(alertprogress.isNetworkAvailable())
            {
                Log.d(TAG, "isMqttConnectedOnResumeIsConnected: "+ApplicationController.getInstance().isMqttConnected());
                if(!ApplicationController.getInstance().isMqttConnected())
                {
                    ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());
                }
                if(isLocationPermissionGranted()){
                    getLocationUpdates();
                }
            }
            else
            {
                alertprogress.showNetworkAlert();
            }
            if(waveDrawable!=null){
                waveDrawable.startAnimation();
            }
            tv_waveDrawable.setVisibility(View.VISIBLE);
            flLoginLayout.setVisibility(View.GONE);
        }
        //Showing splash screen for 2 sec
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(alertprogress.isNetworkAvailable()){
                    //GETTING IPADDRESS NEEDED FOR CATEGORY SERVICE CALL
                    new AlertProgress(SplashActivity.this).IPAddress(SplashActivity.this);
                }else{
                    alertprogress.showNetworkAlert();
                }
            }
        },2000);
    }

    private boolean isLocationPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            VariableConstant.isLocationPermissionGranted=true;
        }
        return VariableConstant.isLocationPermissionGranted;
    }
    private void getLocationUpdates(){
        networkUtilObj = new LocationUtil(this, this);
        if (!networkUtilObj.isGoogleApiConnected())
        {
            networkUtilObj.checkLocationSettings();
        }
    }


    private void getCategories() {
        Log.d(TAG, "callGetCategoriesSess: " + sharedPrefs.getSession() +"LatLong "+latiLongi[0]+" "+latiLongi[1 ]+VariableConstant.setIPAddress);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "categories/" + latiLongi[0] + "/" + latiLongi[1]+"/"+VariableConstant.setIPAddress,
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: getCategories "+result+"");
                        sharedPrefs.setCategoriesList(result);
                        if(result!=null && result!=""){
                            try{
                                mCategoriesPojo=gson.fromJson(result,CategoriesPojo.class);
                                if(Utilities.isLatestVersion(mCategoriesPojo.getData().getCityData().getAppVersion())){
                                    if(mCategoriesPojo.getData().getCityData().getMandatory()){
                                        Utilities.appUpdateMandatory(SplashActivity.this);
                                    }else{
                                        Utilities.appUpdateNonMandatory(SplashActivity.this, SplashActivity.this);
                                    }
                                }else{
                                    handleCategoryResponse(mCategoriesPojo);
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }else{
                            Toast.makeText(SplashActivity.this,"No services found",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d(TAG, "onError: getCategories "+error+ " headererror "+headerError);
                        if(headerError.equals("502")){
                            if(dialog==null){
                                AlertDialog.Builder builder=new AlertDialog.Builder(SplashActivity.this);
                                builder.setMessage("COULD NOT CONNECT TO SERVER");
                                builder.setCancelable(false);
                                builder.setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        getCategories();
                                    }
                                });
                                dialog = builder.create();
                                if(!isFinishing()){
                                    dialog.show();
                                }

                            }else if(dialog!=null && !dialog.isShowing()){
                                if(!isFinishing()){
                                    dialog.show();
                                }
                            }

                        }else if(headerError.equals("549")){
                            if(dialog==null){
                                AlertDialog.Builder builder=new AlertDialog.Builder(SplashActivity.this);
                                builder.setMessage("COULD NOT CONNECT TO SERVER");
                                builder.setCancelable(false);
                                builder.setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        getCategories();
                                    }
                                });
                                dialog = builder.create();
                                if(!isFinishing()){
                                    dialog.show();
                                }
                            }else if(dialog!=null && !dialog.isShowing()){
                                if(!isFinishing()){
                                    dialog.show();
                                }
                            }
                        }else{
                            try {
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch(headerError){
                                    case "549":
                                        if(dialog==null ){
                                            AlertDialog.Builder builder=new AlertDialog.Builder(SplashActivity.this);
                                            builder.setMessage("COULD NOT GET CATEGORY");
                                            builder.setCancelable(false);
                                            builder.setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                    getCategories();
                                                }
                                            });
                                            dialog = builder.create();
                                            if(!isFinishing()){
                                                dialog.show();
                                            }
                                        }else if(dialog!=null && !dialog.isShowing()){
                                            if(!isFinishing()){
                                                dialog.show();
                                            }
                                        }
                                        break;
                                    case "498":
                                        callSetVisiblityVisible();

                                        if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                                            networkUtilObj.stoppingLocationUpdate();
                                        break;
                                    case "404":
                                            Intent inent = new Intent(SplashActivity.this, MenuActivity.class);
                                            startActivity(inent);
                                            finish();
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                }

                                if(error.contains("Failed to connect")){
                                    if(dialog==null ){
                                        AlertDialog.Builder builder=new AlertDialog.Builder(SplashActivity.this);
                                        builder.setMessage("Unable to connect to Server ");
                                        builder.setCancelable(false);
                                        builder.setPositiveButton("RECONNECT", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                getCategories();
                                            }
                                        });
                                        dialog = builder.create();
                                        if(!isFinishing()){
                                            dialog.show();
                                        }
                                    }else if(dialog!=null && !dialog.isShowing()){
                                        if(!isFinishing()){
                                            dialog.show();
                                        }
                                    }
                                }
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
    }

    public void handleCategoryResponse(CategoriesPojo categoriesPojo) {
        long l = Utilities.setServerTimeAndDifference(categoriesPojo.getData().getServerGmtTime());
        sharedPrefs.setServerTimeDifference(l);
        sharedPrefs.setPaymentId(categoriesPojo.getData().getCityData().getStripeKeys());
        VariableConstant.SERVERTIMEDIFFERNCE=l;
        VariableConstant.DISTANCEMATRIXUNIT=categoriesPojo.getData().getCityData().getDistanceMatrix();
        VariableConstant.CARDBOOKING=categoriesPojo.getData().getCityData().getPaymentMode().getCard();
        VariableConstant.CASHBOOKING=categoriesPojo.getData().getCityData().getPaymentMode().getCash();
        VariableConstant.WALLETBOOKING=categoriesPojo.getData().getCityData().getPaymentMode().getWallet();
        VariableConstant.GOOGLEKEY=categoriesPojo.getData().getCityData().getCustGoogleMapKeys();

        Log.e(TAG, "handleCategoryResponse:  google keys" +categoriesPojo.getData().getCityData().getCustGoogleMapKeys().size());

        if(VariableConstant.GOOGLEKEY.size()>0){
            VariableConstant.ROTATEDKEY=VariableConstant.GOOGLEKEY.get(0);
            Log.e(TAG, "handleCategoryResponse: current Key"+VariableConstant.GOOGLEKEY.get(0));
        }
        Log.d(TAG, "onSuccess: islatestVersion: "+Utilities.isLatestVersion(categoriesPojo.getData().getCityData().getAppVersion()));

        try{
            sharedPrefs.setCurrencySymbl(categoriesPojo.getData().getCityData().getCurrencySymbol());
            sharedPrefs.setCurrencyPosition(categoriesPojo.getData().getCityData().getCurrencyAbbrText());
            VariableConstant.CurrencySymbol=categoriesPojo.getData().getCityData().getCurrencySymbol();
            VariableConstant.CurrencyPosition=categoriesPojo.getData().getCityData().getCurrencyAbbrText();
        }catch (Exception e){
            e.printStackTrace();
        }

        ArrayList<ArrayList<ArrayList<Double>>> coordinates = categoriesPojo.getData().getCityData().getPolygons().getCoordinates();
        if(coordinates.size()>0){
            for(ArrayList<Double> doubleArrayList: coordinates.get(0)){
                Log.d(TAG, "onSuccess: lat: "+doubleArrayList.get(0)+ " long:"+doubleArrayList.get(1));
            }
        }
        VariableConstant.PROFREQUENCYINTERVAL = categoriesPojo.getData().getCityData().getCustomerFrequency().getCustomerHomePageInterval();
        //sprefs.setPaymentId(categoriesPojo.getData().getCityData().getStripeTestKeys());
        sharedPrefs.setFcmTopicCity(categoriesPojo.getData().getCityData().getPushTopics().getCity());
        sharedPrefs.setFcmTopicAllCustomer(categoriesPojo.getData().getCityData().getPushTopics().getAllCustomers());
        sharedPrefs.setFcmTopicAllCityCustomer(categoriesPojo.getData().getCityData().getPushTopics().getAllCitiesCustomers());
        sharedPrefs.setFcmTopicAllOutZoneCustomers(categoriesPojo.getData().getCityData().getPushTopics().getOutZoneCustomers());
        try{
            FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicCity());
            FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicAllCustomer());
            FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopicAllCityCustomer());
        }catch (Exception e){
            e.printStackTrace();
        }

        //FirebaseMessaging.getInstance().subscribeToTopic( + sharedPrefs.getFcmTopicAllOutZoneCustomers());
        //Getting Notary Category
        if(categoriesPojo.getData().getCatArr().size()>0){
                categoryId=categoriesPojo.getData().getCatArr().get(0).get_id();
                VariableConstant.selectedCategoryId=categoryId;
                VariableConstant.BILLINGMODEL=categoriesPojo.getData().getCatArr().get(0).getBilling_model();
                VariableConstant.MINIMUM_HOUR=categoriesPojo.getData().getCatArr().get(0).getMinimum_hour();
                //VariableConstant.SURGE_PRICE=categoriesPojo.getData().getCatArr().get(0).getSurgePrice();
                VariableConstant.BOOKINGTYPEAVAILABILITY=categoriesPojo.getData().getCatArr().get(0).getBookingTypeAction();
                VariableConstant.SERVICEHOURLYPRICE=categoriesPojo.getData().getCatArr().get(0).getPrice_per_fees();
                VariableConstant.SELECTEDCATEGORYNAME=categoriesPojo.getData().getCatArr().get(0).getCat_name();
                VariableConstant.SERVICE_TYPE=categoriesPojo.getData().getCatArr().get(0).getService_type();
                VariableConstant.SELECTEDCATEGORYVISITFEE=categoriesPojo.getData().getCatArr().get(0).getVisit_fees();
                sharedPrefs.setCategoryId(VariableConstant.selectedCategoryId);
                sharedPrefs.setBillingModel(VariableConstant.BILLINGMODEL);
                sharedPrefs.setCategoryHourlyPrice(VariableConstant.SERVICEHOURLYPRICE);
                sharedPrefs.setSelectedCategoryName(VariableConstant.SELECTEDCATEGORYNAME);
                sharedPrefs.setSelectedServiceType(VariableConstant.SERVICE_TYPE);
                categoriesPojo.getData().getCatArr().get(0).setSelected(true);

                callGetProvider();
        }else{
            Toast.makeText(SplashActivity.this,"No services found",Toast.LENGTH_LONG).show();
        }
    }



    private void getPushToken() {
        if (checkPlayServices()) {

            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);
            String tokenis =  FirebaseInstanceId.getInstance().getToken();
            if(tokenis!=null) {
                sharedPrefs.setRegistrationId(tokenis);
            }
        }else {
            alertprogress.alertinfo(getResources().getString(R.string.splash_playServicenotfound));
        }
    }
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }

    /**
     * <h1>initializeView</h1>
     * initializing view
     */
    private void initializeView() {
        waveDrawable = new WaveDrawable(ContextCompat.getColor(this, R.color.colorPrimaryDark),301);
        waveDrawable.setWaveInterpolator(new LinearInterpolator());
        tv_waveDrawable=findViewById(R.id.tv_waveDrawable);
        tv_waveDrawable.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_waveDrawable.setBackgroundDrawable(waveDrawable);
        VariableConstant.isSplashCalled = true;
        sharedPrefs = new SharedPrefs(this);
        alertprogress = new AlertProgress(this);
        rlSignText = findViewById(R.id.rlSignText);
        flLoginLayout = findViewById(R.id.flLoginLayout);
        ivLogo = findViewById(R.id.ivLogo);

        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        if(!android_id.equals("") && android_id!=null)
        {
            sharedPrefs.setDeviceId(android_id);
        }
        else
        {
            String uniqueID = UUID.randomUUID().toString();
            Log.i(TAG, "initializeViewDeviceId: "+uniqueID);
            sharedPrefs.setDeviceId(uniqueID);
        }
        rlSplashLiveMVedio=findViewById(R.id.rlSplashLiveMVedio);
        splashLiveMLoading = findViewById(R.id.splashLiveMLoading);
        sharedPrefs.setIsProfile(true);
        //Hidding GIF for notary
        /*Glide.with(this)
                .asGif()
                .load(R.raw.notary_loader)
                .into(splashLiveMLoading);*/

        rlWebView = findViewById(R.id.rlWebView);
        logo=findViewById(R.id.logo);
        //rlSplashLiveMVedio = findViewById(R.id.rlSplashLiveMVedio);
        parentView=findViewById(R.id.parentView);
        llSplashLoinSinupBt = findViewById(R.id.llSplashLoinSinupBt);
        llSplashLoinSinupBt.setVisibility(View.GONE);
        viewpager_custom = findViewById(R.id.viewpager_custom);
        indicator = findViewById(R.id.indicator);
        viewpager_custom.setPageTransformer(true, new ABaseTransformer());
        tv_splsh_login = findViewById(R.id.tv_splsh_login);
        tv_splsh_signup = findViewById(R.id.tv_splsh_signup);
        tvFindingArtist = findViewById(R.id.tvFindingArtist);
        AppTypeface typeface = AppTypeface.getInstance(this);
        tv_splsh_login.setOnClickListener(this);
        tv_splsh_signup.setOnClickListener(this);
        tvFindingArtist.setTypeface(typeface.getHind_light());
        tvFindingArtist.setVisibility(View.GONE);
        tv_splsh_signup.setTypeface(typeface.getHind_semiBold());
        tv_splsh_login.setTypeface(typeface.getHind_semiBold());
        initViewPagerControls();

    }

    /**
     * <h>initViewPagerControls</h>
     * <p>layouts of all welcome sliders
     * add few more layouts if you want</p>
     */
    private void initViewPagerControls() {
        int[] layouts = new int[]{R.layout.welcome_screen1,R.layout.welcome_screen2, R.layout.welcome_screen3};
        //,R.layout.welcome_screen2, R.layout.welcome_screen3
        Splash_ViewPagerAdapter splash_viewPagerAdapter = new Splash_ViewPagerAdapter(SplashActivity.this, layouts);
        viewpager_custom.setAdapter(splash_viewPagerAdapter);
        indicator.setViewPager(viewpager_custom);
        splash_viewPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tv_splsh_login:
                if(latiLongi[0]!=0 && latiLongi[1]!=0)
                {
                    count = 2;
                    tv_splsh_login.setSelected(false);
                    tv_splsh_signup.setSelected(false);
                    VariableConstant.isSplashCalled = false;
                    Intent intent = new Intent(this, ActivityLogin.class);
                    startActivity(intent);
                }
                // finish();
                break;

            case R.id.tv_splsh_signup:
                if(latiLongi[0]!=0 && latiLongi[1]!=0)
                {
                    count = 2;
                    tv_splsh_login.setSelected(true);
                    tv_splsh_signup.setSelected(true);
                    VariableConstant.isSplashCalled = false;
                    stopWaveDrawableAnimation();
                    Intent intent1 = new Intent(this, SignUpActivity.class);
                    Log.d(TAG, "onClick: "+VariableConstant.setIPAddress);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
                }
                break;
            default:
                break;
        }
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    public void updateLocation(Location location) {
        Log.d(TAG, "updateLocation: " + location.getLatitude()+" "+location.getLongitude() + " boolean " + isGetProviderCalled);
        latiLongi[0] = location.getLatitude();
        latiLongi[1]= location.getLongitude();
        sharedPrefs.setSplashLatitude(latiLongi[0] + "");
        sharedPrefs.setSplashLongitude(latiLongi[1] + "");
        VariableConstant.SPLASHLAT=latiLongi[0];
        VariableConstant.SPLASHLONG=latiLongi[1];
        VariableConstant.BOOKINGLAT=latiLongi[0];
        VariableConstant.BOOKINGLONG=latiLongi[1];
        Utilities.getTimeZoneFromLocation();
        locationUpdated=true;
        if (sharedPrefs.isLogin())
        {
            if (!isGetProviderCalled)
            {
                if(alertprogress.isNetworkAvailable())
                {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            if(!called){
                                if(VariableConstant.ipAddressSet){
                                    getCategories();
                                }
                            }else{
                                callGetProvider();
                            }
                        }
                    });
                }
            }
        } else {
            if (visibleCountr == 1) {
                visibleCountr++;
                callSetVisiblityVisible();

                if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                    networkUtilObj.stoppingLocationUpdate();
            }
        }
    }

    private void callSetVisiblityVisible() {
        llSplashLoinSinupBt.setVisibility(View.VISIBLE);
      //  indicator.setVisibility(View.VISIBLE);
        //parentView.setBackgroundDrawable(getResources().getDrawable(R.drawable.splash));
      //  logo.setVisibility(View.VISIBLE);
        rlSplashLiveMVedio.setVisibility(View.VISIBLE);
        splashLiveMLoading.setVisibility(View.GONE);
        rlWebView.setVisibility(View.GONE);
     //   viewpager_custom.setVisibility(View.VISIBLE);
        rlSignText.setVisibility(View.VISIBLE);
        flLoginLayout.setVisibility(View.VISIBLE);
        ivLogo.setVisibility(View.GONE);
        playVideo();
    }

    private void playVideo() {
        videoView = findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+ R.raw.splash_video);
        videoView.setVideoURI(uri);
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }

    private void callGetProvider() {
        if(!categoryId.equalsIgnoreCase("")){
            Log.d(TAG, "callGetProviderSess: " +VariableConstant.SERVICE_URL + "provider/" + latiLongi[0] + "/" + latiLongi[1]+"/"+VariableConstant.setIPAddress+" ,session and "+sharedPrefs.getSession());
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "provider/" + latiLongi[0] + "/" + latiLongi[1]+"/"+VariableConstant.setIPAddress,
                    OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang
                    , new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerData, String result) {
                            isGetProviderCalled = true;
                            Log.d(TAG, "onSuccess: " + result);
                            if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                                networkUtilObj.stoppingLocationUpdate();
                            if(!result.contains("Unable to resolve host"))
                            {
                                // ApplicationController.getInstance().gethModel().saveImage(result,SplashActivity.this);
                                if (isGetProviderCalled) {
                                    if (count == 1)
                                    {
                                        Log.d(TAG, "onSuccessActivity: "+count);
                                        count++;
                                        stopWaveDrawableAnimation();
                                        Intent inent = new Intent(SplashActivity.this, MenuActivity.class);
                                        sharedPrefs.setProviderData(result);
                                        startActivity(inent);
                                        finish();
                                    }
                                }
                            } else {
                                Toast.makeText(SplashActivity.this,getString(R.string.serverProblmInternet),Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(String headerError, String error) {
                            if (headerError.equals("502"))
                                alertprogress.alertinfo(SplashActivity.this.getString(R.string.serverProblm));
                            else {
                                try{
                                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                    Log.d(TAG, "onError: " + error);
                                    switch (headerError) {
                                        case "440":

                                            isGetProviderCalled = false;
                                            getAccessToken(errorHandel.getData());
                                            if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                                                networkUtilObj.stoppingLocationUpdate();
                                            break;
                                        case "498":
                                            callSetVisiblityVisible();

                                            if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                                                networkUtilObj.stoppingLocationUpdate();
                                            break;
                                        case "401":
                                            callSetVisiblityVisible();
                                            if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                                                networkUtilObj.stoppingLocationUpdate();
                                            break;
                                        case "500":
                                            alertprogress.alertinfo(errorHandel.getMessage());
                                            break;
                                        case "404":
                                            if(count2==1){
                                                if(errorHandel.getMessage().equalsIgnoreCase(getString(R.string.no_providers_available))){
                                                    Toast.makeText(SplashActivity.this,errorHandel.getMessage(),Toast.LENGTH_LONG).show();
                                                }
                                                stopWaveDrawableAnimation();
                                                Intent inent = new Intent(SplashActivity.this, MenuActivity.class);
                                                sharedPrefs.setProviderData("");
                                                startActivity(inent);
                                                count2++;
                                            }

                                            break;
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        }else{
            Log.d(TAG, "callGetProvider: Category id is blank");
        }

    }

    private void stopWaveDrawableAnimation() {
        if(waveDrawable!=null){
            waveDrawable.stopAnimation();
        }
    }

    @Override
    public void locationMsg(String error)
    {
        Log.d(TAG, "locationMsg: "+error);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onPause() {
        super.onPause();
        isGetProviderCalled = false;
        if(networkUtilObj!=null && networkUtilObj.isGoogleApiConnected())
            networkUtilObj.destroy_Location_update();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(networkUtilObj!=null && networkUtilObj.isGoogleApiConnected())
            networkUtilObj.destroy_Location_update();
    }

    private void getAccessToken(String data) {
        Log.d(TAG, "getAccessToken: "+VariableConstant.SERVICE_URL + "accessToken");
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            sharedPrefs.setSession(jsonobj.getString("data"));
                            getCategories();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            if(headerError.equals("498")) {
                                callSetVisiblityVisible();
                                sharedPrefs.clearSession();
                            }
                        }catch (Exception e){

                        }
                    }
                });
    }

    @Override
    public void onIPServiceSuccess(IPServicePojo s) {
        VariableConstant.setIPAddress = s.getIp();
        VariableConstant.ipAddressSet = true;
        String[] split = s.getLoc().split(",");
        VariableConstant.IPLAT = Double.parseDouble(split[0]);
        VariableConstant.IPLONG = Double.parseDouble(split[1]);
        Log.d(TAG, "onIPServiceSuccess: updatelocation:"+VariableConstant.IPLAT+" "+VariableConstant.IPLONG);
        if(sharedPrefs.isLogin()){
            if (!VariableConstant.isLocationPermissionGranted) {
                latiLongi[0] = VariableConstant.IPLAT;
                latiLongi[1] = VariableConstant.IPLONG;
                sharedPrefs.setSplashLatitude(latiLongi[0] + "");
                sharedPrefs.setSplashLongitude(latiLongi[1] + "");
                checkLoginAndProceed();
            } else if (locationUpdated) {
                if (!called) {
                    checkLoginAndProceed();
                }
            }
        }else{
            latiLongi[0] = VariableConstant.IPLAT;
            latiLongi[1] = VariableConstant.IPLONG;
            sharedPrefs.setSplashLatitude(latiLongi[0] + "");
            sharedPrefs.setSplashLongitude(latiLongi[1] + "");
            callSetVisiblityVisible();
        }

    }

    private void checkLoginAndProceed(){
        if (sharedPrefs.isLogin()) {
            if (!isGetProviderCalled) {
                if(alertprogress.isNetworkAvailable())
                {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getCategories();
                        }
                    },0);
                }
            }
        } else {
            if (visibleCountr == 1) {
                visibleCountr++;
                callSetVisiblityVisible();

                if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                    networkUtilObj.stoppingLocationUpdate();
            }
        }
    }

    @Override
    public void onIPServiceFailed() {
        Log.e(TAG, "onIPServiceFailed: "+"IPSERVICE FAILED" );
        callSetVisiblityVisible();
    }

    @Override
    public void onLaterClicked() {
        handleCategoryResponse(mCategoriesPojo);
    }

    @Override
    public void onUpdateClicked() {
        finish();
    }


}
