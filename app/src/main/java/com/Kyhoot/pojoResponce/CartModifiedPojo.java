package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by ${3Embed} on 2/2/18.
 */

public class CartModifiedPojo implements Serializable {

        private String message;

        private CartData data;

        public String getMessage ()
        {
            return message;
        }

        public void setMessage (String message)
        {
            this.message = message;
        }

        public CartData getData ()
        {
            return data;
        }

        public void setData (CartData data)
        {
            this.data = data;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", data = "+data+"]";
        }
}
