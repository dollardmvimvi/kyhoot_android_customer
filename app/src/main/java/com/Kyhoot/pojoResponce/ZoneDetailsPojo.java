package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 16/3/18.
 */

public class ZoneDetailsPojo
{
    private String customerPushTopic;

    private PointsProps pointsProps;

    private String _id;

    private String currencySymbol;

    private int distanceMatrix;

    private String isDeleted;

    private String providerPushTopic;

    private Polygons polygons;

    private String currency;

    private String city;

    private String country;

    private String ci_csrf_token;

    private String currencyAbbrText;

    private String stripeKeys;

    private Boolean mandatory;

    private PaymentMode paymentMode;

    private ConfigPojo.ConfigData.PushTopics pushTopics;

    private ConfigPojo.ConfigData.CustomerFrequency customerFrequency;

    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public ConfigPojo.ConfigData.PushTopics getPushTopics() {
        return pushTopics;
    }

    public ConfigPojo.ConfigData.CustomerFrequency getCustomerFrequency() {
        return customerFrequency;
    }

    public String getStripeKeys() {
        return stripeKeys;
    }

    public String getCurrencyAbbrText() {
        return currencyAbbrText;
    }

    public void setCurrencyAbbrText(String currencyAbbrText) {
        this.currencyAbbrText = currencyAbbrText;
    }

    public String getAppVersion() {
        return appVersion;
    }

    private String appVersion;

    public ArrayList<String> getCustGoogleMapKeys() {
        return custGoogleMapKeys;
    }

    private ArrayList<String> custGoogleMapKeys;

    public String getCustomerPushTopic ()
    {
        return customerPushTopic;
    }

    public void setCustomerPushTopic (String customerPushTopic)
    {
        this.customerPushTopic = customerPushTopic;
    }

    public PointsProps getPointsProps ()
    {
        return pointsProps;
    }

    public void setPointsProps (PointsProps pointsProps)
    {
        this.pointsProps = pointsProps;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getCurrencySymbol ()
    {
        return currencySymbol;
    }

    public void setCurrencySymbol (String currencySymbol)
    {
        this.currencySymbol = currencySymbol;
    }

    public int getDistanceMatrix ()
    {
        return distanceMatrix;
    }

    public void setDistanceMatrix (int distanceMatrix)
    {
        this.distanceMatrix = distanceMatrix;
    }

    public String getIsDeleted ()
    {
        return isDeleted;
    }

    public void setIsDeleted (String isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public String getProviderPushTopic ()
    {
        return providerPushTopic;
    }

    public void setProviderPushTopic (String providerPushTopic)
    {
        this.providerPushTopic = providerPushTopic;
    }

    public Polygons getPolygons ()
    {
        return polygons;
    }

    public void setPolygons (Polygons polygons)
    {
        this.polygons = polygons;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getCi_csrf_token ()
    {
        return ci_csrf_token;
    }

    public void setCi_csrf_token (String ci_csrf_token)
    {
        this.ci_csrf_token = ci_csrf_token;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [customerPushTopic = "+customerPushTopic+", pointsProps = "+pointsProps+", _id = "+_id+", currencySymbol = "+currencySymbol+", distanceMatrix = "+distanceMatrix+", isDeleted = "+isDeleted+", providerPushTopic = "+providerPushTopic+", polygons = "+polygons+", currency = "+currency+", city = "+city+", country = "+country+", ci_csrf_token = "+ci_csrf_token+"]";
    }
}

