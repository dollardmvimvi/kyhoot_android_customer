package com.Kyhoot.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.pojoResponce.CartData;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CancelledBooking extends AppCompatActivity {

    private AppTypeface appTypeface;
    private long bid;
    private String imageurl;
    private int status;
    private ArrayList<AllEventsDataPojo> garbegeEventPojo;
    private String currencySymbol = "";
    private TextView tvCancelReasons,tvCancellationReasons,tvCancellationReason,tvTotalBillAmount,tvCancelTotalAmt1,tvDistance,tvCancelCustomerName
            ,tvYourRatedLabel,tvRatingNum,tvTotalAmt,tvCancelAmount,tvPaymentMethod,tvCompleteCard,tvVisitFee,tvVisitFeeAmt,tvLastDues,tvLastDuesAmt
            ,tvPaymentBreak,tvBillingTotalAmt,tvBillingTotal,tvCategory,tvCategoryName;
    private ImageView ivCompleteCardLogo;
    private RatingBar rbRating;
    private RelativeLayout rlCancelLayout,rlLastdues;
    private LinearLayout ll_cancel_profile;
    private LinearLayout llDistancelayout,llLiveFee;
    private RelativeLayout rl_ratingLayout,rldiscount,rlconfm_travelfee,rlconfm_visitfee,rlBillingTotal,rltotal,rlSelectedCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancelled_booking);
        appTypeface = AppTypeface.getInstance(this);
        getIntentValues();
        initialize();
        initializeToolBar();
        // overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
    }

    private void getIntentValues() {
        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            bid = extras.getLong("BID", 0);
            imageurl = extras.getString("ImageUrl");
            garbegeEventPojo = (ArrayList<AllEventsDataPojo>) extras.getSerializable("ALLPOJO");
            status = extras.getInt("STATUS",0);
            Log.w("CancelledBooking", "getIntentValues: "+status);
            currencySymbol = garbegeEventPojo.get(0).getCurrencySymbol();
        }
    }

    private void initializeToolBar() {
        Toolbar toolbarCancel = findViewById(R.id.toolbarCancel);
        setSupportActionBar(toolbarCancel);
        TextView bookingStatusId = findViewById(R.id.bookingStatusId);
        TextView bookingStatusDate = findViewById(R.id.bookingStatusDate);
        getSupportActionBar().setTitle("");
        String eventBid = getResources().getString(R.string.eventId)+" "+bid;
        bookingStatusId.setText(eventBid);
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(garbegeEventPojo.get(0).getServerTime());
        SimpleDateFormat sdf=new SimpleDateFormat("d MMM , yy");
        bookingStatusDate.setText(sdf.format(calendar.getTime()));
        bookingStatusId.setTypeface(appTypeface.getHind_semiBold());
        bookingStatusDate.setTypeface(appTypeface.getHind_regular());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarCancel.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolbarCancel.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        timeMethod(bookingStatusDate);
    }

    private void timeMethod(TextView tvDate) {
        try {

            Log.d("TAGTIME", " expireTime " + garbegeEventPojo.get(0).getBookingRequestedFor());
            Date date = new Date(garbegeEventPojo.get(0).getBookingRequestedFor() * 1000L);

            String formattedDate = Utilities.getFormattedDate(date);

            // String splitDate[] = formattedDate.split(" ");

            String dat[] = formattedDate.split(",");

            tvDate.setText(dat[0]);

            /*String month = Utilities.MonthName(Integer.parseInt(dat[0]));
            String day = Utilities.getDayNumberSuffix(Integer.parseInt(dat[1]));
            String year = dat[2];
            String bookinDate = dat[1] + day + " " + month + " " + year;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                tvDate.setText(Html.fromHtml(bookinDate, Html.FROM_HTML_MODE_LEGACY));
            else
                tvDate.setText(Html.fromHtml(bookinDate));*/
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }

    private void initialize() {
        SharedPrefs sharedPrefs = new SharedPrefs(this);
        double size[] = Scaler.getScalingFactor(this);
        double width = 80 * size[0];
        double height = 80 * size[1];
        rlCancelLayout=findViewById(R.id.rlCancelLayout);
        llDistancelayout=findViewById(R.id.llDistancelayout);
        rltotal=findViewById(R.id.rltotal);
        if(status==5||status==4){
            rltotal.setVisibility(View.GONE);
        }
        llLiveFee=findViewById(R.id.llLiveFee);
        tvVisitFeeAmt=findViewById(R.id.tvVisitFeeAmt);
        tvVisitFee=findViewById(R.id.tvVisitFee);
        rlconfm_visitfee=findViewById(R.id.rlconfm_visitfee);
        tvCancelReasons = findViewById(R.id.tvCancelReasons);
        tvCancellationReasons = findViewById(R.id.tvCancellationReasons);
        tvCancellationReason = findViewById(R.id.tvCancellationReason);
        rlSelectedCategory = findViewById(R.id.rlSelectedCategory);
        ImageView ivCancelProProfile = findViewById(R.id.ivCancelProProfile);
        tvCategory=findViewById(R.id.tvCategory);
        tvCategoryName=findViewById(R.id.tvCategoryName);
        TextView tvCancelEventLoc = findViewById(R.id.tvCancelEventLoc);
        TextView tvCancelEventLocation = findViewById(R.id.tvCancelEventLocation);
        tvTotalAmt=findViewById(R.id.tvTotalAmt);
        ll_cancel_profile=findViewById(R.id.ll_cancel_profile);
        rl_ratingLayout=findViewById(R.id.rl_ratingLayout);
        RelativeLayout rlCancelFee = findViewById(R.id.rlCancelFee);
        TextView tvCancel = findViewById(R.id.tvCancel);
        tvCancelAmount = findViewById(R.id.tvCancelAmount);
        rbRating = findViewById(R.id.rbRating);
        tvTotalBillAmount=findViewById(R.id.tvTotalBillAmount);
        tvCancelTotalAmt1=findViewById(R.id.tvCancelTotalAmt1);
        tvCancelCustomerName=findViewById(R.id.tvCancelCustomerName);
        tvDistance=findViewById(R.id.tvDistance);
        tvCancelCustomerName=findViewById(R.id.tvCancelCustomerName);
        tvYourRatedLabel=findViewById(R.id.tvYourRatedLabel);
        tvRatingNum=findViewById(R.id.tvRatingNum);
        rldiscount=findViewById(R.id.rldiscount);
        tvPaymentMethod=findViewById(R.id.tvPaymentMethod);
        tvCompleteCard=findViewById(R.id.tvCompleteCard);
        ivCompleteCardLogo=findViewById(R.id.ivCompleteCardLogo);
        rlconfm_travelfee=findViewById(R.id.rlconfm_travelfee);
        rlLastdues=findViewById(R.id.rlLastdues);
        tvLastDues=findViewById(R.id.tvLastDues);
        tvPaymentBreak=findViewById(R.id.tvPaymentBreak);
        tvLastDuesAmt=findViewById(R.id.tvLastDuesAmt);
        rlBillingTotal=findViewById(R.id.rlBillingTotal);
        tvBillingTotalAmt=findViewById(R.id.tvBillingTotalAmt);
        tvBillingTotal=findViewById(R.id.tvBillingTotal);
        tvTotalBillAmount.setTypeface(appTypeface.getHind_regular());
        tvCancelTotalAmt1.setTypeface(appTypeface.getHind_semiBold());
        tvBillingTotal.setTypeface(appTypeface.getHind_semiBold());
        tvBillingTotalAmt.setTypeface(appTypeface.getHind_semiBold());
        tvCancelCustomerName.setTypeface(appTypeface.getHind_semiBold());
        tvDistance.setTypeface(appTypeface.getHind_regular());
        tvYourRatedLabel.setTypeface(appTypeface.getHind_semiBold());
        tvRatingNum.setTypeface(appTypeface.getHind_semiBold());
        tvCancelEventLoc.setTypeface(appTypeface.getHind_semiBold());
        tvPaymentMethod.setTypeface(appTypeface.getHind_semiBold());
        tvPaymentBreak.setTypeface(appTypeface.getHind_semiBold());
        tvCancelEventLocation.setTypeface(appTypeface.getHind_regular());
        tvCompleteCard.setTypeface(appTypeface.getHind_regular());
        tvCancel.setTypeface(appTypeface.getHind_regular());
        tvCancelAmount.setTypeface(appTypeface.getHind_regular());
        tvCancellationReasons.setTypeface(appTypeface.getHind_regular());
        tvCancelReasons.setTypeface(appTypeface.getHind_regular());
        tvCancellationReason.setTypeface(appTypeface.getHind_regular());
        tvLastDues.setTypeface(appTypeface.getHind_regular());
        tvVisitFee.setTypeface(appTypeface.getHind_regular());
        tvCategoryName.setTypeface(appTypeface.getHind_regular());
        tvCategory.setTypeface(appTypeface.getHind_regular());
        tvPaymentBreak.setTypeface(appTypeface.getHind_semiBold());
        rldiscount.setVisibility(View.GONE);
        rlconfm_travelfee.setVisibility(View.GONE);
        setPaymentBreakdown(garbegeEventPojo.get(0).getCart());
        if (!imageurl.equals("") && imageurl != null) {
            Picasso.with(this)
                    .load(imageurl)
                    .resize((int) width, (int) height)
                    .transform(new CircleTransform())
                    .into(ivCancelProProfile);
        }
        tvPaymentBreak.setText(R.string.requested_services);
        String name = garbegeEventPojo.get(0).getFirstName()+" "+garbegeEventPojo.get(0).getLastName();
        tvCancelCustomerName.setText(name);
        //tvCancelEvent.setText(garbegeEventPojo.get(0).getTypeofEvent().getName());
        tvCancelEventLocation.setText(garbegeEventPojo.get(0).getAddLine1());
        rbRating.setRating(garbegeEventPojo.get(0).getAverageRating());
        tvRatingNum.setText(garbegeEventPojo.get(0).getAverageRating()+"");
        tvCategoryName.setText(garbegeEventPojo.get(0).getCart().getCategoryName());
        if(garbegeEventPojo.get(0).getPaymentMethod().equalsIgnoreCase("cash")) {
            ivCompleteCardLogo.setImageBitmap(Utilities.getBitmapFromVectorDrawable(this,R.drawable.ic_002_notes));
            if(garbegeEventPojo.get(0).getAccounting().getPaidByWallet()==0){
                tvCompleteCard.setText(garbegeEventPojo.get(0).getPaymentMethod());
            }else{
                tvCompleteCard.setText(garbegeEventPojo.get(0).getPaymentMethod()+" + "+"wallet");
            }

        }else {
            ivCompleteCardLogo.setImageBitmap(Utilities.setCreditCardLogo(garbegeEventPojo.get(0).getAccounting().getBrand(),this));
            String cardEndWith = garbegeEventPojo.get(0).getPaymentMethod() + " "+getString(R.string.endsWith)+" "+garbegeEventPojo.get(0).getAccounting().getLast4();
            if(garbegeEventPojo.get(0).getAccounting().getPaidByWallet()==0){
                tvCompleteCard.setText(cardEndWith);
            }else{
                tvCompleteCard.setText(cardEndWith+" + "+"wallet");
            }
        }
        if(garbegeEventPojo.get(0).getBookingModel()==2){
            ll_cancel_profile.setVisibility(View.VISIBLE);
        }else{
            ll_cancel_profile.setVisibility(View.GONE);
        }
        if(garbegeEventPojo.get(0).getAccounting().getVisitFee()>0){
            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getVisitFee(),tvVisitFeeAmt,garbegeEventPojo.get(0).getCurrencySymbol());
            rlconfm_visitfee.setVisibility(View.VISIBLE);
        }

        if(status==12)
        {
            rlCancelFee.setVisibility(View.VISIBLE);
            rlBillingTotal.setVisibility(View.VISIBLE);
            String totalFee = currencySymbol+" "+garbegeEventPojo.get(0).getAccounting().getCancellationFee();// Cancel fee
            tvCancelAmount.setText(totalFee);

            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getTotal(),tvCancelTotalAmt1,garbegeEventPojo.get(0).getCurrencySymbol());
            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getCart().getTotalAmount(),tvTotalAmt,garbegeEventPojo.get(0).getCurrencySymbol());
            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getTotal(),tvBillingTotalAmt,garbegeEventPojo.get(0).getCurrencySymbol());
        }
        else
        {
            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getAmount(),tvCancelTotalAmt1,garbegeEventPojo.get(0).getCurrencySymbol());
        }
        if(status==5 || status==4){
            rlCancelLayout.setVisibility(View.GONE);
            llDistancelayout.setVisibility(View.GONE);
        }

        tvCancelReasons.setText(garbegeEventPojo.get(0).getStatusMsg());
        tvCancellationReason.setText(garbegeEventPojo.get(0).getCancellationReason());
        if(garbegeEventPojo.get(0).getAccounting().getLastDues()>0){
            rlLastdues.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getLastDues(),tvLastDuesAmt,currencySymbol);
        }
    }

    private void spannableString(TextView tvCancelEventStatus) {
        SpannableString spannableString = new SpannableString("Event Status : "+garbegeEventPojo.get(0).getStatusMsg());
        ForegroundColorSpan foregroundSpan;
        if(status==4)
        {
            tvCancellationReasons.setText("");
            foregroundSpan = new ForegroundColorSpan(getResources().getColor(R.color.red_login));
            tvCancelReasons.setBackgroundColor(getResources().getColor(R.color.red_login));
            tvCancellationReason.setTextColor(getResources().getColor(R.color.red_login));
        }
        else if(status==5)
        {
            tvCancellationReasons.setText("");
            foregroundSpan = new ForegroundColorSpan(getResources().getColor(R.color.safron));
            tvCancelReasons.setBackgroundColor(getResources().getColor(R.color.safron));
            tvCancellationReason.setTextColor(getResources().getColor(R.color.safron));
        }
        else if(status==11)
        {
            foregroundSpan = new ForegroundColorSpan(getResources().getColor(R.color.livemblue3498));
            tvCancelReasons.setBackgroundColor(getResources().getColor(R.color.livemblue3498));
            tvCancellationReason.setTextColor(getResources().getColor(R.color.livemblue3498));
        }
        else
        {
            foregroundSpan = new ForegroundColorSpan(getResources().getColor(R.color.livemblue3498));
            tvCancelReasons.setBackgroundColor(getResources().getColor(R.color.livemblue3498));
            tvCancellationReason.setTextColor(getResources().getColor(R.color.livemblue3498));
        }
        spannableString.setSpan(foregroundSpan, 15, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvCancelEventStatus.setText(spannableString);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
        overridePendingTransition(R.anim.stay_still, R.anim.side_slide_in);
        finish();
    }
    private void setPaymentBreakdown(CartData cartData) {
        int size=cartData.getCheckOutItem().size();
        for(int i=0;i<size;i++){
            Item item=cartData.getCheckOutItem().get(i);
            View infltedView = LayoutInflater.from(this).inflate(R.layout.singleserviceprice_pending, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit;
            if(garbegeEventPojo.get(0).getAccounting().getServiceType()==2){
                int accJobhr= (int) (garbegeEventPojo.get(0).getAccounting().getTotalActualJobTimeMinutes()/60);
                int accJobMin= (int) (garbegeEventPojo.get(0).getAccounting().getTotalActualJobTimeMinutes()%60);
                String actual_hrs;
                if(accJobMin>0) {
                    actual_hrs = accJobhr + " hr " + accJobMin + " min";
                }else{
                    actual_hrs = accJobhr + " hr " ;
                }
                namePerUnit=item.getServiceName() + " (x" + actual_hrs+")";
                Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getTotalActualHourFee(),tvServicePrice,cartData.getCurrencySymbol());
            }else{
                if(item.getServiceName().trim().isEmpty()){
                    namePerUnit="Hourly"+" (x" + item.getQuntity()+")";
                }else{
                    namePerUnit = item.getServiceName() + " (x" + item.getQuntity()+")";
                }
                Utilities.setAmtOnRecept(item.getAmount(),tvServicePrice,cartData.getCurrencySymbol());
            }
            tvServiceName.setText(namePerUnit);
            tvServiceName.setTypeface(AppTypeface.getInstance(this).getHind_regular());
            tvServicePrice.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        }
        if(garbegeEventPojo.get(0).getStatus()!=12){
            Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getTotal(),tvTotalAmt,cartData.getCurrencySymbol());
        }

        Utilities.setAmtOnRecept(garbegeEventPojo.get(0).getAccounting().getCancellationFee(),tvCancelAmount,cartData.getCurrencySymbol());
    }
}
