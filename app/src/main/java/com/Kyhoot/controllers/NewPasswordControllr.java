package com.Kyhoot.controllers;

import android.content.Context;
import android.support.design.widget.TextInputLayout;

import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.LoginResponceIntr;
import com.Kyhoot.models.PasswordHelpModel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.Validator;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>NewPasswordControllr</h>
 * Created by Ali on 9/15/2017.
 */

public class NewPasswordControllr
{
    private Context mcontext;
    private AlertProgress aprogress;
    public NewPasswordControllr(Context mcontext)
    {
        this.mcontext = mcontext;
        aprogress = new AlertProgress(mcontext);
    }

    public void checkPawwordValidty(TextInputLayout passwordlay, TextInputLayout reEnterPaswordlay, CheckNetworkAvailablity chkNetwok)
    {
        Validator validator = new Validator();
        String password = passwordlay.getEditText().getText().toString().trim();
        String reEnterPasword = reEnterPaswordlay.getEditText().getText().toString().trim();
        if(!password.equals(""))
        {
            if (!reEnterPasword.equals(""))
           {
               if(validator.passStatus(reEnterPasword.trim()))
               {
                   if(password.equals(reEnterPasword))
                   {
                       if(new AlertProgress(mcontext).isNetworkAvailable())
                           chkNetwok.networkAvalble(password,true);
                       else
                           chkNetwok.notNetworkAvalble();
                   }
                   else
                       aprogress.alertinfo(mcontext.getString(R.string.passwordNotMatchin));
               }
               else
                   aprogress.alertinfo(mcontext.getString(R.string.passwordcontain));

           }else
               aprogress.alertinfo(mcontext.getString(R.string.repasswordiaMandetory));
        }
        else
            aprogress.alertinfo(mcontext.getString(R.string.password_mandatory));
    }

    public void chengePasswordService(String emailOrPhone, String userId, LoginResponceIntr lginres)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("password",emailOrPhone);
            jsonObject.put("userId", userId);
            jsonObject.put("userType",1);
            new PasswordHelpModel().newPassword(jsonObject,lginres);
        } catch (JSONException e) {
            e.printStackTrace();
            lginres.onError(String.valueOf(e));
        }


    }

}
