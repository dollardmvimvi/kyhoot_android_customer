package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>AddressPojo</h>
 * Created by 3Embed on 10/19/2017.
 */

public class AddressPojo implements Serializable {
    ArrayList<AddressData> data;
    private String message;

    public String getMessage() {
        return message;
    }

    public ArrayList<AddressData> getData() {
        return data;
    }

    public class AddressData {
        /* "_id": "59e861b058eaf47f908ebefa",
      "addLine1": "52, 8th Cross Road, Kempanna Layout, Cholanayakanahalli, Hebbal, Bengaluru, Karnataka 560032, India",
      "addLine2": "SBI",
      "city": "",
      "state": "",
      "country": "",
      "placeId": "",
      "pincode": "",
      "latitude": 13.0372608578232,
      "longitude": 77.59707625955343,
      "taggedAs": "Home",
      "userType": 1,
      "user_Id": "59ce5553b121331d381cad8c"*/
        private String _id, addLine1, addLine2, city, state, country, placeId, pincode, latitude, longitude, taggedAs, userType, user_Id;

        public String get_id() {
            return _id;
        }

        public String getAddLine1() {
            return addLine1;
        }

        public String getAddLine2() {
            return addLine2;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public String getCountry() {
            return country;
        }

        public String getPlaceId() {
            return placeId;
        }

        public String getPincode() {
            return pincode;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public String getTaggedAs() {
            return taggedAs;
        }

        public String getUserType() {
            return userType;
        }

        public String getUser_Id() {
            return user_Id;
        }
    }
}
