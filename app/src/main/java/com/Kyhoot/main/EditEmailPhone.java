package com.Kyhoot.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.controllers.PasswordHelpController;
import com.Kyhoot.countrypic.Country;
import com.Kyhoot.countrypic.CountryPicker;
import com.Kyhoot.countrypic.CountryPickerListener;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;

/**
 * <h>EditEmailPhone</h>
 * <p>
 * Edit the Email of the user which is showing in the profile fragment
 * </p>
 */
public class EditEmailPhone extends AppCompatActivity implements View.OnClickListener, CheckNetworkAvailablity, VerifyOtpIntrface {

    private boolean isEmail;
    private LinearLayout llEditEmail;
    private RelativeLayout rlEditMob;
    private EditText etEidtMobileNumber, etEditEmail;
    private AppTypeface appTypeface;
    private PasswordHelpController controller;
    private ProgressBar progressDialog;
    private SharedPrefs sprefs;
    private ImageView ivEditCountryFlg;
    private TextView tvEditCountryCode;
    private CountryPicker mCountryPicker;
    private String phoneEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email_phone);
        appTypeface = AppTypeface.getInstance(this);
        sprefs = new SharedPrefs(this);
        initializeView();
        initializeToolBar();

    }

    /*
    initializing view of the activity
     */
    private void initializeView() {
        etEidtMobileNumber = findViewById(R.id.etEidtMobileNumber);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));
        progressDialog = findViewById(R.id.progressDialog);
        controller = new PasswordHelpController(this, this, this);
        if (getIntent().getExtras() != null) {
            isEmail = getIntent().getBooleanExtra("ISEMAIL", false);
        }
        TextView tvEditSave = findViewById(R.id.tvEditSave);
        tvEditCountryCode = findViewById(R.id.tvEditCountryCode);
        ivEditCountryFlg = findViewById(R.id.ivEditCountryFlg);
        llEditEmail = findViewById(R.id.llEditEmail);
        rlEditMob = findViewById(R.id.rlEditMob);

        tvEditSave.setOnClickListener(this);
        tvEditSave.setTypeface(appTypeface.getHind_semiBold());

        initializeCountryCode();
    }

    private void initializeCountryCode() {

        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID,int maxDigit) {

                tvEditCountryCode.setText(dialCode);
                ivEditCountryFlg.setImageResource(flagDrawableResID);
                //etEidtMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxDigit)});

                mCountryPicker.dismiss();
            }
        });
        tvEditCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker));
            }
        });

        Country country = mCountryPicker.getUserCountryInfo(this);
        if(country!=null)
        {
            ivEditCountryFlg.setImageResource(country.getFlag());
            tvEditCountryCode.setText(country.getDialCode());
            //etEidtMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(country.getMax_digits())});

        }

    }

    /*
    initializing tool bar
     */
    private void initializeToolBar() {

        Toolbar login_tool = findViewById(R.id.toolEditEmailPhone);
        setSupportActionBar(login_tool);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setTypeface(appTypeface.getHind_semiBold());

        etEditEmail = findViewById(R.id.etEditEmail);
        TextView tveditMobInfo = findViewById(R.id.tveditMobInfo);
        if (isEmail) {
            TextView tvEditProflInfo = findViewById(R.id.tvEditProflInfo);
            tvEditProflInfo.setTypeface(appTypeface.getHind_regular());
            etEditEmail.setTypeface(appTypeface.getHind_regular());
            llEditEmail.setVisibility(View.VISIBLE);
            rlEditMob.setVisibility(View.GONE);
            tv_center.setText(R.string.changeEmail);
            tveditMobInfo.setVisibility(View.GONE);
            etEditEmail.requestFocus();
        } else {
            tveditMobInfo.setVisibility(View.VISIBLE);
            tveditMobInfo.setTypeface(appTypeface.getHind_regular());
            etEidtMobileNumber.setTypeface(appTypeface.getHind_regular());
            etEidtMobileNumber.setBackgroundColor(Color.TRANSPARENT);
            llEditEmail.setVisibility(View.GONE);
            rlEditMob.setVisibility(View.VISIBLE);
            tv_center.setText(R.string.changePhone);
            etEidtMobileNumber.requestFocus();
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tvEditSave) {

            //showHideProgress(true);
            String email = etEditEmail.getText().toString();
            String phone = etEidtMobileNumber.getText().toString();
            String countrycode=tvEditCountryCode.getText().toString();
            controller.checkEmailPhoneValid(isEmail, email,countrycode, phone, etEditEmail, etEidtMobileNumber);
        }
    }

    @Override
    public void networkAvalble(LoginPojo loginPojo) {

    }

    @Override
    public void networkAvalble(String emailOrPhone, boolean isEmailOrPhone) {
        showHideProgress(true);
        phoneEmail = emailOrPhone;
        if (isEmailOrPhone)
            controller.changeEmail(emailOrPhone, sprefs);
        else
            controller.changePhoneNumber(emailOrPhone, sprefs, tvEditCountryCode.getText().toString());

    }

    @Override
    public void notNetworkAvalble() {
        showHideProgress(false);
    }

    @Override
    public void onOtpVerify(String success) {


        showHideProgress(false);
        Intent intent = new Intent(EditEmailPhone.this, ConfirmOTP.class);
        Bundle bundle = new Bundle();
        bundle.putString("mobile", phoneEmail);
        bundle.putString("countryCode", tvEditCountryCode.getText().toString());
        bundle.putString("SignupOrForgot", "PROFILE");
        bundle.putString("SID", success);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    @Override
    public void onOtpError(String error)
    {
        Log.d("TRUE", "onOtpError: "+error);
        showHideProgress(false);
        new AlertProgress(this).alertinfo(error);
        Log.d("TRUE", "onOtpError1: "+error);
    }

    @Override
    public void onSignUpSuccess(String msg) {
        showHideProgress(false);
        finish();
    }

    void showHideProgress(boolean isShowing) {
        if (isShowing) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressDialog.setVisibility(View.VISIBLE);
        } else {
            progressDialog.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
    @Override
    public void onPhoneNumError(){
        showHideProgress(false);
    }
}
