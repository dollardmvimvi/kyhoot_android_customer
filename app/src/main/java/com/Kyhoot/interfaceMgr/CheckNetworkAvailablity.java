package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.LoginPojo;

/**
 * <h>CheckNetworkAvailablity</h>
 * Created by Ali on 9/14/2017.
 */

public interface CheckNetworkAvailablity
{
    void networkAvalble(LoginPojo loginPojo);
    void networkAvalble(String emailOrPhone, boolean isEmailOrPhone);
    void notNetworkAvalble();
}
