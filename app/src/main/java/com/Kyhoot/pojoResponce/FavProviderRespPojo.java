package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 18/6/18.
 */

public class FavProviderRespPojo
{
    private String message;

    private ArrayList<FavProRespData> data;

    public String getMessage ()
    {
        return message;
    }
    public ArrayList<FavProRespData> getData ()
    {
        return data;
    }
}