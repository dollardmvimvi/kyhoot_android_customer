package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.controllers.ScheduleController;
import com.Kyhoot.main.ScheduleActivity;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.ScheduleMonthPojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

public class ScheduleModel
{
    private Gson gson;
    private SharedPrefs sPrefs;
    private ScheduleActivity scheduleActivity;

    public ScheduleModel(ScheduleController scheduleController, ScheduleActivity scheduleActivity)
    {
        gson = new Gson();
        this.scheduleActivity = scheduleActivity;
    }


    public void getShedule(String date, SharedPrefs sPrefs)
    {
        this.sPrefs=sPrefs;
        JSONObject jsonObject = new JSONObject();



        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "schedule/"
                        + date+ "/" + VariableConstant.providerId
                , OkHttpConnection.Request_type.GET, jsonObject, sPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback()
                {

                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.i("SAITESTING","SUCCESS"+result);

                        ScheduleMonthPojo scheduleMonthPojo = gson.fromJson(result,ScheduleMonthPojo.class);
                        sPrefs.setScheduleData(result);
                        scheduleActivity.onSuccessGetSchedule(scheduleMonthPojo);

                        /*if(isFragmentAttached)
                        {
                            presenterImple.onSuccessGetSchedule(scheduleMonthPojo);
                        }*/

                    }

                    @Override
                    public void onError(String headerError, String error)
                    {
                        scheduleActivity.hideProgress();
                        Log.i("SAITESTING","FAILURE"+error);

                        Log.d("SAITESTING", "onError: headerError "+headerError);

                        if(headerError.equals("502"))
                        {
                            scheduleActivity.onError("Internal server error");
                        }
                        else
                        {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        scheduleActivity.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "549":
                                        scheduleActivity.onError("Socket Exception");
                                        break;
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                                scheduleActivity.onSessionError(e.getMessage());
                            }

                        }
                    }
                });

    }
}
