package com.Kyhoot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.ProviderEvents;
import com.Kyhoot.utilities.AppTypeface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * <h>EventAdapter</h>
 * Created by 3Embed on 10/25/2017.
 */

public class EventAdapter extends RecyclerView.Adapter {
    private Context mcontext;
    private ArrayList<ProviderEvents> eventses;
    private boolean isConfirmBooking;
    private int selectedPosition = -1;
    private UtilInterFace.ConfirmBook confirmBook;

    public EventAdapter(Context mcontext, ArrayList<ProviderEvents> eventses, boolean isConfirmBooking, UtilInterFace.ConfirmBook confirmBook) {
        this.mcontext = mcontext;
        this.isConfirmBooking = isConfirmBooking;
        if (isConfirmBooking)
            selectedPosition = 0;
        this.confirmBook = confirmBook;
        this.eventses = eventses;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.event_adapter, parent, false);
        return new ViewHoldr(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHoldr hldr = (ViewHoldr) holder;

        hldr.tvEventService.setText(eventses.get(position).getName());

        if (hldr.isFirstTIme) {
            downloadImage(hldr.ivEventService, eventses.get(position).getSelectImage());
            downloadImage(hldr.ivEventService, eventses.get(position).getUnselectImage());
            hldr.isFirstTIme = false;
        }
        if (position == selectedPosition) {
            if (confirmBook != null)
                confirmBook.onEventSelected(eventses.get(position).get_id());
            hldr.tvEventService.setSelected(true);
            downloadImage(hldr.ivEventService, eventses.get(position).getSelectImage());
        } else {
            hldr.tvEventService.setSelected(false);
            downloadImage(hldr.ivEventService, eventses.get(position).getUnselectImage());
        }

    }

    private void downloadImage(ImageView ivEventService, String unselectImage) {
        if (!unselectImage.equals("")) {
            Picasso.with(mcontext)
                    .load(unselectImage)
                    .into(ivEventService);
        }

    }

    @Override
    public int getItemCount() {
        return eventses.size();
    }

    private class ViewHoldr extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivEventService;
        private TextView tvEventService;
        private boolean isFirstTIme;

        public ViewHoldr(View itemView) {
            super(itemView);

            if (isConfirmBooking)
                itemView.setOnClickListener(this);
            isFirstTIme = true;
            ivEventService = itemView.findViewById(R.id.ivEventService);
            tvEventService = itemView.findViewById(R.id.tvEventService);
            AppTypeface appTypeface = AppTypeface.getInstance(mcontext);
            tvEventService.setTypeface(appTypeface.getHind_regular());
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selectedPosition);
            selectedPosition = getAdapterPosition();
            notifyItemChanged(selectedPosition);
        }
    }
}
