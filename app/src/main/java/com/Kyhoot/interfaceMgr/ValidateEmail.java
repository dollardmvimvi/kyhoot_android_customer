package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 15/5/18.
 */
public interface ValidateEmail {
    public void onSuccessEmailValidate(String headerResponse, String result);
    public void onFailureEmailValidate(String headerError, String server_issue);
    public void onLoginSuccess();
    public void onLoginFailure(String headerResponse);
}
