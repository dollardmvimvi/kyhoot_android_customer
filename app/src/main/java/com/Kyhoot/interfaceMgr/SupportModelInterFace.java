package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.Support_pojo;

/**
 * <h>SupportModelInterFace</h>
 * Created by Ali on 10/5/2017.
 */

public interface SupportModelInterFace {
    void onError(String errormsg);

    void onSupportSuccess(Support_pojo support_pojo);
}
