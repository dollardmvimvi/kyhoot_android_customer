package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 9/3/18.
 */

public class PromocodeData {
    private String promoId;

    private String cartValue;

    private double discountAmount;

    private String deliveryFee;

    public String getPromoId ()
    {
        return promoId;
    }

    public void setPromoId (String promoId)
    {
        this.promoId = promoId;
    }

    public String getCartValue ()
    {
        return cartValue;
    }

    public void setCartValue (String cartValue)
    {
        this.cartValue = cartValue;
    }

    public double getDiscountAmount ()
    {
        return discountAmount;
    }

    public void setDiscountAmount (double discountAmount)
    {
        this.discountAmount = discountAmount;
    }

    public String getDeliveryFee ()
    {
        return deliveryFee;
    }

    public void setDeliveryFee (String deliveryFee)
    {
        this.deliveryFee = deliveryFee;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [promoId = "+promoId+", cartValue = "+cartValue+", discountAmount = "+discountAmount+", deliveryFee = "+deliveryFee+"]";
    }
}
