package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>GetCard_pojo</h>
 * this bean takes the user card information like last four digit card number,expiry date etc
 * Created by embed on 25/11/15.
 */
public class GetCard_pojo implements Serializable {

    private Integer errNum;

    private String errMsg;
    private Integer errFlag;


    private Get_all_cards[] data;


    public Integer getErrNum() {
        return errNum;
    }

    public void setErrNum(Integer errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Integer getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(Integer errFlag) {
        this.errFlag = errFlag;
    }

    public Get_all_cards[] getData() {
        return data;
    }

    public void setData(Get_all_cards[] cards) {
        this.data = cards;
    }
}
