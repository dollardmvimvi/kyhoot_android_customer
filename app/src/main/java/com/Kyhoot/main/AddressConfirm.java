package com.Kyhoot.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.Address_Adaptor;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * <h>AddressConfirm</h>
 * Created by 3Embed on 11/11/2017.
 */

public class AddressConfirm extends AppCompatActivity {
    TextView addaddress, noadressadded;
    AppTypeface textfont;
    ListView addresslist;
    Address_Adaptor address_adaptor;
    ProgressDialog pdialog;
    SharedPrefs manager;
    Gson gson;
    String TAG = "Address_Confirm";
   private int position;
    private ArrayList<AddressPojo.AddressData> saving_addresses = new ArrayList<>();
    private double place_lat,place_long;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.address_confirm);
        textfont = AppTypeface.getInstance(this);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        manager = new SharedPrefs(this);
        gson = new Gson();
        pdialog = new ProgressDialog(this);
        pdialog.setMessage(getResources().getString(R.string.wait));
        pdialog.setCancelable(false);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addresslist = findViewById(R.id.addresslist);
        addaddress = findViewById(R.id.addaddress);
        noadressadded = findViewById(R.id.noadressadded);
        TextView tv_center = findViewById(R.id.tv_center);
        tv_center.setText(getResources().getString(R.string.address));
        tv_center.setTypeface(textfont.getHind_semiBold());
        address_adaptor = new Address_Adaptor(this, saving_addresses, AddressConfirm.this, true);
        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressConfirm.this, CurrentLocation.class);
                startActivity(intent);
            }
        });
        addresslist.setAdapter(address_adaptor);
        addaddress.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
    }

    @Override
    public void onResume() {
        super.onResume();

        callgetAddress();
    }

    public void callgetAddress() {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "address", OkHttpConnection.Request_type.GET,
                new JSONObject(), manager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String responseCode, String result) {

                        if (responseCode.equals("200")) {
                            Log.d("TAG", "onSuccess: " + result);
                            callresponceMethod(result);
                        }

                    }

                    @Override
                    public void onError(String header, String error) {
                        Toast.makeText(AddressConfirm.this, "", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callresponceMethod(String result) {
        AddressPojo addresPojo = new Gson().fromJson(result, AddressPojo.class);
        if (addresPojo.getData().size() > 0) {
            saving_addresses.clear();

            noadressadded.setVisibility(View.GONE);
            saving_addresses.addAll(addresPojo.getData());
            address_adaptor.notifyDataSetChanged();
        } else {
            noadressadded.setVisibility(View.VISIBLE);
            address_adaptor.notifyDataSetChanged();
        }
    }

    public void backaddress(int place) {

        try {
            VariableConstant.SELECTED_ADDRESS_POSITION =place;
            Geocoder geocoder = new Geocoder(this);
            List<Address> addressList = geocoder.getFromLocationName(saving_addresses.get(place).getAddLine1(), 5);
            place_lat = addressList.get(0).getLatitude();
            place_long = addressList.get(0).getLongitude();
            Log.e(TAG, "backaddress:  Place_lat "+place_lat+ "  Place_long  "+place_long);
        } catch (Exception e) {
            Log.e(TAG, "backaddress: Exception  "+e.getMessage() );
            e.printStackTrace();
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("AREA", saving_addresses.get(place).getAddLine1());
        returnIntent.putExtra("CITY", saving_addresses.get(place).getCity());
        if (!TextUtils.isEmpty(saving_addresses.get(place).getLatitude())) {
            returnIntent.putExtra("LATITUDE", saving_addresses.get(place).getLatitude());
        } else {
            Log.e(TAG, "backaddress: Set latitude "+place_lat);
            returnIntent.putExtra("LATITUDE", ""+place_lat);
        }
        if (!TextUtils.isEmpty(saving_addresses.get(place).getLongitude())) {
            returnIntent.putExtra("LONGITUDE", saving_addresses.get(place).getLongitude());
        } else {
            Log.e(TAG, "backaddress: Set longitude  " +place_long);
            returnIntent.putExtra("LONGITUDE", ""+place_long);
        }
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    /**
     * Delete the Address from the DataBase
     *
     * @param positiondelete integer type delete the address at a particular position
     */
    public void deletefromdb(final int positiondelete) {
       position = positiondelete;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.deleteaddress));
        builder.setMessage(getResources().getString(R.string.doyouwishtodelete));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(pdialog!=null)
                {
                    pdialog.show();
                }
                   deleteaddress(saving_addresses.get(positiondelete).get_id());
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void deleteaddress(final String id)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "address/" + id,
                OkHttpConnection.Request_type.DELETE, new JSONObject(), manager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String result) {

                        if (headerResponse.equals("200")) {
                            if (result != null) {

                                Log.d(TAG, "onSuccess: "+id);
                                int index = saving_addresses.indexOf(id);
                                Log.d(TAG, "onSuccess: "+position);
                                saving_addresses.remove(position);
                                address_adaptor.notifyDataSetChanged();
                                if(saving_addresses.size()>0)
                                    address_adaptor.notifyDataSetChanged();
                                else
                                {
                                    address_adaptor.notifyDataSetChanged();
                                    noadressadded.setVisibility(View.VISIBLE);
                                }
                                pdialog.dismiss();

                            }
                        }

                    }

                    @Override
                    public void onError(String header, String error) {

                      //  utilIntrFace.hideProgress();
                    }
                });
    }

}