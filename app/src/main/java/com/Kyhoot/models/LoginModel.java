package com.Kyhoot.models;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.BuildConfig;
import com.Kyhoot.interfaceMgr.LoginResponceIntr;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.Validator;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>LoginModel</h>
 * Created by ${Ali} on 8/22/2017.
 */

public class LoginModel
{

    private Validator validator;
    private String TAG = "LoginModel";
    private String EditTextValu;
    private String signUpType = "";
    String emailPattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    public LoginModel() {
        validator = new Validator();
    }

    /**
     * Represents an signinService login/registration task used to authenticate
     * the user.
     */
    public void signinService(final LoginPojo loginPojo, final LoginResponceIntr lgIntr, final SharedPrefs sharedPrefs)
    {
        if(loginPojo.getData().getLoginType()==1)
        {
            signUpType = "Email";
        }else
            signUpType = "Facebook";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailOrPhone",loginPojo.getData().getEmail());
            jsonObject.put("password",loginPojo.getData().getPassword());
            jsonObject.put("deviceId", sharedPrefs.getDeviceId());
            jsonObject.put("pushToken", sharedPrefs.getRegistrationId());
            jsonObject.put("appVersion", BuildConfig.VERSION_NAME);
            jsonObject.put("devMake", Build.BRAND);//device os
            jsonObject.put("devModel",Build.MODEL);
            jsonObject.put("devType",2);
            jsonObject.put("deviceTime", Utilities.dateintwtfour());
            jsonObject.put("loginType",loginPojo.getData().getLoginType());
            jsonObject.put("facebookId",loginPojo.getData().getFbId());
            jsonObject.put("deviceOsVersion",Build.VERSION.RELEASE);
            jsonObject.put("latitude", sharedPrefs.getSplashLatitude());
            jsonObject.put("longitude", sharedPrefs.getSplashLongitude());
            Log.d(TAG, "signinService: "+jsonObject);
            sharedPrefs.setOldpassword(loginPojo.getData().getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "signIn", OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result)
            {
                Log.d(TAG, "LOginonSuccess: " + result + " HeaderResponce " + headerResponse);
                try {
                    if (headerResponse.equals("200")) {
                        LoginPojo lPojo;
                        Gson gson = new Gson();

                        lPojo = gson.fromJson(result, LoginPojo.class);
                        JSONObject props = new JSONObject();
                        props.put("Name", lPojo.getData().getFirstName() + " " + lPojo.getData().getLastName());
                        props.put("Email", lPojo.getData().getEmail());
                        props.put("UserId", lPojo.getData().getSid());
                        props.put("date", Utilities.dateintwtfour());
                        props.put("SignUpMethod", signUpType);
                        props.put("Phone", lPojo.getData().getCountryCode()+""+lPojo.getData().getPhone());
                        props.put("DOB",lPojo.getData().getDatOB());
                        if(lPojo.getData().getCardDetail()!=null && lPojo.getData().getCardDetail().getLast4()!=null)
                            props.put("CreditCard","Complete");
                        else
                            props.put("CreditCard","Not complete");
                       // callGetProvider(sharedPrefs,lPojo.getData().getToken());
                        sharedPrefs.setCustomerSid(lPojo.getData().getSid());
                        sharedPrefs.setSession(lPojo.getData().getToken());
                        sharedPrefs.setRequesterId(lPojo.getData().getRequester_id());
                        sharedPrefs.setIsLogin(true);
                       /* if(!ApplicationController.getInstance().isMqttConnected())
                            ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());*/
                        sharedPrefs.setImageUrl(lPojo.getData().getProfilePic());
                        sharedPrefs.setEMail(lPojo.getData().getEmail());
                        sharedPrefs.setFirstname(lPojo.getData().getFirstName());
                        sharedPrefs.setLastname(lPojo.getData().getLastName());
                        sharedPrefs.setPassword(lPojo.getData().getPassword());
                        sharedPrefs.setMobileNo(lPojo.getData().getPhone());
                        sharedPrefs.setCountryCode(lPojo.getData().getCountryCode());
                        sharedPrefs.setPaymentId(lPojo.getData().getPublishableKey());
                        sharedPrefs.setReferral(lPojo.getData().getReferralcode());
                        sharedPrefs.setFcmTopic(lPojo.getData().getFcmTopic()); //FCM TOPIC which is used to get push topic
                        Log.d(TAG, "onSuccess: "+lPojo.getData().getCardDetail() +" detailsPojo "+lPojo.getData().getCardDetail().getLast4());
                        if(lPojo.getData().getCardDetail()!=null && lPojo.getData().getCardDetail().getLast4()!=null)
                        {
                            String card = "**** **** **** " + " " + lPojo.getData().getCardDetail().getLast4();
                            sharedPrefs.setDefaultCardNum(lPojo.getData().getCardDetail().getLast4());
                            sharedPrefs.setDefaultCardBrand(lPojo.getData().getCardDetail().getBrand());
                            sharedPrefs.setDefaultCardId(lPojo.getData().getCardDetail().getId());
                        }

                        lgIntr.onSuccess();
                    } else {
                        lgIntr.onError(headerResponse);
                    }

                } catch (Exception e) {
                    lgIntr.onError(e.toString());
                }


            }

            @Override
            public void onError(String headerError, String error) {
                Log.d(TAG, "onError: " + error + " header " + headerError);

                if(headerError.equals("502")||headerError.equals("549") ) {
                    lgIntr.onError("server down");
                }else {
                    Gson gson = new Gson();
                    ErrorHandel lPojo=null;
                    try{
                        lPojo = gson.fromJson(error, ErrorHandel.class);
                        if (headerError.equals("404")) {
                            //  createBundle(loginPojo);
                            if (loginPojo.getData().getLoginType() != 1)
                                lgIntr.onError(lPojo.getMessage(), createBundle(loginPojo));
                            else
                                lgIntr.onError(lPojo.getMessage());
                        } else {
                            lgIntr.onError(lPojo.getMessage());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        lgIntr.onError(e.getMessage());
                    }
                }



            }
        });
    }

    private Bundle createBundle(LoginPojo loginPojo) {

        Bundle bundle = new Bundle();
        bundle.putString("FNAME", loginPojo.getData().getFirstName());
        bundle.putString("LNAME", loginPojo.getData().getLastName());
        bundle.putString("EMAIL", loginPojo.getData().getEmail());
        bundle.putString("FBID", loginPojo.getData().getFbId());
        bundle.putString("DATEOFBIRTH", loginPojo.getData().getDatOB());
        return bundle;
    }

    /**
     * <h1>validatemobileNo</h1>
     * this IS CHECKING THE VALIDITY OF THE PHONE NUMBER, if it is true
     *
     * @param mobileNumberWithoutZero Phone number
     * @param checkValidity             interface to check the validity and send response to the view
     * @param mcontext                ActivityContext
     * @return true
     */
    public boolean validateMobileNo(String mobileNumberWithoutZero, LoginResponceIntr.CheckValidty checkValidity, Activity mcontext) {
        boolean emailphoneflag;
        if (!validator.contactStatus(mobileNumberWithoutZero)) {
            checkValidity.onPhoneError();
            emailphoneflag = false;
        } else {
            EditTextValu = mobileNumberWithoutZero;
            /*emtxtlayout.setError(null);
            emtxtlayout.setErrorEnabled(false);*/
            checkValidity.onEmailValidSuccess();
            emailphoneflag = true;
        }
        return emailphoneflag;
    }

    public boolean isEmailValid(String email,LoginResponceIntr.CheckValidty checkValidity, Activity mcontext) {
        EditTextValu = email;
        boolean emailphoneflag = false;

        //if (!EditTextValu.equals("")) {
        String mobileNumberWithoutZero;
        if (EditTextValu.charAt(0) == '+') {
            mobileNumberWithoutZero = EditTextValu.substring(3);
            emailphoneflag = validateMobileNo(mobileNumberWithoutZero, checkValidity, mcontext);
        } else if (PhoneNumberUtils.isGlobalPhoneNumber(email)) {//Character.isDigit(EditTextValu.charAt(0))
            if (EditTextValu.charAt(0) == '0')
                mobileNumberWithoutZero = EditTextValu.substring(1);
            else
                mobileNumberWithoutZero = EditTextValu;
            emailphoneflag = validateMobileNo(mobileNumberWithoutZero, checkValidity, mcontext);

        } else if (email.matches(emailPattern)) {
            checkValidity.onEmailValidSuccess();
            emailphoneflag = true;
        } else {
            checkValidity.emailValid();
        }
      /*  } else {

            checkValidity.onEnterEmailPhone();
            emailphoneflag = false;
        }*/
        return emailphoneflag;
    }

    private void callGetProvider(final SharedPrefs sharedPrefs, String token) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "provider/" + sharedPrefs.getSplashLatitude() + "/" + sharedPrefs.getSplashLongitude(),
                OkHttpConnection.Request_type.GET, new JSONObject(), token, VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        if(!result.contains("Unable to resolve host"))
                        {
                            sharedPrefs.setProviderData(result);
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }
}
