package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 12/5/18.
 */
public class MqttProviderData {
    private String categoryName;

    private String categoryId;

    private CategoryData categoryData;

    private ArrayList<MqttProviderDetails> providerList;

    public String getCategoryName ()
    {
        return categoryName;
    }

    public void setCategoryName (String categoryName)
    {
        this.categoryName = categoryName;
    }

    public String getCategoryId ()
    {
        return categoryId;
    }

    public void setCategoryData(CategoryData categoryData) {
        this.categoryData = categoryData;
    }

    public CategoryData getCategoryData() {
        return categoryData;
    }

    public void setCategoryId (String categoryId)
    {
        this.categoryId = categoryId;
    }

    public ArrayList<MqttProviderDetails> getProviderList ()
    {
        return providerList;
    }

    public void setProviderList (ArrayList<MqttProviderDetails> providerList)
    {
        this.providerList = providerList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [categoryName = "+categoryName+", categoryId = "+categoryId+", providerList = "+providerList+"]";
    }
}
