package com.Kyhoot.main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LayoutAnimationController;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateTimeDialogReturn;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.Kyhoot.Dialog.BottomsheetFragmentFavProvider;
import com.Kyhoot.Dialog.FragmentLaterBookingSteps;
import com.Kyhoot.R;
import com.Kyhoot.adapter.CategoryAdapter;
import com.Kyhoot.adapter.HomeListAdaptr;
import com.Kyhoot.controllers.HomeFragmentController;
import com.Kyhoot.interfaceMgr.CategoryUpdateInterface;
import com.Kyhoot.interfaceMgr.ETAResultInterface;
import com.Kyhoot.interfaceMgr.HomeModelIntrface;
import com.Kyhoot.interfaceMgr.LaterBookingCallback;
import com.Kyhoot.interfaceMgr.OnFavoriteSelected;
import com.Kyhoot.models.HomeFragmentModel;
import com.Kyhoot.pojoResponce.CategoriesPojo;
import com.Kyhoot.pojoResponce.CategoryData;
import com.Kyhoot.pojoResponce.DistanceMatrixPojo;
import com.Kyhoot.pojoResponce.Elements;
import com.Kyhoot.pojoResponce.MqttProviderData;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.pojoResponce.MqttProvidrResp;
import com.Kyhoot.pojoResponce.WalletData;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.CustomTypefaceSpan;
import com.Kyhoot.utilities.HorizontalCatItemDecorator;
import com.Kyhoot.utilities.LocationUtil;
import com.Kyhoot.utilities.MqttEvents;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static android.os.Build.VERSION_CODES.M;

/**
 * <h>HomeFragment</h>
 * <p>
 *     this shows all the online and offline users on the map
 * Created by 3Embed on 8/12/2017.
 * </p>
 */

public class HomeFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener
        , HomeModelIntrface, LocationUtil.LocationNotifier,ETAResultInterface,CategoryUpdateInterface,LaterBookingCallback
        ,OnFavoriteSelected {
    private static final String TAG = "HomeFragment";
    public static String FAVPROVIDER="FAVPROVIDER";
    private final int REQUEST_CODE_PERMISSION_LOCATION = 1;
    private final int SERACH_RESULT = 4;
    public SingleDateTimeDialogReturn.Builder singleBuilderReturn;
    FragmentLaterBookingSteps fragmentLaterBookingSteps;
    double width, height;
    View view;
    private MenuActivity mainActivity;
    private Context mcontext;
    private SharedPrefs sharedPrefs;
    private HomeFragmentController hFController;
    public ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionLocationArrayList;
    private TextView tvHomeJobAddress, tvHomeJobAddressList,tvSheduledDateTime,tvHomeNotOperational,
            tvSheduledDateTimeList,tvEta,tv_address_bar,btnPreBook,tv_label_categories;
    private boolean IsReturnFromSearch = false;
    public LocationUtil networkUtilObj;
    private double currentLatLng[] = new double[2];
    private double size[];
    private double currentLatitude, currentLongitude;
    private Timer myTimer_publish, myTimer;
    private AlertProgress aProgres;
    private RelativeLayout rlAbarMainAct,rlScheduledtimeDate,rlScheduledtimeDateList,rlToolbarList,rl_providerListTb;
    private HashMap<String, ArrayList<String>> hashMarker = new HashMap<>();
    private ImageView ivProProfilePic,icHomeClear,icHomeClearList,ivAbarMenuBtn;
    ImageView ivToolbarListMenu;;
    private HomeListAdaptr homeListAdaptr;
    private RecyclerView recyclrViewCat;
    private FrameLayout frameLayoutMap, frameLayoutList;
    private ArrayList<MqttProviderDetails> providerDtls = new ArrayList<>();
    private CardView cardHomeMap,cardHomeCategories;
    private boolean isFirstTime = true;
    private SimpleDateFormat simpleDateFormat;
    private ProgressBar progressbar;
    private ImageView ivProviderListBack,ivCategoriesBack;
    ArrayList<LatLng> latLngList;
    ArrayList<CategoryData> categoryDataArray;
    SupportMapFragment mMapFragment;
    ArrayList<Elements> elements;


    /**
     * The map. It is initialized when the map has been fully loaded and is ready to be used.
     *
     * @see #onMapReady(com.google.android.gms.maps.GoogleMap)
     */
    private GoogleMap mMap;
    private TextView btnBookNotary;
    private Gson gson;
    public static boolean listScreen=false;
    //Removed April 26
    private Handler locatingUpdateHandler;
    private Runnable locationUpdateRunnable;
    public boolean isLocationUpdated=true;
    private TimeZone timeZoneFromLocation;
    private CategoryAdapter categoryAdapterV;
    private CategoryAdapter categoryAdapter;
    private ConstraintLayout clCategorySelector,rl_inner_add_lyt;
    private TextView tvProviderList;
    private DistanceMatrixPojo distanceMatrixPojo;
    public static boolean categoryPage;
    public  boolean listFromBookNow=true;
    public static boolean etaCleared;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeVariableValue();
        initializeObj();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.homefragment, container, false);
        sharedPrefs = new SharedPrefs(getActivity());
        initializeRecyclerView(view);
        initializeView(view);
        isFirstTime = true;
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showProgress();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!VariableConstant.isLocationPermissionGranted){
            chckForPermission();
        }else{
            if(!sharedPrefs.getSplashLatitude().isEmpty()){
                currentLatLng[0]=Double.parseDouble(sharedPrefs.getSplashLatitude());
                currentLatLng[1]=Double.parseDouble(sharedPrefs.getSplashLongitude());
            }
        }
    }

    /*
            initializing objects
             */
    private void initializeObj()
    {
        VariableConstant.infltdCnt = 0; //For Mqtt Responce to inflate on the Map
        mcontext = getActivity();
        mainActivity = (MenuActivity) getActivity();
        sharedPrefs = new SharedPrefs(getActivity());
        aProgres = new AlertProgress(mcontext);
        size = Scaler.getScalingFactor(mcontext);
        gson= new Gson();
        latLngList = new ArrayList<>();
        Log.d("HOMEFRAG", "sessionToken: " + sharedPrefs.getSession() + " SiD " + sharedPrefs.getCustomerSid());
        hFController = new HomeFragmentController(this, sharedPrefs,getActivity());
        //  this.simpleDateFormatday = new SimpleDateFormat("EEE d MMM hh:mm a", Locale.getDefault());
        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        simpleDateFormat.setTimeZone(Utilities.getTimeZoneFromLocation());
        singleBuilderReturn = new SingleDateTimeDialogReturn.Builder(mcontext);
        generalMethodToInit();
    }

    private void mapClicked() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if (hashMarker != null) {
                    Intent intent = new Intent(mcontext, ProviderDetails.class);
                    sharedPrefs.setBookLatitude(currentLatitude + "");
                    sharedPrefs.setBookLongitude(currentLongitude + "");
                    intent.putExtra("PROID", hashMarker.get(marker.getId()).get(0));
                    intent.putExtra("YOUTUBELINK", hashMarker.get(marker.getId()).get(1));
                    /*intent.putExtra("LATITUDE",Double.parseDouble(hashMarker.get(marker.getId()).get(2)));
                    intent.putExtra("LONGITUDE",Double.parseDouble(hashMarker.get(marker.getId()).get(3)));*/
                    intent.putExtra("IMAGEURL", hashMarker.get(marker.getId()).get(4));
                    if (Build.VERSION.SDK_INT >= M) {
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(getActivity(),
                                        ivProProfilePic,
                                        ViewCompat.getTransitionName(ivProProfilePic));
                        startActivity(intent, options.toBundle());
                    } else {
                        startActivity(intent);
                    }

                }

                return false;
            }
        });
    }

    /*
    general init method to initialize the general views
     */
    private void generalMethodToInit() {
        width = size[0] * 60;
        height = size[1] * 60;
        currentLatitude = 0.0;
        currentLongitude = 0.0;
        if(sharedPrefs.getSplashLatitude().equals("") && !sharedPrefs.getSplashLongitude().equals("")){
            currentLatitude=Double.parseDouble(sharedPrefs.getSplashLatitude());
            currentLongitude=Double.parseDouble(sharedPrefs.getSplashLongitude());
        }
        //    myTimer = new Timer();
    }

    public void checkLaterAvailable(){
        if(btnPreBook!=null){
            if(VariableConstant.BOOKINGTYPEAVAILABILITY.getSchedule()){
                if(VariableConstant.BOOKINGTYPE==2)
                    btnPreBook.setVisibility(View.GONE);
                Log.e(TAG, "checkLaterAvailable: " );
            }else{
                //No later booking for certain categories, so clearing later booking
                if(VariableConstant.BOOKINGTYPE==2) {
                    onClick(icHomeClearList);
                    btnPreBook.setVisibility(View.GONE);
                } else if (VariableConstant.BOOKINGTYPE==1) {
                    btnPreBook.setVisibility(View.GONE);
                }
            }
        }
    }
    /**
     * <h1>initializeView</h1>
     * <p>
     * initializing the view of the class
     *
     * @param view the to be initialize
     *             </p>
     */
    private void initializeView(View view) {

        AppTypeface appTypeface = AppTypeface.getInstance(mcontext);
        rlToolbarList=view.findViewById(R.id.rlToolbarList);
        ivProviderListBack=view.findViewById(R.id.ivProviderListBack);
        rl_providerListTb=view.findViewById(R.id.rl_providerListTb);
        btnPreBook=view.findViewById(R.id.btnPreBook);
        tvProviderList=view.findViewById(R.id.tvProviderList);
        ivAbarMenuBtn = view.findViewById(R.id.ivAbarMenuBtn);
        ivToolbarListMenu = view.findViewById(R.id.ivToolbarListMenu);
        ivProviderListBack = view.findViewById(R.id.ivProviderListBack);
        ivCategoriesBack = view.findViewById(R.id.ivCategoriesBack);
        tv_address_bar = view.findViewById(R.id.tv_address_bar);
        rl_inner_add_lyt=view.findViewById(R.id.rl_inner_add_lyt);
        tv_label_categories=view.findViewById(R.id.tv_label_categories);
        rl_inner_add_lyt.setOnClickListener(this);
        ivProviderListBack.setOnClickListener(this);
        ivCategoriesBack.setOnClickListener(this);
        ivAbarMenuBtn.setOnClickListener(this);
        ivToolbarListMenu.setOnClickListener(this);
        btnPreBook.setOnClickListener(this);



        RelativeLayout searchrrlout = view.findViewById(R.id.searchrrlout);
        ImageView ivHomeSearchLocation = view.findViewById(R.id.ivHomeSearchLocation);
        RelativeLayout rlHomeSearchList = view.findViewById(R.id.rlHomeSearchList);
        ImageView ivHomeCurrentLocation = view.findViewById(R.id.ivHomecurrentLocatn);
        ImageView ivHomeLtrBokin = view.findViewById(R.id.ivHomeLtrBokin);
        CardView cardHomeList = view.findViewById(R.id.cardHomeList);
        cardHomeCategories=view.findViewById(R.id.cardHomeCategories);
        cardHomeCategories.setOnClickListener(this);
        clCategorySelector=view.findViewById(R.id.clCategorySelector);
        clCategorySelector.setVisibility(View.GONE);
        tvHomeJobAddress = view.findViewById(R.id.tvHomeJobAddress);
        tvHomeJobAddressList = view.findViewById(R.id.tvHomeJobAddressList);
        rlAbarMainAct = view.findViewById(R.id.rlAbarMainAct);
        rlScheduledtimeDate = view.findViewById(R.id.rlScheduledtimeDate);
        rlScheduledtimeDateList = view.findViewById(R.id.rlScheduledtimeDateList);
        rlScheduledtimeDateList.setOnClickListener(this);
        tvSheduledDateTime = view.findViewById(R.id.tvSheduledDateTime);
        tvSheduledDateTimeList = view.findViewById(R.id.tvSheduledDateTimeList);
        tvSheduledDateTimeList.setOnClickListener(this);
        tvSheduledDateTime.setOnClickListener(this);
        icHomeClear = view.findViewById(R.id.icHomeClear);
        icHomeClearList = view.findViewById(R.id.icHomeClearList);
        frameLayoutList = view.findViewById(R.id.frameLayoutList);
        frameLayoutMap = view.findViewById(R.id.frameLayoutMap);
        btnBookNotary.setOnClickListener(this);
        ivHomeCurrentLocation.setOnClickListener(this);
        cardHomeList.setOnClickListener(this);
        ivHomeLtrBokin.setOnClickListener(this);
        searchrrlout.setOnClickListener(this);
        rlHomeSearchList.setOnClickListener(this);
        ivHomeSearchLocation.setOnClickListener(this);
        TextView homejoblocation = view.findViewById(R.id.homejoblocation);
        TextView tvHomeJobAddress = view.findViewById(R.id.tvHomeJobAddress);
        TextView tvHomeJobLocationList = view.findViewById(R.id.tvHomeJobLocationList);
        TextView tvHomeJobAddressList = view.findViewById(R.id.tvHomeJobAddressList);
        ImageView ivHomeLtrBokinList = view.findViewById(R.id.ivHomeLtrBokinList);
        TextView tvHomeList = view.findViewById(R.id.tvHomeList);
        TextView tvHomeMap = view.findViewById(R.id.tvHomeMap);
        tvProviderList.setTypeface(appTypeface.getHind_semiBold());
        homejoblocation.setTypeface(appTypeface.getHind_regular());
        tvHomeJobAddress.setTypeface(appTypeface.getHind_regular());
        tvHomeList.setTypeface(appTypeface.getHind_semiBold());
        tvHomeJobLocationList.setTypeface(appTypeface.getHind_semiBold());
        tvHomeJobAddressList.setTypeface(appTypeface.getHind_regular());
        tv_address_bar.setTypeface(appTypeface.getHind_regular());
        tv_label_categories.setTypeface(appTypeface.getHind_semiBold());
        tvHomeMap.setTypeface(appTypeface.getHind_semiBold());
        tvHomeNotOperational.setTypeface(appTypeface.getHind_light());
        ivHomeLtrBokinList.setOnClickListener(this);
        tvHomeJobAddressList.setOnClickListener(this);
        tvHomeJobLocationList.setOnClickListener(this);
        updateProfilePic();
        tvSheduledDateTime.setTypeface(appTypeface.getHind_regular());
        tvSheduledDateTimeList.setTypeface(appTypeface.getHind_regular());
        icHomeClear.setOnClickListener(this);
        icHomeClearList.setOnClickListener(this);
    }

    @Override
    public void onCancel() {
        //Toggle laterBook button to show when clicked on cancel button
        btnBookNotary.setText(getString(R.string.book_now));
        btnPreBook.setVisibility(View.GONE);
    }

    private void initializeRecyclerView(View view) {
        tvHomeNotOperational = view.findViewById(R.id.tvHomeNotOperational);
        btnBookNotary=view.findViewById(R.id.btnBookNotary);
        tvEta=view.findViewById(R.id.tvEta);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        cardHomeMap = view.findViewById(R.id.cardHomeMap);
        cardHomeMap.setVisibility(View.GONE);
        mMapFragment.getMapAsync(this);
        RecyclerView recyclrViewHomeList = view.findViewById(R.id.recyclrViewHomeList);
        RecyclerView rv_categorylist_v = view.findViewById(R.id.rv_categorylist_v);
        recyclrViewCat = view.findViewById(R.id.recyclrViewCat);
        cardHomeMap.setOnClickListener(this);
        final LinearLayoutManager listLayoutManager = new LinearLayoutManager(mcontext);
        recyclrViewHomeList.setLayoutManager(listLayoutManager);
        int resId = R.anim.layoutanimation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(mcontext, resId);
        recyclrViewHomeList.setLayoutAnimation(animation);

        homeListAdaptr = new HomeListAdaptr(getActivity(), providerDtls, this);
        recyclrViewHomeList.setAdapter(homeListAdaptr);
        setCategories();
        final LinearLayoutManager layoutmngr = new LinearLayoutManager(mcontext,LinearLayoutManager.HORIZONTAL,false);
        /*final SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclrViewCat);*/
        HorizontalCatItemDecorator decorator=new HorizontalCatItemDecorator(20);
        recyclrViewCat.setLayoutManager(layoutmngr);
        recyclrViewCat.addItemDecoration(decorator);
        categoryAdapter = new CategoryAdapter(mcontext, categoryDataArray,true,HomeFragment.this,this);
        recyclrViewCat.setAdapter(categoryAdapter);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(mcontext,2);
        categoryAdapterV=new CategoryAdapter(mcontext,categoryDataArray,false,HomeFragment.this,this);
        rv_categorylist_v.setLayoutManager(gridLayoutManager);
        rv_categorylist_v.addItemDecoration(decorator);
        rv_categorylist_v.setAdapter(categoryAdapterV);
        progressbar = view.findViewById(R.id.progress_bar);
        if (!sharedPrefs.getProviderData().equals("") && VariableConstant.isSplashCalled) {
            Log.d(TAG, "initializeRecyclerView: "+sharedPrefs.getProviderData());
            VariableConstant.isSplashCalled = true;
            ApplicationController.getInstance().gethModel().responceHomeFragModel(sharedPrefs.getProviderData(), this);
        } else {
            //onError("");
            onClearList();
            onClearMapLocation();
        }
        if(!sharedPrefs.getFcmTopic().equals(""))
            FirebaseMessaging.getInstance().subscribeToTopic(sharedPrefs.getFcmTopic());

       /* if (!ApplicationController.getInstance().isMqttConnected())
            ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());*/
    }

    @Override
    public void onStart() {
        super.onStart();
        //if(!VariableConstant.SELECTEDCATEGORYNAME.equals("")){
        //  Log.e(TAG, "onStart: categoryName:"+VariableConstant.SELECTEDCATEGORYNAME );
        //btnBookNotary.setText("BOOK"+" "+VariableConstant.SELECTEDCATEGORYNAME);
        //}
    }

    private void setCategories() {
        String categoriesList=sharedPrefs.getCategoriesList();
        if(categoriesList!=null && categoriesList!="") {
            try{
                CategoriesPojo categoriesPojo = gson.fromJson(categoriesList,CategoriesPojo.class);
                categoryDataArray = categoriesPojo.getData().getCatArr();
                if(categoryDataArray!=null && categoryDataArray.size()>0){
                    categoryDataArray.get(0).setSelected(true);
                    VariableConstant.selectedCategoryId=categoryDataArray.get(0).get_id();;
                    VariableConstant.BILLINGMODEL=categoryDataArray.get(0).getBilling_model();
                    VariableConstant.MINIMUM_HOUR=categoryDataArray.get(0).getMinimum_hour();
                    //VariableConstant.SURGE_PRICE=categoryDataArray.get(0).getSurgePrice();
                    VariableConstant.SERVICEHOURLYPRICE=categoryDataArray.get(0).getPrice_per_fees();
                    VariableConstant.SELECTEDCATEGORYNAME=categoryDataArray.get(0).getCat_name();
                    VariableConstant.SERVICE_TYPE=categoryDataArray.get(0).getService_type();
                    VariableConstant.SELECTEDCATEGORYVISITFEE=categoryDataArray.get(0).getVisit_fees();
                    VariableConstant.BOOKINGTYPEAVAILABILITY=categoryDataArray.get(0).getBookingTypeAction();
                    onCategoryChanged();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }
    private void showProgress() {
        progressbar.setVisibility(View.VISIBLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
    private void hideProgress()
    {
        if(progressbar!=null)
            progressbar.setVisibility(View.GONE);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    public void notifyAdapterNSetButton(String id, String cat_name){
        //btnBookNotary.setText("BOOK "+cat_name);
        notifyAdapters();
    }
    public void notifyAdapters(){
        categoryAdapterV.setCategoryDataArray(categoryDataArray);
        categoryAdapter.setCategoryDataArray(categoryDataArray);
        categoryAdapterV.notifyDataSetChanged();
        categoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //Log.d(TAG, "onMapReady: "+"updating locatingUpdateHandler");
        getLatitudeLongitude();
        if (ActivityCompat.checkSelfPermission(mcontext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mcontext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Log.d(TAG, "onMapReadyGetInstance: "+ApplicationController.getInstance().isMqttConnected());
        if (!sharedPrefs.getProviderData().equals("") && VariableConstant.isSplashCalled) {
            VariableConstant.isSplashCalled = false;
            ApplicationController.getInstance().gethModel().responceHomeFragModel(sharedPrefs.getProviderData(), this);
        }
        if(ApplicationController.getInstance().isMqttConnected())
        {
            ApplicationController.getInstance().gethModel().responceHomeFragModel(sharedPrefs.getProviderData(), this);
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.Provider.value + "/" + sharedPrefs.getCustomerSid(), 1);
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        IsReturnFromSearch = false;
                    }
                }, 5000);
            }
        });


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Log.d(TAG, "onCameraIdle: "+currentLatitude);
                VisibleRegion visibleRegion = mMap.getProjection()
                        .getVisibleRegion();
                Point x1 = mMap.getProjection().toScreenLocation(
                        visibleRegion.farRight);
                Point y = mMap.getProjection().toScreenLocation(
                        visibleRegion.nearLeft);
                Point centerPoint = new Point(x1.x / 2, y.y / 2);
                LatLng centerFromPoint = mMap.getProjection().fromScreenLocation(centerPoint);
                double lat = centerFromPoint.latitude;
                double lon = centerFromPoint.longitude;
                sharedPrefs.setBookLatitude(lat+"");
                sharedPrefs.setBookLongitude(lon+"");
                currentLatitude = lat;
                currentLongitude = lon;
                onCurrentLatLogitude(new LatLng(currentLatitude, currentLongitude));
                /*if(IsReturnFromSearch)
                    hFController.providerService(currentLatitude, currentLongitude);*/
                VariableConstant.BOOKINGLAT=currentLatitude;
                VariableConstant.BOOKINGLONG=currentLongitude;

                VariableConstant.NOWBOOKINGLAT=currentLatitude;
                VariableConstant.NOWBOOKINGLONG=currentLongitude;

                VariableConstant.BOOKINGTIMEZONE = Utilities.getTimeZoneFromBookingLocation();
                //getEtaUpdate();
                checkZone(new LatLng(currentLatitude,currentLongitude));

            }
        });

        mapClicked();

    }

    private void getLatitudeLongitude() {
        LatLng latLng;
        Log.d(TAG, "getLatitudeLongitude: ");
        if (currentLatLng[0] == 0.0 && currentLatLng[1] == 0.0) {
            hFController.providerService(Double.parseDouble(sharedPrefs.getSplashLatitude()), Double.parseDouble(sharedPrefs.getSplashLongitude()));
            latLng = new LatLng(Double.parseDouble(sharedPrefs.getSplashLatitude()), Double.parseDouble(sharedPrefs.getSplashLongitude()));
        }
        else {
            hFController.providerService(currentLatLng[0], currentLatLng[1]);
            latLng = new LatLng(currentLatLng[0], currentLatLng[1]);
        }
        updateCameraInfo(latLng);
        //  return latLng;
    }

    /**
     * <h2>updateCameraInfo</h2>
     * <p>updating the camera to the desired locations</p>
     *
     * @param latLng    latitude and langotude object
     */
    private void updateCameraInfo(LatLng latLng)
    {
        Log.e(TAG, "updateCameraInfo: latitude:"+latLng.latitude+ " longitude:"+ latLng.longitude );
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
        mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(cameraUpdate);
    }


    @Override
    public void onResume() {
        super.onResume();
        workForOnResume();
        MqttWork();
        callReviewPendingBooking();
        checkBookingType();
    }

    public void checkBookingType(){
        Log.d(TAG, "checkBookingType: ichomeclearlist:"
                +icHomeClearList.getVisibility()+" View Visible: "+View.GONE);
        if(VariableConstant.BOOKINGTYPE==1){
            rlScheduledtimeDate.setVisibility(View.GONE);
            rlScheduledtimeDateList.setVisibility(View.GONE);
        }else{
            Calendar temp=Utilities.getCalendarServerTime();
            if(!VariableConstant.SCHEDULEDDATE.isEmpty()){
                temp.setTimeInMillis(Long.parseLong(VariableConstant.SCHEDULEDDATE)*1000);
            }
            tvSheduledDateTime.setText(Utilities.getFormattedDateBookingLocation(temp.getTime()));
            tvSheduledDateTimeList.setText(Utilities.getFormattedDateBookingLocation(temp.getTime()));
        }
    }

    private void callReviewPendingBooking() {
        //Calling this post delayed so that to remove getaccesstoken api call conflict for
        //1) location service
        //2) this method(pending booking)
            new Handler().postDelayed(()->{hFController.pendingBooking();
                Log.d(TAG, "callReviewPendingBooking: ");},4000);
    }

    private void MqttWork() {
        Log.d(TAG, "MqttWorkIsConNNected: "+ApplicationController.getInstance().isMqttConnected());
        if(!ApplicationController.getInstance().isMqttConnected()) {
            ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());
        }
        else {
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.Provider.value + "/" + sharedPrefs.getCustomerSid(), 1);
        }
    }

    /**
     * <h1>workForOnResume</h1>
     * <p>
     * this is called from onResume
     * this checks the permission, get the address, call the publish
     * </p>
     */
    private void workForOnResume() {

        VariableConstant.isAdapterNotified = true;
        VariableConstant.isLiveBokinOpen=false;
        Log.d(TAG+" SHIJENTEST", "workForOnResume: "+VariableConstant.isLiveBokinOpen);
        //hFController.configApi(sharedPrefs, mcontext, aProgres);
        if (myTimer_publish != null) {
            myTimer_publish.cancel();
            myTimer_publish.purge();
        }
        myTimer_publish = null;
        startPublishingWithTimer();
    }
    /**
     * publishing provider topic for every 4 second
     */
    public void startPublishingWithTimer() {

        TimerTask myTimerTask_publish;
        if (myTimer_publish != null)
            return;
        myTimer_publish = new Timer();
        myTimerTask_publish = new TimerTask() {
            @Override
            public void run() {
                locationService();
            }
        };
        myTimer_publish.schedule(myTimerTask_publish, 0, VariableConstant.PROFREQUENCYINTERVAL * 1000);
    }

    private void locationService() {
        try
        {
            if(IsReturnFromSearch)
            {
                hFController.providerService(currentLatitude, currentLongitude);
            }
            else
            {
                if (currentLatitude != 0 || currentLongitude != 0)
                    hFController.providerService(currentLatitude, currentLongitude);
                else
                    hFController.providerService(Double.parseDouble(sharedPrefs.getSplashLatitude()), Double.parseDouble(sharedPrefs.getSplashLongitude()));
            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.icHomeClearList:
            case R.id.icHomeClear:
                VariableConstant.BOOKINGTYPE = 1;
                VariableConstant.SCHEDULEDDATE = "";
                VariableConstant.SCHEDULEDTIME = 0;
                //Toggle the laterBook button back to show when clicked on the button manually.
                btnBookNotary.setText(getString(R.string.book_now));
                btnPreBook.setVisibility(View.GONE);
                rlScheduledtimeDate.setVisibility(View.GONE);
                rlScheduledtimeDateList.setVisibility(View.GONE);
                if(aProgres.isNetworkAvailable())
                {
                    locationService();
                }else
                {
                    aProgres.showNetworkAlert();
                }
                try {
                    onEtaUpdates(distanceMatrixPojo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rlHomeSearchList:
            case R.id.ivHomeSearchLocation:
            case R.id.searchrrlout:
            case R.id.tvHomeJobAddressList:
            case R.id.tvHomeJobLocationList:
            case R.id.rl_inner_add_lyt:
                Intent intent = new Intent(getActivity(),SearchAddressLocation.class);
                intent.putExtra("CominFROM","HOMEFRAG");
                startActivityForResult(intent,SERACH_RESULT);
                //   openAutocompleteActivity();
                break;
            case R.id.ivAbarMenuBtn:
            case R.id.ivToolbarListMenu:
                mainActivity.toggleDrawer();
                break;
            case R.id.ivHomecurrentLocatn:
                IsReturnFromSearch = false;
                VariableConstant.isCurrentLocation = true;
                Log.d(TAG, "onClickDistance: "+hFController.distance(currentLatLng[0],currentLatLng[1],currentLatitude,currentLongitude));
                if(hFController.distance(currentLatLng[0],currentLatLng[1],currentLatitude,currentLongitude)>10)
                    showProgress();
                hFController.onCurrentLocation(currentLatLng[0], currentLatLng[1], mMap);
                break;


            case R.id.cardHomeList:
            case R.id.ivCategoriesBack:
            case R.id.cardHomeCategories:
                //show map view;
                showMapView();
                break;
            case R.id.ivProviderListBack:
                if(listFromBookNow){
                    showMapView();
                }else{
                    showCategoryPage();
                }
                listFromBookNow=false;
                break;
            case R.id.cardHomeMap:
                //showing list view
                showCategoryPage();
                break;

            case R.id.ivHomeLtrBokin:
            case R.id.ivHomeLtrBokinList:
            case R.id.rlScheduledtimeDateList:
            case R.id.tvSheduledDateTimeList:
            case R.id.tvSheduledDateTime:
            case R.id.btnPreBook:

                //hFController.singlewindowBootomSheet(mcontext,singleBuilderReturn,simpleDateFormat);
                //hFController.openDatePicker();
                fragmentLaterBookingSteps = new FragmentLaterBookingSteps();
                fragmentLaterBookingSteps.setCallBack(this);
                fragmentLaterBookingSteps.show(getFragmentManager(),"LATERBOOKINGSTEPS");

                break;
            case R.id.btnBookNotary:
                ArrayList<MqttProviderDetails> favoriteProviders = hFController.getFavoriteProviders();
                if(favoriteProviders!=null && favoriteProviders.size()>0){
                    showFavoriteList(favoriteProviders);
                }else{
                    btnBookNotary.setEnabled(false);
                    checkServiceTypeAndContinue();
                }
                break;
        }
    }


    @Override
    public void onFavoriteSelected(Bundle bundle) {
        String providerId = (String) bundle.get(BottomsheetFragmentFavProvider.PROVIDERID);
        String imageUrl= (String) bundle.get(BottomsheetFragmentFavProvider.IMAGEURL);
        Log.d(TAG, "onFavoriteSelected: providerId:"+providerId);
        Log.d(TAG, "onFavoriteSelected: imageurl:"+imageUrl);
        Intent intent;
        //if(VariableConstant.BUSINESSTYPE==1){
        intent = new Intent(mcontext, ProviderDetails.class);
        /*}else{
            intent = new Intent(mcontext, ProviderDetails_Rides.class);
        }*/
        sharedPrefs.setBookLatitude(currentLatitude + "");
        sharedPrefs.setBookLongitude(currentLongitude + "");
        intent.putExtra(BottomsheetFragmentFavProvider.PROVIDERID, providerId);
        intent.putExtra(BottomsheetFragmentFavProvider.IMAGEURL, imageUrl);
        startActivity(intent);
    }

    /**
     * <h1>onOthersSelected</h1>
     * This method is called when favorite providers list is how if any favorite provider is online
     * and the customer clicks others instead of any favorite provider
     */
    @Override
    public void onOthersSelected() {
        checkServiceTypeAndContinue();
    }


    /**
     * <h1>showFavoriteList</h1>
     * This method is used to show the favorite provider list
     * @param favoriteProviders this is list of favorite providers who are online
     */
    private void showFavoriteList(ArrayList<MqttProviderDetails> favoriteProviders) {
        BottomsheetFragmentFavProvider favProviderBottomsheet=new BottomsheetFragmentFavProvider();
        Bundle bundle=new Bundle();
        bundle.putSerializable(FAVPROVIDER,favoriteProviders);
        favProviderBottomsheet.setArguments(bundle);
        favProviderBottomsheet.setCallBack(this);
        if(getFragmentManager()!=null){
            favProviderBottomsheet.show(getFragmentManager(),"FAVPROVIDER");
        }
    }


    /**
     * <h1>checkServiceTypeAndContinue</h1>
     * <p> This method is used to check the selected category service type and change the booking flow accordingly
     * if selected category's serviceType is 1 then call the services api and move to service selection page
     * else if selected category's serviceType is 2 then show the list view to select providers
     * </p>
     */
    private void checkServiceTypeAndContinue()
    {
        if(sharedPrefs.getSelectedServiceType().equals("1")){
            VariableConstant.BOOKINGMODEL=1;
            if(Utilities.isNetworkAvailable(getActivity())){
                showProgress();
                hFController.callServicesService();
            }else{
                Toast.makeText(getContext(),"Please check your internet connection",Toast.LENGTH_SHORT).show();
            }
        }else{
            listFromBookNow=true;
            showListview();
            btnBookNotary.setEnabled(true);
        }
    }

    public void showMapView() {
        frameLayoutMap.setVisibility(View.VISIBLE);
        frameLayoutList.setVisibility(View.GONE);
        clCategorySelector.setVisibility(View.GONE);
        listScreen=false;
    }

    public void showCategoryPage() {
        categoryPage=true;
        clCategorySelector.setVisibility(View.VISIBLE);
        rlToolbarList.setVisibility(View.VISIBLE);
        rl_providerListTb.setVisibility(View.GONE);
        frameLayoutMap.setVisibility(View.GONE);
        frameLayoutList.setVisibility(View.VISIBLE);
        listScreen=true;
    }

    private void showOnlyProviderList(){
        VariableConstant.SHOWINGPROVIDERLIST=true;
        frameLayoutMap.setVisibility(View.GONE);
        clCategorySelector.setVisibility(View.GONE);
        frameLayoutList.setVisibility(View.VISIBLE);
        rlToolbarList.setVisibility(View.GONE);
        rl_providerListTb.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateLocation(Location location) {

        Log.d(TAG, "updateLocation: "+location.getLatitude());
        currentLatLng[0] = location.getLatitude();
        currentLatLng[1] = location.getLongitude();
        sharedPrefs.setSplashLatitude(currentLatLng[0]+"");
        sharedPrefs.setSplashLongitude(currentLatLng[1]+"");
        VariableConstant.SPLASHLAT=location.getLatitude();
        VariableConstant.SPLASHLONG=location.getLongitude();
        VariableConstant.BOOKINGLAT=location.getLatitude();
        VariableConstant.BOOKINGLONG=location.getLongitude();
        Utilities.getTimeZoneFromLocation();
        networkUtilObj.stop_Location_Update();
        if(isLocationUpdated){
            onMapReady(mMap);
            //locatingUpdateHandler.postDelayed(locationUpdateRunnable,5000);
            hideProgress();
            isLocationUpdated=false;
        }
    }

    @Override
    public void locationMsg(String error) {
        aProgres.alertinfo(error);
    }

    @Override
    public void onAvailableMusic(ArrayList<MqttProviderDetails> providerDtl) {
        providerDtls.clear();
        providerDtls.addAll(providerDtl);
        recyclrViewCat.setVisibility(View.VISIBLE);
        cardHomeMap.setVisibility(View.VISIBLE);
        hFController.sortProviderByDistance(providerDtl);//Sorting provider by distance to get nearest provider
        //hFController.checkDistanceAndUpdateETA(providerDtl,currentLatitude,currentLongitude,this);
        if(btnBookNotary!=null ){
            btnBookNotary.setVisibility(View.VISIBLE);
        }
        checkLaterAvailable();

        tvHomeNotOperational.setVisibility(View.GONE);
        MqttProviderDetails item;
        for(int i=0;i<providerDtls.size();i++){
            item=  providerDtls.get(i);
            providerDtls.set(i,item);
        }
        homeListAdaptr.makeList();
        homeListAdaptr.notifyDataSetChanged();
        checkProvidersAndUpdateButton();
    }

    @Override
    public void onAvailableOnlineProvider(ArrayList<String> proInfo, String image, LatLng latLng) {
        onlineMarker(proInfo, image, latLng);
        if(progressbar!=null)
            hideProgress();
    }

    @Override
    public void onUpdateMakerStatusOnline(ArrayList<String> proInfo, String image, LatLng latLng, Marker proMarker) {
        hashMarker.remove(proMarker.getId());
        proMarker.remove();
        onlineMarker(proInfo, image, latLng);
    }

    @Override
    public void onUpdateMapToProvider(int position) {

        hFController.upDateMapPostion(mMap, providerDtls, hashMarker, position);

    }

    @Override
    public void onAvailableOffLineProvider(ArrayList<String> proInfo, String image, LatLng latLng) {
        offlineMarker(proInfo, image, latLng);
        if(progressbar!=null)
            hideProgress();
    }

    @Override
    public void onUpdateMakerStatusOffline(ArrayList<String> proInfo, String image, LatLng latLng, Marker proMarker) {
        hashMarker.remove(proMarker.getId());
        proMarker.remove();
        offlineMarker(proInfo, image, latLng);

    }

    private Marker plotMarker;
    private LatLng latLng;
    @Override
    public void onUpdateMakerPosition(LatLng latLng, Marker promarker) {
        promarker.setPosition(latLng);
        //Log.d(TAG, "onUpdateMakerPosition: "+latLng.latitude+" latlng "+latLng.longitude);
        this.latLng = latLng;
        plotMarker = promarker;
    }

    private void animateMarker() {
        Log.d(TAG, "animateMarker: ");
        locatingUpdateHandler = new Handler();
        final Handler markeMoveHandler = new Handler();
        final float durationInMs = 4000;
        locationUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                locatingUpdateHandler.postDelayed(this,5000);
                Log.d(TAG, "run: locatingUpdateHandler");
                if(mMap !=null && plotMarker!=null) {
                    final LatLng startPosition = plotMarker.getPosition();
                    final long start = SystemClock.uptimeMillis();
                    final Interpolator interpolator = new AccelerateDecelerateInterpolator();

                    markeMoveHandler.post(new Runnable()
                    {
                        long elapsed;
                        float t;
                        @Override
                        public void run() {
                            // Calculate progress using interpolator
                            elapsed = SystemClock.uptimeMillis() - start;
                            t = elapsed / durationInMs;
                            LatLng currentPosition = new LatLng(startPosition.latitude*(1-t)+ latLng.latitude*t,
                                    startPosition.longitude*(1-t)+ latLng.longitude*t);
                            plotMarker.setPosition(currentPosition);
                            // Repeat till progress is complete.
                            if (t < 1) {
                                // Post again 16ms later.
                                markeMoveHandler.postDelayed(this, 16);
                            }
                        }
                    });
                }
            }
        };
    }



    @Override
    public void onError(String errorMsg) {
        if (errorMsg.equals(""))
            aProgres.alertinfo(mcontext.getString(R.string.serverProblm));
        else {
            if(tvHomeNotOperational!=null && cardHomeMap!=null && btnBookNotary!=null && btnPreBook!=null && cardHomeMap!=null) {
                tvHomeNotOperational.setText(errorMsg);
                tvHomeNotOperational.setVisibility(View.VISIBLE);
                recyclrViewCat.setVisibility(View.GONE);
                btnBookNotary.setVisibility(View.GONE);
                btnPreBook.setVisibility(View.GONE);
                cardHomeMap.setVisibility(View.GONE);
            }
        }
        hideProgress();
        //aProgres.alertinfo(errorMsg);
    }

    @Override
    public void onBookLatLng() {
        if(currentLatitude!=0 && currentLongitude!=0)
        {
            sharedPrefs.setBookLatitude(currentLatitude + "");
            sharedPrefs.setBookLongitude(currentLongitude + "");
        }
        else
        {
            sharedPrefs.setBookLatitude(sharedPrefs.getSplashLatitude());
            sharedPrefs.setBookLongitude(sharedPrefs.getSplashLongitude());
        }
    }

    @Override
    public void onSessionError(String sessionErrorMsg) {
        Utilities.setMAnagerWithBID(mcontext, sharedPrefs,sessionErrorMsg);
    }

    @Override
    public void onScheduledDateTime(String dateTime) {
        if(aProgres.isNetworkAvailable())
        {
            locationService();
            showProgress();
        }else {
            aProgres.showNetworkAlert();
        }
        tvSheduledDateTime.setText(dateTime);
        tvSheduledDateTimeList.setText(dateTime);
        //Toggle and hide the laterBook button if schedule booking available
        btnPreBook.setVisibility(View.GONE);
        btnBookNotary.setText(getString(R.string.book));
        rlScheduledtimeDate.setVisibility(View.VISIBLE);
        rlScheduledtimeDateList.setVisibility(View.VISIBLE);
    }
    @Override
    public void onErrorCategories(String code, String error) {
        recyclrViewCat.setVisibility(View.GONE);
        btnPreBook.setVisibility(View.GONE);
        btnBookNotary.setVisibility(View.GONE);
        cardHomeMap.setVisibility(View.GONE);
        sharedPrefs.setCategoryId("");
        sharedPrefs.setCategoriesList("");
        if(categoryDataArray!=null){
            categoryDataArray.clear();
        }
        notifyAdapters();
        //Toast.makeText(mcontext,code+" "+error,Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessGetCategories(CategoriesPojo categoryData) {
        if(categoryData!=null){
            categoryDataArray = categoryData.getData().getCatArr();
            if(categoryDataArray!=null && categoryDataArray.size()>0){
                sharedPrefs.setCategoryId(categoryDataArray.get(0).get_id());
                categoryDataArray.get(0).setSelected(true);
            }
            notifyAdapters();
        }
    }

    @Override
    public void getEtaUpdate() {
        if(currentLatitude!=0||currentLongitude!=0)
            hFController.getEta(currentLatitude,currentLongitude,this);
        else if(Double.parseDouble(sharedPrefs.getSplashLatitude())!=0){
            hFController.getEta(Double.parseDouble(sharedPrefs.getSplashLatitude()),Double.parseDouble(sharedPrefs.getSplashLongitude()),this);
        }
    }

    @Override
    public void onUpdateProviders(String jsonObject) {
        if(sharedPrefs!=null){
            sharedPrefs.setProviderData(jsonObject);
        }
    }

    @Override
    public void setNoProviders() {
        tvEta.setText("No Provider");
        categoryAdapter.notifyDataSetChanged();
        categoryAdapterV.notifyDataSetChanged();
    }

    @Override
    public void onCurrentLatLogitude(LatLng latLng) {
        Log.d(TAG, "onCameraIdleonCurrentLatLogitude: "+latLng.latitude);
        currentLatitude = latLng.latitude;
        currentLongitude = latLng.longitude;
        final String params[] = {"" + latLng.latitude, "" + latLng.longitude};
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                locatnAddress(params);
            }
        });
    }

    @Override
    public void onLocationSavedAddress(String address) {
        tvHomeJobAddress.setText(address);
        tvHomeJobAddressList.setText(address);
        tv_address_bar.setText(address);
        sharedPrefs.setBookingAddress(address);
    }

    @Override
    public void onClearMapLocation() {
        hashMarker.clear();
        if (mMap != null)
            mMap.clear();
    }

    public void checkZone(final LatLng latLng){
        String categoriesList = sharedPrefs.getCategoriesList();
        CategoriesPojo categoriesPojo=null;
        try{
            categoriesPojo = gson.fromJson(categoriesList, CategoriesPojo.class);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(categoriesPojo!=null && categoriesPojo.getData().getCityData()!=null){
            hFController.checkZone(categoriesPojo.getData().getCityData().getPolygons(),latLng);
        }else{
            double[] loc={latLng.latitude,latLng.longitude};
            hFController.getCategories(loc);
        }

    }
    
    public void clearListData(){
        providerDtls.clear();
        homeListAdaptr.makeList();
        homeListAdaptr.notifyDataSetChanged();
    }

    @Override
    public void onClearList()
    {
        try{
            etaCleared=true;
            hideProgress();
            tvEta.setText("No Provider");
            cardHomeMap.setVisibility(View.GONE);
            providerDtls.clear();
            homeListAdaptr.makeList();
            homeListAdaptr.notifyDataSetChanged();
            //onEtaUpdates(null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onHideProgressBar() {
        hideProgress();
    }

    @Override
    public void onServicesUpdated(String result) {
        onHideProgressBar();
        Intent intent;
        Log.d(TAG, "onServicesUpdated: "+result);
        if(sharedPrefs.getBillingModel().equals("3") || sharedPrefs.getBillingModel().equals("4")){
            intent=new Intent(getActivity(),ConfirmBookingActivity.class);
        }else {
            intent=new Intent(getActivity(),CategoriesAndServices.class);
        }
        intent.putExtra("result",result);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        btnBookNotary.setEnabled(true);
    }

    /**
     * <h1>updateWallet</h1>
     * This method is used to update wallet amount in side screen and also enable or disable the wallet from the side screen
     * @param wallet wallet data sent from admin
     */
    @Override
    public void updateWallet(WalletData wallet) {
        Log.d(TAG, "updateWallet: wallet:"+wallet.getWalletAmount());
        //Wallet postion in side screen
        if(wallet.getEnableWallet()){
            mainActivity.navView.getMenu().getItem(VariableConstant.WALLET_POSTION_IN_SIDESCREEN).setVisible(true);
            mainActivity.navView.getMenu().getItem(VariableConstant.WALLET_POSTION_IN_SIDESCREEN).setTitle("My Wallet ("+Utilities.getAmtForRecept(Double.parseDouble(wallet.getWalletAmount()),wallet.getCurrencySymbol())+")");
            MenuItem mi = mainActivity.navView.getMenu().getItem(VariableConstant.WALLET_POSTION_IN_SIDESCREEN);
            SpannableString mNewTitle = new SpannableString(mi.getTitle());
            mNewTitle.setSpan(new CustomTypefaceSpan("", AppTypeface.getInstance(getActivity()).getHind_regular()), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mi.setTitle(mNewTitle);
        }else{
            mainActivity.navView.getMenu().getItem(VariableConstant.WALLET_POSTION_IN_SIDESCREEN).setVisible(false);
        }
    }

    @Override
    public void onSuccessProviderServices(String code, String response) {
        /*hideProgress();
        sharedPrefs.setProviderData(response);
        ApplicationController.getInstance().gethModel().responceHomeFragModel(response, this);*/
    }

    @Override
    public void onFailureProviderServices(String code, String response) {
        /*hideProgress();
        if (code.equals("502"))
            aProgres.alertinfo(getActivity().getString(R.string.serverProblm));
        else {
            try{
                ErrorHandel errorHandel = new Gson().fromJson(code, ErrorHandel.class);
                Log.d(TAG, "onError: " + code);
                switch (code) {
                    case "498":
                        if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                            networkUtilObj.stoppingLocationUpdate();
                        break;
                    case "401":
                        if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
                            networkUtilObj.stoppingLocationUpdate();
                        break;
                    case "500":
                        aProgres.alertinfo(errorHandel.getMessage());
                        break;
                    case "404":
                        if(errorHandel.getMessage().equalsIgnoreCase(getString(R.string.no_providers_available))){
                            Toast.makeText(getActivity(),errorHandel.getMessage(),Toast.LENGTH_LONG).show();
                        }
                        sharedPrefs.setProviderData("");
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }*/
    }

    @Override
    public void onPendingBooking(long bid) {
        if(bid!=0) {
            VariableConstant.isHomeFragment = true;
            Intent intent = new Intent(mcontext, RateYourBooking.class);
            intent.putExtra("BID", bid);
            startActivity(intent);
        }
    }

    private void locatnAddress(String[] params) {
        if (isAdded()) {
            if (!IsReturnFromSearch) {
                hFController.locatnAddress(params, mcontext);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        VariableConstant.isAdapterNotified = false;
        if (networkUtilObj != null && networkUtilObj.isGoogleApiConnected())
            networkUtilObj.stoppingLocationUpdate();
        stopTimer();
        if (singleBuilderReturn != null)
            singleBuilderReturn.dismiss();

        if(sharedPrefs!=null && !sharedPrefs.getCustomerSid().equals(""))
        {
            if(ApplicationController.getInstance().isMqttConnected())
            {
                //ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.JobStatus.value + "/" + sharedPrefs.getCustomerSid());
                ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Provider.value + "/" + sharedPrefs.getCustomerSid());
                ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Message.value + "/" + sharedPrefs.getCustomerSid());
                //  ApplicationController.getInstance().disconnect(sharedPrefs.getCustomerSid());
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*if(locationUpdateRunnable!=null && locatingUpdateHandler!=null)
            locatingUpdateHandler.removeCallbacks(locationUpdateRunnable);*/
        stopTimer();
    }

    private void stopTimer() {
        if (myTimer_publish != null) {
            myTimer_publish.cancel();
            myTimer_publish.purge();
        }
    }

    /**
     * <h>chckForPermission</h>
     * checking the permission for the location at the run time
     */
    private void chckForPermission() {
        try{
            Log.e(TAG, "chckForPermission: Permission test" );
            if (Build.VERSION.SDK_INT >= 23) {
                myPermissionLocationArrayList = new ArrayList<>();
                myPermissionLocationArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_ACCESS_FINE_LOCATION);
                myPermissionLocationArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_ACCESS_COARSE_LOCATION);

                if (AppPermissionsRunTime.checkPermission(getActivity(), myPermissionLocationArrayList, REQUEST_CODE_PERMISSION_LOCATION)) {
                    networkUtilObj = new LocationUtil(getActivity(), this);
                    if (!networkUtilObj.isGoogleApiConnected()) {
                        showProgress();
                        networkUtilObj.checkLocationSettings();
                        isLocationUpdated=true;
                    }
                }
            } else {
                networkUtilObj = new LocationUtil(getActivity(), this);
                if (!networkUtilObj.isGoogleApiConnected()) {
                    networkUtilObj.checkLocationSettings();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * <h1>offlineMarker</h1>
     * plotting the marker on the map
     *
     * @param proList containg the provide info i.e latitude, longitude and provider id
     * @param image   image to show on the map
     * @param latLng  LatLng of the marker provider
     */
    private void offlineMarker(ArrayList<String> proList, String image, LatLng latLng) {
        final View marker_view = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custome_offline_marker, null);
        ImageView ivOffline = marker_view.findViewById(R.id.ivOffLine);
        ivProProfilePic = ivOffline;
        if (mMap != null) {
            String markerId = hFController.markProviderMarkr(proList.get(0), mMap, ivOffline, image,
                    latLng, marker_view, width, height, 0);
            hashMarker.put(markerId, proList);
        }
    }

    /**
     * <h1>onlineMarker</h1>
     * plotting the marker on the map
     *
     * @param proList containg the provide info i.e latitude, longitude and provider id
     * @param image   image to show on the map
     * @param latLng  LatLng of the marker provider
     */
    private void onlineMarker(ArrayList<String> proList, String image, LatLng latLng) {
        final View marker_view = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custome_online_marker, null);
        ImageView ivOnline = marker_view.findViewById(R.id.ivOffLine);
        ivProProfilePic = ivOnline;
        if (mMap != null) {
            String markerId = hFController.markProviderMarkr(proList.get(0), mMap, ivOnline, image, latLng, marker_view,
                    width, height, 1);
            hashMarker.put(markerId, proList);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SERACH_RESULT)
        {
            if(data!=null)
            {
                IsReturnFromSearch = true;
                VariableConstant.isCurrentLocation = true;
                String addrssname = data.getStringExtra("placename");
                String fuladdrss = data.getStringExtra("formatedaddes");
                double lati = data.getDoubleExtra("LATITUDE",0);
                double langi = data.getDoubleExtra("LONGITUDE",0);
                currentLatitude = lati;
                currentLongitude = langi;
                if("NO".equals(addrssname)) {
                    sharedPrefs.setBookLatitude("" + lati);
                    sharedPrefs.setBookLongitude("" + langi);
                    //  hFController.addressTimer(mcontext, mMap, lati, langi);
                }
                else
                    onLocationSavedAddress(addrssname);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 16.0f));
                if(listScreen){
                    checkZone(new LatLng(lati,langi));
                }
                showProgress();
            }
        }else if(requestCode== LocationUtil.REQUEST_CHECK_SETTINGS){
            if(resultCode==getActivity().RESULT_OK)
            {
                networkUtilObj.checkLocationSettings();
            }
            else if(resultCode==getActivity().RESULT_CANCELED)
            {
                Log.d(TAG, "onActivityResult: "+resultCode);
            }
        }
    }

    public void makeVariableValue()
    {
        VariableConstant.CALLMQTTDATA = 1;
    }

    public void updateProfilePic() {
        if (!sharedPrefs.getImageUrl().equals("")) {
            Picasso.with(mcontext)
                    .load(sharedPrefs.getImageUrl())
                    .resize((int) (60 * size[0]), (int) (60 * size[1]))
                    .transform(new CircleTransform())
                    .centerCrop()
                    .into(ivAbarMenuBtn);
            Picasso.with(mcontext)
                    .load(sharedPrefs.getImageUrl())
                    .resize((int) (60 * size[0]), (int) (60 * size[1]))
                    .transform(new CircleTransform())
                    .centerCrop()
                    .into(ivToolbarListMenu);
        }
    }

    public void showListview() {
        categoryPage=false;
        frameLayoutMap.setVisibility(View.GONE);
        clCategorySelector.setVisibility(View.GONE);
        frameLayoutList.setVisibility(View.VISIBLE);
        listScreen=true;
        tvProviderList.setText(VariableConstant.SELECTEDCATEGORYNAME);
        rlToolbarList.setVisibility(View.GONE);
        rl_providerListTb.setVisibility(View.VISIBLE);
    }

    /*@Override
    public void onEtaUpdates(String duration, String distance) {
        Log.d(TAG, "onEtaUpdates: "+duration);
        tvEta.setText(duration);
        categoryAdapter.setEta(distance);
    }*/
    @Override
    public void onEtaUpdates(DistanceMatrixPojo pojo) {
        LinkedHashMap<String,LatLng> idsLatlng=HomeFragmentModel.idLatLngSet;
        this.distanceMatrixPojo=pojo;

        if(pojo!=null && idsLatlng.size()>0){
            int position=-1;
            int i=0;
            for (Map.Entry m : idsLatlng.entrySet()) {
                if(m.getKey().equals(VariableConstant.selectedCategoryId)){
                    position=i;
                }
                i++;
            }
            if((getString(R.string.ok)).equals(pojo.getStatus())){
                if(position!=-1 && pojo.getRows()!=null && pojo.getRows().size()>0){
                    elements = pojo.getRows().get(0).getElements();
                    if(elements.size()>position){
                        if(elements.get(position).getStatus().equalsIgnoreCase("OK")){
                            tvEta.setText(elements.get(position).getDuration().getText());
                        }
                    }
                }else{
                    tvEta.setText(R.string.no_providers);
                }
                if(pojo.getRows()!=null && pojo.getRows().size()>0)
                    categoryAdapter.setEta(pojo.getRows().get(0).getElements());
            }
        }
    }

    @Override
    public void onCategoryChanged() {
        sharedPrefs.setCategoryId(VariableConstant.selectedCategoryId);
        sharedPrefs.setBillingModel(VariableConstant.BILLINGMODEL);
        sharedPrefs.setCategoryHourlyPrice(VariableConstant.SERVICEHOURLYPRICE);
        sharedPrefs.setSelectedCategoryName(VariableConstant.SELECTEDCATEGORYNAME);
        sharedPrefs.setSelectedServiceType(VariableConstant.SERVICE_TYPE);
        onClearMapLocation();
        clearListData();
        hFController.updateCategoryProviders(sharedPrefs,this);
        checkLaterAvailable();
        onEtaUpdates(distanceMatrixPojo);
        checkProvidersAndUpdateButton();
    }

    private void checkProvidersAndUpdateButton() {
        try{
            MqttProvidrResp mqttProvidrResp = gson.fromJson(sharedPrefs.getProviderData(), MqttProvidrResp.class);
            for(MqttProviderData providerData:mqttProvidrResp.getData()){
                if(providerData.getCategoryId().equals(VariableConstant.selectedCategoryId)){
                    if(providerData.getProviderList().size()>0)
                    {
                        btnBookNotary.setEnabled(true);
                    }else{
                        btnBookNotary.setEnabled(true);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBook(int bookingType, long scheduleTime, int minutes) {
        if(bookingType==1 && VariableConstant.BOOKINGTYPE!=bookingType){
            onClick(icHomeClearList);
        }
        if(bookingType==1){
            VariableConstant.SCHEDULEDDATE="";
        }else{
            VariableConstant.SCHEDULEDDATE=scheduleTime+"";
        }
        VariableConstant.BOOKINGTYPE=bookingType;
        VariableConstant.SCHEDULEDTIME=minutes;
        Calendar cal=Utilities.getCalendarServerTime();
        cal.setTimeInMillis(scheduleTime*1000);
        if(bookingType==2){
            Log.d(TAG, "onBook: "+cal.getTime());
            onScheduledDateTime(Utilities.getFormattedDateBookingLocation(cal.getTime()));

        }
    }

}
