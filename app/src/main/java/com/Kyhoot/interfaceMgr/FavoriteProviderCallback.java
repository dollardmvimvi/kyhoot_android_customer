package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 18/6/18.
 */
public interface FavoriteProviderCallback {
    void onFavProvidersAvailable(String header, String result);
    void onFavProvidersError(String header, String result);
    void onUnfavoritingSuccessful(String header, String result);
    void onUnfavoritingError(String header, String result);
}
