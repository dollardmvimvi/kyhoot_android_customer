package com.Kyhoot.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.main.BookingPendingAccept;
import com.Kyhoot.main.CancelledBooking;
import com.Kyhoot.main.CompletedInvoiceInfo;
import com.Kyhoot.main.LiveStatus;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * <h>MyEventsPageAdapter</h>
 * Created by 3Embed on 11/1/2017.
 */

public class MyEventsPageAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private Activity mActivity;
    private int pageCount;
    private ArrayList<AllEventsDataPojo> allEvents;
    private static final String TAG = "MyEventsPageAdapter";
    private TimeZone timeZone;

    public MyEventsPageAdapter(Context mContext, ArrayList<AllEventsDataPojo> allEventsList,int pagecount) {
        this.mContext = mContext;
        allEvents = allEventsList;
        mActivity = (Activity) mContext;
        this.pageCount=pagecount;
        SharedPrefs prefs=new SharedPrefs(mContext);
        timeZone = Utilities.getTimeZoneFromLocation();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        view = LayoutInflater.from(mContext).inflate(R.layout.my_event_page_adapter, parent, false);
        return new ViewHLdr(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHLdr hldr = (ViewHLdr) holder;
        if(pageCount==0){
            String[] datetime;
            String dateandtime=dateTimeString(allEvents.get(position).getBookingRequestedFor());
            if(dateandtime!=null && !dateandtime.equals("")){
                datetime = dateandtime.split("\\|");
                hldr.tvMyEventProName.setText(datetime[0].trim());
                hldr.tvMyEventProName.setTypeface(AppTypeface.getInstance(mContext).getHind_regular());
                hldr.tvMyEvnetDateNTime.setText(datetime[1].trim());
            }
            hldr.ivMyEventProPic.setVisibility(View.GONE);
        }else{
            hldr.tvMyEventProName.setText(allEvents.get(position).getFirstName());
            if(!allEvents.get(position).getProfilePic().equals(""))
                Picasso.with(mContext).load(allEvents.get(position).getProfilePic())
                        .transform(new CircleTransform())
                        .resize((int) hldr.width, (int) hldr.height)
                        .into(hldr.ivMyEventProPic);
            timeMethod(hldr.tvMyEvnetDateNTime, allEvents.get(position).getBookingRequestedFor());
        }

        Log.d("TAG", "onBindViewHolderPast: ");

        if (hldr.timer != null) {
            hldr.timer.cancel();
            hldr.timer.onFinish();
        }

        hldr.tvMyEventRemaningTiming.setText("");
        switch (allEvents.get(position).getStatus()) {
            case 1:
                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
            case 2:

                //distanceInMiles(allEvents.get(position).getDistance(), hldr.tvMyEvnetProStatus);
                hldr.tvMyEvnetProStatus.setText("BID:"+allEvents.get(position).getBookingId()+"");
                hldr.tvMyEventStatus.setText("");
                hldr.pBMyEventRemainingTime.setVisibility(View.VISIBLE);
                long remainginTime = Utilities.timeStampS(allEvents.get(position).getBookingExpireTime()
                        , allEvents.get(position).getBookingRequestedAt());//Wass servertime and timestamp
                long maxtime = Utilities.timeStamp(allEvents.get(position).getBookingExpireTime(), allEvents.get(position)
                        .getBookingRequestedAt());
                hldr.pBMyEventRemainingTime.setMax((int) maxtime);
                if (remainginTime > 0) {

                   /* hldr.handler = new Handler();
                    hldr.handler.postDelayed(hldr.runnable,1000);
                    hldr.runnable = new Runnable() {
                        @Override
                        public void run() {
                           long l =  remainginTime-1000;
                            Log.d("TAG", "onTick: " + l);
                            hldr.pBMyEventRemainingTime.setProgress((int) l);
                            timeRemain(hldr.tvMyEventRemaningTiming, l);
                            hldr.handler.postDelayed(this,1000);
                        }
                    };*/


                    hldr.timer = new CountDownTimer(remainginTime, 1000) {
                        @Override
                        public void onTick(long l) {
                            Log.d("TAG", "onTick: " + l);
                            hldr.pBMyEventRemainingTime.setProgress((int) l/1000);
                            timeRemain(hldr.tvMyEventRemaningTiming, l);
                        }

                        @Override
                        public void onFinish() {
                            hldr.pBMyEventRemainingTime.setProgress(0);
                            hldr.tvMyEventRemaningTiming.setText("");
                        }
                    }.start();
                } else {
                    hldr.pBMyEventRemainingTime.setProgress(0);
                    hldr.tvMyEventRemaningTiming.setText("");
                }
                break;
            case 3:
            case 6:
            case 7:
            case 8:
            case 9:
                //distanceInMiles(allEvents.get(position).getDistance(), hldr.tvMyEvnetProStatus);
                hldr.tvMyEvnetProStatus.setText("BID:"+allEvents.get(position).getBookingId()+"");
                if (hldr.timer != null) {
                    hldr.timer.cancel();
                    hldr.timer.onFinish();
                }
                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
                hldr.tvMyEventRemaningTiming.setText("");
                hldr.tvMyEventStatus.setText(allEvents.get(position).getStatusMsg());
                break;
            case 4:

                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
                hldr.tvMyEvnetProStatus.setText(allEvents.get(position).getStatusMsg());
                hldr.tvMyEvnetProStatus.setTextColor(Utilities.getColor(mContext, R.color.red_login));
                break;
            case 5:
                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
                hldr.tvMyEvnetProStatus.setText(allEvents.get(position).getStatusMsg());
                hldr.tvMyEvnetProStatus.setTextColor(Utilities.getColor(mContext, R.color.safron));
                //If market place booking booking then the provider pic and Name is shown
                //else default is shown
                if(allEvents.get(position).getBookingModel()==2){
                    if(!allEvents.get(position).getProfilePic().equals("")){
                        Picasso.with(mContext).load(allEvents.get(position).getProfilePic())
                                .transform(new CircleTransform())
                                .resize((int) hldr.width, (int) hldr.height)
                                .into(hldr.ivMyEventProPic);
                    }
                    hldr.tvMyEventProName.setText(allEvents.get(position).getFirstName());
                }else{
                    //Default pic if booking expired
                    hldr.ivMyEventProPic.setImageResource(R.drawable.home_default_image);
                    hldr.tvMyEventProName.setText(R.string.booking_expired);
                }

                break;
            case 10:
                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
                hldr.tvMyEvnetProStatus.setText(allEvents.get(position).getStatusMsg());
                hldr.tvMyEvnetProStatus.setTextColor(Utilities.getColor(mContext, R.color.blue));
                break;
            case 11:
                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
                hldr.tvMyEvnetProStatus.setText(allEvents.get(position).getStatusMsg());
                hldr.tvMyEvnetProStatus.setTextColor(Utilities.getColor(mContext, R.color.livemblue3498));
                break;
            case 12:
                hldr.pBMyEventRemainingTime.setVisibility(View.INVISIBLE);
                hldr.tvMyEvnetProStatus.setText(allEvents.get(position).getStatusMsg());
                hldr.tvMyEvnetProStatus.setTextColor(Utilities.getColor(mContext, R.color.livemblue3498));
                //if(allEvents.get(position).getBookingModel()==2){
                if(!allEvents.get(position).getProfilePic().equals("")){
                    Picasso.with(mContext).load(allEvents.get(position).getProfilePic())
                            .transform(new CircleTransform())
                            .resize((int) hldr.width, (int) hldr.height)
                            .into(hldr.ivMyEventProPic);
                }else{
                    hldr.ivMyEventProPic.setImageResource(R.drawable.home_default_image);
                }
                if(allEvents.get(position).getFirstName().equals("")){
                    hldr.tvMyEventProName.setText(R.string.booking_cancelled);
                }else{
                    hldr.tvMyEventProName.setText(allEvents.get(position).getFirstName());
                }

                //}else{
                //Default pic if booking expired


                //}
                break;
        }

    }

    private void distanceInMiles(double distance, TextView tvDistance) {

        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        //  double kilometerDistance = Double.parseDouble(distance);//*1.6

        if(distance>0)
        {
            double kilometerDistance = distance/1000;
            Log.d("TAG", "distanceInMiles: "+VariableConstant.distanceUnit);
            if(!VariableConstant.distanceUnit.equals("Km away"))
            {
                kilometerDistance = kilometerDistance/1.6;
            }
            @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", kilometerDistance);
            String kilometr = kilometer + " " + VariableConstant.distanceUnit;
            tvDistance.setText(kilometr);
        }else{
            String kilometr = "0.00 " + VariableConstant.distanceUnit;
            tvDistance.setText(kilometr);
        }

    }

    private void timeMethod(TextView tvTime, long requestedTime) {

        try {
            Date date = new Date(requestedTime * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            sdf.setTimeZone(timeZone);
            String formattedDate = sdf.format(date);
            String splitDate[] = formattedDate.split(" ");
            String timeIS = splitDate[1] + " " + splitDate[2];
            String dat[] = splitDate[0].split("/");
            String month = Utilities.MonthName(Integer.parseInt(dat[0]));
            String year = dat[2];
            String bookinDate = dat[1] + " " + month + " " + year + " | " + timeIS;
            tvTime.setText(bookinDate);
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }
    private String dateTimeString(long requestedTime) {

        try {
            Date date = new Date(requestedTime * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            sdf.setTimeZone(timeZone);
            String formattedDate = sdf.format(date);
            String splitDate[] = formattedDate.split(" ");
            String timeIS = splitDate[1] + " " + splitDate[2];
            String dat[] = splitDate[0].split("/");
            String month = Utilities.MonthName(Integer.parseInt(dat[0]));
            String year = dat[2];
            String bookinDate = dat[1] + " " + month + " " + year + " | " + timeIS;
            return bookinDate;
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
        return "";
    }

    private void timeRemain(TextView tvMyEventRemaningTiming, long l) {
        tvMyEventRemaningTiming.setText(formatSeconds(l));
    }

    @SuppressLint("DefaultLocale")
    private String formatSeconds(long timeInSeconds) {

        long second = (timeInSeconds / 1000) % 60;
        long minute = (timeInSeconds / (1000 * 60)) % 60;

        return twoDigitString(minute) + " : " + twoDigitString(second);
    }

    private String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }
        if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }

    @Override
    public int getItemCount() {
        return allEvents.size();
    }

    public class ViewHLdr extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivMyEventProPic;
        private TextView tvMyEventProName, tvMyEvnetDateNTime, tvMyEvnetProStatus, tvMyEventRemaningTiming, tvMyEventStatus;
        private ProgressBar pBMyEventRemainingTime;
        private double size[];
        private double width, height;
        private CountDownTimer timer;
        private Handler handler;
        private Runnable runnable;

        private ViewHLdr(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            size = Scaler.getScalingFactor(mContext);
            width = 80 * size[0];
            height = 80 * size[1];
            AppTypeface typeFace = AppTypeface.getInstance(mContext);
            tvMyEventProName = itemView.findViewById(R.id.tvMyEventProName);
            tvMyEvnetDateNTime = itemView.findViewById(R.id.tvMyEvnetDateNTime);
            tvMyEvnetProStatus = itemView.findViewById(R.id.tvMyEvnetProStatus);
            tvMyEventRemaningTiming = itemView.findViewById(R.id.tvMyEventRemaningTiming);
            tvMyEventStatus = itemView.findViewById(R.id.tvMyEventStatus);
            ivMyEventProPic = itemView.findViewById(R.id.ivMyEventProPic);
            pBMyEventRemainingTime = itemView.findViewById(R.id.pBMyEventRemainingTime);
            tvMyEventProName.setTypeface(typeFace.getHind_semiBold());
            tvMyEventRemaningTiming.setTypeface(typeFace.getHind_regular());
            tvMyEventStatus.setTypeface(typeFace.getHind_regular());
            tvMyEvnetProStatus.setTypeface(typeFace.getHind_regular());
            tvMyEvnetDateNTime.setTypeface(typeFace.getHind_regular());
        }

        @Override
        public void onClick(View view) {
            Log.d("TAG", "onClick: " + allEvents.get(getAdapterPosition()).getStatus());
            AllEventsDataPojo alpojo = allEvents.get(getAdapterPosition());
            ArrayList<AllEventsDataPojo> alPojo = new ArrayList<>();
            alPojo.add(alpojo);
            Intent intnet;
            switch (allEvents.get(getAdapterPosition()).getStatus()) {
                //Pending
                case 1:
                case 2:
                case 15:
                    intnet = new Intent(mContext, BookingPendingAccept.class);
                    callNextActivity(intnet,alPojo,true);
                    break;
                //Upcoming
                case 3:
                case 6:
                case 7:
                case 8:
                case 9:
                    intnet = new Intent(mContext, LiveStatus.class);
                    callNextActivity(intnet,alPojo,false);
                    break;
                //Past
                //Completed
                case 10:
                    intnet = new Intent(mContext, CompletedInvoiceInfo.class);
                    callNextActivity(intnet,alPojo,true);
                    break;
                //cancel
                case 5://Expired
                case 4://Declined
                case 11://ProviderCancelled
                case 12://Cancelled
                    intnet = new Intent(mContext, CancelledBooking.class);
                    callNextActivity(intnet,alPojo,true);
                    break;
            }
        }

        private void callNextActivity(Intent intnet, ArrayList<AllEventsDataPojo> alPojo, boolean b)
        {


            Bundle bundle=new Bundle();
            bundle.putLong("BID", allEvents.get(getAdapterPosition()).getBookingId());
            bundle.putString("ImageUrl", allEvents.get(getAdapterPosition()).getProfilePic());
            bundle.putInt("STATUS", allEvents.get(getAdapterPosition()).getStatus());
            bundle.putInt("BMODEL", allEvents.get(getAdapterPosition()).getBookingModel());
            if(b)
                bundle.putSerializable("ALLPOJO", alPojo);
            intnet.putExtras(bundle);
            mContext.startActivity(intnet);
            mActivity.overridePendingTransition(R.anim.side_slide_out,R.anim.stay_still);
            //  }
        }
    }

    @Override
    public int getItemViewType(int position) {

        return 0;

    }

}
