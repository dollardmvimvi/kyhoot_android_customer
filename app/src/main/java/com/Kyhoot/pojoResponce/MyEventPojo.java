package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>MyEventPojo</h>
 * this bean takes the data from the server for the bookings
 * Created by Ali on 11/1/2017.
 */

public class MyEventPojo implements Serializable {

    private String message;
    private MyEventDataPojo data;

    public String getMessage() {

        return message;

    }

    public MyEventDataPojo getData() {

        return data;

    }
}
