package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.controllers.NewPasswordControllr;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.LoginResponceIntr;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.DialogInterfaceListner;

/**
 * <h>NewPassword</h>
 * <p>
 *
 * @see CheckNetworkAvailablity
 * @see LoginResponceIntr
 * this help to set the new password
 * </p>
 */
public class NewPassword extends AppCompatActivity implements CheckNetworkAvailablity,LoginResponceIntr {

    AlertProgress aprogress;
    ProgressDialog pDialog;
    private TextInputLayout nwPassTxtLayout, reNwPassTxtLayout;
    private String uSid;
    private AppTypeface appTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        appTypeface = AppTypeface.getInstance(this);
        initializeToolBar();
        initializeview();
        getIntnet();
    }

    private void getIntnet()
    {
        if(getIntent().getExtras()!=null)
        {
            uSid = getIntent().getStringExtra("SID");
        }
    }

    private void initializeview()
    {
        aprogress = new AlertProgress(this);
        pDialog = aprogress.getProgressDialog(getString(R.string.progresPasswrod));
        reNwPassTxtLayout = findViewById(R.id.reNwPassTxtLayout);
        nwPassTxtLayout = findViewById(R.id.nwPassTxtLayout);
        TextView tvNewMobileInfo = findViewById(R.id.tvNewMobileInfo);
        tvNewMobileInfo.setTypeface(appTypeface.getHind_medium());
        TextView tvNewPaswrdContinue = findViewById(R.id.tvNewPaswrdContinue);
        tvNewPaswrdContinue.setTypeface(appTypeface.getHind_semiBold());
        EditText etNewPassword = findViewById(R.id.etNewPassword);
        EditText etRenewPassword = findViewById(R.id.etRenewPassword);
        etNewPassword.setTypeface(appTypeface.getHind_regular());
        etRenewPassword.setTypeface(appTypeface.getHind_regular());
    }


    private void initializeToolBar()
    {
        Toolbar nwPaswrdTool = findViewById(R.id.nwPaswrdTool);
        setSupportActionBar(nwPaswrdTool);
        TextView tv_center = findViewById(R.id.tv_center);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.yourNewPasword);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        nwPaswrdTool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        nwPaswrdTool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void networkAvalble(LoginPojo loginPojo) {

    }

    @Override
    public void networkAvalble(String emailOrPhone, boolean isEmailOrPhone) {

        pDialog.show();
        pDialog.setCancelable(false);
        new NewPasswordControllr(this).chengePasswordService(emailOrPhone, uSid, this);
    }

    @Override
    public void notNetworkAvalble()
    {
        aprogress.showNetworkAlert();
    }

    public void onContinueClicked(View view)
    {
        new NewPasswordControllr(this).checkPawwordValidty(nwPassTxtLayout,reNwPassTxtLayout,this);
    }

    @Override
    public void onSuccess() {
        pDialog.dismiss();
        aprogress.alertPostivionclick("You have successfully reset your password. Please login", new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                Intent intnet = new Intent(NewPassword.this, ActivityLogin.class);
                intnet.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intnet);
                finish();
            }
        });
    }

    @Override
    public void onError(String errormsg)
    {
        pDialog.dismiss();
        aprogress.alertinfo(errormsg);
    }

    @Override
    public void onError(String errorMsg, Bundle loginpojo) {

    }
}
