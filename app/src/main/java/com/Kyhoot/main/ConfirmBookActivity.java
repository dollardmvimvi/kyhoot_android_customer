/*
package com.notary90210.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.notary90210.adapter.EventAdapter;
import com.notary90210.adapter.TimeAdapter;
import com.notary90210.controllers.ConfirmBookController;
import com.notary90210.interfaceMgr.UtilInterFace;
import com.notary90210.utilities.AlertProgress;
import com.notary90210.utilities.AppTypeface;
import com.notary90210.utilities.Scaler;
import com.notary90210.utilities.SharedPrefs;
import com.notary90210.utilities.Utilities;
import com.notary90210.utilities.VariableConstant;
import com.Kyhoot.pojoResponce.ProviderEvents;
import com.Kyhoot.pojoResponce.ProviderServiceDtls;

import java.util.ArrayList;

public class ConfirmBookActivity extends AppCompatActivity implements View.OnClickListener, UtilInterFace.ConfirmBook {

    double serviceFee = 0, gigFee = 0, discountFee = 0;
    private SharedPrefs sPrefs;
    private TextView tvConfirmAddCard, tvConfirmCardNumber, tvConfirmEventTime1, tvConfirmEventTime2,
            tvConfirmEventTime3, tvConfirmAppAddress, tvConfirmPromoApply,tvConfirmEventStart;
    private TextView tvServiceFeeAmt, tvDiscountAmount, tvTotalAmt,tvConfirmCard,tvConfirmCash
            ,tvSelectedEventTime;
    private EditText etConfirmPromoCodes;
    private View veConfirmevent1, veConfirmevent2, veConfirmevent3, veConfirmCard, veConfirmCash;
    private String imageurl = "", proName, reviewCount;
    private float rating;
    private ArrayList<ProviderServiceDtls> proServiceList;
    private ArrayList<ProviderEvents> eventses;
    private int paymentType = 1;
    private int bookingModel = 1;
    private int bookingTime = 30;
    private String proId, gigTimeId, eventId, promoCode, paymentCardId;
    private AlertProgress aProgress;
    private ConfirmBookController confirmBookCont;
    private ProgressBar progressBarConfirm;
    private int ADDRESS_CODE = 10;
    private LinearLayout llLiveFee;
    private AppTypeface appTypeface;
    private String currencySymbol;
    private double distance;
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_book);
        takeImage();
        initializeView();
        typeFace();
        initToolBar();
    }

    private void initToolBar() {

            Toolbar toolProvider = findViewById(R.id.toolbar);
            setSupportActionBar(toolProvider);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);
            toolProvider.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
    }

    private void takeImage() {
        aProgress = new AlertProgress(this);
        confirmBookCont = new ConfirmBookController(this);
        double[] size;

        size = Scaler.getScalingFactor(this);
        double width = (120) * size[0];
        double hight = (120) * size[1];
        Bundle bundle = getIntent().getExtras();


        if (bundle != null) {
            imageurl = bundle.getString("IMAGEURL");
            proName = bundle.getString("NAME");
            proId = bundle.getString("PROID");
            rating = bundle.getFloat("RATING");
            reviewCount = bundle.getString("REVIEWNUMBER");
            proServiceList = (ArrayList<ProviderServiceDtls>) bundle.getSerializable("PROSERVICELIST");
            eventses = (ArrayList<ProviderEvents>) bundle.getSerializable("EVENTS");
            distance = bundle.getDouble("DISTANCE");

        }

        //ImageView ivConfirmProfile = findViewById(R.id.ivConfirmProfile);

       */
/* if(!imageurl.equals("") ||imageurl!=null)
        {
            Picasso.with(this).load(imageurl)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .resize((int) width, (int) hight)
                    .into(ivConfirmProfile);
        }*//*


    }

    private void initializeView() {

        progressBarConfirm = findViewById(R.id.progressBarConfirm);
        veConfirmCard = findViewById(R.id.veConfirmCard);
        veConfirmCash = findViewById(R.id.veConfirmCash);
        tvConfirmPromoApply = findViewById(R.id.tvConfirmPromoApply);

        etConfirmPromoCodes = findViewById(R.id.etConfirmPromoCodes);
        tvServiceFeeAmt = findViewById(R.id.tvServiceFeeAmt);
        tvDiscountAmount = findViewById(R.id.tvDiscountAmount);
        tvTotalAmt = findViewById(R.id.tvTotalAmt);
        llLiveFee = findViewById(R.id.llLiveFee);
        appBarLayout = findViewById(R.id.app_bar);

*/
/*        RecyclerView recyclrViewConfirmTime = findViewById(R.id.recyclrViewConfirmTime);
        RecyclerView recyclrViewConfirmEvents = findViewById(R.id.recyclrViewConfirmEvents);*//*

        LinearLayoutManager llManagerTime = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager llManagerEvents = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
    */
/*    recyclrViewConfirmTime.setLayoutManager(llManagerTime);
        recyclrViewConfirmEvents.setLayoutManager(llManagerEvents);*//*


        TimeAdapter tAdapter = new TimeAdapter(this, proServiceList, true, this);
        //recyclrViewConfirmTime.setAdapter(tAdapter);
        tAdapter.notifyDataSetChanged();

        EventAdapter eventAdapter = new EventAdapter(this, eventses, true, this);
        //recyclrViewConfirmEvents.setAdapter(eventAdapter);
        eventAdapter.notifyDataSetChanged();
*/
/*
        LinearLayout lleventTime = findViewById(R.id.lleventTime);
        tvSelectedEventTime = findViewById(R.id.tvSelectedEventTime);
        tvConfirmEventStart = findViewById(R.id.tvConfirmEventStart);

        RelativeLayout rlConfirm30 = findViewById(R.id.rlConfirm30);
        RelativeLayout rlConfirm60 = findViewById(R.id.rlConfirm60);
        RelativeLayout rlConfirm120 = findViewById(R.id.rlConfirm120);
        tvConfirmEventTime1 = findViewById(R.id.tvConfirmEventTime1);
        tvConfirmEventTime2 = findViewById(R.id.tvConfirmEventTime2);
        tvConfirmEventTime3 =  findViewById(R.id.tvConfirmEventTime3);
        veConfirmevent1 = findViewById(R.id.veConfirmevent1);
        veConfirmevent2 = findViewById(R.id.veConfirmevent2);
        veConfirmevent3 = findViewById(R.id.veConfirmevent3);*//*

        if(VariableConstant.BOOKINGTYPE==2)
        {
            tvSelectedEventTime.setVisibility(View.VISIBLE);
           // lleventTime.setVisibility(View.GONE);
            tvConfirmEventStart.setText(getString(R.string.selectDateTime));
            tvSelectedEventTime.setText(VariableConstant.SelectedDateTime);
        }else
        {
           // rlConfirm30.setOnClickListener(this);
           // rlConfirm60.setOnClickListener(this);
           // rlConfirm120.setOnClickListener(this);
            selectStartEventTime(30);
        }


    }
    private void typeFace() {
        sPrefs = new SharedPrefs(this);
        appTypeface = AppTypeface.getInstance(this);
        currencySymbol = sPrefs.getCurrencySymbol();
        setTotalAmount(serviceFee, gigFee, discountFee);
        TextView tvConfirmBook = findViewById(R.id.tvConfirmBook);
       // TextView tvConfirmChoseGigTime = findViewById(R.id.tvConfirmChoseGigTime);
        TextView tvConfirmAppAdd = findViewById(R.id.tvConfirmAppAdd);
        TextView tvConfirmAppcngadd = findViewById(R.id.tvConfirmAppcngadd);
        //TextView tvConfirmEvent = findViewById(R.id.tvConfirmEvent);

        TextView tvConfirmPayment = findViewById(R.id.tvConfirmpayment);
        TextView tvConfirmName = findViewById(R.id.tvConfirmName);
        TextView tvConfirmNumrOfRevw = findViewById(R.id.tvConfirmNumrOfRevw);
        TextView tvConfirmDistance = findViewById(R.id.tvConfirmDistance);
        tvConfirmCard = findViewById(R.id.tvConfirmCard);
        tvConfirmCash = findViewById(R.id.tvConfirmCash);
        TextView tvConfirmPromoCode = findViewById(R.id.tvConfirmPromoCode);

        TextView tvServiceFee = findViewById(R.id.tvServiceFee);
        TextView tvPaymentBreak = findViewById(R.id.tvPaymentBreak);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotal = findViewById(R.id.tvTotal);

        tvServiceFee.setTypeface(appTypeface.getHind_regular());
        tvPaymentBreak.setTypeface(appTypeface.getHind_semiBold());
        tvDiscount.setTypeface(appTypeface.getHind_regular());
        tvTotal.setTypeface(appTypeface.getHind_bold());
        tvServiceFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvDiscountAmount.setTypeface(appTypeface.getHind_regular());
        tvTotalAmt.setTypeface(appTypeface.getHind_bold());
        tvSelectedEventTime.setTypeface(appTypeface.getHind_regular());
        tvConfirmAddCard = findViewById(R.id.tvConfirmAddCard);
        tvConfirmCardNumber = findViewById(R.id.tvConfirmCardNumber);
        RatingBar rtConfirm = findViewById(R.id.rtConfirm);
        rtConfirm.setIsIndicator(true);

        tvConfirmName.setText(proName);
        tvConfirmNumrOfRevw.setText(reviewCount);
        confirmBookCont.distanceCal(distance, tvConfirmDistance, this);
        rtConfirm.setRating(rating);

        tvConfirmAppAddress = findViewById(R.id.tvConfirmAppAddress);
        tvConfirmAppcngadd.setOnClickListener(this);
        tvConfirmAddCard.setOnClickListener(this);
        tvConfirmBook.setOnClickListener(this);
        tvConfirmCash.setOnClickListener(this);
        tvConfirmCard.setOnClickListener(this);
        tvConfirmPromoApply.setOnClickListener(this);
        tvConfirmAppAddress.setText(sPrefs.getBookingAddress());
        tvConfirmAppAddress.setTypeface(appTypeface.getHind_regular());
        tvConfirmCard.setTypeface(appTypeface.getHind_regular());
        tvConfirmCash.setTypeface(appTypeface.getHind_regular());
        if(!sPrefs.getDefaultCardNum().equals("") )
        {
            tvConfirmCard.setSelected(true);
            veConfirmCard.setVisibility(View.VISIBLE);
            tvConfirmAddCard.setText(R.string.change);
            tvConfirmAddCard.setVisibility(View.VISIBLE);
            tvConfirmCardNumber.setVisibility(View.VISIBLE);

            String card = sPrefs.getDefaultCardNum();
            Log.d("CARD", "typeFace: "+sPrefs.getDefaultCardNum());
            tvConfirmCardNumber.setText(card);
            paymentType = 2;
            paymentCardId = sPrefs.getDefaultCardId();
        }
        else
        {
            veConfirmCash.setVisibility(View.VISIBLE);
            tvConfirmCash.setSelected(true);
        }

        tvConfirmCardNumber.setTypeface(appTypeface.getHind_regular());
       // tvConfirmChoseGigTime.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmAppAdd.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmName.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmAppcngadd.setTypeface(appTypeface.getHind_medium());
        tvConfirmAddCard.setTypeface(appTypeface.getHind_medium());
        //tvConfirmEvent.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmEventStart.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmPayment.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmNumrOfRevw.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmDistance.setTypeface(appTypeface.getHind_medium());
        etConfirmPromoCodes.setTypeface(appTypeface.getHind_regular());
        tvConfirmPromoApply.setTypeface(appTypeface.getHind_regular());
        tvConfirmBook.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmPromoCode.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmPromoApply.setClickable(false);


        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTypeface(appTypeface.getHind_semiBold());
        appBarChangeListnr(proName);
        etConfirmPromoCodes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etConfirmPromoCodes.getText().toString().length() > 2) {
                    tvConfirmPromoApply.setTextColor(Utilities.getColor(ConfirmBookActivity.this, R.color.red_login));
                    tvConfirmPromoApply.setClickable(true);
                } else {
                    tvConfirmPromoApply.setClickable(false);
                    tvConfirmPromoApply.setTextColor(Utilities.getColor(ConfirmBookActivity.this, R.color.colorAccent));
                }
            }
        });
    }

    private void appBarChangeListnr(final String name) {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            boolean isShow = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0)
                {
                    Log.d("TAG", "onOffsetChanged: "+name);
                    collapsingToolbarLayout.setTitle(name);

                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle("");
                    // toolBarTitle.setText("");
                    isShow = false;
                }
            }
        });
    }

    private void setTotalAmount(double serviceFee, double gigFee, double discountFee) {
      //  String serve = currencySymbol + " " + serviceFee;
     //   String discount = currencySymbol + " " + discountFee;
        Utilities.setAmtOnRecept(serviceFee,tvServiceFeeAmt,currencySymbol);
      //  tvServiceFeeAmt.setText(serve);
       // tvDiscountAmount.setText(discount);
        Utilities.setAmtOnRecept(discountFee,tvDiscountAmount,currencySymbol);

        double total = serviceFee + gigFee + discountFee;
        Utilities.setAmtOnRecept(total,tvTotalAmt,currencySymbol);
        //String total = currencySymbol + " " + (serviceFee + gigFee + discountFee);
     //   tvTotalAmt.setText(total);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tvConfirmPromoApply:
                Toast.makeText(this, "API NOT AVAILABLE", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvConfirmBook:
                if(aProgress.isNetworkAvailable())
                    liveBooking();
                else
                    aProgress.showNetworkAlert();
                break;
            case R.id.tvConfirmAppcngadd:
                Intent intent = new Intent(this, AddressConfirm.class);
                startActivityForResult(intent, ADDRESS_CODE);

                break;
            case R.id.tvConfirmAddCard:
                Intent cardsIntent = new Intent(ConfirmBookActivity.this, ChangeCardActivity.class);
                startActivityForResult(cardsIntent, 1);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                getSupportActionBar().show();
                break;
            case R.id.tvConfirmCard:
                tvConfirmCash.setSelected(false);
                tvConfirmCard.setSelected(true);
                paymentType = 2;
                veConfirmCash.setVisibility(View.GONE);
                veConfirmCard.setVisibility(View.VISIBLE);
                tvConfirmAddCard.setVisibility(View.VISIBLE);
                tvConfirmCardNumber.setVisibility(View.VISIBLE);
                break;
            case R.id.tvConfirmCash:
                tvConfirmCash.setSelected(true);
                tvConfirmCard.setSelected(false);


                paymentType = 1;
                veConfirmCash.setVisibility(View.VISIBLE);
                tvConfirmAddCard.setVisibility(View.GONE);
                tvConfirmCardNumber.setVisibility(View.GONE);
                veConfirmCard.setVisibility(View.GONE);
                break;
        */
/*    case R.id.rlConfirm30:
                selectStartEventTime(30);
                break;
            case R.id.rlConfirm60:
                selectStartEventTime(60);
                break;
            case R.id.rlConfirm120:
                selectStartEventTime(120);
                break;*//*

        }
    }

    private void liveBooking() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBarConfirm.setVisibility(View.VISIBLE);
        confirmBookCont.checkNetwork(sPrefs, aProgress);
    }

    private void selectStartEventTime(int i) {
        veConfirmevent1.setVisibility(View.GONE);
        veConfirmevent2.setVisibility(View.GONE);
        veConfirmevent3.setVisibility(View.GONE);
        tvConfirmEventTime1.setSelected(false);
        tvConfirmEventTime2.setSelected(false);
        tvConfirmEventTime3.setSelected(false);
        switch (i) {
            case 30:
                veConfirmevent1.setVisibility(View.VISIBLE);
                tvConfirmEventTime1.setSelected(true);
                bookingTime = 30;
                break;
            case 60:
                veConfirmevent2.setVisibility(View.VISIBLE);
                tvConfirmEventTime2.setSelected(true);
                bookingTime = 60;
                break;
            case 120:
                veConfirmevent3.setVisibility(View.VISIBLE);
                tvConfirmEventTime3.setSelected(true);
                bookingTime = 120;
                break;
        }
    }

    @Override
    public void showProgress() {
        confirmBookCont.liveBooking(bookingModel, paymentType, proId, gigTimeId, eventId, promoCode, paymentCardId,bookingTime);
    }

    @Override
    public void hideProgress() {
        progressBarConfirm.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onGigTime(ProviderServiceDtls providerServiceDtls) {
        gigTimeId = providerServiceDtls.getId();
        llLiveFee.removeAllViews();
        View infltedView = LayoutInflater.from(ConfirmBookActivity.this).inflate(R.layout.single_service_price, llLiveFee, false);
        llLiveFee.addView(infltedView);
        TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
        TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);

        String namePerUnit = providerServiceDtls.getName() + " "
                + providerServiceDtls.getUnit();
        tvServiceName.setText(namePerUnit);
        Utilities.setAmtOnRecept(providerServiceDtls.getPrice(),tvServicePrice,sPrefs.getCurrencySymbol());
        tvServiceName.setTypeface(appTypeface.getHind_regular());
        tvServicePrice.setTypeface(appTypeface.getHind_regular());
        gigFee = providerServiceDtls.getPrice();
        setTotalAmount(serviceFee, gigFee, discountFee);
    }

    @Override
    public void onEventSelected(String eventId) {
        this.eventId = eventId;
    }

    @Override
    public void onError(String error) {
        if (error.equals(""))
            showProgress();
        else {
            aProgress.alertinfo(error);
            hideProgress();
        }
    }

    @Override
    public void onSessionError(String error) {
        Utilities.setMAnagerWithBID(ConfirmBookActivity.this, sPrefs,error);
    }

    @Override
    public void onSuccess() {
        VariableConstant.isConfirmBook = true;
        VariableConstant.SCHEDULEDDATE = "";
        VariableConstant.BOOKINGTYPE = 1;
        VariableConstant.SCHEDULEDTIME = 0;
        VariableConstant.CALLMQTTDATA = 0;
        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        VariableConstant.isSplashCalled = true;
        startActivity(intent);
        hideProgress();
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADDRESS_CODE) {
                if (data != null) {
                    String area = data.getStringExtra("AREA");
                    String latitude = data.getStringExtra("LATITUDE");
                    String longitude = data.getStringExtra("LONGITUDE");
                    sPrefs.setBookLatitude(latitude);
                    sPrefs.setBookLongitude(longitude);
                    sPrefs.setBookingAddress(area);
                    tvConfirmAppAddress.setText(area);
                }
            }

            if (requestCode == 1) {

                if (data.getExtras() != null) {

                    String cardNo = data.getStringExtra("cardNo");
                    paymentCardId = data.getStringExtra("cardID");
                    sPrefs.setDefaultCardId(paymentCardId);
                    //    Bitmap cardbitmap = data.getParcelableExtra("cardImage");
                    String card = "**** **** **** " + " " + cardNo;
                    paymentType = 2;
                    sPrefs.setDefaultCardNum(card);
                    tvConfirmCardNumber.setText(card);
                    //  iv_payment.setImageBitmap(cardbitmap);

                    boolean changeDefault = false;

                    while (!changeDefault) {
                        if (aProgress.isNetworkAvailable()) {

                            changeDefault = true;
                        } else {
                            aProgress.alertinfo("Check the Internet Connect");
                        }
                    }
                }
            }
        }
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utilities.checkAndShowNetworkError(this);
    }
}
*/
