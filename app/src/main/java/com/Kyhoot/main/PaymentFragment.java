package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.adapter.CustomListViewAdapter;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.models.CardPaymentModel;
import com.Kyhoot.pojoResponce.CardInfoPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * <h>PaymentFragment</h>
 * Created by ${Ali} on 8/16/2017.
 */

public class PaymentFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener
        , VerifyOtpIntrface.PaymentInterFace {
    Context mcontext;
    SharedPrefs sPrefs;
    AlertProgress aProgress;
    ProgressDialog pDialog;
    List<CardInfoPojo> rowItems;
    Resources resources;
    ListView lvPaymentCard;
    CardPaymentModel cardModel;
    private CustomListViewAdapter adapter;
    private ProgressBar progressBarPayment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        mcontext = getActivity();
        initializeView(view);
        return view;
    }
    /**
     * <p>Intiliazing view elements</p>
     */
    private void initializeView(View view) {
        mcontext = getActivity();
        AppTypeface appTypeface = AppTypeface.getInstance(mcontext);
        sPrefs = new SharedPrefs(mcontext);
        aProgress = new AlertProgress(mcontext);
        progressBarPayment = view.findViewById(R.id.progressBarPayment);
        progressBarPayment.setVisibility(View.VISIBLE);
     /*   pDialog = aProgress.getProgressDialog(mcontext.getString(R.string.wait));
        pDialog.setCancelable(false);*/
        lvPaymentCard = view.findViewById(R.id.lvPaymentCard);
        LayoutInflater footerinflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert footerinflater != null;
        View footerView = footerinflater.inflate(R.layout.footer_add_card, lvPaymentCard, false);
        TextView tvAddCard = footerView.findViewById(R.id.tvAddCard);
        RelativeLayout rlPaymntFragAddCard = footerView.findViewById(R.id.rlPaymntFragAddCard);
        lvPaymentCard.addFooterView(footerView);
        resources = mcontext.getResources();
        rowItems = new ArrayList<>();
        adapter = new CustomListViewAdapter(mcontext, R.layout.card_list_row, rowItems);
        lvPaymentCard.setAdapter(adapter);
        tvAddCard.setOnClickListener(this);
        rlPaymntFragAddCard.setOnClickListener(this);
        lvPaymentCard.setOnItemClickListener(this);
        cardModel = new CardPaymentModel();
        tvAddCard.setTypeface(appTypeface.getHind_regular());


    }

    /**
     * This is onResume() method, which is getting call each time.
     */
    @Override
    public void onResume() {
        super.onResume();
        if (aProgress.isNetworkAvailable()) {
            if (VariableConstant.isPaymentCalled) {
                progressBarPayment.setVisibility(View.VISIBLE);
                cardModel.getPaymentCard(this, sPrefs, mcontext);
            }

        } else
            aProgress.showNetworkAlert();
    }



    /**
     * <h3>onClick()</h3>
     * This is the overridden onClick() method which is used to handle the click events on the view
     *
     * @param v Parameter on which the click event happened
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rlPaymntFragAddCard:
            case R.id.tvAddCard:
                Intent intent = new Intent(mcontext, SignupPayment.class);
                intent.putExtra("coming_From", "payment_Fragment");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.side_slide_in, R.anim.side_slide_out);
                break;

        }

    }

    /**
     * This is an Overrided method got call when a row is clicked
     *
     * @param parent   parent instance
     * @param view     view instance
     * @param position position of row
     * @param id       id.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        CardInfoPojo row_details = (CardInfoPojo) lvPaymentCard.getItemAtPosition(position);
        Intent intent = new Intent(mcontext, DeleteCardActivity.class);
        intent.putExtras(cardModel.createBundle(row_details));
        startActivityForResult(intent, 2);

    }

    @Override
    public void onClearList() {
        rowItems.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String errorMsg) {
        //pDialog.dismiss();
        progressBarPayment.setVisibility(View.GONE);

    }

    @Override
    public void onResponceItem(CardInfoPojo cardInfoPojo)
    {
        rowItems.add(cardInfoPojo);
        String card = "**** **** **** " + " " + rowItems.get(0).getLast4();
        sPrefs.setDefaultCardNum(rowItems.get(0).getLast4());
        sPrefs.setDefaultCardId(rowItems.get(0).getId());
        sPrefs.setDefaultCardBrand(rowItems.get(0).getBrand());
        Log.d("TAG", "onResponceItemCARD: "+sPrefs.getDefaultCardNum() + " last "+rowItems.get(0).getLast4());
       // pDialog.dismiss();
        progressBarPayment.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

}
