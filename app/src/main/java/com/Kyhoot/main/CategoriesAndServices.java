package com.Kyhoot.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.Dialog.DescDialogFragment;
import com.Kyhoot.R;
import com.Kyhoot.adapter.CategoryAndServiceAdapter;
import com.Kyhoot.interfaceMgr.CategoriesAndServicesInterface;
import com.Kyhoot.models.CategoriesAndServicesModel;
import com.Kyhoot.pojoResponce.CartModifiedPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.pojoResponce.ServicePojo;
import com.Kyhoot.pojoResponce.ServicesData;
import com.Kyhoot.pojoResponce.ServicesPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

public class CategoriesAndServices extends AppCompatActivity implements View.OnClickListener,CategoriesAndServicesInterface {
    TextView numOfService,tvOr,tv_price,tvAdditionalFee,tvCheckout,toolBarTitle,tvAddtoCartHourPrice,tvAddToCartHourlyInfo,
            tvHireLabel,tvHireHourly,tvCartNoHr;
    RecyclerView rv_services;
    private static final String TAG = "CategoriesAndServices";
    SharedPrefs prefs;
    int numberOfServices=0;
    double totat_price=0.00;
    RelativeLayout checkOutLayout;
    ServicesPojo servicesPojo;
    Bundle bundle;
    CategoriesAndServicesModel categoriesAndServicesModel;
    CategoryAndServiceAdapter categoryAndServiceAdapter;
    DescDialogFragment dialogFragment;
    private Gson gson;
    private Toolbar toolProvider;
    double totalAmount=0.0;
    private CartModifiedPojo cartPojo;
    private ProgressBar progressbar;
    private LinearLayout ll_hourly_layout;
    private ImageView ivCartRemoveHr,ivCartAddHr;
    private LinearLayout llAddToCartHr;
    private AlertProgress alertProgress;
    private boolean isMinimumHour =false;
    int minimum_hour;
    private int totalQuantityInCart=0;
    ArrayList<ServicesData> servicePojoDataBackup;

     private  int serviceType; // hourly(2) or fixed(1)

    //Mandatory Select Category feature
    // Disabled Mandatory Select For now
    /*private boolean mandatorySelectArr[];
    private boolean isMandatorySelectToggle=false;
    private boolean isMandateFrom=true;
    private int mandatorySelectPosition=0;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_and_services);
        alertProgress = new AlertProgress(this);
        gson=new Gson();
        if(getIntent().hasExtra("result")){
            String json=getIntent().getStringExtra("result");
            Log.d(TAG, "onCreate: "+json);
            servicesPojo = gson.fromJson(json,ServicesPojo.class);
            servicePojoDataBackup = new ArrayList<>();
            servicePojoDataBackup.addAll(servicesPojo.getData());
            minimum_hour = servicesPojo.getCategoryData().getMinimum_hour();
        }
        initializeViews();
        setTypeface();
    }

    private void getCart() {
        if(Utilities.isNetworkAvailable(this)){
            showProgress();
            categoriesAndServicesModel.getCartServiceCall(prefs.getCategoryId(),this);
        }else{
            Toast.makeText(this,"Please check Internet connection.",Toast.LENGTH_SHORT).show();
        }

    }

    private void showProgress() {
        progressbar.setVisibility(View.VISIBLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    private void hideProgress(){

        if(progressbar!=null)
            progressbar.setVisibility(View.GONE);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void initializeViews() {
        prefs=new SharedPrefs(this);
        bundle = new Bundle();
        categoriesAndServicesModel=new CategoriesAndServicesModel(prefs,this);
        numOfService=findViewById(R.id.numOfService);
        tv_price=findViewById(R.id.tv_price);
        progressbar=findViewById(R.id.progress_bar);
        toolBarTitle=findViewById(R.id.toolBarTitle);
        toolProvider=findViewById(R.id.toolProvider);
        setSupportActionBar(toolProvider);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolProvider.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvOr=findViewById(R.id.tvOr);
        tvAdditionalFee=findViewById(R.id.tvAdditionalFee);
        tvCheckout=findViewById(R.id.tvCheckout);
        rv_services=findViewById(R.id.rv_services);
        tvAddtoCartHourPrice=findViewById(R.id.tvAddtoCartHourPrice);
        tvAddToCartHourlyInfo=findViewById(R.id.tvAddToCartHourlyInfo);
        tvHireLabel=findViewById(R.id.tvHireLabel);
        tvHireHourly=findViewById(R.id.tvHireHourly);
        ivCartRemoveHr=findViewById(R.id.ivCartRemoveHr);
        ivCartAddHr=findViewById(R.id.ivCartAddHr);
        tvCartNoHr=findViewById(R.id.tvCartNoHr);
        ll_hourly_layout=findViewById(R.id.ll_hourly_layout);
        llAddToCartHr=findViewById(R.id.llAddToCartHr);
        tvHireHourly.setOnClickListener(this);
        ivCartRemoveHr.setOnClickListener(this);
        ivCartAddHr.setOnClickListener(this);
        checkOutLayout=findViewById(R.id.checkOutLayout);
        checkOutLayout.setOnClickListener(this);
        //checkOutLayout.setEnabled(false);
        checkOutLayout.setVisibility(View.GONE);
        numOfService.setText(numberOfServices+" SERVICES");
        Log.d(TAG, "initializeViews: "+servicesPojo.getServicesData());
        categoryAndServiceAdapter = new CategoryAndServiceAdapter(this, servicesPojo.getServicesData(),this);
        ArrayList<ServicesData> servicesDataOriginal = servicesPojo.getServicesData();

        rv_services.setLayoutManager(new LinearLayoutManager(this));
        rv_services.setAdapter(categoryAndServiceAdapter);
        if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE))
        {
            double service_price_surge=0.0;
            try {
                if (cartPojo!=null)
                {
                    Log.e(TAG, "initializeViews: SURGE PRICE double  "+Double.parseDouble(VariableConstant.SURGE_PRICE));
                    service_price_surge = Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE) * cartPojo.getData().getTotalAmount()));
                    Log.e("TAG_SURGE", TAG + ": SURGE PRICE" + service_price_surge);
                    totalAmount = Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE) * cartPojo.getData().getTotalAmount()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            tv_price.setText(prefs.getCurrencySymbol() + " " + String.format("%.2f", (float) service_price_surge));

            //this is while intiliazing
        } else {
            tv_price.setText(prefs.getCurrencySymbol()+" "+String.format("%.2f",totat_price));
        }
        if(VariableConstant.BILLINGMODEL.equals("2")||VariableConstant.BILLINGMODEL.equals("6")){
            ll_hourly_layout.setVisibility(View.VISIBLE);
            tvAddtoCartHourPrice.setText(prefs.getSelectedCategoryName());
            if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE)){
                double service_price_surge=0.0;
                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*prefs.getCategoryHourlyPrice()));
                Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                tvAddToCartHourlyInfo.setText("Hire "+prefs.getSelectedCategoryName()+" @"+String.format("%.2f",service_price_surge)+" per hour");
            }else{
                tvAddToCartHourlyInfo.setText("Hire "+prefs.getSelectedCategoryName()+" @"+String.format("%.2f", prefs.getCategoryHourlyPrice())+" per hour");
            }

        }
        setTypeface();
        //subscribeToServiceObservable();
        //Disabled Mandatory Select For now
        //checkMandatoryCatSelect();
    }

    //Disabled Mandatory Select For now
    /*private void checkMandatoryCatSelect() {
        if (servicesPojo!=null&&servicesPojo.getServicesData().size()>0) {
            for (int i = 0; i < servicesPojo.getServicesData().size(); i++) {
                ServicesData servicesDataPojo = servicesPojo.getServicesData().get(i);
                if (servicesDataPojo.getService().size()>0) {
                    mandatorySelectArr = new boolean[servicesDataPojo.getService().size()];
                    for (int j=0; j<servicesDataPojo.getService().size();j++) {
                        ServicePojo servicePojo = servicesDataPojo.getService().get(j);
                        if (servicePojo.getMandatorySelect()==1) {
                            mandatorySelectArr[j]=true;
                        } else {
                            mandatorySelectArr[j]=false;
                        }
                    }
                }
            }
        }
    }*/

    private void setTypeface() {
        numOfService.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvOr.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tv_price.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvAdditionalFee.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvCheckout.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        toolBarTitle.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvAddtoCartHourPrice.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvAddToCartHourlyInfo.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvHireLabel.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvHireHourly.setTypeface(AppTypeface.getInstance(this).getHind_regular());
        tvCartNoHr.setTypeface(AppTypeface.getInstance(this).getHind_regular());
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCart();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
           /* case R.id.closebtn:
                finish();
                break;*/
            case R.id.checkOutLayout:
                //Toast.makeText(this,"To Confirmation page",Toast.LENGTH_SHORT).show();
                //Disabled Mandatory Select For now
                /*if (isMandateFrom) {
                    if (mandatorySelectArr != null && mandatorySelectArr.length>0) {
                        for (int i = 0; i < mandatorySelectArr.length; i++) {
                            if (mandatorySelectArr[i]) {
                                isMandatorySelectToggle = true;
                                mandatorySelectPosition = i;
                                break;
                            } else {
                                isMandatorySelectToggle = false;
                            }
                        }
                    }
                }
                if(isMandatorySelectToggle) {
                    if(servicesPojo!=null&&servicesPojo.getServicesData()!=null&&servicesPojo.getServicesData().size()>0) {
                        for(int j=0;j<servicesPojo.getServicesData().size();j++) {
                            alertProgress.alertinfo("Please Select the Mandatory category " + servicesPojo.getServicesData().get(j).getService().get(mandatorySelectPosition).getSer_name());
                        }
                    }
                } else {*/
                Intent intent = new Intent(CategoriesAndServices.this, ConfirmBookingActivity.class);
                intent.putExtra("SERVICEFEE", totalAmount);
                intent.putExtra("CARTDATA", cartPojo);
                startActivity(intent);
                overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                //Disabled Mandatory Select For now
                //}
                break;
            case R.id.ivCartRemoveHr:
                if(Utilities.isNetworkAvailable(this)){
                    //Check if the removed hour is less than or equal to the Minimum hour
                    // setup for the services and then reset it to minimum hour
                    int hourSelected=0;
                    if (tvCartNoHr.getText()!=null) {
                        try {
                            Log.e(TAG, "onClick: hourSelected " + tvCartNoHr.getText().toString().split(" ")[0]);
                            hourSelected = Integer.parseInt(tvCartNoHr.getText().toString().split(" ")[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (VariableConstant.MINIMUM_HOUR>0&&hourSelected!=0&&hourSelected<=VariableConstant.MINIMUM_HOUR) {
                        alertProgress.alertinfo("Cannot select lesser than the minimum hour "+VariableConstant.MINIMUM_HOUR);
                        tvCartNoHr.setText(VariableConstant.MINIMUM_HOUR+" hr");
                    } else {
                        onCartModified("1", 2, true);//2 for removing
                    }
                }else{
                    Toast.makeText(this,"Please check your internet connectivity !",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ivCartAddHr:
            case R.id.tvHireHourly:



                if(Utilities.isNetworkAvailable(this)){
                    if (totalQuantityInCart>=VariableConstant.MINIMUM_HOUR && serviceType==2 ) {
                        isMinimumHour=false;
                    } else {
                        isMinimumHour = true;
                    }
                    onCartModified("1", 1, true);
                }else {
                    Toast.makeText(this,"Please check your internet connectivity !",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onCartModified(String serviceId, int action, boolean isHourlyService) { // action 1 for adding and 2 for removing

        showProgress();
        //Disabled Mandatory Select For now
        /*if(!("1".equals(serviceId))) {
            if (action==2) {
                checkIfMandatoryServiceSelected(serviceId, action);
            } else if (action==1){
                isMandatorySelectToggle=false;
                isMandateFrom=false;
            }
        }*/
        //While adding other service - reset the other services from POST CART API CALL
        // For Either Or Selection of Services
        Log.e(TAG, "onCartModified: isMinimumHour  "+isMinimumHour);
        categoriesAndServicesModel.addCartData(serviceId, action, isHourlyService, isMinimumHour);
       /* if (isMinimumHour) {
            isMinimumHour=false;
        }*/
    }

    //Disabled Mandatory Select For now
    /*private void checkIfMandatoryServiceSelected(String serviceId,int action) {
            //checkMandatoryCatSelect();
        if (servicesPojo != null && servicesPojo.getServicesData().size() > 0) {
            for (int i=0; i < servicesPojo.getServicesData().size(); i++) {
                ServicesData servicesDataPojo = servicesPojo.getServicesData().get(i);
                if (servicesDataPojo.getService().size() > 0) {
                    mandatorySelectArr = new boolean[servicesDataPojo.getService().size()];
                    for (int j=0; j < servicesDataPojo.getService().size();j++) {
                        ServicePojo servicePojo = servicesDataPojo.getService().get(j);
                        if (servicePojo.getMandatorySelect()==1&&serviceId.equals(servicePojo.get_id())) {
                            isMandatorySelectToggle=true;
                            isMandateFrom=false;
                            break;
                        } else {
                            isMandatorySelectToggle=false;
                            isMandateFrom=true;
                        }
                    }
                }
            }
        }
    }*/

    @Override
    public void onSuccessCartDataAdd(String headerData, String result) {
        hideProgress();
        //servicesPojo.getData().clear();
        //RESETTING TEMPQUANT in services
        for(int i=0;i<servicePojoDataBackup.size();i++){
            for(int j=0;j<servicePojoDataBackup.get(i).getService().size();j++){
                servicePojoDataBackup.get(i).getService().get(j).setTempQuant(0);
            }
        }
        //servicesPojo.getData().addAll(servicePojoDataBackup);
       // getCart();
        categoryAndServiceAdapter.notifyDataSetChanged();
        categoryAndServiceAdapter.setCategoryLists(servicesPojo.getData());
        Log.d(TAG, "onSuccessCartDataAdd: "+result);
        if(headerData.equals("200"))
        {
            try{
                cartPojo=gson.fromJson(result,CartModifiedPojo.class);
                //getCart();
                VariableConstant.CARTID=cartPojo.getData().get_id();
                totalQuantityInCart = cartPojo.getData().getTotalQuntity();
                serviceType = cartPojo.getData().getServiceType();
                if(cartPojo.getMessage().equals("sucess")){
                    if(cartPojo.getData().getServiceType()==2){
                        if(cartPojo.getData().getTotalQuntity()==0){
                            llAddToCartHr.setVisibility(View.GONE);
                            tvHireHourly.setVisibility(View.VISIBLE);
                        }else{
                            llAddToCartHr.setVisibility(View.VISIBLE);
                            tvHireHourly.setVisibility(View.GONE);
                        }
                        tvCartNoHr.setText(cartPojo.getData().getTotalQuntity()+" hr");
                        resetFixedServices();
                    } else {
                        //Removed fixed selected services check
                        //resetFixedSelectedServices(cartPojo.getData());
                        //Setting the hourly to 0 if serviceType is 1
                        llAddToCartHr.setVisibility(View.GONE);
                        tvHireHourly.setVisibility(View.VISIBLE);
                    }
                    int count =0;
                    for(int i=0 ; i<cartPojo.getData().getItem().size() ; i++){
                        if(cartPojo.getData().getItem().get(i).getAmount()==0.0)
                        {
                        }else{
                            count++;
                        }
                    }
                    Log.i("TAG","count-->"+count);
                   // int count = cartPojo.getData().getItem().size();
                    numOfService.setText(count+" SERVICES");
                    if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE))
                    {
                        double service_price_surge=0.0;
                        try {
                                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*cartPojo.getData().getTotalAmount()));
                            Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                            totalAmount = Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*cartPojo.getData().getTotalAmount()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tv_price.setText(prefs.getCurrencySymbol() + " " + String.format("%.2f", (float) service_price_surge));

                        //this is while ading to cart
                    } else {
                        totalAmount = cartPojo.getData().getTotalAmount();
                        tv_price.setText(prefs.getCurrencySymbol() + " " + String.format("%.2f", (float) cartPojo.getData().getTotalAmount()));
                    }
                    if(cartPojo.getData().getTotalQuntity()>0){
                        checkOutLayout.setVisibility(View.VISIBLE);
                    }else{
                        checkOutLayout.setVisibility(View.GONE);
                    }

                    for (int i = 0; i < servicesPojo.getServicesData().size(); i++) {
                        ServicesData servicesDataPojo = servicesPojo.getServicesData().get(i);
                        for (int j = 0; j < servicesDataPojo.getService().size(); j++) {
                            ServicePojo servicePojo = servicesDataPojo.getService().get(j);
                            for (int k = 0; k < cartPojo.getData().getItem().size(); k++) {
                                Item item = cartPojo.getData().getItem().get(k);
                                if (servicePojo.get_id().equals(item.getServiceId())) {
                                    servicePojo.setTempQuant(item.getQuntity());
                                    Log.d(TAG, "onSuccessGetCart: " + "comparing");
                                }
                            }
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        Log.d(TAG, "onSuccessCartDataAdd: "+result);
    }

    @Override
    public void onFailureCartDataAdd(String headerError, String error) {
        hideProgress();
        Log.d(TAG, "onFailureCartDataAdd: "+error);
    }

    @Override
    public void onViewMore(double price, String serviceName, String description) {
        showServiceDesc(price,serviceName,description);
    }

    private void resetFixedServices()
    {
        for(int i=0;i<servicesPojo.getServicesData().size();i++){
            ServicesData servicesDataPojo=servicesPojo.getServicesData().get(i);
            for(int j=0;j<servicesDataPojo.getService().size();j++){
                ServicePojo servicePojo=servicesDataPojo.getService().get(j);
                servicePojo.setTempQuant(0);
            }
        }
        categoryAndServiceAdapter.notifyDataSetChanged();
    }

    // Removed for the either Or use case -
    /*private void resetFixedSelectedServices(CartData cartData) {
        for(int i=0;i<servicesPojo.getServicesData().size();i++){
            ServicesData servicesDataPojo=servicesPojo.getServicesData().get(i);
            for(int j=0;j<servicesDataPojo.getService().size();j++){
                ServicePojo servicePojo=servicesDataPojo.getService().get(j);
                if (!cartData.getItem().get(0).getServiceId().equals(servicePojo.get_id())) {
                    servicePojo.setTempQuant(0);
                }
            }
        }
        categoryAndServiceAdapter.notifyDataSetChanged();
    }*/

    @Override
    public void onSuccessGetCart(String headerData, String result) {
        hideProgress();
        switch (headerData) {
            case "200":
                try {
                    cartPojo = gson.fromJson(result, CartModifiedPojo.class);
                    VariableConstant.CARTID = cartPojo.getData().get_id();
                    Log.d(TAG, "onSuccessGetCart: " + cartPojo.getData().getTotalQuntity());
                    totalQuantityInCart=cartPojo.getData().getTotalQuntity();

                    int count =0;
                    for(int i=0 ; i<cartPojo.getData().getItem().size() ; i++){
                        if(cartPojo.getData().getItem().get(i).getAmount()==0.0)
                        {
                        }else{
                            count++;
                        }
                    }
                    Log.i("TAG","count-->"+count);
                    // int count = cartPojo.getData().getItem().size();
                    numOfService.setText(count+" SERVICES");
                 //   numOfService.setText(cartPojo.getData().getTotalQuntity() + " SERVICES");
                  //  numOfService.setText(cartPojo.getData().getItem().size()+" SERVICES");



                    Utilities.setAmtOnRecept(cartPojo.getData().getTotalAmount(), tv_price, prefs.getCurrencySymbol());
                    if (cartPojo.getData().getTotalQuntity() > 0 )
                    {
                        if(cartPojo.getData().getServiceType() ==2){
                            //Hourly service ui Update if there is data in cart if opened for first time
                            tvHireHourly.setVisibility(View.GONE);
                            llAddToCartHr.setVisibility(View.VISIBLE);
                            tvCartNoHr.setText(totalQuantityInCart + " hr");
                        }
                        checkOutLayout.setVisibility(View.VISIBLE);


                    } else {
                        checkOutLayout.setVisibility(View.GONE);
                    }
                    for (int i = 0; i < servicesPojo.getServicesData().size(); i++) {
                        ServicesData servicesDataPojo = servicesPojo.getServicesData().get(i);
                        for (int j = 0; j < servicesDataPojo.getService().size(); j++) {
                            ServicePojo servicePojo = servicesDataPojo.getService().get(j);
                            for (int k = 0; k < cartPojo.getData().getItem().size(); k++) {
                                Item item = cartPojo.getData().getItem().get(k);
                                if (servicePojo.get_id().equals(item.getServiceId())) {
                                    servicePojo.setTempQuant(item.getQuntity());
                                    Log.d(TAG, "onSuccessGetCart: " + "comparing");
                                }
                            }
                        }
                    }

                    if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE))
                    {
                        double service_price_surge=0.0;
                        try {
                            service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*cartPojo.getData().getTotalAmount()));
                            Log.e("TAG_SURGE", TAG+ ": SURGE PRICE"+service_price_surge);
                            totalAmount = Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*cartPojo.getData().getTotalAmount()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tv_price.setText(prefs.getCurrencySymbol() + " " + String.format("%.2f", (float) service_price_surge));

                        //this is while ading to cart
                    } else {
                        totalAmount = cartPojo.getData().getTotalAmount();
                        tv_price.setText(prefs.getCurrencySymbol() + " " + String.format("%.2f", (float) cartPojo.getData().getTotalAmount()));
                    }

                    categoryAndServiceAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        Log.d(TAG, "onSuccessGetCart: "+result);
    }

    @Override
    public void onFailureGetCart(String headerError, String error) {
        hideProgress();
        try{
            ErrorHandel errorHandel=gson.fromJson(error,ErrorHandel.class);
            switch (headerError){
                case "416":
                    //Cart not created
                    checkOutLayout.setVisibility(View.GONE);
                    break;
                case "440":
                    //Cart not created
                    categoriesAndServicesModel.getAccessToken(errorHandel.getMessage(),this);
                    checkOutLayout.setVisibility(View.GONE);
                    break;
            }
            Log.d(TAG, "onFailureGetCart: code:"+headerError+" ErrorMsg"+error);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSessionError(String message) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onSuccessAccessToken(String result) {
        try{
            JSONObject jsonobj = new JSONObject(result);
            prefs.setSession(jsonobj.getString("data"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showServiceDesc(double price, String serviceName, String desc){
        dialogFragment=new DescDialogFragment();
        bundle.putDouble("Price",price);
        bundle.putString("ServiceName",serviceName);
        bundle.putString("Description",desc);
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getSupportFragmentManager(),"SERVICEDESCRIPTION");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay_still,R.anim.slide_down_acvtivity);
    }
}
