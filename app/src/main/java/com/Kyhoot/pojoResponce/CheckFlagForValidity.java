package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>CheckFlagForValidity</h>
 * this class checks the validity of the User's Name, Email, password, DOB, and Mobile Number
 * Created by Ali on 8/31/2017.
 */

public class CheckFlagForValidity implements Serializable
{
   private boolean nFlg,eFlg,pFlg,mFlg,dobFlg,regFlg;

    public boolean isRegFlg() {
        return regFlg;
    }

    public void setRegFlg(boolean regFlg) {
        this.regFlg = regFlg;
    }

    public boolean isnFlg() {
        return nFlg;
    }

    public void setnFlg(boolean nFlg) {
        this.nFlg = nFlg;
    }

    public boolean iseFlg() {
        return eFlg;
    }

    public void seteFlg(boolean eFlg) {
        this.eFlg = eFlg;
    }

    public boolean ispFlg() {
        return pFlg;
    }

    public void setpFlg(boolean pFlg) {
        this.pFlg = pFlg;
    }

    public boolean ismFlg() {
        return mFlg;
    }

    public void setmFlg(boolean mFlg) {
        this.mFlg = mFlg;
    }

    public boolean isDobFlg() {
        return dobFlg;
    }

    public void setDobFlg(boolean dobFlg) {
        this.dobFlg = dobFlg;
    }

}
