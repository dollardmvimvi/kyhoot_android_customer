package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 30/5/18.
 */
public interface LaterBookingCallback {
    public void onBook(int bookingType,long scheduleTime,int hours);
    public void onCancel();
}
