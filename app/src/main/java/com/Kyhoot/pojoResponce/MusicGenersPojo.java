package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>MusicGenersPojo</h>
 * Created by Ali on 9/26/2017.
 */

public class MusicGenersPojo implements Serializable {
    private int errNum;
    private int errFlag;
    private String errMsg;
    private ArrayList<MusciGenresData> data;

    public int getErrNum() {
        return errNum;
    }

    public int getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public ArrayList<MusciGenresData> getData() {
        return data;
    }
}
