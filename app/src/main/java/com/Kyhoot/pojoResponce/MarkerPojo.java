package com.Kyhoot.pojoResponce;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.Serializable;

/**
 * Created by 3Embed
 * on 10/13/2017.
 */

public class MarkerPojo implements Serializable {
    private int status;
    private String image;
    private Marker markerId;
    private LatLng latLng;

    public MarkerPojo(int status, Marker markerId, String image) {
        this.status = status;
        this.markerId = markerId;
        this.image = image;
        //  this.latLng = latLng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Marker getMarkerId() {
        return markerId;
    }

    public void setMarkerId(Marker markerId) {
        this.markerId = markerId;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
