package com.Kyhoot.utilities;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Ali on 1/20/2018.
 */

public class OnMyService extends Service
{
    SharedPrefs sharedPrefs;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        sharedPrefs = new SharedPrefs(this);
        Log.e("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if(sharedPrefs==null){
            sharedPrefs=new SharedPrefs(this);
        }
        String cid = sharedPrefs.getCustomerSid();
        Log.e("ClearFromRecentService", "END "+cid+" MQTTRESPO "+ApplicationController.getInstance().isMqttConnected());
        //Code here

        if(ApplicationController.getInstance().isMqttConnected())
        {
            if(!cid.equals(""))
            {
                Log.d("ONMYSERVICE SHIJENTEST", "onTaskRemoved: unsubscribing");
                ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.JobStatus.value + "/" +cid);
                ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Provider.value + "/" +cid);
                ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Message.value + "/" + cid);
                if(!VariableConstant.LiveTrackBooking.equals(""))
                    ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.LiveTrack.value +"/"+VariableConstant.LiveTrackBooking);
                ApplicationController.getInstance().disconnect(cid);
            }
        }


        stopSelf();
    }

}
