package com.Kyhoot.pojoResponce;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <h>Support_Subcat_pojo</h>
 * Created by 3Embed on 10/5/2017.
 */

public class Support_Subcat_pojo implements Parcelable {
    @SuppressWarnings("unused")
    public static final Creator<Support_Subcat_pojo> CREATOR = new Creator<Support_Subcat_pojo>() {
        @Override
        public Support_Subcat_pojo createFromParcel(Parcel in) {
            return new Support_Subcat_pojo(in);
        }

        @Override
        public Support_Subcat_pojo[] newArray(int size) {
            return new Support_Subcat_pojo[size];
        }
    };
    private String Name;
    private String desc;
    private String link;

    protected Support_Subcat_pojo(Parcel in) {
        Name = in.readString();
        desc = in.readString();
        link = in.readString();
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return "ClassPojo [Name = " + Name + ", desc = " + desc + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(desc);
        dest.writeString(link);
    }
}
