package com.Kyhoot.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.main.AddressConfirm;
import com.Kyhoot.main.AddressFragment;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by embed on 21/8/15.
 * class Address_Adaptor
 */
public class Address_Adaptor extends BaseAdapter implements AdapterView.OnItemClickListener
{
    AppTypeface appTypeface;
    private Activity mContext;
    private ArrayList<AddressPojo.AddressData> adrListData = new ArrayList<>();
    private AddressFragment addressFragment;
    private AddressConfirm addressConfirm;
    private boolean flag;

    public Address_Adaptor(Activity context, ArrayList<AddressPojo.AddressData> addresslist, AddressFragment addressFragment) {
        super();
        this.adrListData = addresslist;
        this.mContext = context;
        this.addressFragment = addressFragment;
        appTypeface = AppTypeface.getInstance(mContext);
    }

    public Address_Adaptor(Activity context, ArrayList<AddressPojo.AddressData> addresslist, AddressConfirm addressConfirm, boolean flag) {
        super();
        this.adrListData = addresslist;
        this.mContext = context;
        this.addressConfirm = addressConfirm;
        appTypeface = AppTypeface.getInstance(mContext);
        this.flag = flag;
    }

    @Override
    public AddressPojo.AddressData getItem(int position) {
        return adrListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {

        return adrListData.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final AddressPojo.AddressData adrsItem = adrListData.get(position);
        ViewHolder holder;
        if(convertView==null||convertView.getTag() == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.view_address_entry, parent, false);
            holder.tv_addrs_typ = convertView.findViewById(R.id.tv_addrs_typ);
            holder.tv_addrs_loc = convertView.findViewById(R.id.tv_addrs_loc);
            holder.tv_addrs_del = convertView.findViewById(R.id.tv_addrs_del);
            holder.iv_addrs_typ = convertView.findViewById(R.id.iv_addrs_typ);
            holder.address1st = convertView.findViewById(R.id.address1st);

            if(!flag) {
                holder.tv_addrs_del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddressFragment addressSelect = addressFragment;
                        addressSelect.deletefromdb(position);
                    }
                });
            } else {
                holder.address1st.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AddressConfirm address_select = addressConfirm;
                        address_select.backaddress(position);
                    }
                });
                holder.tv_addrs_del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddressConfirm addressSelect = addressConfirm;
                        addressSelect.deletefromdb(position);
                    }
                });
            }

        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_addrs_typ.setText(adrsItem.getTaggedAs());
        holder.tv_addrs_typ.setTypeface(appTypeface.getHind_semiBold());
        holder.tv_addrs_loc.setText(adrsItem.getAddLine1());
        holder.tv_addrs_loc.setTypeface(appTypeface.getHind_light());
        holder.tv_addrs_del.setTypeface(appTypeface.getHind_regular());

        if (adrsItem.getTaggedAs().equals(mContext.getResources().getString(R.string.homeaddress))) {
            holder.iv_addrs_typ.setImageBitmap(Utilities.getBitmapFromVectorDrawable(mContext, R.drawable.ic_address_home_icon));
        } else if (adrsItem.getTaggedAs().equals(mContext.getResources().getString(R.string.officeaddress))) {
            holder.iv_addrs_typ.setImageBitmap(Utilities.getBitmapFromVectorDrawable(mContext, R.drawable.ic_address_briefcasae_icon));
        } else {
            holder.iv_addrs_typ.setImageBitmap(Utilities.getBitmapFromVectorDrawable(mContext, R.drawable.ic_address_heart_icon));
        }


        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

    }

    /*public Address_Adaptor(Activity context, ArrayList<Saving_Address> addresslist, Address_Confirm addressConfirm, boolean flag) {
        super();
        this.adrListData = addresslist;
        this.mContext = context;
        this.addressConfirm = addressConfirm;
        this.flag = flag;
    }*/
    private class ViewHolder {
        ImageView iv_addrs_typ;
        TextView tv_addrs_typ, tv_addrs_loc, tv_addrs_del;
        RelativeLayout address1st;

    }


}
