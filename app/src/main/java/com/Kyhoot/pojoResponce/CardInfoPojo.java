package com.Kyhoot.pojoResponce;

import android.graphics.Bitmap;

public class CardInfoPojo {

    private Bitmap cardImage;
    private String name;
    private String last4;
    private String id;
    private String brand;
    private String funding;
    private int expiryYear,expiryMonth;
    private boolean isDefault;


    public CardInfoPojo(Bitmap cardImage, String name, String last4, String id, String brand, String funding
            ,int expiryYear,int expiryMonth,boolean isDefault) {
        super();
        this.cardImage = cardImage;
        this.name = name;
        this.last4 = last4;
        this.id = id;
        this.brand = brand;
        this.funding = funding;
        this.expiryYear = expiryYear;
        this.expiryMonth = expiryMonth;
        this.isDefault = isDefault;
    }


    public int getExpiryYear() {
        return expiryYear;
    }

    public int getExpiryMonth() {
        return expiryMonth;
    }

    public Bitmap getCardImage() {
        return cardImage;
    }

    public String getName() {
        return name;
    }

    public String getLast4() {
        return last4;
    }

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getFunding() {
        return funding;
    }

    public boolean isDefault() {
        return isDefault;
    }
}
