package com.Kyhoot.controllers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.AccessTokenCallback;
import com.Kyhoot.interfaceMgr.RateYourProviderContract;
import com.Kyhoot.pojoResponce.AdditionalService;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.InvoiceDetails;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * <h>RateYourProviderImpl</h>
 * Created by Ali on 2/22/2018.
 */

public class RateYourProviderImpl implements RateYourProviderContract.Presenter
{

    private AccessTokenCallback accessTokenCallback;
    SharedPrefs manager;

    Gson gson;

    RateYourProviderContract.ViewContract viewContract;
    private static final String TAG = "RateYourProviderImpl";
    private Context mContext;

    public RateYourProviderImpl(RateYourProviderContract.ViewContract viewContract,AccessTokenCallback callback) {
        gson=new Gson();
        this.viewContract=viewContract;
        this.accessTokenCallback=callback;
    }

    @Override
    public void onInvoiceDetailsCalled(final long bid,SharedPrefs sharedPrefs) {
        this.manager=sharedPrefs;
        Log.d(TAG, "iNvoiceDetails: bid"+bid+" url: "+VariableConstant.SERVICE_URL + "booking/invoice/" + bid);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "booking/invoice/" + bid,
                OkHttpConnection.Request_type.GET, new JSONObject(), manager.getSession(), VariableConstant.SelLang,
                new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String response) {
                        try{
                            switch (headerData)
                            {
                                case "200":
                                    Log.d("TAG", "onNextInvoice: "+response);
                                    try{
                                        InvoiceDetails invoiceDetails = gson.fromJson(response, InvoiceDetails.class);
                                        viewContract.onGetInvoiceDetails(invoiceDetails.getData());
                                        viewContract.onHideProgress();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            ErrorHandel errorHandel = gson.fromJson(error,ErrorHandel.class);
                            switch (headerError) {
                                case "440":
                                    Utilities.getAccessToken(errorHandel.getData(), new AccessTokenCallback() {
                                        @Override
                                        public void onSessionExpired() {
                                            viewContract.onLogout("");
                                            viewContract.onHideProgress();
                                        }

                                        @Override
                                        public void onSuccessAccessToken(String data) {
                                            try {
                                                JSONObject jsonobj = new JSONObject(data);
                                                manager.setSession(jsonobj.getString("data"));
                                                onInvoiceDetailsCalled(bid,manager);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onFailureAccessToken(String headerError, String server_issue) {

                                        }
                                    });
                                    break;

                                case "498":
                                    viewContract.onLogout(errorHandel.getMessage());
                                    viewContract.onHideProgress();
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }


    @Override
    public void onUpdateReview(final long bId, final ArrayList<InvoiceDetails.CustomerRating> stringList, final String reviewMsg, final SharedPrefs sharedPrefs)
    {
        this.manager=sharedPrefs;

        JSONArray jsonArray = new JSONArray();
        for(InvoiceDetails.CustomerRating strList : stringList)
        {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name",strList.getName());
                jsonObject.put("id",strList.get_id());
                jsonObject.put("rating",strList.getRatings());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonArray.put(jsonObject);
        }
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("bookingId",bId);
            jsonObject.put("rating",jsonArray);
            jsonObject.put("review",reviewMsg );
        } catch (JSONException e) {
            e.printStackTrace();
        }



        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "reviewAndRating",
                OkHttpConnection.Request_type.POST, jsonObject, manager.getSession(), VariableConstant.SelLang,
                new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: UpdateReview "+result);
                        Log.d("TAG", "onNextInvoice: "+result);
                        viewContract.onHideProgress();
                        viewContract.onRateProviderSuccess();

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            final ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    Utilities.setMAnagerWithBID(mContext,sharedPrefs,errorHandel.getMessage());

                                    break;
                                case "440":
                                    Utilities.getAccessToken(errorHandel.getData(), new AccessTokenCallback() {
                                        @Override
                                        public void onSessionExpired() {
                                            Utilities.setMAnagerWithBID(mContext,sharedPrefs,errorHandel.getMessage());
                                        }

                                        @Override
                                        public void onSuccessAccessToken(String data) {
                                            manager.setSession(data);
                                            onUpdateReview(bId,stringList,reviewMsg,sharedPrefs);
                                        }

                                        @Override
                                        public void onFailureAccessToken(String headerError, String server_issue) {

                                        }
                                    });
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void timeMethod(TextView tbServiceAvailable, long bookingRequestedFor)
    {
        try {


            Log.d("TAGTIME", " expireTime " + bookingRequestedFor);
            Date date = new Date(bookingRequestedFor * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            sdf.setTimeZone(VariableConstant.TIMEZONE);

            String formattedDated = Utilities.getFormattedDate(date);
            tbServiceAvailable.setText(formattedDated);

        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }

    @Override
    public void getStringList()
    {
        ArrayList<String> stringList = new ArrayList();
        stringList.add("service");
        stringList.add("quality");
        stringList.add("behaviour");
        stringList.add("on time");
        stringList.add("other");
        stringList.add("cool");
        stringList.add("awesome");
        // viewContract.onGetStarList(stringList);
    }


    private Dialog indialog;
    @Override
    public void openDialog(Context mcontext, AppTypeface apptypeFace, InvoiceDetails.InvoiceData service, String signURL) {

        TextView tvInvoiceDialog, tvInvoiceDialogDismiss, tvInvGigTimeFee, tvInvGigTimeFeeAmt, tvInvDiscount, tvInvDiscountAmount,
                tvInvoiceTotal,tvLocation,tvsignature,tvReceiptDetailsHead,tvInvoiceTotalAmt,
                tvTravelAmount,tvTravelLabel,tvVisitAmount,tvVisitLabel,tvCancelAmount,tvCancelLabel,tvInvLastDuesFee,tvInvLastDuesFeeAmt;
        RelativeLayout rlCancelLayout,rlLastDuesFee,rltravelLayout,rlVisitFee,rldiscount;
        ImageView ivInvoiceSignature;
        LinearLayout llLiveFee;
        this.mContext=mcontext;
        indialog = new Dialog(mcontext);
        indialog.setCanceledOnTouchOutside(true);
        indialog.setCancelable(true);
        indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        indialog.setContentView(R.layout.popup_invoice_fee_breakdown);
        indialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tvInvoiceDialog = indialog.findViewById(R.id.tvInvoiceDialog);
        llLiveFee = indialog.findViewById(R.id.llLiveFee);
        tvInvoiceDialogDismiss = indialog.findViewById(R.id.tvInvoiceDialogDismiss);
        tvInvGigTimeFee = indialog.findViewById(R.id.tvInvGigTimeFee);
        tvInvGigTimeFeeAmt = indialog.findViewById(R.id.tvInvGigTimeFeeAmt);
        tvLocation = indialog.findViewById(R.id.tvLocation);
        tvInvDiscount = indialog.findViewById(R.id.tvInvDiscount);
        tvInvDiscountAmount = indialog.findViewById(R.id.tvInvDiscountAmount);
        tvInvoiceTotal = indialog.findViewById(R.id.tvInvoiceTotal);
        tvInvoiceTotalAmt = indialog.findViewById(R.id.tvInvoiceTotalAmt);
        ivInvoiceSignature = indialog.findViewById(R.id.ivInvoiceSignature);
        tvReceiptDetailsHead = indialog.findViewById(R.id.tvReceiptDetailsHead);
        tvTravelAmount = indialog.findViewById(R.id.tvTravelAmount);
        tvTravelLabel = indialog.findViewById(R.id.tvTravelLabel);
        tvVisitAmount = indialog.findViewById(R.id.tvVisitAmount);
        tvVisitLabel = indialog.findViewById(R.id.tvVisitLabel);
        tvCancelAmount = indialog.findViewById(R.id.tvCancelAmount);
        tvCancelLabel = indialog.findViewById(R.id.tvCancelLabel);
        tvInvLastDuesFee = indialog.findViewById(R.id.tvInvLastDuesFee);
        tvsignature = indialog.findViewById(R.id.tvsignature);
        tvInvLastDuesFeeAmt = indialog.findViewById(R.id.tvInvLastDuesFeeAmt);
        rlCancelLayout = indialog.findViewById(R.id.rlCancelLayout);
        rlLastDuesFee = indialog.findViewById(R.id.rlLastDuesFee);
        rldiscount = indialog.findViewById(R.id.rldiscount);
        rltravelLayout = indialog.findViewById(R.id.rltravelLayout);
        rlVisitFee = indialog.findViewById(R.id.rlVisitFee);
        tvInvoiceDialog.setTypeface(apptypeFace.getHind_bold());
        tvsignature.setTypeface(apptypeFace.getHind_bold());
        tvReceiptDetailsHead.setTypeface(apptypeFace.getHind_bold());
        tvInvoiceDialogDismiss.setTypeface(apptypeFace.getHind_bold());
        tvInvoiceTotal.setTypeface(apptypeFace.getHind_bold());
        tvInvoiceTotalAmt.setTypeface(apptypeFace.getHind_bold());
        tvInvDiscountAmount.setTypeface(apptypeFace.getHind_regular());
        tvTravelAmount.setTypeface(apptypeFace.getHind_regular());
        tvTravelLabel.setTypeface(apptypeFace.getHind_regular());
        tvVisitAmount.setTypeface(apptypeFace.getHind_regular());
        tvVisitLabel.setTypeface(apptypeFace.getHind_regular());
        tvCancelAmount.setTypeface(apptypeFace.getHind_regular());
        tvCancelLabel.setTypeface(apptypeFace.getHind_regular());
        tvInvDiscount.setTypeface(apptypeFace.getHind_regular());
        tvInvLastDuesFee.setTypeface(apptypeFace.getHind_regular());
        tvLocation.setTypeface(apptypeFace.getHind_regular());
        tvInvLastDuesFeeAmt.setTypeface(apptypeFace.getHind_regular());
        tvInvGigTimeFee.setTypeface(apptypeFace.getHind_regular());
        tvInvGigTimeFeeAmt.setTypeface(apptypeFace.getHind_regular());
        if (!signURL.equals("")) {
            Log.d(TAG, "openDialog: "+signURL);
            Picasso.with(mcontext)
                    .load(signURL)
                    .into(ivInvoiceSignature);
        }


        tvInvoiceDialogDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indialog.dismiss();
            }
        });
        if(service.getStatus()==12){
            rlCancelLayout.setVisibility(View.VISIBLE);
        }
        tvLocation.setText(service.getAddLine1());
        String currencySymbol = manager.getCurrencySymbol();
        setPaymentBreakdown(service,llLiveFee,apptypeFace);
        Utilities.setAmtOnRecept(service.getCart().getTotalAmount(),tvInvGigTimeFeeAmt,currencySymbol);
        Utilities.setAmtOnRecept(service.getAccounting().getCancellationFee(),tvCancelAmount,currencySymbol);
        if(service.getAccounting().getVisitFee()>0){
            rlVisitFee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getVisitFee(),tvVisitAmount,currencySymbol);
        }
        if(service.getAccounting().getTravelFee()>0){
            rltravelLayout.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getTravelFee(),tvTravelAmount,currencySymbol);
        }

        if(service.getAccounting().getLastDues()>0){
            rlLastDuesFee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getLastDues(),tvInvLastDuesFeeAmt,currencySymbol);
        }

        if (service.getAccounting().getDiscount()>0) {
            rldiscount.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(service.getAccounting().getDiscount(), tvInvDiscountAmount, service.getCart().getCurrencySymbol());
        }
        double totalamtFee = service.getAccounting().getTotal();
        Utilities.setAmtOnRecept(totalamtFee,tvInvoiceTotalAmt,service.getCart().getCurrencySymbol());
        indialog.show();
    }
    private void setPaymentBreakdown(InvoiceDetails.InvoiceData invoiceData, LinearLayout llLiveFee, AppTypeface apptypeFace) {
        int size=invoiceData.getCart().getCheckOutItem().size();
        for(int i=0;i<size;i++){
            Item item=invoiceData.getCart().getCheckOutItem().get(i);
            View infltedView = LayoutInflater.from(mContext).inflate(R.layout.single_service_price, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price

            String namePerUnit;
            if(invoiceData.getAccounting().getServiceType()==2){
                int accJobhr= (int) (invoiceData.getAccounting().getTotalActualJobTimeMinutes()/60);
                int accJobMin= (int) (invoiceData.getAccounting().getTotalActualJobTimeMinutes()%60);
                String actual_hrs;
                if(accJobMin>0) {
                    actual_hrs = accJobhr + " hr " + accJobMin + " min";
                }else{
                    actual_hrs = accJobhr + " hr " ;
                }
                namePerUnit=item.getServiceName() + " (x" + actual_hrs+")";
                Utilities.setAmtOnRecept(invoiceData.getAccounting().getTotalActualHourFee(),tvServicePrice,invoiceData.getCart().getCurrencySymbol());
            }else{
                if(item.getServiceName().trim().isEmpty()){
                    namePerUnit="Hourly"+" (x" + item.getQuntity()+")";
                }else{
                    namePerUnit = item.getServiceName() + " (x" + item.getQuntity()+")";
                }
                Utilities.setAmtOnRecept(item.getAmount(),tvServicePrice,invoiceData.getCart().getCurrencySymbol());
            }
            tvServiceName.setText(namePerUnit);
            tvServiceName.setTypeface(apptypeFace.getHind_regular());
            tvServicePrice.setTypeface(apptypeFace.getHind_regular());
        }
        size=invoiceData.getAdditionalService().size();
        for(int i=0;i<size;i++){
            AdditionalService item=invoiceData.getAdditionalService().get(i);
            View infltedView = LayoutInflater.from(mContext).inflate(R.layout.single_service_price, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit = item.getServiceName();
            tvServiceName.setText(namePerUnit);
            tvServicePrice.setText(invoiceData.getCart().getCurrencySymbol()+" "+item.getPrice());
            tvServiceName.setTypeface(apptypeFace.getHind_regular());
            tvServicePrice.setTypeface(apptypeFace.getHind_regular());
        }
        //Utilities.setAmtOnRecept(invoiceData.getCartdata().getTotalAmount(),tvInvoiceTotalAmt,invoiceData.getCartdata().getCurrencySymbol());
    }

    @Override
    public void onAddToFav(String providerId,String categoryId,String sessionToken)
    {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("providerId",providerId);
            jsonObject.put("categoryId",categoryId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewContract.onShowProgress();
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "favouriteProvider"
                , OkHttpConnection.Request_type.POST, jsonObject, sessionToken,
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        viewContract.onHideProgress();
                        Log.d("RATEPROVIDERIM", "onSuccess: "+result);
                        try {
                            viewContract.onFavAdded(new JSONObject(result).getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        viewContract.onHideProgress();
                        Log.d(TAG, "onError: headerError "+headerError);

                        if(headerError.equals("502"))
                        {
                            viewContract.onError("Internal server error");
                        }
                        else
                        {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        viewContract.onHideProgress();
                                        viewContract.onSessionExpired();
                                        break;
                                    case "440":
                                        Utilities.getAccessToken(errorHandel.getData(),accessTokenCallback);
                                        break;
                                    case "549":
                                        viewContract.onError("Socket Exception");
                                        break;
                                    case "411":
                                        viewContract.onFavAdded(new JSONObject(error).getString("message"));
                                        viewContract.onHideProgress();
                                        break;
                                    default:
                                        viewContract.onError(error);
                                        break;
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                            }

                        }

                    }
                });

    }

    /**
     * <h1>onRemoveFav</h1>
     * <p>
     *     This method is used to call service to remove a favorite provider
     * </p>
     * @param categoryId category id of the provider
     * @param providerId id of the provider
     * @param sessionToken sessionToken
     */
    @Override
    public void onRemoveFav(String providerId,String categoryId,String sessionToken) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "favouriteProvider/"+providerId+"/"+categoryId, OkHttpConnection.Request_type.DELETE, new JSONObject(), sessionToken, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String jsonResponse) {
                Log.d("unfavoriteProvider", "onSuccess: " + jsonResponse + " headerResponse " + headerResponse);
                try{
                    ErrorHandel errorHandel = gson.fromJson(jsonResponse, ErrorHandel.class);
                    viewContract.onUnfavoritingSuccessful(headerResponse,errorHandel.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String headerError, String error) {
                Log.d("favoriteProviders","headererror:" + headerError + ": error: " + error);
                if (headerError.equals("502"))
                    viewContract.onUnfavoritingError("502","");
                else {
                    try{
                        ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                        switch (headerError) {
                            case "498":
                                accessTokenCallback.onSessionExpired();
                                break;
                            case "440":
                                Utilities.getAccessToken(errorHandel.getData(),accessTokenCallback);
                                break;
                            default:
                                viewContract.onUnfavoritingError(headerError,errorHandel.getMessage());
                                break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

    }


}
