package com.Kyhoot.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.ViewPagerAdapter;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.MyEventPojo;
import com.Kyhoot.pojoResponce.MyEventStatus;
import com.Kyhoot.pojoResponce.MyEventStatusObservable;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.MqttEvents;
import com.Kyhoot.utilities.MyNetworkChangeListner;
import com.Kyhoot.utilities.NetworkChangeReceiver;
import com.Kyhoot.utilities.NotificationUtils;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * <h>MyEventsFrag</h>
 * Created by ${Ali} on 8/16/2017.
 */

public class MyEventsFrag extends Fragment   {

    private static final String TAG = "MyEventsFrag";
    private Context mcontext;
    private ViewPagerAdapter viewPagerAdapter;
    private ProgressBar myEventprogressBar;
    private SharedPrefs sPrefs;
    private Gson gson;
    private ViewPager viewPagerMyEvent;
    private TabLayout tabLayoutMyEvent;
    private AppTypeface appTypeface;
    private ArrayList<AllEventsDataPojo> pendingPojo = new ArrayList<>();
    private ArrayList<AllEventsDataPojo> upComingPojo = new ArrayList<>();
    private ArrayList<AllEventsDataPojo> pastPojo = new ArrayList<>();
    private MyEventsPageFrag pendingBooking, upComing, pastBooking;
    private boolean isRateProviderActivity = true;
    private Observer<MyEventStatus> observer;
    private AlertProgress alertProgress;
    private boolean isPopUpShowing = false;
    private CompositeDisposable disposable;
    private NotificationUtils notificationUtils;

    private MenuActivity mainActivity;

    private HomeFragment homeFrag;
    private MyEventsFrag myEnventFrag;
    private PaymentFragment paymentFrag;
    private ReviewFragment reviewFrag;
    private AddressFragment addressFrag;
    private FaqFragment faqFrag;
    private ShareFragment shareFrag;
    private HelpFragment helpFrag;
    private LiveMFragment liveMFrag;
    private FavProviderFragment favProviderFragment;
    private ProfileFragment profileFrag;


    private Toolbar toolBarMainAct;
    NavigationView navView;


    private String currentTag="";
    FragmentManager fragmentManager;
    private FragmentTransaction fTransaction;
    boolean isHomeOnTop=false;

    private long bId;
    private String proProfilePic = "";

    private int bookStatus = 0;
    //region Creating methods
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mcontext = getActivity();
        disposable = new CompositeDisposable();
        gson = new Gson();
        alertProgress = new AlertProgress(mcontext);


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myevents, container, false);
        initiateView(view);
        return view;
    }
    //endregion


    //region Initializing
    private void initiateView(View view) {
        VariableConstant.isConfirmBook = false;
        appTypeface = AppTypeface.getInstance(getActivity());
        notificationUtils = new NotificationUtils(getActivity());
        sPrefs = new SharedPrefs(getActivity());
        tabLayoutMyEvent = view.findViewById(R.id.tabLayoutMyEvent);
        viewPagerMyEvent = view.findViewById(R.id.viewPagerMyEvent);
        myEventprogressBar = view.findViewById(R.id.myEventprogressBar);
        tabLayoutMyEvent.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayoutMyEvent.setSelectedTabIndicatorHeight(5);
        tabLayoutMyEvent.setSelectedTabIndicatorColor(Utilities.getColor(mcontext, R.color.red_login));//getResources().getColor(R.color.actionbar_color)
        viewPagerMyEvent.setOffscreenPageLimit(3);
        viewPagerMyEvent.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutMyEvent));
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        tabLayoutMyEvent.setupWithViewPager(viewPagerMyEvent);

        mainActivity = (MenuActivity) getActivity();

        toolBarMainAct = view.findViewById(R.id.rlAbarMainAct);
        myEnventFrag = new MyEventsFrag();
        homeFrag = new HomeFragment();
        paymentFrag = new PaymentFragment();
        reviewFrag = new ReviewFragment();
        addressFrag = new AddressFragment();
        faqFrag = new FaqFragment();
        favProviderFragment=new FavProviderFragment();
        shareFrag = new ShareFragment();
        helpFrag = new HelpFragment();
        liveMFrag = new LiveMFragment();
        profileFrag = new ProfileFragment();

        navView = view.findViewById(R.id.nav_view);



        checkNetworkListener();

        if(Utilities.isNetworkAvailable(mcontext)) {
                initializeListener();
                callService();
            } else{
                alertProgress.showNetworkAlert();
        }

        oncreateLoadAdapter();

    }
    //endregion

    private void checkNetworkListener() {

        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Log.d("MyEvent"," NetworkStatus "+nwStatus);
                if(nwStatus)
                {

                    Log.d("TAGMQTT", "onNetworkStateChanges: "+ApplicationController.getInstance().isMqttConnected());

                }
            }
        });
    }

    private void oncreateLoadAdapter() {
        pendingBooking = MyEventsPageFrag.newInstancePen(0);
        viewPagerAdapter.addFragment(pendingBooking
                , getActivity().getResources().getString(R.string.pending));

        upComing = MyEventsPageFrag.newInstancePen(1);
        viewPagerAdapter.addFragment(upComing,
                getActivity().getResources().getString(R.string.UpComing));
        pastBooking = MyEventsPageFrag.newInstancePen(2);
        upComing.addParentFrag(MyEventsFrag.this);
        viewPagerAdapter.addFragment(pastBooking
                , getActivity().getResources().getString(R.string.Past));


        viewPagerMyEvent.setAdapter(viewPagerAdapter);
        ViewGroup vg = (ViewGroup) tabLayoutMyEvent.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int k = 0; k < tabsCount; k++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(k);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(appTypeface.getHind_regular());
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isPopUpShowing = true;
        VariableConstant.myEventOpen=true;
        VariableConstant.isLiveBokinOpen = true;
        Log.d("MYEVENTSFRAG SHIJENTEST", "onresume: "+VariableConstant.isLiveBokinOpen);
        VariableConstant.isBookingPending = false;
        if(ApplicationController.getInstance().isMqttConnected()){
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + sPrefs.getCustomerSid(), 1);
            Log.d("SHIJENTEST", "onResume: subscribing "+MqttEvents.JobStatus.value + "/" + sPrefs.getCustomerSid());
        } else {
            ApplicationController.getInstance().createMQttConnection(sPrefs.getCustomerSid());
            toSubscribe();
        }
        Log.w("SHIJTENTEST", "onResumeMYEvent: "+VariableConstant.isLiveBokinOpen);

        if(observer!=null)
        {
            MyEventStatusObservable.getInstance().replay();
        }

    }

    private void toSubscribe()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("SHIJENTEST", "run: "+MqttEvents.JobStatus.value + "/" + sPrefs.getCustomerSid());
                ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + sPrefs.getCustomerSid(), 1);
            }
        },1000);
    }

    public void callService() {


        myEventprogressBar.setVisibility(View.VISIBLE);
        Log.d("MYEVENTFRAG", "callService: bookings:session="+sPrefs.getSession());
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "bookings",
                OkHttpConnection.Request_type.GET, new JSONObject(), sPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        pendingPojo.clear();
                        upComingPojo.clear();
                        pastPojo.clear();
                        Log.d("MYEVENT", "onSuccessEVENT: " + result);
                        try {
                            MyEventPojo myEventPojo = gson.fromJson(result, MyEventPojo.class);
                            pendingPojo.addAll(myEventPojo.getData().getPending());
                         //   upComingPojo.addAll(myEventPojo.getData().getUpcoming());
                            pastPojo.addAll(myEventPojo.getData().getPast());

                            long fromDate = getStartOfDayInMillisToday();
                            long toDate = getEndOfDayInMillisToday();
                            callOnDateSelectApi(fromDate,toDate);
                            /*notifyiAdapter();
                            myEventprogressBar.setVisibility(View.GONE);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }

    /**
     * this method is to get the data or booking on selection of particular date from the horizoantal calender
     * @author sai
     * @since 17-julay-2019.
     *
     */

    public  void callOnDateSelectApi(long fromDate, long toDate)
    {
        myEventprogressBar.setVisibility(View.VISIBLE);
        long finalFromDate = fromDate/1000;
        long finalToDate = toDate/1000;

        JSONObject jsonObject = new JSONObject();

        Log.d("resendOtp", "resendOtp: " + jsonObject);

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "bookings/"+finalFromDate+"/"+finalToDate,
                OkHttpConnection.Request_type.GET, jsonObject, sPrefs.getSession(), VariableConstant.SelLang,
                new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String result)
                    {
                        upComingPojo.clear();
                        MyEventPojo myEventPojo = gson.fromJson(result, MyEventPojo.class);
                        Log.d("resendOtp", "onSuccess: " + result + " headerResponse " + headerResponse);
                        upComingPojo.addAll(myEventPojo.getData().getUpcoming());
                        notifyiAdapter(true);
                        myEventprogressBar.setVisibility(View.GONE);



                    }

                    @Override
                    public void onError(String headerError, String error)
                    {

                        Gson gon = new Gson();
                        ErrorHandel errorHandel = gon.fromJson(error, ErrorHandel.class);

                    }
                });
    }

    private void notifyiAdapter(boolean isFirstOrItemAdded) {
        if (pendingBooking != null) {
            pendingBooking.notifyDataAdapter(pendingPojo,1,isFirstOrItemAdded);
        }
        if (upComing != null) {
            upComing.notifyDataAdapter(upComingPojo,1,isFirstOrItemAdded);
        }
        if (pastBooking != null) {
            pastBooking.notifyDataAdapter(pastPojo,1,isFirstOrItemAdded);
        }
    }

    private void initializeListener()
    {

        observer = new Observer<MyEventStatus>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable.add(d);
            }

            @Override
            public void onNext(MyEventStatus myEvents) {

                Log.d(TAG, "onNextLive: " + myEvents.getData().getStatus());
                final MyEventStatus myEvent = myEvents;
                if(bookStatus==myEvents.getData().getStatus() && myEvents.getData().getStatus()!=8){
                    //Checking 8 as the status is used many times
                    //when job is paused and started
                    return;
                }
                bookStatus = myEvents.getData().getStatus();
                VariableConstant.BOOKINGTYPEMODEL = myEvent.getData().getBookingModel();

                /*Log.d("MYEVENT TAG", "onNext: " + myEvents.getData().getStatus()+" "+myEvents.getData().getStatusMsg());
                int dataIndex = myEvents.getData().getStatus();
                int index;
                final MyEventStatus myEvent = myEvents;
                Log.d("TAG", "onNextisPopUpShowing: "+isPopUpShowing);*/

                switch (myEvents.getData().getStatus()) {
                    case 1:
                        //New booking from dispatcher
                        callService();
                        break;
                    case 3://booking accepted

                        if(!VariableConstant.isBookingPending)
                        {
                            for (int i = 0; i < pendingPojo.size(); i++) {
                                if (myEvent.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
                                    //  index = i;
                                    pendingPojo.get(i).setStatus(myEvent.getData().getStatus());
                                    pendingPojo.get(i).setStatusMsg(myEvent.getData().getStatusMsg());
                                    pendingPojo.get(i).setProfilePic(myEvent.getData().getProProfilePic());
                                    pendingPojo.get(i).setFirstName(myEvent.getData().getFirstName());
                                    pendingPojo.get(i).setLastName(myEvent.getData().getLastName());
                                    upComingPojo.add(0, pendingPojo.get(i));
                                    pendingPojo.remove(i);
                                    notifyiAdapter(false);
                                    break;
                                }
                            }
                            if(sPrefs.getBookingStatus(myEvent.getData().getBookingId())<myEvent.getData().getStatus()){
                                sPrefs.setBookingStatus(""+myEvent.getData().getBookingId(),myEvent.getData().getStatus());
                                Intent intent = new Intent(getActivity(),LiveStatus.class);
                                intent.putExtra("BID", myEvent.getData().getBookingId());
                                intent.putExtra("STATUS", myEvent.getData().getStatus());
                                intent.putExtra("ImageUrl",myEvent.getData().getProProfilePic());
                                notificationUtils.showJustNotification
                                        ("LIVESTATUS", myEvents.getData().getStatusMsg(),myEvents.getData().getMsg(), intent);
                            }
                        }else {
                            for (int i = 0; i < pendingPojo.size(); i++) {
                                if (myEvent.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
                                    //  index = i;
                                    pendingPojo.get(i).setStatus(myEvent.getData().getStatus());
                                    pendingPojo.get(i).setStatusMsg(myEvent.getData().getStatusMsg());
                                    upComingPojo.add(0, pendingPojo.get(i));
                                    pendingPojo.remove(i);
                                    notifyiAdapter(false);
                                    isPopUpShowing = true;
                                    break;
                                }
                            }
                        }

                        tabLayoutMyEvent.getTabAt(1).select();
                        if(myEvent.getData().getBookingType()==2)
                            Utilities.checkAndAddEvent(getActivity(),myEvent.getData().getBookingId(),myEvent.getData().getBookingRequestedFor(),sPrefs);
                        break;
                    case 5://booking ignored
                    case 4:// if booking rejected

                        /*if(!VariableConstant.isBookingPending)
                        {
                            for (int i = 0; i < pendingPojo.size(); i++) {
                                if (myEvent.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
                                    //   index = i;
                                    pendingPojo.get(i).setStatus(myEvent.getData().getStatus());
                                    pendingPojo.get(i).setStatusMsg(myEvent.getData().getStatusMsg());
                                    pastPojo.add(0, pendingPojo.get(i));
                                    pendingPojo.remove(i);
                                    notifyiAdapter();
                                    break;
                                }

                            }
                        }else
                        {
                            for (int i = 0; i < pendingPojo.size(); i++) {
                                if (myEvents.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
                                    index = i;
                                    pendingPojo.get(index).setStatus(myEvents.getData().getStatus());
                                    pendingPojo.get(index).setStatusMsg(myEvents.getData().getStatusMsg());
                                    pastPojo.add(0, pendingPojo.get(index));
                                    pendingPojo.remove(index);
                                    notifyiAdapter();
                                    break;
                                }
                            }
                        }*/
                        callService();
                        if(sPrefs.getBookingStatus(myEvent.getData().getBookingId())<myEvent.getData().getStatus()){
                            sPrefs.setBookingStatus(""+myEvent.getData().getBookingId(),myEvent.getData().getStatus());
                            notificationUtils.showJustNotification
                                    ("", myEvents.getData().getStatusMsg(),myEvents.getData().getMsg(), new Intent());
                        }

                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        changeAssignedStatus(myEvents);
                        if(sPrefs.getBookingStatus(myEvent.getData().getBookingId())<myEvent.getData().getStatus()){
                            sPrefs.setBookingStatus(""+myEvent.getData().getBookingId(),myEvent.getData().getStatus());
                            Intent intent = new Intent(getActivity(),LiveStatus.class);
                            intent.putExtra("BID", myEvent.getData().getBookingId());
                            intent.putExtra("STATUS", myEvent.getData().getStatus());
                            intent.putExtra("ImageUrl",myEvent.getData().getProProfilePic());
                            notificationUtils.showJustNotification
                                    ("LIVESTATUS",myEvents.getData().getStatusMsg(),myEvents.getData().getMsg(),intent);
                        }
                        break;
                    case 10://booking completed
                        Log.d("TAG", "onNextisLiveB: "+ VariableConstant.isLiveBookingOpen);
                        if(!VariableConstant.isLiveBookingOpen) {
                            if(isRateProviderActivity) {
                                isRateProviderActivity = false;
                                Intent intent = new Intent(mcontext, RateYourBooking.class);
                                intent.putExtra("BID", myEvents.getData().getBookingId());
                                intent.putExtra("PROIMAGE", myEvents.getData().getProProfilePic());
                                startActivity(intent);
                            }
                        }
                        break;
                    case 11: // booking cancelled

                        VariableConstant.SHOWPOPUP = true;
                        for (int i = 0; i < upComingPojo.size(); i++)
                        {
                            if (myEvent.getData().getBookingId() == upComingPojo.get(i).getBookingId()) {
                                upComingPojo.get(i).setStatus(myEvent.getData().getStatus());
                                upComingPojo.get(i).setStatusMsg(myEvent.getData().getStatusMsg());
                                pastPojo.add(0, upComingPojo.get(i));
                                upComingPojo.remove(i);
                                notifyiAdapter(false);
                                isPopUpShowing = true;
                                break;
                            }
                        }


                        if(VariableConstant.BOOKINGTYPEMODEL==1){
                            AlertDialog.Builder builder=new AlertDialog.Builder(mcontext);
                            builder.setMessage("Booking cancelled by the provider ");
                            builder.setPositiveButton(" OK", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    VariableConstant.SCHEDULEAVAILABLE = true;
                                    dialog.dismiss();
                                    if (VariableConstant.SCHEDULEAVAILABLE)
                                    {
                                        AlertDialog.Builder builder2=new AlertDialog.Builder(mcontext);
                                        builder2.setMessage(getString(R.string.book_another_provider));
                                        builder2.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if(alertProgress.isNetworkAvailable()) {

                                                    triggerNewScheduleBooking(myEvent.getData().getBookingId());
                                                    tabLayoutMyEvent.getTabAt(0).select();
                                                    VariableConstant.SCHEDULEAVAILABLE = false;
                                                } else {
                                                    alertProgress.showNetworkAlert();
                                                }
                                                dialog.dismiss();
                                            }
                                        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                        builder2.create().show();

                                    }
                                }
                            });
                            builder.create().show();
                        }



                        if(sPrefs.getBookingStatus(myEvent.getData().getBookingId())<myEvent.getData().getStatus()){
                            sPrefs.setBookingStatus(""+myEvent.getData().getBookingId(),myEvent.getData().getStatus());
                            notificationUtils.showJustNotification
                                    ("", myEvents.getData().getStatusMsg(),myEvents.getData().getMsg(), new Intent());
                        }
                        break;
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
            }
        };
        MyEventStatusObservable.getInstance().subscribe(observer);
        MyEventStatusObservable.getInstance().subscribeOn(Schedulers.io());
    }



    private void changeAssignedStatus(final MyEventStatus myEvents) {
        for (int i = 0; i < upComingPojo.size(); i++) {
            if (myEvents.getData().getBookingId() == upComingPojo.get(i).getBookingId()) {
                upComingPojo.get(i).setStatusMsg(myEvents.getData().getStatusMsg());
                upComingPojo.get(i).setStatus(myEvents.getData().getStatus());
                notifyiAdapter(false);
                break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VariableConstant.myEventOpen=false;
        Log.w("TAG", "onPauseonDestroy: ");
        VariableConstant.isLiveBokinOpen = false;
        //Log.d("MYEVENTSFRAG SHIJENTEST", "ondestroy: "+VariableConstant.isLiveBokinOpen);
        if(ApplicationController.getInstance().isMqttConnected()){
            //Log.d("MYEVENTFRAG SHIJENTEST", "onDestroy: mqtt unsubscribing "+MqttEvents.JobStatus.value + "/" +sPrefs.getCustomerSid());
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.JobStatus.value + "/" +sPrefs.getCustomerSid());
        }

        MyEventStatusObservable.getInstance().removeObserver(observer);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.clear();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        VariableConstant.isLiveBokinOpen = false;
        //Log.w("MYEVENTSFRAG SHIJENTEST", "onPauseMyEvent: "+VariableConstant.isLiveBokinOpen);
    }


    /**
     * This method is called to initiate a new booking when the
     * scheduled booking which has been accepted by provider earlier
     * with the previous bookingId after we show a popup to end user to book or not.
     * if he clicks "OK" then call this API to initiate a new Booking
     * @author Pramod
     * @since 15-May-2019.
     * @param bookingId previous bookingId to trigger the new booking in the system
     */
    public void triggerNewScheduleBooking(long bookingId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "bookingByBookingId",
                OkHttpConnection.Request_type.POST, jsonObject, sPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                          //  Toast.makeText(getContext(), "My EventFrag", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "onSuccess: "+result);
                            VariableConstant.SHOWPOPUP = false;
                            callService();
                            mainActivity.loadMyEvent();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }

    private void loadFragment(Fragment fragment1, String title, String TAG)
    {
        currentTag=TAG;
        Log.d(TAG, "loadFragment: "+TAG);
        fragmentManager = getActivity().getSupportFragmentManager();
        fTransaction = fragmentManager.beginTransaction();
        // fTransaction.addToBackStack(TAG);
        callHomeFrag(TAG, fTransaction);
    }

    private void callHomeFrag(String TAG, FragmentTransaction fTransaction)
    {
        Intent intent;
        switch (TAG) {
            case "MYEVENT":
                isHomeOnTop=false;
                if (paymentFrag.isAdded())
                    fTransaction.hide(paymentFrag);
                if (favProviderFragment.isAdded())
                    fTransaction.hide(favProviderFragment);
                if (reviewFrag.isAdded())
                    fTransaction.hide(reviewFrag);
                if (addressFrag.isAdded())
                    fTransaction.hide(addressFrag);
                if (faqFrag.isAdded())
                    fTransaction.hide(faqFrag);
                if (shareFrag.isAdded())
                    fTransaction.hide(shareFrag);
                if (helpFrag.isAdded())
                    fTransaction.hide(helpFrag);
                if (liveMFrag.isAdded())
                    fTransaction.hide(liveMFrag);
                if (homeFrag.isAdded())
                    fTransaction.hide(homeFrag);
                if (profileFrag.isAdded())
                    fTransaction.hide(profileFrag);
                if (myEnventFrag.isAdded()) {
                    CheckAddedFrag("", "");
                    fTransaction.show(myEnventFrag);
                    fTransaction.addToBackStack("HOME");

                    myEnventFrag.onResume();
                } else {
                    CheckAddedFrag("", "");
                    fTransaction.add(R.id.frameLayoutMainAct, myEnventFrag, TAG);
                    fTransaction.addToBackStack("HOME");
                }
                break;
        }
        fTransaction.commit();
    }

    private void CheckAddedFrag(String isAddress, String isPayment) {

        if (!isAddress.equals("")) {
            VariableConstant.isAddressCalled = true;
            VariableConstant.isReviewCalled = false;
            VariableConstant.isPaymentCalled = false;
        } else if (!isPayment.equals("")) {
            VariableConstant.isAddressCalled = false;
            VariableConstant.isPaymentCalled = true;
            VariableConstant.isReviewCalled = false;

        } else {
            VariableConstant.isAddressCalled = false;
            VariableConstant.isReviewCalled = false;
            VariableConstant.isPaymentCalled = false;
        }
    }

    public long getStartOfDayInMillisToday() {
        // Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance(Utilities.getTimeZoneFromLocation());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }
    public long getEndOfDayInMillisToday() {
        // Add one day's time to the beginning of the day.
        // 24 hours * 60 minutes * 60 seconds * 1000 milliseconds = 1 day
        return getStartOfDayInMillisToday() + (24 * 60 * 60 * 1000)-1000;
    }
}

