package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 15/5/18.
 */
public interface AccessTokenCallback {
    public void onSessionExpired();
    public void onSuccessAccessToken(String data);
    public void onFailureAccessToken(String headerError, String server_issue);
}
