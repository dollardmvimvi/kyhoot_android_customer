package com.Kyhoot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.ProviderServiceDtls;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;

import java.util.ArrayList;

/**
 * <h>TimeAdapter</h>
 * this shows the slot time of the particular provider
 * Created by 3Embed on 10/16/2017.
 */

public class TimeAdapter extends RecyclerView.Adapter {
    private UtilInterFace.ConfirmBook confirmBook;
    private Context mcontext;
    private ArrayList<ProviderServiceDtls> mArrylist;
    private int selectedPosition = -1;
    private boolean isConfirmBooking;
    private String currencySymbol;

    public TimeAdapter(Context mcontext, ArrayList<ProviderServiceDtls> mArrylist, boolean isConfirmBooking, UtilInterFace.ConfirmBook confirmBook) {
        this.mcontext = mcontext;
        this.mArrylist = mArrylist;
        if (isConfirmBooking)
            selectedPosition = 0;
        this.confirmBook = confirmBook;
        this.isConfirmBooking = isConfirmBooking;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mcontext).inflate(R.layout.single_provider_timer, parent, false);
        return new ViewHldr(mView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHldr hldr = (ViewHldr) holder;
        String nameUnit = mArrylist.get(position).getName() + " " + mArrylist.get(position).getUnit();
        hldr.tvProSingleTime.setText(nameUnit);
        hldr.tvProSingleamount.setText(currencySymbol + " " + mArrylist.get(position).getPrice());

        if (position == selectedPosition) {
            if (confirmBook != null)
                mArrylist.get(position);
            confirmBook.onGigTime(mArrylist.get(position));
            //confirmBook.onGigTimeArrys( mArrylist.get(position));
            hldr.tvProSingleTime.setSelected(true);
            hldr.tvProSingleamount.setSelected(true);
            hldr.timerView.setVisibility(View.VISIBLE);
        } else {
            hldr.tvProSingleTime.setSelected(false);
            hldr.tvProSingleamount.setSelected(false);
            hldr.timerView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrylist.size();
    }

    private class ViewHldr extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvProSingleTime, tvProSingleamount;
        LinearLayout llProSingleTimer;
        View timerView;
        AppTypeface apptypeFace;

        private ViewHldr(View itemView) {
            super(itemView);
            if (isConfirmBooking)
                itemView.setOnClickListener(this);

            SharedPrefs sharedPrefs = new SharedPrefs(mcontext);

            apptypeFace = AppTypeface.getInstance(mcontext);
            timerView = itemView.findViewById(R.id.timerView);
            tvProSingleTime = itemView.findViewById(R.id.tvProSingleTime);
            llProSingleTimer = itemView.findViewById(R.id.llProSingleTimer);
            tvProSingleamount = itemView.findViewById(R.id.tvProSingleamount);
            AppTypeface typeFace = AppTypeface.getInstance(mcontext);
            tvProSingleTime.setTypeface(typeFace.getHind_regular());
            tvProSingleamount.setTypeface(typeFace.getHind_regular());
            tvProSingleTime.setTypeface(apptypeFace.getHind_regular());
            tvProSingleamount.setTypeface(apptypeFace.getHind_regular());
            // currencySymbol = mcontext.getString(R.string.)
            currencySymbol = sharedPrefs.getCurrencySymbol();

        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selectedPosition);
            selectedPosition = getAdapterPosition();
            notifyItemChanged(selectedPosition);
        }
    }

}
