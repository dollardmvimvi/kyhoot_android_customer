package com.Kyhoot.zendesk.zendesk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.zendesk.ZendeskController;
import com.Kyhoot.zendesk.ZendeskInterFace;
import com.Kyhoot.zendesk.zendeskadapter.HelpIndexRecyclerAdapter;
import com.Kyhoot.zendesk.zendeskadapter.SpinnerAdapter;
import com.Kyhoot.zendesk.zendeskpojo.SpinnerRowItem;
import com.Kyhoot.zendesk.zendeskpojo.ZendexDataEvent;
import com.Kyhoot.zendesk.zendeskpojo.ZendexHistory;

import java.util.ArrayList;
import java.util.Date;

/**
 * <h>HelpIndexTicketDetails</h>
 * Created by Ali on 12/28/2017.
 */

public class HelpIndexTicketDetails extends AppCompatActivity implements ZendeskInterFace
{
    public static final Integer[] priorityColor = { R.color.green_continue ,
            R.color.livemblue3498,R.color.red_login_dark,
            R.color.safron,};
    private String[] priorityTitles;
    private AppTypeface appTypeface;
    private ArrayList<SpinnerRowItem> rowItems;
    private boolean isToAddTicket;
    private SharedPrefs sharedPrefs;
    private ZendeskController zendexController;
    private int zenId;
    private HelpIndexRecyclerAdapter helpIndexRecyclerAdapter;
    private  EditText etHelpIndexSubjectPre,etWriteMsg,etHelpIndexSubject;
    private TextView tvHelpIndexDateNTimePre,spinnerHelpIndexPre;
    private String subject,priority;
    private ArrayList<ZendexDataEvent> zendexDataEvents = new ArrayList<>();
    private ProgressBar progressBar;
    private AlertProgress alertProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.activity_help_index_ticket);
        isToAddTicket = getIntent().getBooleanExtra("ISTOAddTICKET",false);
        if(getIntent().getExtras()!=null)
        {
            zenId = getIntent().getIntExtra("ZendeskId",0);
        }
        priorityTitles = new String[] {getString(R.string.priorityLow),
                getString(R.string.priorityNormal),
                getString(R.string.priorityUrgent),
                getString(R.string.priorityHigh),
                 };
        initializeArrayList();
        initializeToolBar();
        initializeView();

    }

    private void initializeArrayList() {
        appTypeface = AppTypeface.getInstance(this);
        sharedPrefs = new SharedPrefs(this);
        zendexController = new ZendeskController(this,sharedPrefs,this);
        rowItems = new ArrayList<>();
        rowItems.add(new SpinnerRowItem(priorityColor[0],priorityTitles[0]));
        rowItems.add(new SpinnerRowItem(priorityColor[1],priorityTitles[1]));
        rowItems.add(new SpinnerRowItem(priorityColor[2],priorityTitles[2]));
        rowItems.add(new SpinnerRowItem(priorityColor[3],priorityTitles[3]));
    }

    private void initializeView()
    {
        alertProgress = new AlertProgress(this);
        if(isToAddTicket)
        {
            CardView cardHelpIndexTicket = findViewById(R.id.cardHelpIndexTicket);
            TextView tvHelpIndexImageText = findViewById(R.id.tvHelpIndexImageText);
            ImageView ivHelpIndexImage = findViewById(R.id.ivHelpIndexImage);
            TextView tvHelpIndexCustName = findViewById(R.id.tvHelpIndexCustName);
            TextView tvHelpIndexDateNTime = findViewById(R.id.tvHelpIndexDateNTime);
            etHelpIndexSubject = findViewById(R.id.etHelpIndexSubject);
            ImageView ivHelpCenterPriority = findViewById(R.id.ivHelpCenterPriority);
            Spinner spinnerHelpIndex = findViewById(R.id.spinnerHelpIndex);
            etHelpIndexSubject.setTypeface(appTypeface.getHind_regular());
            tvHelpIndexDateNTime.setTypeface(appTypeface.getHind_regular());
            tvHelpIndexCustName.setTypeface(appTypeface.getHind_medium());
            tvHelpIndexImageText.setTypeface(appTypeface.getHind_semiBold());
            priority = rowItems.get(0).getPriority();
            SpinnerAdapter adapter = new SpinnerAdapter(this,R.layout.spinner_adapter,R.id.tvSpinnerPriority, rowItems);
            spinnerHelpIndex.setAdapter(adapter);
            Log.d("TAG", "initializeView: "+spinnerHelpIndex.getSelectedItem().toString());

            spinnerHelpIndex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.d("TAG", "onItemSelected: "+i);
                    priority = rowItems.get(i).getPriority();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            cardHelpIndexTicket.setVisibility(View.VISIBLE);
            String name = sharedPrefs.getFirstname()+" "+sharedPrefs.getLastname();
            tvHelpIndexCustName.setText(name);
            char c = name.charAt(0);
            tvHelpIndexImageText.setText(c+"");
            Date date = new Date(System.currentTimeMillis());
            String dateTime[] = Utilities.getFormattedDate(date).split(",");
            String timeToSet =  dateTime[0]+" | "+dateTime[1];
            tvHelpIndexDateNTime.setText(timeToSet);



        }else
        {

            progressBar = findViewById(R.id.progress_bar); 
            CardView cardHelpIndexTicketPre = findViewById(R.id.cardHelpIndexTicketPre);
            TextView tvHelpIndexImageTextPre = findViewById(R.id.tvHelpIndexImageTextPre);
            ImageView ivHelpIndexImagePre = findViewById(R.id.ivHelpIndexImagePre);
            TextView tvHelpIndexCustNamePre = findViewById(R.id.tvHelpIndexCustNamePre);
            tvHelpIndexDateNTimePre = findViewById(R.id.tvHelpIndexDateNTimePre);
            etHelpIndexSubjectPre = findViewById(R.id.etHelpIndexSubjectPre);
            ImageView ivHelpCenterPriorityPre = findViewById(R.id.ivHelpCenterPriorityPre);
             spinnerHelpIndexPre = findViewById(R.id.spinnerHelpIndexPre);
            etHelpIndexSubjectPre.setEnabled(false);
            spinnerHelpIndexPre.setTypeface(appTypeface.getHind_regular());
            tvHelpIndexCustNamePre.setTypeface(appTypeface.getHind_medium());
            tvHelpIndexImageTextPre.setTypeface(appTypeface.getHind_semiBold());
            etHelpIndexSubjectPre.setTypeface(appTypeface.getHind_regular());
            tvHelpIndexDateNTimePre.setTypeface(appTypeface.getHind_regular());
            cardHelpIndexTicketPre.setVisibility(View.VISIBLE);
            String name = sharedPrefs.getFirstname()+" "+sharedPrefs.getLastname();
            tvHelpIndexCustNamePre.setText(name);
            char c = name.charAt(0);
            tvHelpIndexImageTextPre.setText(c+"");
            progressBar.setVisibility(View.VISIBLE);
            zendexController.callApiToGetTicketInfo(zenId);
        }

        helpIndexRecyclerAdapter = new HelpIndexRecyclerAdapter(this,zendexDataEvents);
        RecyclerView recyclerViewHelpIndex = findViewById(R.id.recyclerViewHelpIndex);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewHelpIndex.setLayoutManager(linearLayoutManager);
        recyclerViewHelpIndex.setAdapter(helpIndexRecyclerAdapter);
        etWriteMsg = findViewById(R.id.etWriteMsg);
        TextView tvHelpIndexSend = findViewById(R.id.tvHelpIndexSend);

        etWriteMsg.setTypeface(appTypeface.getHind_regular());
        tvHelpIndexSend.setTypeface(appTypeface.getHind_medium());


        tvHelpIndexSend.setOnClickListener(sendQueryMsg());

    }

    private View.OnClickListener sendQueryMsg()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               String trim = etWriteMsg.getText().toString().trim();
                if(!trim.isEmpty())
                {
                    //if() check Network
                    if(isToAddTicket)
                    {

                        subject = etHelpIndexSubject.getText().toString().trim();
                        if(!subject.isEmpty())
                        {
                            zendexController.callApiToCreateTicket(trim,subject,priority);
                            setAndNotifyAdapter(sharedPrefs.getFirstname()+" "+sharedPrefs.getLastname(),trim);
                            etHelpIndexSubject.setEnabled(false);
                            isToAddTicket = false;
                            Utilities.hideKeyboard(HelpIndexTicketDetails.this);

                        }
                        else
                            Toast.makeText(HelpIndexTicketDetails.this,"Please add subject",Toast.LENGTH_SHORT).show();
                          etWriteMsg.setText("");
                       //   Utilities.hideKeyboard(HelpIndexTicketDetails.this);
                      //  setAndNotifyAdapter(sharedPrefs.getFirstname()+" "+sharedPrefs.getLastname(),trim);

                    }
                    else
                    {
                        zendexController.callApiToCommentOnTicket(trim,zenId);
                        etWriteMsg.setText("");
                        setAndNotifyAdapter(sharedPrefs.getFirstname()+" "+sharedPrefs.getLastname(),trim);
                        Utilities.hideKeyboard(HelpIndexTicketDetails.this);
                    }

                }

            }
        };
    }

    private void setAndNotifyAdapter(String name, String trim)
    {
        long timeStsmp = System.currentTimeMillis()/1000;
        ZendexDataEvent dataEvent = new ZendexDataEvent();
        dataEvent.setBody(trim);
        dataEvent.setName(name);
        dataEvent.setTimeStamp(timeStsmp);
        zendexDataEvents.add(dataEvent);
        helpIndexRecyclerAdapter.notifyDataSetChanged();
    }


    /*
    initialize toolBar
     */
    private void initializeToolBar()
    {
        Toolbar toolBarLayout = findViewById(R.id.tool_helpindex_ticket);
        setSupportActionBar(toolBarLayout);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        if(isToAddTicket)
            tv_center.setText(R.string.newTicket);
        else
            tv_center.setText(R.string.ticket);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolBarLayout.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolBarLayout.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String result)
    {

        ZendexHistory zendexHistory = new Gson().fromJson(result,ZendexHistory.class);
        zendexDataEvents.addAll(zendexHistory.getData().getEvents());
        helpIndexRecyclerAdapter.notifyDataSetChanged();
        etHelpIndexSubjectPre.setText(zendexHistory.getData().getSubject());
        Date date = new Date(zendexHistory.getData().getTimeStamp() * 1000L);
        String dateTime[] = Utilities.getFormattedDate(date).split(",");
        String timeToSet =  dateTime[0]+" | "+dateTime[1];
        tvHelpIndexDateNTimePre.setText(timeToSet);
        subject = zendexHistory.getData().getSubject();
        priority = zendexHistory.getData().getPriority();
        spinnerHelpIndexPre.setText(priority);
        onHideProgress();

    }

    @Override
    public void onZendexTicketAdded(String msg)
    {
        onBackPressed();
    }

    @Override
    public void onError(String errMsg) {
      onHideProgress();
        alertProgress.alertinfo(errMsg);
    }

    @Override
    public void onSessionExpired(String sessionMsg)
    {
        onHideProgress();
    }
}
