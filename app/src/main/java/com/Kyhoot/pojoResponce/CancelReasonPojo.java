package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>CancelReasonPojo</h>
 * Created by Ali on 11/15/2017.
 */

public class CancelReasonPojo implements Serializable {
    private String message;
    private CancelReasonData data;

    public String getMessage() {
        return message;
    }

    public CancelReasonData getData() {
        return data;
    }

    public class CancelReasonData implements Serializable {

        private ArrayList<Reason> reason;

        private String cancellationFee;

        private boolean cancellationFeeApplied;

        public ArrayList<Reason> getReason ()
        {
            return reason;
        }

        public String getCancellationFee ()
        {
            return cancellationFee;
        }

        public boolean getCancellationFeeApplied ()
        {
            return cancellationFeeApplied;
        }

        public class Reason {

            private String reason;

            private int res_id;

            public String getReason() {
                return reason;
            }

            public int getRes_id() {
                return res_id;
            }
        }
    }
}
