package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>SidSignupData</h>
 * Created by Ali on 10/5/2017.
 */

public class SidSignupData implements Serializable {
    private String sid;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Override
    public String toString() {
        return "ClassPojo [sid = " + sid + "]";
    }
}
