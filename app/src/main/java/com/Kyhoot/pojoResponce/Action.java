package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by ${3Embed} on 2/2/18.
 */

public class Action implements Serializable{
    private String timeStamp;

    private String type;

    public String getTimeStamp ()
    {
        return timeStamp;
    }

    public String getType ()
    {
        return type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [timeStamp = "+timeStamp+", type = "+type+"]";
    }
}
