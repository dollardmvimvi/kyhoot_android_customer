package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.MusciGenresData;

import java.util.ArrayList;

/**
 * <h>MusicGeners</h>
 * Created by Ali on 9/26/2017.
 */

public interface MusicGeners {
    void onSuccess(ArrayList<MusciGenresData> musicGenres);

    void onError(String errorMsg);
}
