package com.Kyhoot.pojoResponce;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by ${3Embed} on 30/1/18.
 */

public class ServiceSelectedObservable extends Observable<ServiceSelectedPojo> {

    private static ServiceSelectedObservable observebleClass;
    private Observer<? super ServiceSelectedPojo> observer;

    public static ServiceSelectedObservable getInstance() {
        if (observebleClass == null) {
            observebleClass = new ServiceSelectedObservable();
            //  observebleClass.replay();
            return observebleClass;
        } else {
            return observebleClass;
        }
    }

    @Override
    protected void subscribeActual(Observer<? super ServiceSelectedPojo> observer) {
        this.observer = observer;
    }

    public void emit(ServiceSelectedPojo selectedServices) {
        observer.onNext(selectedServices);
        observer.onComplete();

    }
}
