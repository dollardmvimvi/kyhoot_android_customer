package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

/**
 * <h>AddressModel</h>
 * this model calls the service for the address
 * Created by 3Embed on 10/11/2017.
 */

public class AddressModel {


    private UtilInterFace utilIntrFace;

    public void checkNetwork(AlertProgress aProgress, NetworkCheck networkCheck) {
        if (aProgress.isNetworkAvailable())
            networkCheck.onNetworkAvailble();
        else
            networkCheck.onNetworkNotAvailble();
    }

    public void GetAddress(final UtilInterFace utilIntrFace, SharedPrefs manager) {

        this.utilIntrFace = utilIntrFace;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "address", OkHttpConnection.Request_type.GET,
                new JSONObject(), manager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String responseCode, String result) {

                        if (responseCode.equals("200")) {
                            Log.d("TAG", "onSuccess: " + result);
                            utilIntrFace.hideProgress();
                            callresponceMethod(result);
                        }

                    }

                    @Override
                    public void onError(String header, String error) {
                        utilIntrFace.hideProgress();
                    }
                });


    }

    private void callresponceMethod(String result) {
        AddressPojo addresPojo = new Gson().fromJson(result, AddressPojo.class);
        if (addresPojo.getData().size() > 0)
            utilIntrFace.onSuccess(addresPojo.getData());
        else
            utilIntrFace.onError(addresPojo.getMessage());

        // utilIntrFace.onSuccessReview()


    }

    public void callDeleteAddress(UtilInterFace uInterFace, String addressid, SharedPrefs manager) {
        this.utilIntrFace = uInterFace;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "address/" + addressid,
                OkHttpConnection.Request_type.DELETE, new JSONObject(), manager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String result) {

                        if (headerResponse.equals("200")) {
                            if (result != null) {
                                utilIntrFace.hideProgress();

                                utilIntrFace.onSuccess(result);
                                // callresponceMethod(result);

                            }
                        }

                    }

                    @Override
                    public void onError(String header, String error) {

                        utilIntrFace.hideProgress();
                    }
                });

    }

    /**
     * <h1>callAddAddress</h1>
     * Add address Api
     *
     * @param uInterFace UtilInterface For the Address
     */
    public void callAddAddress(UtilInterFace uInterFace) {
        this.utilIntrFace = uInterFace;
    }
}
