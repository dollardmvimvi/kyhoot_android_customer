package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.CheckScheduleSPResponse;
import com.Kyhoot.pojoResponce.Data;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.PromocodePojo;
import com.Kyhoot.pojoResponce.SurgePriceResponse;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ali on 10/31/2017.
 */

public class ConfirmBookModel
{

    private SharedPrefs sharedPrefs;
    private UtilInterFace.ConfirmBook confirmBook;
    private static final String TAG = "ConfirmBookModel";

    public void checkNetwork(AlertProgress alertProgress, NetworkCheck networkCheck) {
        if (alertProgress.isNetworkAvailable())
            networkCheck.onNetworkAvailble();
        else
            networkCheck.onNetworkNotAvailble();
    }

    ///slave/booking
    public void doLiveBooking(JSONObject jsonObject, SharedPrefs sharedPref,
                              UtilInterFace.ConfirmBook confirmBooks) {
        this.sharedPrefs = sharedPref;
        this.confirmBook = confirmBooks;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "booking"
                , OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d("CONFIRMBOOKMODEL", "onSuccess: "+result);
                        confirmBook.onSuccessLiveBooking(headerData,result);
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d(TAG, "onError: headerError "+headerError);

                        if(headerError.equals("502"))
                        {
                            confirmBook.onError("Internal server error");
                        }
                        else
                        {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        confirmBook.hideProgress();
                                        confirmBook.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    case "549":
                                        confirmBook.onError("Socket Exception");
                                        break;
                                    case "411":
                                        confirmBook.onFailureLiveBooking(headerError,errorHandel.getMessage());
                                        break;
                                    default:
                                        confirmBook.onFailureLiveBooking(headerError,errorHandel.getMessage());
                                        break;
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                                confirmBook.onSessionError(e.getMessage());
                            }

                        }

                    }
                });
    }

    ///check slots/booking
    public void checkSchedule(JSONObject jsonObject, SharedPrefs sharedPref,
                              UtilInterFace.ConfirmBook confirmBooks) {
        Log.d("CHECKSCHEDULE", "checkSchedule: "+sharedPref.getSession());
        this.sharedPrefs = sharedPref;
        this.confirmBook = confirmBooks;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "schedule"
                , OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        Log.d(TAG, "getSurgePrice  onSuccessSurge: SURGE "+result);
                        try {
                            CheckScheduleSPResponse scheduleSPResponse=new Gson().fromJson(result, CheckScheduleSPResponse.class);
                            Data surgePriceResponse = scheduleSPResponse.getData();
                            if (scheduleSPResponse!=null)
                            {
                                VariableConstant.SURGE_PRICE=""+surgePriceResponse.getSurgePrice();
                                confirmBook.onSuccessScheduleCheck(headerData,result);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                       /* Log.d("CHECKSCHEDULE", "onSuccess: "+result);
                        confirmBook.onSuccessScheduleCheck(headerData,result);*/
                    }

                    @Override
                    public void onError(String headerError, String error) {

                        if(headerError.equals("502"))
                        {
                            confirmBook.onError("Internal server error");
                        }
                        else
                        {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        confirmBook.hideProgress();
                                        confirmBook.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        confirmBook.onFailureScheduleCheck(headerError,errorHandel.getMessage());
                                        break;
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                                confirmBook.onSessionError(e.getMessage());
                            }

                        }

                    }
                });
    }

    public void getSurgePrice(JSONObject jsonObject, SharedPrefs sharedPref,
                              UtilInterFace.ConfirmBook confirmBooks) {
        this.sharedPrefs = sharedPref;
        this.confirmBook = confirmBooks;
        Log.d(TAG, "getSurgePrice: "+VariableConstant.SERVICE_URL + "surgePrice");
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "surgePrice"
                , OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result)
                    {
                        Log.d(TAG, "getSurgePrice  onSuccessSurge: SURGE "+result);
                        try {
                            SurgePriceResponse surgePriceResponse=new Gson().fromJson(result, SurgePriceResponse.class);
                            if (surgePriceResponse!=null)
                            {
                                VariableConstant.SURGE_PRICE=""+surgePriceResponse.getData().getSurgePrice();
                                confirmBook.onSuccessSurgePrice(surgePriceResponse);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if(headerError.equals("502")) {
                            confirmBook.onError("Internal server error");
                        }
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        confirmBook.hideProgress();
                                        confirmBook.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        confirmBook.onError(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                confirmBook.onError(e.getMessage());
                            }
                        }
                    }
                });
    }


    public void validatePromocode(JSONObject jsonObject, SharedPrefs sharedPref,
                                  UtilInterFace.ConfirmBook confirmBooks) {
        this.sharedPrefs = sharedPref;
        this.confirmBook = confirmBooks;
        Log.d(TAG, "validatePromocode: "+VariableConstant.SERVICE_URL + "promoCodeValidation");
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "promoCodeValidation"
                , OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: "+result);
                        try {
                            Gson gson=new Gson();
                            PromocodePojo pojo=gson.fromJson(result, PromocodePojo.class);
                            confirmBook.onSuccessPromocode(pojo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if(headerError.equals("502")) {
                            confirmBook.onErrorPromocode("Internal server error");
                        }
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        confirmBook.hideProgress();
                                        confirmBook.onSessionError(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        confirmBook.onErrorPromocode(errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                confirmBook.onErrorPromocode(e.getMessage());
                            }
                        }
                    }
                });
    }



    private void getAccessToken(String data) {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {


                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            sharedPrefs.setSession(jsonobj.getString("data"));
                            confirmBook.onError("");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //convert java object to JSON format

                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }


    public void getLastDue(String token,final UtilInterFace.ConfirmBook confirmBook) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "lastDues",
                OkHttpConnection.Request_type.GET, new JSONObject(), token, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d(TAG, "onSuccess: "+headerData+" "+result);
                        confirmBook.onLastDues(headerData,result);
                        //convert java object to JSON format

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d(TAG, "onError: "+headerError+" error:" +error);
                        confirmBook.onLastDues(headerError,error);
                    }
                });
    }
}

