package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 8/5/18.
 */
public class ReferralCodeResponsePojo {

        private String message;

        private ReferralCodeData data;

        public String getMessage ()
        {
            return message;
        }

        public void setMessage (String message)
        {
            this.message = message;
        }

        public ReferralCodeData getData ()
        {
            return data;
        }

        public void setData (ReferralCodeData data)
        {
            this.data = data;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", data = "+data+"]";
        }
}
