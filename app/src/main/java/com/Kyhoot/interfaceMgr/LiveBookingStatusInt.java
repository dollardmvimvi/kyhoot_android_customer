package com.Kyhoot.interfaceMgr;

import com.google.android.gms.maps.model.LatLng;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.pojoResponce.CartData;
import com.Kyhoot.pojoResponce.LiveBookingStatusPojo;

/**
 * <h>LiveBookingStatusInt</h>
 * this interface takes the data according to the value form server and shows to the activity
 * Created by Ali on 11/8/2017.
 */

public interface LiveBookingStatusInt {
    void onSuccess();

    void onGigTimeServices(CartData gigBookinTim);

    void onJobStatusTime(LiveBookingStatusPojo.LiveBookingData.JobBookingStatus jobBookingStatus);

    void onProviderDtls(LiveBookingStatusPojo.LiveBookingData.ProviderBookingDetls providerBookingDetls);

    void onBookingTimer(LiveBookingStatusPojo.LiveBookingData.BookingTimer bookingTimer);

    void onStatus(int status, String paymentType, String signatureUrl, LatLng latLng,String currencySymbol);

    void onSuccessTriggerScheduleBook();

    void onError(String errorMsg);

    void onDistanceTime(String distance, String Eta);

    void onSessionError();

    void onAccounting(AllEventsDataPojo.Accounting accounting);

    void setJobTimer();

    void setCancellationAfterXmin(int cancellationAfterXmin);
}
