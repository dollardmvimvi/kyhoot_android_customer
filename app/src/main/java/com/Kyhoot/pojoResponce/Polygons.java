package com.Kyhoot.pojoResponce;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 16/3/18.
 */

public class Polygons
{
    private String type;

    //private double[][][] coordinates;
    private ArrayList<ArrayList<ArrayList<Double>>> coordinates;

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public ArrayList<ArrayList<ArrayList<Double>>> getCoordinates ()
    {
        return coordinates;
    }

    public void setCoordinates (ArrayList<ArrayList<ArrayList<Double>>> coordinates)
    {
        this.coordinates = coordinates;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [type = "+type+", coordinates = "+coordinates+"]";
    }
}
