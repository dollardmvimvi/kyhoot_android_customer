package com.Kyhoot.utilities;

import com.Kyhoot.pojoResponce.BookingTypeAction;

import java.util.ArrayList;
import java.util.TimeZone;

/**
 * <h></h>
 * Created by Ali on 8/7/2017.
 */

public class VariableConstant
 {

  public static final String APPNAME = "GoTasker";

 //Dev API URLS
 public static final String SERVICE_URL = "https://api.kyhoot.com/customer/";
  public static final String PAYMENT_URL = "https://api.kyhoot.com/";
  public static final String CHAT_URL = "https://api.kyhoot.com/";
 //DEV MQTT
 public static final String HOST = "tcp://77.68.30.222:2052";

  public static final String SERVER_KEY = "AIzaSyA8hhP2VhGg1osJUIODwR9VYHOULsxcNZs";
  public static final String IPINFO = "https://ipinfo.io/json";
  //54.146.95.71:2052
  public static final String Amazonbucket = "kyhoot2";
  public static final String PARENT_FOLDER="gotasker";
  public static final String Amazoncognitoid = "ap-south-1:9ba98ff8-c5b1-4646-883d-5a32c8c7e573";
  public static final String AmazonProfileFolderName = "ProfileImages";
  public static final String AmazonChatFolder = "ChatImages";
  public static final String AMAZON_BASE_URL="https://s3.amazonaws.com/";
  public static final String TERMS_LINK = "https://superadmin.kyhoot.com/index.php?/utilities/getTermsdata/Customer/en";
  //http://admin2.0.iserve.ind.in/privacyPolicy.php
  //http://admin2.0.iserve.ind.in/termsAndCondions.php
  public static final String PRIVECY_LINK = "https://superadmin.kyhoot.com/index.php?/utilities/getPrivacyPolicyData/Customer/en";

  public static final int WALLET_POSTION_IN_SIDESCREEN = 4;
  public static int SELECTED_ADDRESS_POSITION = 0;
  public static String SURGE_PRICE="";
  public static boolean SCHEDULEAVAILABLE=false;

  public static final String VERIFYPHONE = SERVICE_URL + "verifyPhoneNumber";
  public static final String UPDATEPROFILE = SERVICE_URL + "profile/me";
  public static final String TWILIO_LINK = "https://twillo.heon.app/getcapability";
  public static int MINIMUM_HOUR=0;
  //public static final String TWILIO_LINK = "http://52.56.70.95:5011/getcapability";
  public static int DISTANCEMATRIXUNIT=0;
  //TitilliumWeb-Light
  public static final String lightfont = "fonts/OpenSans-Light.ttf";
  //TitilliumWeb-Bold
  public static final String boldFont = "fonts/OpenSans-Bold.ttf";
  //TitilliumWeb-Regular
  public static final String regularfont = "fonts/OpenSans-Regular.ttf";
  //TitilliumWeb-SemiBold
  public static final String semiBoldFont = "fonts/OpenSans-Semibold.ttf";
  //TitilliumWeb-ExtraLight
  public static final String mediumFont = "fonts/OpenSans-Light.ttf";
  public static final String montserratRegular = "fonts/Montserrat-Regular.ttf";
  public static final int GALLERY_PIC = 10;
  public static final int CAMERA_PIC = 11;
  public static final int CROP_IMAGE = 12;

 // public static final String PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=com.heon";
 public static final String PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=com.Kyhoot";
  public static final String RESPONSE_CODE_SUCCESS = "200";
  public static final String WEBSITE_LINK = "https://kyhoot.com/";
  public static  BookingTypeAction BOOKINGTYPEAVAILABILITY = null;
  public static float SELECTEDCATEGORYVISITFEE =0 ;
  public static boolean WALLETUPDATEINTENT = false;
  public static boolean WALLETBOOKING = true;
  public static boolean CASHBOOKING = true;
  public static boolean CARDBOOKING = true;
  public static boolean ENABLEWALLET = false;
  public static TimeZone TIMEZONE ;
  public static long SERVERTIMEDIFFERNCE = 0;
  public static  double IPLONG = 0;
  public static  double IPLAT = 0;

  public static boolean IS_NETWORK_ERROR_SHOWED = false;
  public static String SelLang = "en";
  public static String dateOB = "";
  public static int infltdCnt = 0; //Responce from MQTT
  public static boolean isSplashCalled = false;
  public static boolean isAddressCalled = false;
  public static boolean isPaymentCalled = false;
  public static boolean isReviewCalled = false;
  public static boolean isChattingCalled = false;


  public static boolean isConfirmBook = false;
  public static int PROFREQUENCYINTERVAL = 4;

  public static int CALLMQTTDATA = 1;
  public static int BOOKINGTYPE = 1;
  public static String SCHEDULEDDATE = "";
  public static double SCHEDULEDTIME = 0;
  public static int BOOKINGMODEL=-1;
  public static String distanceUnit = "Miles away";
  public static String distanceOnlyMiles = "Miles";
  public static boolean isAdapterNotified = false;
  public static boolean isLiveBokinOpen = false;
  public static boolean isLiveBookingOpen = false;

  public static String setIPAddress = "";
  public static boolean isChattingOpen = false;
  public static boolean isChattingToSave = false;
  public static boolean isHelpIndexOpen = false;
  public static boolean isHomeFragment = false;
  public static boolean isCurrentLocation = false;
  public static boolean isADRESELETD = false;
  public static boolean isBookingPending = false;
  public static String LiveTrackBooking = "";
  public static boolean toHelpFrag=false;
  public static String selectedCategoryId="";
  public static String CARTID="";
  public static String providerId="";
  public static boolean ipAddressSet=false;
  public static boolean myEventOpen=false;
  public static boolean isLocationPermissionGranted=false;
  public static String walletBalance="0";
  public static String walletCurrency="";
  public static String CurrencyPosition="Prefix";
  public static double SPLASHLAT=0;
  public static double SPLASHLONG=0;
  public static double BOOKINGLONG ;
  public static double BOOKINGLAT ;
  public static float SERVICEHOURLYPRICE=0;
  public static String SERVICE_TYPE="";
  public static String SELECTEDCATEGORYNAME = "";
  public static String BILLINGMODEL = "";
  public static boolean SHOWINGPROVIDERLIST;
  public static TimeZone BOOKINGTIMEZONE;
  public static String MQTTRESPONSEPROVIDER="";
  public static String ROTATEDKEY = "";
  public static ArrayList<String> GOOGLEKEY;
  public static String CurrencySymbol="$";
  public static int CANCELLATION_AFTER_X_MIN=0;
  public static boolean isCancellableAfterXMin=false;
  public static long bookingAcceptedTime=0;

  public static double NOWBOOKINGLONG ;
  public static double NOWBOOKINGLAT;

  public static boolean SHOWPOPUP=true;
  public static int BOOKINGTYPEMODEL =0;

  public static final int REPEAT_RESULT_CODE = 152;
  public static long diffServerTime=0;

  public static final int TIME_RESULT_CODE = 154;
  public static long SCHEDULE_BOOK_REPEAT_END_DATE = 0L;
  public static long SCHEDULE_BOOK_REPEAT_START_DATE =0L;

  public static ArrayList<String> STUFFIMAGES;

  public static ArrayList<String> repeatDays = new ArrayList<>();
  public static String repeatStartTime = "";
  public static String repeatStartDate = "";
  public static String repeatEndTime = "";
  public static String repeatEndDate = "";
  public static int repeatNumOfShift = 0;

  public static final String JOB_COMPLETED_RAISE_INVOICE = "10";

 }
