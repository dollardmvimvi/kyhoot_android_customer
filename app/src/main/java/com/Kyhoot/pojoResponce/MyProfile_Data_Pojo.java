package com.Kyhoot.pojoResponce;

/**
 * <h>MyProfile_Data_Pojo</h>
 * Created by embed on 25/5/17.
 */

public class MyProfile_Data_Pojo
{
    private String firstName;

    private String phone;

    private String email;

    private String dateOfBirth, profilePic, about,countryCode;

    private String countrySymbol;

    public String getCountrySymbol() {
        return countrySymbol;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getAbout() {
        return about;
    }

    public String getName ()
    {
        return firstName;
    }

    public String getPhone ()
    {
        return phone;
    }

    public String getEmail ()
    {
        return email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = " + firstName + ", phone = " + phone + ", email = " + email + "]";
    }
}

