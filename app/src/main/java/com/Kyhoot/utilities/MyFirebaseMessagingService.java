package com.Kyhoot.utilities;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.main.ChattingActivity;
import com.Kyhoot.main.LiveStatus;
import com.Kyhoot.main.MenuActivity;
import com.Kyhoot.main.NotificationHandler;
import com.Kyhoot.pojoResponce.NotificationPojo;
import com.Kyhoot.zendesk.zendesk.HelpIndex;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * <h>MyFirebaseMessagingService</h>
 * Created by 3Embed on 10/26/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    private String TAG = "MyFirebaseMessagingService";
    private String message;
    private int action;
    private NotificationUtils notificationUtils;
    private Gson gson = new Gson();
    private long bid;
    private String picUrl = "";
    private String title = "Heon Customer";
    private TaskStackBuilder stackBuilder;
    private String timestamp;
    private SharedPrefs prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs=new SharedPrefs(getApplicationContext());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);
        Log.e(TAG, "onMessageReceived: "+remoteMessage );
        timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());


        if (remoteMessage == null)
            return;
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());

        }
        if (remoteMessage.getData().size() > 0)
        {
            Log.e(TAG, "Notification Data: " + remoteMessage.getData());
            stackBuilder = TaskStackBuilder.create(this);
            String s = remoteMessage.getData().get("action");
            action = Integer.parseInt(s);
            message = remoteMessage.getData().get("msg");
            String data = remoteMessage.getData().get("data");
            int index = data.lastIndexOf("}");
            String dataSubString = data.substring(0,index+1);
            title = remoteMessage.getData().get("title");
            if(title.trim().equals("")){
                title=getResources().getString(R.string.app_name);
            }

            try
            {
                if(action==23)
                {
                    handleDataMessage(action,picUrl,12);
                }
                else if(action==70)
                {
                    if(!VariableConstant.isHelpIndexOpen)
                        sendNotification(message);
                }
                else if(action==111)
                {
                    information();
                }
                else
                {
                    NotificationPojo notificationPojo = gson.fromJson(dataSubString,NotificationPojo.class);
                    bid = notificationPojo.getBookingId();
                    if(action==1)
                        handleChatNotification(notificationPojo);
                    else
                        handleDataMessage(action,picUrl,bid);
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }



    private void information() {
        if(!Utilities.isAppIsInBackground(getApplicationContext()))
        {
            // app is in foreGround
            Intent pushNotification = new Intent(getApplicationContext(), MenuActivity.class);
            stackBuilder.addParentStack(MenuActivity.class);
            stackBuilder.addNextIntent(pushNotification);
            showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification,false);
        }else
        {
            Intent resultIntent = new Intent(getApplicationContext(), MenuActivity.class);//MainActivity
            stackBuilder.addParentStack(MenuActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent,false);
        }
    }

    private void handleChatNotification(NotificationPojo notificationPojo)
    {
        timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());

        if(!VariableConstant.isChattingOpen)
        {
            //DataBaseHelper db = new DataBaseHelper(this);
            Intent resultIntent = new Intent(getApplicationContext(), ChattingActivity.class);//MainActivity

            stackBuilder.addParentStack(ChattingActivity.class);
           /* PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);*/
            stackBuilder.addNextIntent(resultIntent);
            resultIntent.putExtra("BID", notificationPojo.getBid());
            resultIntent.putExtra("PROIMAGE", notificationPojo.getProfilePic());
            resultIntent.putExtra("PROID", notificationPojo.getFromID());
            resultIntent.putExtra("PRONAME", notificationPojo.getName());

            /*if(!VariableConstant.isChattingToSave)
            {

                db.addNewChat(message,notificationPojo.getTargetId(),notificationPojo.getFromID()
                        ,notificationPojo.getTimestamp(),notificationPojo.getType(),2,notificationPojo.getBid());
            }*/


            // check for image attachment
            if(message.contains("https://s3.amazonaws.com"))
                showNotificationMessageWithBigImage(getApplicationContext(), title, "Heon", timestamp, resultIntent, message,true);
            else
                showNotificationMessage(getApplicationContext(), "Msg from "+notificationPojo.getName(), message, timestamp, resultIntent,true);
        }
    }

    // End onReceive


    /**
     *
     * @param action action
     * @param picUrl picUrl
     * @param bid bid
     */

    private void handleDataMessage(int action, String picUrl, long bid)
    {

        /*if(prefs.getBookingStatus(bid)>=action){
            return;
        }*/
        this.action = action;
        this.picUrl = picUrl;
        this.bid = bid;

        timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());


        Log.e(TAG, "action " + this.action);
        Log.e(TAG, "message: " + message);
        Log.e(TAG, "bid: " + this.bid);
        Log.e(TAG, "timestamp: " + timestamp);


       /* if (!Utilities.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
              //Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            Log.d(TAG, "handleDataMessage: "+VariableConstant.isLiveBokinOpen);
            if(!VariableConstant.isLiveBokinOpen)
            {
                genralmethod();
            }
            else if (action==23)
            {
                genralmethod();
            }
        } else {*/
        // app is in background, show the notification in notification tray
        //6 Provider onthe way
        //7 Provider Arrived
        //8 Job Started
        //9 Job completed
        //10 Invoice raised

        if(action==3 || action==6 || action==7 || action==8 || action==9 )
        {
            //if(!VariableConstant.isLiveBokinOpen){

            Intent resultIntent = new Intent(getApplicationContext(), LiveStatus.class);//MainActivity
            stackBuilder.addParentStack(LiveStatus.class);
            stackBuilder.addNextIntent(resultIntent);
            resultIntent.putExtra("BID", this.bid);
            resultIntent.putExtra("STATUS", this.action);
            VariableConstant.isSplashCalled = true;
            Log.e(TAG, "initializehandleDataMessage: "+bid+" BID "+this.bid +" action "+this.action);
            // check for image attachment
            if (TextUtils.isEmpty(picUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent,true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl,true);
            }
        }else if(action==23){
            genralmethod();
        }
        else if (action == 11)
        {


           /* AlertDialog.Builder builder=new AlertDialog.Builder(getApplicationContext());
            builder.setMessage("Booking cancelled by the provider ");
            builder.setPositiveButton(" OK", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    VariableConstant.SCHEDULEAVAILABLE = true;
                    dialog.dismiss();
                    if (VariableConstant.SCHEDULEAVAILABLE)
                    {
                        AlertDialog.Builder builder2=new AlertDialog.Builder(getApplicationContext());
                        builder2.setMessage(getString(R.string.book_another_provider));
                        builder2.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                *//*if(aProgress.isNetworkAvailable()) {

                                    triggerNewScheduleBooking(myEventStatus.getData().getBookingId());
                                    VariableConstant.SCHEDULEAVAILABLE = false;
                                } else {
                                    aProgress.showNetworkAlert();
                                }
                                dialog.dismiss();*//*
                            }
                        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder2.show();

                    }
                }
            });
            builder.create().show();*/

            Intent resultIntent=new Intent(getApplicationContext(), MenuActivity.class);
            //  resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            stackBuilder.addParentStack(MenuActivity.class);
            stackBuilder.addNextIntent(resultIntent);

            resultIntent.putExtra("BID", this.bid);
            resultIntent.putExtra("STATUS", this.action);
            VariableConstant.isSplashCalled = true;
            Log.e(TAG, "initializehandleDataMessage: "+bid+" BID "+this.bid +" action "+this.action);
            // check for image attachment
            if (TextUtils.isEmpty(picUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent,true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl,true);
            }
            //===================================================

            //Added April 25 2018
            if(VariableConstant.isLiveBokinOpen){
                return;
            }
                /*Intent resultIntent = new Intent(getApplicationContext(), NotificationHandler.class);//MainActivity
                stackBuilder.addParentStack(NotificationHandler.class);
                stackBuilder.addNextIntent(resultIntent);
                resultIntent.putExtra("message", message);
                resultIntent.putExtra("statcode", this.action);
                resultIntent.putExtra("bid", this.bid);
                // check for image attachment
                if (TextUtils.isEmpty(picUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent,true);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl,true);
                }*/
        }
        //}
    }


    /**
     *
     * @param message notification message
     */

    private void handleNotification(String message)
    {
        if (!Utilities.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            if(action!=2)
            {
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("statcode",action);
                pushNotification.putExtra("bid",bid);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            }

        }/*else
        {
            sendNotification(message);
        }*/
    }



    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent,boolean isChatting) {
        notificationUtils = new NotificationUtils(context);
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent,isChatting,stackBuilder);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl,boolean isChatting)
    {
        notificationUtils = new NotificationUtils(context);
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl,isChatting,stackBuilder);
    }

    //    /**
//     * Create and show a simple notification containing the received FCM message.
//     *
//     * @param messageBody FCM message body received.
//     */
    private void sendNotification(String messageBody)
    {
        Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);
        int numMessages = 0;

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setSummaryText("" + getResources().getString(R.string.livemAppLink));
        inboxStyle.setBigContentTitle(messageBody);
        String[] events = new String[6];
        stackBuilder.addParentStack(HelpIndex.class);
        // Moves events into the expanded layout
        // Moves events into the expanded layout
        for (int i=0; i < events.length; i++) {

            inboxStyle.addLine(events[i]);
        }
        // mBuilder.setStyle(inboxStyle);

        Intent intent = new Intent(this, MenuActivity.class);
        VariableConstant.toHelpFrag=true;
        stackBuilder.addNextIntent(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        /*PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  , intent,
                PendingIntent.FLAG_ONE_SHOT);*/

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(icon1)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(inboxStyle)
                .setNumber(++numMessages)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1 , notificationBuilder.build());
    }

    public void genralmethod()
    {
        Intent pushNotification = new Intent(getApplicationContext(), NotificationHandler.class);

        pushNotification.putExtra("message", message);
        pushNotification.putExtra("statcode", this.action);
        pushNotification.putExtra("bid", this.bid);
        pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(pushNotification);
        notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }
}

