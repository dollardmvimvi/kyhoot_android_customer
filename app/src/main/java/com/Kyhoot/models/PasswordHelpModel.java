package com.Kyhoot.models;


import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.LoginResponceIntr;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.SignUpPojo;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>PasswordHelpModel</h>
 * Created by Ali on 9/14/2017.
 */

public class PasswordHelpModel
{
    private String TAG="PASSWORDHELPMODEL";

    /**
     * <h1>forGotPasswor</h1>
     * <p>
     *     this sends the otp or the link to given email for the password change
     * @param emailOrPhone user phone number or email
     * @param isEmailOrPhone tells its phone number or email id
     * @param countrycode country code
     * @param verfyiotp Interface {@see VerifyOtpIntrface}
     * @param gson Gson object
     */

    public void forGotPasswor(String emailOrPhone, final boolean isEmailOrPhone, String countrycode, final VerifyOtpIntrface verfyiotp, final Gson gson)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailOrPhone",emailOrPhone);
            jsonObject.put("countryCode",countrycode);
            jsonObject.put("userType",1);
            if(isEmailOrPhone)
                jsonObject.put("type",2);
            else
                jsonObject.put("type",1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "forgotPassword", OkHttpConnection.Request_type.POST, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result)
            {
                Log.d("forgotPassword", "onSuccess: " + result + " headerResponse " + headerResponse);
                SignUpPojo pojo = new Gson().fromJson(result, SignUpPojo.class);

                   if(isEmailOrPhone)
                     verfyiotp.onSignUpSuccess(pojo.getErrMsg());
                    else
                       verfyiotp.onOtpVerify(pojo.getData().getSid());

            }

            @Override
            public void onError(String headerError, String error) {
                try{
                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                    verfyiotp.onOtpError(errorHandel.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * <h1>newPassword</h1>
     * change the password to the new one
     * @param jsonObject JsonObject jsonObject as an argument paramenter to the a password service
     * @param lginres {@see LoginResponceIntr}
     */

    public void newPassword(JSONObject jsonObject, final LoginResponceIntr lginres)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "password", OkHttpConnection.Request_type.PATCH, jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result)
            {
                Log.d("Password", "onSuccess: " + result + " headerResponse " + headerResponse);
                    lginres.onSuccess();


            }
            @Override
            public void onError(String headerError, String error) {
                try{
                    ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                    lginres.onError(errorHandel.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }


    public void changeEmail(final String email, final SharedPrefs sprefs, final VerifyOtpIntrface verfyiotp, final Gson gson) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userType", 1);
            jsonObject.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "email"
                , OkHttpConnection.Request_type.PATCH, jsonObject, sprefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d("TAG", "PROonSuccessEmail: " + result);
                        sprefs.setEMail(email);
                        verfyiotp.onSignUpSuccess("");
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                            verfyiotp.onOtpError(errorHandel.getMessage());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void changePhoneNumber(String emailOrPhone, SharedPrefs sprefs, final VerifyOtpIntrface verfyiotp, final Gson gson, String countryCode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userType", 1);
            jsonObject.put("countryCode", countryCode);
            jsonObject.put("phone", emailOrPhone);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "phoneNumber"
                , OkHttpConnection.Request_type.PATCH, jsonObject, sprefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                       /* {"message":"Got The Details.","data":{"sid":"59ce5553b121331d381cad8c"}}*/
                        SignUpPojo pojo = gson.fromJson(result, SignUpPojo.class);
                        Log.d("TAG", "PROonSuccess: " + result);

                        verfyiotp.onOtpVerify(pojo.getData().getSid());
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d(TAG, "onError: error"+error+" headererror: "+headerError);

                        try{
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                            verfyiotp.onOtpError(errorHandel.getMessage());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }
}
