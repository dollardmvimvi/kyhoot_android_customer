package com.Kyhoot.countrypic;

/**
 * <h>Country</h>
 * Created by embed on 6/9/16.
 */
public class Country
{
    private String code;
    private String name;
    private String dialCode;
    private int flag;
    private int max_digits;

    public int getMax_digits() {
        return max_digits;
    }

    public void setMax_digits(int max_digits) {
        this.max_digits = max_digits;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
