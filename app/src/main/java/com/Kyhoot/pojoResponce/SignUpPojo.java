package com.Kyhoot.pojoResponce;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * <h>SignUpPojo</h>
 * Created by Ali on 10/5/2017.
 */

public class SignUpPojo implements Serializable {
    /*"errNum":200,
"errMsg":"Got The Details",
"errFlag":0,
"data":{
"sid":"59d5f8e1b121331d381cae0a"
}*/
    @Expose
    private int errNum;

    @Expose
    private String message;

    @Expose
    private SidSignupData data;
    @Expose
    private int errFlag;

    public int getErrNum() {
        return errNum;
    }

    public void setErrNum(int errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return message;
    }

    public void setErrMsg(String errMsg) {
        this.message = errMsg;
    }

    public SidSignupData getData() {
        return data;
    }

    public void setData(SidSignupData data) {
        this.data = data;
    }

    public int getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(int errFlag) {
        this.errFlag = errFlag;
    }

    @Override
    public String toString() {
        return "ClassPojo [errNum = " + errNum + ", errMsg = " + message + ", data = " + data + ", errFlag = " + errFlag + "]";
    }
}
