package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>PreferedGen</h>
 * Created by Ali on 10/6/2017.
 */

public class PreferedGen implements Serializable {
    /* "id": "59ae5dd1b055496bd6497262",
        "catName": "Country music"*/
    private String id, catName;

    public String getId() {
        return id;
    }

    public String getCatName() {
        return catName;
    }
}
