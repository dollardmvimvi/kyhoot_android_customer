package com.Kyhoot.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.Dialog.BottomsheetFragmentFavProvider;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.HomeModelIntrface;
import com.Kyhoot.main.ProviderDetails;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * <h>HomeListAdaptr</h>
 * this shows the list of providers on the ListView
 * Created by 3Embed on 10/17/2017.
 */

public class HomeListAdaptr extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context mContext;
    private ArrayList<MqttProviderDetails> providerList,onlineProviderList,offlineProviderList,finalArray;
    private HomeModelIntrface hInterface;
    private Activity mActivity;
    public static final String TAG="HOMELISTADAPTER";
    public HomeListAdaptr(Activity mContext, ArrayList<MqttProviderDetails> providerList, HomeModelIntrface hInterface) {
        this.mContext = mContext;
        this.providerList = providerList;
        onlineProviderList=new ArrayList<>();
        offlineProviderList=new ArrayList<>();
        finalArray=new ArrayList<>();
        splitList();
        this.hInterface = hInterface;
        mActivity = (Activity) mContext;
        //Log.d("HOMELISTADAPTR", "getItemCount: " + providerList.size());
    }

    private void splitList() {
        onlineProviderList.clear();
        offlineProviderList.clear();
        finalArray.clear();
        if(providerList.size()>0) {
            for (MqttProviderDetails providerDetail : providerList) {
                if (providerDetail.getStatus() == 1) {
                    onlineProviderList.add(providerDetail);
                } else {
                    offlineProviderList.add(providerDetail);
                }
            }
            Collections.sort(onlineProviderList,new FavoriteProvider());
            Collections.sort(offlineProviderList,new FavoriteProvider());
            if(onlineProviderList.size()>0){
                finalArray.add(new MqttProviderDetails());
                finalArray.addAll(onlineProviderList);
            }
            if(offlineProviderList.size()>0){
                finalArray.add(new MqttProviderDetails());
                finalArray.addAll(offlineProviderList);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType==1){
            view = LayoutInflater.from(mContext).inflate(R.layout.online_provider_header_layout, parent, false);
            return new ViewHoldrHeader(view);
        }else{
            view = LayoutInflater.from(mContext).inflate(R.layout.single_home_list_adapter, parent, false);
            return new ViewHoldr(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(finalArray.get(position).getId()==null){
            ViewHoldrHeader header=(ViewHoldrHeader) holder;
            if(onlineProviderList.size()>0 && position==0){
                header.tvHeader.setText(String.format("AVAILABLE NOW(%d)", onlineProviderList.size()));
            }else{
                header.tvHeader.setText(String.format("AVAILABLE LATER(%d)", offlineProviderList.size()));
            }
        }else{
            try {
                if (finalArray.get(position)!=null) {
                    final ViewHoldr hldr = (ViewHoldr) holder;
                    hldr.currencySymbol = finalArray.get(position).getCurrencySymbol();
                    ((ViewHoldr) holder).rvImageScroll.setAdapter(new HomeListImageAdapter(finalArray.get(position).getWorkImage(), mContext,hldr));
                    ((ViewHoldr) holder).rvImageScroll.addItemDecoration(new LinePagerIndicatorDecoration());
                    Utilities.setAmtOnRecept(finalArray.get(position).getAmount(), hldr.tvHomeListProAmount, hldr.currencySymbol);
                    //hldr.tvHomeListProAmount.setText(amount);
                    String name = finalArray.get(position).getFirstName() + " " + finalArray.get(position).getLastName();
                    hldr.tvHomeListProName.setText(name);
                    if (finalArray.get(position).getStatus() == 1) {
                        hldr.ivOnLineOffline.setText("ONLINE");
                        hldr.ivOnLineOffline.setTextColor(mContext.getResources().getColor(R.color.green));
                    } else {
                        hldr.ivOnLineOffline.setText("OFFLINE");
                        hldr.ivOnLineOffline.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    }
                    if (finalArray.get(position).isFavouriteProvider()) {
                        hldr.iv_fav.setVisibility(View.VISIBLE);
                    } else {
                        hldr.iv_fav.setVisibility(View.GONE);
                    }
                    if (finalArray.get(position).getDistanceMatrix() == 0)
                        VariableConstant.distanceUnit = hldr.distanceUnitKm;
                    else VariableConstant.distanceUnit = hldr.distanceUnitMiles;
                    String distance = finalArray.get(position).getDistance() + " " + VariableConstant.distanceUnit;
                    hldr.tvHomeListProDistance.setText(distance);
                    hldr.rtHomeListProRating.setRating(finalArray.get(position).getAverageRating());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        //return (providerList.size()+2);
        return finalArray.size();
    }

    public void makeList() {
        splitList();
    }

    private class ViewHoldr extends RecyclerView.ViewHolder  implements View.OnClickListener {
        // YTParams params;
        //   private YoutubePlayerView youtubePlayerListView;
        private ImageView iv_fav;
        private ImageView youTubeThumbnailView;
        private TextView tvHomeListProName, tvHomeListProDistance, tvHomeListProAmount, tvHomeListProMinute,ivOnLineOffline;
        private RatingBar rtHomeListProRating;
        private RelativeLayout rlHomeListItem;
        private String currencySymbol;
        private LinearLayout llHomeListAdaptr;
        private String youtubeLink;
        private String distanceUnitKm;
        private String distanceUnitMiles;
        private RecyclerView rvImageScroll;
        private ViewHoldr(View itemView) {
            super(itemView);
            //  params = new YTParams();
            AppTypeface typeface = AppTypeface.getInstance(mContext);
            distanceUnitKm = mContext.getString(R.string.distanceKm);
            distanceUnitMiles = mContext.getString(R.string.distanceMiles);
            //   youtubePlayerListView = itemView.findViewById(R.id.youtubePlayerListView);
            youTubeThumbnailView = itemView.findViewById(R.id.yt_thumbnail);
            tvHomeListProDistance = itemView.findViewById(R.id.tvHomeListProDistance);
            tvHomeListProAmount = itemView.findViewById(R.id.tvHomeListProAmount);
            tvHomeListProMinute = itemView.findViewById(R.id.tvHomeListProMinute);
            rtHomeListProRating = itemView.findViewById(R.id.rtHomeListProRating);
            tvHomeListProName = itemView.findViewById(R.id.tvHomeListProName);
            rlHomeListItem = itemView.findViewById(R.id.rlHomeListItem);
            ivOnLineOffline = itemView.findViewById(R.id.ivOnLineOffline);
            rvImageScroll = itemView.findViewById(R.id.rvImageScroll);
            iv_fav = itemView.findViewById(R.id.iv_fav);
           // tvOnlineOrOffline=itemView.findViewById(R.id.tvOnlineOrOffline);
            llHomeListAdaptr = itemView.findViewById(R.id.llHomeListAdaptr);
            rvImageScroll.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
            PagerSnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(rvImageScroll);
            ivOnLineOffline.setTypeface(typeface.getMontserratReg());
            tvHomeListProDistance.setTypeface(typeface.getHind_regular());
            tvHomeListProAmount.setTypeface(typeface.getHind_semiBold());
            tvHomeListProMinute.setTypeface(typeface.getHind_regular());
            tvHomeListProName.setTypeface(typeface.getHind_semiBold());
            rtHomeListProRating.setIsIndicator(true);
            llHomeListAdaptr.setOnClickListener(this);
            rvImageScroll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            hInterface.onBookLatLng();
            Intent intent;
            //if(VariableConstant.BUSINESSTYPE==1)
            intent = new Intent(mContext, ProviderDetails.class);
            /*else
                intent = new Intent(mContext, ProviderDetails_Rides.class);*/

            intent.putExtra(BottomsheetFragmentFavProvider.PROVIDERID, finalArray.get(getAdapterPosition()).getId());
            intent.putExtra(BottomsheetFragmentFavProvider.IMAGEURL, finalArray.get(getAdapterPosition()).getImage());
            mContext.startActivity(intent);
            mActivity.overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
        }
    }
    private class ViewHoldrHeader extends RecyclerView.ViewHolder {
        private TextView tvHeader;
        private ViewHoldrHeader(View itemView) {
            super(itemView);
            AppTypeface typeface = AppTypeface.getInstance(mContext);
            tvHeader = itemView.findViewById(R.id.tv_availableHeader);
            this.tvHeader.setTypeface(typeface.getHind_regular());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(finalArray.get(position).getId()==null){
            return  1;//Available Online header
        }else {
            return 0;
        }
    }

    class FavoriteProvider implements Comparator<MqttProviderDetails> {
        @Override
        public int compare(MqttProviderDetails mqttProviderDetails, MqttProviderDetails t1) {
            int b1 = mqttProviderDetails.isFavouriteProvider() ? 1 : 0;
            int b2 = t1.isFavouriteProvider() ? 1 : 0;
            return b2 - b1;
        }
    }

}
