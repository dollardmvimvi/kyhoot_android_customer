package com.Kyhoot.interfaceMgr;

import android.os.Bundle;

/**
 * <h>LoginResponceIntr</h>
 * this interface update the user interface according to the responce it gets
 * Created by Ali on 8/23/2017.
 */

public interface LoginResponceIntr
{
    void onSuccess();
    void onError(String errormsg);
    void onError(String errorMsg, Bundle loginpojo);

    interface CheckValidty {
        void passwordValid();
        void emailValid();
        void inValidEmail();
        void onEnterEmailPhone();
        void onEmailValidSuccess();
        void onPhoneError();
        void emailEmpty();
    }
}
