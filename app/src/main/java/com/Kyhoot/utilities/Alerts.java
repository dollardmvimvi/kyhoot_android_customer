package com.Kyhoot.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

import com.Kyhoot.R;

/**
 * <h1>Alerts</h1>
 * <p> class to show different types of alerts </p>
 */
public class Alerts extends Activity {

    private final Context context;

    public Alerts(Context context) {
        this.context=context;
    }
    /**
     * <h2>showNetworkAlert</h2>
     * This method is used to show network alert box, for opening settings.
     * @param context
     */
    public void showNetworkAlert(final Context context)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(context.getResources().getString(R.string.network_alert_title));
        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage(context.getResources().getString(R.string.network_alert_message));
        // On pressing Settings button
        alertDialog.setPositiveButton(context.getResources().getString(R.string.action_settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        boolean isbkrnd= Utilities.isAppIsInBackground(context);
      // Showing Alert Message
        if(isbkrnd == false)
            alertDialog.show();
    }



    public ProgressDialog getProcessDialog(Activity mContext)
    {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setCancelable(true);
        dialog.setMessage(mContext.getResources().getString(R.string.pleaseWait));
        return dialog;
    }


}
