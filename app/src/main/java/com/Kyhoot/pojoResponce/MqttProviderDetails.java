package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>MqttProviderDetails</h>
 * Created by Ali on 10/4/2017.
 */

public class MqttProviderDetails implements Serializable {
    /*"amount":29,
"id":"5a198978b121331d381ccfa4",
"bannerImage":"",
"lastName":"maharajn",
"distance":1.6,
"averageRating":3.8,
"status":0,
"location":{
"longitude":77.5895155,
"latitude":13.0286011
},
"image":"https:\/\/s3.amazonaws.com\/iserve\/11716315831493.png",
"youtubeUrlLink":"https:\/\/www.youtube.com\/watch?v=nXkzOCqlw3M",
"firstName":"ajay"
"currencySymbol":"$"
"distanceMatrix" : ""*/


    private String id, firstName, lastName, image, bannerImage, youtubeUrlLink,currencySymbol;
    private int status,distanceMatrix;
    private ProviderLocation location;
    private float averageRating;
    private double amount,distance;
    private String videoId;
    private boolean favouriteProvider;

    private ArrayList<String> workImage;


    private boolean alreadyPlayed=false;


    private boolean playRequested=false;
    private boolean isReady=false;

    public ArrayList<String> getWorkImage() {
        return workImage;
    }

    public int getDistanceMatrix() {
        return distanceMatrix;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public boolean isPlayRequested() {
        return playRequested;
    }

    public void setPlayRequested(boolean playRequested) {
        this.playRequested = playRequested;
    }

    public boolean isAlreadyPlayed() {
        return alreadyPlayed;
    }

    public void setAlreadyPlayed(boolean alreadyPlayed) {
        this.alreadyPlayed = alreadyPlayed;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public double getAmount() {
        return amount;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public String getYoutubeUrlLink() {
        return youtubeUrlLink;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getImage() {
        return image;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public int getStatus() {
        return status;
    }

    public double getDistance() {
        return distance;
    }

    public ProviderLocation getLocation() {
        return location;
    }

    public boolean isFavouriteProvider() {
        return favouriteProvider;
    }
}
