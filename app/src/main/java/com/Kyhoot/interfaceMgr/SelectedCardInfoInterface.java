package com.Kyhoot.interfaceMgr;
import com.Kyhoot.pojoResponce.CardGetData;
import com.Kyhoot.utilities.SharedPrefs;

import java.util.ArrayList;



public interface SelectedCardInfoInterface
{
    interface SelectedPresenter
    {
        void onGetCards(SharedPrefs sharedPrefs);
    }
    interface SelectedView extends BaseView
    {
        void onToCallIntent();

        void onToBackIntent(int adapterPosition);

        void addItems(ArrayList<CardGetData> cardsList);

        void onVisibilitySet();
    }
}
