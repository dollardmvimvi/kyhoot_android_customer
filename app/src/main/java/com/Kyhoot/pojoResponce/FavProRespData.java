package com.Kyhoot.pojoResponce;

import android.support.annotation.NonNull;

/**
 * Created by ${3Embed} on 18/6/18.
 */
public class FavProRespData {
    private String isOnline,providerId,profilePic,name,categoryName;

    public String getIsOnline() {
        return isOnline;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getName() {
        return name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getProviderId() {
        return providerId;
    }

    @NonNull
    @Override
    public String toString()
    {
        return "ClassPojo [isOnline = "+isOnline+", categoryName = "+categoryName+", name = "+name+", profilePic = "+profilePic+", providerId = "+providerId+"]";
    }
}
