package com.Kyhoot.main;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.utilities.VariableConstant;

/**
 * <h>TermsActivity</h>
 * Created by 3Embed on 10/6/2017.
 */

public class TermsActivity extends AppCompatActivity implements View.OnClickListener {
    TextView terms_conditions_Tv, privacy_policy_Tv;

    /**
     * <p>Setting content view</p>
     *
     * @param savedInstanceState saved instance of the Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.termsandcond_activity);
        initializeToolBar();
        initializeVariables();

    }

    private void initializeToolBar() {
        Toolbar sign_up_actionbar = findViewById(R.id.sign_up_actionbar);

        setSupportActionBar(sign_up_actionbar);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sign_up_actionbar.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        sign_up_actionbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * <p>here we are initialize all view element</p>
     */
    private void initializeVariables() {
        RelativeLayout terms_conditions_Rl = findViewById(R.id.terms_conditions_Rl);
        RelativeLayout privacy_policy_Rl = findViewById(R.id.privacy_policy_Rl);
        TextView tv_center = findViewById(R.id.tv_center);
        tv_center.setText(R.string.loginTitle);
        terms_conditions_Tv = findViewById(R.id.terms_conditions_Tv);
        privacy_policy_Tv = findViewById(R.id.privacy_policy_Tv);
        tv_center.setText(getResources().getString(R.string.legal));
        privacy_policy_Rl.setOnClickListener(this);
        terms_conditions_Rl.setOnClickListener(this);
        Typeface clanProNarNews = Typeface.createFromAsset(this.getAssets(), VariableConstant.lightfont);

        Typeface clanproNarMedium = Typeface.createFromAsset(this.getAssets(), VariableConstant.regularfont);
        tv_center.setTypeface(clanproNarMedium);
        terms_conditions_Tv.setTypeface(clanProNarNews);
        privacy_policy_Tv.setTypeface(clanProNarNews);
    }

    /**
     * <p>override the onclick method of OnClickListener</p>
     *
     * @param v View of the clicked
     * @see View.OnClickListener
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.terms_conditions_Rl) {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("Link", VariableConstant.TERMS_LINK);
            intent.putExtra("Title", getResources().getString(R.string.terms_and_conditions));
            intent.putExtra("COMINFROM", getResources().getString(R.string.terms_and_conditions));
            startActivity(intent);
            overridePendingTransition(R.anim.side_slide_in, R.anim.side_slide_out);
        } else {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("Link", VariableConstant.PRIVECY_LINK);
            intent.putExtra("Title", getResources().getString(R.string.privacy_policy));
            intent.putExtra("COMINFROM", getResources().getString(R.string.terms_and_conditions));
            startActivity(intent);
            overridePendingTransition(R.anim.side_slide_in, R.anim.side_slide_out);
        }

    }

    /**
     * onBack pressed close the activity.
     */
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.side_slide_in, R.anim.side_slide_out);
    }
}

