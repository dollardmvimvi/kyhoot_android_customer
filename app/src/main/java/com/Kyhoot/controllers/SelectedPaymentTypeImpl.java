package com.Kyhoot.controllers;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.SelectedCardInfoInterface;
import com.Kyhoot.pojoResponce.CardGetData;
import com.Kyhoot.pojoResponce.CardGetResponse;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

public class SelectedPaymentTypeImpl implements SelectedCardInfoInterface.SelectedPresenter
{
    SharedPrefs manager;

    SelectedCardInfoInterface.SelectedView selectedView;

    public SelectedPaymentTypeImpl( SelectedCardInfoInterface.SelectedView view) {
        this.selectedView=view;
    }



    @Override
    public void onGetCards(SharedPrefs sharedPrefs)
    {
        final Gson gson=new Gson();
        this.manager=sharedPrefs;
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL + "card",
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang
                , new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                            CardGetResponse cardGetResponse = gson.fromJson(result, CardGetResponse.class);
                            if (result!=null) {
                                ArrayList<CardGetData> cardsList = cardGetResponse.getData();
                                if (cardsList.size()>0) {
                                    Log.d("TAG", "onNextCARDBODY: "+cardsList.get(0).getBrand()
                                            +" "+cardsList.get(0).getFunding());
                                    selectedView.addItems(cardsList);
                                }
                                selectedView.onHideProgress();
                            } else {
                                selectedView.onHideProgress();
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try {
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                            selectedView.onError(errorHandel.getMessage());
                        } catch (Exception e) {
                            selectedView.onError(e.getMessage());
                        }
                    }
                });
    }

}
