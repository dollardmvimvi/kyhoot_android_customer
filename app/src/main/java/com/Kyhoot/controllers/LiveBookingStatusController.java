package com.Kyhoot.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.CancelAdapter;
import com.Kyhoot.interfaceMgr.LiveBookingStatusInt;
import com.Kyhoot.interfaceMgr.TimeDuration;
import com.Kyhoot.main.BookingPendingAccept;
import com.Kyhoot.main.GoogleRoute;
import com.Kyhoot.main.LiveStatus;
import com.Kyhoot.main.MenuActivity;
import com.Kyhoot.pojoResponce.CancelReasonPojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.LiveBookingStatusPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.BitmapCustomMarker;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TimeZone;

/**
 * <h>LiveBookingStatusController</h>
 * Created by Ali on 11/8/2017.
 */

public class LiveBookingStatusController {

    private Marker proMarker = null, ownMArker = null;
    private SharedPrefs sharedPrefs, prefs;
    private LiveBookingStatusInt liveBookingStatusInt;
    private ProgressDialog pDialog;
    private Gson gson;
    private Dialog indialog;
    private Context mcontext;
    private BookingPendingAccept bookingPendingAccept;
    private LiveStatus liveBookingStatus;
    private long bid;
    private String etaTime = "";
    private static final String TAG = "LiveBookingStatusContro";
    TimeZone  timeZone;
    // private LatLng previousLatlng;


    public LiveBookingStatusController(SharedPrefs sharedPrefs, LiveBookingStatusInt liveBookingStatusInt) {
        this.sharedPrefs = sharedPrefs;
        this.liveBookingStatusInt = liveBookingStatusInt;
    }


    public LiveBookingStatusController() {

    }

    /**
     * BugId: https://trello.com/c/61WUELbk
     * Bug: App Crashes When we do a Services Booking and then Ride booking when we get few keys as empty string
     * Fix: Removed the Strongly Typed primitives check so that even if the values change from the Number to a String
     * Developer: @author 3Embed
     */

    public static final TypeAdapter<Number> UNRELIABLE_INTEGER = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            JsonToken jsonToken = in.peek();
            switch (jsonToken) {
                case NUMBER:
                case STRING:
                    String s = in.nextString();
                    try {
                        return Integer.parseInt(s);
                    } catch (NumberFormatException ignored) {
                    }
                    try {
                        return (int)Double.parseDouble(s);
                    } catch (NumberFormatException ignored) {
                    }
                    return null;
                case NULL:
                    in.nextNull();
                    return null;
                case BOOLEAN:
                    in.nextBoolean();
                    return null;
                default:
                    throw new JsonSyntaxException("Expecting number, got: " + jsonToken);
            }
        }
        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };

    public static final TypeAdapterFactory UNRELIABLE_INTEGER_FACTORY = TypeAdapters.newFactory(int.class, Integer.class, UNRELIABLE_INTEGER);

    // /slave/booking/{bookingId}
    public void doGetBookingDetails(long bookingId) {
        Log.d(this.getClass().getSimpleName(), "doGetBookingDetails: "+VariableConstant.SERVICE_URL + "booking/" + bookingId);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "booking/" + bookingId,
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        Log.d("TAG", "LIVESTATUSonSuccess: "+result);
                        try {

                            Gson gson = new GsonBuilder()
                                    .registerTypeAdapterFactory(UNRELIABLE_INTEGER_FACTORY)
                                    .create();
                            LiveBookingStatusPojo liveBookingStatusPojo = gson.fromJson(result, LiveBookingStatusPojo.class);
                            liveBookingStatusPojo.getData().getStatus();
                            LatLng latLng = new LatLng(liveBookingStatusPojo.getData().getLatitude(),
                                    liveBookingStatusPojo.getData().getLongitude());
                            liveBookingStatusInt.onStatus(liveBookingStatusPojo.getData().getStatus(),
                                    liveBookingStatusPojo.getData().getPaymentMethod(),
                                    liveBookingStatusPojo.getData().getSignURL(),latLng,liveBookingStatusPojo.getData().getCurrencySymbol());
                            liveBookingStatusInt.onSuccess();
                            liveBookingStatusInt.onBookingTimer(liveBookingStatusPojo.getData().getBookingTimer());
                            liveBookingStatusInt.onJobStatusTime(liveBookingStatusPojo.getData().getJobStatusLogs());
                            liveBookingStatusInt.onProviderDtls(liveBookingStatusPojo.getData().getProviderDetail());
                            liveBookingStatusInt.onAccounting(liveBookingStatusPojo.getData().getAccounting());
                            liveBookingStatusInt.onGigTimeServices(liveBookingStatusPojo.getData().getCart());
                            if(liveBookingStatusPojo.getData().getStatus()==8){
                                liveBookingStatusInt.setJobTimer();
                            }

                            if (liveBookingStatusPojo.getData().getCancellationAfterXMinute()>0) {
                                VariableConstant.CANCELLATION_AFTER_X_MIN=liveBookingStatusPojo.getData().getCancellationAfterXMinute();
                                liveBookingStatusInt.setCancellationAfterXmin(liveBookingStatusPojo.getData().getCancellationAfterXMinute());
                            }

                            if (liveBookingStatusPojo.getData().getJobStatusLogs()!=null) {
                                VariableConstant.bookingAcceptedTime=liveBookingStatusPojo.getData().getJobStatusLogs().getAcceptedTime();
                            }
                        } catch (Exception e) {
                            liveBookingStatusInt.onError(e.getMessage());
                        }


                    }

                    @Override
                    public void onError(String headerError, String error) {
                        //502 result
                        Log.d(this.getClass().getSimpleName(), "onError: headerError:"+headerError+" Error: "+error);
                        if(headerError.equals("502"))
                        {
                            liveBookingStatusInt.onError("Server issue");
                        }else if(headerError.equals("549")){
                            liveBookingStatusInt.onError("Socket timeout");
                        }else
                        {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                {
                                    if (headerError.equals("440")) {
                                        ErrorHandel errHandle = new Gson().fromJson(error, ErrorHandel.class);
                                        getAccessToken(errHandle.getData());
                                    } else {
                                        liveBookingStatusInt.onError(errorHandel.getMessage());
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    /**
     * This method is called to initiate a new booking when the
     * scheduled booking which has been accepted by provider earlier
     * with the previous bookingId after we show a popup to end user to book or not.
     * if he clicks "OK" then call this API to initiate a new Booking
     * @author Pramod
     * @since 15-May-2019.
     * @param bookingId previous bookingId to trigger the new booking in the system
     */
    public void triggerNewScheduleBooking(long bookingId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "bookingByBookingId",
                OkHttpConnection.Request_type.POST, jsonObject, sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try {
                            liveBookingStatusInt.onSuccessTriggerScheduleBook();
                            VariableConstant.SHOWPOPUP = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }

    private void getAccessToken(String data) {

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {


                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            Log.d("TAG", "onSuccessACCESSTOKE: " + jsonobj.getString("data"));
                            sharedPrefs.setSession(jsonobj.getString("data"));
                            liveBookingStatusInt.onSessionError();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {

                    }
                });
    }

    public void setStatusImageUi(Activity mActivity, int status, ImageView ivLiveOnTheWayTime,
                                 ImageView ivLiveArrivedTime, ImageView ivLiveEventStartedTime,
                                 ImageView ivLiveCompletedTime, ImageView ivLiveInvoiceTime) {
        switch (status) {
            case 6:
                ivLiveOnTheWayTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveArrivedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp));
                //ontheway
                break;
            case 7:
                ivLiveOnTheWayTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveArrivedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveEventStartedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp));
                //arrived
                break;
            case 8:
                ivLiveOnTheWayTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveArrivedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveEventStartedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveCompletedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp));
                //timerStarted
                break;
            case 9:
                ivLiveOnTheWayTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveArrivedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveEventStartedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveCompletedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveInvoiceTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp));
                //time completed
                break;
            case 10:
                ivLiveOnTheWayTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveArrivedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveEventStartedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveCompletedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                ivLiveInvoiceTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
                //invoiceRaised
                break;
        }
    }

    public void setStatusTextUi(Activity mActivity, int status, TextView tvLiveOnTheWayTime,
                                TextView tvLiveOnTheWayArtist, TextView tvLiveArrivedTime,
                                TextView tvLiveArrivedArtist, TextView tvLiveEventStartedTime,
                                TextView tvLiveEventStartedArtist, TextView tvLiveCompletedTime,
                                TextView tvLiveCompletedArtist, TextView tvLiveInvoiceTime,
                                TextView tvLiveInvoiceArtist) {
        switch (status) {
            case 6:
                //ontheway
                tvLiveOnTheWayTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveOnTheWayArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedTime.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                tvLiveArrivedArtist.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                break;
            case 7:
                tvLiveOnTheWayTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveOnTheWayArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedTime.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                tvLiveEventStartedArtist.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                //arrived
                break;
            case 8:
                tvLiveOnTheWayTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveOnTheWayArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveCompletedTime.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                tvLiveCompletedArtist.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                //timerStarted
                break;
            case 9:
                tvLiveOnTheWayTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveOnTheWayArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveCompletedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveCompletedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveInvoiceTime.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                tvLiveInvoiceArtist.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
                //time completed
                break;
            case 10:
                tvLiveOnTheWayTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveOnTheWayArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveArrivedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveEventStartedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveCompletedTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveCompletedArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveInvoiceTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                tvLiveInvoiceArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
                //invoiceRaised
                break;
        }


    }

    public void setJobStatusTimer(int bookStatus, TextView tvLiveReqTime, TextView tvLiveAcptTime,
                                  TextView tvLiveOnTheWayTime, TextView tvLiveArrivedTime,
                                  TextView tvLiveEventStartedTime, TextView tvLiveCompletedTime, TextView tvLiveInvoiceTime,
                                  LiveBookingStatusPojo.LiveBookingData.JobBookingStatus jobBookingStatus) {
        String formateTime;
        String formateTimeSplit[];
        if(VariableConstant.TIMEZONE!=null) {
            timeZone = VariableConstant.TIMEZONE;
        }
        else{
            timeZone=VariableConstant.TIMEZONE;
        }
        switch (bookStatus) {

            case 3:

                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getRequestedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveReqTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getAcceptedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveAcptTime.setText(formateTime);
                break;
            case 6:
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getRequestedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveReqTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getAcceptedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveAcptTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getOnthewayTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveOnTheWayTime.setText(formateTime);
                tvLiveOnTheWayTime.setVisibility(View.VISIBLE);
                //ontheway
                break;
            case 7:
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getRequestedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveReqTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getAcceptedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveAcptTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getOnthewayTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveOnTheWayTime.setText(formateTime);
                tvLiveOnTheWayTime.setVisibility(View.VISIBLE);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getArrivedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveArrivedTime.setText(formateTime);
                tvLiveArrivedTime.setVisibility(View.VISIBLE);
                //arrived
                break;
            case 8:
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getRequestedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveReqTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getAcceptedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveAcptTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getOnthewayTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveOnTheWayTime.setText(formateTime);
                tvLiveOnTheWayTime.setVisibility(View.VISIBLE);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getArrivedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveArrivedTime.setText(formateTime);
                tvLiveArrivedTime.setVisibility(View.VISIBLE);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getStartedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveEventStartedTime.setText(formateTime);
                tvLiveEventStartedTime.setVisibility(View.VISIBLE);
                //timerStarted
                break;
            case 9:
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getRequestedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveReqTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getAcceptedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveAcptTime.setText(formateTime);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getOnthewayTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveOnTheWayTime.setText(formateTime);
                tvLiveOnTheWayTime.setVisibility(View.VISIBLE);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getArrivedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveArrivedTime.setText(formateTime);
                tvLiveArrivedTime.setVisibility(View.VISIBLE);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getStartedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveEventStartedTime.setText(formateTime);
                tvLiveEventStartedTime.setVisibility(View.VISIBLE);
                formateTimeSplit = Utilities.jobStatusTime(jobBookingStatus.getCompletedTime());
                formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
                tvLiveCompletedTime.setText(formateTime);
                tvLiveCompletedTime.setVisibility(View.VISIBLE);
                //time completed
                break;
            case 10:

                //invoiceRaised
                break;
        }
    }

    public void onRxJavaUiUpdate(Activity mActivity, long statusUpdateTime, TextView tvLiveOnTheWayTime, TextView tvLiveOnTheWayArtist,
                                 TextView tvLiveArrivedTime, TextView tvLiveArrivedArtist, ImageView ivLiveOnTheWayTime
            , ImageView ivLiveArrivedTime) {
        String formateTime;
        String formateTimeSplit[];
        formateTimeSplit = Utilities.jobStatusTime(statusUpdateTime);
        formateTime = formateTimeSplit[0] + '\n' + formateTimeSplit[1];
        tvLiveOnTheWayTime.setText(formateTime);
        tvLiveOnTheWayTime.setVisibility(View.VISIBLE);

        tvLiveOnTheWayTime.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
        tvLiveOnTheWayArtist.setTextColor(Utilities.getColor(mActivity, R.color.red_login));
        tvLiveArrivedTime.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
        tvLiveArrivedArtist.setTextColor(Utilities.getColor(mActivity, R.color.black_john));
        ivLiveOnTheWayTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_round));
        ivLiveArrivedTime.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp));
    }

    public Marker plotMArker(final Activity activity, LatLng cusLatLng, final LatLng proLatLng, final GoogleMap mMap, String proProfilePic) {

        String url = GoogleRoute.makeURL(cusLatLng.latitude, cusLatLng.longitude,
                proLatLng.latitude, proLatLng.longitude);
        GoogleRoute.startPlotting(mMap, url, new TextView(activity), new TimeDuration()
        {
            @Override
            public void onDistanceTime(String timeEta,String distance) {
                etaTime = timeEta;

                liveBookingStatusInt.onDistanceTime(distance,timeEta);

                if(proMarker!=null)
                {
                    BitmapCustomMarker bitmapCustomMarker = new BitmapCustomMarker(activity, etaTime);
                    proMarker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmapCustomMarker.createBitmap()));
                }

            }
        });

        if (proMarker != null) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(proLatLng);
            mMap.moveCamera(center);
            return proMarker;

        } else {

            BitmapCustomMarker bitmapCustomMarker = new BitmapCustomMarker(activity, etaTime);
            proMarker = mMap
                    .addMarker(new MarkerOptions()
                            .position(proLatLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmapCustomMarker.createBitmap()))
                    );
            CameraUpdate center = CameraUpdateFactory.newLatLng(cusLatLng);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(11.0f);
            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
        }


        ownMArker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.add_new_address_red_pin_icon))
                .position(cusLatLng));
        ownMArker.setFlat(false);

        return proMarker;
    }

    private void animateMarker(LatLng latLng, GoogleMap mMap)
    {
       /* final Location mCurrentLoc=new Location("current_location");
        final Location mPreviousLoc=new Location("previous_location");*/
        final LatLng finalPosition = latLng;
        final LatLng startPosition = proMarker.getPosition();

        //  animateMa();
        //  final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
        final Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(new LatLng(startPosition.latitude,startPosition.longitude));
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 4000;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        // final Interpolator interpolator = new LinearInterpolator();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d("LiveBooking","TRACK marker inside run "+proMarker);


                long elapsed = SystemClock.uptimeMillis() - start;

               /* float t = interpolator.getInterpolation((float) elapsed
                        / duration);*/
                float t =  elapsed/duration;
                float v = interpolator.getInterpolation(t);
               /* double lng = t * mCurrentLoc.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * mCurrentLoc.getLatitude() + (1 - t)
                        * startLatLng.latitude;*/
                // proMarker.setPosition(new LatLng(lat, lng));
                LatLng currentPosition = new LatLng(startPosition.latitude*(1-t)+finalPosition.latitude*t,
                        startPosition.longitude*(1-t)+finalPosition.longitude*t);

                proMarker.setPosition(currentPosition);
                proMarker.setAnchor(0.5f, 0.5f);
                //  proMarker.setRotation(bearing);
                proMarker.setFlat(true);
                Log.d("LiveBooking","TRACK marker inside setFlat "+proMarker);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
        // previousLatlng = new LatLng(mCurrentLoc.getLatitude(),mCurrentLoc.getLongitude());
    }

    public void cancelBooking(final Context mcontext, SharedPrefs sprefs, AlertProgress alertProgress, final boolean isPending, long bid,final boolean isCancellable) {
        prefs = sprefs;
        this.bid = bid;
        if (isPending) {
            bookingPendingAccept = (BookingPendingAccept) mcontext;
        } else {
            liveBookingStatus = (LiveStatus) mcontext;
        }
        gson = new Gson();
        this.mcontext = mcontext;
        pDialog = alertProgress.getProgressDialog(mcontext.getString(R.string.wait));
        pDialog.show();
        Log.d(TAG, "cancelBooking: "+VariableConstant.SERVICE_URL + "cancelReasons/"+bid);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "cancelReasons/"+bid, OkHttpConnection.Request_type.GET
                , new JSONObject(), sprefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        try{
                            final CancelReasonPojo cancel = gson.fromJson(result, CancelReasonPojo.class);
                            if(cancel.getData().getCancellationFeeApplied()&&isCancellable){
                                AlertDialog.Builder aBuilder=new AlertDialog.Builder(mcontext);
                                aBuilder.setTitle("Cancellation charges")
                                        .setMessage(cancel.getMessage())
                                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                cancelReasonDialog(cancel.getData().getReason(), isPending);
                                            }
                                        })
                                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                aBuilder.create().show();
                            }else {
                                cancelReasonDialog(cancel.getData().getReason(), isPending);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        if(headerError.equals("549")){
                            Toast.makeText(mcontext, "Timed Out", Toast.LENGTH_SHORT).show();
                        }
                        pDialog.dismiss();
                    }
                });
    }

    private void cancelReasonDialog(final ArrayList<CancelReasonPojo.CancelReasonData.Reason> data, final boolean isPending) {
        if (isPending) {
            bookingPendingAccept.selectedpostion = -1;
        }
        AppTypeface appTypeface = AppTypeface.getInstance(mcontext);
        final int[] reasontext = new int[1];
        indialog = new Dialog(mcontext);
        indialog.setCanceledOnTouchOutside(true);
        indialog.setCancelable(true);
        indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        indialog.setContentView(R.layout.cancel_dialog);
        indialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ListView listcancel = indialog.findViewById(R.id.listcancel);
        TextView cancelDialogReson = indialog.findViewById(R.id.cancelDialogReson);
        TextView cancelDialog = indialog.findViewById(R.id.cancelDialog);
        TextView submitcancel = indialog.findViewById(R.id.submitcancel);
        cancelDialogReson.setTypeface(appTypeface.getHind_semiBold());
        submitcancel.setTypeface(appTypeface.getHind_semiBold());
        cancelDialog.setTypeface(appTypeface.getHind_semiBold());
        cancelDialog.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                indialog.dismiss();
            }
        });
        final CancelAdapter cancelAdapter = new CancelAdapter(mcontext, data, isPending);
        listcancel.setAdapter(cancelAdapter);
        listcancel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isPending) {
                    bookingPendingAccept.selectedpostion = position;
                } else {
                    liveBookingStatus.selectedpostion = position;
                }

                cancelAdapter.notifyDataSetChanged();
                reasontext[0] = data.get(position).getRes_id();
            }
        });
        submitcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reasontext[0] != 0) {
                    cancelapptservice(reasontext[0]);
                    indialog.dismiss();
                } else {
                    Toast.makeText(mcontext, "Please provide valid reason", Toast.LENGTH_SHORT).show();
                }
            }
        });

        indialog.show();
    }

    private void cancelapptservice(final int reason) {

        pDialog.show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bid);
            jsonObject.put("resonId", reason);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "cancelBooking",
                OkHttpConnection.Request_type.PATCH, jsonObject, prefs.getSession(),
                VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        cancelResponce(result, reason);
                        pDialog.dismiss();
                        Log.d("TAG", "ALIresponcecancel " + result);
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        pDialog.dismiss();
                    }
                });

    }

    private void cancelResponce(String result, int reason) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
        builder.setTitle(mcontext.getString(R.string.cancel));
        builder.setCancelable(false);
        //builder.setMessage(cancellationJava.getErrMsg());
        JSONObject jsonObject;
        Object message="";
        try {
            jsonObject = new JSONObject(result);
            message = jsonObject.get("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(((String)message).isEmpty()){
            builder.setMessage("Booking Cancelled Successfully");
        }else{
            builder.setMessage(((String)message));
        }

        builder.setPositiveButton(mcontext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VariableConstant.isSplashCalled = true;
                Intent intent = new Intent(mcontext, MenuActivity.class);
               /* intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mcontext.startActivity(intent);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void makeCallToProvider(String phoneNo,Context mcontext)
    {
        if (!phoneNo.equals("")) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phoneNo));//+ countryCode+""+
            mcontext.startActivity(callIntent);
        } else {
            Toast.makeText(mcontext, mcontext.getApplicationContext().getResources().getString(R.string.driverPhoneNoNotAvailable), Toast.LENGTH_SHORT).show();
        }
    }


}



