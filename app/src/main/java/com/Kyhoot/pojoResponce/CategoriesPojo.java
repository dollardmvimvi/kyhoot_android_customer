package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 31/1/18.
 */

public class CategoriesPojo
{
    private String message;

    private GetCategoryData data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public GetCategoryData getData ()
    {
        return data;
    }

    public void setData (GetCategoryData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }
}
