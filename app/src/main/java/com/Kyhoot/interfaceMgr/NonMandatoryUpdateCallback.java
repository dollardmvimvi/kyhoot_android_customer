package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 25/5/18.
 */
public interface NonMandatoryUpdateCallback {
    public void onLaterClicked();
    public void onUpdateClicked();
}
