package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.DistanceMatrixPojo;

/**
 * Created by ${3Embed} on 19/4/18.
 */
public interface ETAResultInterface {
    //public void onEtaUpdates(String result, String distance);

    void onEtaUpdates(DistanceMatrixPojo distanceMatrixPojo);
}
