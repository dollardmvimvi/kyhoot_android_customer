package com.Kyhoot.main.bookingType;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.adapter.RepeatDaysAdapter;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class RepeatDays extends AppCompatActivity implements RepeatDaysAdapter.RepeatDaysSelected {

    @BindView(R.id.checkBoxAllDays)
    CheckBox checkBoxAllDays;
    @BindView(R.id.checkBoxWeekendDays)
    CheckBox checkBoxWeekendDays;
    @BindView(R.id.checkBoxWeekDays)
    CheckBox checkBoxWeekDays;
    @BindView(R.id.tvRepeatDays)
    TextView tvRepeatDays;
    @BindView(R.id.tv_center)
    TextView tv_center;
    @BindView(R.id.tvSelectRepeatDays)
    TextView tvSelectRepeatDays;
    @BindView(R.id.toolBarRepeatDays)
    Toolbar toolBarRepeatDays;
    @BindView(R.id.recyclerRepeatDays)
    RecyclerView recyclerRepeatDays;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> arrayList;
    private ArrayList<String> selectedArrayList = new ArrayList<>();
    private ArrayList<String> allDaysArray = new ArrayList<>();
    private AppTypeface appTypeface;
    private RepeatDaysAdapter repeatDaysAdapter;
    private AlertProgress alertProgress;
    private static final String TAG = "RepeatDays";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat_days);

        ButterKnife.bind(this);
        appTypeface = AppTypeface.getInstance(this);
        if(getIntent().getExtras()!=null)
        {
            arrayList = getIntent().getStringArrayListExtra("SelectedDays");
            allDaysArray = getIntent().getStringArrayListExtra("allDaysOfTheWeek");
        }
        Log.d(TAG, "onCreate: arrayList:"+arrayList);
        Log.d(TAG, "onCreate: allDaysArray"+allDaysArray);

        Comparator<String> dateComparator = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                try{
                    SimpleDateFormat format = new SimpleDateFormat("EEE", Locale.getDefault());
                    Date d1 = format.parse(s1);
                    Date d2 = format.parse(s2);
                    if(d1.equals(d2)){
                        return s1.substring(s1.indexOf(" ") + 1).compareTo(s2.substring(s2.indexOf(" ") + 1));
                    }else{
                        Calendar cal1 = Calendar.getInstance();
                        Calendar cal2 = Calendar.getInstance();
                        cal1.setTime(d1);
                        cal2.setTime(d2);
                        return cal1.get(Calendar.DAY_OF_WEEK) - cal2.get(Calendar.DAY_OF_WEEK);
                    }
                } catch(ParseException pe) {
                    pe.printStackTrace();
                }
                return 0;
            }
        };
        Collections.sort(allDaysArray, dateComparator);

        for (int i=0;i<allDaysArray.size();i++) {
            Log.d(TAG, "onCreate: Sorted allDaysArray " + allDaysArray.get(i));
        }

        alertProgress = AlertProgress.getInstance(this);
        setToolBar();
        setRecyclerView();
    }

    private void setRecyclerView() {

        layoutManager = new LinearLayoutManager(this);
        repeatDaysAdapter = new RepeatDaysAdapter(this,allDaysArray,arrayList,this);
        recyclerRepeatDays.setLayoutManager(layoutManager);
        recyclerRepeatDays.setAdapter(repeatDaysAdapter);
        if(arrayList.size()==7)
        {
            checkBoxAllDays.setChecked(true);
            /* *
             * Commented the below line as @selectedArrayList was added twice.
             * because it is already added when checkBoxAllDays is set as true and {@link #onCheckBoxCheck(CompoundButton, boolean)} is called automatically
             */
            //selectedArrayList.addAll(arrayList);
            repeatDaysAdapter.notifyDataSetChanged();
        }
        checkBoxAllDays.setTypeface(appTypeface.getHind_semiBold());
        checkBoxWeekendDays.setTypeface(appTypeface.getHind_semiBold());
        checkBoxWeekDays.setTypeface(appTypeface.getHind_semiBold());
        tvRepeatDays.setTypeface(appTypeface.getHind_semiBold());

    }

    private void setToolBar() {
        tv_center.setText(R.string.shift_RepeatDays);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        tvSelectRepeatDays.setTypeface(appTypeface.getHind_semiBold());
        setSupportActionBar(toolBarRepeatDays);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolBarRepeatDays.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolBarRepeatDays.setNavigationOnClickListener(view -> onBackPressed());
    }

    @OnCheckedChanged({R.id.checkBoxAllDays,R.id.checkBoxWeekDays,R.id.checkBoxWeekendDays})
    public void onCheckBoxCheck(CompoundButton v, boolean b)
    {
        switch (v.getId())
        {
            case R.id.checkBoxAllDays:
                checkBoxWeekDays.setChecked(false);
                checkBoxWeekendDays.setChecked(false);
                if(b)
                {
                    selectedArrayList.clear();
                    repeatDaysAdapter.makeFlag(true);
                    selectedArrayList.addAll(allDaysArray);
                    repeatDaysAdapter.notifyDataSetChanged();
                    Log.d(TAG, "onCheckBoxCheck: "+selectedArrayList);
                }else
                    selectedArrayList.clear();
                break;
            case R.id.checkBoxWeekDays:
                checkBoxAllDays.setChecked(false);
                checkBoxWeekendDays.setChecked(false);
                if(b)
                {
                    ArrayList<String> arrWeekDays = new ArrayList<>(allDaysArray);
                    arrWeekDays.remove(arrWeekDays.size()-1);
                    arrWeekDays.remove(arrWeekDays.size()-1);
                    selectedArrayList.clear();
                    selectedArrayList.addAll(arrWeekDays);
                    repeatDaysAdapter.makeFlag(true);
                    repeatDaysAdapter.notifyDataSetChanged();
                }else
                    selectedArrayList.clear();
                break;
            case R.id.checkBoxWeekendDays:
                checkBoxAllDays.setChecked(false);
                checkBoxWeekDays.setChecked(false);
                if(b)
                {
                    ArrayList<String> arrWeekDays = new ArrayList<>();
                    arrWeekDays.add("Saturday");
                    arrWeekDays.add("Sunday");
                    selectedArrayList.clear();
                    selectedArrayList.addAll(arrWeekDays);
                    repeatDaysAdapter.makeFlag(true);
                    repeatDaysAdapter.notifyDataSetChanged();
                }else
                    selectedArrayList.clear();
                break;
        }
    }

    @OnClick(R.id.tvRepeatDays)
    public void onSaveClicked()
    {
        if(allDaysArray.size()>6)
            callNextIntent();
        else
        {
            if(selectedArrayList.size()>0)
                callNextIntent();
            else
                alertProgress.alertinfo(getString(R.string.shift_requirement));
        }

    }

    public void callNextIntent()
    {
        Intent intent = new Intent();
        intent.putExtra("SELECTEDDAys",selectedArrayList);
        setResult(RESULT_OK,intent);
        Log.d(TAG, "callNextIntent: "+selectedArrayList);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onRepeatDays()
    {
        checkBoxAllDays.setSelected(false);
        checkBoxAllDays.setChecked(false);
        selectedArrayList.clear();
        Log.d(TAG, "onRepeatDays: "+selectedArrayList);
    }

    @Override
    public void onRepeatDaysList(ArrayList<String> selectList)
    {
        selectedArrayList.clear();
        selectedArrayList.addAll(selectList);
        Log.d(TAG, "onRepeatDaysList: "+selectedArrayList);
    }
}
