package com.Kyhoot.controllers;

import android.graphics.Color;

import com.github.sundeepk.compactcalendarview.domain.Event;
import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.main.ScheduleActivity;
import com.Kyhoot.models.ScheduleModel;
import com.Kyhoot.pojoResponce.Booked;
import com.Kyhoot.pojoResponce.Schedule;
import com.Kyhoot.pojoResponce.ScheduleMonthData;
import com.Kyhoot.pojoResponce.Slot;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class ScheduleController implements NetworkCheck
{

    private static final String TAG = "ConfirmBookController";
    private ScheduleModel scheduleModel;
    // private UtilInterFace.ConfirmBook confirmBook;
    private SharedPrefs sPrefs;
    private ScheduleActivity scheduleActivity;

    private boolean isFragmentAttached = false, isCurrentMonth = false;
    private String scheduleData="";

    private SimpleDateFormat serverFormat,displayHourFormat, displayHourFormatInBooked, displayPeriodFormat;

    public ScheduleController(ScheduleActivity scheduleActivity, SharedPrefs sPrefs) {

        this.sPrefs = sPrefs;
        this.scheduleActivity = scheduleActivity;

        scheduleModel = new ScheduleModel(this,scheduleActivity);

        displayHourFormat = new SimpleDateFormat("h:mm", Locale.US);
        displayHourFormatInBooked = new SimpleDateFormat("hh:mm a", Locale.US);
        displayPeriodFormat = new SimpleDateFormat("a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    }


    public void checkSchedule(SharedPrefs sPrefs ,String date, boolean isCurrentMonth){
        /*this.isCurrentMonth = isCurrentMoCnth;
        if(!isCurrentMonth || sPrefs.getScheduleData().equals(""))
        {
            scheduleActivity.showProgress();
        }
        else
        {
            onSuccessGetSchedule(sPrefs.getScheduleData());
        }*/
        scheduleActivity.showProgress();
        scheduleModel.getShedule(date,sPrefs);
    }


    /**
     * Returing the created events based on the sheduleMontDatas
     * create blue color for schedule day and differrent color for booked color
     * @param scheduleMonthDatas list of scheduleMonthDatas
     * @param calendarEvents List of calendar events
     * @return calendarEvents
     */
    public ArrayList<Event> getEvents(ArrayList<ScheduleMonthData> scheduleMonthDatas, ArrayList<Event> calendarEvents)
    {
        for (ScheduleMonthData scheduleMonthData : scheduleMonthDatas)
        {
            for (Schedule schedule : scheduleMonthData.getSchedule())
            {
                Event event;
                if(schedule.getBooked().size() == 0)
                {
                    //color primary
                    event = new Event(Color.parseColor("#3498db"), Utilities.convertUTCToTimeStamp(scheduleMonthData.getDate()),schedule);
                }
                else
                {
                    if(!schedule.getBooked().get(schedule.getBooked().size()-1).getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE))
                    {
                        //color green for upcoming and ongoing shedule booking,
                        event = new Event(Color.parseColor("#5BC24F"), Utilities.convertUTCToTimeStamp(scheduleMonthData.getDate()),schedule);
                    }
                    else
                    {
                        //color red for completed booking
                        event = new Event(Color.parseColor("#EB4942"), Utilities.convertUTCToTimeStamp(scheduleMonthData.getDate()),schedule);
                    }
                }
                calendarEvents.add(event);
            }
        }
        return calendarEvents;
    }


    /**
     * Create slot for Each based on booked and arrange according to the time
     * @param eventList calendar event list
     * @return list of Slot
     */
    public ArrayList<Slot> createSlotFromSchedules(List<Event> eventList)
    {
        ArrayList<Slot> slots = new ArrayList<>();
        ArrayList<Schedule> schedules = new ArrayList<>();
        for(Event event : eventList)
        {
            schedules.add((Schedule) event.getData());
        }

        try {
            for(Schedule schedule : schedules)
            {
                Slot slotStartTime = new Slot();
                slotStartTime.setSlotHour(displayHourFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(schedule.getStartTime()))));
                slotStartTime.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(schedule.getStartTime()))));
                slotStartTime.setStartTimeStamp(schedule.getStartTime());
                slots.add(slotStartTime);
                if(schedule.getBooked().size() > 0)
                {
                    for(Booked booked : schedule.getBooked())
                    {
                        Slot bookedSlot = new Slot();
                        bookedSlot.setStatus(booked.getStatus());
                        bookedSlot.setBookingId(booked.getBookingId());
                        bookedSlot.setCustomerId(booked.getCustomerId());
                        bookedSlot.setSlotHour(displayHourFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setSlotEndHourBooking(displayHourFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setSlotEndPeriodBooking(displayPeriodFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setBookedStartHour(displayHourFormatInBooked.format(serverFormat.parse(Utilities.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setBookedEndHour(displayHourFormatInBooked.format(serverFormat.parse(Utilities.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setEvent(booked.getEvent());
                        bookedSlot.setCutomerName(booked.getFirstName() +" "+ booked.getLastName());
                        bookedSlot.setStartTimeStamp(booked.getStart());
                        slots.add(bookedSlot);
                    }
                }
                Slot slotEndTime = new Slot();
                slotEndTime.setSlotHour(displayHourFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(schedule.getEndTime()))));
                slotEndTime.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utilities.convertUTCToServerFormat(schedule.getEndTime()))));
                slotEndTime.setStartTimeStamp(schedule.getEndTime());
                slots.add(slotEndTime);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        Collections.sort(slots, new Comparator<Slot>() {
            @Override
            public int compare(Slot o1, Slot o2) {
                return o1.getStartTimeStamp().compareTo(o2.getStartTimeStamp());
            }
        });

        //Remove repeated Slot time (i.e) if 5 to 6 and 6 to 8 then show only 5 6 8 no need to show 6 again
       /*

       ArrayList<Integer> removablePosition = new ArrayList<>();
        int j = 0;
        for( int i=0 ; i < slots.size()-1 ; i++)
        {
            if(slots.get(i).getSlotHour().equals(slots.get(i+1).getSlotHour()))
            {
                removablePosition.add(i);
            }
        }

        for(int i = 0 ; i < removablePosition.size() ; i++)
        {
            slots.remove((int)removablePosition.get(i) - j);
            j++;
        }*/

        return slots;
    }


    @Override
    public void onNetworkAvailble() {

    }

    @Override
    public void onNetworkNotAvailble() {

    }
}
