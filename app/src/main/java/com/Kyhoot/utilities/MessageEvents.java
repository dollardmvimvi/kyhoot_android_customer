package com.Kyhoot.utilities;

import org.json.JSONObject;

/**
 * <h>MessageEvents</h>
 * Created by Ali on 9/29/2017.
 */

public class MessageEvents {
    private JSONObject jsonObj;

    public MessageEvents(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }

    public JSONObject getJsonObj() {
        return jsonObj;
    }
}
