package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 16/3/18.
 */

public class PointsProps
{
    private Paths[] paths;

    private String visible;

    private String fillColor;

    private String editable;

    private String draggable;

    private String strokeColor;

    private String fillOpacity;

    private String strokeWeight;

    private String strokeOpacity;

    public Paths[] getPaths ()
    {
        return paths;
    }

    public void setPaths (Paths[] paths)
    {
        this.paths = paths;
    }

    public String getVisible ()
    {
        return visible;
    }

    public void setVisible (String visible)
    {
        this.visible = visible;
    }

    public String getFillColor ()
    {
        return fillColor;
    }

    public void setFillColor (String fillColor)
    {
        this.fillColor = fillColor;
    }

    public String getEditable ()
    {
        return editable;
    }

    public void setEditable (String editable)
    {
        this.editable = editable;
    }

    public String getDraggable ()
    {
        return draggable;
    }

    public void setDraggable (String draggable)
    {
        this.draggable = draggable;
    }

    public String getStrokeColor ()
    {
        return strokeColor;
    }

    public void setStrokeColor (String strokeColor)
    {
        this.strokeColor = strokeColor;
    }

    public String getFillOpacity ()
    {
        return fillOpacity;
    }

    public void setFillOpacity (String fillOpacity)
    {
        this.fillOpacity = fillOpacity;
    }

    public String getStrokeWeight ()
    {
        return strokeWeight;
    }

    public void setStrokeWeight (String strokeWeight)
    {
        this.strokeWeight = strokeWeight;
    }

    public String getStrokeOpacity ()
    {
        return strokeOpacity;
    }

    public void setStrokeOpacity (String strokeOpacity)
    {
        this.strokeOpacity = strokeOpacity;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [paths = "+paths+", visible = "+visible+", fillColor = "+fillColor+", editable = "+editable+", draggable = "+draggable+", strokeColor = "+strokeColor+", fillOpacity = "+fillOpacity+", strokeWeight = "+strokeWeight+", strokeOpacity = "+strokeOpacity+"]";
    }
}
