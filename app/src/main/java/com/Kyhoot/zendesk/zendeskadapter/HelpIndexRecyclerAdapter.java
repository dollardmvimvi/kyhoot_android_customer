package com.Kyhoot.zendesk.zendeskadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.zendesk.zendeskpojo.ZendexDataEvent;

import java.util.ArrayList;
import java.util.Date;

/**
 * <h>HelpIndexRecyclerAdapter</h>
 * Created by Ali on 12/29/2017.
 */

public class HelpIndexRecyclerAdapter extends RecyclerView.Adapter
{
    private Context mContext;
    private ArrayList<ZendexDataEvent> zendexDataEvents;

    public HelpIndexRecyclerAdapter(Context mContext, ArrayList<ZendexDataEvent> zendexDataEvents) {
        this.mContext = mContext;
        this.zendexDataEvents = zendexDataEvents;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(mContext).inflate(R.layout.help_index_recycler_content,parent,false);
        return new ViewHolders(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        ViewHolders holdr = (ViewHolders) holder;

        char c = zendexDataEvents.get(position).getName().charAt(0);
        holdr.tvHelpIndexImageTextPre.setText(c+"");
        holdr.tvHelpIndexCustNamePre.setText(zendexDataEvents.get(position).getName());
        holdr.tvHelpIndexText.setText(zendexDataEvents.get(position).getBody());
        Date date = new Date(zendexDataEvents.get(0).getTimeStamp() * 1000L);
        String dateTime[] = Utilities.getFormattedDate(date).split(",");
        holdr.tvHelpIndexDateNTimePre.setText(dateTime[0]);
        holdr.tvHelpIndexTime.setText(dateTime[1]);

    }

    @Override
    public int getItemCount() {
        return zendexDataEvents.size();
    }

    private class ViewHolders extends RecyclerView.ViewHolder
    {
        private AppTypeface appTypeface;
        private TextView tvHelpIndexImageTextPre,tvHelpIndexCustNamePre,tvHelpIndexDateNTimePre
                ,tvHelpIndexText,tvHelpIndexTime;
        private ImageView ivHelpIndexImagePre;
        public ViewHolders(View itemView) {
            super(itemView);

            appTypeface = AppTypeface.getInstance(mContext);
            tvHelpIndexImageTextPre = itemView.findViewById(R.id.tvHelpIndexImageTextPre);
            tvHelpIndexCustNamePre = itemView.findViewById(R.id.tvHelpIndexCustNamePre);
            tvHelpIndexDateNTimePre = itemView.findViewById(R.id.tvHelpIndexDateNTimePre);
            tvHelpIndexText = itemView.findViewById(R.id.tvHelpIndexText);
            tvHelpIndexTime = itemView.findViewById(R.id.tvHelpIndexTime);
            ivHelpIndexImagePre = itemView.findViewById(R.id.ivHelpIndexImagePre);
            tvHelpIndexImageTextPre.setTypeface(appTypeface.getHind_semiBold());
        }
    }
}
