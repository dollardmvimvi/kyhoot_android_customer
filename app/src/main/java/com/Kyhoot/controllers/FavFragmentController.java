package com.Kyhoot.controllers;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.AccessTokenCallback;
import com.Kyhoot.interfaceMgr.FavoriteProviderCallback;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

/**
 * Created by ${3Embed} on 18/6/18.
 */
public class FavFragmentController {
    private final FavoriteProviderCallback favProviderCallback;
    private final AccessTokenCallback accessTokenCallback;

    public FavFragmentController(FavoriteProviderCallback callback, AccessTokenCallback accessTokenCallback) {
        this.favProviderCallback=callback;
        this.accessTokenCallback=accessTokenCallback;
    }

    /** <h1>getFavoriteProvider</h1>
     * This method is used to call the service from where we can get all the favorite providers data.
     * @param sharedPrefs it is the sharedpreference object
     *
     */
    public void getFavoriteProvider(final SharedPrefs sharedPrefs) {

                OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "favouriteProvider", OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String jsonResponse) {
                        Log.d("favoriteProviders", "onSuccess: " + jsonResponse + " headerResponse " + headerResponse);
                        favProviderCallback.onFavProvidersAvailable(headerResponse,jsonResponse);
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        Log.d("favoriteProviders","headererror:" + headerError + ": error: " + error);
                        if (headerError.equals("502"))
                            favProviderCallback.onFavProvidersError("502","");
                        else {
                            try{
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        accessTokenCallback.onSessionExpired();
                                        break;
                                    case "440":
                                        Utilities.getAccessToken(errorHandel.getData(),accessTokenCallback);
                                        break;
                                    default:
                                        favProviderCallback.onFavProvidersError(headerError,errorHandel.getMessage());
                                        break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }

    /**
     * <h1>unfavoriteProvider</h1>
     * <p>
     *     This method is used to call service to remove a favorite provider
     * </p>
     * @param categoryId category id of the provider
     * @param providerId id of the provider
     * @param sPrefs shared preference object
     */
    public void unFavoriteProvider(String categoryId, String providerId,SharedPrefs sPrefs) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "favouriteProvider/"+providerId+"/"+categoryId, OkHttpConnection.Request_type.DELETE, new JSONObject(), sPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String jsonResponse) {
                Log.d("unfavoriteProvider", "onSuccess: " + jsonResponse + " headerResponse " + headerResponse);
                favProviderCallback.onUnfavoritingSuccessful(headerResponse,jsonResponse);
            }

            @Override
            public void onError(String headerError, String error) {
                Log.d("favoriteProviders","headererror:" + headerError + ": error: " + error);
                if (headerError.equals("502"))
                    favProviderCallback.onUnfavoritingError("502","");
                else {
                    try{
                        ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                        switch (headerError) {
                            case "498":
                                accessTokenCallback.onSessionExpired();
                                break;
                            case "440":
                                Utilities.getAccessToken(errorHandel.getData(),accessTokenCallback);
                                break;
                            default:
                                favProviderCallback.onUnfavoritingError(headerError,errorHandel.getMessage());
                                break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
