package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.adapter.RatinReviewAdaptr;
import com.Kyhoot.controllers.ReviewFragmnetControllr;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.pojoResponce.RateReviews;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;

import java.util.ArrayList;

/**
 * <h>ReviewFragment</h>
 * Created by ${Ali} on 8/16/2017.
 */

public class ReviewFragment extends Fragment implements UtilInterFace {
    private Context mcontext;
    private AlertProgress aProgress;
    private ArrayList<RateReviews> rateReviewses;
    private RatinReviewAdaptr rAdapter;
    private ProgressDialog pDialog;
    private int pageCount = 0;
    private boolean loading = true;
    private TextView tvRevwAvrgRtin, tvavgrtnTotalRev;
    private RatingBar rtReview;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private AppTypeface appTypeface;
    private RelativeLayout rlNoReview,rlRateReviewMain;
    private RecyclerView recyclrRevw;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        mcontext = getActivity();
        initializeView(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    private void initializeView(View view) {
        rateReviewses = new ArrayList<>();
        aProgress = new AlertProgress(mcontext);
        appTypeface = AppTypeface.getInstance(mcontext);
        final SharedPrefs sPrefs = new SharedPrefs(mcontext);
        tvRevwAvrgRtin = view.findViewById(R.id.tvRevwAvrgRtin);
        tvavgrtnTotalRev = view.findViewById(R.id.tvavgrtnTotalRev);
        TextView tvNoReview = view.findViewById(R.id.tvNoReview);
        rlNoReview = view.findViewById(R.id.rlNoReview);
        rlRateReviewMain = view.findViewById(R.id.rlRateReviewMain);
        rtReview = view.findViewById(R.id.rtReview);
        rtReview.setIsIndicator(true);
        recyclrRevw = view.findViewById(R.id.recyclrRevw);
        final LinearLayoutManager lManagr = new LinearLayoutManager(mcontext);
        recyclrRevw.setLayoutManager(lManagr);
        int resId = R.anim.layoutanimation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(mcontext, resId);
        recyclrRevw.setLayoutAnimation(animation);
        rAdapter = new RatinReviewAdaptr(mcontext, rateReviewses, false);
        recyclrRevw.setAdapter(rAdapter);
        final ReviewFragmnetControllr reviewContrl = new ReviewFragmnetControllr(this);
        reviewContrl.callReviewApi(pageCount, sPrefs.getSession(), aProgress);

        tvavgrtnTotalRev.setTypeface(appTypeface.getHind_medium());
        tvNoReview.setTypeface(appTypeface.getHind_regular());

        recyclrRevw.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = lManagr.getChildCount();
                    totalItemCount = lManagr.getItemCount();
                    pastVisiblesItems = lManagr.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            pageCount++;
                            reviewContrl.callReviewApi(pageCount, sPrefs.getSession(), aProgress);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void showProgress() {
        pDialog = aProgress.getProgressDialog(mcontext.getString(R.string.reviews));
        pDialog.show();
    }

    @Override
    public void hideProgress() {

        if (pDialog != null)
            pDialog.dismiss();
    }
    @Override
    public void onSuccessReview(ValidatorPojo reviewlist) {
        ValidatorPojo.SignUpDataSid reviewList = reviewlist.getData();


        Log.d("TAG", "onSuccessReview: "+reviewlist.getData().getAverageRating() +" AVG "
                +reviewList.getAverageRating());
        if(reviewList.getReviews().size()>0)
        {

            loading = true;
            rateReviewses.addAll(reviewList.getReviews());
            rAdapter.notifyDataSetChanged();
            String avregRating = reviewList.getAverageRating() + "";
            tvRevwAvrgRtin.setText(avregRating);
            String totalReviewC = mcontext.getResources().getString(R.string.youravgrating) + " | " + reviewList.getReviewCount() + " " + mcontext.getString(R.string.reviews);
            tvavgrtnTotalRev.setText(totalReviewC);
            rtReview.setRating(reviewList.getAverageRating());

        }
        else
        {
            loading = false;
        }

        if(rateReviewses.size()>0)
        {
        }
        else
        {
            rlNoReview.setVisibility(View.VISIBLE);
            recyclrRevw.setVisibility(View.GONE);
            tvavgrtnTotalRev.setVisibility(View.GONE);
            rlRateReviewMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccess(String onSuccess) {

    }

    @Override
    public void onError(String errorMsg) {

    }

    @Override
    public void onSuccess(ArrayList<AddressPojo.AddressData> reviewpojo) {

    }
}
