package com.Kyhoot.controllers;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.CheckNetworkAvailablity;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.models.PasswordHelpModel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Validator;

/**
 * <h>PasswordHelpController</h>
 * Controls the user functionalities
 * Created by Ali on 9/14/2017.
 */

public class PasswordHelpController
{
    Gson gson;
    private Context mcontext;
    private AlertProgress aprogress;
    private String emailOrPhone;
    private CheckNetworkAvailablity chentwkAvil;
    private VerifyOtpIntrface verfyiotp;
    private boolean isEmailOrPhone;
    public PasswordHelpController(Context mcontext, CheckNetworkAvailablity chentwkAvil, VerifyOtpIntrface verfyiotp)
    {
        this.mcontext = mcontext;
        this.chentwkAvil = chentwkAvil;
        this.verfyiotp = verfyiotp;
        aprogress = new AlertProgress(mcontext);
        gson = new Gson();
    }

    public void checkEmailPhoneValid(boolean isEmail, String email,String countryCode, String phone, EditText tilForgotPassEmail, EditText tilForgotMobNo)
    {
        isEmailOrPhone = isEmail;
        if(isEmailOrPhone)
        {
            emailOrPhone = email;
            checkEmailValidity(tilForgotPassEmail);
        }
        else
        {
            emailOrPhone =  phone;
            checkPhoneValidity(countryCode,tilForgotMobNo);
        }


    }

    private void checkEmailValidity(EditText tilForgotPassEmail)
    {
        if (emailOrPhone.length() < 1) {
            tilForgotPassEmail.setError(mcontext.getResources().getString(R.string.email_mandatory));

            // tilForgotPassEmail.setErrorEnabled(true);
        } else if (!new Validator().emailValidation(emailOrPhone)) {
            tilForgotPassEmail.setError(mcontext.getResources().getString(R.string.email_invalid));
            // tilForgotPassEmail.setErrorEnabled(true);
        } else {
            tilForgotPassEmail.setError(null);
            //  tilForgotPassEmail.setErrorEnabled(false);
            checkNetWork();
        }

    }

    /**
     * <h1>checkPhoneValidity</h1>
     * <p>
     * checks the mobile number validity
     *
     * @param tilForgotMobNo mobile NUmber EditText
     *                       </p>
     */
    private void checkPhoneValidity(String countryCode,EditText tilForgotMobNo)
    {

        String mobilewithoutZero = "";
        if(!emailOrPhone.isEmpty()) {
            String mobileNumer = emailOrPhone.trim();
            switch (mobileNumer.charAt(0)) {
                case '+':
                    mobilewithoutZero = mobileNumer.substring(3);
                    break;
                case '0':
                    mobilewithoutZero=mobileNumer.substring(1);
                    break;
                default:
                    mobilewithoutZero=mobileNumer;
            }

        }

        boolean isValid=false;
        String phoneNumberE164Format = countryCode.concat(mobilewithoutZero);
        PhoneNumberUtil phoneNumberUtil=PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumberProto = null;
        try {
            phoneNumberProto = phoneNumberUtil.parse(phoneNumberE164Format, null);
            isValid = phoneNumberUtil.isValidNumber(phoneNumberProto); // returns true if valid
        } catch (NumberParseException e) {
            e.printStackTrace();
            tilForgotMobNo.setError(mcontext.getResources().getString(R.string.mobile_invalid));
        }


        if (mobilewithoutZero.isEmpty() ) {
            tilForgotMobNo.setError(mcontext.getResources().getString(R.string.contact_mandatory));
            // tilForgotMobNo.setErrorEnabled(true);
            verfyiotp.onPhoneNumError();

        }else if(!isValid){
            tilForgotMobNo.setError(mcontext.getString(R.string.invalid_mob_num));
            verfyiotp.onPhoneNumError();
        } else {
            tilForgotMobNo.setError(null);
            //   tilForgotMobNo.setErrorEnabled(false);
            emailOrPhone = mobilewithoutZero;
            checkNetWork();

        }

    }


    /*
    checks the internet availability
     */
    private void checkNetWork() {
        if(aprogress.isNetworkAvailable())
        {
            chentwkAvil.networkAvalble(emailOrPhone,isEmailOrPhone);
        }
        else
        {
            chentwkAvil.notNetworkAvalble();
        }
    }


    /**
     * <h1>emailPhoneService</h1>
     * <p>
     * calling the service for getting the password
     *
     * @param emailOrPhone   phone number or Email
     * @param isEmailOrPhone tells whether emailOrPhone is  Email or Mobile Number
     * @param countrycode    country code of the selected mobile number
     *                       </p>
     */
    public void emailPhoneService(String emailOrPhone, boolean isEmailOrPhone, String countrycode) {
        Log.d("ERRORMSG", "emailPhoneService: call ");
        new PasswordHelpModel().forGotPasswor(emailOrPhone, isEmailOrPhone, countrycode, verfyiotp, gson);
    }

    public void changeEmail(String email, SharedPrefs sprefs) {
        new PasswordHelpModel().changeEmail(email, sprefs, verfyiotp, gson);
    }

    public void changePhoneNumber(String emailOrPhone, SharedPrefs sprefs, String countryCode) {
        new PasswordHelpModel().changePhoneNumber(emailOrPhone, sprefs, verfyiotp, gson, countryCode);
    }
}
