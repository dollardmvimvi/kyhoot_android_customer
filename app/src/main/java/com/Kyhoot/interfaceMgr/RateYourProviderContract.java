package com.Kyhoot.interfaceMgr;

import android.content.Context;
import android.widget.TextView;

import com.Kyhoot.pojoResponce.InvoiceDetails;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;

import java.util.ArrayList;

/**
 * <h>RateYourProviderContract</h>
 * Created by Ali on 2/22/2018.
 */

public interface RateYourProviderContract
{
    interface Presenter
    {
        void onInvoiceDetailsCalled(long bId, SharedPrefs sharedPrefs);

        void onUpdateReview(long bId,ArrayList<InvoiceDetails.CustomerRating> stringList,String reviewMsg,SharedPrefs sharedPrefs);

        void timeMethod(TextView tbServiceAvailable, long bookingRequestedFor);

        void getStringList();

        void openDialog(Context context, AppTypeface appTypeface, InvoiceDetails.InvoiceData service, String signURL);

        void onAddToFav(String providerId,String categoryId,String sessionToken);
        void onRemoveFav(String providerId,String categoryId,String sessionToken);
    }
    interface ViewContract extends BaseView
    {

        void onGetInvoiceDetails(InvoiceDetails.InvoiceData data);

        void onRateProviderSuccess();

        void onGetStarList(ArrayList<String> stringList);

        void onFavAdded(String message);
        void onUnfavoritingSuccessful(String headerError,String message);
        void onUnfavoritingError(String headerError,String message);
    }
}
