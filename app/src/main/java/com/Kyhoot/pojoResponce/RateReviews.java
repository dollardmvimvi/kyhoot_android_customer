package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>RateReviews</h>
 * Created by Ali on 9/19/2017.
 */

public class RateReviews implements Serializable {
    /* "review": "fine DENG AHRCT JSKkjsakfk dklfdsjsf",
        "rating": 1,
        "reviewBy": "dhaval",
        "reviewAt": 1505247353*/

    /*"bookingId":168,
"rating":5,
"review":"hh",
"userId":"59dce722b121331d381cae63",
"reviewAt":1507650056,
"firstName":"Raghu",
"lastName":"",
"profilePic":"https://s3-us-west-2.amazonaws.com/dayrunner/Drivers/ProfilePics/1507561807166_0_01.png"*/

    private String bookingId, userId, firstName, lastName;
    private String review, reviewBy, profilePic;
    private float rating;
    private long reviewAt;

    public String getBookingId() {
        return bookingId;
    }

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getReview() {
        return review;
    }

    public float getRating() {
        return rating;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public long getReviewAt() {
        return reviewAt;
    }

    public void setReviewAt(long reviewAt) {
        this.reviewAt = reviewAt;
    }
}
