package com.Kyhoot.main;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.Kyhoot.R;
import com.Kyhoot.controllers.SignUpController;
import com.Kyhoot.countrypic.Country;
import com.Kyhoot.countrypic.CountryPicker;
import com.Kyhoot.countrypic.CountryPickerListener;
import com.Kyhoot.interfaceMgr.FaceBookSignupResponce;
import com.Kyhoot.interfaceMgr.ImageUploadedAmazon;
import com.Kyhoot.interfaceMgr.RegisterOTPResponce;
import com.Kyhoot.interfaceMgr.ValidateEmail;
import com.Kyhoot.interfaceMgr.Terms_privacy_interface;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.pojoResponce.SignUpData;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.HandlePictureEvents;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, FaceBookSignupResponce,
        RegisterOTPResponce,View.OnFocusChangeListener, TextWatcher,ValidateEmail,Terms_privacy_interface
        ,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final int RESOLVE_HINT = 212;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 11;
    //private final int REQUEST_CODE_SMS_PERMISSION = 2;
    private ImageView iv_reg_profile;
    private ImageView ivCountryFlag;
    private TextView tvCountryCode;
    private SharedPrefs sharedpref;
    private CallbackManager callbackManager;
    private EditText et_reg_fnm, et_reg_eml, et_reg_paswrd, et_reg_mobno, et_reg_dateofBirth, et_reg_lnm,et_reg_referral;
    private SignUpController sinupcontrolr;
    private AlertProgress alertProgresss;
    private ProgressDialog pDialog, generatePdialog;
    private HandlePictureEvents handlePicEvent;
    private TextInputLayout til_reg_fnm, til_reg_email, til_reg_paswrd, til_reg_dateofBirth,til_reg_referral;//til_reg_MobNo
    private View focusView;
    private boolean facebookFlag = false;
    private String fbId;
    private String facebookId;
    private CountryPicker mCountryPicker;
    private AppTypeface appTypeface;
    private static final String TAG = "SignUpActivity";
    private boolean facebookLogin=false;
    private String birthday;
    private String email;
    private String lName;
    private String fName;
    // Boolean flag for checking referral code while signup button click
    private boolean isReferralValid=true;
    private GoogleApiClient mGoogleApiClient;
    private int phoneNumLength=10;

    private String countrySymbol="";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        appTypeface = AppTypeface.getInstance(this);

        initializeToolBar();
        initializeEditText();
        initialize();

        //setupWindowAnimations();
        getLoginIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utilities.showKeyBoard(this,et_reg_fnm);
    }

    private void getLoginIntent() {
        Bundle bundle = getIntent().getExtras();
        Log.d("TAG", "getLoginIntent: " + bundle);
        VariableConstant.dateOB = "";
        if (bundle != null && bundle.getString("FBID") != null) {
            facebookFlag = true;
            String firstName = bundle.getString("FNAME");
            String lastName = bundle.getString("LNAME");
            String email = bundle.getString("EMAIL");
            fbId = bundle.getString("FBID");
            VariableConstant.dateOB = bundle.getString("DATEOFBIRTH");
            Log.d("TAG", "getLoginIntent: " + fbId + " Dob " + VariableConstant.dateOB);
            if (fbId != null) {
                setValueTOEditTxt(firstName, lastName, email, fbId, VariableConstant.dateOB);
            }
        }
    }

    /*
        initialzing view of the class
         */
    private void initialize()
    {
        progressDialog = new ProgressDialog(this);
        sharedpref = new SharedPrefs(this);
        alertProgresss = new AlertProgress(this);handlePicEvent = new HandlePictureEvents(this);
        sinupcontrolr = new SignUpController(this,this);
        generatePdialog = alertProgresss.getProgressDialog(getString(R.string.generatingotp));
        RelativeLayout loginwithfacbookbuttonrlout = findViewById(R.id.loginwithfacbookbuttonrlout);
        iv_reg_profile = findViewById(R.id.iv_reg_profile);
        ImageView iv_reg_AddProflPic = findViewById(R.id.iv_reg_AddProflPic);
        ivCountryFlag = findViewById(R.id.countryFlag);
        tvCountryCode = findViewById(R.id.countryCode);

        //CountryCode from SIM card
        String countryCode = Utilities.getCountryCode(SignUpActivity.this);
        tvCountryCode.setText(countryCode);

        TextView tv_login_withfb = findViewById(R.id.tv_login_withfb);
        tv_login_withfb.setText(R.string.signUpwithFacebook);
        ImageView iv_reg_calendr = findViewById(R.id.iv_reg_calendr);
        iv_reg_calendr.setOnClickListener(this);
        TextView tv_signup = findViewById(R.id.tv_signup);
        TextView tv_termsclick = findViewById(R.id.tv_termsclick);
        Utilities.setSpannableString(this,tv_termsclick,getString(R.string.termsAndConditionclick),"terms & conditions","privacy policy",this);
        loginwithfacbookbuttonrlout.setOnClickListener(this);
        iv_reg_profile.setOnClickListener(this);
        iv_reg_AddProflPic.setOnClickListener(this);
        tvCountryCode.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));


        //Setting Flag
        Country country = mCountryPicker.getCountryFlag(countryCode);
        ivCountryFlag.setImageResource(country.getFlag());

        Log.e(TAG, "flag: "+country.getCode());
        countrySymbol = country.getCode();

        tv_login_withfb.setTypeface(appTypeface.getHind_regular());
        tv_signup.setTypeface(appTypeface.getHind_regular());
        tv_termsclick.setTypeface(appTypeface.getHind_regular());
        tv_signup.setTypeface(appTypeface.getHind_semiBold());
        setListener();
    }

    /*
    initializing the editText and the Textinputlayout
     */
    private void initializeEditText()
    {

        TextView signinortextv = findViewById(R.id.signinortextv);
        signinortextv.setTypeface(appTypeface.getHind_regular());
        /*=========================EditText===============================*/
        et_reg_fnm = findViewById(R.id.et_reg_fnm);
        et_reg_eml = findViewById(R.id.et_reg_eml);
        et_reg_paswrd = findViewById(R.id.et_reg_paswrd);
        et_reg_mobno = findViewById(R.id.et_reg_mobno);

        et_reg_dateofBirth = findViewById(R.id.et_reg_dateofBirth);
        et_reg_referral = findViewById(R.id.et_reg_referral);
        et_reg_lnm = findViewById(R.id.et_reg_lnm);

        /*=========================TextInputLayout===============================*/

        til_reg_fnm = findViewById(R.id.til_reg_fnm);
        til_reg_email = findViewById(R.id.til_reg_email);
        til_reg_paswrd = findViewById(R.id.til_reg_paswrd);
        til_reg_dateofBirth = findViewById(R.id.til_reg_dateofBirth);
        til_reg_referral = findViewById(R.id.til_reg_referral);

        et_reg_fnm.setOnFocusChangeListener(this);
        et_reg_eml.setOnFocusChangeListener(this);
        et_reg_paswrd.setOnFocusChangeListener(this);
        et_reg_mobno.setOnFocusChangeListener(this);
        et_reg_dateofBirth.setOnFocusChangeListener(this);
        et_reg_dateofBirth.setOnClickListener(this);
        et_reg_fnm.setTypeface(appTypeface.getHind_regular());
        et_reg_paswrd.setTypeface(appTypeface.getHind_regular());
        et_reg_dateofBirth.setTypeface(appTypeface.getHind_regular());
        et_reg_eml.setTypeface(appTypeface.getHind_regular());
        et_reg_mobno.setTypeface(appTypeface.getHind_regular());
        et_reg_referral.addTextChangedListener(this);
        et_reg_referral.setTypeface(appTypeface.getHind_regular());

        onTextChangeListener();
        et_reg_fnm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                til_reg_fnm.setErrorEnabled(false);
                til_reg_fnm.setError("");
            }
        });

    }

    private void onTextChangeListener() {

        et_reg_paswrd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                errorFocusNull(til_reg_paswrd);

            }
        });
    }

    /*
    initializing tool bar
     */
    private void initializeToolBar()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();

        callbackManager = CallbackManager.Factory.create();
        Toolbar login_tool = findViewById(R.id.signup_tool);
        setSupportActionBar(login_tool);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        tv_center.setText(R.string.signupwithus);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay_still,R.anim.slide_down_acvtivity);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.loginwithfacbookbuttonrlout:
                // checkPermission(true);
                facebookLogin=true;
                sinupcontrolr.facebook(callbackManager,this);
                break;
            case R.id.iv_reg_AddProflPic:
            case R.id.iv_reg_profile:
                checkPermission();
                break;
            case R.id.countryCode:
                break;
            case R.id.et_reg_dateofBirth:
            case R.id.iv_reg_calendr:
                sinupcontrolr.openCalendr(et_reg_dateofBirth,til_reg_dateofBirth);
                break;
            case R.id.tv_signup:
                if(focusView!=null){
                    focusView.requestFocus();
                    if(focusView!=null && focusView.getId()==et_reg_referral.getId()){
                        et_reg_referral.setError(getString(R.string.referral_code_invalid));
                    }
                }
                else {
                    //  sinupcontrolr.checkAuthenticity();
                    Log.e(TAG, "Referral codeError  "+isReferralValid);
                    if (et_reg_referral.getText().toString().length()>0 && isReferralValid) {
                        // there is some referral code entered  and its invalid
                        Toast.makeText(this, getString(R.string.referral_code_invalid), Toast.LENGTH_SHORT).show();
                    } else {
                        // If there is no referral code entered allow to signup
                        //checkForSmsPermission();
                        setValuesToPojo();
                    }
                }
                break;
        }
    }

    /**
     * <h1>requestHint</h1>
     * <p>
     *     Construct a request for phone numbers and show the picker.
     * </p>
     *
     */
    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();


        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                mGoogleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    /**
     * <h1>setValuesToPojo</h1>
     * <p1>
     *     this sets the value on the signuppojo objts
     * </p1>
     *
     */
    private void setValuesToPojo()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("firstName", et_reg_fnm.getText().toString());
            jsonObject.put("lastName", et_reg_lnm.getText().toString());
            jsonObject.put("email", et_reg_eml.getText().toString().trim());
            jsonObject.put("password", et_reg_paswrd.getText().toString().trim());
            jsonObject.put("countryCode", tvCountryCode.getText().toString().trim());
            jsonObject.put("phone", et_reg_mobno.getText().toString().trim());
            jsonObject.put("dateOfBirth", VariableConstant.dateOB);
            jsonObject.put("profilePic", sharedpref.getImageUrl());
            jsonObject.put("referralCode",et_reg_referral.getText().toString().trim() );
            jsonObject.put("countrySymbol",countrySymbol);
            sharedpref.setOldpassword(et_reg_paswrd.getText().toString().trim());
            //googleId GOOGLE ID TAG
            if (facebookFlag)
            {
                jsonObject.put("loginType", 2);
                jsonObject.put("facebookId", fbId);
            }
            else
                jsonObject.put("loginType", 1);
            sinupcontrolr.getOTPVerificationCode(jsonObject, sharedpref);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getMobileNumber(String contactNumber) {
        contactNumber=contactNumber.replaceAll("[^0-9]","");
        if (contactNumber.length()>phoneNumLength) {
            contactNumber = contactNumber.substring(contactNumber.length()-phoneNumLength,contactNumber.length());;
        }
        return contactNumber;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)    //result code to check is the result is ok or not
        {
            return;
        }
        switch (requestCode) {

            case VariableConstant.CAMERA_PIC:
                handlePicEvent.startCropImage(handlePicEvent.newFile);
                break;

            case RESOLVE_HINT:
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);

                String formatted_ph_no = getMobileNumber(credential.getId());
                if (!TextUtils.isEmpty(formatted_ph_no))
                    et_reg_mobno.setText(formatted_ph_no);
                // credential.getId();  <-- will need to process phone number string
                break;

            case VariableConstant.GALLERY_PIC:
                if(data!=null)
                {

                    handlePicEvent.gallery(data.getData());
                }
                break;

            case VariableConstant.CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path != null)
                {
                    try {
                        File fileExist = new File(path);double size[] = Scaler.getScalingFactor(this);
                        double height = (80) * size[1];
                        double width = (80) * size[0];
                        Picasso.with(SignUpActivity.this).load(fileExist)
                                .resize((int)width, (int)height)
                                .centerCrop().transform(new CircleTransform())
                                .into(iv_reg_profile);
                        /*handlePicEvent.uploadToAmazon(VariableConstant.Amazonbucket+"/"+VariableConstant.AmazonProfileFolderName,new File(path), new ImageUploadedAmazon() {
                            @Override
                            public void onSuccessAdded(String image)
                            {
                                sharedpref.setImageUrl(image);
                            }
                            @Override
                            public void onerror(String errormsg)
                            {

                            }
                        });*/
                        progressDialog.setMessage(getString(R.string.uploading));
                        progressDialog.show();
                        new UploadFileToServer().execute(path);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    break;
                }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (handlePicEvent.newFile != null)
            outState.putString("cameraImage", handlePicEvent.newFile.getPath());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("cameraImage")) {
                Uri uri = Uri.parse(savedInstanceState.getString("cameraImage"));
                handlePicEvent.newFile = new File(uri.getPath());
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void setValueTOEditTxt(String firstName, String lastName, String email, String fbId, String dateOB) {

        //et_reg_eml.requestFocus();
        Log.d("REGISTER", "onSuccessAdded: " + dateOB);
        et_reg_fnm.setText(firstName);
        et_reg_eml.setText(email);
        et_reg_eml.requestFocus();
        et_reg_lnm.setText(lastName);
        String fb_pic = "https://graph.facebook.com/"+fbId+"/picture?type=large";
        Uri uri = Uri.parse(fb_pic);
        String dateOfBrth;
        if(dateOB.equals("")){
            dateOfBrth = "";
        }else{
            dateOfBrth = sinupcontrolr.setDateOfBirth(dateOB);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            et_reg_dateofBirth.setText(Html.fromHtml(dateOfBrth, Html.FROM_HTML_MODE_LEGACY));
        else
            et_reg_dateofBirth.setText(Html.fromHtml(dateOfBrth));

        sharedpref.setImageUrl(fb_pic);
        double size[] = Scaler.getScalingFactor(this);
        double height = (80) * size[1];double width = (80) * size[0];
        Picasso.with(SignUpActivity.this).load(uri)
                .resize((int)width, (int)height)
                .centerCrop().transform(new CircleTransform())
                .into(iv_reg_profile);
        facebookFlag = true;
        this.fbId = fbId;
    }

    /**
     * <h1>onsuccessForFacebook</h1>
     * <p>
     *
     * @param fName    facebook name
     * @param email    facebook Email
     * @param birthday facebook birthday
     * @param fbId     facebook id
     *                 </p>
     * @see FaceBookSignupResponce
     */

    @Override
    public void onsuccess(String fName, String lName, String email, String birthday, String fbId) {
        this.fName=fName;
        this.lName=lName;
        this.email=email;
        this.birthday=birthday;
        this.facebookId=fbId;
        //sinupcontrolr.checkEmailValidation(et_reg_eml.getText().toString().trim());
        sinupcontrolr.checkEmailValidationFacebook(email,this);


    }

    @Override
    public void onError(String errorMsg)
    {
        alertProgresss.alertinfo(errorMsg);
    }


    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkPermission()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE))
            {
                //if(!isFb)
                selectImage();
            }

        }else
        {
            // if(!isFb)
            selectImage();
        }

    }
    /**
     * <h1>selectImage</h1>
     * @see HandlePictureEvents
     * This mehtod is used to show the popup where we can select our images.
     */
    private void selectImage()
    {
        handlePicEvent.openDialog();
    }

    /**
     * predefined method to check run time permissions list call back
     * @param requestCode   request code
     * @param permissions:  contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean isDenine = false;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    alertProgresss.alertinfo(getResources().getString(R.string.permissiondenied)+ " " + REQUEST_CODE_PERMISSION_MULTIPLE);
                } else {
                    selectImage();
                }
                break;

           /* case REQUEST_CODE_SMS_PERMISSION:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    alertDialogTORecallOrGo();
                    alertProgresss.alertinfo(getResources().getString(R.string.permissiondenied));
                } else {
                    generatePdialog.show();
                    setValuesToPojo();
                }
                break;*/
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    /**
     * <h>checkForSmsPermission</h>
     * <p>
     * check the permission at run time
     * </p>
     */
   /* public void checkForSmsPermission()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_SMS);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_RECEIVE_SMS);
            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_SMS_PERMISSION)) {
                generatePdialog.show();
                setValuesToPojo();
            }
        } else {
            generatePdialog.show();
            setValuesToPojo();
        }
    }*/

    /**
     * <h1>alertDialogTORecallOrGo</h1>
     * <p>
     * this shows alert to progress or to stay to check the permission
     *
     * @see AlertProgress
     * </p>
     */
    /*private void alertDialogTORecallOrGo()
    {
        String title = getResources().getString(R.string.smsPerminssionDenid); String positive = getResources().getString(R.string.yes);String negatve = getResources().getString(R.string.no);
        String message = getResources().getString(R.string.smsPerminssion);
        alertProgresss.alertonclick(title, message, positive, negatve, new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                if (isClicked) {

                    generatePdialog.show();
                    setValuesToPojo();
                }
                else
                {
                    setValuesToPojo();
                }
            }
        });
    }*/

    @Override
    public void onOTPSuccess(String onOTPSuccss) {
        pDialog.dismiss();
    }

    @Override
    public void otpResponce(String msg, Bundle data)
    {
        Intent inent = new Intent(this,ConfirmOTP.class);
        inent.putExtras(data);
        startActivity(inent);


    }

    @Override
    public void onOTPError(String errorMsg) {
        if (generatePdialog != null)
            generatePdialog.dismiss();
        alertProgresss.alertinfo(errorMsg);
    }

    @Override
    public void onValidationError(int errAt) {

        if (generatePdialog != null) {
            generatePdialog.dismiss();
        }

        switch (errAt)
        {
            case 1:
                focusVieWRequestFocus(til_reg_fnm,getString(R.string.first_name_mandatory));
                break;
            case 2:
                focusVieWRequestFocus(til_reg_email,getString(R.string.email_mandatory));
                break;
            case 3:
                focusVieWRequestFocus(til_reg_paswrd,getString(R.string.password_mandatory));
                break;
            case 4:
                focusviewMobile(et_reg_mobno, getString(R.string.mobile_invalid));
                break;
            case 5:
                focusVieWRequestFocus(til_reg_dateofBirth,getString(R.string.dob_mandatory));
                break;
        }
    }

    @Override
    public void onEmailValidFromAPi() {
        errorFocusNull(til_reg_email);
        focusView = null;
        til_reg_email.setErrorEnabled(false);
        til_reg_email.setError("");
    }

    @Override
    public void onEmailValidate(String mailError) {

        if(generatePdialog!=null && generatePdialog.isShowing())
        {
            generatePdialog.dismiss();
        }
        pDialog.dismiss();
        alertProgresss.alertinfo(mailError);
        focusVieWRequestFocus(til_reg_email,mailError);

    }

    @Override
    public void onEmailLocalValidity(String mailError) {
        til_reg_email.setErrorEnabled(true);
        til_reg_email.setError(mailError);
        focusView = et_reg_eml;
    }

    @Override
    public void onPhoneValidate(String phonError)
    {
        if(generatePdialog!=null && generatePdialog.isShowing())
        {
            generatePdialog.dismiss();
        }
        pDialog.dismiss();
        alertProgresss.alertinfo(phonError);
        focusviewMobile(et_reg_mobno, phonError);
    }

    @Override
    public void onPhoneLocalValidity(String phonError) {

        et_reg_mobno.setError(phonError);
        focusView = et_reg_mobno;
    }

    @Override
    public void onNameValidity(String nameError) {

        til_reg_fnm.setError(getString(R.string.first_name_mandatory));
        til_reg_fnm.setErrorEnabled(true);
        focusView = et_reg_fnm;
    }

    @Override
    public void onEmailLocalSuccess(String email) {

        focusviewNull();
        pDialog = alertProgresss.getProgressDialog(getResources().getString(R.string.mail_validating));
        pDialog.show();
        sinupcontrolr.checkEmail(et_reg_eml.getText().toString().trim(), true);
    }

    @Override
    public void onMobLocalSuccess(String email) {
        focusviewNull();
        pDialog = alertProgresss.getProgressDialog(getResources().getString(R.string.mob_validating));
        pDialog.show();
        sinupcontrolr.checkMobilNumbr(et_reg_mobno.getText().toString().trim(), tvCountryCode.getText().toString(), true);
    }

    @Override
    public void onPasswordLocalValidity(String password) {

        if (generatePdialog != null && generatePdialog.isShowing()) {
            generatePdialog.dismiss();
        }
        til_reg_paswrd.setErrorEnabled(true);
        til_reg_paswrd.setError(password);
        //et_reg_paswrd.setText("");
        focusView = til_reg_paswrd;
    }

    @Override
    public void onDateOBirthLocalValidity(String email) {

        til_reg_dateofBirth.setErrorEnabled(true);
        til_reg_dateofBirth.setError(email);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_reg_fnm:
                if(!hasFocus) {
                    if (sinupcontrolr.checkNameValidation(et_reg_fnm.getText().toString().trim()))
                        focusviewNull();
                    else
                        focusVieW(til_reg_fnm);
                }else
                    errorFocusNull(til_reg_fnm);

                break;
            case R.id.et_reg_eml:
                if(!hasFocus) {
                    Log.e(TAG, "onFocusChange: EMAIL VALIDATION");
                    if (sinupcontrolr.checkEmailValidation(et_reg_eml.getText().toString().trim())) {
                        focusviewNull();
                        pDialog = alertProgresss.getProgressDialog(getResources().getString(R.string.mail_validating));
                        pDialog.show();
                        sinupcontrolr.checkEmail(et_reg_eml.getText().toString().trim(), false);
                    } else
                        focusVieW(til_reg_email);
                }else {
                    errorFocusNull(til_reg_email);
                    focusView = null;
                    til_reg_email.setErrorEnabled(false);
                    til_reg_email.setError("");
                }
                break;
            case R.id.et_reg_paswrd:
                if(!hasFocus) {
                    if (sinupcontrolr.checkPasswordValidation(et_reg_paswrd.getText().toString().trim()))
                        focusviewNull();
                    else
                        focusVieW(til_reg_paswrd);
                }else
                    errorFocusNull(til_reg_paswrd);
                break;
            case R.id.et_reg_mobno:
                if (!hasFocus) {
                    if (sinupcontrolr.checMobilevalidation(tvCountryCode.getText().toString().trim(),et_reg_mobno.getText().toString().trim())) {
                        focusviewNull();
                        pDialog = alertProgresss.getProgressDialog(getResources().getString(R.string.mob_validating));
                        pDialog.show();
                        sinupcontrolr.checkMobilNumbr(et_reg_mobno.getText().toString().trim(), tvCountryCode.getText().toString(), false);
                    } else
                        focusViewNumbr(et_reg_mobno);
                    // focusVieW(til_reg_MobNo);
                } else {
                    //Added Request Hint if SIM is present
                    TelephonyManager telephonyManager =
                            (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    if (telephonyManager!=null&&!(telephonyManager.getSimState()==TelephonyManager.SIM_STATE_ABSENT)) {
                        requestHint();
                    }

                    errorFocusNullNum(et_reg_mobno);
                }
                //  errorFocusNull(til_reg_MobNo);
                break;
            case R.id.et_reg_dateofBirth:
                if(!hasFocus) {
                    if(et_reg_dateofBirth.getText().toString().trim().length()>0) {
                        focusviewNull();

                        if (sinupcontrolr.checkDobValidity()) {
                            et_reg_dateofBirth.setFocusable(false);
                        } else {
                            et_reg_dateofBirth.setFocusable(false);
                            et_reg_dateofBirth.setText("");
                            sinupcontrolr.openCalendr(et_reg_dateofBirth, til_reg_dateofBirth);
                        }
                    } else
                        focusVieW(til_reg_dateofBirth);
                }else {
                    if (et_reg_dateofBirth.getText().toString().trim().length() > 0) {
                        if (sinupcontrolr.checkDobValidity()) {
                            errorFocusNull(til_reg_dateofBirth);
                            et_reg_dateofBirth.setFocusable(false);
                            et_reg_referral.requestFocus();
                        } else {
                            et_reg_dateofBirth.setText("");
                        }

                    } else {
                        sinupcontrolr.openCalendr(et_reg_dateofBirth, til_reg_dateofBirth);
                        et_reg_referral.requestFocus();
                    }
                }
                break;
        }
    }

    /**
     * <h1>errorFocusNullNum</h1>
     * <p>
     * this helps to set the error value to set null
     *
     * @param et_reg_mobno et_reg_mobno EditText mobile number
     *                     </p>
     */

    private void errorFocusNullNum(EditText et_reg_mobno) {
        et_reg_mobno.setError("");
        et_reg_mobno.setError(null);
        focusView = null;
    }

    /**
     * <h1>focusviewMobile</h1>
     * <p>
     * this shows the error on the mobile number field
     *
     * @param et_reg_mobno editText Registered mobile number
     * @param error        error for the mobile number
     *                     </p>
     */
    private void focusviewMobile(EditText et_reg_mobno, String error) {
        focusView = et_reg_mobno;
        focusView.requestFocus();
        et_reg_mobno.setError(error);
    }

    /**
     * <h1>focusViewNumbr</h1>
     * set focus view of the mobile number
     *
     * @param et_reg_mobno EditText of the mobile number
     */
    private void focusViewNumbr(EditText et_reg_mobno) {
        focusView = et_reg_mobno;
    }

    /**
     * this is to null the focusview
     * @param til_reg_fnm is a TextInputLayout
     */
    private void errorFocusNull(TextInputLayout til_reg_fnm) {

        til_reg_fnm.setErrorEnabled(false);
        til_reg_fnm.setError("");
        focusView = null;
    }

    /**
     * this is to request the focus view
     * @param til_reg_fnm is a TextInputLayout
     * @param error Error Message
     */
    private void focusVieWRequestFocus(TextInputLayout til_reg_fnm,String error)
    {
        focusView = til_reg_fnm;
        focusView.requestFocus();
        til_reg_fnm.setErrorEnabled(true);
        til_reg_fnm.setError(error);
    }

    /**
     *<h1>focusVieW</h1>
     * this set the focus to the number
     * @param til_reg_lyout is a TextInputLayout
     */
    private void focusVieW(TextInputLayout til_reg_lyout) {
        focusView = til_reg_lyout;
    }

    /**
     * <h1>focusviewNull</h1>
     * set the focus view to null
     */
    private void focusviewNull() {
        focusView = null;
    }

    /**
     * <h1>setListener</h1>
     * <p>
     *     this method sets the  code number and the flag of the country
     * </p>
     */
    private void setListener()
    {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,
                                                  int flagDrawableResID,int maxDigit) {
                Log.d(TAG, "onSelectCountry: "+maxDigit);
                Log.d(TAG, "flag: "+code);
                tvCountryCode.setText(dialCode);
                ivCountryFlag.setImageResource(flagDrawableResID);
                countrySymbol=code;
                //et_reg_mobno.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxDigit)});
                mCountryPicker.dismiss();
            }
        });
        tvCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker));
            }
        });

        Country country = mCountryPicker.getUserCountryInfo(this);
        if(country!=null) {
            Log.d(TAG, "flag: "+country.getCode());
            ivCountryFlag.setImageResource(country.getFlag());
            tvCountryCode.setText(country.getDialCode());
            countrySymbol=country.getCode();
            int et_reg_mob = country.getMax_digits();
            //et_reg_mobno.setFilters(new InputFilter[]{new InputFilter.LengthFilter(et_reg_mob)});
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (generatePdialog != null) {
            generatePdialog.dismiss();
            generatePdialog.cancel();
        }
    }

    @Override
    public void onReferralValidate(String msg) {
        til_reg_referral.setErrorEnabled(false);
        focusviewNull();
        if(pDialog!=null && pDialog.isShowing())
        {
            pDialog.dismiss();
        }
        isReferralValid=false;
        Toast.makeText(this,"Referral code added successfully !",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReferralError(String error) {
        if(pDialog!=null && pDialog.isShowing())
        {
            pDialog.dismiss();
        }
        isReferralValid=true;
        focusVieWRequestFocus(til_reg_referral,error);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(charSequence.length()>0){
            focusView=et_reg_referral;
        }else{
            errorFocusNull(til_reg_referral);
        }
        if(charSequence.length()==5){
            focusviewNull();
            pDialog = alertProgresss.getProgressDialog("checking referral code");
            pDialog.show();
            sinupcontrolr.checkreferralcode(charSequence.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
    // Added to login after email validation
    @Override
    public void onSuccessEmailValidate(String headerResponse, String result) {
        Log.d(TAG, "onSuccessEmailValidate: result:"+result+" headerResponse:"+headerResponse);
        setValueTOEditTxt(fName, lName, email, facebookId, birthday);
    }

    @Override
    public void onFailureEmailValidate(String headerError, String server_issue) {
        if(headerError.equals("410") | headerError.equals("412")){
            LoginPojo loginPojo = new LoginPojo();
            SignUpData loginData = new SignUpData();
            loginPojo.setData(loginData);
            loginPojo.getData().setFbId(facebookId);
            loginPojo.getData().setPassword(facebookId);
            loginPojo.getData().setEmail(email);
            loginPojo.getData().setLoginType(2);
            loginPojo.getData().setDatOB(birthday);
            loginPojo.getData().setFirstName(fName);
            loginPojo.getData().setLastName(lName);
            setValueTOEditTxt(fName, lName, email, facebookId, birthday);
            sinupcontrolr.doLogin(loginPojo,this,sharedpref);
        }
    }

    @Override
    public void onLoginSuccess() {
        Log.d("ONActivity", "isMqttConnectedLogin: "+ ApplicationController.getInstance().isMqttConnected());
        VariableConstant.isChattingCalled = true;
        Intent intnet = new Intent(this, SplashActivity.class);
        intnet.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intnet);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        finish();
    }

    @Override
    public void onLoginFailure(String headerResponse) {

    }

    @Override
    public void toTermsLink() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }

    @Override
    public void toPrivacyPage() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Uploading the file to server
     */
    @SuppressLint("StaticFieldLeak")
    public class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(VariableConstant.CHAT_URL + "uploadImageOnServer")
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d("Shijen", "doInBackground: " + responseCode);
                Log.d("Shijen", "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            progressDialog.dismiss();
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");
                        sharedpref.setImageUrl(image);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }

    }
}