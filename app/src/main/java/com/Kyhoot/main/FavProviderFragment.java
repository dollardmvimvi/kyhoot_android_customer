package com.Kyhoot.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.FavoriteProviderGridViewAdapter;
import com.Kyhoot.controllers.FavFragmentController;
import com.Kyhoot.interfaceMgr.AccessTokenCallback;
import com.Kyhoot.interfaceMgr.FavoriteProviderCallback;
import com.Kyhoot.pojoResponce.FavProRespData;
import com.Kyhoot.pojoResponce.FavProviderRespPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.HorizontalCatItemDecorator;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavProviderFragment extends Fragment implements AccessTokenCallback,FavoriteProviderCallback,FavoriteProviderGridViewAdapter.FavProviderFragmentCallback {

    FavFragmentController controller;
    ArrayList<FavProRespData> favoriteProvidersList;
    FavoriteProviderGridViewAdapter favProviderAdapter;
    RecyclerView rv_grid_fav_provider;
    LinearLayout ll_progress;
    ConstraintLayout ll_no_favorites;
    SharedPrefs sharedPrefs;
    private int adapterPostion=-1;

    public FavProviderFragment() {
        // Required empty public constructor
        controller = new FavFragmentController(this,this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fav_provider, container, false);
        init(view);
        return view;
    }


    private void init(View view) {
        sharedPrefs = new SharedPrefs(getActivity());
        rv_grid_fav_provider = view.findViewById(R.id.rv_grid_fav_provider);
        favoriteProvidersList=new ArrayList<>();
        favProviderAdapter = new FavoriteProviderGridViewAdapter(favoriteProvidersList,this);
        HorizontalCatItemDecorator decorator=new HorizontalCatItemDecorator(20);
        TextView tv_no_fav=view.findViewById(R.id.tv_no_fav);
        tv_no_fav.setTypeface(AppTypeface.getInstance(getActivity()).getHind_regular());
        rv_grid_fav_provider.addItemDecoration(decorator);
        ll_progress=view.findViewById(R.id.ll_progress);
        ll_no_favorites=view.findViewById(R.id.cl_no_favorites);
        rv_grid_fav_provider.setLayoutManager(new GridLayoutManager(view.getContext(),2));
        rv_grid_fav_provider.setAdapter(favProviderAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getFavoriteProviderList();
    }

    private void getFavoriteProviderList() {
        showProgress();
        controller.getFavoriteProvider(sharedPrefs);
    }

    @Override
    public void onSessionExpired() {
        Utilities.setMAnagerWithBID(getActivity(),sharedPrefs,"");
    }

    @Override
    public void onSuccessAccessToken(String data) {
        hideProgress();
        sharedPrefs.setSession(data);
    }

    @Override
    public void onFailureAccessToken(String headerError, String server_issue) {
        hideProgress();
        Toast.makeText(getContext(),server_issue,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFavProvidersAvailable(String header, String result) {
        hideProgress();
        Gson gson=new Gson();
        try{
            FavProviderRespPojo favProviderRespPojo=gson.fromJson(result,FavProviderRespPojo.class);
            if(favProviderRespPojo.getData().size()>0){
                ll_no_favorites.setVisibility(View.GONE);
                favoriteProvidersList.clear();
                favoriteProvidersList.addAll(favProviderRespPojo.getData());
                favProviderAdapter.notifyDataSetChanged();
            }else{
                ll_no_favorites.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onFavProvidersError(String header, String result) {
        hideProgress();
        Toast.makeText(getContext(),result,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnfavoritingSuccessful(String header, String result) {
        hideProgress();
        if(adapterPostion>=0 && adapterPostion<favoriteProvidersList.size()){
            favoriteProvidersList.remove(adapterPostion);
            favProviderAdapter.notifyItemRemoved(adapterPostion);
        }
        if(favoriteProvidersList.size()==0){
            ll_no_favorites.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUnfavoritingError(String header, String result) {
        hideProgress();
        Toast.makeText(getActivity(),result,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnFavoriting(final String categoryId, final String providerId,int adapterPosition) {
        this.adapterPostion=adapterPosition;
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Unfavorite Provider");
        alertDialogBuilder.setMessage("Are you sure you want to unfavorite this tasker ?");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgress();
                controller.unFavoriteProvider(categoryId,providerId,sharedPrefs);
            }
        });
        alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        if(!getActivity().isFinishing()){
            alertDialog.show();
        }
    }
    public void showProgress(){
        if(ll_progress!=null)
        ll_progress.setVisibility(View.VISIBLE);
    }
    public void hideProgress(){
        if(ll_progress!=null)
        ll_progress.setVisibility(View.GONE);
    }

}
