package com.Kyhoot.interfaceMgr;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.Kyhoot.pojoResponce.CategoriesPojo;
import com.Kyhoot.pojoResponce.MqttProviderDetails;
import com.Kyhoot.pojoResponce.WalletData;

import java.util.ArrayList;

/**
 * <h>HomeModelIntrface</h>
 * Created by Ali on 10/4/2017.
 */

public interface HomeModelIntrface {
    void onAvailableMusic(ArrayList<MqttProviderDetails> mqttresp);

    void onAvailableOnlineProvider(ArrayList<String> proInfoReq, String image, LatLng latLng);

    void onAvailableOffLineProvider(ArrayList<String> proInfoReq, String image, LatLng latLng);

    void onUpdateMakerPosition(LatLng latLng, Marker promarker);

    void onUpdateMakerStatusOffline(ArrayList<String> proInfoReq, String image, LatLng latLng, Marker promarker);

    void onUpdateMakerStatusOnline(ArrayList<String> proInfoReq, String image, LatLng latLng, Marker promarker);

    void onUpdateMapToProvider(int position);

    void onCurrentLatLogitude(LatLng latLng);

    void onLocationSavedAddress(String address);

    void onClearMapLocation();

    void onClearList();

    void onError(String errorMsg);

    void onBookLatLng();

    void onSessionError(String sessionErrorMsg);

    void onScheduledDateTime(String dateTime);

    void onPendingBooking(long bid);

    void onHideProgressBar();

    void onServicesUpdated(String result);

    void updateWallet(WalletData wallet);

    void onSuccessProviderServices(String code,String response);

    void onFailureProviderServices(String code, String response);

    void onErrorCategories(String headerError, String error);

    void onSuccessGetCategories(CategoriesPojo categoriesPojo);

    void getEtaUpdate();

    void onUpdateProviders(String jsonObject);

    void setNoProviders();
}
