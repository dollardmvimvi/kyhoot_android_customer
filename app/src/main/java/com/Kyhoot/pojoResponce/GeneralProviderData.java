package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>GeneralProviderData</h>
 * Created by 3Embed on 10/17/2017.
 */

public class GeneralProviderData implements Serializable {
        /*"firstName":"mahendra",
"lastName":"vaghani",
"profilePic":"",
"email":"mahi2@gmail.cm",
"bannerImage":"",
"lastActive":1508149440,
"photo":"",
"noOfReview":1,
"rating":5,
"countryCode":"",
"mobile":"",
"about":"",
"instrument":"",
services*/

        /*"bookingId":168,
"rating":5,
"review":"hh",
"userId":"59dce722b121331d381cae63",
"reviewAt":1507650056,
"firstName":"Raghu",
"lastName":"",
"distance":""
"profilePic":"https://s3-us-west-2.amazonaws.com/dayrunner/Drivers/ProfilePics/1507561807166_0_01.png"*/

    private String firstName, lastName, profilePic, email, bannerImage, lastActive, photo,
            noOfReview, countryCode, mobile, about, rules, musicGenres, instrument,youtubeUrlLink,currencySymbol;
    private float rating;
    private float amount;
    private ArrayList<ProviderEvents> events;
    private ArrayList<ProviderServiceDtls> services;
    private ArrayList<RateReviews> review;
    private int distanceMatrix,totalBooking;
    private ArrayList<ProMetaData> metaDataArr;
    private double distance;


    public ArrayList<ProMetaData> getMetaDataArr() {
        return metaDataArr;
    }
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public int getTotalBooking() {
        return totalBooking;
    }

    public int getDistanceMatrix() {
        return distanceMatrix;
    }

    public double getDistance() {
        return distance;
    }

    public float getAmount() {
        return amount;
    }

    public String getMusicGenres() {
        return musicGenres;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getEmail() {
        return email;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public String getLastActive() {
        return lastActive;
    }

    public String getPhoto() {
        return photo;
    }

    public String getNoOfReview() {
        return noOfReview;
    }

    public float getRating() {
        return rating;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getMobile() {
        return mobile;
    }

    public String getAbout() {
        return about;
    }

    public String getRules() {
        return rules;
    }

    public String getInstrument() {
        return instrument;
    }

    public String getYoutubeUrlLink() {
        return youtubeUrlLink;
    }

    public ArrayList<ProviderEvents> getEvents() {
        return events;
    }

    public ArrayList<ProviderServiceDtls> getServices() {
        return services;
    }

    public ArrayList<RateReviews> getReview() {
        return review;
    }
}
