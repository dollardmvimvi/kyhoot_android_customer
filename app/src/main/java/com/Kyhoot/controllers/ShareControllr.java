package com.Kyhoot.controllers;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.VariableConstant;

import java.util.List;

/**
 * <h>ShareControllr</h>
 * Created by Ali on 9/18/2017.
 */

public class ShareControllr {
    private Context mcontext;
    private String text;
    private String referalCode, download, andsignup, andearndiscount;
    private AlertProgress aprogress;

    public ShareControllr(Context mcontext, AlertProgress aprogress) {
        this.mcontext = mcontext;
        this.aprogress = aprogress;
        download = mcontext.getResources().getString(R.string.Download);
        andsignup = mcontext.getResources().getString(R.string.andsignup);
        andearndiscount = mcontext.getResources().getString(R.string.andearndiscount);

    }

    public void performOnClikdAction(int clickTag, String referral) {
        referalCode = referral;

        text = download + " " + mcontext.getResources().getString(R.string.app_name) + " " + andsignup + " '" + referral + "' " + andearndiscount;
        String TAG = "ShareControllr";
        switch (clickTag) {
            case 1:

                Log.d(TAG, "performOnClikdActionFB: " + referral);
                facebookShare();
                break;
            case 2:
                Log.d(TAG, "performOnClikdActionEmil: ");
                emailShare();
                break;
            case 3:
                Log.d(TAG, "performOnClikdActionMesg: ");
                mesgShare();
                break;
            case 4:
                Log.d(TAG, "performOnClikdActionWhatsApp: ");
                whatsAppShare();
                break;
            default:
                Log.d(TAG, "performOnClikdActiontwittr: ");
                twitterShare();


        }
    }


    private void twitterShare() {
        try {
            text = mcontext.getResources().getString(R.string.shareTwitterInfo)+" "+referalCode+" "+
                    mcontext.getResources().getString(R.string.chearseFb);
            Uri uri = Uri
                    .parse("android.resource://com.livem.customer/drawable/ic_launcher");
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("image/jpeg");
            intent.setClassName("com.twitter.android", "com.twitter.android.PostActivity");
            boolean twitterAppFound = false;
            List<ResolveInfo> matches2 = mcontext.getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches2) {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                    intent.setPackage(info.activityInfo.packageName);
                    twitterAppFound = true;
                    break;

                }
            }
            if(twitterAppFound)
                mcontext.startActivity(intent);
            else
            {
                if (aprogress.isNetworkAvailable()) {
                    Uri uri1 = Uri.parse("market://details?id=" + "com.twitter.android");
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri1);
                    try {
                        mcontext.startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        mcontext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android&hl=en")));
                    }
                }
            }

        } catch (final ActivityNotFoundException e) {
            aprogress.alertinfo("You don't seem to have twitter installed on this device");
        }


    }


    private void whatsAppShare() {
        String urlBody = text = mcontext.getResources().getString(R.string.shareWhatsAppInfo)+"\n"+referalCode+" "+
                mcontext.getResources().getString(R.string.chearseMsg);

        Intent intent2 = new Intent(Intent.ACTION_SEND);
        intent2.setType("text/plain");
        intent2.putExtra(Intent.EXTRA_TEXT, urlBody);
        boolean whatsappAppFound = false;
        List<ResolveInfo> matches2 = mcontext.getPackageManager().queryIntentActivities(intent2, 0);
        for (ResolveInfo info : matches2) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.whatsapp")) {
                intent2.setPackage(info.activityInfo.packageName);
                whatsappAppFound = true;
                break;

            }
        }

        if (whatsappAppFound) {
            mcontext.startActivity(intent2);
        } else {


            if (aprogress.isNetworkAvailable()) {
                Uri uri = Uri.parse("market://details?id=" + "com.whatsapp");
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    mcontext.startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    mcontext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp&hl=en")));
                }
            } else {
                aprogress.showNetworkAlert();
            }
        }
    }

    private void mesgShare() {
        String smsBody = text = mcontext.getResources().getString(R.string.shareMessageInfo)+"\n"+referalCode+" "+
                mcontext.getResources().getString(R.string.chearseMsg)+download+" "+ VariableConstant.PLAY_STORE_LINK;

        Intent sms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"));
        sms.putExtra("sms_body", smsBody);
        mcontext.startActivity(sms);
    }

    private void emailShare() {
        String emailBody = text = mcontext.getResources().getString(R.string.shareEmailInfo)+"\n"+referalCode+" "+
                mcontext.getResources().getString(R.string.chearseMsg)+download+" "+ VariableConstant.PLAY_STORE_LINK;

        Intent email = new Intent(Intent.ACTION_SENDTO);
        email.putExtra(Intent.EXTRA_SUBJECT, mcontext.getResources().getString(R.string.registeron) + " " + mcontext.getResources().getString(R.string.app_name));
        email.putExtra(Intent.EXTRA_TEXT, emailBody);
        email.setType("text/plain");
        email.setType("message/rfc822");
        email.setData(Uri.parse("mailto:" + " "));
        mcontext.startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }


    private void facebookShare() {
        text = mcontext.getResources().getString(R.string.shareFacebookInfo) +" "+referalCode+" "+
                mcontext.getResources().getString(R.string.chearseFb)+download+" "+ VariableConstant.PLAY_STORE_LINK;
        String urlToShare = "https://www.facebook.com/livem.today/";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = mcontext.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

        if (facebookAppFound) {
            mcontext.startActivity(intent);
        } else {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello," + "\n" + "  " + text
            );
            mcontext.startActivity(Intent.createChooser(shareIntent, referalCode));

        }
    }
}
