package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by ${3Embed} on 2/2/18.
 */

public class Item implements Serializable{
        private double amount;

        private String unit;

        private String serviceId;

        private String status;

        private String addedToCartOn;

        private String additionalPrice;

        private String unitPrice;

        private int maxquantity;

        private String plusOneCost;

        private String quantityAction;


        private int quntity;

        private String serviceName;

        public double getAmount ()
        {
            return amount;
        }

        public String getStatus() {
                return status;
        }

        public String getUnit ()
        {
            return unit;
        }

        public String getServiceId ()
        {
            return serviceId;
        }

        public int getQuntity ()
        {
            return quntity;
        }

        public String getServiceName ()
        {
            return serviceName;
        }

        public String getAdditionalPrice() {
                return additionalPrice;
        }

        public String getUnitPrice() {
                return unitPrice;
        }

        public int getMaxquantity() {
                return maxquantity;
        }

        public String getPlusOneCost() {
                return plusOneCost;
        }

        public void setQuntity(int quntity) {
                this.quntity = quntity;
        }

        public String getQuantityAction() {
                return quantityAction;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [amount = "+amount+", unit = "+unit+", serviceId = "+serviceId+", status = "+status+", addedToCartOn = "+addedToCartOn+", additionalPrice = "+additionalPrice+", unitPrice = "+unitPrice+", plusOneCost = "+plusOneCost+", quntity = "+quntity+""+"]";
        }
}
