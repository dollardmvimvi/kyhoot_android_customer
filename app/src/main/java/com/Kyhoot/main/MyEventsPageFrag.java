package com.Kyhoot.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.adapter.MyEventsPageAdapter;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;


/**
 * <h>MyEventsPageFrag</h>
 * Created by 3Embed on 11/1/2017.
 */

public class MyEventsPageFrag extends Fragment {
    private static final String TAG = "MyEVENTPAGEFRAG";
   // private static final String PENDINGALL_JOBS = "pendingjob";
    private static final String PAGE_COUNT= "pagecount";
    private MyEventsPageAdapter adapterAssign;
    private Context mcontext;
    private RelativeLayout rlMyEventNoBokinScheduled;
    private ArrayList<AllEventsDataPojo> eventsAL = new ArrayList<>();
    private RecyclerView recyclerMyEvents;
    private TextView tvMyEventNoBooking;
    private int pageCount = 0;
//ArrayList<AllEventsDataPojo> bookingEvents,

    private HorizontalCalendar horizontalCalendar;
    HorizontalCalendarView calendarView;

    private String currentTag="";

    long currentDateEpoch,selectedDateEpoch;
    private CalendarView fullMonthCalendarView;
    private boolean isFabClicked = true;

    private MyEventsFrag myEnventFrag;


    LinearLayout llMyBookingPageFarg;

    FloatingActionButton fab;

    ArrayList<CalendarEvent> events = new ArrayList<>();
    Calendar startDate,endDate,defaultSelectedDate;
    long fromDate,toDate;

    private OnFragmentInteractionListener mListener;
    private boolean isFirstTime = true;



    public static MyEventsPageFrag newInstancePen(int pageValue) {
        Bundle args = new Bundle();
        //args.putSerializable(PENDINGALL_JOBS, bookingEvents);
        args.putInt(PAGE_COUNT,pageValue);
        MyEventsPageFrag fragment = new MyEventsPageFrag();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        assert bundle != null;
        pageCount = bundle.getInt(PAGE_COUNT,0);
        mcontext = getContext();
        onCreateSetDate();
    }

    private void onCreateSetDate()
    {
        startDate = Calendar.getInstance();
        startDate.setTimeZone(Utilities.getTimeZoneFromLocation());
        startDate.add(Calendar.MONTH, -2);

        endDate = Calendar.getInstance();
        endDate.setTimeZone(Utilities.getTimeZoneFromLocation());
        endDate.add(Calendar.MONTH, 2);

        defaultSelectedDate = Calendar.getInstance();
        defaultSelectedDate.setTimeZone(Utilities.getTimeZoneFromLocation());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.myevents_page, container, false);
        initializeView(view);
        return view;
    }

    private void initializeView(View view) {


        AppTypeface typeface = AppTypeface.getInstance(mcontext);

        fab = view.findViewById(R.id.fab);

        calendarView = view.findViewById(R.id.calendarView);
        llMyBookingPageFarg = view.findViewById(R.id.llMyBookingPageFarg);

        fullMonthCalendarView = view.findViewById(R.id.fullMonthCalendarView);
        fullMonthCalendarView.setVisibility(View.GONE);

        if (fullMonthCalendarView != null) {

            fullMonthCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange( CalendarView view, int year, int month, int dayOfMonth) {

                    int months = month+1;
                    int day = dayOfMonth-1;
                    Long value =  view.getDate();
                    Log.i("value","value --> "+value.toString());

                    String formate = year+"-"+months+"-"+dayOfMonth+"_00-00-00_000";

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss_SSS");

                    try {
                        Date gmt = formatter.parse(formate);
                        long startDate = gmt.getTime();
                        Log.i("formate","formate --> "+startDate);

                        long endDate = startDate + (24 * 60 * 60 * 1000)-1000;
                        Log.i("formate","formate --> "+endDate);

                        fullMonthCalendarView.setVisibility(View.GONE);
                        calendarView.setVisibility(View.VISIBLE);



                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss_SSS");
                        Date date = sdf. parse(formate);
                        DateAndCalendar obj = new DateAndCalendar();
                        Calendar cal = obj.dateToCalendar(date);
                        horizontalCalendar.selectDate(cal,false);




                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }


        String dateStr = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        // String dateStr = "2019-06-25";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss_SSS");
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dateObj = curFormater.parse(dateStr);
            SimpleDateFormat postFormater = new SimpleDateFormat("yyyy-MM-dd");
            String newDateStr = postFormater.format(dateObj);
            String formate = newDateStr+"_00-00-00_000";

            Date gmt = formatter.parse(formate);
            currentDateEpoch = gmt.getTime();
            Log.i("formate","formate --> "+currentDateEpoch);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvMyEventNoBooking = view.findViewById(R.id.tvMyEventNoBooking);
        TextView tvMyEventBook = view.findViewById(R.id.tvMyEventBook);
        rlMyEventNoBokinScheduled = view.findViewById(R.id.rlMyEventNoBokinScheduled);
        LinearLayoutManager llManager = new LinearLayoutManager(mcontext);
        recyclerMyEvents = view.findViewById(R.id.recyclerMyEvents);
        int resId = R.anim.layoutanimation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(mcontext, resId);
        recyclerMyEvents.setLayoutAnimation(animation);
        recyclerMyEvents.setLayoutManager(llManager);
        adapterAssign = new MyEventsPageAdapter(mcontext, eventsAL,pageCount);
        recyclerMyEvents.setAdapter(adapterAssign);
        tvMyEventNoBooking.setTypeface(typeface.getHind_regular());
        tvMyEventBook.setTypeface(typeface.getHind_semiBold());
        tvMyEventBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(mcontext, MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                ((MenuActivity)getActivity()).popBackstack();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);


              //  int year =  2019;
                int month =  horizontalCalendar.getSelectedDate().getTime().getMonth()+1;
                int date =  horizontalCalendar.getSelectedDate().getTime().getDate();

                String formate = year+"-"+month+"-"+date+"_00-00-00_000";
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss_SSS");

                try {
                    Date  gmt = formatter.parse(formate);
                    selectedDateEpoch = gmt.getTime();
                    Log.i("formate","formate --> "+selectedDateEpoch);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                if(currentDateEpoch == selectedDateEpoch)
                {
                    calendarView.setVisibility(View.GONE);
                    fullMonthCalendarView.setVisibility(View.VISIBLE);
                }else{
                    horizontalCalendar.goToday(false);
                    calendarView.setVisibility(View.VISIBLE);
                    fullMonthCalendarView.setVisibility(View.GONE);
                }

            }
        });

        textValueToBeSet();

    }
    public void notifyDataAdapter(ArrayList<AllEventsDataPojo> eventData,int bookingType,boolean isFirstOrItemAdded) {
        eventsAL.clear();
        eventsAL.addAll(eventData);
        if (eventsAL.size() > 0) {
            rlMyEventNoBokinScheduled.setVisibility(View.GONE);
            recyclerMyEvents.setVisibility(View.VISIBLE);
        } else {
            recyclerMyEvents.setVisibility(View.GONE);
            rlMyEventNoBokinScheduled.setVisibility(View.VISIBLE);
        }
        adapterAssign.notifyDataSetChanged();

        if(bookingType == 2)
        {
            if(isFirstTime)
            {
                isFirstTime = false;
                todayDate();
            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void textValueToBeSet()
    {
        if(pageCount==0)
        {
            tvMyEventNoBooking.setText(getString(R.string.youHaveNoPendingEvent));
            calendarView.setVisibility(View.GONE);
            fullMonthCalendarView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }else if(pageCount==1)
        {
            tvMyEventNoBooking.setText(getString(R.string.youHaveNoUpcomingEvent));
            fullMonthCalendarView.setVisibility(View.GONE);
            calendarView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            setCalendarValue();
        }else
        {
            tvMyEventNoBooking.setText(getString(R.string.youHaveNoPastEvent));
            calendarView.setVisibility(View.GONE);
            fullMonthCalendarView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
    }

    public void todayDate()
    {
        fromDate = getStartOfDayInMillisToday();
        toDate = getEndOfDayInMillisToday();
        myEnventFrag.callOnDateSelectApi(fromDate,toDate);

    }

    private void setCalendarValue()
    {

        /* start 2 months ago from now */


        horizontalCalendar = new HorizontalCalendar.Builder(llMyBookingPageFarg, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.parseColor("#3A3A3A"),Color.parseColor("#74257f"))
                .colorTextMiddle(Color.parseColor("#3A3A3A"), Color.parseColor("#74257f"))
                .end()
                .defaultSelectedDate(this.defaultSelectedDate)

                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.getDefault());
                // String selectedDateStr =  sdf.format(date);

                String selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                Toast.makeText(mcontext, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();

                //  defaultSelectedDate
                horizontalCalendar.post(new Runnable() {
                    @Override
                    public void run() {

                        fromDate = getStartOfDayInMillis(date);
                        toDate = getEndOfDayInMillis(date);

                        Log.i("formate","formate --> "+fromDate);
                        Log.i("formate","formate --> "+toDate);

                        myEnventFrag.callOnDateSelectApi(fromDate,toDate);
                    }
                });

            }

        });
    }

    public long getStartOfDayInMillis(Calendar date) {
        // Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));//
        Calendar calendar = Calendar.getInstance(Utilities.getTimeZoneFromLocation());//
        calendar.setTime(date.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
        //  if(isStartTime)
        return  calendar.getTimeInMillis();//+(24 * 60 * 60 * 1000);
       /* else
            return calendar.getTimeInMillis();*/
    }

    public long getEndOfDayInMillis(Calendar date) {
        return getStartOfDayInMillis(date) + (24 * 60 * 60 * 1000)-1000;
    }

    public long getStartOfDayInMillisToday() {
        // Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance(Utilities.getTimeZoneFromLocation());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }
    public long getEndOfDayInMillisToday() {
        // Add one day's time to the beginning of the day.
        // 24 hours * 60 minutes * 60 seconds * 1000 milliseconds = 1 day
        return getStartOfDayInMillisToday() + (24 * 60 * 60 * 1000)-1000;
    }

    public void addParentFrag(MyEventsFrag frag) {
        this.myEnventFrag=frag;

    }

    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onDateSelectedApi(long fromDate,long toDate,boolean ApiCalled);
    }

    private class DateAndCalendar
    {
        //Convert Date to Calendar
        private Calendar dateToCalendar(Date date) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;

        }

        //Convert Calendar to Date
        private Date calendarToDate(Calendar calendar) {
            return calendar.getTime();
        }
    }

    @Override
    public void onResume() {

        fullMonthCalendarView.setVisibility(View.GONE);
        super.onResume();
    }

    @Override
    public void onPause() {

        fullMonthCalendarView.setVisibility(View.GONE);
        super.onPause();
    }
}
