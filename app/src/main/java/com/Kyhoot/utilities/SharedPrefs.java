package com.Kyhoot.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * <h>SharedPrefs</h>
 * Created by user on 8/7/2017.
 */

public class SharedPrefs
{
    // Sharedpref file name
    private static final String PREF_NAME = "liveMPref";
    private static final String DEVICE_ID = "deviceid";

    // Shared Preferences
    private SharedPreferences prefs;
    // Editor for Shared preferences

    private SharedPreferences.Editor editor;
    private static SharedPrefs sharedPrefs;

    public SharedPrefs(Context mcontext)
    {
        int PRIVATE_MODE = 0;
        prefs = mcontext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
        editor.apply();
    }

    public static SharedPrefs getInstance(Context context){
        if(sharedPrefs==null){
            sharedPrefs=new SharedPrefs(context);
        }
        return sharedPrefs;
    }

    public void clearSession()
    {
        editor.clear();
        editor.commit();
    }

    public String getDeviceId() {
        return prefs.getString(DEVICE_ID, "");
    }

    /*--------------------------DeviceId----------------------*/
    public void setDeviceId(String deviceId)
    {
        editor.putString(DEVICE_ID, deviceId);
        editor.commit();
    }

    public String getCustomerSid() {
        return prefs.getString("CUSTOMERSID", "");
    }

    /*--------------------------CustomerId----------------------*/
    public void setCustomerSid(String customerSid) {
        editor.putString("CUSTOMERSID", customerSid);
        editor.commit();
    }

    /*--------------------------ProviderData----------------------*/
    public String getProviderData() {
        return prefs.getString("PROVIDERDATA", "");
    }

    public void setProviderData(String providerData) {
        editor.putString("PROVIDERDATA", providerData);
        editor.commit();
    }

    /*--------------------------CountrySymbol----------------------*/
    public String getCountrySymbol() {
        return prefs.getString("COUNTRY_SYMBOL", "");
    }

    public void setCountrySymbol(String countrySymbol) {
        editor.putString("COUNTRY_SYMBOL", countrySymbol);
        editor.commit();
    }

    /*--------------------------CancellationAfterXMin----------------------*/
    public String getCancellationAfterXMin() {
        return prefs.getString("CANCEL_AFTER", "");
    }

    public void setCancellationAfterXMin(String cancellationAfterXMin) {
        editor.putString("CANCEL_AFTER", cancellationAfterXMin);
        editor.commit();
    }

    public String getRegistrationId() {
        return prefs.getString("getRegistrationId","");
    }

    /*--------------------------Location----------------------*/
    public void setRegistrationId(String RegistrationId) {
        editor.putString("getRegistrationId", RegistrationId);
        editor.commit();
    }

    /*--------------------------Location----------------------*/
    public void setIsLogin(boolean value){
        editor.putBoolean("is_LOGIN",value);
        editor.commit();
    }
    public boolean isLogin(){
        return prefs.getBoolean("is_LOGIN",false);
    }

    /*--------------------------SplashLatitude----------------------*/
    public String getSplashLatitude() {
        return prefs.getString("Splash_Latitude", "0");
    }

    public void setSplashLatitude(String splashLatitude){
        editor.putString("Splash_Latitude", splashLatitude);
        editor.commit();
    }
    /*--------------------------SplashLongitude----------------------*/
    public String getSplashLongitude() {
        return prefs.getString("Splash_Longitude", "0");
    }

    public void setSplashLongitude(String splashLongitude){
        editor.putString("Splash_Longitude", splashLongitude);
        editor.commit();
    }

    public String getBookLatitude() {
        return prefs.getString("book_Latitude", "");
    }

    /*--------------------------SplashLatitude----------------------*/
    public void setBookLatitude(String bookLatitude) {
        editor.putString("book_Latitude", bookLatitude);
        editor.commit();
    }

    public String getBookLongitude() {
        return prefs.getString("book_Longitude", "");
    }

    /*--------------------------SplashLongitude----------------------*/
    public void setBookLongitude(String bookLongitude) {
        editor.putString("book_Longitude", bookLongitude);
        editor.commit();
    }

    public String getSession() {
        return prefs.getString("got_Session","");

    }

    /*--------------------------Session----------------------*/
    public void setSession(String got_Session) {
        editor.putString("got_Session", got_Session);
        editor.commit();
    }

    public String getRequesterId() {
        return prefs.getString("requesterId","");

    }

    /*--------------------------Session----------------------*/
    public void setRequesterId(String requesterId) {
        editor.putString("requesterId", requesterId);
        editor.commit();
    }

    /*--------------------------UserName----------------------*/
    public String getFirstname() {
        return prefs.getString("getFirstname", "");
    }

    public void setFirstname(String firstname) {
        editor.putString("getFirstname", firstname);
        editor.commit();
    }

    public String getLastname() {
        return prefs.getString("getLastname", "");
    }

    public void setLastname(String lastname) {
        editor.putString("getLastname", lastname);
        editor.commit();
    }


    public String getUserId(){
        return prefs.getString("UserId", "");
    }
    public void setUserId(String UserId){
        editor.putString("UserId", UserId);
        editor.commit();
    }
    /*--------------------------Password----------------------*/
    public String getPassword(){
        return prefs.getString("Password", "");
    }
    public void setPassword(String Password){
        editor.putString("Password", Password);
        editor.commit();
    }
    /*--------------------------MobileNumber----------------------*/
    public String getMobileNo(){
        return prefs.getString("MobileNo", "");
    }
    public void setMobileNo(String MobileNo){
        editor.putString("MobileNo", MobileNo);
        editor.commit();
    }
    /*--------------------------Email----------------------*/
    public String getEMail(){
        return prefs.getString("EMail", "");
    }

    public void setEMail(String EMail){
        editor.putString("EMail", EMail);
        editor.commit();
    }
    /*--------------------------ImageURl----------------------*/
    public String getImageUrl(){
        return prefs.getString("getImageUrl","");
    }
    public void setImageUrl(String got_ImageUrl){
        editor.putString("getImageUrl",got_ImageUrl);
        editor.commit();
    }

    /*--------------------------SavePassword----------------------*/
    public String getOldpassword() {
        return prefs.getString("Oldpassword", "");
    }

    public void setOldpassword(String oldpassword) {
        editor.putString("Oldpassword", oldpassword);
        editor.commit();
    }

    /*--------------------------SavePassword----------------------*/
    public String getFcmTopic() {
        return prefs.getString("FCMTopic", "");
    }

    public void setFcmTopic(String fcmTopic) {
        editor.putString("FCMTopic", fcmTopic);
        editor.commit();
    }

    public String getFcmTopicCity() {
        return prefs.getString("FCMTopicCity", "");
    }

    public void setFcmTopicCity(String fcmTopicCity) {
        editor.putString("FCMTopicCity", fcmTopicCity);
        editor.commit();
    }

    public String getFcmTopicAllCustomer() {
        return prefs.getString("FCMTopicAllCustomer", "");
    }

    public void setFcmTopicAllCustomer(String fcmTopicAllCustomer) {
        editor.putString("FCMTopicAllCustomer", fcmTopicAllCustomer);
        editor.commit();
    }


    public String getFcmTopicAllCityCustomer() {
        return prefs.getString("FCMTopicAllCityCustomer", "");
    }

    public void setFcmTopicAllCityCustomer(String fcmTopicAllCityCustomer) {
        editor.putString("FCMTopicAllCityCustomer", fcmTopicAllCityCustomer);
        editor.commit();
    }

    public String getFcmTopicAllOutZoneCustomers() {
        return prefs.getString("FCMTopicAllOutZoneCustomers", "");
    }

    public void setFcmTopicAllOutZoneCustomers(String fcmTopicAllOutZoneCustomers) {
        editor.putString("FCMTopicAllOutZoneCustomers", fcmTopicAllOutZoneCustomers);
        editor.commit();
    }

    /*--------------------------profile----------------------*/
    public void setIsProfile(boolean isProfile){
        editor.putBoolean("isProfile", isProfile);
        editor.commit();
    }

    public boolean IsProfile() {
        return prefs.getBoolean("isProfile",true);

    }
     /*--------------------------SplashLongitude----------------------*/
     public void setCurrencySymbl(String currencySymbl)
     {
         editor.putString("currencySymBol",currencySymbl);
         editor.commit();
     }
     public String getCurrencySymbol()
     {
         return prefs.getString("currencySymBol","");
     }

    public String getPaymentId() {
        return prefs.getString("PayemntID", "");
    }

    /*--------------------------SplashLongitude----------------------*/
    public void setPaymentId(String paymentId)
    {
        editor.putString("PayemntID",paymentId);
        editor.commit();
    }

    /*-----------------------Date of Birth----------------------------*/

    public String getDateOBirth()
    {
        return prefs.getString("DateOfBirth", "");
    }

    public void setDateOBirth(String dateOBirth) {
        editor.putString("DateOfBirth", dateOBirth);
        editor.commit();
    }

     /*-----------------------Default Card Num----------------------------*/

    public String getDefaultCardNum()
    {
        return prefs.getString("defaultCardNum", "");
    }

    public void setDefaultCardNum(String defaultCardNum) {
        editor.putString("defaultCardNum", defaultCardNum);
        editor.commit();
    }

     /*-----------------------Default Card Id----------------------------*/

    public String getDefaultCardId()
    {
        return prefs.getString("defaultCardId", "");
    }

    public void setDefaultCardId(String defaultCardNum) {
        editor.putString("defaultCardId", defaultCardNum);
        editor.commit();
    }

    /*-----------------------Paymentto show----------------------------*/

    public String getPaymentToShow()
    {

        return prefs.getString("paymentMethodtoShow", "");
    }

    public void setPaymentToShow(String paymentMethodtoShow) {
        editor.putString("paymentMethodtoShow", paymentMethodtoShow);
        editor.commit();
    }

     /*-----------------------About Me----------------------------*/

    public String getAboutMe() {
        return prefs.getString("Aboutmee", "");
    }

    public void setAboutMe(String aboutMe) {
        editor.putString("Aboutmee", aboutMe);
        editor.commit();
    }
    /*-----------------------Music Genre----------------------------*/

    public String getMusicGen() {
        return prefs.getString("Musicgene", "");
    }

    public void setMusicGen(String musicGen) {
        editor.putString("Musicgene", musicGen);
        editor.commit();
    }

     /*-----------------------Referral Code----------------------------*/

    public String getReferral() {
        return prefs.getString("Referral", "");
    }

    public void setReferral(String referral) {
        editor.putString("Referral", referral);
        editor.commit();
    }

    /************************country code**********************/
    public String getCountryCode() {
        return prefs.getString("CountryCod", "");
    }

    public void setCountryCode(String countryCode) {
        editor.putString("CountryCod", countryCode);
        editor.commit();
    }

     /*-----------------------Booking Address----------------------------*/

    public String getBookingAddress() {
        return prefs.getString("BookingAdd", "");
    }

    public void setBookingAddress(String bookingAddress) {
        editor.putString("BookingAdd", bookingAddress);
        editor.commit();
    }


    public void setCategoriesList(String result) {
        editor.putString("CategoriesList", result);
        editor.commit();
    }
    public String getCategoriesList() {
        return prefs.getString("CategoriesList", "");
    }

    public void setDefaultCardBrand(String defaultCardBrand) {
        editor.putString("DefaultCardBrand", defaultCardBrand);
        editor.commit();
    }
    public String getDefaultCardBrand() {
        return prefs.getString("DefaultCardBrand", "");
    }

    public void setCategoryId(String categoryId) {
        editor.putString("CATEGORYID", categoryId);
        editor.commit();
    }
    public String getCategoryId(){
        return prefs.getString("CATEGORYID", "");
    }
    public void setBookingStatus(String bid,int status) {
        editor.putInt(bid, status);
        editor.commit();
    }
    public int getBookingStatus(long bid){
        return prefs.getInt(""+bid, -1);
    }
    public void setCurrencyPosition(String currencyAbbrText) {
        editor.putString("CURRENCYABBRTEXT",currencyAbbrText);
        editor.commit();
    }


    public void setBillingModel(String billingModel) {
        editor.putString("BILLINGMODEL", billingModel);
        editor.commit();
    }
    public String getBillingModel(){
        return prefs.getString("BILLINGMODEL", "");
    }
    public void setCategoryHourlyPrice(float categoryHourlyPrice) {
        editor.putFloat("CategoryHourlyPrice", categoryHourlyPrice);
        editor.commit();
    }
    public float getCategoryHourlyPrice(){
        return prefs.getFloat("CategoryHourlyPrice", 0);
    }
    public void setSelectedCategoryName(String SelectedCategoryName) {
        editor.putString("SelectedCategoryName", SelectedCategoryName);
        editor.commit();
    }
    public String  getSelectedCategoryName(){
        return prefs.getString("SelectedCategoryName", "");
    }
    public void setSelectedServiceType(String serviceType) {
        editor.putString("SelectedServiceType", serviceType);
        editor.commit();
    }
    public String  getSelectedServiceType(){
        return prefs.getString("SelectedServiceType", "");
    }

    public String getCurrencyPosition(long bid){
        return prefs.getString("CURRENCYABBRTEXT", "Prefix");
    }
    public void setServerTimeDifference(long serverTimeDifference) {
        editor.putLong("SERVERTIMEDIFFERENCE",serverTimeDifference);
        editor.commit();
    }
    public long getServerTimeDifference(){
        return prefs.getLong("SERVERTIMEDIFFERENCE", 0);
    }
    public void setAppVersion(String appVersion) {
        editor.putString("APPVERSION",appVersion);
        editor.commit();
    }
    public long getAppVersion(){
        return prefs.getLong("APPVERSION", 1);
    }

    public void setScheduleData(String data)
    {
        editor.putString("ScheduleData",data);
        editor.commit();
    }
}
