package com.Kyhoot.interfaceMgr;

import android.os.Bundle;

/**
 * Created by ${3Embed} on 15/6/18.
 */
public interface OnFavoriteSelected {
    void onFavoriteSelected(Bundle bundle);
    void onOthersSelected();
}
