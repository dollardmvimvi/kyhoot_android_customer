package com.Kyhoot.main;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.twiliocallmodule.ClientActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.Kyhoot.R;
import com.Kyhoot.controllers.LiveBookingStatusController;
import com.Kyhoot.interfaceMgr.LiveBookingStatusInt;
import com.Kyhoot.pojoResponce.AllEventsDataPojo;
import com.Kyhoot.pojoResponce.CartData;
import com.Kyhoot.pojoResponce.Item;
import com.Kyhoot.pojoResponce.LiveBookingStatusPojo;
import com.Kyhoot.pojoResponce.LiveTackPojo;
import com.Kyhoot.pojoResponce.LiveTrackObservable;
import com.Kyhoot.pojoResponce.MyEventStatus;
import com.Kyhoot.pojoResponce.MyEventStatusObservable;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.MqttEvents;
import com.Kyhoot.utilities.MyNetworkChangeListner;
import com.Kyhoot.utilities.NetworkChangeReceiver;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.os.Build.VERSION_CODES.M;

public class LiveStatus extends AppCompatActivity implements View.OnClickListener, LiveBookingStatusInt {

    public int selectedpostion = -1;
    Observer<LiveTackPojo> liveObserver;
    private AppTypeface apptypeFace;
    private TextView tv_skip;
    private TextView tvLiveReqTime;
    private TextView tvLiveAcptTime;
    private TextView tvLiveAssgnedTOInfo;
    private TextView tvLiveOnTheWayTime, tvLiveOnTheWayArtist;
    private ImageView ivLiveOnTheWayTime;
    private ImageView ivLiveProProfile,ivLiveProInfo;
    private TextView tvLiveArrivedTime, tvLiveArrivedArtist;
    private ImageView ivLiveArrivedTime;
    private TextView tvLiveEventStartedTime, tvLiveEventStartedArtist;
    private ImageView ivLiveEventStartedTime;
    private TextView tvLiveCompletedTime, tvLiveCompletedArtist;
    private ImageView ivLiveCompletedTime;
    private TextView tvLiveInvoiceTime, tvLiveInvoiceArtist;
    private ImageView ivLiveInvoiceTime;
    private TextView tvVisitFeeAmt, tvDiscountAmount, tvTotalAmt;
    private LinearLayout llLiveFee, llLiveStatus;
    private RatingBar rtConfirm;
    private TextView tvConfirmName,tvConfirmNumrOfRevw,tvViewProfile,tvCenter,jobTimer,tvYourGigTimeIs,
            tvLiveDistance,tvLiveETA,tvLiveDistanceKm,tvLiveETAMin,tvLiveReqArtist,tvLiveAcptArtist;
    private RelativeLayout rlStatusMap,rlconfm_travelfee,rlSelectedCategory,rldiscount;
    private SupportMapFragment mGoogleMap;
    private GoogleMap mMap;
    private long bId;
    private String phoneNo;
    private String proProfilePic = "";
    private double proLat = 0, proLng = 0;
    private double cusLat = 0, cusLng = 0;
    private int bookStatus = 0;
    private boolean isMapShowing = false;
    private SharedPrefs sharedPrefs;
    private AlertProgress aProgress;
    private ProgressBar progressLive;
    private LiveBookingStatusController liveBookingController;
    private String currencySymbol, proId = "",proName;
    private Runnable locationUpdateRunnable;
    private Handler locatingUpdateHandler;
    private Marker proMarker = null;
    private LatLng proLatLng = null;
    private boolean isRateProviderCalled = true;
    private double width,width1,hight,hight1;
    private Toolbar toolBarLiveStatus;
    private TextView tv_nrd_track;
    private RelativeLayout rlProContact,rlconfm_visitfee;
    private Observer<MyEventStatus> observer;
    private boolean isMapLoaded = false;
    private CompositeDisposable disposable,liveDispose;
    private TextView tv_travelFeeFee;
    private TextView tvTravelFeeAmt,tvLastDues,tvLastDuesAmt;
    private RelativeLayout rlTimerLayout,rlLastdues;
    private long gigSeconds=0;
    private View vacceptedbel,vOntheWay,vNotaryArrived,vNotaryStarted,vNotaryCompleted,vNotaryInvoice;
    Animation animation;
    private static final String TAG = "LiveStatus";

    int oldStatus = 0;
    private Timer timer;
    private long timeConsumedSecond;
    private TimerTask task;
    private LiveBookingStatusPojo.LiveBookingData.BookingTimer bookingTimer;
    private AllEventsDataPojo.Accounting accounting;

    private long cancelTimeStamp=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_slive_tracking);
        VariableConstant.isLiveBokinOpen = true;

        Log.d("LIVESTATUS SHIJENTEST", "onCreate: "+VariableConstant.isLiveBokinOpen);
        disposable = new CompositeDisposable();
        liveDispose = new CompositeDisposable();

        initialize();
        initializeToolBar();
        typeFace();
        animateMarker();
    }

    private void initializeToolBar() {

        toolBarLiveStatus = findViewById(R.id.toolBarLiveTrack);
        setSupportActionBar(toolBarLiveStatus);
        tv_skip = findViewById(R.id.tv_skip);
        tvCenter = findViewById(R.id.tv_center);
        tv_skip.setText(getString(R.string.cancel));
        tv_skip.setTextColor(Utilities.getColor(this,R.color.red_login));
        if(bookStatus>6)
            tv_skip.setVisibility(View.INVISIBLE);
        getSupportActionBar().setTitle("");
        String setTitle = getResources().getString(R.string.eventId) + " " + bId;
        tvCenter.setText(setTitle);
        tvCenter.setTypeface(apptypeFace.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolBarLiveStatus.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolBarLiveStatus.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (System.currentTimeMillis()>=(cancelTimeStamp)) {
                    liveBookingController.cancelBooking(LiveStatus.this, sharedPrefs,
                            aProgress, false, bId, true);
                } else {
                    liveBookingController.cancelBooking(LiveStatus.this, sharedPrefs,
                            aProgress, false, bId, false);
                }

                if (cancelTimeStamp==0) {
                    liveBookingController.cancelBooking(LiveStatus.this, sharedPrefs,
                            aProgress, false, bId, false);
                }
            }
        });
    }

    private void initialize() {

        sharedPrefs = new SharedPrefs(this);
        aProgress = new AlertProgress(this);
        Log.d("TAG", "isMqttConnectedOnResumeIsConnected: "+ApplicationController.getInstance().isMqttConnected());
        if(!ApplicationController.getInstance().isMqttConnected())
        {
            ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());
        }
        liveBookingController = new LiveBookingStatusController(sharedPrefs, this);
        double size[] = Scaler.getScalingFactor(this);
        width = (40) * size[0];
        hight = (40) * size[1];
        width1 = (80) * size[0];
        hight1 = (80) * size[1];
        if (getIntent().getExtras() != null)
        {
            Bundle extras = getIntent().getExtras();
            bId = extras.getLong("BID", 0);
            bookStatus = extras.getInt("STATUS", 0);
            proProfilePic = extras.getString("ImageUrl");
            Log.e("TAG", "initializeLIVEBOOKING: "+bId +" pic "+proProfilePic);
        }
        apptypeFace = AppTypeface.getInstance(this);

        ivLiveProInfo = findViewById(R.id.ivLiveProInfo);
        ivLiveProProfile = findViewById(R.id.ivLiveProProfile);

        profilePicset();

        tvLiveDistance = findViewById(R.id.tvLiveDistance);
        tvLiveDistanceKm = findViewById(R.id.tvLiveDistanceKm);
        tvLiveETA = findViewById(R.id.tvLiveETA);
        tvLiveETAMin = findViewById(R.id.tvLiveETAMin);

        rlProContact = findViewById(R.id.rlProContact);
        tvLiveReqTime = findViewById(R.id.tvLiveReqTime);
        tvLiveAcptTime = findViewById(R.id.tvLiveAcptTime);
        tvLiveAssgnedTOInfo = findViewById(R.id.tvLive_assgnedTOInfo);
        tvLiveOnTheWayTime = findViewById(R.id.tvLiveOnTheWayTime);
        ivLiveOnTheWayTime = findViewById(R.id.ivLiveOnTheWayTime);
        tvLiveOnTheWayArtist = findViewById(R.id.tvLiveOnTheWayArtist);
        tvLiveArrivedTime = findViewById(R.id.tvLiveArrivedTime);
        ivLiveArrivedTime = findViewById(R.id.ivLiveArrivedTime);
        tvLiveArrivedArtist = findViewById(R.id.tvLiveArrivedArtist);
        tvLiveEventStartedTime = findViewById(R.id.tvLiveEventStartedTime);
        ivLiveEventStartedTime = findViewById(R.id.ivLiveEventStartedTime);
        tvLiveReqArtist = findViewById(R.id.tvLiveReqArtist);
        tvLiveAcptArtist = findViewById(R.id.tvLiveAcptArtist);
        rldiscount = findViewById(R.id.rldiscount);
        tvLiveEventStartedArtist = findViewById(R.id.tvLiveEventStartedArtist);
        vacceptedbel = findViewById(R.id.vacceptedbel);
        vOntheWay = findViewById(R.id.vOntheWay);
        vNotaryArrived = findViewById(R.id.vNotaryArrived);
        vNotaryStarted = findViewById(R.id.vNotaryStarted);
        vNotaryCompleted = findViewById(R.id.vNotaryCompleted);
        vNotaryInvoice = findViewById(R.id.vNotaryInvoice);
        tvLiveEventStartedArtist = findViewById(R.id.tvLiveEventStartedArtist);
        rlconfm_travelfee = findViewById(R.id.rlconfm_travelfee);

        jobTimer = findViewById(R.id.jobTimer);
        rlTimerLayout=findViewById(R.id.rlTimerLayout);
        tvYourGigTimeIs = findViewById(R.id.tvYourGigTimeIs);

        tvLiveCompletedTime = findViewById(R.id.tvLiveCompletedTime);
        tvLiveCompletedArtist = findViewById(R.id.tvLiveCompletedArtist);
        ivLiveCompletedTime = findViewById(R.id.ivLiveCompletedTime);

        tvLiveInvoiceTime = findViewById(R.id.tvLiveInvoiceTime);
        tvLiveInvoiceArtist = findViewById(R.id.tvLiveInvoiceArtist);
        ivLiveInvoiceTime = findViewById(R.id.ivLiveInvoiceTime);

        rtConfirm = findViewById(R.id.rtConfirm);
        tvConfirmName = findViewById(R.id.tvConfirmName);
        tvConfirmNumrOfRevw = findViewById(R.id.tvConfirmNumrOfRevw);
        tvViewProfile = findViewById(R.id.tvConfirmDistance);
        rlSelectedCategory = findViewById(R.id.rlSelectedCategory);
        rlSelectedCategory.setVisibility(View.GONE);
        tvViewProfile.setText(getResources().getString(R.string.viewprofile));
        tvViewProfile.setTextColor(Utilities.getColor(this,(R.color.red_login)));

        tvViewProfile.setOnClickListener(this);

        tvVisitFeeAmt = findViewById(R.id.tvVisitFeeAmt);
        tv_travelFeeFee = findViewById(R.id.tv_travelFeeFee);
        tvTravelFeeAmt = findViewById(R.id.tvTravelFeeAmt);
        llLiveFee = findViewById(R.id.llLiveFee);
        llLiveStatus = findViewById(R.id.llLiveStatus);
        tvDiscountAmount = findViewById(R.id.tvDiscountAmount);
        tvTotalAmt = findViewById(R.id.tvTotalAmt);
        progressLive = findViewById(R.id.progressLive);
        rlconfm_visitfee = findViewById(R.id.rlconfm_visitfee);
        rlconfm_visitfee.setVisibility(View.GONE);
        rlLastdues = findViewById(R.id.rlLastdues);
        tvLastDues = findViewById(R.id.tvLastDues);
        tvLastDuesAmt = findViewById(R.id.tvLastDuesAmt);
        progressLive.setVisibility(View.VISIBLE);

        rlStatusMap = findViewById(R.id.rlStatusMap);
        mGoogleMap = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mGoogleMap);
        mapPloting();
        liveBookingController.doGetBookingDetails(bId);
        //setTotalAmount(serviceFee, gigFee, discountFee);
        checkNetworkListener();
    }

    private void profilePicset() {
        Log.e("TAG", "initializeprofilePicset: "+proProfilePic );
        if (proProfilePic!=null && !proProfilePic.equals("")) {
            Picasso.with(this)
                    .load(proProfilePic)
                    .resize((int) width, (int) hight)
                    .transform(new CircleTransform())
                    .into(ivLiveProInfo);
            Picasso.with(this)
                    .load(proProfilePic)
                    .resize((int) width1, (int) hight1)
                    .transform(new CircleTransform())
                    .into(ivLiveProProfile);
        }
    }
    private void setPaymentBreakdown(CartData cartData) {
        int size=cartData.getCheckOutItem().size();
        for(int i=0;i<size;i++){
            Item item=cartData.getCheckOutItem().get(i);
            View infltedView = LayoutInflater.from(LiveStatus.this).inflate(R.layout.singleserviceprice_pending, llLiveFee, false);
            llLiveFee.addView(infltedView);
            TextView tvServiceName = infltedView.findViewById(R.id.tvServiceName);
            TextView tvServicePrice = infltedView.findViewById(R.id.tvServicePrice);
            // Setting fonts and price
            String namePerUnit;
            if(accounting.getServiceType()==2){
                int accJobhr= (int) (accounting.getTotalActualJobTimeMinutes()/60);
                int accJobMin= (int) (accounting.getTotalActualJobTimeMinutes()%60);
                String actual_hrs;
                if(accJobMin>0) {
                    actual_hrs = accJobhr + " hr " + accJobMin + " min";
                }else{
                    actual_hrs = accJobhr + " hr " ;
                }
                namePerUnit=item.getServiceName() + " (x" + actual_hrs+")";
                Utilities.setAmtOnRecept(accounting.getTotalActualHourFee(),tvServicePrice,cartData.getCurrencySymbol());
            }else{
                if(item.getServiceName().trim().isEmpty()){
                    namePerUnit="Hourly"+" (x" + item.getQuntity()+")";
                }else{
                    namePerUnit = item.getServiceName() + " (x" + item.getQuntity()+")";
                }
                Utilities.setAmtOnRecept(item.getAmount(),tvServicePrice,cartData.getCurrencySymbol());
            }

            tvServiceName.setText(namePerUnit);
            tvServiceName.setTypeface(apptypeFace.getHind_regular());
            tvServicePrice.setTypeface(apptypeFace.getHind_regular());
        }
    }

    private void checkNetworkListener() {
        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Log.d("LIVESTATUS"," NetworkStatus "+nwStatus);
                if(nwStatus) {
                    Log.d("TAGBOOKING", "onNetworkStateChanges: "+ ApplicationController.getInstance().isMqttConnected()+" Proid "+proId);
                }
            }
        });
    }

    private void mapPloting() {
        if (mGoogleMap != null) {
            mGoogleMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    locatingUpdateHandler.postDelayed(locationUpdateRunnable,5000);
                }
            });
        }
    }

    private void loadMap(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        isMapLoaded = true;
        Log.d("TAG", "loadMapLiveStatus: "+cusLat+" "+cusLng);
        googleMap.setMyLocationEnabled(false);
        LatLng latLng = new LatLng(cusLat, cusLng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
        mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(cameraUpdate);
        if(cusLat!=0 && cusLng!=0 && proLat!=0 && proLng!=0)
        {
            plotMarker();
        }
    }


    private void initializeRxJava() {

        observer = new Observer<MyEventStatus>()
        {
            @Override
            public void onSubscribe(Disposable d) {
                disposable.add(d);
            }

            @Override
            public void onNext(MyEventStatus myEventStatus)
            {
                Log.d(TAG, "onNextLive: " + myEventStatus.getData().getStatus());
                if(bookStatus==myEventStatus.getData().getStatus() && myEventStatus.getData().getStatus()!=8){
                    //Checking 8 as the status is used many times
                    //when job is paused and started
                    return;
                }
                bookStatus = myEventStatus.getData().getStatus();
                switch (myEventStatus.getData().getStatus()) {
                    case 6:
                        if(mMap!=null && isMapLoaded)
                            loadMap(mMap);
                        subscribeToLiveTrack(bookStatus);
                        tv_nrd_track.setVisibility(View.VISIBLE);
                        tvCenter.setText(getString(R.string.onTheWay));
                        liveBookingController.onRxJavaUiUpdate(LiveStatus.this, myEventStatus.getData().getStatusUpdateTime()
                                , tvLiveOnTheWayTime, tvLiveOnTheWayArtist, tvLiveArrivedTime, tvLiveArrivedArtist
                                , ivLiveOnTheWayTime, ivLiveArrivedTime);
                        break;
                    case 7:
                        unsubscribeToLiveTrack();
                        visibleStatus(getString(R.string.artistHasArrived));
                        tv_skip.setVisibility(View.INVISIBLE);
                        liveBookingController.onRxJavaUiUpdate(LiveStatus.this, myEventStatus.getData().getStatusUpdateTime()
                                , tvLiveArrivedTime, tvLiveArrivedArtist, tvLiveEventStartedTime, tvLiveEventStartedArtist
                                , ivLiveArrivedTime, ivLiveEventStartedTime);
                        rlProContact.setVisibility(View.GONE);
                        removeLocationHandler();
                        break;
                    case 8:
                        rlTimerLayout.setVisibility(View.VISIBLE);
                        tv_nrd_track.setVisibility(View.INVISIBLE);
                        visibleStatus(getString(R.string.artistEventStarted));
                        liveBookingController.onRxJavaUiUpdate(LiveStatus.this, myEventStatus.getData().getStatusUpdateTime()
                                , tvLiveEventStartedTime, tvLiveEventStartedArtist, tvLiveCompletedTime, tvLiveCompletedArtist
                                , ivLiveEventStartedTime, ivLiveCompletedTime);
                        rlProContact.setVisibility(View.GONE);
                        Log.e(TAG, "onNext: Status8 startingtimer:::: "+myEventStatus.getData().getBookingTimer().getStartTimeStamp()+" "+myEventStatus.getData().getBookingTimer().getSecond()+"  "+myEventStatus.getData().getBookingTimer().getStatus());
                        setTimer(myEventStatus.getData().getBookingTimer().getStartTimeStamp(),myEventStatus.getData().getBookingTimer().getSecond(),myEventStatus.getData().getBookingTimer().getStatus());
                        removeLocationHandler();
                        break;
                    case 9:
                        rlTimerLayout.setVisibility(View.GONE);
                        tv_nrd_track.setVisibility(View.INVISIBLE);
                        visibleStatus(getString(R.string.artistEventCompleted));
                        liveBookingController.onRxJavaUiUpdate(LiveStatus.this, myEventStatus.getData().getStatusUpdateTime()
                                , tvLiveCompletedTime, tvLiveCompletedArtist, tvLiveInvoiceTime, tvLiveInvoiceArtist
                                , ivLiveCompletedTime, ivLiveInvoiceTime);
                        rlProContact.setVisibility(View.GONE);
                        removeLocationHandler();
                        break;
                    case 10:
                        if(isRateProviderCalled) {
                            removeLocationHandler();
                            isRateProviderCalled = false;
                            Intent intent;
                           /* if (Build.VERSION.SDK_INT >= 23) {
                                intent = new Intent(LiveStatus.this, RateProviderActivity.class);
                                intent.putExtra("BID", bId);
                                intent.putExtra("PROIMAGE", proProfilePic);
                                ActivityOptionsCompat options = ActivityOptionsCompat.
                                        makeSceneTransitionAnimation(LiveStatus.this,
                                                ivLiveProProfile,
                                                ViewCompat.getTransitionName(ivLiveProProfile));
                                startActivity(intent, options.toBundle());
                                //finish(); i am doing
                                onDestroyCalled();
                            }*/
                            // else {
                            intent = new Intent(LiveStatus.this, RateYourBooking.class);
                            intent.putExtra("BID", bId);
                            intent.putExtra("PROIMAGE", proProfilePic);
                            startActivity(intent);
                            // finish(); i am doing

                            onDestroyCalled();
                            // }

                        }

                        break;

                    case 11:
                        removeLocationHandler();


                            aProgress.alertPostivionclick(myEventStatus.getData().getStatusMsg(),
                                    new DialogInterfaceListner() {
                                        @Override
                                        public void dialogClick(boolean isClicked) {
                                            if (VariableConstant.SCHEDULEAVAILABLE) {

                                                aProgress.alertonclick(getString(R.string.alert_cap), getResources().getString(R.string.book_another_provider), getString(R.string.yes),
                                                        getString(R.string.no), new DialogInterfaceListner() {
                                                            @Override
                                                            public void dialogClick(boolean isClicked) {
                                                                if(isClicked) {
                                                                    Log.e(TAG, "dialogClick: Rebook pro "+isClicked);
                                                                    if(aProgress.isNetworkAvailable()) {
                                                                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                                                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                                                        liveBookingController.triggerNewScheduleBooking(bId);
                                                                    } else {
                                                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                                        aProgress.showNetworkAlert();
                                                                    }
                                                                }
                                                            }
                                                        });
                                            } else {
                                                VariableConstant.isConfirmBook = false;
                                                VariableConstant.isSplashCalled = true;
                                                Intent intent1 = new Intent(LiveStatus.this, MenuActivity.class);
                                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent1);
                                                // finish(); i am doing
                                                onDestroyCalled();
                                            }
                                        }
                                    });
                        break;

                    case 15:
                        Toast.makeText(LiveStatus.this,"Unassigned",Toast.LENGTH_SHORT).show();
                        removeLocationHandler();
                        aProgress.alertPostivionclick(myEventStatus.getData().getStatusMsg(),
                                new DialogInterfaceListner() {
                                    @Override
                                    public void dialogClick(boolean isClicked) {
                                        VariableConstant.isConfirmBook = false;
                                        VariableConstant.isSplashCalled = true;
                                        Intent intent1 = new Intent(LiveStatus.this, MenuActivity.class);
                                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent1);
                                        // finish(); i am doing
                                        onDestroyCalled();
                                    }
                                });
                        break;

                }
                setProgressLine(bookStatus);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        MyEventStatusObservable.getInstance().subscribeOn(Schedulers.io());
        MyEventStatusObservable.getInstance().subscribe(observer);
    }

    private void setProgressLine(int bookStatus) {

        if(oldStatus ==bookStatus){
            return;
        }
        oldStatus = bookStatus;
        animation = AnimationUtils.loadAnimation(this,R.anim.top_to_bottom);

        Log.d("setProgressLine", "setProgressLine: "+bookStatus);
        switch (bookStatus)
        {
//            vacceptedbel,vOntheWay,vNotaryArrived,vNotaryStarted,vNotaryCompleted,vNotaryInvoice;
            case 3://Status Accepted
                vacceptedbel.setVisibility(View.VISIBLE);
                vacceptedbel.startAnimation(animation);
                break;
            case 6://Ontheway
                vacceptedbel.setVisibility(View.VISIBLE);
                vOntheWay.setVisibility(View.VISIBLE);
                vOntheWay.startAnimation(animation);
                break;
            case 7://Arrived
                vacceptedbel.setVisibility(View.VISIBLE);
                vOntheWay.setVisibility(View.VISIBLE);
                vNotaryArrived.setVisibility(View.VISIBLE);
                vNotaryArrived.startAnimation(animation);
                break;
            case 8://JobStarted
                vacceptedbel.setVisibility(View.VISIBLE);
                vOntheWay.setVisibility(View.VISIBLE);
                vNotaryArrived.setVisibility(View.VISIBLE);
                vNotaryStarted.setVisibility(View.VISIBLE);
                vNotaryStarted.startAnimation(animation);
                break;
            case 9://Completed
                vacceptedbel.setVisibility(View.VISIBLE);
                vOntheWay.setVisibility(View.VISIBLE);
                vNotaryArrived.setVisibility(View.VISIBLE);
                vNotaryStarted.setVisibility(View.VISIBLE);
                vNotaryCompleted.setVisibility(View.VISIBLE);
                vNotaryCompleted.startAnimation(animation);
                break;
            case 10://InvoiceRaised
                vacceptedbel.setVisibility(View.VISIBLE);
                vOntheWay.setVisibility(View.VISIBLE);
                vNotaryArrived.setVisibility(View.VISIBLE);
                vNotaryStarted.setVisibility(View.VISIBLE);
                vNotaryCompleted.setVisibility(View.VISIBLE);
                vNotaryInvoice.setVisibility(View.VISIBLE);
                vNotaryInvoice.startAnimation(animation);
                break;
        }
    }

    private void unsubscribeToLiveTrack()
    {
        if(ApplicationController.getInstance().isMqttConnected())
        {
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.LiveTrack.value + "/" + proId);
        }
        tv_nrd_track.setVisibility(View.INVISIBLE);

    }

    private void typeFace() {

        tvLiveReqTime.setTypeface(apptypeFace.getHind_bold());
        TextView tvLiveReqArtist = findViewById(R.id.tvLiveReqArtist);
        tvLiveReqArtist.setTypeface(apptypeFace.getHind_regular());

        tvLiveAcptTime.setTypeface(apptypeFace.getHind_bold());
        TextView tvLiveAcptArtist = findViewById(R.id.tvLiveAcptArtist);
        tvLiveAcptArtist.setTypeface(apptypeFace.getHind_regular());

        tvLiveAssgnedTOInfo.setTypeface(apptypeFace.getHind_regular());
        ImageView ivLiveCallInfo = findViewById(R.id.ivLiveCallInfo);
        TextView tvLiveCallPro = findViewById(R.id.tvLiveCallPro);
        ImageView ivLiveMsgInfo = findViewById(R.id.ivLiveMsgInfo);
        TextView tvLiveMsgInfo = findViewById(R.id.tvLiveMsgInfo);
        TextView tvLiveCallMsg = findViewById(R.id.tvLiveCallMsg);
        TextView tvLiveCallInfo = findViewById(R.id.tvLiveCallInfo);
        tvLiveCallInfo.setTypeface(apptypeFace.getHind_regular());
        tvLiveMsgInfo.setTypeface(apptypeFace.getHind_regular());
        tvLiveCallPro.setTypeface(apptypeFace.getHind_regular());
        tvLiveMsgInfo.setTypeface(apptypeFace.getHind_regular());
        tvLiveCallMsg.setTypeface(apptypeFace.getHind_regular());

        tvConfirmNumrOfRevw.setTypeface(apptypeFace.getHind_regular());
        tvViewProfile.setTypeface(apptypeFace.getHind_medium());
        tvConfirmName.setTypeface(apptypeFace.getHind_semiBold());
        tvLiveCallInfo.setOnClickListener(this);
        tvLiveMsgInfo.setOnClickListener(this);
        ivLiveCallInfo.setOnClickListener(this);
        ivLiveMsgInfo.setOnClickListener(this);
        tvLiveCallMsg.setOnClickListener(this);
        tvLiveCallPro.setOnClickListener(this);

        tvLiveOnTheWayTime.setTypeface(apptypeFace.getHind_bold());
        tvLiveOnTheWayArtist.setTypeface(apptypeFace.getHind_regular());
        tv_nrd_track = findViewById(R.id.tv_nrd_track);
        tv_nrd_track.setTypeface(apptypeFace.getHind_medium());
        tv_nrd_track.setOnClickListener(this);

        tvLiveArrivedTime.setTypeface(apptypeFace.getHind_bold());
        tvLiveArrivedArtist.setTypeface(apptypeFace.getHind_regular());

        tvLiveEventStartedTime.setTypeface(apptypeFace.getHind_bold());
        tvLiveEventStartedArtist.setTypeface(apptypeFace.getHind_regular());

        tvLiveCompletedTime.setTypeface(apptypeFace.getHind_bold());
        tvLiveCompletedArtist.setTypeface(apptypeFace.getHind_regular());

        tvLiveInvoiceTime.setTypeface(apptypeFace.getHind_bold());
        tvLiveInvoiceArtist.setTypeface(apptypeFace.getHind_regular());

        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvPaymentBreak = findViewById(R.id.tvPaymentBreak);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotal = findViewById(R.id.tvTotal);
        tvVisitFee.setTypeface(apptypeFace.getHind_regular());
        tvPaymentBreak.setTypeface(apptypeFace.getHind_bold());
        tv_travelFeeFee.setTypeface(apptypeFace.getHind_regular());
        tvTravelFeeAmt.setTypeface(apptypeFace.getHind_regular());
        tvPaymentBreak.setText(R.string.yourPaymentBreak);
        tvVisitFeeAmt.setTypeface(apptypeFace.getHind_regular());
        tvDiscount.setTypeface(apptypeFace.getHind_regular());
        tvDiscountAmount.setTypeface(apptypeFace.getHind_regular());
        tvTotal.setTypeface(apptypeFace.getHind_bold());
        tvTotalAmt.setTypeface(apptypeFace.getHind_bold());
        tvLiveDistanceKm.setTypeface(apptypeFace.getHind_regular());
        tvLiveDistance.setTypeface(apptypeFace.getHind_regular());
        tvLiveETAMin.setTypeface(apptypeFace.getHind_regular());
        tvLiveETA.setTypeface(apptypeFace.getHind_regular());

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        VariableConstant.isLiveBokinOpen = true;
        Log.d("LIVESTATUS SHIJENTEST", "onResume: "+VariableConstant.isLiveBokinOpen);
        VariableConstant.isLiveBookingOpen = true;
        initializeRxJava();
        //  onMqttChange();
        if(ApplicationController.getInstance().isMqttConnected()){
            Log.d("LIVESTATUS SHIJENTEST ", " onResume: subscribing");
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + sharedPrefs.getCustomerSid(), 1);
        }
        else
        {
            ApplicationController.getInstance().createMQttConnection(sharedPrefs.getCustomerSid());
            toSubscribe();
        }
        if(observer!=null)
        {
            MyEventStatusObservable.getInstance().replay();
        }
        //FupdateIUmageUi(bookStatus);
        updateITextUi(bookStatus);
        subscribeToLiveTrack(bookStatus);
    }

    private void toSubscribe()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("LIVESTATUS SHIJENTEST", "run: subscribing");
                ApplicationController.getInstance().subscribeToTopic(MqttEvents.JobStatus.value + "/" + sharedPrefs.getCustomerSid(), 1);
            }
        },1000);
    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvLiveCallPro:
            case R.id.tvLiveCallInfo:
            case R.id.ivLiveCallInfo:
                //liveBookingController.makeCallToProvider(phoneNo,this);
                try{

                    if (phoneNo!=null&&!TextUtils.isEmpty(phoneNo)) {
                        Intent callIntent = new Intent(LiveStatus.this, ClientActivity.class);
                        callIntent.putExtra("phonenumber", phoneNo);
                        callIntent.putExtra("TWILIO_API_LINK", VariableConstant.TWILIO_LINK);
                        startActivity(callIntent);
                    } else {
                        Toast.makeText(this,getResources().getString(R.string.driverPhoneNoNotAvailable),Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvLiveCallMsg:
            case R.id.tvLiveMsgInfo:
            case R.id.ivLiveMsgInfo:
                intent = new Intent(LiveStatus.this, ChattingActivity.class);
                intent.putExtra("BID", bId);
                intent.putExtra("PROIMAGE", proProfilePic);
                intent.putExtra("PROID",proId);
                intent.putExtra("PRONAME",proName);
                startActivity(intent);
                break;
            case R.id.tv_nrd_track:

                visibleMap();
                loadMap(mMap);

                break;
            case R.id.tvConfirmDistance:
                intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("IMAGEURL", proProfilePic);
                intent.putExtra("PROVIDERID", proId);
                intent.putExtra("ISLIVESTATUS", true);

                if (Build.VERSION.SDK_INT >= M) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(LiveStatus.this,
                                    ivLiveProProfile,
                                    ViewCompat.getTransitionName(ivLiveProProfile));
                    startActivity(intent, options.toBundle());
                } else

                {
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                break;
        }
    }

    private void visibleMap()
    {
        llLiveStatus.setVisibility(View.GONE);
        rlStatusMap.setVisibility(View.VISIBLE);
        tv_skip.setVisibility(View.VISIBLE);
        isMapShowing = true;
        toolBarLiveStatus.setNavigationIcon(R.drawable.ic_close_24dp);
        String setTitle = getResources().getString(R.string.eventId) + " " + bId;
        tvCenter.setText(setTitle);

    }

    @Override
    public void onSuccess() {
        progressLive.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessTriggerScheduleBook() {
        progressLive.setVisibility(View.GONE);
        onBackPressed();
    }

    @Override
    public void onGigTimeServices(CartData gigBookinTim) {
        try{
            setPaymentBreakdown(gigBookinTim);
            tvLiveReqArtist.setText(gigBookinTim.getCategoryName()+" "+getString(R.string.artistReq));
            tvLiveAcptArtist.setText(gigBookinTim.getCategoryName()+" "+getString(R.string.artistAcpt));
            tvLiveOnTheWayArtist.setText(gigBookinTim.getCategoryName()+" "+getString(R.string.artistOnTheWay));
            tvLiveArrivedArtist.setText(gigBookinTim.getCategoryName()+" "+getString(R.string.artistHasArrived));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStatus(int status, String paymentType, String signatureUrl,LatLng latLng,String currencySymbol) {
        bookStatus = status;
        this.currencySymbol = currencySymbol;
        updateITextUi(status);

        cusLat = latLng.latitude;
        cusLng = latLng.longitude;
        if(mMap!=null)
        {
            if(bookStatus<=7)
                loadMap(mMap);
        }
        updateIUmageUi(status);

    }

    @Override
    public void onJobStatusTime(LiveBookingStatusPojo.LiveBookingData.JobBookingStatus jobBookingStatus) {
        liveBookingController.setJobStatusTimer(bookStatus, tvLiveReqTime, tvLiveAcptTime, tvLiveOnTheWayTime
                , tvLiveArrivedTime, tvLiveEventStartedTime, tvLiveCompletedTime, tvLiveInvoiceTime, jobBookingStatus);
    }

    @Override
    public void onProviderDtls(LiveBookingStatusPojo.LiveBookingData.ProviderBookingDetls providerBookingDetls) {
        phoneNo = providerBookingDetls.getPhone();
        proLat = providerBookingDetls.getProLocation().getLatitude();
        proLng = providerBookingDetls.getProLocation().getLongitude();
        Log.d("TAG", "onProviderDtlsPID: "+providerBookingDetls.getProviderId());
        proId = providerBookingDetls.getProviderId();
        String name = providerBookingDetls.getFirstName() + " " + providerBookingDetls.getLastName();
        String proNameInfo = name + " " + getResources().getString(R.string.hasAcceptedYourReq);
        tvLiveAssgnedTOInfo.setText(proNameInfo);
        proName = name;
        tvConfirmName.setText(name);
        rtConfirm.setIsIndicator(true);
        rtConfirm.setRating(providerBookingDetls.getAverageRating());
        tvConfirmNumrOfRevw.setTextColor(Utilities.getColor(this, R.color.black_john));
        String reviews = providerBookingDetls.getReviewCount() + " reviews";
        tvConfirmNumrOfRevw.setText(reviews);
        Log.d("TAG", "isMqttConnectedonProviderDtls: "+ApplicationController.getInstance().isMqttConnected());
        proProfilePic = providerBookingDetls.getProfilePic();

        profilePicset();
        ListenRxJavaRespo();
        if(bookStatus<=7)
        {
            subscribeToLiveTrack(bookStatus);
            plotMarker();
        }
    }
    private void subscribeToLiveTrack(final int bookStatus)
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("TAG", "run: "+proId +" bookStatus "+bookStatus);

                if(bookStatus==6 && !proId.equals(""))
                    ApplicationController.getInstance().subscribeToTopic(MqttEvents.LiveTrack.value + "/" + proId, 1);
            }
        },2000);
    }


    private void ListenRxJavaRespo() {
        liveObserver = new Observer<LiveTackPojo>() {
            @Override
            public void onSubscribe(Disposable d)
            {
                liveDispose.add(d);
            }

            @Override
            public void onNext(LiveTackPojo liveTackPojo)
            {

                Log.d("LIVESTATUS", "onNextLIVE: bookingid:"+liveTackPojo.getPid()+" "+liveTackPojo.getLatitude()+", "+liveTackPojo.getLongitude());
                if(liveTackPojo.getPid().equals(proId)){
                    proLat = liveTackPojo.getLatitude();
                    proLng = liveTackPojo.getLongitude();
                    plotMarker();
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        LiveTrackObservable.getInstance().subscribe(liveObserver);
        LiveTrackObservable.getInstance().subscribeOn(Schedulers.io());
    }

    @Override
    public void onBookingTimer(LiveBookingStatusPojo.LiveBookingData.BookingTimer bookingTimer) {
        this.bookingTimer=bookingTimer;
    }
    @Override
    public void onError(String errorMsg) {
        progressLive.setVisibility(View.GONE);
        aProgress.alertinfo(errorMsg);
    }

    @Override
    public void setCancellationAfterXmin(int cancellationAfterXmin) {

        long cancellationMillis = TimeUnit.MINUTES.toMillis(cancellationAfterXmin);

        cancelTimeStamp = (VariableConstant.bookingAcceptedTime*1000)+cancellationMillis;

    }

    @Override
    public void onSessionError() {
        liveBookingController.doGetBookingDetails(bId);
    }

    @Override
    public void onAccounting(AllEventsDataPojo.Accounting accounting) {
        this.accounting=accounting;
        Utilities.setAmtOnRecept(accounting.getTotal(),tvTotalAmt,currencySymbol);
        if(accounting.getDiscount()>0){
            rldiscount.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(accounting.getDiscount(),tvDiscountAmount,currencySymbol);
        }else{
            rldiscount.setVisibility(View.GONE);
        }

        if(accounting.getTravelFee()>0){
            rlconfm_travelfee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(accounting.getTravelFee(),tvTravelFeeAmt,currencySymbol);
        }else{
            rlconfm_travelfee.setVisibility(View.GONE);
        }
        if(accounting.getLastDues()>0){
            rlLastdues.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(accounting.getLastDues(),tvLastDuesAmt,currencySymbol);
        }else{
            rlLastdues.setVisibility(View.GONE);
        }
        if(accounting.getVisitFee()>0){
            rlconfm_visitfee.setVisibility(View.VISIBLE);
            Utilities.setAmtOnRecept(accounting.getVisitFee(),tvVisitFeeAmt,currencySymbol);
        }
    }

    @Override
    public void setJobTimer() {
        rlTimerLayout.setVisibility(View.VISIBLE);
        Log.e(TAG, "setJobTimer:");
        setTimer(this.bookingTimer.getStartTimeStamp(),this.bookingTimer.getSecond(),this.bookingTimer.getStatus());
    }

    /**
     * <h1>updateITextUi</h1>
     * updating the Text user interface
     *
     * @param bookStatus booking status int
     */
    private void updateITextUi(int bookStatus)
    {
        Log.d("TAG", "updateITextUi: "+bookStatus);

        if(bookStatus<=3)
            tv_nrd_track.setVisibility(View.INVISIBLE);
        else if(bookStatus>6)
            tv_nrd_track.setVisibility(View.INVISIBLE);
        else
            tv_nrd_track.setVisibility(View.VISIBLE);
        liveBookingController.setStatusTextUi(this, bookStatus, tvLiveOnTheWayTime, tvLiveOnTheWayArtist,
                tvLiveArrivedTime, tvLiveArrivedArtist, tvLiveEventStartedTime, tvLiveEventStartedArtist
                , tvLiveCompletedTime, tvLiveCompletedArtist, tvLiveInvoiceTime, tvLiveInvoiceArtist);
        if(bookStatus>6)
        {
            rlProContact.setVisibility(View.GONE);
        }
    }

    /**
     * <h1>updateIUmageUi</h1>
     * updating the Image user interface
     *
     * @param bookStatus booking status int
     */
    private void updateIUmageUi(int bookStatus) {
        liveBookingController.setStatusImageUi(this, bookStatus, ivLiveOnTheWayTime, ivLiveArrivedTime, ivLiveEventStartedTime
                , ivLiveCompletedTime, ivLiveInvoiceTime);
        setProgressLine(bookStatus);
    }

    private void plotMarker() {
        LatLng cusLatLng = new LatLng(cusLat, cusLng);
        proLatLng = new LatLng(proLat, proLng);
        proMarker = liveBookingController.plotMArker(LiveStatus.this, cusLatLng, proLatLng, mMap, proProfilePic);
    }
    private void setTimer(long startTimeStamp, long completedSeconds, int status) {
        stoptimer();
        Log.e(TAG, "setTimer: checkingJobStatus:"+status);
        if (status == 1) {
            long currentTime = (Calendar.getInstance().getTimeInMillis()-sharedPrefs.getServerTimeDifference()) / 1000;
            long elapsedtime = currentTime - startTimeStamp;
            jobTimer(completedSeconds+elapsedtime);
        } else if (status == 0) {
            long minutes = (completedSeconds) / 60;
            long seconds = (completedSeconds) % 60;
            jobTimer.setText(String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
        }
    }

    private void jobTimer( long timecunsumedsecond)
    {
        timeConsumedSecond = timecunsumedsecond;
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        timeConsumedSecond = timeConsumedSecond + 1;
                        jobTimer.setText(formatSeconds(timeConsumedSecond));

                    }
                });
            }
        };
        Log.e(TAG, "jobTimer: Starting timertask");
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

    private void stoptimer() {
        if(timer!=null){
            timer.cancel();
        }
    }

    public static String formatSeconds(long timeInSeconds)
    {
        int hours = (int)timeInSeconds / 3600;
        int secondsLeft = (int)timeInSeconds - hours * 3600;
        int minutes = secondsLeft / 60;
        int seconds = secondsLeft - minutes * 60;

        String formattedTime = "";
        if (hours < 10)
            formattedTime += "0";
        formattedTime += hours + ":";

        if (minutes < 10)
            formattedTime += "0";
        formattedTime += minutes + ":";

        if (seconds < 10)
            formattedTime += "0";
        formattedTime += seconds ;

        return formattedTime;
    }

    private void animateMarker() {
        locatingUpdateHandler = new Handler();
        final Handler markeMoveHandler = new Handler();
        final float durationInMs = 4000;
        locationUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                locatingUpdateHandler.postDelayed(this,5000);
                if(mMap !=null && proMarker!=null) {
                    final LatLng startPosition = proMarker.getPosition();

                    final long start = SystemClock.uptimeMillis();
                    final Interpolator interpolator = new AccelerateDecelerateInterpolator();

                    markeMoveHandler.post(new Runnable() {
                        long elapsed;
                        float t;
                        float v;
                        @Override
                        public void run() {
                            // Calculate progress using interpolator
                            elapsed = SystemClock.uptimeMillis() - start;
                            t = elapsed / durationInMs;
                            v = interpolator.getInterpolation(t);
                            LatLng currentPosition = new LatLng(startPosition.latitude*(1-t)+ proLatLng.latitude*t,
                                    startPosition.longitude*(1-t)+ proLatLng.longitude*t);
                            proMarker.setPosition(currentPosition);
                            // Repeat till progress is complete.
                            if (t < 1) {
                                // Post again 16ms later.
                                markeMoveHandler.postDelayed(this, 16);
                            }
                        }
                    });
                }
            }
        };

    }

    @Override
    public void onBackPressed()
    {
        if (isMapShowing) {
            visibleStatus(getString(R.string.onTheWay));
        } else {
            onDestroyCalled();
            //  finish();
            //overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
            overridePendingTransition(R.anim.stay_still, R.anim.side_slide_in);
        }
    }

    private void onDestroyCalled()
    {
        VariableConstant.isLiveBokinOpen = false;
        Log.d("LIVESTATUS SHIJENTEST", "onDestroycalled: "+VariableConstant.isLiveBokinOpen);
        VariableConstant.isLiveBookingOpen = false;
        removeLocationHandler();
        Log.d("LIVESTATUS", "onResumeonDestroyCalled: "+ApplicationController.getInstance().isMqttConnected());
        if(ApplicationController.getInstance().isMqttConnected())
        {
            Log.d("LIVESTATUS", " SHIJENTEST onDestroyCalled: unsubscribing jobstatus" );
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.LiveTrack.value + "/" + proId);
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.JobStatus.value + "/" + sharedPrefs.getCustomerSid());
        }
        MyEventStatusObservable.getInstance().removeObserver(observer);
        disposable.clear();
        finish();
    }

    private void visibleStatus(String jobStatus) {
        if(isMapShowing)
        {
            isMapShowing = false;
            rlStatusMap.setVisibility(View.GONE);
            tv_skip.setVisibility(View.INVISIBLE);
            llLiveStatus.setVisibility(View.VISIBLE);
            toolBarLiveStatus.setNavigationIcon(R.drawable.ic_login_back_icon_off);
            tvCenter.setText(jobStatus);

        }

    }
    /**
     * @param serviceFee  service fee
     * @param gigFee      gig fee
     * @param discountFee discount fee
     */
    private void setTotalAmount(double serviceFee, double gigFee, double discountFee) {
        //Utilities.setAmtOnRecept(serviceFee,tvServiceFeeAmt,currencySymbol);
        Utilities.setAmtOnRecept(discountFee,tvDiscountAmount,currencySymbol);
        //double total = serviceFee + gigFee + discountFee;
        //Utilities.setAmtOnRecept(total,tvTotalAmt,currencySymbol);
    }

    @Override
    protected void onPause() {
        super.onPause();
        VariableConstant.isLiveBokinOpen = false;
        Log.d("LIVESTATUS", "onResumeonPause: "+VariableConstant.isLiveBokinOpen);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VariableConstant.isLiveBookingOpen = false;
        MyEventStatusObservable.getInstance().removeObserver(observer);
        liveDispose.clear();

        if(ApplicationController.getInstance().isMqttConnected())
        {
            ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.LiveTrack.value + "/" + proId);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utilities.checkAndShowNetworkError(this);
    }
    private void removeLocationHandler()
    {
        if(locationUpdateRunnable!=null && locatingUpdateHandler!=null)
            locatingUpdateHandler.removeCallbacks(locationUpdateRunnable);
    }

    @Override
    public void onDistanceTime(String distance, String eta) {

        String distanceAway = distance +" away";
        tvLiveETAMin.setText(eta);
        tvLiveDistanceKm.setText(distanceAway);
    }
}
