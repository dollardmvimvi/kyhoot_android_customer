package com.Kyhoot.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.Kyhoot.Dialog.BottomsheetFragmentFavProvider;
import com.Kyhoot.R;
import com.Kyhoot.adapter.LangExpertiseAdapter;
import com.Kyhoot.adapter.RatinReviewAdaptr;
import com.Kyhoot.controllers.ProviderDetailsControl;
import com.Kyhoot.interfaceMgr.ProfileDataResponce;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.pojoResponce.GeneralProviderData;
import com.Kyhoot.pojoResponce.ProMetaData;
import com.Kyhoot.pojoResponce.ProviderEvents;
import com.Kyhoot.pojoResponce.ProviderServiceDtls;
import com.Kyhoot.pojoResponce.RateReviews;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProviderDetails extends AppCompatActivity implements ProfileDataResponce.ProProfileDataResponce,UtilInterFace {

    private static final String TAG = "PROVIDERDETAILS";
    private TextView tvProDtlsName, tvProNumrOfRevw, tvProDistance,tvProBook,tvNoOfJobs,tvRating,tvNumberOfReview;//tvProAbout
    private RatingBar rtProvidrDtls;
    private String url;
    private ImageView ivProDtlsPic,img_calender;
    private SharedPrefs prefs;
    private AlertProgress aProgress;
    private ProgressBar progressBarPro,providerProgress;
    private ArrayList<ProviderServiceDtls> proServiceList = new ArrayList<>();
    private ArrayList<ProviderEvents> eventses = new ArrayList<>();
    private ArrayList<RateReviews> rateReview = new ArrayList<>();
    private RatinReviewAdaptr ratinReviewAdaptr;
    private Bundle intentBundle;
    private String proId;
    private LinearLayout metaDataContainer;
    private ProviderDetailsControl providerDetailscontrol;
    private boolean isLiveStatus;
    private AppBarLayout appBarLayout;
    private TextView toolBarTitle,tvProReviews;
    private Toolbar toolProvider;
    private RelativeLayout rlProRateReview;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int pageCount = 0;
    private boolean loading = true;
    String triedToBook;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppTypeface apptypeFace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details2);
        apptypeFace = AppTypeface.getInstance(this);
        intentBundle = new Bundle();
        isLiveStatus = getIntent().getBooleanExtra("ISLIVESTATUS", false);
        url = getIntent().getStringExtra(BottomsheetFragmentFavProvider.IMAGEURL);
        proId = getIntent().getStringExtra(BottomsheetFragmentFavProvider.PROVIDERID);
        InitializeView();
        typeFace();
        initializeToolBar();
        getIntentResource();

    }

    private void getIntentResource() {

        ivProDtlsPic = findViewById(R.id.ivProDtlsPic);
        ivProDtlsPic.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void getProviderDetails(){
        double latitude = Double.parseDouble(prefs.getBookLatitude());
        double longitude = Double.parseDouble(prefs.getBookLongitude());
        if (latitude == 0 && longitude == 0) {
            latitude = Double.parseDouble(prefs.getSplashLatitude());
            longitude = Double.parseDouble(prefs.getSplashLongitude());
        }
        LatLng latlng = new LatLng(latitude, longitude);
        progressBarPro.setVisibility(View.VISIBLE);
        providerDetailscontrol = new ProviderDetailsControl(this, prefs);
        providerDetailscontrol.ProDetailsService(this, latlng, proId);
    }

    private void initializeToolBar() {

        aProgress = new AlertProgress(this);
        toolProvider = findViewById(R.id.toolProvider);
        toolBarTitle = findViewById(R.id.toolBarTitle);
        setSupportActionBar(toolProvider);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolProvider.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolBarTitle.setTypeface(apptypeFace.getHind_semiBold());
    }

    private void InitializeView() {
        prefs = new SharedPrefs(this);
        progressBarPro = findViewById(R.id.progressBarPro);
        getProviderDetails();
        providerProgress = findViewById(R.id.providerProgress);
        rlProRateReview = findViewById(R.id.rlProRateReview);
        tvProDtlsName = findViewById(R.id.tvProDtlsName);
        tvProNumrOfRevw = findViewById(R.id.tvProNumrOfRevw);
        tvProDistance = findViewById(R.id.tvProDistance);
        tvNumberOfReview=findViewById(R.id.tvNumberOfReview);
        tvRating=findViewById(R.id.tvRating);
        tvNoOfJobs=findViewById(R.id.tvNoOfJobs);
        //tvProAbout = findViewById(R.id.tvProAbout);
        metaDataContainer =findViewById(R.id.metaDataContainer);
        rtProvidrDtls = findViewById(R.id.rtProvidrDtls);
        collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        appBarLayout = findViewById(R.id.app_bar);
        rtProvidrDtls.setIsIndicator(true);
        RecyclerView recyclerViewReview = findViewById(R.id.recyclerViewReview);
        final LinearLayoutManager llManageReview = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewReview.setLayoutManager(llManageReview);
        int resId = R.anim.layoutanimation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerViewReview.setLayoutAnimation(animation);
        ratinReviewAdaptr = new RatinReviewAdaptr(this, rateReview, true);
        recyclerViewReview.setAdapter(ratinReviewAdaptr);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTypeface(apptypeFace.getHind_semiBold());
        NestedScrollView nestedScrollView = findViewById(R.id.nestedScrollView);
        recyclerViewReview.setNestedScrollingEnabled(false);
        final UtilInterFace utilInterFace = ProviderDetails.this;
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        Log.i("TAG", "onScrolledN: ");
                        visibleItemCount = llManageReview.getChildCount();
                        totalItemCount = llManageReview.getItemCount();
                        pastVisiblesItems = llManageReview.findFirstVisibleItemPosition();
                        Log.i("TAG", "onScrolledN: "+visibleItemCount+" to "+totalItemCount+"  pa "+pastVisiblesItems);
                        if (loading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                loading = false;
                                Log.v("...", "Last Item Wow !");
                                providerProgress.setVisibility(View.VISIBLE);
                                pageCount++;
                                providerDetailscontrol.callReviewApi(pageCount, proId,utilInterFace);
                            }
                        }
                    }
                }
            }
        });

        img_calender = findViewById(R.id.img_calender);

        img_calender.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ProviderDetails.this,ScheduleActivity.class);
                startActivity(in);
            }
        });
    }

    private void appBarChangeListnr(final String name) {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            boolean isShow = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0)
                {
                    collapsingToolbarLayout.setTitle(name);
                    toolBarTitle.setText("Registerer");
                    toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle("");
                    // toolBarTitle.setText("");
                    toolProvider.setNavigationIcon(R.drawable.ic_login_back_icon_off);

                    isShow = false;
                }
            }
        });
    }

    private void typeFace() {
        //TextView tvProAboutMe;
        //tvProAboutMe = findViewById(R.id.tvProAboutMe);
        tvProReviews = findViewById(R.id.tvProReviews);
        tvProDtlsName.setTypeface(apptypeFace.getHind_semiBold());
        tvProNumrOfRevw.setTypeface(apptypeFace.getHind_semiBold());
        tvProDistance.setTypeface(apptypeFace.getHind_medium());
        tvRating.setTypeface(apptypeFace.getHind_regular());
        tvNumberOfReview.setTypeface(apptypeFace.getHind_regular());
        tvNoOfJobs.setTypeface(apptypeFace.getHind_regular());
        ((TextView)findViewById(R.id.tvReviews)).setTypeface(apptypeFace.getHind_regular());
        ((TextView)findViewById(R.id.tvAvgRating)).setTypeface(apptypeFace.getHind_regular());
        ((TextView)findViewById(R.id.tvJobs)).setTypeface(apptypeFace.getHind_regular());
        //tvProAbout.setTypeface(apptypeFace.getHind_regular());
        //tvProAboutMe.setTypeface(apptypeFace.getHind_semiBold());
        tvProReviews.setTypeface(apptypeFace.getHind_semiBold());
        if (!isLiveStatus) {
            RelativeLayout rlBook = findViewById(R.id.rlBook);
            tvProBook = findViewById(R.id.tvProBook);
            rlBook.setVisibility(View.VISIBLE);
            tvProBook.setTypeface(apptypeFace.getHind_semiBold());
            tvProBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    providerDetailscontrol.callServicesService(ProviderDetails.this,proId);
                    tvProBook.setEnabled(false);
                    VariableConstant.BOOKINGMODEL=2;
                    triedToBook = "Yes";
                }
            });
        }
        else
            tvProBook = findViewById(R.id.tvProBook);

    }

    @Override
    public void onProSuccessDtls(GeneralProviderData providerDataResp)
    {
        int height=(int)(260*this.getResources().getDisplayMetrics().density);

        int width= (this.getResources().getDisplayMetrics().widthPixels);
        if (progressBarPro != null)
            progressBarPro.setVisibility(View.GONE);
        String name = providerDataResp.getFirstName() + " " + providerDataResp.getLastName();
        appBarChangeListnr(name);
        VariableConstant.providerId=proId;
        String bookProName = getResources().getString(R.string.book) + " "+name;
        tvProBook.setText(bookProName);
        tvProDtlsName.setText(name);
        tvNumberOfReview.setText(providerDataResp.getNoOfReview());
        tvRating.setText(providerDataResp.getRating()+"");
        rtProvidrDtls.setRating(providerDataResp.getRating());
        tvNoOfJobs.setText(providerDataResp.getTotalBooking()+"");
        String reviews = providerDataResp.getNoOfReview() + " " + getString(R.string.reviews);
        tvProNumrOfRevw.setText(reviews);
        proServiceList.addAll(providerDataResp.getServices());
        if(!providerDataResp.getProfilePic().isEmpty()){
            Picasso.with(this).load(providerDataResp.getProfilePic())
                    .transform(new CircleTransform())
                    .resize((int) ivProDtlsPic.getWidth(), (int) ivProDtlsPic.getHeight())
                    .into(ivProDtlsPic);
        }
        Log.d(TAG, "onProSuccessDtls: "+providerDataResp.getAmount());
        if(providerDataResp.getAmount()>0){
            prefs.setCategoryHourlyPrice(providerDataResp.getAmount());
        }
        rateReview.addAll(providerDataResp.getReview());
        if(rateReview.size()>0)
        {
            rlProRateReview.setVisibility(View.VISIBLE);
        }
        String reviewsNumber = getString(R.string.reviews)+" ("+providerDataResp.getNoOfReview()+")";
        tvProReviews.setText(reviewsNumber);
        ratinReviewAdaptr.notifyDataSetChanged();
        //tvProAbout.setText(providerDataResp.getAbout());
        eventses.addAll(providerDataResp.getEvents());

        if(providerDataResp.getDistanceMatrix()==0)
            VariableConstant.distanceUnit = getString(R.string.distanceKm);
        else
            VariableConstant.distanceUnit = getString(R.string.distanceMiles);
        String distance = providerDataResp.getDistance()+" "+VariableConstant.distanceUnit;
        tvProDistance.setText(distance);
        //  providerDetailscontrol.distanceCal(providerDataResp.getDistance(), tvProDistance, ProviderDetails.this);
        //providerDetailscontrol.moreReadable(tvProAbout);

        intentBundle.putString("IMAGEURL", url);
        intentBundle.putString("NAME", tvProDtlsName.getText().toString());
        intentBundle.putString("PROID", proId);
        intentBundle.putFloat("RATING", rtProvidrDtls.getRating());
        intentBundle.putString("REVIEWNUMBER", tvProNumrOfRevw.getText().toString());
        intentBundle.putSerializable("PROSERVICELIST", proServiceList);
        intentBundle.putSerializable("EVENTS", eventses);
        intentBundle.putDouble("DISTANCE", providerDataResp.getDistance());
        if(providerDataResp.getMetaDataArr().size()>0) {
            metaDataContainer.setVisibility(View.VISIBLE);
            for(int i =0;i<providerDataResp.getMetaDataArr().size();i++)
            {
                ProMetaData metaDataArray = providerDataResp.getMetaDataArr().get(i);
                TextView tvProInstrument,tvProInstruments;
                RecyclerView recyclerViewproInstrument;
                View inflatedView = LayoutInflater.from(this).inflate(R.layout.provider_rules_info, metaDataContainer,false);
                metaDataContainer.addView(inflatedView);

                tvProInstrument = inflatedView.findViewById(R.id.tvProInstrument);
                tvProInstruments = inflatedView.findViewById(R.id.tvProInstruments);
                recyclerViewproInstrument = inflatedView.findViewById(R.id.recyclerViewproInstrument);

                tvProInstrument.setTypeface(apptypeFace.getHind_semiBold());
                tvProInstruments.setTypeface(apptypeFace.getHind_regular());

                if(metaDataArray.getFieldType()==1)
                {
                    tvProInstruments.setVisibility(View.VISIBLE);
                    tvProInstrument.setText(metaDataArray.getFieldName());
                    tvProInstruments.setText(metaDataArray.getData());
                    providerDetailscontrol.moreReadable(tvProInstruments);
                }
                else if(metaDataArray.getFieldType()==2 ||metaDataArray.getFieldType()==6)
                {

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                    String[] langArrya = metaDataArray.getData().split(",");
                    Log.d("TAG", "onSuccessMetaDataArray: "+langArrya.length+" atZero "+langArrya[0]);
                    recyclerViewproInstrument.setVisibility(View.VISIBLE);
                    recyclerViewproInstrument.setLayoutManager(linearLayoutManager);
                    if(langArrya.length>0) {
                        LangExpertiseAdapter langAdapter = new LangExpertiseAdapter(this,langArrya,false,null);
                        recyclerViewproInstrument.setAdapter(langAdapter);
                        langAdapter.notifyDataSetChanged();
                    }
                    tvProInstrument.setText(metaDataArray.getFieldName());
                }else if(metaDataArray.getFieldType()==5){
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                    recyclerViewproInstrument.setVisibility(View.VISIBLE);
                    recyclerViewproInstrument.setLayoutManager(linearLayoutManager);
                    tvProInstrument.setText(metaDataArray.getFieldName());
                    LangExpertiseAdapter  langAdapter = new LangExpertiseAdapter(this,null,true,metaDataArray.getPreDefined());
                    recyclerViewproInstrument.setAdapter(langAdapter);
                    langAdapter.notifyDataSetChanged();
                }
            }
        }

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    public void onSessionExpired(String error) {
        aProgress.alertPostivionclick(error, new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {

                Utilities.setMAnagerWithBID(ProviderDetails.this, prefs,"");

            }
        });

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onSuccess(String onSuccess) {

    }

    @Override
    public void onError(String errorMsg) {

        providerProgress.setVisibility(View.GONE);
        progressBarPro.setVisibility(View.GONE);
        aProgress.alertinfo(errorMsg);
    }

    @Override
    public void onSuccess(ArrayList<AddressPojo.AddressData> addressList) {

    }

    @Override
    public void onSuccessReview(ValidatorPojo reviewlist)
    {
        ValidatorPojo.SignUpDataSid reviewList = reviewlist.getData();

        Log.d("TAG", "onSuccessReview: "+reviewlist.getData().getAverageRating() +" AVG "
                +reviewList.getAverageRating());
        providerProgress.setVisibility(View.GONE);
        if(reviewList.getReviews().size()>0) {
            loading = true;
            rateReview.addAll(reviewList.getReviews());
            ratinReviewAdaptr.notifyDataSetChanged();
        } else {
            loading = false;
        }
    }

    @Override
    public void onSuccessServicesUpdate(String headerResponse, String result) {
        Intent intent;
        if(prefs.getBillingModel().equals("3") || prefs.getBillingModel().equals("4")){
            intent=new Intent(this,ConfirmBookingActivity.class);
        }else {
            intent=new Intent(this,CategoriesAndServices.class);
        }
        intent.putExtra("result",result);
        VariableConstant.providerId=proId;
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        tvProBook.setEnabled(true);
    }

    @Override
    public void onFailureServicesUpdate(String responseCode, String error) {
        Log.d(TAG, "onFailureCategoriesAvailable: responseCode:"+responseCode+" ERROR:"+error);
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
        tvProBook.setEnabled(true);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay_still, R.anim.slide_down_acvtivity);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utilities.checkAndShowNetworkError(this);
    }
}
