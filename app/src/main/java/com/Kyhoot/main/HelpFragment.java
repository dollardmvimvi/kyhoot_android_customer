package com.Kyhoot.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.Kyhoot.zendesk.zendesk.HelpIndexTicketDetails;
import com.Kyhoot.zendesk.zendeskadapter.HelpIndexAdapter;
import com.Kyhoot.zendesk.zendeskpojo.AllTicket;
import com.Kyhoot.zendesk.zendeskpojo.OpenClose;
import com.Kyhoot.zendesk.zendeskpojo.TicketClose;
import com.Kyhoot.zendesk.zendeskpojo.TicketOpen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * <h>HelpFragment</h>
 * for the help of the users
 * Created by 3Embed on 8/16/2017.
 */

public class HelpFragment extends Fragment
{
/*

    private View view;
    private Context mcontext;
    private EditText helpName, helpEmail, helpSubject, helpMessage;




    */
/*
    initializing views
     *//*

    private void initializeView() {
        helpName = view.findViewById(R.id.helpName);
        helpEmail = view.findViewById(R.id.helpEmail);
        helpSubject = view.findViewById(R.id.helpSubject);
        helpMessage = view.findViewById(R.id.helpMessage);
    }

    */
/*
    Setting the typeFace
     *//*

    public void setTypeface() {
        AppTypeface apptypeFace = AppTypeface.getInstance(getActivity());
        TextView tvHlpCntrhlp = view.findViewById(R.id.tvHlpCntrhlp);
        TextView tvHelpSend = view.findViewById(R.id.tvHelpSend);
        tvHlpCntrhlp.setTypeface(apptypeFace.getHind_regular());
        tvHelpSend.setTypeface(apptypeFace.getHind_semiBold());
        tvHelpSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }
*/
    private AppTypeface appTypeface;
    private RelativeLayout rlHelpIndex;
    private RecyclerView recyclerHelpIndex;
    private SharedPrefs sprefs;
    private HelpIndexAdapter helpIndexAdapter;
    private ArrayList<OpenClose> openCloses;
    private ProgressBar progressbarHelpIndex;
    private AlertProgress alertProgress;
    private View view;
    private ImageView ivAbarMenuBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.help_index, container, false);
        initializeView();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeToolBar();
        initializeView();
        getZendexTicket();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appTypeface = AppTypeface.getInstance(getActivity());
        openCloses = new ArrayList<>();

    }

    private void initializeView() {
        sprefs = new SharedPrefs(getActivity());
        alertProgress = new AlertProgress(getActivity());
        rlHelpIndex = view.findViewById(R.id.rlHelpIndex);
        recyclerHelpIndex = view.findViewById(R.id.recyclerHelpIndex);
        progressbarHelpIndex = view.findViewById(R.id.progressbarHelpIndex);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerHelpIndex.setLayoutManager(layoutManager);
        helpIndexAdapter = new HelpIndexAdapter(getActivity(),openCloses);
        recyclerHelpIndex.setAdapter(helpIndexAdapter);
    }

    /*
    initialize toolBar
     */
    private void initializeToolBar()
    {
        ivAbarMenuBtn = view.findViewById(R.id.ivAbarMenuBtn);
        TextView tv_center = view.findViewById(R.id.tv_center);
        TextView tv_skip = view.findViewById(R.id.tv_skip);
        tv_center.setText(R.string.helpcenter);
        tv_skip.setText("+");
        tv_skip.setTextSize(20);
        tv_skip.setTextColor(Utilities.getColor(getActivity(),R.color.red_login));
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        tv_skip.setTypeface(appTypeface.getHind_semiBold());
        ivAbarMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MenuActivity)getActivity()).popBackstack();
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),HelpIndexTicketDetails.class);
                intent.putExtra("ISTOAddTICKET",true);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
                //getActivity().overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
            }
        });
    }

    private void getZendexTicket()
    {
        progressbarHelpIndex.setVisibility(View.VISIBLE);
        //http://138.197.78.50:9871/zendesk/user/ticket/dhaval
        OkHttpConnection.requestOkHttpConnection(VariableConstant.PAYMENT_URL+"zendesk/user/ticket/" + sprefs.getEMail(), OkHttpConnection.Request_type.GET
                , new JSONObject(), sprefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result)
                    {
                        Log.d("TAG", "SpinneronSuccess: "+result);
                        progressbarHelpIndex.setVisibility(View.GONE);
                        AllTicket allTicket = new Gson().fromJson(result,AllTicket.class);

                        if(allTicket.getData().getClose().size()>0 || allTicket.getData().getOpen().size()>0)
                        {
                            rlHelpIndex.setVisibility(View.GONE);
                            recyclerHelpIndex.setVisibility(View.VISIBLE);
                            if(allTicket.getData().getClose().size()>0)
                            {
                                for(int i = 0;i<allTicket.getData().getClose().size();i++)
                                {
                                    TicketClose ticketClose = allTicket.getData().getClose().get(i);
                                    OpenClose openClose = new OpenClose(ticketClose.getId(),ticketClose.getTimeStamp()
                                            ,ticketClose.getStatus(),ticketClose.getSubject(),ticketClose.getType(),ticketClose.getPriority()
                                            ,ticketClose.getDescription());
                                    if(i==0)
                                    {
                                        openClose.setFirst(true);
                                    }
                                    openCloses.add(openClose);
                                }
                            }
                            if(allTicket.getData().getOpen().size()>0)
                            {
                                for(int i = 0;i<allTicket.getData().getOpen().size();i++)
                                {
                                    TicketOpen ticketOpen = allTicket.getData().getOpen().get(i);
                                    OpenClose openClose = new OpenClose(ticketOpen.getId(),ticketOpen.getTimeStamp()
                                            ,ticketOpen.getStatus(),ticketOpen.getSubject(),ticketOpen.getType(),
                                            ticketOpen.getPriority(),ticketOpen.getDescription());

                                    if(i==0)
                                    {
                                        openClose.setFirst(true);
                                    }
                                    openCloses.add(openClose);
                                }
                            }
                            helpIndexAdapter.notifyDataSetChanged();
                        }
                        else
                        {
                            rlHelpIndex.setVisibility(View.VISIBLE);
                            recyclerHelpIndex.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onError(String headerError, String error)
                    {
                        progressbarHelpIndex.setVisibility(View.GONE);
                        if (headerError.equals("502"))
                        {
                            alertProgress.alertinfo(getString(R.string.serverProblm));
                        }
                        else {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    sessionExpired(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccessToken(errorHandel.getData());
                                    break;
                                default:
                                    alertProgress.alertinfo(errorHandel.getMessage());
                                    break;
                            }
                        }
                    }
                });
    }

    private void getAccessToken(String data)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            sprefs.setSession(jsonobj.getString("data"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            alertProgress.alertinfo(getString(R.string.serverProblm));
                        else {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    sessionExpired(errorHandel.getMessage());

                                    break;
                                default:
                                    alertProgress.alertinfo(errorHandel.getMessage());
                                    break;
                            }
                        }
                    }
                });
    }

    private void sessionExpired(final String errorMsg)
    {
        alertProgress.alertPostivionclick(errorMsg,
                new DialogInterfaceListner() {
                    @Override
                    public void dialogClick(boolean isClicked) {
                        Utilities.setMAnagerWithBID(getActivity(),sprefs,errorMsg);
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VariableConstant.isHelpIndexOpen = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        VariableConstant.isHelpIndexOpen = true;
    }
}
