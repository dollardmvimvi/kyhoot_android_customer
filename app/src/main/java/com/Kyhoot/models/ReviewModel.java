package com.Kyhoot.models;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.interfaceMgr.MusicGeners;
import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.MusicGenersPojo;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONObject;

/**
 * <h>ReviewModel</h>
 * Created by Ali on 9/19/2017.
 */

public class ReviewModel {
    public void checkNetwork(AlertProgress aProgress, NetworkCheck network) {

        if (aProgress.isNetworkAvailable())
            network.onNetworkAvailble();
        else
            network.onNetworkNotAvailble();
    }

    public void getReviewService(int page, final UtilInterFace onsuccessIntrfce, String session) {

        JSONObject jsonObject = new JSONObject();
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "reviewAndRating/" + page, OkHttpConnection.Request_type.GET, jsonObject, session, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
            @Override
            public void onSuccess(String headerResponse, String result) {
                Log.d("REVIEW", "onSuccess: " + result + " headerResponse " + headerResponse);
                //reviews
                ValidatorPojo vPojo = new Gson().fromJson(result, ValidatorPojo.class);

                onsuccessIntrfce.onSuccessReview(vPojo);
                onsuccessIntrfce.hideProgress();

            }

            @Override
            public void onError(String headerError, String error) {
                try{
                    ErrorHandel vPojo = new Gson().fromJson(error, ErrorHandel.class);
                    onsuccessIntrfce.hideProgress();
                    onsuccessIntrfce.onError(vPojo.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void getMusicGenres(final MusicGeners musicGeners) {

        JSONObject jsonObject = new JSONObject();
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "serviceCateogries", OkHttpConnection.Request_type.GET
                , jsonObject, "", VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String result) {

                        Log.d("serveiceCategory", "onSuccess: " + result + " headerResponse " + headerResponse);
                        MusicGenersPojo mpojo = new Gson().fromJson(result, MusicGenersPojo.class);

                        musicGeners.onSuccess(mpojo.getData());
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        try{
                            ErrorHandel mpojo = new Gson().fromJson(error, ErrorHandel.class);
                            musicGeners.onError(mpojo.getMessage());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }
}
