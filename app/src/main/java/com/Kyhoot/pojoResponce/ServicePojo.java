package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * Created by ${3Embed} on 31/1/18.
 */

public class ServicePojo implements Serializable {
    private String bannerImageApp;

    private String ser_desc;

    private String bannerImageWeb;

    private double is_unit;

    private String currency;

    private String city_id;

    private String cat_id;

    private String sub_cat_id;

    private String unit;

    private String maxFees;

    private String _id;

    private String currencySymbol;

    private String ser_name;

    private String additionalPrice;

    private String minFees;

    private String quantity;

    private int maxquantity;

    private String plusOneCost;

    private int mandatorySelect;

    public int getMandatorySelect() {
        return mandatorySelect;
    }

    public int getTempQuant() {
        return tempQuant;
    }

    public void setTempQuant(int tempQuant) {
        this.tempQuant = tempQuant;
    }

    private int tempQuant=0;

    public String getBannerImageApp ()
    {
        return bannerImageApp;
    }

    public void setBannerImageApp (String bannerImageApp)
    {
        this.bannerImageApp = bannerImageApp;
    }

    public String getSer_desc ()
    {
        return ser_desc;
    }

    public void setSer_desc (String ser_desc)
    {
        this.ser_desc = ser_desc;
    }

    public String getBannerImageWeb ()
    {
        return bannerImageWeb;
    }

    public void setBannerImageWeb (String bannerImageWeb)
    {
        this.bannerImageWeb = bannerImageWeb;
    }

    public double getIs_unit ()
    {
        return is_unit;
    }

    public void setIs_unit (double is_unit)
    {
        this.is_unit = is_unit;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getCat_id ()
    {
        return cat_id;
    }

    public void setCat_id (String cat_id)
    {
        this.cat_id = cat_id;
    }

    public String getSub_cat_id ()
    {
        return sub_cat_id;
    }

    public void setSub_cat_id (String sub_cat_id)
    {
        this.sub_cat_id = sub_cat_id;
    }

    public String getUnit ()
    {
        return unit;
    }

    public void setUnit (String unit)
    {
        this.unit = unit;
    }

    public String getMaxFees ()
    {
        return maxFees;
    }

    public void setMaxFees (String maxFees)
    {
        this.maxFees = maxFees;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getCurrencySymbol ()
    {
        return currencySymbol;
    }

    public void setCurrencySymbol (String currencySymbol)
    {
        this.currencySymbol = currencySymbol;
    }

    public String getSer_name ()
    {
        return ser_name;
    }

    public void setSer_name (String ser_name)
    {
        this.ser_name = ser_name;
    }

    public String getAdditionalPrice ()
    {
        return additionalPrice;
    }

    public void setAdditionalPrice (String additionalPrice)
    {
        this.additionalPrice = additionalPrice;
    }

    public String getMinFees ()
    {
        return minFees;
    }

    public void setMinFees (String minFees)
    {
        this.minFees = minFees;
    }

    public String getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (String quantity)
    {
        this.quantity = quantity;
    }

    public int getMaxquantity ()
    {
        return maxquantity;
    }

    public void setMaxquantity (int maxquantity)
    {
        this.maxquantity = maxquantity;
    }

    public String getPlusOneCost ()
    {
        return plusOneCost;
    }

    public void setPlusOneCost (String plusOneCost)
    {
        this.plusOneCost = plusOneCost;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bannerImageApp = "+bannerImageApp+", ser_desc = "+ser_desc+", bannerImageWeb = "+bannerImageWeb+", is_unit = "+is_unit+", currency = "+currency+", city_id = "+city_id+", cat_id = "+cat_id+", sub_cat_id = "+sub_cat_id+", unit = "+unit+", maxFees = "+maxFees+", _id = "+_id+", currencySymbol = "+currencySymbol+", ser_name = "+ser_name+", additionalPrice = "+additionalPrice+", minFees = "+minFees+", quantity = "+quantity+", maxquantity = "+maxquantity+", plusOneCost = "+plusOneCost+"]";
    }

    public String getQuanity() {
        return quantity;
    }
}
