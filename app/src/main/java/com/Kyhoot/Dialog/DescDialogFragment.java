package com.Kyhoot.Dialog;


import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;

public class DescDialogFragment extends DialogFragment implements View.OnClickListener {

    private double price;
    private String serviceName,description;
    SharedPrefs prefs;
    public DescDialogFragment(){

    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        assert args != null;
        price=args.getDouble("Price");
        serviceName=args.getString("ServiceName");
        description=args.getString("Description");
        super.setArguments(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,R.style.DialogTransparent);
        prefs = new SharedPrefs(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_desc_dialog, container, false);
        initializeViews(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            getDialog().getWindow().setElevation(4);
        }
        return view;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    private void initializeViews(View view) {
        ImageView iv_closebtn=view.findViewById(R.id.iv_closebtn);
        TextView tv_serviceName,tv_servicePrice,tv_serviceDesc;
        tv_serviceDesc=view.findViewById(R.id.tv_serviceDesc);
        tv_servicePrice=view.findViewById(R.id.tv_servicePrice);
        tv_serviceName=view.findViewById(R.id.tv_serviceName);
        tv_serviceName.setTypeface(AppTypeface.getInstance(getActivity()).getHind_semiBold());
        tv_servicePrice.setTypeface(AppTypeface.getInstance(getActivity()).getHind_semiBold());
        tv_serviceDesc.setTypeface(AppTypeface.getInstance(getActivity()).getHind_regular());
        iv_closebtn.setOnClickListener(this);
        tv_serviceDesc.setText(description);
        tv_serviceName.setText(serviceName);
        Utilities.setAmtOnRecept(price,tv_servicePrice,prefs.getCurrencySymbol());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_closebtn:
                dismiss();
                break;
        }
    }
}
