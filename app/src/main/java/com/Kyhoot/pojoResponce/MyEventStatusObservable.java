package com.Kyhoot.pojoResponce;

import android.util.Log;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;


/**
 * <h>MyEventStatusObservable</h>
 * Created by Ali on 11/1/2017.
 */
//
public class MyEventStatusObservable extends Observable<MyEventStatus> {

    private static MyEventStatusObservable observebleClass;
    private Observer<? super MyEventStatus> observer;

    private ArrayList<Observer<? super MyEventStatus>> myListObserver = new ArrayList<>();

    public static MyEventStatusObservable getInstance() {
        if (observebleClass == null) {
            observebleClass = new MyEventStatusObservable();

            return observebleClass;
        } else {
            return observebleClass;
        }
    }

    @Override
    protected void subscribeActual(Observer<? super MyEventStatus> observer) {
        this.observer = observer;
        Log.w("TAG", "emitsubscribeActual: "+myListObserver.size());
        if(!myListObserver.contains(observer))
        {
            myListObserver.add(observer);
            Log.w("TAG", "emitsubscribeActual1: "+myListObserver.size());
        }
    }

    public void removeObserver(Observer<? super MyEventStatus> observer)
    {
        Log.w("TAG", "emitremoveObserver: "+myListObserver.size());
        if(myListObserver.contains(observer))
        {
            myListObserver.remove(observer);
            Log.w("TAG", "emitremoveObserver1: "+myListObserver.size());
        }

    }
    public void emit(MyEventStatus myEventStatus)
    {
      //  observebleClass.replay();
        Log.w("TAG", "emit: "+myListObserver.size());
        for (int i =0 ;i<myListObserver.size();i++)
        {
            Observer<?super MyEventStatus> tempobserver = myListObserver.get(i);
            tempobserver.onNext(myEventStatus);
            tempobserver.onComplete();
        }
       /* observer.onNext(myEventStatus);
        observer.onComplete();*/

    }
}
