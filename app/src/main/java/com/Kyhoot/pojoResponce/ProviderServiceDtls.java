package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>ProviderServiceDtls</h>
 * these are the services provided by the providers
 * Created by 3Embed on 10/17/2017.
 */

public class ProviderServiceDtls implements Serializable {
    /*"id":"59c11789b0554924860bb2c8",
"name":"15",
"unit":"mint",
"price":20*/
    private String id, name, unit;
    private int price;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public int getPrice() {
        return price;
    }
}
