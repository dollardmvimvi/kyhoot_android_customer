package com.Kyhoot.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.utilities.AppTypeface;

/**
 * <h>Splash_ViewPagerAdapter</h>
 * Created by @Ali on 8/1/2017.
 */

public class Splash_ViewPagerAdapter extends PagerAdapter {

    AppTypeface appTypeface;
    private Context mcontext;
    private int[] layouts;
    public Splash_ViewPagerAdapter(Context mcontext, int[] layouts) {

        this.mcontext = mcontext;
        this.layouts = layouts;
        appTypeface = AppTypeface.getInstance(mcontext);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(layouts[position], container, false);
        TextView tvWelcome1 = view.findViewById(R.id.tvWelcome1);
        TextView tvWelcomeTitle1 = view.findViewById(R.id.tvWelcomeTitle1);
        TextView tvWelcome2 = view.findViewById(R.id.tvWelcome2);
        TextView tvWelcomeTitle2 = view.findViewById(R.id.tvWelcomeTitle2);
        TextView tvWelcome3 = view.findViewById(R.id.tvWelcome3);
        TextView tvWelcomeTitle3 = view.findViewById(R.id.tvWelcomeTitle3);
        TextView tvWelcome4 = view.findViewById(R.id.tvWelcome4);
        TextView tvWelcomeTitle4 = view.findViewById(R.id.tvWelcomeTitle4);
        if (tvWelcome1 != null) {
            tvWelcome1.setTypeface(appTypeface.getHind_regular());
            tvWelcomeTitle1.setTypeface(appTypeface.getHind_semiBold());
        }


        if (tvWelcome2 != null) {
            tvWelcome2.setTypeface(appTypeface.getHind_regular());
            tvWelcomeTitle2.setTypeface(appTypeface.getHind_semiBold());
        }

        if (tvWelcome3 != null) {
            tvWelcome3.setTypeface(appTypeface.getHind_regular());
            tvWelcomeTitle3.setTypeface(appTypeface.getHind_semiBold());
        }

        if (tvWelcome4 != null) {
            tvWelcome4.setTypeface(appTypeface.getHind_regular());
            tvWelcomeTitle4.setTypeface(appTypeface.getHind_semiBold());
        }


        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
