package com.Kyhoot.interfaceMgr;

/**
 * Created by ${3Embed} on 9/4/18.
 */
public interface CategoryUpdateInterface {
    public void onCategoryChanged();
}
