package com.Kyhoot.main;

//import com.Kyhoot.R;


//public class RateProviderActivity extends AppCompatActivity implements RateProviderInterface,AccessTokenCallback {
/*

    private String TAG = RateProviderActivity.class.getName();
    private long bId;
    private TextView tvDate, tvPrice;
    private AppTypeface apptypeFace;
    private EditText etReview;
    private RatingBar ratingStar;
    private SharedPrefs sprefs;
    private RateProviderController rateProviderController;
    private TextView tvCustomerName, tvRatingDetails;
    private boolean isImageDrawn = false;
    private double width, height;
    private ImageView ivRateProProfile;
    private ProgressBar progressBar;
    private Gson gson;
    public InvoiceDetails.InvoiceData data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_customer);
        sprefs = new SharedPrefs(this);
        gson =new Gson();
        apptypeFace = AppTypeface.getInstance(this);
        Log.d(TAG, "onCreateRATEPROVIDER: ");
        initializeView();
        setTypeFace();
        initializeToolBar();
    }

    private void initializeToolBar() {


        Toolbar login_tool = findViewById(R.id.ratingToolBar);
        setSupportActionBar(login_tool);
        TextView rateTitle1 = findViewById(R.id.rateTitle1);
        tvDate = findViewById(R.id.tvDate);
        getSupportActionBar().setTitle("");
        rateTitle1.setTypeface(apptypeFace.getHind_regular());
        tvDate.setTypeface(apptypeFace.getHind_medium());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTypeFace() {
        TextView tvPriceLabel = findViewById(R.id.tvPriceLabel);
        tvRatingDetails = findViewById(R.id.tvRatingDetails);
        TextView tvSubmit = findViewById(R.id.tvSubmit);
        etReview = findViewById(R.id.etReview);
        ratingStar = findViewById(R.id.ratingStar);
        tvPriceLabel.setTypeface(apptypeFace.getHind_regular());
        tvRatingDetails.setTypeface(apptypeFace.getHind_regular());
        etReview.setTypeface(apptypeFace.getHind_regular());
        tvSubmit.setTypeface(apptypeFace.getHind_semiBold());
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowProgress();
                rateProviderController.updateReview(bId, ratingStar.getRating(), etReview.getText().toString());
            }
        });

        rateProviderController.iNvoiceDetails(bId);
    }


    private void initializeView() {
        rateProviderController = new RateProviderController(sprefs, this);
        ivRateProProfile = findViewById(R.id.ivRateProProfile);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvPrice = findViewById(R.id.tvPrice);
        progressBar = findViewById(R.id.progressBar);
        tvPrice.setTypeface(apptypeFace.getHind_bold());
        tvCustomerName.setTypeface(apptypeFace.getHind_semiBold());
        double size[] = Scaler.getScalingFactor(this);
        width = 90 * size[0];
        height = 90 * size[1];
        String proPic = "";
        if (getIntent().getExtras() != null) {

            proPic = getIntent().getStringExtra("PROIMAGE");
            bId = getIntent().getLongExtra("BID", 0);
            if (proPic != null && !proPic.equals("")) {
                Picasso.with(this)
                        .load(proPic)
                        .resize((int) width, (int) height)
                        .transform(new CircleTransform())
                        .into(ivRateProProfile);
                isImageDrawn = true;
            }

        }
    }


    @Override
    public void onRateProviderSuccess() {
        VariableConstant.isSplashCalled = true;
        DataBaseHelper db = new DataBaseHelper(this);
        db.deleteChat(bId);
        if(!VariableConstant.isHomeFragment) {
            VariableConstant.isHomeFragment = false;
            VariableConstant.isConfirmBook = false;
            Intent intent = new Intent(RateProviderActivity.this, MenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onRateProviderError(String headerError, String errorMsg) {
        try{
            if(headerError.equals("440")){
                Utilities.getAccessToken(errorMsg,RateProviderActivity.this);
            }else{
                Toast.makeText(this,errorMsg,Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSessionExpired(String sessionExp) {
        Utilities.setMAnagerWithBID(this, sprefs,sessionExp);
    }

    @Override
    public void onShowProgress() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


    }

    @Override
    public void onHideProgress() {

        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    @Override
    public void onGetInvoiceDetails(InvoiceDetails.InvoiceData invoiceData)
    {
        Log.d(TAG, "onGetInvoiceDetails: "+invoiceData);
        data = invoiceData;
        try
        {
            timeMethod(tvDate, invoiceData.getBookingRequestedFor());
            Utilities.setAmtOnRecept(invoiceData.getAccounting().getTotal(),tvPrice,sprefs.getCurrencySymbol());
            //String totalAmt = sprefs.getCurrencySymbol() + " " + invoiceData.getAccounting().getAmount();
            //  tvPrice.setText(totalAmt);
            String name = invoiceData.getProviderData().getFirstName() + " " + invoiceData.getProviderData().getLastName();
            tvCustomerName.setText(name);
            if (!isImageDrawn) {
                String url = invoiceData.getProviderData().getProfilePic();
                if (!url.equals("")) {
                    Picasso.with(this)
                            .load(url)
                            .resize((int) width, (int) height)
                            .transform(new CircleTransform())
                            .into(ivRateProProfile);
                }
            }

            tvRatingDetails.setOnClickListener(detailsClicked(invoiceData, invoiceData.getSignURL(), invoiceData.getAccounting().getDiscount()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }



    private void timeMethod(TextView tvDate, long bookingRequestedFor) {
        try {


            Log.d("TAGTIME", " expireTime " + bookingRequestedFor);
            Date date = new Date(bookingRequestedFor * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            String formattedDate = sdf.format(date);

            String formattedDated = Utilities.getFormattedDate(date);
            String dat1[] = formattedDated.split(",");

            tvDate.setText(dat1[0]);
           *//* String splitDate[] = formattedDate.split(" ");
            String dat[] = splitDate[0].split("/");
            String month = Utilities.MonthName(Integer.parseInt(dat[0]));
            String day = Utilities.getDayNumberSuffix(Integer.parseInt(dat[1]));
            String year = dat[2];
            String bookinDate = dat[1] + day + " " + month + " " + year;*//*
           *//* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
                tvDate.setText(Html.fromHtml(bookinDate, Html.FROM_HTML_MODE_LEGACY));
            else
                tvDate.setText(Html.fromHtml(bookinDate));*//*
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }

    private View.OnClickListener detailsClicked(final InvoiceDetails.InvoiceData service, final String signURL, final double discount) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rateProviderController.openDialog(RateProviderActivity.this, apptypeFace, service, signURL, discount);

            }
        };
    }

    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();
    }

    @Override
    public void onSessionExpired() {
        //Utilities.setMAnagerWithBID();
    }

    @Override
    public void onSuccessAccessToken(String data) {

    }

    @Override
    public void onFailureAccessToken(String headerError, String server_issue) {

    }*/
//}
