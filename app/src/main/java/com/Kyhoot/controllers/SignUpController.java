package com.Kyhoot.controllers;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import com.facebook.CallbackManager;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.EmailPhoneValidtn;
import com.Kyhoot.interfaceMgr.FaceBookSignupResponce;
import com.Kyhoot.interfaceMgr.RegisterOTPResponce;
import com.Kyhoot.interfaceMgr.ValidateEmail;
import com.Kyhoot.models.SignupModel;
import com.Kyhoot.pojoResponce.CheckFlagForValidity;
import com.Kyhoot.pojoResponce.LoginPojo;
import com.Kyhoot.utilities.FacebookLogin;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.Validator;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * <h>SignUpController</h>
 * Signup controllers communicate with the User and the server
 * Created by Ali on 8/25/2017.
 */

public class SignUpController extends CheckFlagForValidity implements EmailPhoneValidtn
{
    private Activity mcontext;
    private SignupModel signupModel;
    private Validator validate;
    private CheckFlagForValidity checklag;
    private RegisterOTPResponce regOtpResp;
    private JSONObject pojo;
    private SharedPrefs spref;

    public SignUpController(Activity mcontext,RegisterOTPResponce regOtpResp)
    {
       this.mcontext = mcontext;
        this.regOtpResp = regOtpResp;
        initializeObj();
    }

    private void initializeObj()
    {
        signupModel = new SignupModel();
        validate = new Validator();
        checklag = new CheckFlagForValidity();
    }

    public void facebook(CallbackManager callbackManager, final FaceBookSignupResponce fbResp)
    {
        FacebookLogin fbLogin = new FacebookLogin(mcontext);
        fbLogin.refreshToken();
        fbLogin.facebook_login(callbackManager, fbLogin.createFacebook_requestData(), new FacebookLogin.Facebook_callback() {
            @Override
            public void sucess(JSONObject json)
            {
                Log.d("SIGNUPCONTROLLER", "sucess: facebook response"+json);
                try {
                    String fb_id =  json.getString("id");
                    String fb_emailId =  json.getString("email");
                    String fb_birthDay = json.optString("birthday");
                    String  Fb_firstName = json.optString("first_name");
                    String fb_lastName = json.optString("last_name");
                    Log.d("SIGNUPCONTROLLER", "sucess: "+fb_birthDay);
                    if(fb_birthDay.equals("")){
                        fbResp.onsuccess(Fb_firstName, fb_lastName, fb_emailId, "", fb_id);
                    }else{
                        fbResp.onsuccess(Fb_firstName, fb_lastName, fb_emailId, signupModel.getForMatedDOB(mcontext, fb_birthDay), fb_id);
                    }

                } catch (JSONException e)
                {
                    fbResp.onError(e.getMessage());
                    try
                    {
                        String fb_id =  json.getString("id");
                        String fb_birthDay = json.optString("birthday");
                        String  Fb_firstName = json.optString("first_name");
                        String fb_lastName = json.optString("last_name");
                        fbResp.onsuccess(Fb_firstName, fb_lastName, "", signupModel.getForMatedDOB(mcontext, fb_birthDay), fb_id);
                    }catch (JSONException e1)
                    {
                        fbResp.onError(e1.getMessage());
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                }
            }
            @Override
            public void error(String error)
            {
                fbResp.onError(error);
            }

            @Override
            public void cancel(String cancel)
            {
                fbResp.onError(cancel);
            }
        });
    }

    public void getOTPVerificationCode(JSONObject pojo, SharedPrefs spref) throws JSONException {
        this.pojo = pojo;
        this.spref = spref;
        Log.d("TAG", "getOTPVerificationCodeN: " + checklag.isnFlg() + " Em " + checklag.iseFlg()
                + " pas " + checklag.ispFlg() + " mob " + checklag.ismFlg() + " Dob " + checklag.isDobFlg());
        if(!checklag.isnFlg())
        {
            String name = pojo.get("firstName").toString();
            if (checkNameValidation(name)) {
                checkEmailFlag(pojo, spref);
            } else
                regOtpResp.onValidationError(1);
        }
        else if(!checklag.iseFlg())
        {
            checkEmailFlag(pojo, spref);
            //  regOtpResp.onValidationError(2);
        }
        else if(!checklag.ispFlg())
        {
            // checkEmailFlag(pojo,  spref);
            checkPasFlag(pojo, spref);
            //regOtpResp.onValidationError(3);
        }
        else if(!checklag.ismFlg())
        {
            //checkEmailFlag(pojo,  spref);
            checkMobFlag(pojo, spref);
            //regOtpResp.onValidationError(4);
        }
        else if (!checklag.isDobFlg())
        {
            checkPasDobFlag(pojo, spref);
            // regOtpResp.onValidationError(5);
        }
        else
        {
            signupModel.signupService(pojo, regOtpResp, spref, mcontext);
        }

    }

    private void checkEmailFlag(JSONObject pojo, SharedPrefs spref) throws JSONException {
        this.pojo = pojo;
        this.spref = spref;
        if (!checklag.iseFlg()) {
            String email = pojo.get("email").toString();
            if (checkEmailValidation(email)) {
                regOtpResp.onEmailLocalSuccess(email);
            } else
                regOtpResp.onValidationError(2);
        } else {
            checkPasFlag(pojo, spref);
        }
    }

    private void checkPasFlag(JSONObject pojo, SharedPrefs spref) throws JSONException {
        this.pojo = pojo;
        this.spref = spref;
        if (!checklag.ispFlg()) {
            String password = pojo.get("password").toString();

            if (checkPasswordValidation(password)) {
                checkMobFlag(pojo, spref);
            } else
                regOtpResp.onValidationError(3);

        } else {
            checkMobFlag(pojo, spref);
        }
    }

    private void checkMobFlag(JSONObject pojo, SharedPrefs spref) throws JSONException {

        this.pojo = pojo;
        this.spref = spref;
        if (!checklag.ismFlg()) {
            String phone = pojo.get("phone").toString();
            String countryCode = pojo.get("countryCode").toString();
            if (checMobilevalidation(countryCode,phone)) {
                regOtpResp.onMobLocalSuccess(phone);
            } else
                regOtpResp.onValidationError(4);
        } else {
            checkPasDobFlag(pojo, spref);
        }
    }

    private void checkPasDobFlag(JSONObject pojo, SharedPrefs spref) throws JSONException {
        this.pojo = pojo;
        this.spref = spref;


        if (!checklag.isDobFlg()) {
            // String dateOfB = pojo.get("dateOfBirth").toString();
            if (checkDobValidity()) {
                //   checkMobFlag(pojo,  spref);
                signupModel.signupService(pojo, regOtpResp, spref, mcontext);
            } else
                regOtpResp.onValidationError(5);
        } else {
            signupModel.signupService(pojo, regOtpResp, spref, mcontext);
        }
    }

    public boolean checkNameValidation(String trim)
    {
        if (trim.length() < 1) {
            regOtpResp.onNameValidity(mcontext.getResources().getString(R.string.first_name_mandatory));
            checklag.setnFlg(false);
        }
        else {
            checklag.setnFlg(true);
        }
        return checklag.isnFlg();
    }

    public boolean checkEmailValidation(String emailtrim)
    {
        if (emailtrim.length() < 1) {

            regOtpResp.onEmailLocalValidity(mcontext.getResources().getString(R.string.email_mandatory));

            checklag.seteFlg(false);
        } else if (!validate.emailValidation(emailtrim)) {
            regOtpResp.onEmailLocalValidity(mcontext.getResources().getString(R.string.email_invalid));
            checklag.seteFlg(false);
        } else {
           /* til_reg_eml.setError(null);
            til_reg_eml.setErrorEnabled(false);*/
            checklag.seteFlg(true);

        }
        return  checklag.iseFlg();
    }
    public boolean checkPasswordValidation(String passsword)
    {
        if (passsword.isEmpty()) {
            regOtpResp.onPasswordLocalValidity(mcontext.getResources().getString(R.string.password_mandatory));
            checklag.setpFlg(false);
        } else if (!validate.passStatus(passsword)) {
            regOtpResp.onPasswordLocalValidity(mcontext.getResources().getString(R.string.passwordcontain));
            checklag.setpFlg(false);
        } else {
            checklag.setpFlg(true);
        }
        return checklag.ispFlg();
    }
    public boolean checMobilevalidation(String countryCode,String mobileTrim)
    {


        boolean isValid=false;
        String phoneNumberE164Format = countryCode.concat(mobileTrim);
        PhoneNumberUtil phoneNumberUtil=PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumberProto = null;
        try {
            phoneNumberProto = phoneNumberUtil.parse(phoneNumberE164Format, null);
        } catch (NumberParseException e) {
            e.printStackTrace();
            checklag.setmFlg(isValid);
            regOtpResp.onPhoneLocalValidity(mcontext.getResources().getString(R.string.mobile_invalid));
            return isValid;
        }
        isValid = phoneNumberUtil.isValidNumber(phoneNumberProto); // returns true if valid
        if(!isValid){
            regOtpResp.onPhoneLocalValidity(mcontext.getResources().getString(R.string.mobile_invalid));
        }
        checklag.setmFlg(isValid);
        return  isValid;
    }
    public void checkEmail(String email, boolean isSignup)
    {
        signupModel.emailValidationRequest(email, this, isSignup);
    }

    public void checkMobilNumbr(String mobileNumbr, String countryCode, boolean isSignUp)
    {
        signupModel.mobileNumberVerifcation(mobileNumbr, countryCode, this, isSignUp);
    }
    @Override
    public void onEmailValid(boolean isValid, String msg, boolean isSignUp)
    {
       if(!isValid)
       {
            regOtpResp.onEmailValidate(msg);
            checklag.seteFlg(false);
       }
       else
       {
           checklag.seteFlg(true);

           //if Valid then remove the error from Email field
           if (checklag.iseFlg())
                regOtpResp.onEmailValidFromAPi();
           
           regOtpResp.onOTPSuccess(msg);
           if (isSignUp) {
               try {
                   checkPasFlag(pojo, spref);
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }
    }
    @Override
    public void onPhoneValid(boolean isValid, String msg, boolean isSignUp) {
        if(!isValid)
        {
            regOtpResp.onPhoneValidate(msg);
            checklag.setmFlg(false);
        }
        else
        {
            checklag.setmFlg(true);
            regOtpResp.onOTPSuccess(msg);
            if (isSignUp) {
                try {
                    checkPasDobFlag(pojo, spref);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onReferralCodeValid(boolean isValid, String msg) {
        if(!isValid)
        {
            //checklag.setRefFlg(false);
            regOtpResp.onReferralError(msg);
        }
        else
        {
            //checklag.setRefFlg(true);
            regOtpResp.onReferralValidate(msg);
        }

    }

    /**
     * <h1>openDate_Picker</h1>
     * Method is used to open a date picker and Pict a date.
     * <p>
     *     This method open a Date Picker dialog by DatePickerDialog object.
     *     Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime())}.
     *     Also listening the response From the DatePicker and setting the Date to the Given Edit text.
     * </p>
     * @param chosedate contains the Textview reference to which data has to set.
     * @param til_reg_dateofBirth TextInputLayout of DOB
     */
    public void openCalendr(final EditText chosedate,final TextInputLayout til_reg_dateofBirth)
    {
        final Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mcontext, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newCalendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                VariableConstant.dateOB = dateFormatter.format(newCalendar.getTime());
                String title = DateUtils.formatDateTime(mcontext,
                        newCalendar.getTimeInMillis(),
                        DateUtils.FORMAT_SHOW_DATE
                                | DateUtils.FORMAT_SHOW_WEEKDAY
                                | DateUtils.FORMAT_SHOW_YEAR
                                | DateUtils.FORMAT_ABBREV_MONTH
                                | DateUtils.FORMAT_ABBREV_WEEKDAY);
                chosedate.setText(title);
                checkDobValidity();

            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        newCalendar.add(Calendar.YEAR, -18);
        fromDatePickerDialog.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());
        fromDatePickerDialog.show();

    }//End of selecting the date

    /**
     * <h1>checkDob</h1>
     * checking the required mininum age and return according
     * @param userDob user's input Date of birth
     * @param minimumAge required minimum Age
     * @return true if the DoB is more than minimum Age
     */
    private boolean checkDob(String userDob, int minimumAge)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        try {
            Date parseddate = sdf.parse(userDob);
            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.DAY_OF_YEAR, -minimumAge);
            return parseddate.before(c2.getTime());

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public boolean checkDobValidity()
    {
        if(checkDob(VariableConstant.dateOB,18))
        {
            checklag.setDobFlg(true);
            return true;
        }else
        {
            checklag.setDobFlg(false);
            regOtpResp.onDateOBirthLocalValidity(mcontext.getString(R.string.dateofbirtherror));
            return false;
        }
    }

    public String setDateOfBirth(String dateOB) {
        String dateOfBrth;
        String day, month, year;
        if (dateOB.contains(",")) {
            String splitdate[] = dateOB.split(",");
            day = Utilities.getDayNumberSuffix(Integer.parseInt(splitdate[0].split(" ")[1]));
            month = splitdate[0].split(" ")[0];
            year = dateOB.split(" ")[2];
            dateOfBrth = splitdate[0].split(" ")[1] + day + " " + month + " " + year;
            return dateOfBrth;
        } else if (dateOB.contains(".")) {
            String splitDate[] = dateOB.split(".");
            day = Utilities.getDayNumberSuffix(Integer.parseInt(splitDate[0]));
            month = splitDate[1];
            year = splitDate[2];
            dateOfBrth = splitDate[0] + day + " " + month + " " + year;
            return dateOfBrth;
        } else {
            String splitDate[] = dateOB.split(" ");
            day = Utilities.getDayNumberSuffix(Integer.parseInt(splitDate[0]));
            month = splitDate[1];
            year = splitDate[2];
            dateOfBrth = splitDate[0] + day + " " + month + " " + year;
            return dateOfBrth;
        }

    }

    public void checkreferralcode(String s) {
        signupModel.refferalCodeValidation(s,this);
    }

    public void checkEmailValidationFacebook(String email, ValidateEmail validateEmail) {
        signupModel.emailValidationRequestFacebook(email,validateEmail);
    }

    public void doLogin(LoginPojo loginPojo, ValidateEmail validateEmail,SharedPrefs sharedPrefs) {
        signupModel.signinService(loginPojo,validateEmail,sharedPrefs);
    }
}
