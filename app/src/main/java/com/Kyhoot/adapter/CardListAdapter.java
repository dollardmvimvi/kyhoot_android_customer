package com.Kyhoot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.SelectedCardInfoInterface;
import com.Kyhoot.pojoResponce.CardGetData;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import java.util.ArrayList;
/**
 * <h>CardListAdapter</h>
 * Created by Ali on 3/12/2018.
 */

public class CardListAdapter extends RecyclerView.Adapter
{
    private Context mContext;
    private ArrayList<CardGetData> cardItem;
    private int selectedPosition = 0;
    String selectedMTD;
    private SelectedCardInfoInterface.SelectedView selectedView;



    public CardListAdapter(Context mContext, ArrayList<CardGetData> cardItem, SelectedCardInfoInterface.SelectedView selectedView, String selectedMTD) {
        this.mContext = mContext;
        this.cardItem = cardItem;
        this.selectedView = selectedView;
        this.selectedMTD = selectedMTD;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(mContext).inflate(R.layout.card_item_list,parent,false);
        return new ViewHolderNormal(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        Log.d("TAG", "onBindViewHolder: "+position+" holder "+holder.getItemViewType());
        Log.d("TAG", "onBindViewHolder: "+cardItem.get(position).getLast4());
        ViewHolderNormal vHolderNormal = (ViewHolderNormal) holder;
        String cardNum = mContext.getString(R.string.card_stars)+" "+cardItem.get(position).getLast4();
        vHolderNormal.cardInfo.setText(cardNum);
        if(selectedPosition == position /*&& selectedMTD.equalsIgnoreCase("card")*/)
        {
            vHolderNormal.cardInfo.setSelected(true);
            vHolderNormal.tvSelectedCardPay.setVisibility(View.VISIBLE);
        }else
        {
            vHolderNormal.cardInfo.setSelected(false);
            vHolderNormal.tvSelectedCardPay.setVisibility(View.GONE);
        }

        vHolderNormal.ivCardImage.setImageBitmap(Utilities.setCreditCardLogo(cardItem.get(position).getBrand(),mContext));
    }

    @Override
    public int getItemCount() {
        Log.d("TAG", "getItemCount: "+cardItem.size());
        return cardItem.size();
    }


    class ViewHolderNormal extends RecyclerView.ViewHolder
    {
        ImageView ivCardImage;
        TextView cardInfo;
        TextView tvSelectedCardPay;
        private AppTypeface appTypeface;
        ViewHolderNormal(View itemView) {
            super(itemView);
            ivCardImage=itemView.findViewById(R.id.ivCardImage);
            cardInfo=itemView.findViewById(R.id.cardInfo);
            tvSelectedCardPay=itemView.findViewById(R.id.tvSelectedCardPay);
            appTypeface = AppTypeface.getInstance(mContext);
            cardInfo.setTypeface(appTypeface.getHind_regular());
            tvSelectedCardPay.setTypeface(appTypeface.getHind_semiBold());

            cardInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedView.onVisibilitySet();
                    notifyItemChanged(selectedPosition);
                    selectedPosition = getAdapterPosition();
                    notifyItemChanged(selectedPosition);
                }
            });

            tvSelectedCardPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectedView.onToBackIntent(getAdapterPosition());
                }
            });
        }
    }

    public void onAdapterChanged(int Position)
    {
        notifyItemChanged(selectedPosition);
        selectedPosition = Position;
        notifyItemChanged(selectedPosition);
    }

}
