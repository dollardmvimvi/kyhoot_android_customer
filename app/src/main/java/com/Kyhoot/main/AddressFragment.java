package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.Address_Adaptor;
import com.Kyhoot.controllers.AddressController;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.pojoResponce.AddressPojo;
import com.Kyhoot.pojoResponce.ValidatorPojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import java.util.ArrayList;

/**
 * <h>AddressFragment</h>
 * <p>
 *     this class manages the address
 *     @see UtilInterFace
 * Created by 3Embed on 30/8/16.
 * </p>
 *
 */
public class AddressFragment extends Fragment implements UtilInterFace
{

    ListView lvAddress;
    TextView tvAddAddress, tvNoAdressAdded;
    Address_Adaptor addressAdaptor;
    ProgressDialog pDialog;
    SharedPrefs manager;
    AlertProgress alertDialog;
    ImageView iv_address_placeholder;
    Gson gson;
    AddressController adresControlrs;
    private ArrayList<AddressPojo.AddressData> alSavingAddress = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.addresslayout,container,false);
        intilizeVariable(view);
        initializeObj();
        return view;
    }

    /**
     * <h>initializeObj</h>
     * this helps in initializing the obj
     */
    private void initializeObj() {
        manager = new SharedPrefs(getActivity());
        gson = new Gson();
        alertDialog = new AlertProgress(getActivity());
        adresControlrs = new AddressController(this, manager);
        pDialog = alertDialog.getProgressDialog(getResources().getString(R.string.wait));
        pDialog.setCancelable(false);
    }

    /*
    initializing the view
     */
    private void intilizeVariable(View view)
    {
        AppTypeface appTypeface = AppTypeface.getInstance(getActivity());
        lvAddress = view.findViewById(R.id.addresslist);
        tvAddAddress = view.findViewById(R.id.tvAddaddress);
        tvNoAdressAdded = view.findViewById(R.id.noadressadded);
        iv_address_placeholder = view.findViewById(R.id.iv_address_placeholder);
        tvAddAddress.setTypeface(appTypeface.getHind_semiBold());
        tvNoAdressAdded.setTypeface(appTypeface.getHind_regular());
        addressAdaptor = new Address_Adaptor(getActivity(), alSavingAddress, AddressFragment.this);
        tvAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CurrentLocation.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
            }
        });
        lvAddress.setAdapter(addressAdaptor);
    }
    @Override
    public void onResume() {
        super.onResume();
        if (VariableConstant.isAddressCalled)
            adresControlrs.callNetworkCheck(alertDialog, "GETADDRESS", "");
    }

    /**
     * <h1>deletefromdb</h1>
     * <p>
     * Delete the Address from the DataBase
     * @param positiondelete integer type delete the address at a particular position
     *                       </p>
     */
    public void deletefromdb(final int positiondelete)
    {
        alertDialog.alertonclick(getResources().getString(R.string.deleteaddress),getResources().getString(R.string.doyouwishtodelete),
                getResources().getString(R.string.yes),getResources().getString(R.string.no),
                new DialogInterfaceListner() {
                    @Override
                    public void dialogClick(boolean isClicked) {
                        if (isClicked)
                            adresControlrs.callNetworkCheck(alertDialog, "DELETE",
                                    alSavingAddress.get(positiondelete).get_id());
                    }
                });
    }

    @Override
    public void showProgress() {
        pDialog.show();
    }

    @Override
    public void hideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void onSuccess(String onSuccess) {
        adresControlrs.callNetworkCheck(alertDialog, "GETADDRESS", "");
    }

    @Override
    public void onError(String errorMsg) {
        tvNoAdressAdded.setVisibility(View.VISIBLE);
        iv_address_placeholder.setVisibility(View.VISIBLE);
        alSavingAddress.clear();
        addressAdaptor.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(ArrayList<AddressPojo.AddressData> ValidatorPojo) {
        alSavingAddress.clear();
        tvNoAdressAdded.setVisibility(View.GONE);
        iv_address_placeholder.setVisibility(View.GONE);
        alSavingAddress.addAll(ValidatorPojo);
        addressAdaptor.notifyDataSetChanged();
    }

    @Override
    public void onSuccessReview(ValidatorPojo reviewlist) {

    }
}
