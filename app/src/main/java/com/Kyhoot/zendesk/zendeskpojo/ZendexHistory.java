package com.Kyhoot.zendesk.zendeskpojo;

import java.io.Serializable;

/**
 * <h>ZendexHistory</h>
 * Created by Ali on 12/29/2017.
 */

public class ZendexHistory implements Serializable
{
    /*"data":{
"ticket_id":27,
"timeStamp":1514550475,
"subject":"errorGot",
"type":"open",
"priority":"high",
"events":[]
}*/

    private ZendexHistoryData data;

    public ZendexHistoryData getData() {
        return data;
    }


}
