package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 4/4/18.
 */
public class PredefinedData {
    private String icon;

    private String _id;

    private String name;

    public String getIcon ()
    {
        return icon;
    }

    public void setIcon (String icon)
    {
        this.icon = icon;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [icon = "+icon+", _id = "+_id+", name = "+name+"]";
    }
}
