package com.Kyhoot.utilities;

/**
 * <h>MqttEvents</h>
 * Created by ALi on 27/09/17.
 */


public enum MqttEvents {


    Provider("provider"),
    JobStatus("jobStatus"),
    LiveTrack("liveTrack"),
    Message("message"),
    PresenceTopic("PresenceTopic");
    public String value;

    MqttEvents(String value) {
        this.value = value;
    }


}