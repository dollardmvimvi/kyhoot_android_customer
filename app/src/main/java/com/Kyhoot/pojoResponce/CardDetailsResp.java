package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h>CardDetailsResp</h>
 * Created by 3Embed on 10/20/2017.
 */

public class CardDetailsResp implements Serializable {

    private String message;
    private ArrayList<CardDetail> data;

    public String getMessage() {
        return message;
    }

    public ArrayList<CardDetail> getData() {
        return data;
    }

    public class CardDetail {
        /*"name":"",
"last4":"4242",
"expYear":2019,
"expMonth":1,
"id":"card_1BgX2WHH011Rur9WTLYk374B",
"brand":"Visa",
"funding":"credit",
"isDefault":true*/
        private String name;
        private String last4;
        private int expYear,expMonth;
        private String id;
        private String brand;
        private String funding;
        private boolean isDefault;

        public int getExpYear() {
            return expYear;
        }

        public int getExpMonth() {
            return expMonth;
        }

        public String getName() {
            return name;
        }

        public String getLast4() {
            return last4;
        }

        public String getId() {
            return id;
        }

        public String getBrand() {
            return brand;
        }

        public String getFunding() {
            return funding;
        }

        public boolean isDefault() {
            return isDefault;
        }
    }
}
