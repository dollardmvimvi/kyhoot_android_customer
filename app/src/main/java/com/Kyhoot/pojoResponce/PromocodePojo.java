package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 9/3/18.
 */

public class PromocodePojo {

    private String message;

    private PromocodeData data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public PromocodeData getData ()
    {
        return data;
    }

    public void setData (PromocodeData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }

}