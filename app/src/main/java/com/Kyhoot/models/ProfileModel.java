package com.Kyhoot.models;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.ProfileDataResponce;
import com.Kyhoot.interfaceMgr.ProfilePageCallback;
import com.Kyhoot.main.PasswordHelp;
import com.Kyhoot.pojoResponce.EmailValidator_pojo;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.pojoResponce.MyProfile_pojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>ProfileModel</h>
 * Profile Model which handle the service call Db
 * Created by ${Ali} on 8/18/2017.
 */

public class ProfileModel
{
    private Context mcontext;
    private AlertProgress alertProgress;
    private ProgressDialog pDialog;
    private SharedPrefs sessionManager;
    private Gson gson;
    private ProfileDataResponce prodataResponce;
    private ProfileDataResponce.proProfileSelectedMusic proProfileDataResponce;
    private ProfilePageCallback callback;

    public ProfileModel(Context mcontext)
    {
        this.mcontext = mcontext;
        initializPrgresDialg();
        initialzeObj();

    }

    private void initialzeObj()
    {
        gson = new Gson();
        sessionManager = new SharedPrefs(mcontext);
    }

    private void initializPrgresDialg()
    {
        alertProgress = new AlertProgress(mcontext);
        pDialog = alertProgress.getProgressDialog(mcontext.getString(R.string.wait));
        pDialog.setCancelable(false);

    }

    /**
     * checking  email already registered or not calling email validation service using okhttp
     * @param profilePicUrl picture url to be udated on the server
     */
    public void updateProfilePic(String profilePicUrl)
    {
        try {
            pDialog.setMessage(mcontext.getString(R.string.wait));
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ent_profile", profilePicUrl);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.UPDATEPROFILE, OkHttpConnection.Request_type.PUT, jsonObject, sessionManager.getSession(),VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result) {
                    Log.d("UPDATEPROFILE", "onSuccess: " + result + " headerResponse " + headerResponse);
                    EmailValidator_pojo emailValidator_pojo;

                    emailValidator_pojo = gson.fromJson(result, EmailValidator_pojo.class);
                    if (emailValidator_pojo.getErrFlag().equals("0") && emailValidator_pojo.getErrNum().equals("200")) {
                    } else {
                        Toast.makeText(mcontext, mcontext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(String headerError, String error) {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    Toast.makeText(mcontext, mcontext.getString(R.string.serverProblm), Toast.LENGTH_LONG).show();
                }
            });

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


    /**
     * This metod will check that user entered password is correct or not.
     * @param password .
     */
    public void checkCurrentPassAPI(String password) {
        try {
            final SharedPrefs sessionManager = new SharedPrefs(mcontext);

            pDialog.setMessage(mcontext.getString(R.string.wait));
            pDialog.show();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("password", password);

            OkHttpConnection.requestOkHttpConnection("", OkHttpConnection.Request_type.POST, jsonObject, sessionManager.getSession(),VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result)
                {
                    Log.d("password", "onSuccess: " + result + " headerResponse " + headerResponse);
                    
                    pDialog.dismiss();
                    if (result != null && !result.equals("")) {

                        Intent intent = new Intent(mcontext, PasswordHelp.class);//EditPasswordActivity.class);
                        intent.putExtra("comingFrom", "profile");
                        mcontext.startActivity(intent);

                    }
                }

                @Override
                public void onError(String headerError, String error)
                {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    try{
                        ErrorHandel errhandl = new Gson().fromJson(error, ErrorHandel.class);
                        errhandl.getMessage();
                        alertProgress.alertinfo(errhandl.getMessage());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to call the service from where we can get all the details of profile data.
     * @param profileDataRes its the object for the interface ProfileDataResponce
     *                       this is used to udate the ui of the profile fragment
     */
    public void profileService(ProfileDataResponce profileDataRes,ProfileDataResponce.proProfileSelectedMusic proProfileDataResponce)
    {
        this.proProfileDataResponce = proProfileDataResponce;
        pDialog.setMessage(mcontext.getString(R.string.wait));
        pDialog.show();
        prodataResponce = profileDataRes;
        JSONObject jsonObj = new JSONObject();
        if (alertProgress.isNetworkAvailable())
        {
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "profile/me", OkHttpConnection.Request_type.GET, jsonObj, sessionManager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String jsonResponse) {
                    Log.d("getprofile", "onSuccess: " + jsonResponse + " headerResponse " + headerResponse);
                    if (jsonResponse.contains("ï")) {
                        jsonResponse = jsonResponse.replace("ï", "");
                        jsonResponse = jsonResponse.replace("»", "");
                        jsonResponse = jsonResponse.replace("¿", "");
                    }
                    Log.d("TAG","Profile JSON DATA" + jsonResponse);
                    callProfileResponse(jsonResponse);
                }

                @Override
                public void onError(String headerError, String error) {
                    pDialog.dismiss();
                    if (headerError.equals("502"))
                        Toast.makeText(mcontext, mcontext.getString(R.string.serverProblm), Toast.LENGTH_LONG).show();
                    else {
                        try{
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    prodataResponce.sessionExpired(errorHandel.getMessage());
                                    break;
                                case "440":
                                    getAccesToken(errorHandel.getData());
                                    break;
                                default:
                                    Toast.makeText(mcontext, errorHandel.getMessage(), Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    Log.d("TAG", "HomeFrag JSON DATA Error" + error + " onAMp " + headerError);
                }
            });

        }
        else
        {
            Toast.makeText(mcontext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void getAccesToken(String data) {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken", OkHttpConnection.Request_type.GET,
                new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {
                        JSONObject jsonobj = null;
                        try {
                            jsonobj = new JSONObject(result);
                            sessionManager.setSession(jsonobj.getString("data"));
                            profileService(prodataResponce,proProfileDataResponce);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(String headerError, String error)
                    {

                    }
                });
    }

    /**
     * This method is used to perform the work, that we will do after getting the response from server.
     */
    private void callProfileResponse(String jsonResponse) {
        pDialog.dismiss();
        try {
            Gson gson = new Gson();
            MyProfile_pojo MyProfile_pojo = gson.fromJson(jsonResponse, MyProfile_pojo.class);
            sessionManager.setFirstname(MyProfile_pojo.getData().getName());
            sessionManager.setMobileNo(MyProfile_pojo.getData().getPhone());
            sessionManager.setEMail(MyProfile_pojo.getData().getEmail());
            sessionManager.setCountryCode(MyProfile_pojo.getData().getCountryCode());
            sessionManager.setImageUrl(MyProfile_pojo.getData().getProfilePic());
            sessionManager.setDateOBirth(MyProfile_pojo.getData().getDateOfBirth());
            sessionManager.setCountrySymbol(MyProfile_pojo.getData().getCountrySymbol());
            prodataResponce.successUpdateUi();


        }catch (Exception e){
            e.printStackTrace();
            if (pDialog != null)
                pDialog.dismiss();
        }
    }

    public void logoutService(ProfileDataResponce profileDataRes)
    {
        pDialog.setMessage(mcontext.getString(R.string.loggingOut));
        prodataResponce = profileDataRes;
        pDialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userType",1);
            OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "logout", OkHttpConnection.Request_type.POST, jsonObject, sessionManager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                @Override
                public void onSuccess(String headerResponse, String result) {
                    pDialog.dismiss();
                    Log.d("LOGOUT", "onSuccess: " + result + " headerResponse " + headerResponse);
                    prodataResponce.sessionExpired("");
                }

                @Override
                public void onError(String headerError, String error)
                {

                    Log.d("LOGOUT", "error: " + error);
                    pDialog.dismiss();
                    if (headerError.equals("498")) {
                        prodataResponce.sessionExpired("");
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            pDialog.dismiss();

        }
    }

    public void updateName(boolean b, String name, String lastName, ProfilePageCallback callback) {//, String etProflAbt, String etProflMusicGnr
        this.callback=callback;
        try {
            JSONObject json = new JSONObject();
            if (b) {
                /*if (!etProflAbt.equals("") && !etProflAbt.equals(sessionManager.getAboutMe())) {*/
                    json.put("firstName", name);
                    if (!lastName.equals(""))
                        json.put("lastName", lastName);
                    //json.put("about", etProflAbt);
                    /*if(!etProflMusicGnr.equals(""))
                    {
                        json.put("preferredGenres", etProflMusicGnr);
                        sessionManager.setMusicGen(etProflMusicGnr);
                    }*/

                    sessionManager.setFirstname(name);
                    sessionManager.setLastname(lastName);
                    //sessionManager.setAboutMe(etProflAbt);
                    updateProfile(json);
                /*} else {
                    json.put("firstName", name);
                    if (!lastName.equals(""))
                        json.put("lastName", lastName);
                    *//*if(!etProflMusicGnr.equals(""))
                    {
                        json.put("preferredGenres", etProflMusicGnr);
                        sessionManager.setMusicGen(etProflMusicGnr);
                    }*//*
                    sessionManager.setFirstname(name);
                    sessionManager.setLastname(lastName);
                    updateProfile(json);
                }*/

            //} else {
               /* if (!etProflAbt.equals("") && !etProflAbt.equals(sessionManager.getAboutMe())) {
                    json.put("about", etProflAbt);
                    sessionManager.setAboutMe(etProflAbt);

                    if(!etProflMusicGnr.equals(""))
                    {
                        json.put("preferredGenres", etProflMusicGnr);
                        sessionManager.setMusicGen(etProflMusicGnr);
                    }
                    updateProfile(json);
                }
                else
                {
                    if(!etProflMusicGnr.equals("") && !etProflMusicGnr.equals(sessionManager.getMusicGen()))
                    {
                        json.put("preferredGenres", etProflMusicGnr);
                        sessionManager.setMusicGen(etProflMusicGnr);
                        updateProfile(json);
                    }

                }*/

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void updateProfile(JSONObject json) {
        Log.d("UpDATEPROFILE", "updateProfile: " + json);
        pDialog.setMessage(mcontext.getResources().getString(R.string.updating));
        pDialog.show();

        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "profile/me", OkHttpConnection.Request_type.PATCH
                , json, sessionManager.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerResponse, String result) {
                        pDialog.dismiss();
                        Log.d("UpDATEPROFILE", "onSuccess: " + result + " headerResponse " + headerResponse);
                        callback.onProfileUpdate();

                    }

                    @Override
                    public void onError(String headerError, String error) {
                        callback.onProfileUpdateFailed();
                        pDialog.dismiss();
                    }
                });
    }
}
