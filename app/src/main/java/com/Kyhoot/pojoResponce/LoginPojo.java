package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>LoginPojo</h>
 * Created by Ali on 9/14/2017.
 */

public class LoginPojo implements Serializable
{
    /*"errNum": 200,
  "errMsg": "Got The Details",
  "errFlag": 0,
  "data": {
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OWI5NTFjNDgxNjU0ZTQyZmJhZTk3MjIiLCJrZXkiOiJhY2MiLCJhY2Nlc3NDb2RlIjo5MzQzLCJpYXQiOjE1MDUzNzY2ODQsImV4cCI6MTUwNzk2ODY4NCwic3ViIjoic2xhdmUifQ.AKhtqulMiRoarHu82tymL9uGriLhbBFzJ8DnCWQzN10",
    "sid": "59b951c481654e42fbae9722",
    "email": "akbar@gmail.com",
    "firstName": "Akbar",
    "lastName": "",
    "phone": "8880675883",
    "countryCode": "+91",
    "profilePic": "",
    "paymentId": "",
    "currencyCode": "$",
    "PublishableKey": "pk_test_uJq2O46c1QWxAXQJnQe1dgI4"
    "cardDetail":{}*/
    private int errNum,errFlag;
    private String errMsg;
    private SignUpData data;





    public int getErrNum() {
        return errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public int getErrFlag() {
        return errFlag;
    }

    public SignUpData getData() {
        return data;
    }

    public void setData(SignUpData data) {
        this.data = data;
    }



}
