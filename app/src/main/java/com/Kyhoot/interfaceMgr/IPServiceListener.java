package com.Kyhoot.interfaceMgr;

import com.Kyhoot.pojoResponce.IPServicePojo;

/**
 * Created by ${3Embed} on 2/4/18.
 */
public interface IPServiceListener {
    public void onIPServiceSuccess(IPServicePojo s);
    public void onIPServiceFailed();

}
