package com.Kyhoot.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.CategoriesAndServicesInterface;
import com.Kyhoot.pojoResponce.ServicePojo;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Scaler;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 25/1/18.
 */

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ServiceViewHolder>{
    ArrayList<ServicePojo> servicesList;
    Context mContext;
    ServicePojo pojo;

    CategoriesAndServicesInterface callback;

    public ServicesAdapter(Context mContext, ArrayList<ServicePojo> servicesList, CategoriesAndServicesInterface callback) {
        this.servicesList = servicesList;
        this.mContext=mContext;
        this.callback=callback;
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.service_item,parent,false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {
        holder.serviceName.setText(servicesList.get(position).getSer_name());
        String serviceImg = servicesList.get(position).getBannerImageApp();
        if (!TextUtils.isEmpty(serviceImg)) {
            Log.e("TAG", "onBindViewHolder: service image "+serviceImg );
            Picasso.with(mContext)
                    .load(serviceImg)
                    .error(R.drawable.ic_launcher)
                    .placeholder(R.drawable.user_default)
                    .resize((int)holder.width,(int)holder.height)
                    .into(holder.iv_serviceImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            Log.e("TAG", "onSuccess: Load Image "+serviceImg);
                        }

                        @Override
                        public void onError() {
                            Log.e("TAG", "onError: Load Image "+serviceImg);
                        }

                    });
        }
        if (!TextUtils.isEmpty(VariableConstant.SURGE_PRICE)) {
            double service_price_surge=0.0;
            try {
                service_price_surge=Double.parseDouble(Utilities.doubleformate(Double.parseDouble(VariableConstant.SURGE_PRICE)*servicesList.get(position).getIs_unit()));
                Log.e("TAG_SURGE", "onBindViewHolder: SURGE PRICE"+service_price_surge);
            } catch (Exception e) {
                Toast.makeText(mContext, "Surge not applied", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            Utilities.setAmtOnRecept(service_price_surge,holder.totalCost, VariableConstant.CurrencySymbol);
        } else
            Utilities.setAmtOnRecept(servicesList.get(position).getIs_unit(),holder.totalCost, VariableConstant.CurrencySymbol);

        holder.tv_quantity.setText(servicesList.get(position).getTempQuant()+"");
        if(servicesList.get(position).getQuantity().equals("1"))
        {
            if(servicesList.get(position).getTempQuant()==0){
                holder.ll_add_service.setVisibility(View.GONE);
                holder.addbtn.setVisibility(View.VISIBLE);
                holder.iv_removeService.setVisibility(View.GONE);
            }else {
                holder.ll_add_service.setVisibility(View.VISIBLE);
                holder.addbtn.setVisibility(View.GONE);
                holder.iv_removeService.setVisibility(View.GONE);
            }
            //Showing additional price if it is plusOneCost
            double onePlusCost=Double.parseDouble(servicesList.get(position).getPlusOneCost());
            if(onePlusCost==0){
                holder.serviceDisc.setText(servicesList.get(position).getSer_desc());
            }else{
                holder.serviceDisc.setText("+ "+Utilities.getAmtForRecept(Double.parseDouble(servicesList.get(position).getAdditionalPrice()),servicesList.get(position).getCurrencySymbol())+" After that");
            }
        }else
            {
            if(servicesList.get(position).getTempQuant()==0){
                holder.ll_add_service.setVisibility(View.GONE);
                holder.addbtn.setVisibility(View.VISIBLE);
                holder.iv_removeService.setVisibility(View.GONE);
            }else if(servicesList.get(position).getTempQuant()==1){
                holder.ll_add_service.setVisibility(View.GONE);
                holder.addbtn.setVisibility(View.GONE);
                holder.iv_removeService.setVisibility(View.VISIBLE);
            }
            //Showing just description if
            holder.serviceDisc.setText(servicesList.get(position).getSer_desc());
        }
        holder.tvPerSignature.setText(servicesList.get(position).getUnit());
    }

    @Override
    public int getItemCount() {
        return servicesList.size();
    }


    class ServiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView serviceName,totalCost,tvPerSignature,serviceDisc,viewMore,addbtn,tv_quantity,iv_removeService;
        LinearLayout ll_add_service;
        ImageView iv_remove,iv_add;
        ImageView iv_serviceImage;
        private double width,height;

        public ServiceViewHolder(View itemView) {
            super(itemView);
            double size[];
            size = Scaler.getScalingFactor(mContext);
            width = 80*size[0];
            height = 80*size[1];
            ll_add_service=itemView.findViewById(R.id.ll_add_service);
            iv_remove=itemView.findViewById(R.id.iv_remove);
            iv_add=itemView.findViewById(R.id.iv_add);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            serviceName=itemView.findViewById(R.id.serviceName);
            iv_serviceImage=itemView.findViewById(R.id.iv_serviceImage);
            totalCost=itemView.findViewById(R.id.totalCost);
            tvPerSignature=itemView.findViewById(R.id.tvPerSignature);
            serviceDisc=itemView.findViewById(R.id.serviceDisc);
            viewMore=itemView.findViewById(R.id.viewMore);
            iv_removeService=itemView.findViewById(R.id.iv_removeService);
            iv_removeService.setOnClickListener(this);
            viewMore.setOnClickListener(this);
            addbtn=itemView.findViewById(R.id.addbtn);
            serviceName.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_semiBold());
            totalCost.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
            tvPerSignature.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
            serviceDisc.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
            viewMore.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
            addbtn.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
            tv_quantity.setTypeface(AppTypeface.getInstance(itemView.getContext()).getHind_regular());
            addbtn.setOnClickListener(this);
            iv_remove.setOnClickListener(this);
            iv_add.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            pojo = servicesList.get(position);

            int quantity=pojo.getTempQuant();
            for(ServicePojo servicePojo:servicesList)
                Log.d("Shijen", "onClick: "+servicePojo.getSer_name()+" "+servicePojo.getTempQuant());
            switch (view.getId()){
                case R.id.iv_add:
                    if(pojo.getTempQuant()<pojo.getMaxquantity()){
                        callback.onCartModified(servicesList.get(position).get_id(),1,false);
                        pojo.setTempQuant(pojo.getTempQuant()+1);
                    }else{
                        Toast.makeText(mContext, R.string.max_limit_service,Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.iv_remove:
                    if(pojo.getTempQuant()>0){
                        callback.onCartModified(servicesList.get(position).get_id(),2,false);
                        pojo.setTempQuant(pojo.getTempQuant()-1);
                    }
                    break;
                case R.id.addbtn:
                    callback.onCartModified(servicesList.get(position).get_id(),1,false);
                    pojo.setTempQuant(quantity+1);
                    break;
                case R.id.viewMore:
                    callback.onViewMore(pojo.getIs_unit(),pojo.getSer_name(),pojo.getSer_desc());
                    break;
                case R.id.iv_removeService:
                    if(pojo.getTempQuant()>0){
                        callback.onCartModified(servicesList.get(position).get_id(),2,false);
                        pojo.setTempQuant(pojo.getTempQuant()-1);
                    }
                    break;
            }
            notifyItemChanged(position);
        }
    }
}
