package com.Kyhoot.main;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.Kyhoot.R;
import com.Kyhoot.adapter.ChattingAdapter;
import com.Kyhoot.pojoResponce.ChatData;
import com.Kyhoot.pojoResponce.ChatDataObervable;
import com.Kyhoot.pojoResponce.ChatResponseHistory;
import com.Kyhoot.pojoResponce.ErrorHandel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppPermissionsRunTime;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.ApplicationController;
import com.Kyhoot.utilities.DialogInterfaceListner;
import com.Kyhoot.utilities.HandlePictureEvents;
import com.Kyhoot.utilities.MqttEvents;
import com.Kyhoot.utilities.OkHttpConnection;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.CropImage;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChattingActivity extends AppCompatActivity implements View.OnClickListener{

    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 11;
    private AppTypeface appTypeface;
    private HandlePictureEvents handlePicEvent;
    private EditText etMsg;
    private RecyclerView rcvChatMsg;
    private int typeMsg;
    private JSONObject jsonObj;
    private SharedPrefs sharedPrefs;
    private long bid;
    private String proId,cId;
    private String proName;
    private ArrayList<ChatData> chatDataArry;
    private ChattingAdapter cAdapter;
    private long responcecTimeStap = 0;
    private Observer<ChatData> observer;
    private AlertProgress alertProgress;
    private ProgressBar chatProgress;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        getIntents();
        chatDataArry = new ArrayList<>();
        appTypeface = AppTypeface.getInstance(this);
        initializeView();
        initializeToolBar();
    }

    private void getIntents()
    {
        bid = getIntent().getLongExtra("BID",0);
        // String proImage = getIntent().getStringExtra("PROIMAGE");
        proId = getIntent().getStringExtra("PROID");
        proName = getIntent().getStringExtra("PRONAME");
    }

    private void initializeToolBar()
    {
        Toolbar toolbarChatting = findViewById(R.id.toolbarChatting);
        setSupportActionBar(toolbarChatting);
        TextView tvchatproname = findViewById(R.id.tvchatproname);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setTitle("");
        tvchatproname.setText(proName);
        tvchatproname.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarChatting.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        toolbarChatting.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initializeView()
    {
        progressDialog = new ProgressDialog(this);
        sharedPrefs = new SharedPrefs(this);
        handlePicEvent = new HandlePictureEvents(this);
        chatProgress = findViewById(R.id.chatProgress);
        alertProgress = new AlertProgress(this);
        jsonObj = new JSONObject();
        cId = sharedPrefs.getCustomerSid();


        Log.d("initializeView", "CUSTOMERId: "+cId + " ProId "+proId);

        if(!ApplicationController.getInstance().isMqttConnected())
            ApplicationController.getInstance().createMQttConnection(cId);
        else
        {
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.Message.value + "/" + cId, 2);
        }
        ImageView ivAddFiles = findViewById(R.id.ivAddFiles);
        TextView tvchatproname = findViewById(R.id.tvchatproname);
        TextView tvSend = findViewById(R.id.tvSend);
        TextView tvEventId = findViewById(R.id.tvEventId);
        etMsg = findViewById(R.id.etMsg);
        rcvChatMsg = findViewById(R.id.rcvChatMsg);
        String eventId = getResources().getString(R.string.eventId)+" "+bid;
        tvEventId.setText(eventId);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        cAdapter = new ChattingAdapter(this,chatDataArry);
        rcvChatMsg.setLayoutManager(mLayoutManager);

        int resId = R.anim.layoutanimation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        rcvChatMsg.setLayoutAnimation(animation);
        rcvChatMsg.setAdapter(cAdapter);


        if(chatDataArry.size()>0)
        {
            Log.d("Chatin", "initializeView: "+chatDataArry.get(0).getContent());
            Log.d("TAG", "initializeView: "+VariableConstant.isChattingCalled);
            if(VariableConstant.isChattingCalled)
            {
                chatProgress.setVisibility(View.VISIBLE);
                callChatHistory();
            }else
            {
                cAdapter.notifyDataSetChanged();
                scrollToBottom();
            }
        }else {
            chatProgress.setVisibility(View.VISIBLE);
            callChatHistory();
        }
        if(chatDataArry.size()>0)
        {
            cAdapter.notifyDataSetChanged();
            scrollToBottom();
        }
        tvchatproname.setTypeface(appTypeface.getHind_semiBold());
        tvEventId.setTypeface(appTypeface.getHind_regular());
        etMsg.setTypeface(appTypeface.getHind_regular());
        tvSend.setTypeface(appTypeface.getHind_semiBold());
        ivAddFiles.setOnClickListener(this);
        tvSend.setOnClickListener(this);
        initializeRxJava();
    }


    @Override
    public void onClick(View view)
    {
        if(view.getId()==R.id.tvSend)
        {
            if(!etMsg.getText().toString().trim().isEmpty())
            {
                String contentMsg = etMsg.getText().toString().trim();
                etMsg.setText("");
                typeMsg = 1;

                long msgId = (System.currentTimeMillis())/1000;
                sendMessage(contentMsg,typeMsg,msgId);
            }

        }else
        {
            // addFile
            checkPermission();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        VariableConstant.isChattingOpen = true;
        VariableConstant.isChattingToSave = true;
        if(!ApplicationController.getInstance().isMqttConnected())
            ApplicationController.getInstance().createMQttConnection(cId);
        else
        {
            ApplicationController.getInstance().subscribeToTopic(MqttEvents.Message.value + "/" + cId, 2);
        }

    }

    private void initializeRxJava()
    {

        observer = new Observer<ChatData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ChatData chatData) {


                if(responcecTimeStap!=chatData.getTimestamp())
                {
                    responcecTimeStap = chatData.getTimestamp();
                    notifyAdapter(chatData.getTimestamp(),chatData.getType(),chatData.getContent(),cId,proId,2);
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        ChatDataObervable.getInstance().subscribe(observer);
    }

    /**
     * <h2>sendmessage</h2>
     * <p>
     *     sending request to channel a "Message" of socket
     * @param msg message to be send
     * @param msgType message type, i.e what type of message is it. example text message type ,image message type, video message type
     *                for txt msgtype = 0, for video 2, for image 1;
     * @param msgId timeStamp in GMT
     */
    private void sendMessage(String msg, int msgType, long msgId)
    {
        try {

            jsonObj.put("type",msgType);
            jsonObj.put("timestamp",msgId);
            jsonObj.put("content",msg);
            jsonObj.put("fromID",cId);
            jsonObj.put("bid",bid+"");
            jsonObj.put("targetId",proId);
            Log.d("Shijen", "sendMessage: "+VariableConstant.CHAT_URL+"message");
            OkHttpConnection.requestOkHttpConnection(VariableConstant.CHAT_URL+"message", OkHttpConnection.Request_type.POST
                    , jsonObj, sharedPrefs.getSession(), VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                        @Override
                        public void onSuccess(String headerData, String result) {
                            Log.d("TAG", "sendMessageRec: "+result);
                        }

                        @Override
                        public void onError(String headerError, String error)
                        {
                            if(headerError.equals("502"))
                            {
                                alertProgress.alertinfo(getString(R.string.serverProblm));
                            }else
                            {
                                ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                                switch (headerError) {
                                    case "498":
                                        sessionExpired(errorHandel.getMessage());
                                        break;
                                    case "440":
                                        getAccessToken(errorHandel.getData());
                                        break;
                                    default:
                                        alertProgress.alertinfo(errorHandel.getMessage());
                                        break;
                                }

                            }
                        }
                    });

            notifyAdapter(msgId,msgType,msg,cId,proId,1);
            scrollToBottom();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private void notifyAdapter(long msgid, int msgtype, String msg, String customerSid, String proId, int custProType)
    {
        ChatData chatData = new ChatData();
        chatData.setTimestamp(msgid);
        chatData.setType(msgtype);
        chatData.setTargetId(proId);
        chatData.setFromID(customerSid);
        chatData.setContent(msg);
        chatData.setCustProType(custProType);
        chatDataArry.add(chatData);
        cAdapter.notifyDataSetChanged();
        scrollToBottom();
    }

    /*scrolling to the bottom of the recyclerview*/
    private void scrollToBottom() {
        rcvChatMsg.scrollToPosition(cAdapter.getItemCount() - 1);
    }

    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkPermission()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE))
            {
                //if(!isFb)
                selectImage();
            }

        }else
        {
            // if(!isFb)
            selectImage();
        }

    }

    /**
     * predefined method to check run time permissions list call back
     * @param requestCode   request code
     * @param permissions:  contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean isDenine = false;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    alertProgress.alertinfo(getResources().getString(R.string.permissiondenied)+ " " + REQUEST_CODE_PERMISSION_MULTIPLE);
                } else {
                    selectImage();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void selectImage() {
        handlePicEvent.openDialog();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        VariableConstant.isChattingOpen = false;
        VariableConstant.isChattingToSave = false;
        ApplicationController.getInstance().unsubscribeToTopic(MqttEvents.Message.value + "/" + cId);
        observer.onComplete();
    }

    @Override
    protected void onPause() {
        super.onPause();
        VariableConstant.isChattingOpen = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)    //result code to check is the result is ok or not
        {
            return;
        }
        switch (requestCode) {
            case VariableConstant.CAMERA_PIC:
                handlePicEvent.startCropImage(handlePicEvent.newFile);
                break;
            case VariableConstant.GALLERY_PIC:
                if(data!=null)
                {

                    handlePicEvent.gallery(data.getData());
                }
                break;
            case VariableConstant.CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path != null)
                {
                    try {
                        /*handlePicEvent.uploadToAmazon(VariableConstant.Amazonbucket+"/"+VariableConstant.AmazonChatFolder,new File(path), new ImageUploadedAmazon() {
                            @Override
                            public void onSuccessAdded(String image)
                            {
                                Log.e("TAG_AMZ", "onSuccessAdded: Chat "+image );
                                typeMsg=2;
                                sendMessage(image, typeMsg,(System.currentTimeMillis())/1000);
                                Log.e("TAG_AMZ", "onSuccessAdded: sendMsg called "+typeMsg);
                            }
                            @Override
                            public void onerror(String errormsg)
                            {
                                Log.e("TAG_AMZ", "onerror: Chat "+errormsg );
                            }
                        });*/
                        progressDialog.setMessage(getString(R.string.uploading));
                        progressDialog.show();
                        new UploadFileToServer().execute(path);
                    } catch (Exception e)
                    {
                        Log.e("TAG_AMZ", "Exception "+e);
                        e.printStackTrace();
                    }
                    break;
                }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utilities.checkAndShowNetworkError(this);
    }

    private void getAccessToken(String data)
    {
        OkHttpConnection.requestOkHttpConnection(VariableConstant.SERVICE_URL + "accessToken",
                OkHttpConnection.Request_type.GET, new JSONObject(), data, VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        try {
                            JSONObject jsonobj = new JSONObject(result);
                            sharedPrefs.setSession(jsonobj.getString("data"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String headerError, String error) {
                        if (headerError.equals("502"))
                            alertProgress.alertinfo(getString(R.string.serverProblm));
                        else {
                            ErrorHandel errorHandel = new Gson().fromJson(error, ErrorHandel.class);
                            switch (headerError) {
                                case "498":
                                    sessionExpired(errorHandel.getMessage());
                                    break;
                                default:
                                    alertProgress.alertinfo(errorHandel.getMessage());
                                    break;
                            }
                        }
                    }
                });
    }

    private void sessionExpired(final String message)
    {
        alertProgress.alertPostivionclick(message, new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                Utilities.setMAnagerWithBID(ChattingActivity.this,sharedPrefs,message);
            }
        });
    }

    private void callChatHistory()
    {
        Log.d("Shijen", "callChatHistory: "+VariableConstant.CHAT_URL+"chatHistory/" + bid + "/" + 0);
        OkHttpConnection.requestOkHttpConnection(VariableConstant.CHAT_URL+"chatHistory/" + bid + "/" + 0,
                OkHttpConnection.Request_type.GET, new JSONObject(), sharedPrefs.getSession()
                , VariableConstant.SelLang, new OkHttpConnection.JsonResponceCallback() {
                    @Override
                    public void onSuccess(String headerData, String result) {

                        Log.e("TAG", "onSuccess: Chat "+result );

                        VariableConstant.isChattingCalled = false;
                        ChatResponseHistory chatReponce =  new Gson().fromJson(result, ChatResponseHistory.class);

                        if(chatDataArry.size()>0)
                        {
                            for(int j = 0; j<chatDataArry.size();j++)
                            {
                                for(int i = 0; i<chatReponce.getData().size();i++)
                                {

                                    if(chatDataArry.get(j).getTimestamp()!=chatReponce.getData().get(i).getTimestamp())
                                    {
                                        if(chatReponce.getData().get(i).getFromID().equals(cId))
                                        {
                                            chatReponce.getData().get(i).setCustProType(1);
                                        }else
                                        {
                                            chatReponce.getData().get(i).setCustProType(2);
                                        }
                                        ChatData chatData = new ChatData();
                                        chatData.setTimestamp(chatReponce.getData().get(i).getTimestamp());
                                        chatData.setType(chatReponce.getData().get(i).getType());
                                        chatData.setTargetId(proId);
                                        chatData.setFromID(cId);
                                        chatData.setContent(chatReponce.getData().get(i).getContent());
                                        chatData.setCustProType(chatReponce.getData().get(i).getCustProType());
                                        chatDataArry.add(chatData);
                                    }
                                }
                            }

                        }else
                        {
                            if(!chatReponce.getMessage().equals("No Data found"))
                            {
                                for(int i = 0; i<chatReponce.getData().size();i++)
                                {
                                    if(chatReponce.getData().get(i).getFromID().equals(cId))
                                    {
                                        chatReponce.getData().get(i).setCustProType(1);
                                    }else
                                    {
                                        chatReponce.getData().get(i).setCustProType(2);
                                    }
                                }
                                chatDataArry.addAll(chatReponce.getData());
                            }
                        }
                        if(chatDataArry.size()>0)
                        {
                            Collections.sort(chatDataArry, new Comparator<ChatData>() {
                                @Override
                                public int compare(ChatData o1, ChatData o2) {
                                    return (int)(o1.getTimestamp() - o2.getTimestamp());
                                }
                            });
                            cAdapter.notifyDataSetChanged();
                            scrollToBottom();
                        }
                        chatProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String headerError, String error)
                    {
                        chatProgress.setVisibility(View.GONE);
                    }
                });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (handlePicEvent.newFile != null)
            outState.putString("cameraImage", handlePicEvent.newFile.getPath());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("cameraImage")) {
                Uri uri = Uri.parse(savedInstanceState.getString("cameraImage"));
                handlePicEvent.newFile = new File(uri.getPath());
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }


    // Not Working - PRAMOD
    /**
     * Uploading the file to server
     * */
    @SuppressLint("StaticFieldLeak")
    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String[] doInBackground(String... params)
        {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result,responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(VariableConstant.CHAT_URL+"uploadImageOnServer")
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d("Shijen", "doInBackground: "+responseCode);
                Log.d("Shijen", "doInBackground: "+result);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            progressDialog.dismiss();
            try {
                if(result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                {
                    typeMsg=2;
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");
                    sendMessage(image, typeMsg,(System.currentTimeMillis())/1000);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }

    }
}
