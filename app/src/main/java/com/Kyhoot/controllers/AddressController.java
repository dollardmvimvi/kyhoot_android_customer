package com.Kyhoot.controllers;

import com.Kyhoot.interfaceMgr.NetworkCheck;
import com.Kyhoot.interfaceMgr.UtilInterFace;
import com.Kyhoot.models.AddressModel;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.SharedPrefs;

/**
 * <h>AddressController</h>
 * <p>
 * this controlls the addressType activity
 * Created by 3Embed on 10/11/2017.</p>
 */

public class AddressController implements NetworkCheck {
    private UtilInterFace uInterFace;
    private AlertProgress aProgress;
    private String addressType = "";
    private AddressModel aModel;
    private SharedPrefs sprefs;
    private String addressId = "";

    public AddressController(UtilInterFace uInterFace, SharedPrefs sprefs) {
        this.uInterFace = uInterFace;
        this.sprefs = sprefs;

    }

    /**
     * <h1>callNetworkCheck</h1>
     * <p>
     *
     * @param aProgress Alert progree to show the alert on the screen
     * @param address   this is the address Type
     * @param addressId this is the address id for Address to delete;
     * @see AlertProgress
     * </p>
     */
    public void callNetworkCheck(AlertProgress aProgress, String address, String addressId) {
        aModel = new AddressModel();
        this.aProgress = aProgress;
        this.addressType = address;
        this.addressId = addressId;
        aModel.checkNetwork(aProgress, this);

    }

    @Override
    public void onNetworkAvailble() {

        uInterFace.showProgress();
        switch (addressType) {
            case "GETADDRESS":
                aModel.GetAddress(uInterFace, sprefs);
                break;
            case "DELETE":
                aModel.callDeleteAddress(uInterFace, addressId, sprefs);
                break;
            default:
                aModel.callAddAddress(uInterFace);
                break;
        }
    }

    @Override
    public void onNetworkNotAvailble() {
        aProgress.showNetworkAlert();
    }


}
