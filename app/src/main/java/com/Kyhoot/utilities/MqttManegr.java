package com.Kyhoot.utilities;

import android.util.Log;

import com.google.gson.Gson;
import com.Kyhoot.models.HomeFragmentModel;
import com.Kyhoot.pojoResponce.ChatDataObervable;
import com.Kyhoot.pojoResponce.ChatReponce;
import com.Kyhoot.pojoResponce.LiveTackPojo;
import com.Kyhoot.pojoResponce.LiveTrackObservable;
import com.Kyhoot.pojoResponce.MyEventStatus;
import com.Kyhoot.pojoResponce.MyEventStatusObservable;

import org.json.JSONObject;

/**
 * <h>MqttManegr</h>
 * Created by Ali on 9/29/2017.
 */

public class MqttManegr {
    private HomeFragmentModel homeModel;
    private Gson gson;

    public MqttManegr(HomeFragmentModel homeFragmentModel) {
        homeModel = homeFragmentModel;
        gson = new Gson();
        Log.d("HOMEMODEL", "MqttManegr: " + homeModel);
    }

    public void managerMqtt(JSONObject jsonObject, String topic) {
        //Log.d("1", "managerMqtt: "+jsonObject);
        Log.d("MQTTRES", "managerMqttProviders: " + jsonObject.toString());

        String topicsplit[] = topic.split("/");
        if (topicsplit[0].equals(MqttEvents.Provider.value)) {
            Log.d("MQTTRES", "managerMqttProviders: " + jsonObject.toString());
            homeModel.responceHomeModel(jsonObject.toString());
        } else if (topicsplit[0].equals(MqttEvents.JobStatus.value)) {
            Log.d("MQTTRES", "managerMqttMSGS: " + jsonObject.toString());
            try{
                MyEventStatus myEventStatus = new Gson().fromJson(jsonObject.toString(), MyEventStatus.class);
                MyEventStatusObservable.getInstance().emit(myEventStatus);
            }catch(Exception e){
                e.printStackTrace();
            }

        } else if (topicsplit[0].equals(MqttEvents.LiveTrack.value)) {
            //Log.d("LIVETRACK", "managerMqttLIVE: " + jsonObject.toString());
            LiveTackPojo liveTrackPojo = new Gson().fromJson(jsonObject.toString(), LiveTackPojo.class);
            LiveTrackObservable.getInstance().emit(liveTrackPojo);
            VariableConstant.LiveTrackBooking = liveTrackPojo.getPid();
        }
        else if(topicsplit[0].equals(MqttEvents.Message.value)) {
            //Log.d("MESSAGE", "managerMqttMESSGE: "+jsonObject.toString());
            ChatReponce chatReponce = gson.fromJson(jsonObject.toString(),ChatReponce.class);
            ChatDataObervable.getInstance().emitData(chatReponce.getData());
        }
    }
}
