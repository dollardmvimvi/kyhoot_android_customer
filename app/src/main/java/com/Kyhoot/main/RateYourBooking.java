package com.Kyhoot.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.adapter.RatingAdapter;
import com.Kyhoot.controllers.RateYourProviderImpl;
import com.Kyhoot.interfaceMgr.AccessTokenCallback;
import com.Kyhoot.interfaceMgr.RateYourProviderContract;
import com.Kyhoot.pojoResponce.CartData;
import com.Kyhoot.pojoResponce.InvoiceDetails;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.CircleTransform;
import com.Kyhoot.utilities.NotificationUtils;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;


import java.util.ArrayList;
import com.squareup.picasso.Picasso;

public class RateYourBooking extends AppCompatActivity implements RateYourProviderContract.ViewContract
        ,RatingAdapter.ViewChange,View.OnClickListener, AccessTokenCallback
{//,RatingBar.OnRatingBarChangeListener
    Toolbar tbLayout;
    TextView tv_yourBookingOnLabel;
    TextView tv_yourBookingOn;
    TextView tvTotalBillAmtTitle;
    TextView tvTotalBillAmt;
    TextView tvReceipt;
    TextView tvAddToFav;
    TextView tvRateProviderTitle;
    TextView tvRateProviderName;
    ImageView ivProfilePic;
    Button btnSave;
    EditText tvComments;
    RecyclerView rvGrid;
    ProgressBar progressBarRateYourPro;
   // @BindView(R.id.rbProvider)RatingBar rbProvider;

    AppTypeface appTypeface;

    RateYourProviderContract.Presenter presenter;
    SharedPrefs manager;

    private RatingAdapter adapter;
    private ArrayList<InvoiceDetails.CustomerRating> stringList;
    private CartData cartInfo;
    private String signURL,addressLine,providerId;
    private long bId;
    AlertProgress alertProgress;
    private InvoiceDetails.InvoiceData data;
    private boolean isFavProvider=false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_your_booking);
        init();
        getIntentValue();
        setToolBarValue();
        initializeTypeFace();
    }

    private void init() {
        presenter =new RateYourProviderImpl(this,this);
        manager=new SharedPrefs(this);
        tbLayout=findViewById(R.id.tbLayout);
        appTypeface=AppTypeface.getInstance(this);
        tv_yourBookingOn=findViewById(R.id.tv_yourBookingOn);
        tv_yourBookingOnLabel=findViewById(R.id.tv_yourBookingOnLabel);
        tvTotalBillAmtTitle=findViewById(R.id.tvTotalBillAmtTitle);
        tvTotalBillAmt=findViewById(R.id.tvTotalBillAmt);
        tvReceipt=findViewById(R.id.tvReceipt);
        tvAddToFav=findViewById(R.id.tvAddToFav);
        tvRateProviderTitle=findViewById(R.id.tvRateProviderTitle);
        tvRateProviderName=findViewById(R.id.tvRateProviderName);
        ivProfilePic=findViewById(R.id.ivProfilePic);
        btnSave=findViewById(R.id.btnSave);
        tvComments=findViewById(R.id.tvComments);
        rvGrid=findViewById(R.id.rvGrid);
        progressBarRateYourPro=findViewById(R.id.progressBarRateYourPro);
        btnSave.setOnClickListener(this);
        tvReceipt.setOnClickListener(this);
        tvAddToFav.setOnClickListener(this);
    }

    private void getIntentValue() {

        if (getIntent().getExtras() != null) {

            bId = getIntent().getLongExtra("BID", 0);
            // isImageDrawn = true;
        }
    }

    private void initializeTypeFace()
    {
        tv_yourBookingOnLabel.setTypeface(appTypeface.getHind_medium());
        tv_yourBookingOn.setTypeface(appTypeface.getHind_regular());
        tvTotalBillAmtTitle.setTypeface(appTypeface.getHind_regular());
        tvTotalBillAmt.setTypeface(appTypeface.getHind_semiBold());
        tvReceipt.setTypeface(appTypeface.getHind_regular());
        tvAddToFav.setTypeface(appTypeface.getHind_regular());
        tvRateProviderTitle.setTypeface(appTypeface.getHind_medium());
        tvRateProviderName.setTypeface(appTypeface.getHind_medium());
        btnSave.setTypeface(appTypeface.getHind_semiBold());
        tvComments.setTypeface(appTypeface.getHind_regular());
        tv_yourBookingOnLabel.setText("Your Last Booking on");

    }

    private void setToolBarValue()
    {
        setSupportActionBar(tbLayout);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tbLayout.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateYourBooking.this.onBackPressed();
            }
        });
        presenter.onInvoiceDetailsCalled(bId,manager);
        stringList=new ArrayList<>();

      //  rbProvider.setOnRatingBarChangeListener(this);
      /*  stringList=new ArrayList<>();
        if(rbProvider.getRating()>3)
            presenter.getStringList();*/
        setAdapters();
    }

    private void setAdapters() {
        adapter=new RatingAdapter(stringList,this,this);
      //  rvGrid.setLayoutManager(new GridLayoutManager(this,3));
        rvGrid.setLayoutManager(new LinearLayoutManager(this));
        rvGrid.setAdapter(adapter);
        rvGrid.setNestedScrollingEnabled(false);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.dp_5);
        RatingAdapter.SpacesItemDecoration itemDecoration=new RatingAdapter.SpacesItemDecoration(spacingInPixels);
        rvGrid.addItemDecoration(itemDecoration);
    }


    @Override
    public void onSessionExpired()
    {
        Utilities.setMAnagerWithBID(this,manager,"");
    }

    @Override
    public void onSuccessAccessToken(String data) {
        manager.setSession(data);
    }

    @Override
    public void onFailureAccessToken(String headerError, String server_issue) {
        Toast.makeText(this, server_issue, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLogout(String message)
    {
        Utilities.setMAnagerWithBID(this,manager,"");
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onShowProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBarRateYourPro.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgress() {
        progressBarRateYourPro.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onGetInvoiceDetails(InvoiceDetails.InvoiceData data)
    {
        this.data=data;
        String name = data.getProviderData().getFirstName()+" "+data.getProviderData().getLastName();
        tvRateProviderName.setText(name);
        Utilities.setAmtOnRecept(data.getAccounting().getTotal(),tvTotalBillAmt,data.getCart().getCurrencySymbol());

        stringList.addAll(data.getCustomerRating());
        adapter.notifyDataSetChanged();
        providerId = data.getProviderData().getProviderId();
        presenter.timeMethod(tv_yourBookingOn, data.getBookingRequestedFor());
        signURL = data.getSignURL();
        addressLine = data.getAddLine1();
        cartInfo = data.getCart();
        if (data.getProviderData().getProfilePic() != null && !data.getProviderData().getProfilePic().equals("")) {
            Picasso.with(this)
                    .load(data.getProviderData().getProfilePic())
                    .transform(new CircleTransform())
                    .into(ivProfilePic);
        }
    }

    @Override
    public void onRateProviderSuccess() {

        if(!VariableConstant.isHomeFragment)
        {
            VariableConstant.isHomeFragment = false;
            VariableConstant.isConfirmBook = false;
            //VariableConstant.isJobDetailsOpen = false;
            Intent intent = new Intent(RateYourBooking.this, MenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            /*if(mqttManager.isMQTTConnected())
                mqttManager.unSubscribeToTopic(MqttEvents.JobStatus.value + "/" + manager.getCustomerSid());*/
            startActivity(intent);
        }
        NotificationUtils.clearNotifications(this);
        finish();
    }

    @Override
    public void onGetStarList(ArrayList<String> stringList)
    {
        //this.stringList=stringList;
    }

    @Override
    public void onFavAdded(String message) {
        isFavProvider=true;
        updateFavoriteButton();
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
        //alertProgress.alertinfo(message);
    }

    @Override
    public void onUnfavoritingSuccessful(String headerError, String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
        isFavProvider=false;
        updateFavoriteButton();
    }

    @Override
    public void onUnfavoritingError(String headerError, String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onRatingChanged(float rating, int position)
    {
        stringList.get(position).setRatings(rating);
        updateFavoriteButton();
        /*if(v>3){
            presenter.getStringList();
            if(adapter.getItemCount()==0)
                adapter.setList(stringList);
        }else{
            if(adapter!=null){
                adapter.clearList();
            }
        }*/
    }

    @Override
    public void onAdapterSet() {
        isFavProvider = data.isFavouriteProvider();
        updateFavoriteButton();
    }

    public float checkRatingAvg() {
        float avg=0;
        for(int i=0;i<stringList.size();i++){
            avg=avg+stringList.get(i).getRatings();
        }
        avg=avg/stringList.size();
        return avg;
    }

    /**
     * <h1>updateFavoriteButton</h1>
     * <p>This method is used to update the button according to rating given</p>
     */
    private void updateFavoriteButton() {
        float addFavoriteProviderRating = data.getAddFavoriteProviderRating();
        float removeFavoriteProviderRating = data.getRemoveFavoriteProviderRating();
        float ratingAvg = checkRatingAvg();

        if (isFavProvider) {
            tvAddToFav.setText(R.string.remove_favorite);
            if(ratingAvg<=removeFavoriteProviderRating){
                tvAddToFav.setVisibility(View.VISIBLE);
            }else{
                tvAddToFav.setVisibility(View.INVISIBLE);
            }
        } else {
            tvAddToFav.setText(R.string.add_to_favorite);
            if(ratingAvg>=addFavoriteProviderRating){
                tvAddToFav.setVisibility(View.VISIBLE);
            }else{
                tvAddToFav.setVisibility(View.INVISIBLE);
            }
        }
    }

    /*@Override
    public void onTextSelected(float rating, int position)
    {
        stringList.get(position).setRatings(rating);
      *//*  if(!rating.equals("other"))
            tvComments.setText(rating);
        else{tvAddToFav
            tvComments.setText("");
            tvComments.requestFocus();
        }*//*
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tvAddToFav:
                if(!isFavProvider){
                    presenter.onAddToFav(providerId,data.getCategoryId(),manager.getSession());
                }else{
                    presenter.onRemoveFav(providerId,data.getCategoryId(),manager.getSession());
                }
                /*onShowProgress();
                presenter.onAddToFav(providerId);*/
                break;
            case R.id.tvReceipt:
                presenter.openDialog(RateYourBooking.this, appTypeface, data, signURL);
                break;
            case R.id.btnSave:
                onShowProgress();
                presenter.onUpdateReview(bId,stringList,tvComments.getText().toString(),manager);
                break;
        }
    }
}
