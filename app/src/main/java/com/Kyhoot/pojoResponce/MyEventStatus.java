package com.Kyhoot.pojoResponce;

import java.io.Serializable;

/**
 * <h>MyEventStatus</h>
 * this get the event status for the live bookig form the server api
 * Created by 3Embed on 11/1/2017.
 */

public class MyEventStatus implements Serializable {
    private DataEvent data;

    public MyEventStatus(DataEvent data) {
        this.data = data;
    }

    public DataEvent getData() {
        return data;
    }

    public class DataEvent {
        /*"bookingId":1509723436076,"status":3,"statusMsg":"Accepted","statusUpdateTime":1509723754,"proProfilePic":"https:\/\/s3.amazonaws.com\/livemapplication\/Provider\/ProfilePics\/LIVEMProfile1509030309086.png"}*/
        String statusMsg;
        String proProfilePic;
        String firstName;

        public int getBookingModel() {
            return bookingModel;
        }

        public void setBookingModel(int bookingModel) {
            this.bookingModel = bookingModel;
        }

        String lastName;
        String msg;
        long statusUpdateTime, bookingId,bookingRequestedFor;
        int status,bookingType,bookingModel;

        LiveBookingStatusPojo.LiveBookingData.BookingTimer bookingTimer;

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

        public String getProProfilePic() {
            return proProfilePic;
        }

        public void setProProfilePic(String proProfilePic) {
            this.proProfilePic = proProfilePic;
        }

        public long getStatusUpdateTime() {
            return statusUpdateTime;
        }

        public void setStatusUpdateTime(long statusUpdateTime) {
            this.statusUpdateTime = statusUpdateTime;
        }
        public LiveBookingStatusPojo.LiveBookingData.BookingTimer getBookingTimer() {
            return bookingTimer;
        }

        public long getBookingId() {
            return bookingId;
        }

        public void setBookingId(long bookingId) {
            this.bookingId = bookingId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getMsg() {
            return msg;
        }

        public long getBookingRequestedFor() {
            return bookingRequestedFor;
        }

        public int getBookingType() {
            return bookingType;
        }
    }
}
