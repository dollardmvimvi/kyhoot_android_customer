package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${3Embed} on 31/1/18.
 */

public class ServicesData implements Serializable
{
    private String sub_cat_name;

    private String sub_cat_id;

    private String sub_cat_desc;

    private ArrayList<ServicePojo> service;

    public String getSub_cat_name ()
    {
        return sub_cat_name;
    }

    public void setSub_cat_name (String sub_cat_name)
    {
        this.sub_cat_name = sub_cat_name;
    }

    public ArrayList<ServicePojo> getService ()
    {
        return service;
    }

    public void setService (ArrayList<ServicePojo> service)
    {
        this.service = service;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public String getSub_cat_desc() {
        return sub_cat_desc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sub_cat_name = "+sub_cat_name+", service = "+service+"]";
    }
}
