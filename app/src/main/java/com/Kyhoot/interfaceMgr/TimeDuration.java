package com.Kyhoot.interfaceMgr;

/**
 * Created by Ali on 1/12/2018.
 */

public interface TimeDuration
{
    void onDistanceTime(String timeEta,String distance);
}