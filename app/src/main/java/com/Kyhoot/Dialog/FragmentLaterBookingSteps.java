package com.Kyhoot.Dialog;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.LaterBookingCallback;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLaterBookingSteps extends DialogFragment implements View.OnClickListener, TimePicker.OnTimeChangedListener {
    private LaterBookingCallback callback;
    TextView tv_categoryName,tv_nowLabel,tvNowDesc,tv_LaterLabel,tvLaterDesc,tv_cancel,
            tv_book,tv_next,tv_add_no_hr_label,tv_quantity;
    ImageView iv_dialogback,imageView,imageViewLater,iv_remove,iv_add;
    ConstraintLayout cl_now_later,cl_now,cl_later,cl_dateSelector,cl_timeSelector,include8,include7,include6,
            include5,include4,include3,include2,i_other;
    ArrayList<ConstraintLayout> calAll;
    private static final int TIME_PICKER_INTERVAL = 15;
    Calendar calendarAfter7Days,mCalendar;
    private static final String TAG = "FragmentLaterBookingSte";
    int screenNo=2;
    SimpleDateFormat dateFormat,dateFormatMonth;
    private TimePicker tp_timePicker;
    AlertProgress alertProgress;
    int tempBookingType=1;
    Calendar tempTime=null;
    int numberOfHours=1;

    public void setCallBack(LaterBookingCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fragment_later_booking_steps, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        alertProgress=new AlertProgress(getActivity());
        mCalendar=Utilities.getCalendarServerTime();
        tempTime=Utilities.getCalendarServerTime();
        // Header
        tv_categoryName=view.findViewById(R.id.tv_categoryName);
        iv_dialogback=view.findViewById(R.id.iv_dialogback);
        iv_dialogback.setOnClickListener(this);
        //footer
        tv_cancel=view.findViewById(R.id.tv_cancel);
        tv_book=view.findViewById(R.id.tv_book);
        tv_next=view.findViewById(R.id.tv_next);
        tv_cancel.setOnClickListener(this);
        tv_book.setOnClickListener(this);
        tv_next.setOnClickListener(this);
        //Bookingtype
        cl_now_later=view.findViewById(R.id.cl_now_later);
        cl_now=view.findViewById(R.id.cl_now);
        cl_later=view.findViewById(R.id.cl_later);
        imageView=view.findViewById(R.id.imageView);
        tv_nowLabel=view.findViewById(R.id.tv_nowLabel);
        tvNowDesc=view.findViewById(R.id.tvNowDesc);
        imageViewLater=view.findViewById(R.id.imageViewLater);
        tv_LaterLabel=view.findViewById(R.id.tv_LaterLabel);
        tvLaterDesc=view.findViewById(R.id.tvLaterDesc);
        cl_now.setOnClickListener(this);
        cl_later.setOnClickListener(this);
        //DateSelector
        cl_dateSelector=view.findViewById(R.id.cl_dateSelector);
        include8=view.findViewById(R.id.include8);
        include7=view.findViewById(R.id.include7);
        include6=view.findViewById(R.id.include6);
        include5=view.findViewById(R.id.include5);
        include4=view.findViewById(R.id.include4);
        include3=view.findViewById(R.id.include3);
        include2=view.findViewById(R.id.include2);
        i_other=view.findViewById(R.id.i_other);

        include8.setOnClickListener(this);
        include7.setOnClickListener(this);
        include6.setOnClickListener(this);
        include5.setOnClickListener(this);
        include4.setOnClickListener(this);
        include3.setOnClickListener(this);
        include2.setOnClickListener(this);
        i_other.setOnClickListener(this);
        setCalendarArraList();
        Calendar calendar=Calendar.getInstance(VariableConstant.TIMEZONE);
        calendar.setTime(new Date(System.currentTimeMillis()-VariableConstant.SERVERTIMEDIFFERNCE));
        setDatesOnUi(calendar);

        //TimeSelector
        cl_timeSelector=view.findViewById(R.id.cl_timeSelector);
        tv_add_no_hr_label=view.findViewById(R.id.tv_add_no_hr_label);
        tv_quantity=view.findViewById(R.id.tv_quantity);
        tp_timePicker=view.findViewById(R.id.tp_timePicker);
        tp_timePicker.setOnTimeChangedListener(this);
        setTimePickerInterval(tp_timePicker);
        iv_remove=view.findViewById(R.id.iv_remove);
        iv_remove.setOnClickListener(this);
        iv_add=view.findViewById(R.id.iv_add);
        iv_add.setOnClickListener(this);






        settypeface();
        //================================
        cl_later.setSelected(true);
        //showNowOrLaterSelector();
        showDateSelector();

    }

    private void settypeface() {
        AppTypeface instance = AppTypeface.getInstance(getActivity());
        tv_nowLabel.setTypeface(instance.getHind_bold());
        tvNowDesc.setTypeface(instance.getHind_regular());
        tv_LaterLabel.setTypeface(instance.getHind_bold());
        tvLaterDesc.setTypeface(instance.getHind_regular());

        tv_next.setTypeface(instance.getHind_semiBold());
        tv_cancel.setTypeface(instance.getHind_semiBold());
        tv_book.setTypeface(instance.getHind_semiBold());
        tv_categoryName.setTypeface(instance.getHind_semiBold());
        for(int i=0;i<calAll.size();i++){
            ((TextView)calAll.get(i).findViewById(R.id.tvCalDate)).setTypeface(instance.getHind_regular());
            ((TextView)calAll.get(i).findViewById(R.id.tvCalDay)).setTypeface(instance.getHind_regular());
            ((TextView)calAll.get(i).findViewById(R.id.others)).setTypeface(instance.getHind_regular());
        }
        tv_add_no_hr_label.setTypeface(instance.getHind_semiBold());
        tv_quantity.setTypeface(instance.getHind_semiBold());
    }

    private void setCalendarArraList() {
        calAll = new ArrayList<>();
        calAll.add(include2);
        calAll.add(include3);
        calAll.add(include4);
        calAll.add(include5);
        calAll.add(include6);
        calAll.add(include7);
        calAll.add(include8);
        calAll.add(i_other);
    }

    private void setDatesOnUi(Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        dateFormat = new SimpleDateFormat("EEE",Locale.getDefault());
        dateFormatMonth = new SimpleDateFormat("MMMM",Locale.getDefault());
        dateFormat.setTimeZone(VariableConstant.TIMEZONE);
        dateFormatMonth.setTimeZone(VariableConstant.TIMEZONE);

        Calendar temp=Calendar.getInstance(VariableConstant.TIMEZONE);

        temp.setTime(calendar.getTime());
        ((TextView)include8.findViewById(R.id.tvCalDay)).setText("Today");
        ((TextView)include8.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include8.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include8.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+1,hour,minute);
        ((TextView)include7.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)include7.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include7.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include7.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+2,hour,minute);
        ((TextView)include6.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)include6.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include6.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include6.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+3,hour,minute);
        ((TextView)include5.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)include5.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include5.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include5.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+4,hour,minute);
        ((TextView)include4.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)include4.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include4.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include4.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+5,hour,minute);
        ((TextView)include3.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)include3.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include3.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include3.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+6,hour,minute);
        ((TextView)include2.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)include2.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)include2.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        include2.setTag(temp.getTimeInMillis());
        temp.set(year,month,day+7,hour,minute);

        calendarAfter7Days=Calendar.getInstance();
        calendarAfter7Days.setTime(temp.getTime());
        if(!VariableConstant.SCHEDULEDDATE.equals("")){
            i_other.setTag(Long.parseLong(VariableConstant.SCHEDULEDDATE)*1000);
            temp.setTimeInMillis(Long.parseLong(VariableConstant.SCHEDULEDDATE)*1000);
        }
        ((TextView)i_other.findViewById(R.id.tvCalDay)).setText(dateFormat.format(temp.getTime()));
        ((TextView)i_other.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(temp.getTime()));
        ((TextView)i_other.findViewById(R.id.tvCalDate)).setText(temp.get(Calendar.DAY_OF_MONTH)+"");
        ((TextView)i_other.findViewById(R.id.tvCalDay)).setVisibility(View.INVISIBLE);
        ((TextView)i_other.findViewById(R.id.tvCalDate)).setVisibility(View.INVISIBLE);
        ((TextView)i_other.findViewById(R.id.others)).setVisibility(View.VISIBLE);

    }


    private void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            Field field = classForid.getField("minute");
            NumberPicker minutePicker = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));

            minutePicker.setMinValue(0);
            minutePicker.setMaxValue(3);
            ArrayList<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            minutePicker.setDisplayedValues(displayedValues
                    .toArray(new String[0]));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case  R.id.cl_now:
                setNowBooking();
                break;
            case  R.id.cl_later:
                setLaterBooking();
                break;
            case R.id.i_other:
                openDatePicker();
                setCalUnselect(view);
                break;
            case R.id.include2:
                setCalUnselect(view);
                break;
            case R.id.include3:
                setCalUnselect(view);
                break;
            case R.id.include4:
                setCalUnselect(view);
                break;
            case R.id.include5:
                setCalUnselect(view);
                break;
            case R.id.include6:
                setCalUnselect(view);
                break;
            case R.id.include7:
                setCalUnselect(view);
                break;
            case R.id.include8:
                setCalUnselect(view);
                break;
            case R.id.tv_next:
                if(screenNo==1){
                    showDateSelector();
                }else if(screenNo==2){
                    showTimeSelector();
                }
                break;
            case R.id.tv_book:
                setBookingValues();
                dismiss();

                break;
            case R.id.tv_cancel:
                callback.onCancel();
                dismiss();
                break;
            case R.id.iv_dialogback:
                onBackButtonClicked();
                break;
            case R.id.iv_add:
                setNumberofHr(1);
                break;
            case R.id.iv_remove:
                setNumberofHr(-1);
                break;
        }
    }

    private void setBookingValues() {
        if(tempBookingType==1){
            callback.onBook(tempBookingType,0,0);
        }else{
            callback.onBook(tempBookingType,tempTime.getTimeInMillis()/1000,numberOfHours*60);
        }
    }

    private void setNowBooking() {
        cl_later.setSelected(false);
        cl_now.setSelected(true);
        checkNowOrLater();
        tempBookingType=1;
    }

    private void setLaterBooking() {
        cl_later.setSelected(true);
        cl_now.setSelected(false);
        checkNowOrLater();
        tempBookingType=2;
    }

    private void setNumberofHr(int i) {
        if(i==-1){
            if(numberOfHours==1){
                alertProgress.alertinfo("Minimum hours selected should be 1");
            }else{
                numberOfHours=numberOfHours+i;
            }
        }else{
            numberOfHours=numberOfHours+i;
        }
        tv_quantity.setText(numberOfHours+"");
    }

    private void onBackButtonClicked() {
        if(screenNo==2){
            showNowOrLaterSelector();
        }else if(screenNo==3){
            showDateSelector();
        }
    }

    private void showNowOrLaterSelector() {
        cl_dateSelector.setVisibility(View.GONE);
        cl_now_later.setVisibility(View.VISIBLE);
        cl_timeSelector.setVisibility(View.GONE);
        tv_cancel.setVisibility(View.VISIBLE);
        checkNowOrLater();
        tv_categoryName.setText(VariableConstant.SELECTEDCATEGORYNAME+"");
        iv_dialogback.setVisibility(View.GONE);
        screenNo=1;
    }

    private void showDateSelector() {
        tempBookingType=2;
        cl_dateSelector.setVisibility(View.VISIBLE);
        cl_now_later.setVisibility(View.GONE);
        cl_timeSelector.setVisibility(View.GONE);
        tv_cancel.setVisibility(View.VISIBLE);
        tv_book.setVisibility(View.GONE);
        tv_next.setVisibility(View.VISIBLE);
        iv_dialogback.setVisibility(View.GONE);
        tv_categoryName.setText(R.string.select_date_of_service);
        if(!VariableConstant.SCHEDULEDDATE.equals("")){

            Calendar calendarServerTime = Utilities.getCalendarServerTime();
            calendarServerTime.setTimeInMillis(Long.parseLong(VariableConstant.SCHEDULEDDATE)*1000);
            Log.d(TAG, "showDateSelector: "+calendarServerTime.get(Calendar.DAY_OF_MONTH));
            checkSelectedDate(calendarServerTime.get(Calendar.DAY_OF_MONTH));

        }else{
            setCalUnselect(include8);
        }
        screenNo=2;
    }

    private void checkSelectedDate(int i) {
        Calendar tempCalendar = Utilities.getCalendarServerTime();
        ConstraintLayout selectedView=null;
        for (ConstraintLayout calView:calAll) {
            tempCalendar.setTimeInMillis((Long) calView.getTag());

            if(i==tempCalendar.get(Calendar.DAY_OF_MONTH)){
                selectedView=calView;
                setCalUnselect(selectedView);
                break;
            }
        }
    }

    private void showTimeSelector() {
        tempBookingType=2;
        cl_dateSelector.setVisibility(View.GONE);
        cl_now_later.setVisibility(View.GONE);
        cl_timeSelector.setVisibility(View.VISIBLE);
        tv_cancel.setVisibility(View.VISIBLE);
        tv_book.setVisibility(View.VISIBLE);
        tv_next.setVisibility(View.GONE);
        tv_quantity.setText(numberOfHours+"");
        tv_categoryName.setText(R.string.select_time_hours);
        /*tempTime.set(Calendar.HOUR_OF_DAY,8);
        tempTime.set(Calendar.MINUTE,0);*/
        tempTime.set(Calendar.HOUR_OF_DAY,tp_timePicker.getCurrentHour());
        tempTime.set(Calendar.MINUTE,tp_timePicker.getCurrentMinute()*TIME_PICKER_INTERVAL);
        screenNo=3;
    }

    private void checkNowOrLater() {
        if(cl_now.isSelected()){
            tv_book.setVisibility(View.VISIBLE);
            tv_next.setVisibility(View.GONE);
        }else{
            tv_book.setVisibility(View.GONE);
            tv_next.setVisibility(View.VISIBLE);
        }
    }

    private void setCalUnselect(View view) {
        for(int i=0;i<calAll.size();i++){
            if(view.getId()!=calAll.get(i).getId()){//Unselected
                calAll.get(i).setSelected(false);
                if(calAll.get(i).getId()==R.id.i_other){
                    ((TextView)i_other.findViewById(R.id.tvCalDay)).setVisibility(View.INVISIBLE);
                    ((TextView)i_other.findViewById(R.id.tvCalDate)).setVisibility(View.INVISIBLE);
                    ((TextView)i_other.findViewById(R.id.tvCalMonth)).setVisibility(View.INVISIBLE);
                    ((TextView)i_other.findViewById(R.id.others)).setVisibility(View.VISIBLE);
                }
            }else{//Selected
                calAll.get(i).setSelected(true);
                if(calAll.get(i).getId()!=R.id.i_other){
                    mCalendar.setTimeInMillis((long)calAll.get(i).getTag());
                    tempTime.set(Calendar.DAY_OF_MONTH,mCalendar.get(Calendar.DAY_OF_MONTH));
                    tempTime.set(Calendar.MONTH,mCalendar.get(Calendar.MONTH));
                    Log.d(TAG, "onClick: temptime:"+tempTime.getTime());
                }else{
                    if(!VariableConstant.SCHEDULEDDATE.equals("")){
                        mCalendar.setTimeInMillis((long)calAll.get(i).getTag());
                        tempTime.set(Calendar.DAY_OF_MONTH,mCalendar.get(Calendar.DAY_OF_MONTH));
                        tempTime.set(Calendar.MONTH,mCalendar.get(Calendar.MONTH));
                    }
                    ((TextView)i_other.findViewById(R.id.tvCalDay)).setVisibility(View.VISIBLE);
                    ((TextView)i_other.findViewById(R.id.tvCalMonth)).setVisibility(View.VISIBLE);
                    ((TextView)i_other.findViewById(R.id.tvCalDate)).setVisibility(View.VISIBLE);
                    ((TextView)i_other.findViewById(R.id.others)).setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    private void openDatePicker() {
        Calendar calendarMin = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        Calendar calendarMax = Calendar.getInstance(VariableConstant.BOOKINGTIMEZONE);
        long systemCurrenttime=System.currentTimeMillis();
        calendarMin.setTime(calendarAfter7Days.getTime()); // Set min now
        calendarMax.setTime(new Date(systemCurrenttime-VariableConstant.SERVERTIMEDIFFERNCE + TimeUnit.DAYS.toMillis(150)));
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //
                Calendar calendar=Calendar.getInstance(VariableConstant.TIMEZONE);
                calendar.set(year,monthOfYear,dayOfMonth,8,8);
                ((TextView)i_other.findViewById(R.id.tvCalDay)).setText(dateFormat.format(calendar.getTime()));
                ((TextView)i_other.findViewById(R.id.tvCalMonth)).setText(dateFormatMonth.format(calendar.getTime()));
                ((TextView)i_other.findViewById(R.id.tvCalDate)).setText(calendar.get(Calendar.DAY_OF_MONTH)+"");
                i_other.setTag(calendar.getTimeInMillis());
                tempTime.set(Calendar.YEAR,year);
                tempTime.set(Calendar.MONTH,monthOfYear);
                tempTime.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                i_other.setSelected(true);
                setCalUnselect(i_other);
            }

        },calendarMin.get(Calendar.YEAR), calendarMin.get(Calendar.MONTH), calendarMin.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());
        //fromDatePickerDialog.getDatePicker().updateDate(calendarMin.get(Calendar.YEAR),calendarMin.get(Calendar.MONTH),calendarMin.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMinDate(calendarMin.getTimeInMillis());
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                include8.setSelected(true);
                setCalUnselect(include8);
            }
        });
        fromDatePickerDialog.show();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onTimeChanged(TimePicker timePicker, int i, int i1) {
        Calendar currentTime= Utilities.getCalendarServerTime();

        Calendar currentTimePlus1=Utilities.getCalendarServerTime();
        currentTimePlus1.set(Calendar.HOUR_OF_DAY,(currentTime.get(Calendar.HOUR_OF_DAY)+1));


        Calendar selectedTime=Utilities.getCalendarServerTime();
        selectedTime.setTimeInMillis(tempTime.getTimeInMillis());
        selectedTime.set(Calendar.HOUR_OF_DAY,i);
        selectedTime.set(Calendar.MINUTE,(i1*TIME_PICKER_INTERVAL));
        Log.d(TAG, "onTimeChanged:temptime"+tempTime.getTime()
                +" Selectedtime:"+selectedTime.getTime()
                +" currentTimePlus1:"+currentTimePlus1.getTime());
        if(selectedTime.before(currentTimePlus1))
        {
            alertProgress.alertinfoSingle("Please select time atleast 1 hr later from now");
            timePicker.setCurrentHour(currentTimePlus1.get(Calendar.HOUR_OF_DAY));
            timePicker.setCurrentMinute(3);
        }else{
            tempTime.set(Calendar.HOUR_OF_DAY,i);
            tempTime.set(Calendar.MINUTE,(i1*TIME_PICKER_INTERVAL));
            Log.d(TAG, "onTimeChanged: tempTime:"+tempTime.getTime()+" currentMinute:"+timePicker.getCurrentMinute());
        }
    }
}
