package com.Kyhoot.pojoResponce;

/**
 * Created by ${3Embed} on 16/4/18.
 */
public class PaymentMode
{
    private boolean cash;

    private boolean card;

    private boolean wallet;

    public boolean getCash ()
    {
        return cash;
    }

    public void setCash (boolean cash)
    {
        this.cash = cash;
    }

    public boolean getCard ()
    {
        return card;
    }

    public void setCard (boolean card)
    {
        this.card = card;
    }

    public boolean getWallet ()
    {
        return wallet;
    }

    public void setWallet (boolean wallet)
    {
        this.wallet = wallet;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cash = "+cash+", card = "+card+", wallet = "+wallet+"]";
    }
}
