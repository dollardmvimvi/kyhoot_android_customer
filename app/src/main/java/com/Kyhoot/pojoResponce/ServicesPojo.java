package com.Kyhoot.pojoResponce;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${3Embed} on 31/1/18.
 */

public class ServicesPojo implements Serializable {
    transient private String message;

    private ArrayList<ServicesData> data;
    private CategoryData categoryData;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<ServicesData> getServicesData ()
    {
        return data;
    }

    public ArrayList<ServicesData> getData() {
        return data;
    }

    public void setData(ArrayList<ServicesData> data) {
        this.data = data;
    }

    public CategoryData getCategoryData() {
        return categoryData;
    }

    public void setCategoryData(CategoryData categoryData) {
        this.categoryData = categoryData;
    }

    public void setServicesData (ArrayList<ServicesData> servicesData)
    {
        this.data = servicesData;
    }


    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }
}
