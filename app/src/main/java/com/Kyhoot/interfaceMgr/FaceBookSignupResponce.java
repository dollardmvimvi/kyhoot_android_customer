package com.Kyhoot.interfaceMgr;

/**
 * <h>FaceBookSignupResponce</h>
 * Created by Ali on 8/25/2017.
 */

public interface FaceBookSignupResponce
{
    void onsuccess(String fName, String Lname, String email, String birthday, String fbId);
    void onError(String errorMsg);
}
