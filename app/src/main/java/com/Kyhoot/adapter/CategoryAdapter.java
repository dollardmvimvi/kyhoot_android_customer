package com.Kyhoot.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.Kyhoot.R;
import com.Kyhoot.interfaceMgr.CategoryUpdateInterface;
import com.Kyhoot.main.HomeFragment;
import com.Kyhoot.models.HomeFragmentModel;
import com.Kyhoot.pojoResponce.CategoryData;
import com.Kyhoot.pojoResponce.Elements;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ${3Embed} on 26/3/18.
 */

public class CategoryAdapter extends RecyclerView.Adapter  {
    //this interface is implemented in HomeFragment
    private final CategoryUpdateInterface categoryChangeListener;
    private ArrayList<CategoryData> categoryDataArray;
    private boolean isHorizontal = true;
    private Context mContext;
    private HomeFragment homeFragment;
    private ArrayList<Elements> duration=null;
    private LinkedHashMap<String,LatLng> idsLatLngList;
    private ArrayList<String> categoryIds;
    private static final String TAG = "CategoryAdapter";
    public CategoryAdapter(Context mcontext, ArrayList<CategoryData> categoryDataArray, boolean isHorizontal, HomeFragment fragment, CategoryUpdateInterface categoryChangeListener)
    {
        this.categoryDataArray=categoryDataArray;
        this.mContext=mcontext;
        this.isHorizontal=isHorizontal;
        this.homeFragment=fragment;
        this.categoryChangeListener=categoryChangeListener;
    }

    public void setCategoryDataArray(ArrayList<CategoryData> categoryDataArray) {
        this.categoryDataArray = categoryDataArray;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType==1){
            view = LayoutInflater.from(mContext).inflate(R.layout.category_rv_item_h, parent, false);
            return new CategoryImageVH_H(view);
        }else{
            view = LayoutInflater.from(mContext).inflate(R.layout.category_rv_item_v, parent, false);
            return new CategoryImageVH_V(view);
        }
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CategoryData categoryData=categoryDataArray.get(position);
        CategoryImageVH_V hHolder=(CategoryImageVH_V)holder;
        hHolder.tv_category_name.setText(categoryData.getCat_name());
        categoryIds=HomeFragmentModel.categoryIds;
        idsLatLngList=HomeFragmentModel.idLatLngSet;
        int keySetposition=-1;
        String imageUrl="";
        int text_color;
        if(categoryData.isSelected()){
            imageUrl=categoryData.getSel_img();
            text_color=mContext.getResources().getColor(R.color.blue_color);
        }else{
            imageUrl=categoryData.getUnsel_img();
            text_color=mContext.getResources().getColor(android.R.color.tab_indicator_text);
        }
        if(imageUrl!=null || !imageUrl.equals("")){
            Picasso.with(mContext).load(imageUrl)
                    .into(hHolder.image);
        }
        hHolder.tv_category_name.setTextColor(text_color);
        if(idsLatLngList!=null && categoryIds!=null){
            keySetposition = -1;
            int i =0 ;
            Log.d(TAG, " CategoryAdapter onBindViewHolder: "+idsLatLngList.size());
            for(Map.Entry m : idsLatLngList.entrySet())
            {
                m.getKey();
                if(categoryData.get_id().equals(m.getKey())){
                    keySetposition=i;
                }
                i++;
            }
        }
        if(holder.getItemViewType()==1){
            ((CategoryImageVH_H)holder).tv_distance.setTextColor(text_color);
            if(duration!=null && idsLatLngList!=null && duration.size()==idsLatLngList.size()){
                if(keySetposition==-1){
                    ((CategoryImageVH_H)holder).tv_distance.setText("No Providers" );
                }else{
                    if(duration!=null && duration.get(keySetposition).getStatus().equalsIgnoreCase("OK")){
                        //Log.d(TAG, "onBindViewHolder: durationsize:"+duration.size()+" " +keySetposition);
                        String units= Utilities.getEtaInUnits(duration.get(keySetposition).getDistance().getValue());
                        ((CategoryImageVH_H)holder).tv_distance.setText(units);
                    }
                }
                //Setting eta on categories to null if new providers are available
            }else if(idsLatLngList!=null && idsLatLngList.size()==0){
                ((CategoryImageVH_H)holder).tv_distance.setText("No Providers" );
            }
        }
    }



    @Override
    public int getItemCount() {
        int itemCount=0;
        if(categoryDataArray!=null )
            itemCount=categoryDataArray.size();
        return itemCount;
    }

    @Override
    public int getItemViewType(int position) {
       if(isHorizontal ){
           return 1;
       }else{
           return 0;
       }
    }

    public void setEta(ArrayList<Elements> duration) {
        this.duration=duration;
        notifyDataSetChanged();
    }

    public class CategoryImageVH_V extends RecyclerView.ViewHolder{
        ImageView image;
        TextView tv_category_name;
        //ConstraintLayout cl_category_item;
        public CategoryImageVH_V(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.tv_category_image);
            tv_category_name=itemView.findViewById(R.id.tv_category_name);
            tv_category_name.setTypeface(AppTypeface.getInstance(mContext).getHind_regular());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=getAdapterPosition();
                    if(pos!=RecyclerView.NO_POSITION){
                        CategoryData categoryData = categoryDataArray.get(getAdapterPosition());
                        // Setting the ui from this data====================
                        for(int i=0;i<categoryDataArray.size();i++){
                            if(i!=getAdapterPosition()){
                                categoryDataArray.get(i).setSelected(false);
                            }else{
                                categoryDataArray.get(i).setSelected(true);
                            }
                        }
                        //===================================================
                        //Updating categories if category is different from selected category===================================
                        if(!VariableConstant.selectedCategoryId.equals(categoryDataArray.get(getAdapterPosition()).get_id())){
                            VariableConstant.selectedCategoryId=categoryDataArray.get(getAdapterPosition()).get_id();
                            VariableConstant.BILLINGMODEL=categoryDataArray.get(getAdapterPosition()).getBilling_model();
                            VariableConstant.MINIMUM_HOUR=categoryDataArray.get(getAdapterPosition()).getMinimum_hour();
                            //VariableConstant.SURGE_PRICE=categoryDataArray.get(getAdapterPosition()).getSurgePrice();
                            VariableConstant.SERVICEHOURLYPRICE=categoryDataArray.get(getAdapterPosition()).getPrice_per_fees();
                            VariableConstant.SELECTEDCATEGORYNAME=categoryDataArray.get(getAdapterPosition()).getCat_name();
                            VariableConstant.SERVICE_TYPE=categoryDataArray.get(getAdapterPosition()).getService_type();
                            VariableConstant.SELECTEDCATEGORYVISITFEE=categoryDataArray.get(getAdapterPosition()).getVisit_fees();
                            VariableConstant.BOOKINGTYPEAVAILABILITY=categoryDataArray.get(getAdapterPosition()).getBookingTypeAction();
                            homeFragment.notifyAdapterNSetButton(categoryData.get_id(),categoryData.getCat_name());
                            categoryChangeListener.onCategoryChanged();
                        }
                        if(!isHorizontal){
                            homeFragment.listFromBookNow=false;
                            homeFragment.showListview();
                        }
                        //======================================================================================================
                    }
                }
            });
        }
    }
    class CategoryImageVH_H extends CategoryImageVH_V{
        TextView tv_distance;
        public CategoryImageVH_H(View itemView) {
            super(itemView);
            tv_distance=itemView.findViewById(R.id.tv_distance);
            tv_distance.setTypeface(AppTypeface.getInstance(mContext).getHind_regular());
        }
    }
}



