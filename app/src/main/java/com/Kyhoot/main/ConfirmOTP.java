package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.Kyhoot.R;
import com.Kyhoot.controllers.ConfirmOTPControlr;
import com.Kyhoot.interfaceMgr.ProfileDataResponce;
import com.Kyhoot.interfaceMgr.VerifyOtpIntrface;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.AppTypeface;
import com.Kyhoot.utilities.MySMSBroadcastReceiver;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;

public class ConfirmOTP extends AppCompatActivity implements View.OnClickListener,TextWatcher,
        VerifyOtpIntrface, ProfileDataResponce, View.OnFocusChangeListener {

    private EditText et_otp1,et_otp2,et_otp3,et_otp4;
    private IntentFilter intentFilter;
    //private ReadSms readSms;
    private MySMSBroadcastReceiver mySMSBroadcastReceiver;
    private AlertProgress aProgress;
    private ProgressDialog pDialog;
    private String sid;
    private ConfirmOTPControlr confrmOtpContrl;
    private String signupOrForgot;
    private AppTypeface appTypeface;
    private TextView tvTimerOtp, tv_opt_resend;
    private String mobileNumber;
    private String countryCode;

    private SharedPrefs sharedPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        setContentView(R.layout.activity_confirm_otp);
        appTypeface = AppTypeface.getInstance(this);
        sharedPrefs = new SharedPrefs(this);
        IntentBundle();
        initializeToolBar();
        initializeEditText();
        initializeObj();
        //autoReadSms();
        autoSmsRetriever();
    }

    public void autoSmsRetriever() {

        mySMSBroadcastReceiver= new MySMSBroadcastReceiver() {
            @Override
            protected void onSmsReceived(String s) {
                Log.e(TAG, "onSmsReceived: TAG_SMS received  "+s );
                String splitted[] =s.split(" ");
                if(splitted[0].length()==4){
                    String one = splitted[0].charAt(0)+"";
                    String tw = splitted[0].charAt(1)+"";
                    String thr = splitted[0].charAt(2)+"";
                    String fur = splitted[0].charAt(3)+"";
                    et_otp1.setText(one);
                    et_otp2.setText(tw);
                    et_otp3.setText(thr);
                    et_otp4.setText(fur);

                }
            }
        };
        intentFilter = new IntentFilter("SmsRetriever.SMS_RETRIEVED_ACTION");
        intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);



        // Get an instance of SmsRetrieverClient, used to start listening for a matching
        // SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(this /* context */);

        // Starts SmsRetriever, which waits for ONE matching SMS message until timeout
        // (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
        // action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

        // Listen for success/failure of the start Task. If in a background thread, this
        // can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
                Log.e("TAG_SMS", "onSuccess:  OTP SMS");
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
                Log.e("TAG_SMS", "onFailure:  OTP SMS");
            }
        });
    }

    private void IntentBundle()
    {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            signupOrForgot = bundle.getString("SignupOrForgot");
            sid = bundle.getString("SID");
            mobileNumber = bundle.getString("mobile");
            if (getIntent().hasExtra("countryCode")) {
                countryCode=bundle.getString("countryCode");
            }
        }
    }

    private void initializeObj() {
        aProgress = new AlertProgress(this);
        confrmOtpContrl = new ConfirmOTPControlr(this);
        pDialog =  aProgress.getProgressDialog(getString(R.string.verifyingOtp));
        TextView tv_otp_verify = findViewById(R.id.tv_otp_verify);
        tv_opt_resend = findViewById(R.id.tv_opt_resend);
        TextView tvConfirmOtpHlp = findViewById(R.id.tvConfirmOtpHlp);
        String otpMessage=getString(R.string.otpmessage1)+" "+mobileNumber+getString(R.string.otpmessage2);
        tvConfirmOtpHlp.setText(otpMessage);
        tvTimerOtp = findViewById(R.id.tvTimerOtp);
        tv_otp_verify.setTypeface(appTypeface.getHind_semiBold());
        tv_opt_resend.setTypeface(appTypeface.getHind_semiBold());
        tvConfirmOtpHlp.setTypeface(appTypeface.getHind_regular());
        tvTimerOtp.setTypeface(appTypeface.getHind_semiBold());
        LinearLayout linmain = findViewById(R.id.linmain);
        tv_otp_verify.setOnClickListener(this);
        tv_opt_resend.setOnClickListener(this);
        tvTimerOtp.setOnClickListener(this);
        confrmOtpContrl.setTimerOntheTime(tvTimerOtp, this);
        tv_opt_resend.setEnabled(false);
    }

    /*
    initializing the edit text
     */
    private void initializeEditText() {
        et_otp1 = findViewById(R.id.et_otp1);
        et_otp2 = findViewById(R.id.et_otp2);
        et_otp3 = findViewById(R.id.et_otp3);
        et_otp4 = findViewById(R.id.et_otp4);
        et_otp1.setTypeface(appTypeface.getHind_semiBold());
        et_otp2.setTypeface(appTypeface.getHind_semiBold());
        et_otp3.setTypeface(appTypeface.getHind_semiBold());
        et_otp4.setTypeface(appTypeface.getHind_semiBold());
        et_otp1.setOnFocusChangeListener(this);
        et_otp2.setOnFocusChangeListener(this);
        et_otp3.setOnFocusChangeListener(this);
        et_otp4.setOnFocusChangeListener(this);
        et_otp1.addTextChangedListener(this);
        et_otp2.addTextChangedListener(this);
        et_otp3.addTextChangedListener(this);
        et_otp4.addTextChangedListener(this);
    }

    /*
    initializing the tool bar
     */
    private void initializeToolBar()
    {

        Toolbar login_tool = findViewById(R.id.tool_otp);
        setSupportActionBar(login_tool);
        TextView tv_center = findViewById(R.id.tv_center);
        getSupportActionBar().setTitle("");
        tv_center.setText(R.string.verifyYourNumber);
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        login_tool.setNavigationIcon(R.drawable.ic_login_back_icon_off);
        login_tool.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        //LocalBroadcastManager.getInstance(this).registerReceiver(mySMSBroadcastReceiver,intentFilter);
        ConfirmOTP.this.registerReceiver(mySMSBroadcastReceiver, intentFilter);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        if(s==et_otp1.getEditableText())
        {
            if(et_otp1.getText().toString().length()==1)
                et_otp2.requestFocus();
        }
        else if(s==et_otp2.getEditableText())
        {
            if(et_otp2.getText().toString().length()==1)
                et_otp3.requestFocus();
        }

        else if(s==et_otp3.getEditableText())
        {
            if(et_otp3.getText().toString().length()==1)
                et_otp4.requestFocus();
        }
        else if(s==et_otp4.getEditableText())
        {
            if(et_otp4.getText().toString().length()==1)
                methodToVerifyOTP();
        }
    }

    /**
     * <h1>methodToVerifyOTP</h1>
     * <p>
     * this is verifying the otp insert by the users and calling the service
     * </p>
     */

    private void methodToVerifyOTP() {
        if(!TextUtils.isEmpty(et_otp1.getText().toString()) && !TextUtils.isEmpty(et_otp2.getText().toString()) &&
                !TextUtils.isEmpty(et_otp3.getText().toString()) && !TextUtils.isEmpty(et_otp4.getText().toString()))
        {
            if(aProgress.isNetworkAvailable())
                verifyCode();
            else
                aProgress.showNetworkAlert();
        }
    }

    /**
     * <h1>verifyCode</h1>
     * <p>
     * this is to verify the code insert by user and call service
     * </p>
     */

    void verifyCode() {
        pDialog = aProgress.getProgressDialog(getString(R.string.verifyingOtp));
        pDialog.show();
        String s = (et_otp1.getText().toString() + "" + et_otp2.getText().toString() + "" +
                et_otp3.getText().toString() + "" + et_otp4.getText().toString());
        confrmOtpContrl.verifyOtp(this, s, sid, signupOrForgot);
    }

    @Override
    public void onClick(View v)
    {
      switch (v.getId())
      {
          case R.id.tv_otp_verify:

              verifyCode();
              break;
         /* case R.id.tv_opt_resend:
              if(tv_opt_resend.isEnabled())
              {
                  if(tvTimerOtp.getText().toString().equals(getString(R.string.resendcode)))
                  {
                      pDialog = aProgress.getProgressDialog(getString(R.string.generatingotp));
                      pDialog.show();
                      confrmOtpContrl.resendOtp(this,sid,signupOrForgot);
                  }

              }

              break;*/
          case R.id.tvTimerOtp:
              if (tvTimerOtp.getText().toString().equals(getString(R.string.resendcode))) {
                  pDialog = aProgress.getProgressDialog(getString(R.string.resending));
                  pDialog.show();
                  confrmOtpContrl.resendOtp(this, sid, signupOrForgot);
              }
              break;
      }
    }
    @Override
    public void onOtpVerify(String success) {
        pDialog.dismiss();
        Intent inent;
        switch (signupOrForgot) {
            case "SIGNUP":
                Intent intnet = new Intent(this, SplashActivity.class);
                intnet.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intnet);
                finish();
               /* if(!ApplicationController.getInstance().isMqttConnected())
                    ApplicationController.getInstance().createMQttConnection(new SharedPrefs(this).getCustomerSid());
                inent = new Intent(this, MenuActivity.class);
                inent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(inent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                finish();*/

                break;
            case "PROFILE":
                sharedPrefs.setMobileNo(mobileNumber);
                sharedPrefs.setCountryCode(countryCode);
                finish();
                break;
            default:
                inent = new Intent(this, NewPassword.class);
                inent.putExtra("SID", sid);
                startActivity(inent);
                finish();
                break;
        }

    }

    @Override
    public void onOtpError(String error)
    {
        pDialog.dismiss();
        //   sDialog.dismiss();
        aProgress.alertinfo(error);
        et_otp1.setText(null);et_otp2.setText(null);et_otp3.setText(null);et_otp4.setText(null);
    }


    @Override
    public void onSignUpSuccess(String msg) {

        tv_opt_resend.setEnabled(false);
        //  tvTimerOtp.setVisibility(View.VISIBLE);
        tv_opt_resend.setTextColor(Utilities.getColor(this, R.color.colorAccent));
        confrmOtpContrl.setTimerOntheTime(tvTimerOtp, this);
        pDialog.dismiss();
        aProgress.alertinfo(msg);
    }

    @Override
    public void onPhoneNumError() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mySMSBroadcastReceiver);
        ConfirmOTP.this.unregisterReceiver(mySMSBroadcastReceiver);//registerReceiver();
        if(pDialog!=null)
        {
            pDialog.dismiss();pDialog.cancel();
        }
    }

    @Override
    public void successUpdateUi() {

    }

    @Override
    public void sessionExpired(String message) {
        tv_opt_resend.setEnabled(true);
        tv_opt_resend.setTextColor(Utilities.getColor(this, R.color.black_john));

    }


    @Override
    public void onFocusChange(View view, boolean b) {
        if(b){
            ((EditText)view).setHint("");
        }else{
            //if(((EditText)view).getText().equals("")){
                Log.d("CONFIRMOTP", "onFocusChange: "+((EditText)view).getText());
                ((EditText)view).setHint(R.string.bullethint);
            //}
        }
    }
}
