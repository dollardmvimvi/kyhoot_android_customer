package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.Kyhoot.R;
import com.Kyhoot.adapter.ScheduleListAdapter;
import com.Kyhoot.controllers.ScheduleController;
import com.Kyhoot.interfaceMgr.scheduleInterFace;
import com.Kyhoot.pojoResponce.ScheduleMonthPojo;
import com.Kyhoot.pojoResponce.Slot;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.SharedPrefs;
import com.Kyhoot.utilities.Utilities;
import com.Kyhoot.utilities.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ScheduleActivity extends AppCompatActivity  implements  CompactCalendarView.CompactCalendarViewListener, View.OnClickListener, scheduleInterFace
{

    private static final String TAG = "ScheduleActivity";
    private CompactCalendarView compactCalendarView;
    private TextView tvMonth,tvNoScheduleMsg;
    private SimpleDateFormat displayMonthFormat,sendingMonthFormat;
    private RecyclerView rvShedule;
    private ScheduleListAdapter scheduleListAdapter;
    private ArrayList<Slot> slots;
    private ArrayList<Event> calendarEvents;

    private FloatingActionButton fabAddSchedule,fabView,fabAdd;
    private TextView tvViewSchedule,tvAddSchedule;
    private Animation fade_open, fade_close,rotate_forward,rotate_backward;
    private Animation fade_half_open, fade_half_close;
    public boolean isFabOpen = false;
    private LinearLayout llSchedule;

    private ProgressBar progressBarConfirm,progress_bar;
    private AlertProgress aProgress;
    private SharedPrefs sPrefs;

    private ProgressDialog progressDialog;
    private boolean isFirstTime;
    private String currentDate = "";

    private BroadcastReceiver receiver;
    private IntentFilter filter;

    private ScheduleController scheduleController;

    ImageView ivPrevBtn;

    int currentMonth ;
    int selectedMonth ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        initializeView();

        String providerId = VariableConstant.providerId;
        Log.i("SAITESTING","providerId-->"+providerId.toString());
    }

    private void initializeView()
    {

        progress_bar=findViewById(R.id.progress_bar);
        progressBarConfirm = findViewById(R.id.progressBarConfirm);
        sPrefs = SharedPrefs.getInstance(this);
        aProgress = AlertProgress.getInstance(this);

        scheduleController = new ScheduleController(this,sPrefs);

        /*filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);*/
        /*receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                presenter.getSchedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),false);
            }
        };*/


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingSchedule));
        progressDialog.setCancelable(false);



       /* Typeface fontBold = Utility.getFontBold(getActivity());
        Typeface fontMedium = Utility.getFontMedium(getActivity());
        Typeface fontRegular = Utility.getFontRegular(getActivity());*/

        displayMonthFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
        sendingMonthFormat = new SimpleDateFormat("MM-yyyy", Locale.getDefault());

        calendarEvents = new ArrayList<>();
        slots = new ArrayList<>();
        scheduleListAdapter = new ScheduleListAdapter(this, slots);

        tvMonth = findViewById(R.id.tvMonth);
        tvNoScheduleMsg = findViewById(R.id.tvNoScheduleMsg);
        TextView tvTitle = findViewById(R.id.tvTitle);
        compactCalendarView = findViewById(R.id.compactCalendarView);
        compactCalendarView.shouldScrollMonth(false);
        rvShedule = findViewById(R.id.rvShedule);
        ivPrevBtn = findViewById(R.id.ivPrevBtn);
        ImageView ivnextBtn = findViewById(R.id.ivnextBtn);
        fabAddSchedule = findViewById(R.id.fabAddSchedule);
        fabView = findViewById(R.id.fabView);
        fabAdd = findViewById(R.id.fabAdd);
        tvViewSchedule = findViewById(R.id.tvViewSchedule);
        tvAddSchedule = findViewById(R.id.tvAddSchedule);
        llSchedule = findViewById(R.id.llSchedule);

        /*fade_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(getActivity(),R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate_left_to_center);
        fade_half_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_half_open);
        fade_half_close = AnimationUtils.loadAnimation(getActivity(),R.anim.fade_half_close);*/

        rvShedule.setLayoutManager(new LinearLayoutManager(this));
        rvShedule.setAdapter(scheduleListAdapter);
       /* tvMonth.setTypeface(fontRegular);
        tvNoScheduleMsg.setTypeface(fontRegular);
        tvTitle.setTypeface(fontBold);*/
        tvMonth.setText(displayMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        compactCalendarView.setDayColumnNames(getResources().getStringArray(R.array.calendarDays));
        compactCalendarView.setListener(this);

        ivPrevBtn.setOnClickListener(this);
        ivnextBtn.setOnClickListener(this);
        fabAddSchedule.setOnClickListener(this);
        fabView.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
        tvAddSchedule.setOnClickListener(this);
        tvViewSchedule.setOnClickListener(this);

        //get current month in order to compare with the selected month
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
        currentMonth = month+1;


        isFirstTime = true;
        // presenter.getSchedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),true);

        if(Utilities.isNetworkAvailable(ScheduleActivity.this)){
            scheduleController.checkSchedule(sPrefs,sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),true);
            currentDate =  sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());

            String date=currentDate;
            String[] items1 = date.split("-");
            String month0=items1[0];




        }


    }

    @Override
    public void onDayClick(Date dateClicked) {
        slots.clear();
        slots.addAll(scheduleController.createSlotFromSchedules(compactCalendarView.getEvents(dateClicked)));
        scheduleListAdapter.notifyDataSetChanged();

        if(slots.size() == 0)
        {
            tvNoScheduleMsg.setVisibility(View.VISIBLE);
        }
        else
        {
            tvNoScheduleMsg.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMonthScroll(Date firstDayOfNewMonth) {

        tvMonth.setText(displayMonthFormat.format(firstDayOfNewMonth));
        if(currentDate.equals(sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth())))
        {
            scheduleController.checkSchedule(sPrefs,sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),true);
            // presenter.getSchedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),true);
            String month = sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());

            String date=month;
            String[] items1 = date.split("-");
            String month1=items1[0];
            selectedMonth = Integer.parseInt(month1);
            showIconVisiblity();

        }
        else
        {
            scheduleController.checkSchedule(sPrefs,sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),false);
            //  presenter.getSchedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()),false);
            String month = sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());

            Log.i("SAITESTING","currrentmonth -->"+month);

            String date=month;
            String[] items1 = date.split("-");
            String month2=items1[0];
            selectedMonth = Integer.parseInt(month2);
            showIconVisiblity();

        }
    }

    //this method is is used to compare the selected and current month by this we can hide the back arrow button
    private void showIconVisiblity()
    {
        if(selectedMonth>currentMonth)
        {
            ivPrevBtn.setVisibility(View.VISIBLE);
            //  compactCalendarView.shouldScrollMonth(true);
        }else{
            ivPrevBtn.setVisibility(View.GONE);
            // compactCalendarView.shouldScrollMonth(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivnextBtn:
                compactCalendarView.showNextMonth();
                break;

            case R.id.ivPrevBtn:
                compactCalendarView.showPreviousMonth();
                break;

            case R.id.fabAddSchedule:
                // animateFAB();
                break;

            case R.id.fabAdd:
            case R.id.tvAddSchedule:
                /*animateFAB();
                Intent intentAdd=new Intent(getActivity(),ScheduleAddActivity.class);
                startActivity(intentAdd);
                getActivity().overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);*/
                break;

            case R.id.fabView:
            case R.id.tvViewSchedule:
               /* animateFAB();
                Intent intentView=new Intent(getActivity(),ScheduleViewActivity.class);
                startActivity(intentView);
                getActivity().overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);*/
                break;
        }
    }


    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccessGetSchedule(String result) {

    }

    @Override
    public void onFailure(String failureMsg) {

    }

    @Override
    public void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo) {
        hideProgress();
        calendarEvents.clear();
        compactCalendarView.removeAllEvents();
        compactCalendarView.addEvents(scheduleController.getEvents(scheduleMonthPojo.getData(),calendarEvents));
        if(isFirstTime)
        {
            isFirstTime = false;
            onDayClick(new Date());
        }
        else
        {
            onDayClick(compactCalendarView.getFirstDayOfCurrentMonth());
        }
    }

    @Override
    public void onError(String error) {
        if (!error.equals("")) {
            aProgress.alertinfo(error);
        }
        hideProgress();
    }

    @Override
    public void onSessionError(String error) {
        Utilities.setMAnagerWithBID(ScheduleActivity.this, sPrefs,error);
    }
}
