package com.Kyhoot.main.wallet.wallet_transaction_activity;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Kyhoot.pojoResponce.CreditDebitTransctions;
import com.Kyhoot.utilities.Alerts;
import com.Kyhoot.R;
import com.Kyhoot.adapter.WalletViewPagerAdapter;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * <h1>WalletFragment</h1>
 * <p> Class to load WalletTransActivity and show all transactions list </p>
 */
public class WalletTransActivity extends AppCompatActivity
        implements WalletTransactionContract.WalletTrasactionView
{
    private ArrayList<CreditDebitTransctions> allTransactionsAL;
    private ArrayList<CreditDebitTransctions> debitTransactionsAL;
    private ArrayList<CreditDebitTransctions> creditTransactionsAL;
    private WalletTransactionsFragment allTransactionsFrag, debitsFrag, creditsFrag;
    private Alerts alerts;
    private ProgressDialog pDialog;

    Toolbar toolBar;
    RelativeLayout rlABarBackBtn ;
    TextView tvAbarTitle;
    ViewPager viewPager;
    WalletTransactionActivityPresenter walletTransPresenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transactions);
        alerts = new Alerts(this);
        walletTransPresenter=new WalletTransactionActivityPresenter(this,this);
        pDialog=new ProgressDialog(this);
        initToolBar();
        initViews();
    }

    /* <h2>initToolBar</h2>
     * <p> method to initialize customer toolbar </p>
     */
    private void initToolBar()
    {
        toolBar=findViewById(R.id.mToolBarCustom);
        rlABarBackBtn=findViewById(R.id.rl_custom_toolBarBackBtn);
        tvAbarTitle=findViewById(R.id.tv_custom_toolBarTitle);
        viewPager=findViewById(R.id.pager);
        if(getActionBar() != null)
        {
            getActionBar().hide();
        }
        setSupportActionBar(toolBar);
        rlABarBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WalletTransActivity.this.onBackPressed();
            }
        });
        tvAbarTitle.setTypeface(AppTypeface.getInstance(this).getHind_semiBold());
        tvAbarTitle.setText(R.string.recent_trans);
    }

    public void setAllTransactionsAL(ArrayList<CreditDebitTransctions> allTransactionsAL) {
        this.allTransactionsAL = allTransactionsAL;
    }

    public void setDebitTransactionsAL(ArrayList<CreditDebitTransctions> debitTransactionsAL) {
        this.debitTransactionsAL = debitTransactionsAL;
    }

    public void setCreditTransactionsAL(ArrayList<CreditDebitTransctions> creditTransactionsAL) {
        this.creditTransactionsAL = creditTransactionsAL;
    }


    /**
     *<h2>initViews</h2>
     * <P> custom method to initializes all the views of the screen </P>
     */
    private void initViews()
    {
        viewPager.setOffscreenPageLimit(3);
        TabLayout tabLayout =  findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        String tabTitles[]  = new String[]{"All", "Debit", "Credit"};
        WalletViewPagerAdapter viewPagerAdapter = new WalletViewPagerAdapter(getSupportFragmentManager());


        this.allTransactionsFrag = WalletTransactionsFragment.getNewInstance();
        viewPagerAdapter.addFragment(allTransactionsFrag, tabTitles[0]);

        this.debitsFrag = WalletTransactionsFragment.getNewInstance();
        viewPagerAdapter.addFragment(debitsFrag, tabTitles[1]);

        this.creditsFrag = WalletTransactionsFragment.getNewInstance();
        viewPagerAdapter.addFragment(creditsFrag, tabTitles[2]);
        viewPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(viewPagerAdapter);
    }


    @Override
    public void onResume()
    {
        super.onResume();
        showProgressDialog("");

        loadTransactions(false);
    }


    /**
     * <h2>loadTransactions</h2>
     * <p> method to init getTransactionsList api </p>
     */
    public void loadTransactions(boolean isFromOnRefresh)
    {
        walletTransPresenter.initLoadTransactions(false, isFromOnRefresh);
    }



    @Override
    public void hideProgressDialog()
    {
        if(pDialog.isShowing())
        {
            pDialog.dismiss();
        }
    }



    @Override
    public void showProgressDialog(String msg)
    {
        if(pDialog == null)
        {
            pDialog = new ProgressDialog(this);
        }

        if(!pDialog.isShowing())
        {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Wait");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }


    @Override
    public void showToast(String msg, int duration)
    {
        hideProgressDialog();
        Toast.makeText(this, msg, duration).show();
    }


    @Override
    public void showAlert(String title, String msg)
    {
        hideProgressDialog();
    }


    /**
     *<h2>showAlert</h2>
     * <p> method to show alert that these is no internet connectivity there </p>
     */
    @Override
    public void noInternetAlert()
    {
        alerts.showNetworkAlert(this);
    }



    /**
     *<h2>walletTransactionsApiSuccessViewNotifier</h2>
     * <p> method to update fields data on the success response of api </p>
     */
    @Override
    public void walletTransactionsApiSuccessViewNotifier()
    {
        hideProgressDialog();
        String TAG = "WalletTransactionAct";
        Log.d(TAG, "walletTransactionsApiSuccessViewNotifier onSuccess: ");

        if(this.allTransactionsFrag != null)
        {
            this.allTransactionsFrag.hideRefreshingLayout();
            this.allTransactionsFrag.notifyDataSetChangedCustom(allTransactionsAL);
        }

        if(this.debitsFrag != null)
        {
            this.debitsFrag.hideRefreshingLayout();
            this.debitsFrag.notifyDataSetChangedCustom(debitTransactionsAL);
        }

        if(this.creditsFrag != null)
        {
            this.creditsFrag.hideRefreshingLayout();
            this.creditsFrag.notifyDataSetChangedCustom(creditTransactionsAL);
        }
    }


    @Override
    public void onBackPressed()
    {
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_acvtivity);
    }

}

