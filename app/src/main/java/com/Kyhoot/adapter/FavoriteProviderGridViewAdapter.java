package com.Kyhoot.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.Kyhoot.R;
import com.Kyhoot.pojoResponce.FavProRespData;
import com.Kyhoot.utilities.AppTypeface;

import java.util.ArrayList;

/**
 * Created by ${3Embed} on 18/6/18.
 */
public class FavoriteProviderGridViewAdapter extends RecyclerView.Adapter<FavoriteProviderGridViewAdapter.FavoriteProvider>{

    private final FavProviderFragmentCallback fragmentCallback;
    private ArrayList<FavProRespData> favoriteProviderList;
    public FavoriteProviderGridViewAdapter(ArrayList<FavProRespData> favoriteProvidersList, FavProviderFragmentCallback fragmentCallback) {
        this.favoriteProviderList=favoriteProvidersList;
        this.fragmentCallback=fragmentCallback;
    }

    @NonNull
    @Override
    public FavoriteProvider onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_provider_layout_grid,parent,false);
        return new FavoriteProvider(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteProvider holder, int position) {
        FavProRespData temp=favoriteProviderList.get(position);
        if(!temp.getProfilePic().isEmpty()){
            Glide.with(holder.itemView.getContext()).load(temp.getProfilePic()).apply(new RequestOptions().circleCrop()).into(holder.iv_favoriteProvider);
        }
        holder.tv_favProvName.setText(temp.getName());
        holder.tv_fav_categories.setText(temp.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return favoriteProviderList.size();
    }
    class FavoriteProvider extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView iv_favoriteProvider,iv_selected_icon,iv_favIcon;
        TextView tv_favProvName,tv_fav_categories;
        public FavoriteProvider(View itemView) {
            super(itemView);
            iv_favoriteProvider=itemView.findViewById(R.id.iv_favoriteProvider);
            iv_selected_icon=itemView.findViewById(R.id.iv_selected_icon);
            iv_favIcon=itemView.findViewById(R.id.iv_favIcon);
            iv_favIcon.setVisibility(View.VISIBLE);
            tv_favProvName=itemView.findViewById(R.id.tv_favProvName);
            tv_fav_categories=itemView.findViewById(R.id.tv_fav_categories);
            tv_fav_categories.setVisibility(View.VISIBLE);
            tv_favProvName.setTypeface(AppTypeface.getInstance(tv_favProvName.getContext()).getHind_regular());
            tv_fav_categories.setTypeface(AppTypeface.getInstance(tv_fav_categories.getContext()).getHind_regular());
            iv_favIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.iv_favIcon:
                    fragmentCallback.onUnFavoriting("0",favoriteProviderList.get(getAdapterPosition()).getProviderId(),getAdapterPosition());
                        break;

            }
        }
    }
    public interface FavProviderFragmentCallback{
        void onUnFavoriting(String categoryId, String providerId, int adapterPositon);
    }
}
