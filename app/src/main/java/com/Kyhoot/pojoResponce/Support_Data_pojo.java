package com.Kyhoot.pojoResponce;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ali on 10/5/2017.
 */

public class Support_Data_pojo implements Parcelable {
    @SuppressWarnings("unused")
    public static final Creator<Support_Data_pojo> CREATOR = new Creator<Support_Data_pojo>() {
        @Override
        public Support_Data_pojo createFromParcel(Parcel in) {
            return new Support_Data_pojo(in);
        }

        @Override
        public Support_Data_pojo[] newArray(int size) {
            return new Support_Data_pojo[size];
        }
    };
    private String Name;
    private ArrayList<Support_Subcat_pojo> subcat;
    private String link;
    private String desc;

    protected Support_Data_pojo(Parcel in) {
        Name = in.readString();
        link = in.readString();
        desc = in.readString();
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public ArrayList<Support_Subcat_pojo> getSubcat() {
        return subcat;
    }

    @Override
    public String toString() {
        return "ClassPojo [Name = " + Name + ", subcat = " + subcat + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(link);
        dest.writeString(desc);
    }
}
