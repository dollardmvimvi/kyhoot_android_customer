package com.Kyhoot.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.Kyhoot.R;
import com.Kyhoot.adapter.FaqAdaptr;
import com.Kyhoot.adapter.SupportAdapter;
import com.Kyhoot.interfaceMgr.SupportModelInterFace;
import com.Kyhoot.models.SupportModel;
import com.Kyhoot.pojoResponce.Support_Data_pojo;
import com.Kyhoot.pojoResponce.Support_pojo;
import com.Kyhoot.utilities.AlertProgress;
import com.Kyhoot.utilities.SharedPrefs;

import java.util.ArrayList;

/**
 * <h>FaqFragment</h>
 * this is for the frequently asked questions
 * @see  SupportModelInterFace
 * Created by 3Embed on 8/16/2017.
 */

public class FaqFragment extends Fragment implements SupportModelInterFace
{
    Context mcontext;
    FaqAdaptr fAdaptr;
    AlertProgress aprogress;
    ProgressDialog Pdialog;
    SupportAdapter adapter;
    RecyclerView mListVHelp;
    private ArrayList<Support_Data_pojo> supportview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        mcontext = getActivity();
        initializeView(view);
        return view;
    }

    /**
     * @param view this is the view of the layout
     * @see LinearLayoutManager
     */

    private void initializeView(View view) {
        supportview = new ArrayList<>();
        SupportModel suporModel = new SupportModel();
        aprogress = new AlertProgress(getActivity());
        Pdialog = aprogress.getProgressDialog(getActivity().getResources().getString(R.string.wait));
        mListVHelp = view.findViewById(R.id.mListVHelp);
        LinearLayoutManager lManager = new LinearLayoutManager(mcontext);
        mListVHelp.setLayoutManager(lManager);
        adapter = new SupportAdapter(getActivity(), supportview);
        mListVHelp.setAdapter(adapter);
        fAdaptr = new FaqAdaptr(mcontext);
        if (aprogress.isNetworkAvailable()) {
            Pdialog.show();
            suporModel.callSupportService(new SharedPrefs(getActivity()), this);
        } else
            aprogress.showNetworkAlert();
    }

    @Override
    public void onError(String errormsg) {
        Toast.makeText(getActivity(), errormsg, Toast.LENGTH_SHORT).show();
        Pdialog.cancel();

    }

    @Override
    public void onSupportSuccess(Support_pojo support_pojo) {
        Pdialog.cancel();
        supportview.addAll(support_pojo.getData());
        adapter.notifyDataSetChanged();
    }
}
